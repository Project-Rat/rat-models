// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// DESCRIPTION
// Minimalist example of a solenoid model

// header files for common
#include "rat/common/log.hh"

// header files for model
#include "pathaxis.hh"
#include "crosspoint.hh"
#include "vtkunstr.hh"
#include "modelcoil.hh"
#include "meshdata.hh"
#include "surfacedata.hh"
#include "calcmesh.hh"
#include "calcplane.hh"

// this is an example for a line current
// note that the current density inside the line
// is infinite. This is unphysical and
// therefore the softening factor determines
// the field near the line

// main function
int main(){
	// SETTINGS
	// switchboard
	const bool run_coil_field = true;
	const bool run_plane = true;

	// settings
	const rat::fltp dtape = RAT_CONST(0.1e-3);
	const rat::fltp wtape = RAT_CONST(12e-3); // these are hidden from view but used to calculate current density for example
	const rat::fltp ltape = RAT_CONST(400e-3); // these are hidden from view but used to calculate current density for example
	const rat::fltp delem = RAT_CONST(4e-3);
	const rat::fltp Iop = RAT_CONST(800.0);
	
	// output file
	const boost::filesystem::path output_dir = "./line/";
	
	// output time
	const rat::fltp output_time = RAT_CONST(0.0);


	// MODEL
	// create axial path
	const rat::mdl::ShPathAxisPr axis = rat::mdl::PathAxis::create('x','z',ltape,0,0,0,delem);

	// create a rectangular cross section object
	const rat::mdl::ShCrossPointPr point = rat::mdl::CrossPoint::create(0,0,dtape,wtape);

	// create a coil object
	const rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(axis, point);
	coil->set_number_turns(1); coil->set_operating_current(Iop);
	coil->set_num_gauss(2);


	// CALCULATION AND OUTPUT
	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(coil);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({output_time},output_dir,lg);
	}

	// create grid calculation
	if(run_plane){
		// create grid calculation
		const rat::mdl::ShCalcPlanePr plane = rat::mdl::CalcPlane::create(coil,'x',0.1,0.1,800,800);

		// calculate and write vtk output files to specified directory
		plane->calculate_write({output_time},output_dir,lg);
	}

}
