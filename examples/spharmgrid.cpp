// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>
#include <boost/filesystem.hpp>

// header files for FX-MLFMM
#include "rat/mlfmm/extra.hh"
#include "rat/mlfmm/spharm.hh"

// VTK headers
#include <vtkSmartPointer.h>
#include <vtkRectilinearGrid.h>
#include <vtkDoubleArray.h>
#include <vtkFloatArray.h>
#include <vtkXMLRectilinearGridWriter.h>
#include <vtkXMLImageDataWriter.h>
#include <vtkPointData.h>
#include <vtkImageData.h>

// DESCRIPTION
// This example writes all spherical harmonics up to nmax
// to a VTK grid. This is purely intended for educational
// purposes.

#ifdef RAT_DOUBLE_PRECISION
typedef vtkDoubleArray vtkArray;
#else
typedef vtkFloatArray vtkArray;
#endif

// main
int main(){
	// settings
	boost::filesystem::path output_dir = "./spharm";
	const int nmax = 4;
	const arma::uword num_x = 100;
	const arma::uword num_y = 100;
	const arma::uword num_z = 100;
	const rat::fltp x1 = -0.3; const rat::fltp x2 = 0.3;
	const rat::fltp y1 = -0.3; const rat::fltp y2 = 0.3;
	const rat::fltp z1 = -0.3; const rat::fltp z2 = 0.3;

	// arrays defining coordinates
	arma::Row<rat::fltp> xa = arma::linspace<arma::Row<rat::fltp> >(x1,x2,num_x);
	arma::Row<rat::fltp> ya = arma::linspace<arma::Row<rat::fltp> >(y1,y2,num_y);
	arma::Row<rat::fltp> za = arma::linspace<arma::Row<rat::fltp> >(z1,z2,num_z);

	// allocate coordinates
	arma::Mat<rat::fltp> Rt(3,num_x*num_y*num_z);

	// walk over x
	for(arma::uword i=0;i<num_z;i++){
		for(arma::uword j=0;j<num_y;j++){
			// calculate indexes
			const arma::uword idx1 = i*num_x*num_y + j*num_x;
			const arma::uword idx2 = i*num_x*num_y + (j+1)*num_x - 1;

			// set coordinates
			Rt.submat(0,idx1,0,idx2) = xa;
			Rt.submat(1,idx1,1,idx2).fill(ya(j));
			Rt.submat(2,idx1,2,idx2).fill(za(i));
		}
	}

	// convert to spherical
	const arma::Mat<rat::fltp> Rsph = rat::fmm::Extra::cart2sph(Rt);

	// calculate spherical harmonics
	rat::fmm::Spharm sph(nmax,Rsph);



	// get strength for specified harmonic
	arma::Mat<rat::fltp> Mr = arma::real(sph.get_all()).t(); assert(Mr.is_finite());
	arma::Mat<rat::fltp> Mi = arma::imag(sph.get_all()).t(); assert(Mi.is_finite());
	assert((int)Mr.n_cols==rat::fmm::Extra::polesize(nmax));
	assert((int)Mi.n_cols==rat::fmm::Extra::polesize(nmax));

	// scale with radius
	Mr.each_col()/=(0.1+Rsph.row(0).t()); assert(Mr.is_finite());
	Mi.each_col()/=(0.1+Rsph.row(0).t()); assert(Mi.is_finite());

	// normalize
	const arma::Mat<rat::fltp> Mmax = arma::max(arma::max(arma::abs(Mi),0),arma::max(arma::abs(Mr),0));
	Mr.each_row()/=Mmax; assert(Mr.is_finite());
	Mi.each_row()/=Mmax; assert(Mi.is_finite());

	// calculate spacing
	rat::fltp dx = (x2-x1)/num_x; 
	rat::fltp dy = (y2-y1)/num_y;
	rat::fltp dz = (z2-z1)/num_z;


	// create directory	
	boost::filesystem::create_directory(output_dir);

	// wrap coordinate arrays
	vtkSmartPointer<vtkArray> xvtk = vtkArray::New();
	xvtk->SetNumberOfComponents(1); xvtk->SetArray(xa.memptr(), num_x, 1);
	vtkSmartPointer<vtkArray> yvtk = vtkArray::New();
	yvtk->SetNumberOfComponents(1); yvtk->SetArray(ya.memptr(), num_y, 1);
	vtkSmartPointer<vtkArray> zvtk = vtkArray::New();
	zvtk->SetNumberOfComponents(1); zvtk->SetArray(za.memptr(), num_z, 1);

	// setup grid
	vtkSmartPointer<vtkImageData> voxels = vtkImageData::New();
	voxels->SetDimensions(num_x,num_y,num_z);
	voxels->SetSpacing(dx,dy,dz);
	voxels->SetOrigin(x1,y1,z1);

	// walk over n
	for(int n=0;n<=nmax;n++){
		for(int m=-n;m<=n;m++){
			// wrap to VTK
			{
				vtkSmartPointer<vtkArray> real_array = vtkArray::New();
				real_array->SetNumberOfComponents(1);
				real_array->SetArray(Mr.colptr(rat::fmm::Extra::nm2fidx(n,m)), Mr.n_rows, 1);
				const std::string str = "re, n = " + std::to_string(n) + ", m = " + std::to_string(m);
				real_array->SetName(str.c_str());
				voxels->GetPointData()->AddArray(real_array);
			}

			// wrap to VTK
			{
				vtkSmartPointer<vtkArray> imag_array = vtkArray::New();
				imag_array->SetNumberOfComponents(1);
				imag_array->SetArray(Mi.colptr(rat::fmm::Extra::nm2fidx(n,m)), Mi.n_rows, 1);
				const std::string str = "im, n = " + std::to_string(n) + ", m = " + std::to_string(m);
				imag_array->SetName(str.c_str());
				voxels->GetPointData()->AddArray(imag_array);
			}
		}
	}



	// Write the file
	vtkSmartPointer<vtkXMLImageDataWriter> writer =  
		vtkSmartPointer<vtkXMLImageDataWriter>::New();
	
	writer->SetFileName((output_dir/"spharm.vti").string().c_str());
	writer->SetInputData(voxels);
	writer->Write();

}