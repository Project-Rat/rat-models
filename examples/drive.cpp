// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for models
#include "drivescurve.hh"

// main
int main(){
	// settings
	const rat::fltp time = RAT_CONST(0.0);
	const arma::uword num_points = 100;

	// create drive
	rat::mdl::ShDriveSCurvePr drive = rat::mdl::DriveSCurve::create();

	// create linspace
	const arma::Row<rat::fltp> xx = arma::linspace<arma::Row<rat::fltp> >(-1,1,num_points);
	
	// get scaling from the drive
	arma::Mat<rat::fltp> M(6,num_points);
	M.row(0) = arma::Row<rat::fltp>(num_points, arma::fill::value(time)); 
	M.row(1) = xx;
	M.row(2) = drive->get_scaling_vec(xx,time,0);
	M.row(3) = drive->get_scaling_vec(xx,time,1);
	M.row(4) = drive->get_scaling_vec(xx,time,2);
	M.row(5) = drive->get_scaling_vec(xx,time,3);

	// show matrix
	std::cout<<M.t()<<std::endl;

	return 0;
}