// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// command line parser
#include <tclap/CmdLine.h>

// common header files
#include "rat/common/extra.hh"

// header files for models
#include "pathbezier.hh"
#include "crossrectangle.hh"
#include "crosspoint.hh"
#include "pathgroup.hh"
#include "modelcoil.hh"
#include "modelgroup.hh"
#include "modelarray.hh"
#include "calcmesh.hh"
#include "calcharmonics.hh"
#include "pathaxis.hh"
#include "serializer.hh"
#include "driveinterpbcr.hh"
#include "pathcctcustom.hh"
#include "pathsuperellipse.hh"
#include "modelsphere.hh"
#include "pathstraight.hh"
#include "modelmesh.hh"
#include "version.hh"

// Description:
// canted cosine theta coil constructed out of cos-theta ends (with cone option)
// this allows for creating a hardway bend free frenet serret ribbon

// main
int main(int argc, char * argv[]){

	// COMMAND LINE INPUT
	// create tclap object
	TCLAP::CmdLine cmd("Frenet-Serret Example with Cone Approach",' ',rat::cmn::Extra::create_version_tag(
		MAT_PROJECT_VER_MAJOR,MAT_PROJECT_VER_MINOR,MAT_PROJECT_VER_PATCH));

	// arguments
	TCLAP::UnlabeledValueArg<std::string> output_path_argument(
		"od","Output dir",false,"./frenetconecct/", "string", cmd);
	TCLAP::ValueArg<rat::fltp> omega_argument(
		"","omega","coil winding pitch [m]",false,0.0,"double",cmd);
	TCLAP::ValueArg<rat::fltp> alpha_argument(
		"","alpha","coil winding angle [deg]",false,65.0,"double",cmd);
	TCLAP::ValueArg<rat::fltp> radius_argument(
		"","radius","coil radius [m]",false,0.03,"double",cmd);
	TCLAP::ValueArg<rat::fltp> ba_argument(
		"","ba","length of coil end [m]",false,0.02,"double",cmd);

	// parse the command line arguments
	cmd.parse(argc, argv);


	// INPUT SETTINGS
	// data storage directory and filename
	const boost::filesystem::path output_dir = output_path_argument.getValue();
	
	// switchboard
	const bool run_coil_field = true;
	const bool run_harmonics = true;
	const bool run_json_export = true;
	const rat::fltp output_times = 0.0;

	// settings
	const arma::uword num_poles = 3;
	const rat::fltp radius = radius_argument.getValue();
	const rat::fltp alpha = alpha_argument.getValue()*arma::Datum<rat::fltp>::tau/360.0;
	const rat::fltp element_size = 1e-3;
	const rat::fltp dcable = 1e-3;
	const rat::fltp wcable = 4e-3;
	const arma::uword num_turns = 1;
	const arma::uword num_turns_internal = 10;
	const rat::fltp operating_current = 250.0;
	const rat::fltp operating_temperature = 20.0;
	const rat::fltp omega = omega_argument.getValue();
	const rat::fltp factor = 1.0;
	const rat::fltp arclength = arma::Datum<rat::fltp>::pi/2;
	const arma::uword n1 = 4; // order of straight section
	const arma::uword n2 = 2; // order of end
	const rat::fltp ba = ba_argument.getValue();
	const rat::fltp phase = arma::Datum<rat::fltp>::pi/3;
	const rat::fltp ell_str = 0.1;
	const bool analytic = true;

	// GEOMETRY
	// number of points
	const arma::uword num_sections = 2*num_poles;
	const arma::uword num_anchor_points = num_sections+1;

	// create theta
	const arma::Row<rat::fltp> theta = arma::linspace<arma::Row<rat::fltp> >(0,arma::Datum<rat::fltp>::tau,num_anchor_points);

	// create coordinates on z=0 plane
	const arma::Row<rat::fltp> x = radius*arma::cos(theta + phase);
	const arma::Row<rat::fltp> y = radius*arma::sin(theta + phase);
	const arma::Row<rat::fltp> z = omega*theta/arma::Datum<rat::fltp>::tau;

	// create velocities on z=0 plane
	arma::Row<rat::fltp> vx = -radius*arma::sin(theta + phase)/factor;
	arma::Row<rat::fltp> vy = radius*arma::cos(theta + phase)/factor;
	arma::Row<rat::fltp> vz = radius*std::tan(alpha)*arma::cos(num_poles*theta) + omega/arma::Datum<rat::fltp>::tau; // - 40e-3*arma::cos(3*num_poles*theta);

	// when alpha is 90
	if(alpha>arma::Datum<rat::fltp>::pi/2 - 1e-9){
		for(int i=0;i<num_anchor_points;i++)vz(i) = -2*(i%2)-1;
		vz.fill(0.0); vy.fill(0.0);
	}

	// create coordinates
	const arma::Mat<rat::fltp> R = arma::join_vert(x,y,z);
	const arma::Mat<rat::fltp> V = arma::join_vert(vx,vy,vz);

	// normalize velocity to get longitudinal vector
	const arma::Mat<rat::fltp> L = V.each_row()/rat::cmn::Extra::vec_norm(V);

	// transverse vector
	arma::Mat<rat::fltp> Vax(3,L.n_cols,arma::fill::zeros); Vax.row(2).fill(RAT_CONST(1.0));
	arma::Mat<rat::fltp> D = rat::cmn::Extra::cross(L,Vax);		
	D.each_row()/=rat::cmn::Extra::vec_norm(D); // normalize

	// normal vector
	const arma::Mat<rat::fltp> N = rat::cmn::Extra::cross(L,D);

	// create a pathgroup
	const rat::mdl::ShPathGroupPr pthgrp = rat::mdl::PathGroup::create(R.col(0), L.col(0), N.col(0));
	pthgrp->set_align_frame(true); // no alignment the points should already be aligned

	const rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
	model->set_name("cone-cct");

	// create and add sections
	for(arma::uword i=0;i<num_sections;i++){
		// get indices of connected control points
		const arma::uword idx1 = i;
		const arma::uword idx2 = i+1;

		// sign
		const bool is_flipped = i%2!=0;
		const rat::fltp sgn = is_flipped ? -1.0 : 1.0;

		// get frame at start point
		const arma::Col<rat::fltp>::fixed<3> R1 = R.col(idx1);
		const arma::Col<rat::fltp>::fixed<3> L1 = L.col(idx1);
		const arma::Col<rat::fltp>::fixed<3> N1 = N.col(idx1);
		const arma::Col<rat::fltp>::fixed<3> D1 = D.col(idx1);

		// get frame at end point
		const arma::Col<rat::fltp>::fixed<3> R2 = R.col(idx2);
		const arma::Col<rat::fltp>::fixed<3> L2 = -L.col(idx2);
		const arma::Col<rat::fltp>::fixed<3> N2 = N.col(idx2);
		const arma::Col<rat::fltp>::fixed<3> D2 = D.col(idx2);

		// vector from R1 to R2
		const arma::Col<rat::fltp>::fixed<3> V12 = R2 - R1;

		// check if edges parallel then cone not needed
		if(!(arma::as_scalar(rat::cmn::Extra::dot(L1,L2))>(1.0-1e-9))){
			// calculate angle between curves the curves should intersect at the tip
			const rat::fltp gamma = std::acos(arma::as_scalar(rat::cmn::Extra::dot(L1,L2)));

			// distance between points
			const rat::fltp l12 = arma::as_scalar(rat::cmn::Extra::vec_norm(V12));

			// distance to tip
			const rat::fltp ht = l12/(2*std::tan(gamma/2));

			// triangle edge length is also edge of cone from tip to base
			const rat::fltp ledge = std::sqrt(std::pow(ht,2) + std::pow(l12/2,2));

			// intersection point
			const arma::Col<rat::fltp>::fixed<3> Ri1 = R1 + ledge*L1;
			const arma::Col<rat::fltp>::fixed<3> Ri2 = R2 + ledge*L2;

			// // check identical
			// std::cout<<Ri1<<std::endl;
			// std::cout<<Ri2<<std::endl;

			// cone vector is at intersection of LxD planes
			// even if Rbase does not lie along D1 nor D2
			arma::Col<rat::fltp>::fixed<3> cone_axis = -sgn*rat::cmn::Extra::cross(N1, N2);

			// normalize
			cone_axis.each_row()/=rat::cmn::Extra::vec_norm(cone_axis);

			// cone angle
			const rat::fltp beta1 = std::acos(arma::as_scalar(rat::cmn::Extra::dot(cone_axis,L1)));
			const rat::fltp beta2 = std::acos(arma::as_scalar(rat::cmn::Extra::dot(cone_axis,L2)));

			// check identical

			// cone radius and height
			const rat::fltp rcone = std::sin(beta1)*ledge;
			const rat::fltp hcone = std::cos(beta1)*ledge;

			// find cone base
			const arma::Col<rat::fltp>::fixed<3> Rbase = Ri1 - hcone*cone_axis;

			// vectors from base to R1 and R2
			const arma::Col<rat::fltp>::fixed<3> V1 = R1 - Rbase;
			const arma::Col<rat::fltp>::fixed<3> V2 = R2 - Rbase;

			// lengths of these vectors
			const rat::fltp lv1 = arma::as_scalar(rat::cmn::Extra::vec_norm(V1));
			const rat::fltp lv2 = arma::as_scalar(rat::cmn::Extra::vec_norm(V2));

			// angle between R1 and R2 with respect to Rbase
			const rat::fltp phi = arma::Datum<rat::fltp>::pi/2 - RAT_CONST(0.5)*std::acos(arma::as_scalar(rat::cmn::Extra::dot(V1,V2))/(lv1*lv2));

			// create super elliptical path
			const rat::mdl::ShPathSuperEllipsePr ellipse1 = 
				rat::mdl::PathSuperEllipse::create(rcone, ba, beta1, phi, arclength, element_size, 1, n1, n2);
			const rat::mdl::ShPathSuperEllipsePr ellipse2 = 
				rat::mdl::PathSuperEllipse::create(rcone, ba, beta1, phi, arclength, element_size, 1, n1, n2);

			// add the flip
			ellipse2->add_reflect_yz();
			ellipse2->add_reverse();
			ellipse2->add_flip(); 
			ellipse1->add_flip();
			
			// add the spline to a path
			rat::mdl::ShPathGroupPr end = rat::mdl::PathGroup::create();
			end->add_path(ellipse1); end->add_path(ellipse2);

			if(is_flipped){
				end->add_reverse();
				end->add_flip();
			}
			pthgrp->add_path(end);

			// reference points
			rat::mdl::ShModelSpherePr sphere1 = rat::mdl::ModelSphere::create(0.1e-3, 0.25e-3);
			sphere1->add_translation(Ri1);
			sphere1->set_name("tip");
			model->add_model(sphere1);
			rat::mdl::ShModelSpherePr sphere2 = rat::mdl::ModelSphere::create(0.1e-3, 0.25e-3);
			sphere2->add_translation(Ri2);
			sphere2->set_name("tip");
			model->add_model(sphere2);
			rat::mdl::ShModelSpherePr sphere3 = rat::mdl::ModelSphere::create(0.1e-3, 0.25e-3);
			sphere3->add_translation(R.col(idx1));
			sphere3->set_name("R1");
			model->add_model(sphere3);
			rat::mdl::ShModelSpherePr sphere4 = rat::mdl::ModelSphere::create(0.1e-3, 0.25e-3);
			sphere4->add_translation(R.col(idx2));
			sphere4->set_name("R2");
			model->add_model(sphere4);
			rat::mdl::ShModelSpherePr sphere5 = rat::mdl::ModelSphere::create(0.1e-3, 0.25e-3);
			sphere5->add_translation(Rbase);
			sphere5->set_name("base");
			model->add_model(sphere5);

			// path
			rat::mdl::ShPathGroupPr grp1 = rat::mdl::PathGroup::create(R1,L1,D1);
			grp1->add_path(rat::mdl::PathStraight::create(ledge,1e-3));
			model->add_model(rat::mdl::ModelMesh::create(grp1, rat::mdl::CrossPoint::create()));
			rat::mdl::ShPathGroupPr grp2 = rat::mdl::PathGroup::create(R2,L2,D2);
			grp2->add_path(rat::mdl::PathStraight::create(ledge,1e-3));
			model->add_model(rat::mdl::ModelMesh::create(grp2, rat::mdl::CrossPoint::create()));
		}

		// cylinder case
		else{
			// angle between R1 and R2 with respect to Rbase
			const rat::fltp phi = arma::Datum<rat::fltp>::pi/2 - RAT_CONST(0.5)*
				std::acos(arma::as_scalar(rat::cmn::Extra::dot(R1,R2))/
				(arma::as_scalar(rat::cmn::Extra::vec_norm(R1))*
				arma::as_scalar(rat::cmn::Extra::vec_norm(R2))));

			// create super elliptical path
			const rat::mdl::ShPathSuperEllipsePr ellipse1 = 
				rat::mdl::PathSuperEllipse::create(radius, ba, 0.0, phi, arclength, element_size, 1, n1, n2);
			const rat::mdl::ShPathSuperEllipsePr ellipse2 = 
				rat::mdl::PathSuperEllipse::create(radius, ba, 0.0, phi, arclength, element_size, 1, n1, n2);

			// add the flip
			ellipse2->add_reflect_yz();
			ellipse2->add_reverse();
			ellipse2->add_flip(); 
			ellipse1->add_flip();
			
			// add the spline to a path
			rat::mdl::ShPathGroupPr end = rat::mdl::PathGroup::create();
			end->add_path(ellipse1); end->add_path(ellipse2);

			if(is_flipped){
				end->add_reverse();
				end->add_flip();
			}
			pthgrp->add_path(end);

			// optional extra straight section
			pthgrp->add_path(rat::mdl::PathStraight::create(ell_str,element_size));
		}
		
	}

	// create a coil
	const rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(
		-dcable/2,dcable/2,-wcable/2,wcable/2,element_size);
	// const rat::mdl::ShCrossPointPr cross_rect = rat::mdl::CrossPoint::create();

	// create inner and outer coils
	const rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(pthgrp, cross_rect);
	coil->set_name("ribbon-cct"); 
	coil->set_number_turns(num_turns_internal);
	coil->set_operating_current(operating_current);
	coil->set_operating_temperature(operating_temperature);

	// turns
	const rat::mdl::ShModelArrayPr array = rat::mdl::ModelArray::create(coil, 1,1,num_turns, 0,0,omega, true);

	// create model that combines coils
	model->add_model(array);



	// CALCULATION AND OUTPUT
	// axis of magnet for field quality calculation
	const rat::mdl::ShPathAxisPr pth_axis = rat::mdl::PathAxis::create('z','y',10*radius,0,0,6e-3,element_size);

	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({output_times},output_dir,lg);
	}

	// calculate harmonics
	if(run_harmonics){
		// settings
		const rat::fltp reference_radius = RAT_CONST((2.0/3)*(radius - wcable/2));
		const bool compensate_curvature = false;

		// create calculation
		rat::mdl::ShCalcHarmonicsPr harmonics_calculator = rat::mdl::CalcHarmonics::create(
			model, pth_axis, reference_radius, compensate_curvature);
		harmonics_calculator->set_num_max(20);

		// calculate harmonics and get data
		harmonics_calculator->calculate_write({RAT_CONST(0.0)},output_dir,lg);
	}

	// export json
	if(run_json_export){
		// write to output file
		rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model);
		slzr->export_json(output_dir/"model.json");
	}

}