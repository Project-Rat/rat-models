// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// command line parser
#include <tclap/CmdLine.h>

// common header files
#include "rat/common/extra.hh"

// header files for models
#include "pathbezier.hh"
#include "crossrectangle.hh"
#include "crosspoint.hh"
#include "pathgroup.hh"
#include "modelcoil.hh"
#include "modelgroup.hh"
#include "modelarray.hh"
#include "calcmesh.hh"
#include "calcharmonics.hh"
#include "pathaxis.hh"
#include "serializer.hh"
#include "driveinterpbcr.hh"
#include "pathcctcustom.hh"
#include "pathsuperellipse.hh"
#include "version.hh"

// Description:
// canted cosine theta coil constructed out of bezier splines
// this allows for creating a hardway bend free frenet serret ribbon

// main
int main(int argc, char * argv[]){

	// COMMAND LINE INPUT
	// create tclap object
	TCLAP::CmdLine cmd("Frenet-Serret Example with Bezier Approach",' ',rat::cmn::Extra::create_version_tag(
		MAT_PROJECT_VER_MAJOR,MAT_PROJECT_VER_MINOR,MAT_PROJECT_VER_PATCH));

	// filename input argument
	TCLAP::UnlabeledValueArg<std::string> output_path_argument(
		"od","Output dir",false,"./frenetbeziercct/", "string", cmd);

	// parse the command line arguments
	cmd.parse(argc, argv);


	// SETTINGS
	// data storage directory and filename
	const boost::filesystem::path output_dir = output_path_argument.getValue();
	
	// switchboard
	const bool run_coil_field = true;
	const bool run_harmonics = true;
	const bool run_json_export = true;
	const rat::fltp output_times = 0.0;

	// settings
	const arma::uword num_poles = 3;
	const rat::fltp radius = 0.03;
	const rat::fltp alpha = 65.0*arma::Datum<rat::fltp>::tau/360.0;
	const rat::fltp element_size = 1e-3;
	const rat::fltp dcable = 1e-3;
	const rat::fltp wcable = 4e-3;
	const arma::uword num_turns = 10;
	const arma::uword num_turns_internal = 10;
	const rat::fltp operating_current = 250.0;
	const rat::fltp operating_temperature = 20.0;
	const rat::fltp omega = 4e-3;
	const bool analytic = true;
	const rat::fltp factor = 1.0;

	// transition length
	const rat::fltp ell_trans = 0.005;

	// GEOMETRY
	// number of points
	const arma::uword num_sections = 2*num_poles;
	const arma::uword num_anchor_points = num_sections+1;

	// create theta
	const arma::Row<rat::fltp> theta = arma::linspace<arma::Row<rat::fltp> >(0,arma::Datum<rat::fltp>::tau,num_anchor_points);

	// create coordinates on z=0 plane
	const arma::Row<rat::fltp> x = radius*arma::cos(theta);
	const arma::Row<rat::fltp> y = radius*arma::sin(theta);
	const arma::Row<rat::fltp> z = omega*theta/arma::Datum<rat::fltp>::tau;

	// create velocities on z=0 plane
	const arma::Row<rat::fltp> vx = -radius*arma::sin(theta)/factor;
	const arma::Row<rat::fltp> vy = radius*arma::cos(theta)/factor;
	const arma::Row<rat::fltp> vz = radius*arma::cos(num_poles*theta)/std::tan(arma::Datum<rat::fltp>::pi/2 - alpha) + omega/arma::Datum<rat::fltp>::tau; // - 40e-3*arma::cos(3*num_poles*theta);

	// create coordinates
	const arma::Mat<rat::fltp> R = arma::join_vert(x,y,z);
	const arma::Mat<rat::fltp> V = arma::join_vert(vx,vy,vz);

	// normalize velocity to get longitudinal vector
	const arma::Mat<rat::fltp> L = V.each_row()/rat::cmn::Extra::vec_norm(V);

	// transverse vector
	arma::Mat<rat::fltp> Vax(3,L.n_cols,arma::fill::zeros); Vax.row(2).fill(RAT_CONST(1.0));
	arma::Mat<rat::fltp> D = rat::cmn::Extra::cross(L,Vax);		
	D.each_row()/=rat::cmn::Extra::vec_norm(D); // normalize

	// normal vector
	const arma::Mat<rat::fltp> N = rat::cmn::Extra::cross(L,D);

	// strengths for bezier curves
	const rat::fltp str12 = radius*0.4718/num_poles;
	const rat::fltp str34 = radius*0.042890/num_poles;

	// strengths
	const rat::fltp str1 = str12; const rat::fltp str2 = str12;
	const rat::fltp str3 = str34; const rat::fltp str4 = str34;

	// bezier control points in UV plane
	const arma::Mat<rat::fltp> Puv1 = arma::reshape(arma::Col<rat::fltp>{
		0.0,0.0, 1*str1,0, 2*str1,0, 3*str1,0, 4*str1,1*str3, 
		5*str1,2*str3, 6*str1,4*str3, 6*str1,10*str3},2,8);
	const arma::Mat<rat::fltp> Puv2 = arma::reshape(arma::Col<rat::fltp>{
		0.0,0.0, 1*str2,0, 2*str2,0, 3*str2,0, 4*str2,1*str4, 
		5*str2,2*str4, 6*str2,4*str4, 6*str2,10*str4},2,8);

	// create a pathgroup
	const rat::mdl::ShPathGroupPr pthgrp = rat::mdl::PathGroup::create();
	pthgrp->set_align_frame(false); // no alignment the points should already be aligned

	// create and add sections
	for(arma::uword i=0;i<num_sections;i++){
		// get indices of connected control points
		const arma::uword idx1 = i;
		const arma::uword idx2 = i+1;

		// sign
		const bool is_flipped = i%2!=0;
		const rat::fltp sgn = is_flipped ? -1.0 : 1.0;

		// create a bezier spline (note changed in 2.015.1)
		const rat::mdl::ShPathBezierPr bezier = rat::mdl::PathBezier::create(
			R.col(idx1), L.col(idx1), sgn*N.col(idx1),
			R.col(idx2), L.col(idx2), sgn*N.col(idx2),
			Puv1,Puv2, ell_trans, ell_trans, element_size);

		// add settings
		bezier->set_analytic_frame(analytic);
		if(is_flipped)bezier->add_flip();

		// add the spline to a path
		pthgrp->add_path(bezier);
	}

	// create a coil
	const rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(
		-dcable/2,dcable/2,-wcable/2,wcable/2,element_size);
	// const rat::mdl::ShCrossPointPr cross_rect = rat::mdl::CrossPoint::create();

	// create inner and outer coils
	const rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(pthgrp, cross_rect);
	coil->set_name("ribbon-cct"); 
	coil->set_number_turns(num_turns_internal);
	coil->set_operating_current(operating_current);
	coil->set_operating_temperature(operating_temperature);

	// turns
	const rat::mdl::ShModelArrayPr array = rat::mdl::ModelArray::create(coil, 1,1,num_turns, 0,0,omega, true);

	// create model that combines coils
	const rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create(array);


	// CALCULATION AND OUTPUT
	// axis of magnet for field quality calculation
	const rat::mdl::ShPathAxisPr pth_axis = rat::mdl::PathAxis::create('z','y',10*radius,0,0,0,element_size);

	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({output_times},output_dir,lg);
	}

	// calculate harmonics
	if(run_harmonics){
		// settings
		const rat::fltp reference_radius = RAT_CONST((2.0/3)*(radius - wcable/2));
		const bool compensate_curvature = false;

		// create calculation
		rat::mdl::ShCalcHarmonicsPr harmonics_calculator = rat::mdl::CalcHarmonics::create(
			model, pth_axis, reference_radius, compensate_curvature);
		harmonics_calculator->set_num_max(20);

		// calculate harmonics and get data
		harmonics_calculator->calculate_write({RAT_CONST(0.0)},output_dir,lg);
	}

	// export json
	if(run_json_export){
		// write to output file
		rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model);
		slzr->export_json(output_dir/"model.json");
	}

	// {
	// 	// test
	// 	rat::mdl::ShFramePr frame = pthgrp->create_frame(rat::mdl::MeshSettings());
	// 	frame->combine();
	// 	const arma::Mat<rat::fltp> Rf = frame->get_coords(0);
	// 	arma::Row<rat::fltp> thetaf = arma::atan2(Rf.row(1),Rf.row(0)); thetaf.cols(arma::find(Rf.row(1)<0)) += arma::Datum<rat::fltp>::tau; thetaf -= arma::Datum<rat::fltp>::pi;
	// 	const arma::Row<rat::fltp> rhof = arma::sqrt(Rf.row(1)%Rf.row(1) + Rf.row(0)%Rf.row(0));

	// 	// build radius drive
	// 	const rat::mdl::ShDriveInterpBCRPr rho_drive = rat::mdl::DriveInterpBCR::create(thetaf.t()/arma::Datum<rat::fltp>::tau,rhof.t(),4);

	// 	// custom cct 
	// 	const arma::uword nnpt = 180; const bool is_skew = false; const bool use_skew_angle = true;
	// 	const rat::mdl::ShPathCCTCustomPr path_cct = rat::mdl::PathCCTCustom::create();
	// 	path_cct->add_harmonic(rat::mdl::CCTHarmonicDrive::create(num_poles,is_skew,rat::mdl::DriveDC::create(alpha),use_skew_angle));
	// 	path_cct->set_range(-arma::sword(num_turns)/2,num_turns/2);
	// 	path_cct->set_num_nodes_per_turn(nnpt);
	// 	path_cct->set_normalize_length(false);
	// 	path_cct->set_omega(rat::mdl::DriveDC::create(omega));
	// 	path_cct->set_rho(rho_drive);
	// 	path_cct->set_use_local_radius(false);
	// 	path_cct->set_range(-0.5,0.5);
		
	// 	const arma::uword num_layers = 1;
	// 	const rat::fltp rho_increment = 6e-3;
	// 	path_cct->set_num_layers(num_layers);
	// 	path_cct->set_rho_increment(rho_increment);
	// 	path_cct->set_same_alpha(false);
	// 	path_cct->set_use_frenet_serret(true);

	// 	// create inner and outer coils
	// 	const rat::mdl::ShModelCoilPr coil2 = rat::mdl::ModelCoil::create(path_cct, cross_rect);
	// 	coil2->set_name("ribbon-cct"); 
	// 	coil2->set_number_turns(num_turns_internal);
	// 	coil2->set_operating_current(operating_current);
	// 	coil2->set_operating_temperature(operating_temperature);

	// 	// create model that combines coils
	// 	const rat::mdl::ShModelGroupPr model2 = rat::mdl::ModelGroup::create(coil2);

	// 	// write to output file
	// 	rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model2);
	// 	slzr->export_json(output_dir/"model2.json");
	// }


	// // draw the shape directly
	// {
	// 	// settings
	// 	// const rat::fltp str = radius/12;

	// 	// create theta
	// 	// const arma::Row<rat::fltp> theta = arma::linspace<arma::Row<rat::fltp> >(0,arma::Datum<rat::fltp>::tau,num_anchor_points);

	// 	// // create coordinates on z=0 plane
	// 	// const arma::Row<rat::fltp> x = radius*arma::cos(theta);
	// 	// const arma::Row<rat::fltp> y = radius*arma::sin(theta);
	// 	// const arma::Row<rat::fltp> z = omega*theta/arma::Datum<rat::fltp>::tau;

	// 	// // create velocities on z=0 plane
	// 	// const arma::Row<rat::fltp> vx = -radius*arma::sin(theta);
	// 	// const arma::Row<rat::fltp> vy = radius*arma::cos(theta);
	// 	// const arma::Row<rat::fltp> vz = radius*arma::cos(num_poles*theta)/std::tan(arma::Datum<rat::fltp>::pi/2 - alpha) + omega/arma::Datum<rat::fltp>::tau;

	// 	// // normalized velocity
	// 	// const arma::Mat<rat::fltp> R = arma::join_vert(x,y,z);
	// 	// const arma::Mat<rat::fltp> V = arma::join_vert(vx,vy,vz);

	// 	// // normalize
	// 	// const arma::Mat<rat::fltp> L = V.each_row()/rat::cmn::Extra::vec_norm(V);

	// 	// // normal vector
	// 	// arma::Mat<rat::fltp> Vax(3,num_anchor_points,arma::fill::zeros);
	// 	// Vax.row(2).fill(RAT_CONST(1.0));
	// 	// const arma::Mat<rat::fltp> N = rat::cmn::Extra::cross(L,Vax);

	// 	// allocate
	// 	arma::field<arma::Mat<rat::fltp> > Rspl(1,num_sections);

	// 	// control points
	// 	for(arma::uword i=0;i<num_sections;i++){
	// 		// get indices
	// 		const arma::uword idx1 = i; const arma::uword idx2 = i+1;

	// 		// sign
	// 		const bool is_flipped = i%2!=0;
	// 		const rat::fltp sgn = is_flipped ? -1.0 : 1.0;

	// 		// add points the u-direction is longitudinal and the v-direction is normal
	// 		// for start points
	// 		arma::Mat<rat::fltp> P1(3,Puv1.n_cols);
	// 		for(arma::uword j=0;j<3;j++)P1.row(j) = R(j,idx1) + L(j,idx1)*Puv1.row(0) + sgn*N(j,idx1)*Puv1.row(1);

	// 		// for end points
	// 		arma::Mat<rat::fltp> P2(3,Puv2.n_cols);
	// 		for(arma::uword j=0;j<3;j++)P2.row(j) = R(j,idx2) - L(j,idx2)*Puv2.row(0) + sgn*N(j,idx2)*Puv2.row(1);

	// 		// combine points
	// 		const arma::Mat<rat::fltp> P = arma::join_horiz(P1,arma::fliplr(P2));

	// 		// create time array
	// 		// const arma::Row<rat::fltp> t = arma::linspace<arma::Row<rat::fltp> >(0,1.0,100);
	// 		const arma::Row<rat::fltp> t = rat::mdl::PathBezier::regular_times(P,element_size,0.0,100);

	// 		// spline it
	// 		const arma::field<arma::Mat<rat::fltp> > C = rat::mdl::PathBezier::create_bezier_tn(t, P, 3);

	// 		// store
	// 		Rspl(i) = C(0);

	// 		// remove last
	// 		if(i!=num_sections-1)Rspl(i) = Rspl(i).head_cols(Rspl(i).n_cols-1);
	// 	}

	// 	// combine
	// 	const arma::Mat<rat::fltp> Rf = rat::cmn::Extra::field2mat(Rspl);

	// 	// create interpolation
	// 	arma::Row<rat::fltp> thetaf = arma::atan2(Rf.row(1),Rf.row(0)); thetaf.cols(arma::find(Rf.row(1)<0)) += arma::Datum<rat::fltp>::tau; thetaf -= arma::Datum<rat::fltp>::pi;
	// 	const arma::Row<rat::fltp> rhof = arma::sqrt(Rf.row(1)%Rf.row(1) + Rf.row(0)%Rf.row(0));

	// 	// build radius drive
	// 	const rat::mdl::ShDriveInterpBCRPr rho_drive = rat::mdl::DriveInterpBCR::create(thetaf.t()/arma::Datum<rat::fltp>::tau,rhof.t(),4);

	// 	// custom cct 
	// 	const arma::uword nnpt = 180; const bool is_skew = false; const bool use_skew_angle = true;
	// 	const rat::mdl::ShPathCCTCustomPr path_cct = rat::mdl::PathCCTCustom::create();
	// 	path_cct->add_harmonic(rat::mdl::CCTHarmonicDrive::create(num_poles,is_skew,rat::mdl::DriveDC::create(alpha),use_skew_angle));
	// 	path_cct->set_range(-arma::sword(num_turns)/2,num_turns/2);
	// 	path_cct->set_num_nodes_per_turn(nnpt);
	// 	path_cct->set_normalize_length(false);
	// 	path_cct->set_omega(rat::mdl::DriveDC::create(omega));
	// 	path_cct->set_rho(rho_drive);
	// 	path_cct->set_use_local_radius(false);
	// 	path_cct->set_range(-0.5,0.5);

	// 	const arma::uword num_layers = 1;
	// 	const rat::fltp rho_increment = 6e-3;
	// 	path_cct->set_num_layers(num_layers);
	// 	path_cct->set_rho_increment(rho_increment);
	// 	path_cct->set_same_alpha(false);
	// 	path_cct->set_use_frenet_serret(true);

	// 	// create inner and outer coils
	// 	const rat::mdl::ShModelCoilPr coil2 = rat::mdl::ModelCoil::create(path_cct, cross_rect);
	// 	coil2->set_name("ribbon-cct"); 
	// 	coil2->set_number_turns(num_turns_internal);
	// 	coil2->set_operating_current(operating_current);
	// 	coil2->set_operating_temperature(operating_temperature);

	// 	// create model that combines coils
	// 	const rat::mdl::ShModelGroupPr model2 = rat::mdl::ModelGroup::create(coil2);

	// 	// write to output file
	// 	rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model2);
	// 	slzr->export_json(output_dir/"model3.json");


	// 	// higher order (for study)
	// 	const arma::Row<rat::fltp> az = -radius*num_poles*arma::sin(num_poles*thetaf)/std::tan(arma::Datum<rat::fltp>::pi/2 - alpha); 
	// 	const arma::Row<rat::fltp> jz = -radius*num_poles*num_poles*arma::cos(num_poles*thetaf)/std::tan(arma::Datum<rat::fltp>::pi/2 - alpha); 
	// 	const arma::Row<rat::fltp> drho = rho_drive->Drive::get_scaling(thetaf/arma::Datum<rat::fltp>::tau,1);
	// 	const arma::Row<rat::fltp> ddrho = rho_drive->Drive::get_scaling(thetaf/arma::Datum<rat::fltp>::tau,2);
	// 	const arma::Row<rat::fltp> dddrho = rho_drive->Drive::get_scaling(thetaf/arma::Datum<rat::fltp>::tau,3);


	// 	std::cout<<arma::join_vert(thetaf,arma::join_vert(az,jz,rhof,drho),arma::join_vert(ddrho, dddrho)).t()<<std::endl;

	// 	// std::cout<<rho_drive->get_scaling(0,0)<<std::endl;
	// 	// std::cout<<rho_drive->get_scaling(0,1)<<std::endl;;
	// 	// std::cout<<rho_drive->get_scaling(0,2)<<std::endl;;
	// 	// std::cout<<rho_drive->get_scaling(0,3)<<std::endl;;

	// }

	// // provided theta and z find rho such that 
	// // 1. the cable stands edge up at z = 0, i.e. D == R
	// // 2. the coil end is a valid Frenet-Serret frame

	// // // superellipse russenschuck 627
	// // a = R*(arma::Datum<fltp>::pi/2 - phi);
	// // x = R*sin((a/R)*arma::pow(arma::cos(t),2.0/n));
	// // y = R*cos((a/R)*arma::pow(arma::cos(t),2.0/n));
	// // z = ba*arma::pow(arma::sin(t),2/n);

	// // straight + circle might work (but the cylinder is skewed). 

	





}