// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// DESCRIPTION
// Minimalist example of a solenoid model

// header files for common
#include "rat/common/log.hh"

// header files for model
#include "pathcircle.hh"
#include "crossrectangle.hh"
#include "modelbar.hh"
#include "meshdata.hh"
#include "pathaxis.hh"
#include "cartesiangriddata.hh"
#include "calcmesh.hh"
#include "calcgrid.hh"
#include "calcplane.hh"

// main function
int main(){
	// switchboard
	const bool run_grid = false;
	const bool run_coil_field = false;
	const bool run_plane = true;

	// output files
	const std::string output_dir = "./bar/";

	// output time
	const rat::fltp output_time = RAT_CONST(0.0);

	// model geometric input parameters
	const rat::fltp dbar = 10e-3; // thickness of the bar [m]
	const rat::fltp ell = 20e-3; // length of the bar [m]
	const rat::fltp delem = 2e-3; // element size [m]

	// model operating parameters
	const rat::fltp M = 1.0/arma::Datum<rat::fltp>::mu_0;;


	// GEOMETRY
	// create a circular path object
	rat::mdl::ShPathAxisPr axis = rat::mdl::PathAxis::create('z','x',ell,rat::cmn::Extra::null_vec(),delem);

	// create a rectangular cross section object
	rat::mdl::ShCrossRectanglePr rectangle = rat::mdl::CrossRectangle::create(-dbar/2, dbar/2, -dbar/2, dbar/2, delem);

	// create a coil object
	rat::mdl::ShModelBarPr bar = rat::mdl::ModelBar::create(axis, rectangle);
	bar->set_magnetisation(arma::Col<rat::fltp>::fixed<3>{M,0,0});


	// CALCULATION AND OUTPUT
	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(bar);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({output_time},output_dir,lg);
	}

	// create grid calculation
	if(run_grid){
		// create grid calculation
		const rat::mdl::ShCalcGridPr grid = rat::mdl::CalcGrid::create(bar,0.1,0.1,0.2,100,100,200);

		// calculate and write vtk output files to specified directory
		grid->calculate_write({output_time},output_dir,lg);
	}

	// create grid calculation
	if(run_plane){
		// create grid calculation
		const rat::mdl::ShCalcPlanePr plane = rat::mdl::CalcPlane::create(bar,'y',0.1,0.1,800,800);

		// calculate and write vtk output files to specified directory
		plane->calculate_write({output_time},output_dir,lg);
	}

}
