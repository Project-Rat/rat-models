// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header for common
#include "rat/common/log.hh"

// materials headers
#include "rat/mat/database.hh"
#include "rat/mat/rutherfordcable.hh"

// header files for Models
#include "pathcctcustom.hh"
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "modelgroup.hh"
#include "transbend.hh"
#include "cartesiangriddata.hh"
#include "serializer.hh"
#include "calcmesh.hh"
#include "calcharmonics.hh"
#include "calcgrid.hh"
#include "drivelinear.hh"
#include "modelroot.hh"
#include "calcgroup.hh"

// DESCRIPTION
// Example of Custom CCT with varying radius along its length

// main
int main(){
	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = true;
	const bool run_harmonics = true;
	const bool run_json_export = true;

	// data storage directory and filename
	const boost::filesystem::path output_dir = "./tapered_cct/"; // (relative) output directory
	const rat::fltp output_time = RAT_CONST(0.0); // [s] unimportant as we are not using time dependence here

	// geometry settings
	const rat::fltp bending_radius = RAT_CONST(0.15); // [m] bending radius of the magnet
	const rat::fltp nt = RAT_CONST(40.0); // number of turns
	const rat::fltp radius = RAT_CONST(0.025); // inner coil radius [m]
	const arma::uword nnpt = 180; // number of nodes per turn

	// angle of the dipole component
	const rat::fltp dipole_angle = RAT_CONST(60.0)*2*arma::Datum<rat::fltp>::pi/360; // angle of dipole component [rad]

	// operating conditions
	const rat::fltp operating_current = RAT_CONST(480.0); // operating current [A]
	const rat::fltp operating_temperature = RAT_CONST(4.5); // operating temperature [K]

	// conductor settings
	const arma::uword nd = 2; // number of wires across cable thickness
	const arma::uword nw = 5; // number of wires across cable width
	const arma::uword num_strands = nd*nw;	
	const rat::fltp dstr = RAT_CONST(0.825e-3); // strand diameter [m]
	const rat::fltp ddstr = RAT_CONST(1.0e-3); // strand diameter incl insulation [m]
	const rat::fltp fcu2sc = 1.9; // LHC wire

	// cable dimensions
	const rat::fltp dcable = nd*ddstr; // thickness of cable [m] (slot width)
	const rat::fltp wcable = nw*ddstr; // width of cable [m] (slot depth)
	const rat::fltp element_size = ddstr; // size of the elements [m]
	
	// thickness between cables on the inside of the inner layer on 
	// the inside of the bend (this is where the rib is thinnest)
	const rat::fltp drib = 0.5e-3; // half a mm is just machineable

	// use multiple layers
	const arma::uword num_layers = 2llu; // should be even for cancelling solenoid component
	const rat::fltp dformer = 4e-3; // [m] thickness of tube between the outside of the inner coil and inside of the outer coil
	
	// winding pitch to ensure spacing between the wires
	const rat::fltp omega = (dcable + drib)/std::cos(dipole_angle); // winding pitch [m]

	// spacing between two consecutive layers
	const rat::fltp rho_increment = wcable + dformer; // [m]


	// GEOMETRY SETUP
	// create custom CCT path
	const rat::mdl::ShPathCCTCustomPr path_cct = rat::mdl::PathCCTCustom::create();
	
	// you can set both the starting turn and end turn
	path_cct->set_range(-nt/2,nt/2);

	// number of elements per turn determins the element size
	path_cct->set_num_nodes_per_turn(nnpt);

	// when normalize length is set to true the interval [-nt/2,nt/2] is
	// mapped to the interval [-0.5, 0.5]. This is then used by the 
	// drives (functions) for rho and omega to calculate the local
	// pitch and radius respectively. For example, the slope in the 
	// linear drive is the delta radius over the full length of the coil.
	// If the normalize length is disabled, the interval is mapped to 
	// [-nt,nt]. The slope is then the change of radius per turn.
	path_cct->set_normalize_length(true);

	// const pitch (we are using the pitch in the axial direction)
	// there is a possibility use use it in the cable normal direction
	// this would be necessary for having a combined function magnet
	// but the consequence would be that the length of the coil is no
	// longer defined as nt*omega
	path_cct->set_omega(rat::mdl::DriveDC::create(omega));

	// linear increasing radius
	const rat::fltp offset = radius; const rat::fltp slope = 2*radius;
	path_cct->set_rho(rat::mdl::DriveLinear::create(offset, slope));

	// const dipole along the length of the coil
	// note that more than one harmonic can be added 
	// in this way to create a combined function magnet
	arma::uword num_poles = 1; // 1 dipole, 2 quadrupole, 3 hexapole, etc.
	bool is_skew = false; // we want the main harmonic B1 not the skew one A1
	bool use_skew_angle = true; // interprets input as angle instead of amplitude
	path_cct->add_harmonic(rat::mdl::CCTHarmonicDrive::create(
		num_poles,is_skew,rat::mdl::DriveDC::create(dipole_angle),use_skew_angle));

	// bend the CCT path to a radius
	// this radius is defined to the center of the 
	// aperture. The bending is performed using the
	// neutral axis on the inside of the outer
	// layer. This way the turns are only stretched 
	// and not compressed.
	path_cct->set_use_radius(true); // use bending radius input instead of angle
	path_cct->set_bending_radius(bending_radius);

	// use multiple layers
	path_cct->set_num_layers(num_layers);
	path_cct->set_rho_increment(rho_increment);

	// calculate the radius as function of z instead of theta
	path_cct->set_use_local_radius(true);

	// use the same angle for the next layer
	// I personally prefer to keep the amplitude 
	// constant as it makes the coil end length 
	// the same for all layers.
	path_cct->set_same_alpha(false);

	// axis of magnet for field quality calculation
	const rat::mdl::ShPathAxisPr pth_axis = rat::mdl::PathAxis::create('z','y',0.5,{0.0,0.0,0.0}, 2e-3);

	// we use the bending transformation here to 
	// create a bend path along the aperture
	// this path can be used for setting up the 
	// targets for the harmonics calculation
	pth_axis->add_transformation(rat::mdl::TransBend::create(rat::cmn::Extra::unit_vec('y'),rat::cmn::Extra::unit_vec('x'),bending_radius));

	// create cross section this cross section
	// will get extruded along the path. Make sure
	// that the first two coordinates. I.e. the 
	// normal direction is symmetric around zero
	// otherwise you would introduce higher order harmonics
	// for the transverse direction it doens't matter what
	// you set
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(
		-dcable/2,dcable/2,RAT_CONST(0.0),wcable,element_size/2);


	// For the cable of the coil we can use the RutherfordCable wrapper
	// by setting a pitch of zero the material knows that all the wires
	// are running in parallel
	rat::mat::ShRutherfordCablePr nbti_cable = rat::mat::RutherfordCable::create();
	nbti_cable->set_strand(rat::mat::Database::nbti_wire_lhc(dstr,fcu2sc));
	nbti_cable->set_num_strands(nd*nw);
	nbti_cable->set_width(wcable);
	nbti_cable->set_thickness(dcable); 
	nbti_cable->set_keystone(0); // keystoning angle
	nbti_cable->set_pitch(0); // cabling pitch
	nbti_cable->set_dinsu(0); // added insulation thickness
	nbti_cable->set_fcabling(1.0); // degradation due to cabling
	nbti_cable->setup();

	// create coil using the path and the cross section
	rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(path_cct, cross_rect, nbti_cable);	
	coil->set_name("Custom CCT Coil"); // name is only really used in GUI

	// this is specific to glyn's coil where the wires in the slot are powered in series. Use 
	coil->set_number_turns(num_strands); 
	coil->set_operating_current(operating_current);

	// operating temperature is used for material properties only
	coil->set_operating_temperature(operating_temperature);

	// cluster the coil in a model
	// in case you want to add more than one coil later on
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
	model->set_name("model tree");
	model->add_model(coil);

	// model extended for storing the calculations
	// this can be used directly in the rat-gui
	rat::mdl::ShModelRootPr root = rat::mdl::ModelRoot::create();
	root->set_name("Conical CCT");
	root->set_model_tree(model);
	rat::mdl::ShCalcGroupPr cgrp = rat::mdl::CalcGroup::create();
	cgrp->set_name("calculation tree");
	root->set_calc_tree(cgrp);


	// CALCULATION AND OUTPUT
	// the default log displays the status in the
	// terminal window in which the code is called
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// mesh calculatoin
	if(run_coil_field){
		// create calculation
		rat::mdl::ShCalcMeshPr mesh_calculator = rat::mdl::CalcMesh::create(model);
		cgrp->add_calculation(mesh_calculator);

		// calculate harmonics and get data
		mesh_calculator->calculate_write({output_time},output_dir,lg);
	}

	// calculate harmonics
	if(run_harmonics){
		// reference radius is usually 2/3s of the aperture
		const rat::fltp reference_radius = 2*radius/3;

		// the harmonics can be compensated for the curvature of the coil
		// I'm more or less convinced that in a curved magnet to make the 
		// beam traverse a bend magnet without distortion, it is necessary
		// to enable this and reoptimize the coil harmonics.
		const bool compensate_curvature = true;

		// create calculation
		rat::mdl::ShCalcHarmonicsPr harmonics_calculator = rat::mdl::CalcHarmonics::create(
			model, pth_axis, reference_radius, compensate_curvature);
		cgrp->add_calculation(harmonics_calculator);

		// calculate harmonics and write data to vtk file
		// you can also get the data calling calculate
		harmonics_calculator->calculate_write({output_time},output_dir,lg);
	}

	// export json
	if(run_json_export){
		// create serializer
		rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(root);

		// write to output file
		slzr->export_json(output_dir/"curved_cct_model.json");
	}

	// done
	return 0;
}