// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

#include "pathcct.hh"
#include "pathoffset.hh"
#include "modeledges.hh"
#include "pathsub.hh"
#include "modelgroup.hh"
#include "calcmesh.hh"
#include "serializer.hh"
#include "crossrectangle.hh"
#include "modelcoil.hh"

// main
int main(){

	// settings
	const arma::uword num_poles = 1;
	const rat::fltp radius = 0.04; 
	const rat::fltp a = 0.05; 
	const rat::fltp omega = 5e-3;
	const rat::fltp nt1 = -2.0; 
	const rat::fltp nt2 = 2.0; 
	const arma::uword num_nodes_per_turn = 180;
	const rat::fltp output_time = 0.0;
	const boost::filesystem::path output_dir = "./edges2coil";
	const bool is_reverse = true;

	// create a cct path
	const rat::mdl::ShPathCCTPr cct_path = rat::mdl::PathCCT::create(num_poles, radius, a, omega, nt1, nt2, num_nodes_per_turn);
	const arma::uword num_sect_per_turn = cct_path->get_num_sect_per_turn();
	cct_path->set_is_reverse(is_reverse);

	// create edges from cct path
	const rat::mdl::ShPathOffsetPr e1 = rat::mdl::PathOffset::create(cct_path, -1e-3, 5e-3);
	const rat::mdl::ShPathOffsetPr e2 = rat::mdl::PathOffset::create(cct_path, 1e-3, 5e-3);
	const rat::mdl::ShPathOffsetPr e3 = rat::mdl::PathOffset::create(cct_path, 1e-3, 0.0);
	const rat::mdl::ShPathOffsetPr e4 = rat::mdl::PathOffset::create(cct_path, -1e-3, 0.0);

	// create edges from cct path
	const rat::mdl::ShPathSubPr ie1 = rat::mdl::PathSub::create(e2,num_sect_per_turn,(nt2-nt1)*num_sect_per_turn-1);
	const rat::mdl::ShPathSubPr ie2 = rat::mdl::PathSub::create(e1,0,(nt2-nt1-1)*num_sect_per_turn-1);
	const rat::mdl::ShPathSubPr ie3 = rat::mdl::PathSub::create(e4,0,(nt2-nt1-1)*num_sect_per_turn-1);
	const rat::mdl::ShPathSubPr ie4 = rat::mdl::PathSub::create(e3,num_sect_per_turn,(nt2-nt1)*num_sect_per_turn-1);

	// create rectangle
	const rat::mdl::ShCrossRectanglePr crss_rectangle = rat::mdl::CrossRectangle::create(-1e-3,1e-3,0,5e-3,1e-3);

	// create coil
	const rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(cct_path, crss_rectangle);
	coil->set_name("coil");
	coil->set_operating_current(400.0);
	coil->set_num_turns(10.0);
	coil->set_color({0.0,0.3,0.7});
	coil->set_use_custom_color();

	// create ribs
	const rat::mdl::ShModelEdgesPr rib = rat::mdl::ModelEdges::create(ie1,ie2,ie3,ie4,2,5);
	rib->set_name("rib");
	rib->set_color({1.0,0.0,0.0});
	rib->set_use_custom_color();

	// combine
	const rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create({coil, rib});

	// check model
	model->is_valid(true);

	// create log
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// create calculation
	const rat::mdl::ShCalcMeshPr calc = rat::mdl::CalcMesh::create(model);

	// write output
	calc->calculate_write({output_time},output_dir,lg);

	// export json
	rat::cmn::Serializer::create(model)->export_json(output_dir/"model.json");
}