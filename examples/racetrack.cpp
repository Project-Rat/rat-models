// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// DESCRIPTION
// Minimalist example of a solenoid model

// header files for common
#include "rat/common/log.hh"

// multipole method headers
#include "rat/mlfmm/mlfmm.hh"
#include "rat/mlfmm/multisources.hh"
#include "rat/mlfmm/multitargets2.hh"

// header files for model
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "meshdata.hh"
#include "patharc.hh"
#include "pathstraight.hh"
#include "pathgroup.hh"
#include "pathcable.hh"
#include "calcmesh.hh"

// main function
int main(){
	// SETTINGS
	// switch board
	const bool run_coil_field = true;

	// data storage directory and filename
	const boost::filesystem::path output_dir = "./racetrack/";

	// output time
	const rat::fltp output_time = RAT_CONST(0.0);

	// model geometric input parameters
	const rat::fltp radius = RAT_CONST(40e-3); // coil inner radius [m]
	const rat::fltp delem = RAT_CONST(2e-3); // element size [m]
	const rat::fltp ell = RAT_CONST(100e-3);
	const rat::fltp wcable = RAT_CONST(12e-3); // width of the cable [m]
	const rat::fltp dcable = RAT_CONST(1.2e-3);
	const rat::fltp dinsu = RAT_CONST(0.1e-3);
	
	// model operating parameters
	const rat::fltp operating_current = RAT_CONST(200.0); // operating current in [A]
	const rat::fltp num_turns = RAT_CONST(10.0); // number of turns

	// winding parameters
	const arma::uword idx_incr = 1; // location at which the turn is incremented to the next
	const arma::uword idx_start = 2; // section in which the cable starts 
	const arma::sword num_add = 2; // number of extra sections to add/remove at end


	// MODEL
	// create an arc
	rat::mdl::ShPathArcPr path_arc = rat::mdl::PathArc::create(
		radius,arma::Datum<rat::fltp>::pi/2,delem,(dcable+2*dinsu)*num_turns);

	// create straight section
	rat::mdl::ShPathStraightPr path_straight = rat::mdl::PathStraight::create(ell/2, delem);

	// add sections to a pathgroup
	rat::mdl::ShPathGroupPr racetrack = rat::mdl::PathGroup::create();
	racetrack->add_path(path_straight);	racetrack->add_path(path_arc);	
	racetrack->add_path(path_arc); racetrack->add_path(path_straight);
	racetrack->add_path(path_straight);	racetrack->add_path(path_arc);	
	racetrack->add_path(path_arc); racetrack->add_path(path_straight);
	racetrack->add_translation(radius,0,0);

	// create a rectangular cross section object
	rat::mdl::ShCrossRectanglePr rectangle = 
		rat::mdl::CrossRectangle::create(0, dcable, 0, wcable, delem);

	// create cable
	rat::mdl::ShPathCablePr path_cable = rat::mdl::PathCable::create(racetrack);
	path_cable->set_turn_step(dcable + 2*dinsu);
	path_cable->set_num_turns(num_turns);
	path_cable->set_idx_incr(idx_incr);
	path_cable->set_idx_start(idx_start);
	path_cable->set_num_add(num_add);
	path_cable->set_offset(dinsu);

	// create a coil object
	rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(path_cable, rectangle);
	coil->set_operating_current(operating_current);
	coil->set_num_gauss(1);


	// CALCULATION AND OUTPUT
	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(coil);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({output_time},output_dir,lg);
	}

}