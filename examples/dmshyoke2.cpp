// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for common
#include "rat/common/gmshfile.hh"

// header files for distmesh-cpp
#include "rat/dmsh/dfrrectangle.hh"
#include "rat/dmsh/dfdiff.hh"
#include "rat/dmsh/dfrectangle.hh"

// header files for models
#include "crossdmsh.hh"
#include "pathaxis.hh"
#include "calcmesh.hh"
#include "modelcoil.hh"
#include "modelnl.hh"
#include "serializer.hh"
#include "modelgroup.hh"
#include "pathclover.hh"
#include "crossrectangle.hh"
#include "modelmirror.hh"
#include "hbcurvevlv.hh"
#include "drivelinear.hh"

// main
int main(){
	// INPUT SETTINGS
	// switch board
	const bool run_calc_mesh = true;
	const bool run_json_export = true;
	
	// output directory
	const boost::filesystem::path output_dir = "./dmshyoke2/";

	// operating conditions
	const rat::fltp Iop = 2000; // operating current [A]
	const rat::fltp Top = 20; // operating temperature [K]

	// coil parameters
	const rat::fltp ellstr1 = 0.140; // straight section length along aperture [m]
	const rat::fltp ellstr2 = 0.060; // straight section length accross aperture [m]
	const rat::fltp bridge_angle = arma::Datum<rat::fltp>::pi*2*7.0/360;

	// winding parameters
	const arma::uword idx_incr = 1; // location at which the turn is incremented to the next
	const arma::uword idx_start = 2; // section in which the cable starts 
	const arma::sword num_add = 2; // number of extra sections to add/remove at end

	// cable settings
	const rat::fltp wcable = 12e-3; // width of the cable (or coil pack) [m]
	const rat::fltp dcable = 0.2e-3; // thickness of the cable [m]
	const rat::fltp dinsu = 0e-3/2; // thickness of insulation layer around each cable [m]
	const arma::uword num_turns = 60; // number of turns
	const rat::fltp coil_element_size = 1.5e-3; // size of the used elements [m]

	// the cloverleaf coil-end spline parameters
	const rat::fltp ell_trans = 0; // transition length [m]
	const rat::fltp height = 20e-3; // height of the bridge [m]
	const rat::fltp str12 = 0.37*35e-3;	// strength for determining control point positions [m]
	const rat::fltp str34 = 0.37*14e-3; // strength for determining control point positions [m]

	// calculate remaining parameters
	const rat::fltp dpack = num_turns*(dcable + 2*dinsu); // thickness of the coil pack

	const rat::fltp dformer = 0; //12e-3;
	const rat::fltp dring = 0;

	// settings
	const rat::fltp yoke_element_size = 0.01; // requested size of the elements [m]
	const rat::fltp ell = 0.1; // length of the yoke [m]


	// YOKE GEOMETRY SETUP
	// setup the distance function
	const rat::dm::ShDistFunPr df = rat::dm::DFDiff::create(
		rat::dm::DFRRectangle::create(-0.1,0.1,-0.07,0.07,0.02),
		rat::dm::DFRRectangle::create(-0.05,0.05,-0.03,0.03,0.02));

	// create cross section with distance function
	const rat::mdl::ShCrossDMshPr cdmsh = rat::mdl::CrossDMsh::create(yoke_element_size,df,rat::dm::DFOnes::create());

	// create path
	//rat::mdl::ShPathStraightPr pth = rat::mdl::PathStraight::create(ell,element_size);
	const rat::mdl::ShPathAxisPr pth = rat::mdl::PathAxis::create('y','z',ell,0,0,0,yoke_element_size);

	// with non-linear solver
	#ifdef ENABLE_NL_SOLVER
		// create hb curve
		const rat::mdl::ShHBCurvePr hb_curve = rat::mdl::HBCurveVLV::create(500.0,1.5);

		// create yoke as non-linear mesh
		const rat::mdl::ShModelNLPr yoke = rat::mdl::ModelNL::create(pth,cdmsh,hb_curve);

	// no non-linear solver present
	#else
		// create yoke as regular mesh
		const rat::mdl::ShModelMeshPr yoke = rat::mdl::ModelMesh::create(pth,cdmsh);
	#endif

	// COIL GEOMETRY SETUP'
	// create clover path
	const rat::mdl::ShPathCloverPr path_clover = rat::mdl::PathClover::create(
		ellstr1+2*dpack,ellstr2+2*dpack,height,0,ell_trans,str12,str34,coil_element_size);
	path_clover->add_flip();
	path_clover->set_bridge_angle(bridge_angle);

	// create block cross section
	const rat::mdl::ShCrossRectanglePr cross_coil = rat::mdl::CrossRectangle::create(
		dring,dring+dpack,-wcable/2,wcable/2,coil_element_size);

	// create block coil
	rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(path_clover, cross_coil); 
	coil->set_name("clover");
	coil->set_number_turns(num_turns);
	coil->add_translation(0,0,8e-3);
	coil->set_operating_current(Iop);
	coil->set_operating_temperature(Top);
	coil->set_circuit_index(1);
	coil->add_flip();

	// create mirror
	const rat::mdl::ShModelMirrorPr mirror = rat::mdl::ModelMirror::create(coil,true);


	// create a model to combine the yoke and the coil
	const rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create({yoke,mirror});

	// output times
	const arma::Row<rat::fltp> tt = arma::linspace<arma::Row<rat::fltp> >(0,2200.0,10);

	// create a cache for solve storage
	const rat::mdl::ShSolverCachePr cache = rat::mdl::SolverCache::create();

	// create a circuit
	const rat::mdl::ShCircuitPr circuit = rat::mdl::Circuit::create(
		1,rat::mdl::DriveLinear::create(RAT_CONST(0.0),RAT_CONST(1.0)));

	// CALCULATION
	// mesh calculation
	if(run_calc_mesh){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model);
		mesh->add_circuit(circuit);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write(tt,output_dir,rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT),cache);
	}

	// export json
	if(run_json_export){
		rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model);
		slzr->export_json(output_dir/"model.json");
	}
}