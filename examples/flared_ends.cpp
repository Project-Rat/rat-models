// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for common
#include "rat/common/log.hh"

// header files for models
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "pathcable.hh"
#include "transbend.hh"
#include "modelgroup.hh"
#include "pathflared.hh"
#include "calcmesh.hh"
#include "calcinductance.hh"

// DESCRIPTION
// Flared ends are a common solution for allowing access
// to the aperture of a block coil. This is an example
// showing how to create a coil with flared ends.

// main
int main(){
	// INPUT SETTINGS
	// calculation
	const bool run_coil_field = true; // calculate field on coil
	const bool run_inductance = true; // calculate inductance matrix

	// data storage directory and filename
	const boost::filesystem::path output_dir = "./flared/";
	
	// output time
	const rat::fltp output_time = RAT_CONST(0.0);

	// cable settings
	const rat::fltp wcable = RAT_CONST(12e-3); // width of the cable [m]
	const rat::fltp dcable = RAT_CONST(1.2e-3); // thickness of the cable [m]
	const rat::fltp dinsu = RAT_CONST(0.1e-3); // thickness of the insulation layer around each cable [m]
	
	// coil geometry settings
	const rat::fltp ell1 = RAT_CONST(0.1); // length of straight section [m]
	const rat::fltp ell2 = RAT_CONST(0.15); // length of sloped section [m]
	const rat::fltp arcl = arma::Datum<rat::fltp>::pi/12; // angle of end [rad]
	const rat::fltp radius1 = RAT_CONST(0.2); // hardway bending radius [m]
	const rat::fltp radius2 = RAT_CONST(0.05); // softway bending radius [m]
	const rat::fltp element_size = RAT_CONST(2e-3); // size of the elements [m]
	const arma::uword num_turns = 12; // number of turns 

	// operating conditions
	const rat::fltp Top = RAT_CONST(4.5);
	const rat::fltp Iop = RAT_CONST(4000.0);


	// GEOMETRY SETUP
	// create flared end path
	const rat::mdl::ShPathFlaredPr pathflared = rat::mdl::PathFlared::create(
		ell1,ell2,-arcl,radius1,radius2,element_size,num_turns*dcable);

	// create cable
	rat::mdl::ShPathCablePr path_cable = rat::mdl::PathCable::create(pathflared);
	path_cable->set_turn_step(dcable + 2*dinsu);
	path_cable->set_num_turns(num_turns);
	path_cable->set_idx_incr(1);
	path_cable->set_idx_start(0);
	
	// create cross section
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(
		0,dcable,-wcable/2,wcable/2,1.2e-3);

	// create coil
	rat::mdl::ShModelCoilPr coil1 = rat::mdl::ModelCoil::create(path_cable, cross_rect);
	coil1->add_translation(0,0,2e-2);
	coil1->set_operating_current(Iop);
	coil1->set_operating_temperature(Top);

	// second pole
	rat::mdl::ShModelCoilPr coil2 = rat::mdl::ModelCoil::create(path_cable, cross_rect);
	coil2->add_translation(0,0,2e-3);
	//coil2->add_rotation(1,0,0,arma::Datum<rat::fltp>::pi);
	coil2->add_reflect_xy();
	coil2->add_flip();
	//coil2->add_reverse();
	coil2->set_operating_current(Iop);
	coil2->set_operating_temperature(Top);

	// model
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
	model->add_model(coil1);
	model->add_model(coil2);


	// CALCULATION AND OUTPUT
	// create log
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({output_time},output_dir,lg);
	}

	// inductance calculation
	if(run_inductance){
		// create inductance calculation
		const rat::mdl::ShCalcInductancePr inductance_calculator = rat::mdl::CalcInductance::create(model);

		// calculate and show matrix in terminal
		inductance_calculator->calculate_write({output_time},output_dir,lg);
	}
	
}