// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for Rat-Common
#include "rat/common/log.hh"
#include "rat/common/gmshfile.hh"

// materials headers
#include "rat/mat/database.hh"

// header files for Rat-Models
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "modelgroup.hh"
#include "pathcable.hh"
#include "modeltoroid.hh"
#include "pathrectangle.hh"
#include "pathdshape.hh"
#include "serializer.hh"
#include "calcinductance.hh"
#include "calcmesh.hh"
#include "calcgrid.hh"
#include "calcpolargrid.hh"

// DESCRIPTION
// Example geometry setup for a toroid with 
// 6 rat::fltp pancake D-shaped limbs.

// main
int main(){
	// INPUT SETTINGS
	// calculation 
	const bool run_coil_field = true;
	const bool run_grid = true;
	const bool run_polar_grid = false;
	const bool run_json_export = false; // export geometry to json file
	const bool run_inductance = true; 

	// data storage directory and filename
	const boost::filesystem::path output_dir = "./toroid/";

	// output time
	const rat::fltp output_time = RAT_CONST(0.0);

	// coil geometry
	const rat::fltp width = RAT_CONST(0.25);
	const rat::fltp height = RAT_CONST(0.4);
	const rat::fltp element_size = RAT_CONST(4e-3);

	// cable settings
	const rat::fltp wcable = RAT_CONST(12e-3);
	const rat::fltp dcable = RAT_CONST(1.2e-3);
	const rat::fltp dinsu = RAT_CONST(0.1e-3);
	const arma::uword num_turns = 12;
	const bool use_current_sharing = true;

	// cable settings
	const arma::uword idx_incr = 1;
	const arma::uword idx_start = 0;

	// operating conditions
	const rat::fltp operating_current = RAT_CONST(8000.0);
	const rat::fltp operating_temperature = RAT_CONST(20.0);


	// GEOMETRY SETUP
	// construct conductor
	rat::mat::ShConductorPr rebco_cu = rat::mat::Database::rebco_fujikura_tape_cern();

	// create unified path
	//rat::mdl::ShPathRectanglePr path_rect = rat::mdl::PathRectangle::create(width,height,radius,element_size);
	rat::mdl::ShPathDShapePr path_rect = rat::mdl::PathDShape::create(height,width,element_size);

	// create cable
	rat::mdl::ShPathCablePr path_cable1 = rat::mdl::PathCable::create(path_rect);
	path_cable1->set_turn_step(dcable + 2*dinsu);
	path_cable1->set_num_turns(num_turns);
	path_cable1->set_idx_incr(idx_incr);
	path_cable1->set_idx_start(idx_start);

	// create cable
	rat::mdl::ShPathCablePr path_cable2 = rat::mdl::PathCable::create(path_rect);
	path_cable2->set_turn_step(dcable + 2*dinsu);
	path_cable2->set_num_turns(num_turns);
	path_cable2->set_idx_incr(idx_incr);
	path_cable2->set_idx_start(idx_start);
	path_cable2->set_reverse_base(true);

	// create cross section
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(0,dcable,-wcable/2,wcable/2,1.2e-3);

	// create coil
	rat::mdl::ShModelCoilPr coil1 = rat::mdl::ModelCoil::create(path_cable1, cross_rect, rebco_cu);
	coil1->add_translation(0,0,2e-2);
	coil1->set_enable_current_sharing(use_current_sharing);
	coil1->set_name("c1");
	coil1->set_operating_current(operating_current);
	coil1->set_operating_temperature(operating_temperature);	

	// create coil
	rat::mdl::ShModelCoilPr coil2 = rat::mdl::ModelCoil::create(path_cable2, cross_rect, rebco_cu);
	coil2->add_translation(0,0,-2e-2);
	coil2->set_enable_current_sharing(use_current_sharing);
	coil2->set_name("c2");
	coil2->set_operating_current(operating_current);
	coil2->set_operating_temperature(operating_temperature);	
	
	// create model
	rat::mdl::ShModelGroupPr coilset = rat::mdl::ModelGroup::create();
	coilset->add_model(coil1);
	coilset->add_model(coil2);
	coilset->set_name("lm");

	// make toroid
	rat::mdl::ShModelToroidPr toroid = rat::mdl::ModelToroid::create(coilset);
	toroid->set_num_coils(6);
	toroid->set_radius(0.1);
	toroid->set_name("tf");

	// add toroid to a model
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
	model->add_model(toroid);


	// CALCULATION AND OUTPUT
	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({output_time},output_dir,lg);
	}

	// create grid calculation
	if(run_grid){
		// settings
		const rat::fltp grid_size = RAT_CONST(0.8);
		const arma::uword grid_num_steps = 100;

		// create grid calculation
		const rat::mdl::ShCalcGridPr grid = rat::mdl::CalcGrid::create(
			model,grid_size,grid_size,grid_size,grid_num_steps,grid_num_steps,grid_num_steps);

		// calculate and write vtk output files to specified directory
		grid->calculate_write({output_time},output_dir,lg);
	}

	// create grid calculation
	if(run_polar_grid){
		// create grid calculation
		const rat::fltp rin = 0.0, rout = 0.4, theta1 = 0, theta2 = arma::Datum<rat::fltp>::tau, zlow=-0.25,zhigh=0.25;
		const arma::uword num_rad = 40llu, num_theta = 90llu, num_axial = 50llu;
		const rat::mdl::ShCalcPolarGridPr grid = rat::mdl::CalcPolarGrid::create(model,'z',rin,rout,num_rad,theta1,theta2,num_theta,zlow,zhigh,num_axial);

		// calculate and write vtk output files to specified directory
		grid->calculate_write({output_time},output_dir,lg);
	}

	// inductance calculation
	if(run_inductance){
		// create inductance calculation
		const rat::mdl::ShCalcInductancePr inductance_calculator = rat::mdl::CalcInductance::create(model);

		// calculate and show matrix in terminal
		inductance_calculator->calculate_write({output_time},output_dir,lg);
	}

	// export json
	if(run_json_export){
		// write to output file
		const rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model);
		slzr->export_json(output_dir/"model.json");
	}
}