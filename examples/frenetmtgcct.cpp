// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// command line parser
#include <tclap/CmdLine.h>

// common header files
#include "rat/common/extra.hh"

// header files for models
#include "pathbezier.hh"
#include "crossrectangle.hh"
#include "crosspoint.hh"
#include "pathgroup.hh"
#include "modelcoil.hh"
#include "modelgroup.hh"
#include "modelarray.hh"
#include "calcmesh.hh"
#include "calcharmonics.hh"
#include "pathaxis.hh"
#include "serializer.hh"
#include "pathcctcustom.hh"
#include "pathsuperellipse.hh"
#include "version.hh"
#include "drivercct.hh"
#include "driveinterpbcr.hh"

// Description:
// canted cosine theta coil constructed out of bezier splines
// this allows for creating a hardway bend free frenet serret ribbon

// main
int main(int argc, char * argv[]){

	// COMMAND LINE INPUT
	// create tclap object
	TCLAP::CmdLine cmd("Frenet-Serret Example with Bezier Approach",' ',rat::cmn::Extra::create_version_tag(
		MAT_PROJECT_VER_MAJOR,MAT_PROJECT_VER_MINOR,MAT_PROJECT_VER_PATCH));

	// filename input argument
	TCLAP::UnlabeledValueArg<std::string> output_path_argument(
		"od","Output dir",false,"./frenetmtgcct/", "string", cmd);
	TCLAP::ValueArg<rat::fltp> alpha_argument(
		"","alpha","coil winding angle [deg]",false,65.0,"double",cmd);
	TCLAP::ValueArg<unsigned int> num_poles_argument(
		"n","num_poles","number of poles [#]",false,3,"unsigned int",cmd);

	// parse the command line arguments
	cmd.parse(argc, argv);


	// SETTINGS
	// data storage directory and filename
	const boost::filesystem::path output_dir = output_path_argument.getValue();
	
	// switchboard
	const bool run_coil_field = true;
	const bool run_harmonics = true;
	const bool run_json_export = true;
	const rat::fltp output_times = 0.0;

	// settings
	const arma::uword num_poles = num_poles_argument.getValue();
	const rat::fltp radius = 0.03;
	const rat::fltp alpha = alpha_argument.getValue()*arma::Datum<rat::fltp>::tau/360.0;
	const rat::fltp element_size = 1e-3;
	const rat::fltp dcable = 1e-3;
	const rat::fltp wcable = 4e-3;
	const rat::fltp num_turns = 1.0;
	const arma::uword num_turns_internal = 1;
	const rat::fltp operating_current = 250.0;
	const rat::fltp operating_temperature = 20.0;
	const rat::fltp omega = 0.0;
	const arma::uword nnpt = 360; 


	// GEOMETRY
	// build radius drive
	const rat::mdl::ShDriveRCCTPr rho_drive = 
		rat::mdl::DriveRCCT::create(num_poles, radius, alpha);

	// custom cct 
	const rat::mdl::ShPathCCTCustomPr path_cct = rat::mdl::PathCCTCustom::create();
	path_cct->set_range(-num_turns/2,num_turns/2,-0.5,0.5);
	path_cct->set_num_nodes_per_turn(nnpt);
	path_cct->set_normalize_length(true);
	path_cct->set_omega(rat::mdl::DriveDC::create(omega));
	path_cct->set_rho(rho_drive);
	path_cct->set_use_local_radius(false);
	const arma::uword num_layers = 1;
	const rat::fltp rho_increment = 6e-3;
	path_cct->set_num_layers(num_layers);
	path_cct->set_rho_increment(rho_increment);
	path_cct->set_same_alpha(false);
	path_cct->set_use_frenet_serret(true);

	// // create z vs theta interpolation
	// const arma::uword num_interp = 361;
	// const arma::Row<rat::fltp> theta = arma::linspace<arma::Row<rat::fltp> >(0,arma::Datum<rat::fltp>::tau,num_interp);
	// const arma::Row<rat::fltp> rho = rho_drive->get_scaling_vec(arma::Row<rat::fltp>(num_interp,arma::fill::zeros),theta/arma::Datum<rat::fltp>::tau);
	// const arma::Row<rat::fltp> J = arma::pow(rho/radius,num_poles)%arma::cos(2*num_poles*theta);


	// std::cout<<arma::join_vert(theta,rho,J).t()<<std::endl;




	// harmonics
	// path_cct->add_harmonic(rat::mdl::CCTHarmonicInterp::create());


	// create a coil
	const rat::mdl::ShCrossPointPr cross_rect = rat::mdl::CrossPoint::create();

	// create inner and outer coils
	const rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(path_cct, cross_rect);
	coil->set_name("ribbon-cct"); 
	coil->set_number_turns(num_turns_internal);
	coil->set_operating_current(operating_current);
	coil->set_operating_temperature(operating_temperature);

	// create model that combines coils
	const rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create(coil);


	// CALCULATION AND OUTPUT
	// axis of magnet for field quality calculation
	const rat::mdl::ShPathAxisPr pth_axis = rat::mdl::PathAxis::create('z','y',10*radius,0,0,0,element_size);

	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({output_times},output_dir,lg);
	}

	// calculate harmonics
	if(run_harmonics){
		// settings
		const rat::fltp reference_radius = RAT_CONST((2.0/3)*(radius - wcable/2));
		const bool compensate_curvature = false;

		// create calculation
		rat::mdl::ShCalcHarmonicsPr harmonics_calculator = rat::mdl::CalcHarmonics::create(
			model, pth_axis, reference_radius, compensate_curvature);
		harmonics_calculator->set_num_max(20);

		// calculate harmonics and get data
		harmonics_calculator->calculate_write({RAT_CONST(0.0)},output_dir,lg);
	}

	// export json
	if(run_json_export){
		// write to output file
		rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model);
		slzr->export_json(output_dir/"model.json");
	}



}