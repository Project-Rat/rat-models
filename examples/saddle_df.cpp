#include <iostream>
#include <armadillo>
#include <cmath>

// header files for common
#include "rat/common/extra.hh"

// header files for distmesh-cpp
#include "rat/dmsh/dfrrectangle.hh"
#include "rat/dmsh/dfdiff.hh"
#include "rat/dmsh/dfrectangle.hh"

// header files for models
#include "pathgroup.hh"
#include "crosscircle.hh"
#include "pathstraight.hh"
#include "patharc.hh"
#include "modelcoil.hh"
#include "calcmesh.hh"
#include "transbend.hh"
#include "crossrectangle.hh"
#include "modeltoroid.hh"
#include "calcplane.hh"
#include "calcharmonics.hh"
#include "pathaxis.hh"
#include "modelroot.hh"
#include "calcgroup.hh"
#include "modelnl.hh"
#include "hbcurvevlv.hh"
#include "crossdmsh.hh"

// this makes a saddle coil based ona a density function

int main() {
	// calculation settings
	const boost::filesystem::path output_dir = "./saddle/"; // output directory
	const rat::fltp output_time = 0; // output time [s]
	const bool enable_yoke = true;
	const bool run_mesh = false;
	const bool run_plane = false;
	const bool run_harmonic = true;

	// coil geometry
	const rat::fltp aperture_radius = 0.075; // for harmonics
	const rat::fltp ell_coil = 0.4; // less than 350 length
	const rat::fltp element_size = 2e-3;
	const rat::fltp end_radius = 0.025;
	const rat::fltp operating_temperature = 4.5;
		
	// limits on windings
	const rat::fltp straight_section_min = 0.035;
	const rat::fltp end_radius_min = 0.025;

	// cable dimensions
	const arma::Row<arma::uword> num_wires = {42,70};
	const arma::Row<rat::fltp> cable_width = {10.26e-3,17.1e-3};
	const arma::Row<rat::fltp> cable_thickness = {5.88e-3,5.88e-3};

	// end spacing
	const arma::Row<rat::fltp> delta_end = cable_thickness + 1e-3;
	const arma::Row<rat::fltp> min_delta_turn = cable_thickness + 1.0e-3;
	const arma::uword num_turns_max = 1000;

	// pdf settings
	const arma::uword num_pdf = 1000llu;

	// create a group to store the layers
	const rat::mdl::ShModelGroupPr model_group = rat::mdl::ModelGroup::create();

	// harmonics table
	const rat::fltp dformer = 4e-3;
	const arma::Row<rat::fltp> radius = {
		aperture_radius+dformer,
		aperture_radius+2*dformer+cable_width(0)};
	const arma::Row<arma::uword> n = {2,1};
	const arma::Mat<rat::fltp> M = arma::join_vert(
		arma::Row<rat::fltp>{1.0, -0.85, 0.0, 0.0},
		arma::Row<rat::fltp>{0.095, -0.065, 0.0, 0.0});
	const arma::Row<arma::uword> circuit = {1,0};
	const arma::Row<rat::fltp> operating_current = {400.0,400.0}; // 400 A max
	const arma::Row<arma::uword>enable{1,1};
	
	// yoke settings
	const rat::fltp yoke_inner_radius = aperture_radius+arma::accu(cable_width)+cable_width.n_elem*dformer + 20e-3;
	const rat::fltp yoke_outer_radius = yoke_inner_radius + 0.08;
	const rat::fltp iron_element_size = (yoke_outer_radius - yoke_inner_radius)/3;

	// need valid conductor

	// walk over layers
	for(arma::uword j=0;j<n.n_elem;j++){
		// check if enabled
		if(!enable(j))continue;

		// calculate the radius of this layer
		const rat::fltp layer_radius = radius(j);

		// bounds
		const rat::fltp lower_bound = RAT_CONST(0.0);
		const rat::fltp higher_bound = arma::Datum<rat::fltp>::pi/(2*n(j));

		// create theta
		const arma::Row<rat::fltp> theta = arma::linspace<arma::Row<rat::fltp> >(lower_bound, higher_bound, num_pdf);

		// probability density function (PDF)
		// const arma::Row<rat::fltp> pdf = 1.0 + 0.45*arma::cos((2*n(j))*theta) + 0.55*arma::cos(2*(n(j)+2)*theta);
		
		arma::Row<rat::fltp> pdf(theta.n_elem,arma::fill::ones);
		for(arma::uword k=0;k<M.n_cols;k++)
			pdf += M(j,k)*arma::cos(2*(n(j)+2*k)*theta);

		// normalize
		const arma::Row<rat::fltp> pdf_normalized = pdf/arma::as_scalar(arma::trapz(theta,pdf,1));

		// cumulative distribution function (CDF)
		const arma::Row<rat::fltp> cdf = rat::cmn::Extra::cumtrapz(theta,pdf_normalized,1);

		// regular array of points
		arma::uword num_turns=2;
		arma::Col<rat::fltp> theta_turn;
		bool stop_on_next = false;
		for(;num_turns<num_turns_max;){
			// create time array
			const arma::Row<rat::fltp> t = arma::linspace<arma::Row<rat::fltp> >(1.0/num_turns/2, 1.0 - 1.0/num_turns/2, num_turns);
			arma::interp1(cdf.t(),theta.t(),t.t(),theta_turn);
			if(stop_on_next)break;
			if(arma::min(arma::diff(theta_turn*layer_radius))<=min_delta_turn(j)){
				num_turns--; stop_on_next = true;
			}else{
				num_turns++;
			}
		}

		// create flat coil
		const rat::mdl::ShPathGroupPr path_group = rat::mdl::PathGroup::create();
		path_group->set_start_coord({layer_radius*(arma::Datum<rat::fltp>::pi/2 - theta_turn.front()),-ell_coil/2 + end_radius,0});
		if(j%2==1){
			path_group->add_rotation({0,1,0},arma::Datum<rat::fltp>::pi);
			path_group->add_reverse();
		}

		// create turns
		for(arma::uword i=0;i<num_turns;i++){

			// parameters for this turn
			const rat::fltp x1 = layer_radius*(higher_bound - theta_turn(i));
			const rat::fltp turn_end_radius = std::min(end_radius,x1);
			const rat::fltp ell_straight = ell_coil/2 - turn_end_radius - (i*delta_end(j));
			if(ell_straight<straight_section_min || turn_end_radius<end_radius_min)break;
			path_group->add_path(rat::mdl::PathStraight::create(ell_straight,element_size));
			path_group->add_path(rat::mdl::PathStraight::create(ell_straight,element_size));
			path_group->add_path(rat::mdl::PathArc::create(turn_end_radius, arma::Datum<rat::fltp>::pi/2, element_size));
			if(x1-turn_end_radius>0){
				path_group->add_path(rat::mdl::PathStraight::create(x1-turn_end_radius,element_size));
				path_group->add_path(rat::mdl::PathStraight::create(x1-turn_end_radius,element_size));
			}
			path_group->add_path(rat::mdl::PathArc::create(turn_end_radius, arma::Datum<rat::fltp>::pi/2, element_size));
			path_group->add_path(rat::mdl::PathStraight::create(ell_straight,element_size));
			path_group->add_path(rat::mdl::PathStraight::create(ell_straight-delta_end(j),element_size));
			path_group->add_path(rat::mdl::PathArc::create(turn_end_radius, arma::Datum<rat::fltp>::pi/2, element_size));
			if(x1-turn_end_radius>0){
				path_group->add_path(rat::mdl::PathStraight::create(x1-turn_end_radius,element_size));
			}
			if(i!=num_turns-1){
				const rat::fltp x2 = layer_radius*(arma::Datum<rat::fltp>::pi/(2*n(j)) - theta_turn(i+1));
				const rat::fltp turn_end_radius = std::min(end_radius,x2);
				if(x2-turn_end_radius>0){
					path_group->add_path(rat::mdl::PathStraight::create(x2-turn_end_radius,element_size));
				}
				path_group->add_path(rat::mdl::PathArc::create(turn_end_radius, arma::Datum<rat::fltp>::pi/2, element_size));
			}	
		}

		path_group->add_transformation(rat::mdl::TransBend::create(
			rat::cmn::Extra::unit_vec('y'),rat::cmn::Extra::unit_vec('z'),layer_radius,0));
		path_group->add_translation({0,0,layer_radius});

		const rat::mdl::ShCrossRectanglePr cross_rectangle = rat::mdl::CrossRectangle::create(
			-cable_thickness(j)/2,cable_thickness(j)/2,0,cable_width(j),cable_thickness(j)/2);

		// create inner and outer coils
		rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(path_group, cross_rectangle);
		coil->set_name("coil"); 
		coil->set_operating_current(operating_current(circuit(j)));
		coil->set_operating_temperature(operating_temperature);
		coil->set_number_turns(num_wires(j));
		coil->set_circuit_index(circuit(j)+1);

		const rat::mdl::ShModelToroidPr array = rat::mdl::ModelToroid::create(coil, 2*n(j));
		// const rat::mdl::ShModelToroidPr array = rat::mdl::ModelToroid::create(coil, 1);
		array->set_alternate();

		// add the coil
		model_group->add_model(array);
	}

	// yoke
	#ifdef ENABLE_NL_SOLVER
	if(enable_yoke){
		// setup the distance function
		const rat::dm::ShDistFunPr df = rat::dm::DFDiff::create(
			rat::dm::DFCircle::create(yoke_outer_radius,0,0),
			rat::dm::DFCircle::create(yoke_inner_radius,0,0));

		// create cross section with distance function
		const rat::mdl::ShCrossDMshPr cdmsh = rat::mdl::CrossDMsh::create(iron_element_size,df,rat::dm::DFOnes::create());

		// create path
		const rat::mdl::ShPathAxisPr pth = rat::mdl::PathAxis::create('y','z',ell_coil,0,0,0,iron_element_size);

		// create hb curve
		const rat::mdl::ShHBCurvePr hb_curve = rat::mdl::HBCurveVLV::create(500.0,2.15);

		// create modelcoil
		const rat::mdl::ShModelNLPr yoke = rat::mdl::ModelNL::create(pth,cdmsh,hb_curve);
		yoke->add_rotation({1,0,0},arma::Datum<rat::fltp>::pi/2);

		// add the yoke
		model_group->add_model(yoke);
	}
	#endif


	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create calculation
	const rat::mdl::ShCalcMeshPr calc_mesh = rat::mdl::CalcMesh::create(model_group);

	// calculate everything
	
	// create calculation
	const rat::mdl::ShCalcPlanePr calc_plane = rat::mdl::CalcPlane::create(model_group,'z',0.5,0.5,400,400);

	// harmonics calculation
	const rat::mdl::ShCalcHarmonicsPr calc_harm = rat::mdl::CalcHarmonics::create(model_group, rat::mdl::PathAxis::create('z','y',1.0,{0,0,0},2e-3), (2.0/3)*aperture_radius);

	// create a cache for solve storage
	const rat::mdl::ShSolverCachePr cache = rat::mdl::SolverCache::create();


	
	// write to output file
	const rat::mdl::ShModelGroupPr tree = rat::mdl::ModelGroup::create(model_group);
	tree->set_name("Model Tree"); tree->set_tree_open();
	const rat::mdl::ShCalcGroupPr calc = rat::mdl::CalcGroup::create();
	calc->set_name("Calculation Tree"); calc->set_tree_open();
	calc->add_calculation(calc_mesh);
	calc->add_calculation(calc_plane);
	calc->add_calculation(calc_harm);
	const rat::mdl::ShModelRootPr root = rat::mdl::ModelRoot::create(tree,calc);
	root->set_name("Saddle Coil");

	rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create();
	slzr->flatten_tree(root); slzr->export_json(output_dir/"model.json");
		
	// run calculations
	if(run_mesh)calc_mesh->calculate_write({output_time},output_dir,lg,cache);
	if(run_plane)calc_plane->calculate_write({output_time},output_dir,lg,cache);
	if(run_harmonic)calc_harm->calculate_harmonics(output_time,lg,cache)->export_vtk_table()->write(output_dir/"harmonics.csv",lg);


    return 0;
}

