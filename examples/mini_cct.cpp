// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files from Common
#include "rat/common/log.hh"
#include "rat/common/opera.hh"
#include "rat/common/freecad.hh"
#include "rat/common/gmshfile.hh"

// include materials database
#include "rat/mat/database.hh"
#include "rat/mat/rutherfordcable.hh"

// header files for Models
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "pathcct.hh"
#include "modelgroup.hh"
#include "linedata.hh"
#include "calcharmonics.hh"
#include "calctracks.hh"
#include "calcmesh.hh"
#include "emitterbeam.hh"
#include "crosscircle.hh"
#include "pathaxis.hh"
#include "calcmlfmm.hh"
#include "serializer.hh"

// DESCRIPTION
// This example shows how to create a small CCT magnet with two layers.
// We use the default CCT magnet input which only supports one harmonic.
// For more customizable CCT geometries have a look at the custom_cct example.

// main
int main(){
	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = true; // calculate field on surface of coil
	const bool run_surface_field = true; // calculate field on surface of coil
	const bool run_harmonics = true; // calculate harmonics along aperture
	const bool run_tracking = true; // track particles through the aperture
	const bool run_freecad = false; // export model to freecad
	const bool run_opera = false; // export model to opera
	const bool run_json_export = true; // export geometry to json file
	const bool run_peak_field = true; // calculate peak field on conductor and show in log

	// data storage directory and filename
	const boost::filesystem::path output_dir = "./mini_cct/";
	
	// geometry settings
	const arma::uword num_poles = 3; // harmonic 1: dipole, 2: quadrupole, 3: sextupole, 4: octupole etc
	const rat::fltp delta =  0.5e-3; // spacing between turns on inner radius
	const rat::fltp alpha = 30.0*2*arma::Datum<rat::fltp>::pi/360; // skew angle for inner radius
	
	// coil geometry
	const bool is_reverse = false; // flag for reverse slanting
	const arma::uword num_turns = 10; // number of turns
	const rat::fltp radius = 0.025; // aperture radius in [m]
	const rat::fltp dradial = 0.3e-3; // spacing between cylinders [m]
	const rat::fltp dformer = 2e-3; // thickness of formers [m]
	const arma::uword num_nodes_per_turn = 180;	// number of nodes each turn
	const rat::fltp element_size = 2e-3; // size of the elements [m]
		
	// conductor geometry
	const arma::uword nd = 2; // number of elements in cable thickness
	const arma::uword nw = 5; // number of elements in cable width
	const rat::fltp num_strands = nd*nw; // number of turns (can be non-integer)
	const rat::fltp dstr = 0.825e-3; // diameter of the wire [m]
	const rat::fltp ddstr = 1.0e-3;
	const rat::fltp fcu2sc = 1.9; // copper to superconductor fraction of the ware

	// cable dimensions
	const rat::fltp dcable = 2*ddstr; // thickness of cable [m] (width of slot)
	const rat::fltp wcable = 5*ddstr; // width of cable [m] (depth of slot)

	// operating conditions
	const rat::fltp operating_current = 400; // operating current [A]
	const rat::fltp operating_temperature = 4.5; // operating temperature [K]

	// calculate remaining parameters
	const rat::fltp r1 = radius+dformer; // inner coil radius [m]
	const rat::fltp r2 = radius+2*dformer+wcable+dradial; // outer coil radius [m]

	// inner radius parameters
	const rat::fltp a = radius/(num_poles*std::tan(alpha));
	const rat::fltp omega = (dcable + delta)/std::sin(alpha);



	// GEOMETRY SETUP
	// construct conductor
	rat::mat::ShRutherfordCablePr nbti_cable = rat::mat::RutherfordCable::create();
	nbti_cable->set_strand(rat::mat::Database::nbti_wire_lhc(dstr,fcu2sc));
	nbti_cable->set_num_strands(nd*nw);
	nbti_cable->set_width(wcable);
	nbti_cable->set_thickness(dcable);
	nbti_cable->set_keystone(0);
	nbti_cable->set_pitch(0);
	nbti_cable->set_dinsu(0);
	nbti_cable->set_fcabling(1.0);
	nbti_cable->setup();

	// create path for inner layer
	rat::mdl::ShPathCCTPr path_cct1 = rat::mdl::PathCCT::create(
		num_poles,r1,a,omega,num_turns,num_nodes_per_turn);
	path_cct1->set_is_reverse(is_reverse); // reverse slanting
	path_cct1->set_twist(alpha/num_poles);

	// create path for outer layer
	rat::mdl::ShPathCCTPr path_cct2 = rat::mdl::PathCCT::create(
		num_poles,r2,a,omega,num_turns,num_nodes_per_turn);
	path_cct2->set_is_reverse(!is_reverse); // reverse slanting
	path_cct2->set_twist(alpha/num_poles);
	

	// create cable cross section
	// note that the cable is centered at least
	// in the thickness direction
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(-dcable/2,dcable/2,0,wcable,dcable/nd,wcable/nw);

	// create inner and outer coils
	rat::mdl::ShModelCoilPr coil1 = rat::mdl::ModelCoil::create(path_cct1, cross_rect, nbti_cable);
	coil1->set_name("inner");
	coil1->set_operating_temperature(operating_temperature);
	coil1->set_operating_current(operating_current);
	coil1->set_number_turns(num_strands);
	
	rat::mdl::ShModelCoilPr coil2 = rat::mdl::ModelCoil::create(path_cct2, cross_rect, nbti_cable);
	coil2->set_name("outer");
	coil2->set_operating_temperature(operating_temperature);
	coil2->set_operating_current(operating_current);
	coil2->set_number_turns(num_strands);

	// create model that combines coils
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
	model->add_model(coil1); model->add_model(coil2);

	// axis of magnet for field quality calculation
	rat::mdl::ShPathAxisPr pth_axis = rat::mdl::PathAxis::create('z','y',0.4,0,0,0,element_size);
	



	// CALCULATION AND OUTPUT
	// Create log
	// the default log displays the status in the
	// terminal window in which the code is called
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// ensure existence of output dir
	boost::filesystem::create_directory(output_dir);

	// create calculation
	rat::mdl::ShCalcMlfmmPr mlfmm = 
		rat::mdl::CalcMlfmm::create(model);
	mlfmm->set_target_meshes(run_coil_field); 
	mlfmm->set_target_surface(run_surface_field);

	// Create a grid of 140x160x50 points in the volume defined by intervals:
	rat::mdl::ShModelPr beam_pipe = rat::mdl::ModelCoil::create(
		rat::mdl::PathAxis::create('z', 'y', 0.7, 0,0,0, element_size),
		rat::mdl::CrossCircle::create(radius,element_size));

	// run tracking code
	if(run_tracking){
		// create an emitter
		rat::mdl::ShEmitterBeamPr beam_emitter = rat::mdl::EmitterBeam::create();
		beam_emitter->set_proton();
		beam_emitter->set_num_particles(5000);
		beam_emitter->set_beam_energy(0.948); // GeV
		beam_emitter->set_spawn_coord(
			arma::Col<rat::fltp>::fixed<3>{0,0,-0.349},
			rat::cmn::Extra::unit_vec('z'),
			rat::cmn::Extra::unit_vec('x'),
			rat::cmn::Extra::unit_vec('y'));
		beam_emitter->set_xx(0.9, 0.005, 0.0*2*arma::Datum<rat::fltp>::pi/360);
		beam_emitter->set_yy(0.9, 0.005, 0.0*2*arma::Datum<rat::fltp>::pi/360);
		beam_emitter->set_start_idx(0);

		// create tracker
		rat::mdl::ShCalcTracksPr tracker = rat::mdl::CalcTracks::create(model, beam_pipe, {beam_emitter});
		tracker->set_rabstol(1e-3); tracker->set_rreltol(1e-4);
		// tracker->set_pabstol(1.0); tracker->set_preltol(1e-4);

		// perform tracking algorithm
		rat::mdl::ShTrackDataPr track_data = tracker->calculate_tracks(RAT_CONST(0.0), lg);

		// write to vtk
		track_data->export_vtk()->write(output_dir/"tracks.vtu");
	}

	// calculate harmonics
	if(run_harmonics){
		// settings
		const rat::fltp reference_radius = RAT_CONST(10e-3);
		const bool compensate_curvature = true;

		// create calculation
		rat::mdl::ShCalcHarmonicsPr harmonics_calculator = rat::mdl::CalcHarmonics::create(model, pth_axis, reference_radius, compensate_curvature);

		// calculate harmonics and get data
		harmonics_calculator->calculate_write({RAT_CONST(0.0)},output_dir,lg);
	}

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({RAT_CONST(0.0)},output_dir,lg);
	}

	// calculate peak field and show in terminal
	if(run_peak_field){
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model);
		const std::list<rat::mdl::ShMeshDataPr> mesh_data = mesh->calculate_mesh(); arma::uword idx = 0;
		lg->msg(2,"%sPeak field calculation%s\n",KGRN,KNRM);
		for(auto it=mesh_data.begin();it!=mesh_data.end();it++,idx++){
			const rat::fltp Bmax = arma::max(rat::cmn::Extra::vec_norm((*it)->get_field('B')));
			lg->msg("%02i, %12s, %s%8.5g [T]%s\n",idx,(*it)->get_name().c_str(),KYEL,Bmax,KNRM);
		}
		lg->msg(-2,"\n");
	}

	// export json
	if(run_json_export){
		// create serializer
		const rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model);

		// write to output file
		slzr->export_json(output_dir/"model.json");
	}

	// write a freecad macro
	if(run_freecad){
		const rat::cmn::ShFreeCADPr fc = rat::cmn::FreeCAD::create(output_dir/"model.FCMacro","clover");
		fc->set_num_sub(1); model->export_freecad(fc);
	}

	// export json
	if(run_json_export){
		const rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model);
		slzr->export_json(output_dir/"model.json");
	}

	// export to an opera conductor file
	if(run_opera){
		model->export_opera(rat::cmn::Opera::create(output_dir/"opera.cond"));
	}

	


}