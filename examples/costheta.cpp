// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// command line parser
#include <tclap/CmdLine.h>

// rat-common headers
#include "rat/common/log.hh"

// rat-models header files
#include "version.hh"
#include "pathcostheta.hh"
#include "crosswedge.hh"
#include "modelcoil.hh"
#include "modelgroup.hh"
#include "calcmesh.hh"
#include "modelcylinder.hh"


// main
int main(int argc, char * argv[]){

	// COMMAND LINE INPUT
	// create tclap object
	TCLAP::CmdLine cmd("Cos-Theta Coil-End Example",' ',rat::cmn::Extra::create_version_tag(
		MAT_PROJECT_VER_MAJOR,MAT_PROJECT_VER_MINOR,MAT_PROJECT_VER_PATCH));

	// filename input argument
	TCLAP::UnlabeledValueArg<std::string> output_path_argument(
		"od","Output dir",false,"./costheta/", "string", cmd);

	// parse the command line arguments
	cmd.parse(argc, argv);

	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = true; // calculate field on surface of coil
	const bool run_json_export = true; // export geometry to json file

	// data storage directory and filename
	const boost::filesystem::path output_dir = output_path_argument.getValue();

	// settings
	const arma::uword num_poles = 2;
	const arma::uword num_cables1 = 4llu;
	const arma::uword num_cables2 = 2llu;
	const rat::fltp radius1 = RAT_CONST(0.025);
	const rat::fltp radius2 = RAT_CONST(0.025);
	const rat::fltp wcable = RAT_CONST(5e-3);
	const rat::fltp dinsu = RAT_CONST(0.1e-3);
	const rat::fltp phi1 = 5*arma::Datum<rat::fltp>::tau/360;
	const rat::fltp phi2 = 20*arma::Datum<rat::fltp>::tau/360;
	const rat::fltp alpha1 = phi1;
	const rat::fltp alpha2 = phi2;
	const rat::fltp dinner_bare = RAT_CONST(0.9e-3);
	const rat::fltp douter_bare = RAT_CONST(1.0e-3); 
	const rat::fltp element_size = RAT_CONST(1e-3);
	const rat::fltp operating_current = RAT_CONST(8000); // [A]
	const rat::fltp operating_temperature = RAT_CONST(1.9);
	const rat::fltp zend1 = RAT_CONST(0.125);
	const rat::fltp zend2 = RAT_CONST(0.1);
	const rat::fltp beta1 = RAT_CONST(55.0)*arma::Datum<rat::fltp>::tau/360;
	const rat::fltp beta2 = RAT_CONST(65.0)*arma::Datum<rat::fltp>::tau/360;

	// GEOMETRY
	// combine paths into a group
	const rat::mdl::ShPathCosThetaPr costheta_path = rat::mdl::PathCosTheta::create({
		rat::mdl::CosThetaBlock::create(num_poles, num_cables1, radius1, phi1, alpha1, dinner_bare, douter_bare, wcable, zend1, beta1, dinsu),
		rat::mdl::CosThetaBlock::create(num_poles, num_cables2, radius2, phi2, alpha2, dinner_bare, douter_bare, wcable, zend2, beta2, dinsu)}, element_size);

	// create a rectangular cross section
	// the cable is centered around the origin
	const rat::mdl::ShCrossWedgePr cross_wedge = rat::mdl::CrossWedge::create(dinner_bare,douter_bare,-wcable,0.0,2,10);
	cross_wedge->set_surface_offset(dinsu);


	// create inner and outer coils
	const rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(costheta_path, cross_wedge);
	coil->set_name("cos-theta end"); 
	coil->set_number_turns(1);
	coil->set_operating_current(operating_current);
	coil->set_operating_temperature(operating_temperature);

	// create model that combines coils
	const rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create(coil);
	model->add_model(rat::mdl::ModelCylinder::create(std::min(radius1,radius2), 1.5*zend1, element_size));

	// CALCULATION AND OUTPUT
	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(rat::mdl::ModelGroup::create(model));

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({RAT_CONST(0.0)},output_dir,lg);
	}
	
	// export json
	if(run_json_export){
		const rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model);
		slzr->export_json(output_dir/"model.json");
	}
}
