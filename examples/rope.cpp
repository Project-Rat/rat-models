// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files from Common
#include "rat/common/log.hh"

// header files for Models
#include "crosscircle.hh"
#include "modelcoil.hh"
#include "pathcct.hh"
#include "modelgroup.hh"
#include "linedata.hh"
#include "calcmesh.hh"
#include "crosscircle.hh"
#include "serializer.hh"
#include "pathspiral.hh"
#include "pathmap.hh"
#include "pathoffset.hh"
#include "crosspoint.hh"

// DESCRIPTION
// example of a 6 around 1 rope cable mapped to a CCT path

// main
int main(){
	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = true; // calculate field on surface of coil
	const bool run_json_export = true; // export to json file to be loaded in GUI

	// data storage directory and filename
	const boost::filesystem::path output_dir = "./rope/";

	// cct settings
	const bool is_reverse = false;
	const arma::uword num_poles = 1; // [#]
	const rat::fltp cct_radius = RAT_CONST(0.03); // [m]
	const rat::fltp cct_angle = RAT_CONST(60.0)*arma::Datum<rat::fltp>::tau/360; // [rad]
	const rat::fltp strand_diameter = 0.3e-3; // [m]
	const rat::fltp drib = 0.5e-3; // [m]
	const rat::fltp num_turns = 10.0; // [#]
	const arma::uword nnpt = 360; // [#]

	// cable settings
	const bool use_point_crss = false; // use point cross section
	const arma::uword nrope_width = 1;
	const arma::uword nrope_thickness = 1;
	const rat::fltp pitch = 30e-3; // [m]
	const rat::fltp element_size = 1e-3; // [m]

	// operation settings
	const rat::fltp operating_temperature = 20.0; // [K]
	const rat::fltp operating_current = 60.0; // [A]

	// calculate rope thickness
	const rat::fltp drope = 3*strand_diameter;

	// GEOMETRY SETUP
	// create CCT path
	rat::mdl::ShPathCCTPr path_cct = rat::mdl::PathCCT::create();
	path_cct->set_num_poles(num_poles); 
	path_cct->set_radius(cct_radius);
	path_cct->set_alpha(cct_angle);
	path_cct->set_use_skew_angle(); // use angle instead of amplitude
	path_cct->set_pitch(nrope_thickness*drope + drib);
	path_cct->set_normal_omega(); // apply pitch in the normal direction
	path_cct->set_num_turns(num_turns);
	path_cct->set_num_nodes_per_turn(nnpt);
	path_cct->set_is_reverse(is_reverse); // reverse slanting

	// create model that combines coils
	const rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
	model->set_name("cct coil");

	// create strand positions
	for(arma::uword i=0;i<nrope_width;i++){
		for(arma::uword j=0;j<nrope_thickness;j++){
			// create offset path
			const rat::fltp noff = -(nrope_thickness*drope)/2 + drope/2 + j*drope;
			const rat::fltp doff = drope/2 + i*drope;
			rat::mdl::ShPathOffsetPr path_offset = rat::mdl::PathOffset::create(path_cct, noff, doff);

			// calculate length of this path
			const rat::fltp ell = path_offset->create_frame(rat::mdl::MeshSettings())->calc_total_ell();
			assert(ell!=0);

			// calculate number of pitch lengths 
			const rat::fltp nt2 = ell/pitch;
			assert(nt2!=0);

			// cross section
			rat::mdl::ShCrossPr strand_cross;
			const rat::fltp strand_cross_area = arma::Datum<rat::fltp>::pi*std::pow(strand_diameter/2,2);
			if(use_point_crss)strand_cross = rat::mdl::CrossPoint::create(RAT_CONST(0.0),RAT_CONST(0.0),std::sqrt(strand_cross_area),std::sqrt(strand_cross_area));
			else strand_cross = rat::mdl::CrossCircle::create(strand_diameter/2, strand_diameter/4);

			// create model that combines coils
			const rat::mdl::ShModelGroupPr rope = rat::mdl::ModelGroup::create();
			rope->set_name("rope");

			// walk over 6 wires
			for(arma::uword i=0;i<6;i++){
				// create spiral
				const rat::mdl::ShPathSpiralPr spiral = rat::mdl::PathSpiral::create(strand_diameter, 0, nt2, pitch, element_size, false);

				// offset the spiral for this strand
				spiral->set_phase(i*arma::Datum<rat::fltp>::tau/6);

				// map spiral to CCT path
				const rat::mdl::ShPathMapPr map = rat::mdl::PathMap::create(path_offset, spiral);

				// create inner and outer coils
				const rat::mdl::ShModelCoilPr strand = rat::mdl::ModelCoil::create(map, strand_cross);
				strand->set_name("strand " + std::to_string(i+1));
				strand->set_operating_temperature(operating_temperature);
				strand->set_operating_current(operating_current);
				strand->set_number_turns(1);

				// add to rope
				rope->add_model(strand);
			}

			// put the 7th middle strand
			// create inner and outer coils
			const rat::mdl::ShModelCoilPr strand = rat::mdl::ModelCoil::create(path_offset, strand_cross);
			strand->set_name("strand 7");
			strand->set_operating_temperature(operating_temperature);
			strand->set_operating_current(operating_current);
			strand->set_number_turns(1);

			// add to rope
			rope->add_model(strand);

			// add rope to model
			model->add_model(rope);
		}
	}

	// CALCULATION AND OUTPUT
	// Create log
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// ensure existence of output dir
	boost::filesystem::create_directory(output_dir);

	// export json
	if(run_json_export){
		// create serializer
		const rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model);

		// write to output file
		slzr->export_json(output_dir/"model.json");
	}

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({RAT_CONST(0.0)},output_dir,lg);
	}

	
}