// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>

// header files for common
#include "rat/common/extra.hh"

// header files for Models
#include "pathcircle.hh"
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "pathconnect.hh"
#include "pathconnect2.hh"
#include "pathcable.hh"
#include "pathconnect.hh"
#include "modelmesh.hh"
#include "calcmesh.hh"

// main
int main(){
	// SETTINGS
	// switchboard
	bool run_coil_field = true;
	bool run_json_export = true;

	// output directory
	const boost::filesystem::path output_dir = "./connector/";

	// parameters
	const rat::fltp inner_radius = 0.1;
	const rat::fltp element_size = 2.5e-3;
	const arma::uword num_turns = 10;
	const rat::fltp dcable = 1.2e-3;
	const rat::fltp wcable = 12e-3;
	const rat::fltp dinsu = 0.1e-3;
	const arma::uword num_sections = 4;
	const rat::fltp operating_current = 4000;


	// GEOMETRY
	// create circle
	rat::mdl::ShPathCirclePr path_circle1 = rat::mdl::PathCircle::create(inner_radius,num_sections,element_size);

	// create cable
	rat::mdl::ShPathCablePr path_cable1 = rat::mdl::PathCable::create(path_circle1);
	path_cable1->set_turn_step(dcable + 2*dinsu);
	path_cable1->set_num_turns(num_turns);
	path_cable1->set_idx_incr(1);
	path_cable1->set_idx_start(0);
	path_cable1->set_reverse_base(false);
	
	// create circle
	rat::mdl::ShPathCirclePr path_circle2 = rat::mdl::PathCircle::create(inner_radius,num_sections,element_size);
	path_circle2->add_rotation(-arma::Datum<rat::fltp>::pi/5,-arma::Datum<rat::fltp>::pi/5,0);
	path_circle2->add_translation(0.3,0,0.1);

	// create cable
	rat::mdl::ShPathCablePr path_cable2 = rat::mdl::PathCable::create(path_circle2);
	path_cable2->set_turn_step(dcable + 2*dinsu);
	path_cable2->set_num_turns(num_turns);
	path_cable2->set_num_add(-2);
	path_cable2->set_idx_incr(1);
	path_cable2->set_idx_start(0);
	path_cable2->set_reverse_base(false);
	path_cable2->add_reverse();
	// path_cable2->add_flip();

	// create connector (note changed in 2.015.1)
	rat::mdl::ShPathConnect2Pr connect = rat::mdl::PathConnect2::create(path_cable1,path_cable2,0.35,0,0,element_size);
	connect->optimize_control_points();

	// create cable cross section
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(-dcable/2,dcable/2,-wcable/2,wcable/2,element_size);

	// create coil
	rat::mdl::ShModelCoilPr model_coil = rat::mdl::ModelCoil::create(connect, cross_rect);
	model_coil->set_number_turns(num_turns);
	
	// create a circuit and coolant
	model_coil->set_operating_current(operating_current);


	// create log
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model_coil);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({0},output_dir,rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT));
	}

	// export json
	if(run_json_export){
		// write to output file
		const rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model_coil);
		slzr->export_json(output_dir/"model.json");
	}
}