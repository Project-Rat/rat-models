// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for Rat-Common
#include "rat/common/gmshfile.hh"

// materials headers
#include "rat/mat/database.hh"

// distmesh headrers
#include "rat/dmsh/dfdiff.hh"
#include "rat/dmsh/dfintersect.hh"
#include "rat/dmsh/dfscale.hh"
#include "rat/dmsh/dfellipse.hh"
#include "rat/dmsh/dfcircle.hh"
#include "rat/dmsh/dfrectangle.hh"
#include "rat/dmsh/dfrrectangle.hh"
#include "rat/dmsh/dfpolygon.hh"

// header files for Rat-Models
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "pathcable.hh"
#include "pathrectangle.hh"
#include "transbend.hh"
#include "modelgroup.hh"
#include "pathflared.hh"
#include "meshdata.hh"
#include "cartesiangriddata.hh"
#include "background.hh"
#include "calcinductance.hh"
#include "calcmesh.hh"
#include "calcgrid.hh"
#include "calcplane.hh"
#include "modelnl.hh"
#include "pathaxis.hh"
#include "crossdmsh.hh"
#include "hbcurvetable.hh"
#include "modelmirror.hh"
#include "pathattach.hh"
#include "calcfreecad.hh"

// DESCRIPTION
// FRESCA2 is a large aperture Nb3Sn coil developed 
// at TE-MSC-MDT CERN. This example shows how to produce
// a model of its coils with fx-models. Note that this 
// demonstration model was derived from papers, 
// presentations and an Opera model and is by 
// no means the official version. 

// main
int main(){
	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = true; // calculate magnetic field on the coil
	const bool run_inductance = false; // calculate coil inductance and stored energy
	const bool run_grid = false;
	const bool run_json_export = true; // export geometry to json file
	const bool run_plane = true;
	const bool run_freecad = false; // export geometry to freecad files

	// data storage directory and filename
	const boost::filesystem::path output_dir = "./fresca2/";

	// output time
	const rat::fltp output_time = RAT_CONST(0.0);

	// coil geometry settings
	const rat::fltp wcable = RAT_CONST(21.8e-3); // width of the cable [m] (incl insulation)
	// const rat::fltp dcable = RAT_CONST(0.002334); // thickness of cable [m] (incl insulation)
	const rat::fltp radius1 = RAT_CONST(0.7679); // hardway radius of flared end [m]
	const rat::fltp radius21 = RAT_CONST(0.058); // radius at end of the central coils [m]
	const rat::fltp radius22 = RAT_CONST(0.04468); // radius at end of the outer coils [m]
	const rat::fltp element_size = RAT_CONST(6e-3); // size of the elements [m]
	const rat::fltp dcoil1 = RAT_CONST(0.084024);	// thickness of coil pack for first set of layers [m]
	const rat::fltp dcoil2 = RAT_CONST(0.098028); // thickness of coil pack for second set of layers [m]
	const rat::fltp ell1 = 2*RAT_CONST(0.3640615); // straight section length [m]
	const rat::fltp ell21 = RAT_CONST(0.024); // straight section length between bend and coil-end for central coils [m]
	const rat::fltp ell22 = RAT_CONST(0.032); // straight section length between bend and coil-end for outer coils [m]
	const rat::fltp arcl = RAT_CONST(17.0)*arma::Datum<rat::fltp>::tau/360; // flared end angle [rad]
	const rat::fltp dspace1 = RAT_CONST(0.5e-3); // spacing between the single pancakes [m]
	const rat::fltp dspace2 = RAT_CONST(1.5e-3); // spacing between the rat::fltp pancakes [m]
	const rat::fltp operating_current = RAT_CONST(10900.0); // operating current [A] (12T)
	// const rat::fltp operating_current = 12400; // operating current [A] (15T)
	const rat::fltp operating_temperature = RAT_CONST(1.9); // operating temperature [K]
	const rat::fltp num_turns1 = RAT_CONST(36.0); // number of turns in the central coils
	const rat::fltp num_turns2 = RAT_CONST(42.0); // number of turns in the outer coils

	// iron geometry settings
	#ifdef ENABLE_NL_SOLVER
	const bool use_iron = true; // add the iron yoke and pole pieces to the model, requires the code to be linked to nlsolver
	const rat::fltp shell_radius_a = 0.43; // [m]
	const rat::fltp shell_radius_b = 0.45; // [m]
	const rat::fltp shell_length = 1.6; // [m]
	const rat::fltp shell_element_size = 40e-3; // [m]
	const rat::fltp shell_separation_distance = 2*0.0026; // [m]
	const rat::fltp shell_aperture_width = 2*0.216; // [m]
	const rat::fltp shell_aperture_height = 2*0.24; // [m]
	const rat::fltp shell_aperture_radius = 0.02; // [m]
	const rat::fltp shell_hole_diameter = 2*0.03; // [m]
	const rat::fltp shell_hole_x = 0.062; // [m]
	const rat::fltp shell_hole_y = 0.3; // [m]
	const rat::fltp shell_hole_element_size = 20e-3; // [m]
	const rat::fltp shell_hole_scaling = 0.15;
	const rat::fltp shell_element_size_max = 0.1; // [m]
	const rat::fltp vpad_width = 2*0.1545; // [m]
	const rat::fltp vpad_y1 = 0.111; // [m]
	const rat::fltp vpad_y2 = 0.235; // [m]
	const rat::fltp vpad_element_size = 0.03; // [m]
	const rat::fltp vpad_length = 0.8; // [m]
	const rat::fltp post_element_size = 0.02; // [m]
	const arma::Col<rat::fltp> post_x{0.039,0.044,0.044,0,0,0.0125,0.0164,0.0178,0.0191}; // [m]
	const arma::Col<rat::fltp> post_y{0.048,0.048,0.093,0.093,0.0625,0.0625,0.0615,0.0605,0.0588}; // [m]
	#endif

	// GEOMETRY SETUP
	// construct conductor
	rat::mat::ShConductorPr nb3sn_cable = 
		rat::mat::Database::nb3sn_fresca2_cable(true);

	// create flared end path
	rat::mdl::ShPathFlaredPr pathflared1 = rat::mdl::PathFlared::create(
		ell1,ell21,-arcl,radius1,
		radius21,element_size,dcoil1,-wcable);
	rat::mdl::ShPathFlaredPr pathflared2 = rat::mdl::PathFlared::create(
		ell1,ell21,-arcl,radius1-wcable-dspace1,
		radius21,element_size,dcoil1,-2*wcable-dspace1);
	rat::mdl::ShPathFlaredPr pathflared3 = rat::mdl::PathFlared::create(
		ell1,ell22,-arcl,radius1-2*wcable-dspace1-dspace2,
		radius22,element_size,dcoil2,-3*wcable-dspace1-dspace2);
	rat::mdl::ShPathFlaredPr pathflared4 = rat::mdl::PathFlared::create(
		ell1,ell22,-arcl,radius1-3*wcable-2*dspace1-dspace2,
		radius22,element_size,dcoil2,-4*wcable-2*dspace1-dspace2);
	
	// create cross section
	rat::mdl::ShCrossRectanglePr cross_rect1 = rat::mdl::CrossRectangle::create(0,dcoil1,-wcable,0,element_size);
	rat::mdl::ShCrossRectanglePr cross_rect2 = rat::mdl::CrossRectangle::create(0,dcoil2,-wcable,0,element_size);

	// create coil
	rat::mdl::ShModelCoilPr coil1 = rat::mdl::ModelCoil::create(pathflared1, cross_rect1);
	coil1->set_name("layer1");
	coil1->add_translation(0,0,0.0144 + wcable/2);
	coil1->set_number_turns(num_turns1);
	coil1->set_operating_current(operating_current);
	coil1->set_operating_temperature(operating_temperature);
	coil1->set_input_conductor(nb3sn_cable);
	
	// create coil
	rat::mdl::ShModelCoilPr coil2 = rat::mdl::ModelCoil::create(pathflared2, cross_rect1);
	coil2->set_name("layer2");
	coil2->add_translation(0,0,0.0367 + wcable/2);
	coil2->set_number_turns(num_turns1);
	coil2->set_operating_current(operating_current);
	coil2->set_operating_temperature(operating_temperature);
	coil2->set_input_conductor(nb3sn_cable);
	
	// create coil
	rat::mdl::ShModelCoilPr coil3 = rat::mdl::ModelCoil::create(pathflared3, cross_rect2);
	coil3->set_name("layer3");
	coil3->add_translation(0,0,0.06 + wcable/2);
	coil3->set_number_turns(num_turns2);
	coil3->set_operating_current(operating_current);
	coil3->set_operating_temperature(operating_temperature);
	coil3->set_input_conductor(nb3sn_cable);
	
	// create coil
	rat::mdl::ShModelCoilPr coil4 = rat::mdl::ModelCoil::create(pathflared4, cross_rect2);
	coil4->set_name("layer4");
	coil4->add_translation(0,0,0.0823 + wcable/2);
	coil4->set_number_turns(num_turns2);
	coil4->set_operating_current(operating_current);
	coil4->set_operating_temperature(operating_temperature);
	coil4->set_input_conductor(nb3sn_cable);
	
	// magnet upper pole
	rat::mdl::ShModelGroupPr pole1 = rat::mdl::ModelGroup::create();
	pole1->set_name("pole1");
	pole1->add_model(coil1); pole1->add_model(coil2);
	pole1->add_model(coil3); pole1->add_model(coil4);

	// magnet lower pole
	rat::mdl::ShModelGroupPr pole2 = rat::mdl::ModelGroup::create();
	pole2->set_name("pole2");
	pole2->add_model(coil1); pole2->add_model(coil2);
	pole2->add_model(coil3); pole2->add_model(coil4);
	pole2->add_rotation(1,0,0,arma::Datum<rat::fltp>::pi);
	pole2->add_reverse();

	// model
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
	model->set_name("Fresca2");
	model->add_model(pole1); 
	model->add_model(pole2);

	// iron yoke
	// TODO: this could be better parametrized
	#ifdef ENABLE_NL_SOLVER
	if(use_iron){
		// the hb curve (default table is for ARMCO)
		const rat::mdl::ShHBCurvePr hb_curve = rat::mdl::HBCurveTable::create();

		// the clamshell
		const rat::mdl::ShModelNLPr yoke = rat::mdl::ModelNL::create(
			rat::mdl::PathAxis::create('y','z',shell_length,{0,0,0},shell_element_size),
			rat::mdl::CrossDMsh::create(shell_hole_element_size, // build the cross section using distmesher
				rat::dm::DFDiff::create({rat::dm::DFIntersect::create({ // take one quadrant of the ellipse
					rat::dm::DFEllipse::create(shell_radius_a,shell_radius_b), // the outer elliptical shape
					rat::dm::DFRectangle::create(shell_separation_distance/2,shell_radius_a+0.02,0.0,shell_radius_b+0.02)}), // one quadrant
				rat::dm::DFRRectangle::create(
					-shell_aperture_width/2,shell_aperture_width/2,
					-shell_aperture_height/2,shell_aperture_height/2,shell_aperture_radius), // the aperture hole
				rat::dm::DFCircle::create(shell_hole_diameter/2,shell_hole_x,shell_hole_y)}), // the circular hole at the top
				rat::dm::DFScale::create(rat::dm::DFCircle::create(
					shell_hole_diameter/2,shell_hole_x,shell_hole_y),
					shell_hole_element_size,shell_hole_scaling,shell_element_size_max)), // scale element size around the hole
			hb_curve);
		yoke->set_name("yoke");
		yoke->set_operating_temperature(operating_temperature);

		// the vertical pad
		const rat::mdl::ShModelNLPr vpad = rat::mdl::ModelNL::create(
			rat::mdl::PathAxis::create('y','z',vpad_length,{0,0,0},shell_element_size),
			rat::mdl::CrossRectangle::create(0.0,vpad_width/2,vpad_y1,vpad_y2,vpad_element_size,vpad_element_size),
			hb_curve);
		vpad->set_name("vpad");
		vpad->set_operating_temperature(operating_temperature);

		// the post
		const rat::mdl::ShModelNLPr post = rat::mdl::ModelNL::create(
			rat::mdl::PathAttach::create({
				rat::mdl::PathArc::create(0.793,arcl,shell_element_size,0.0,true),
				rat::mdl::PathAxis::create('y','z',0.728123,{0,0,0},shell_element_size),
				rat::mdl::PathArc::create(0.793,-arcl,shell_element_size,0.0,true)},2),
			rat::mdl::CrossDMsh::create(post_element_size,
				rat::dm::DFPolygon::create(arma::join_horiz(post_x,post_y))),
			hb_curve);
		post->set_name("post");
		post->set_operating_temperature(operating_temperature);

		// mirror
		const rat::mdl::ShModelMirrorPr mirror_a = rat::mdl::ModelMirror::create();
		const rat::mdl::ShModelMirrorPr mirror_b = rat::mdl::ModelMirror::create();
		mirror_a->set_plane_vector({0,0,1});
		mirror_a->add_models({yoke,vpad,post});
		mirror_b->set_plane_vector({1,0,0});
		mirror_b->add_model(mirror_a);
		mirror_b->set_name("iron");
		model->add_model(mirror_b);
	}
	#endif


	// CALCULATION AND OUTPUT
	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create a cache for solve storage
	const rat::mdl::ShSolverCachePr cache = rat::mdl::SolverCache::create();


	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({output_time},output_dir,lg,cache);
	}

	// create grid calculation
	if(run_grid){
		// create grid calculation
		const rat::mdl::ShCalcGridPr grid = rat::mdl::CalcGrid::create(model,0.4,1.6,0.4,100,400,100);

		// calculate and write vtk output files to specified directory
		grid->calculate_write({output_time},output_dir,lg,cache);
	}

	// create grid calculation
	if(run_plane){
		// create grid calculation
		const rat::mdl::ShCalcPlanePr plane = rat::mdl::CalcPlane::create(model,'x',2.4,1.35,600,350);

		// calculate and write vtk output files to specified directory
		plane->calculate_write({output_time},output_dir,lg,cache);
	}

	// inductance calculation
	if(run_inductance){
		// create inductance calculation
		const rat::mdl::ShCalcInductancePr inductance_calculator = rat::mdl::CalcInductance::create(model);

		// calculate and show matrix in terminal
		inductance_calculator->calculate_write({output_time},output_dir,lg,cache);
	}

	// export json
	if(run_json_export){
		// write to output file
		const rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model);
		slzr->export_json(output_dir/"model.json");
	}

	// freecad builder
	if(run_freecad){
		const rat::mdl::ShCalcFreeCADPr fc = rat::mdl::CalcFreeCAD::create(model, output_dir/"freecad");
		fc->calculate_write({output_time},output_dir,lg,cache);
	}

}



