// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// header files for materials
#include "rat/mat/database.hh"

// header files for model
#include "crosscircle.hh"
#include "pathxyz.hh"
#include "modelcoil.hh"
#include "modelgroup.hh"
#include "calcgroup.hh"
#include "modelroot.hh"
#include "calcmesh.hh"
#include "calcplane.hh"

// main function
int main(){
	// settings
	// ===
	// directory for storing the data and output model
	const boost::filesystem::path output_dir = "./coil_from_coords";

	// intermediate file for storing the coordinates
	const boost::filesystem::path fpath = output_dir/"coord.csv";

	// calculation settings
	const bool run_calculations = false;
	const rat::fltp output_time = RAT_CONST(0.0);

	// wire settings
	const rat::fltp wire_radius = RAT_CONST(0.5e-3); // [m]
	const rat::fltp cross_element_size = RAT_CONST(0.2e-3); // [m]
	const rat::fltp fcu2sc = RAT_CONST(1.9);
	const rat::fltp RRR = RAT_CONST(150.0);

	// operating conditions
	const rat::fltp operating_current = RAT_CONST(1000.0); // [A]
	const rat::fltp operating_temperature = RAT_CONST(4.5); // [K]

	// create a table with spiral coordinates
	// ====
	// this part can be done in matlab for example
	// or in a completely separate script
	{
		// settings for generating the coordinate table file
		const rat::fltp coil_radius = RAT_CONST(0.05);
		const arma::sword nt1 = -5llu;
		const arma::sword nt2 = 5llu;
		const rat::fltp omega = RAT_CONST(0.01);
		const arma::uword num_element_per_turn = 90llu;
		const arma::Row<rat::fltp> theta = arma::linspace<arma::Row<rat::fltp> >(
			nt1*arma::Datum<rat::fltp>::tau, 
			nt2*arma::Datum<rat::fltp>::tau, 
			(nt2 - nt1 + 1)*num_element_per_turn);
		const arma::Mat<rat::fltp> R1 = arma::join_vert(
			coil_radius*arma::cos(theta), 
			coil_radius*arma::sin(theta), 
			omega*theta/arma::Datum<rat::fltp>::tau);
		const arma::field<std::string> header1{"x [m]","y [m]","z [m]"};
		R1.save(arma::csv_name(fpath.string(),header1,arma::csv_opts::trans)); // save transpose of the matrix
	}


	// here we load the coil from the table
	// ====
	// read coordinates from file with columns X,Y,Z
	arma::Mat<rat::fltp> R2; arma::field<std::string> header2; 
	R2.load(arma::csv_name(fpath.string(),header2,arma::csv_opts::trans)); // note transpose

	// create a circular cross section
	const rat::mdl::ShCrossCirclePr circle_cross = rat::mdl::CrossCircle::create(wire_radius, cross_element_size);

	// put the coordinates onto a cylinder path
	bool is_cylinder = true;
	const arma::Col<rat::fltp>::fixed<3> cylinder_axis{RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(1.0)};
	const rat::mdl::ShPathXYZPr xyz_path = rat::mdl::PathXYZ::create(R2,cylinder_axis,is_cylinder);

	// create a material
	
	const rat::mat::ShConductorPr nbti_strand_mat = rat::mat::Database::nbti_wire_lhc(2*wire_radius, fcu2sc, RRR);

	// create the coil
	const rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(xyz_path,circle_cross,nbti_strand_mat);
	coil->set_operating_current(operating_current);
	coil->set_operating_temperature(operating_temperature);
	coil->set_number_turns(1.0);
	coil->set_num_gauss(1ll);
	coil->set_name("XYZ Coil");


	
	// run calculations
	// ===
	// create tree structure for GUI
	const rat::mdl::ShModelGroupPr tree = rat::mdl::ModelGroup::create(coil);
	const rat::mdl::ShCalcGroupPr calc = rat::mdl::CalcGroup::create();
	const rat::mdl::ShModelRootPr root = rat::mdl::ModelRoot::create(tree,calc);


	// create mesh calculation
	const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(tree);
	calc->add_calculation(mesh);

	// create plane calculation
	const rat::mdl::ShCalcPlanePr plane = rat::mdl::CalcPlane::create(tree,'y',0.2,0.2,400,400);
	calc->add_calculation(plane);

	// calculate and write vtk output files to specified directory
	if(run_calculations){
		// create log
		const rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

		// call calculate and write functions on each of the calculations
		mesh->calculate_write({output_time},output_dir,lg);
		plane->calculate_write({output_time},output_dir,lg);
	}

	// write to json file for GUI
	const rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(root);
	slzr->export_json(output_dir/"model.json");
}
