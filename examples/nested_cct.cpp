// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files from Common
#include "rat/common/log.hh"
#include "rat/common/opera.hh"
#include "rat/common/freecad.hh"
#include "rat/common/gmshfile.hh"

// conductor
#include "rat/mat/rutherfordcable.hh"
#include "rat/mat/database.hh"

// multipole method
#include "rat/mlfmm/mlfmm.hh"
#include "rat/mlfmm/multisources.hh"
#include "rat/mlfmm/multitargets.hh"

// header files for Models
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "pathcct.hh"
#include "modelgroup.hh"
#include "driveac.hh"
#include "emitterbeam.hh"
#include "crosscircle.hh"
#include "pathaxis.hh"
#include "transbend.hh"
#include "background.hh"
#include "serializer.hh"
#include "driveac.hh"
#include "vtkpvdata.hh"
#include "cartesiangriddata.hh"
#include "calcmesh.hh"
#include "calcpath.hh"
#include "calcgrid.hh"
#include "calcinductance.hh"
#include "calcharmonics.hh"
#include "linedata.hh"

// DESCRIPTION
// This example shows how to create a small CCT magnet with 
// both horizontal and vertical nested layers, by using
// the time dependence the magnetic field rotates around

// main
int main(){
	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = true; // calculate field in volume of mesh
	const bool run_line = false; // calculate field on a line
	const bool run_harmonics = false; // calculate harmonics along aperture
	const bool run_grid = false; // calculate field on a volume grid
	const bool run_freecad = false; // export model to freecad
	const bool run_inductance = false; // calculate inductance matrix
	const bool run_opera = false; // export model to opera
	const bool run_json_export = false; // export model to json file

	// data storage directory and filename
	const boost::filesystem::path output_dir = "./nested_cct/";
	
	// geometry settings
	const arma::uword num_poles = 1; // harmonic 1: dipole, 2: quadrupole, 3: sextupole, 4: octupole etc
	const arma::uword num_layers = 4; // number of layers in the coil 

	// coil geometry
	const bool is_reverse = false; // flag for reverse slanting
	const rat::fltp bending_radius = 0.0; // ooil bending in [m] (0 for no bending)
	const rat::fltp ell_coil = 0.4; // length of the coil in [m]
	//const rat::fltp ell_coil = 2*arma::Datum<rat::fltp>::pi*bending_radius/4; // coil length calculated from angle [m]
	const rat::fltp aperture_radius = 30e-3; // aperture radius in [m]
	const rat::fltp dradial = 0.3e-3; // spacing between cylinders [m]
	const rat::fltp dformer = 2e-3; // thickness of formers [m]
	const arma::uword num_nodes_per_turn = 180;	// number of nodes each turn

	// geometry specific per layer
	const arma::Row<rat::fltp> alpha = arma::Row<rat::fltp>{20.0,26.0}*2*arma::Datum<rat::fltp>::pi/360; // skew angle per two layers [rad]
	const arma::Row<arma::uword> is_horizontal = {0,1}; // specifies whether the coil generates horizontal field

	// conductor geometryde
	const arma::uword nd = 2; // number of wires in width of slot
	const arma::uword nw = 4; // number of wires in depth of slot
	const rat::fltp dstr = 0.825e-3; // thickness of bare wire [m]
	const rat::fltp ddstr = 1.0e-3; // space allocated for insulated wire [m]
	const rat::fltp fcu2sc = 1.9; // copper to superconductor fraction
	const rat::fltp dcable = nd*ddstr; // thickness of cable [m] (width of slot)
	const rat::fltp wcable = nw*ddstr; // width of cable [m] (depth of slot)
	const rat::fltp delta = 0.5e-3; // spacing between turns

	// scaling
	const rat::fltp scale_x = RAT_CONST(1.0);
	const rat::fltp scale_y = RAT_CONST(1.0)/scale_x;

	// operational settings per layer pair
	const arma::Row<rat::fltp> operating_current = {420,420}; // operating current for each circuit
	const arma::Row<rat::fltp> operating_temperature = {4.5,4.5};

	// background magnetic field
	//arma::Col<rat::fltp>::fixed<3> Hbg = {0,-1.0/arma::Datum<rat::fltp>::mu_0,0};
	arma::Col<rat::fltp>::fixed<3> Hbg = rat::cmn::Extra::null_vec();

	// GEOMETRY SETUP
	// construct conductor
	rat::mat::ShRutherfordCablePr nbti_cable = rat::mat::RutherfordCable::create();
	nbti_cable->set_strand(rat::mat::Database::nbti_wire_lhc(dstr,fcu2sc));
	nbti_cable->set_num_strands(nd*nw);
	nbti_cable->set_width(wcable);
	nbti_cable->set_thickness(dcable);
	nbti_cable->set_keystone(0);
	nbti_cable->set_pitch(0);
	nbti_cable->set_dinsu(0);
	nbti_cable->set_fcabling(1.0);
	nbti_cable->setup();

	// create cable cross section
	// note that the cable is centered at least
	// in the thickness direction
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(
		-dcable/2,dcable/2,0,wcable,dcable/nd,wcable/nw);

	// create bending transformation
	rat::mdl::ShTransBendPr bending = rat::mdl::TransBend::create(rat::cmn::Extra::unit_vec('y'), rat::cmn::Extra::unit_vec('x'), bending_radius);

	// create model that combines coils
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();

	// walk over layers
	for(arma::uword i=0;i<num_layers;i++){
		// calculate radius
		const rat::fltp coil_radius = aperture_radius + (i+1)*dformer + i*(wcable + dradial);
		const rat::fltp inner_radius = aperture_radius + dformer + (i/2)*2*(dformer + wcable + dradial);

		// inner radius parameters
		const rat::fltp omega = (delta + dcable)/std::sin(alpha(i/2));
		const rat::fltp aest = inner_radius/(num_poles*std::tan(alpha(i/2))); // skew amplitude
		const rat::fltp num_turns = 2*std::floor((ell_coil-2*aest)/(2*omega));

		// account for integer number of turns
		rat::fltp a = num_poles*(ell_coil - num_turns*omega)/2;
		if(i%2==1)a+=omega/(num_poles*4); else a+=omega*(num_poles*4-1)/(num_poles*4);

		// create path
		rat::mdl::ShPathCCTPr path_cct = rat::mdl::PathCCT::create(
			num_poles,coil_radius,a,omega,num_turns,num_nodes_per_turn);
		
		// slanting direction
		if(i%2==0)path_cct->set_is_reverse(is_reverse); // reverse slanting
		else path_cct->set_is_reverse(!is_reverse); // reverse slanting
			
		// set scaling
		path_cct->set_scale_x(scale_x);
		path_cct->set_scale_y(scale_y);

		// horizontal
		if(is_horizontal(i/2))path_cct->add_rotation(0,0,1,arma::Datum<rat::fltp>::pi/2);

		// add bending
		path_cct->add_transformation(bending);

		// create inner and outer coils
		rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(
			path_cct, cross_rect, nbti_cable);
		coil->set_name("layer" + std::to_string(i)); 
		coil->set_number_turns(nd*nw);
		coil->set_operating_current(operating_current(i/2));
		coil->set_operating_temperature(operating_temperature(i/2));

		// connect layer to circuit
		if((i/2)%2==0)coil->set_circuit_index(1);
		if((i/2)%2==1)coil->set_circuit_index(2);

		// add coil to model
		model->add_model(coil);
	}

	// axis of magnet for field quality calculation
	rat::mdl::ShPathAxisPr pth = rat::mdl::PathAxis::create('z','y',ell_coil+0.4,0,0,0,2e-3);
	pth->add_transformation(bending);


	// create drives
	const rat::mdl::ShDriveACPr drive1 = rat::mdl::DriveAC::create(operating_current(0), 0.1, 0); 
	const rat::mdl::ShDriveACPr drive2 = rat::mdl::DriveAC::create(operating_current(1), 0.1, arma::Datum<rat::fltp>::pi/2); 

	// create circuits
	const rat::mdl::ShCircuitPr circuit1 = rat::mdl::Circuit::create(1,drive1);
	const rat::mdl::ShCircuitPr circuit2 = rat::mdl::Circuit::create(2,drive2);

	// CALCULATION AND OUTPUT
	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create output time array settings
	const rat::fltp t1 = 0; const rat::fltp t2 = 10;
	const arma::uword num_times = 50; 
	const arma::Row<rat::fltp> output_times = arma::linspace<arma::Row<rat::fltp> >(t1,t2,num_times);

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model);
		mesh->add_circuit(circuit1); mesh->add_circuit(circuit2);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write(output_times,output_dir,lg);
	}

	// calculate field on axis
	if(run_line){
		// create mesh calculatoin
		const rat::mdl::ShCalcPathPr cpth = rat::mdl::CalcPath::create(model, pth);
		cpth->add_circuit(circuit1); cpth->add_circuit(circuit2);

		// calculate and write vtk output files to specified directory
		cpth->calculate_write(output_times,output_dir,lg);
	}

	// create grid calculation
	if(run_grid){
		// create grid calculation
		const rat::mdl::ShCalcGridPr grid = rat::mdl::CalcGrid::create(model,0.3,0.4,0.1,150,200,50);
		grid->add_circuit(circuit1); grid->add_circuit(circuit2);

		// calculate and write vtk output files to specified directory
		grid->calculate_write(output_times,output_dir,lg);
	}
	

	// harmonics calculation
	if(run_harmonics){
		// settings
		const rat::fltp reference_radius = aperture_radius*2.0/3.0;
		const bool compensate_curvature = true;

		// create calculation
		rat::mdl::ShCalcHarmonicsPr harmonics_calculator = rat::mdl::CalcHarmonics::create(model, pth, reference_radius, compensate_curvature);
		harmonics_calculator->add_circuit(circuit1); harmonics_calculator->add_circuit(circuit2);

		// calculate harmonics and get data
		harmonics_calculator->calculate_write(output_times,output_dir,lg);
	}

	// inductance calculation
	if(run_inductance){
		// create inductance calculation
		rat::mdl::ShCalcInductancePr inductance_calculator = rat::mdl::CalcInductance::create(model);
		inductance_calculator->add_circuit(circuit1); inductance_calculator->add_circuit(circuit2);

		// calculate and show matrix in terminal
		inductance_calculator->calculate_write(output_times,output_dir,lg);
	}

	// export json
	if(run_json_export){
		// create serializer
		rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model);

		// write to output file
		slzr->export_json(output_dir/"model.json");
	}

	// write a freecad macro
	if(run_freecad){
		model->export_freecad(rat::cmn::FreeCAD::create(output_dir/"freecad.FCMacro","cct"));
	}

	// export to an opera conductor file
	if(run_opera){
		model->export_opera(rat::cmn::Opera::create(output_dir/"opera.cond"));
	}

	// export json
	if(run_json_export){
		// write to output file
		rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model);
		slzr->export_json(output_dir/"model.json");
	}
}