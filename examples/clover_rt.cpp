// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>
#include <memory>

// header files for common
#include "rat/common/newtonraphson.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/freecad.hh"
#include "rat/common/log.hh"

// header files for distmesh
#include "rat/dmsh/dfrrectangle.hh"

// conductor
#include "rat/mat/database.hh"
#include "rat/mat/parallelconductor.hh"

// header files for Rat-Models
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "pathclover.hh"
#include "pathcable.hh"
#include "modelgroup.hh"
#include "cartesiangriddata.hh"
#include "calcharmonics.hh"
#include "transbend.hh"
#include "pathrectangle.hh"
#include "trackdata.hh"
#include "crosscircle.hh"
#include "emitterbeam.hh"
#include "crossdmsh.hh"
#include "calcmlfmm.hh"
#include "serializer.hh"
#include "calclength.hh"


// DESCRIPTION
// This example shows how to create a single layer 
// cloverleaf coil complemented by a racetrack coil.
// The example includes both magnet poles. Cloverleaf coils
// are probably a good solution for creating a dipole with high
// temperature superconducting (HTS) tape.

// shared pointer definition
typedef std::shared_ptr<class CloverRT> ShCloverRTPr;

// construct a cloverleaf and racetrack coil model
class CloverRT{
	// INPUT SETTINGS
	public:
		// filename
		boost::filesystem::path datdir_ = "./clover_rt/";

		// operating conditions
		rat::fltp Iop_ = 2000; // operating current [A]
		rat::fltp Top_ = 20; // operating temperature [K]

		// coil geometry
		rat::fltp ellstr1_ = 0.25; // straight section length [m]
		rat::fltp ellstr2_ = 0.080; // width of the coil [m]
		rat::fltp endspace_ = 0.020; // spacing between the cloverleaf and the racetrack [m]
		rat::fltp cl_num_turns_ = 80; // number of turns in the cloverleaf coil
		rat::fltp rt_num_turns_ = 170; // number of turns in the racetrack coil
		rat::fltp dplate_ = 8e-3; // thickness of the plate separating the coils [m]
		rat::fltp dmid_ = 2e-3; // thickness of the mid-plane [m]
		rat::fltp bending_radius_ = 0; // radius for small accelerators [m] (0 is disable bending)
		rat::fltp element_size_ = 2e-3; // size of the elements [m]
		rat::fltp bridge_angle_ = arma::Datum<rat::fltp>::pi*2*10.0/360; // bridge angle [rad]

		// cable settings
		arma::uword cl_Ntpca_ = 2; // number of tapes in the cable for clover
		arma::uword rt_Ntpca_ = 2; // number of tapes in the cable for racetrack
		rat::fltp dtape_ = 1e-4; // thickness of tape [m]
		rat::fltp wcable_ = 12e-3; // width of cable [m]
		rat::fltp dinsu_ = 0e-3/2; // thickness of insulation for each cable [m]
			
		// spline
		rat::fltp ell_trans_ = 0; // transition length [m]
		rat::fltp str12_ = 0.37*35e-3; // control point strength [m]
		rat::fltp str34_ = 0.37*14e-3; // control point strength [m]

		// winding parameters
		// these are only used when the coil is modelled 
		// as a cable (i.e. is_block==false);
		arma::uword cl_idx_incr_ = 1; // location at which the turn is incremented to the next
		arma::uword cl_idx_start_ = 5; // section in which the cable starts 
		arma::sword cl_num_add_ = -2; // number of extra sections to add/remove at end
		arma::uword rt_idx_incr_ = 1; // location at which the turn is incremented to the next
		arma::uword rt_idx_start_ = 3; // section in which the cable starts 
		arma::sword rt_num_add_ = 0; // number of extra sections to add/remove at end

		// required central field
		rat::fltp B1req_ = 1.7; // required central field for optimisation [Tm]

	// Methods
	public:
		// constructor
		CloverRT();
		static ShCloverRTPr create();
		
		// geometry
		rat::mdl::ShModelPr setup_geometry(const bool is_block) const;
		rat::mdl::ShPathGroupPr get_beampath() const;
		
		// harmonics calculation for optimization
		rat::mdl::ShHarmonicsDataPr calc_harmonics_core(const bool is_block, rat::cmn::ShLogPr lg = rat::cmn::NullLog::create()) const;
		
		// calculations
		void optimize_block(rat::cmn::ShLogPr lg = rat::cmn::NullLog::create());
		void optimize_cable(rat::cmn::ShLogPr lg = rat::cmn::NullLog::create());
		void calc_block_field(rat::cmn::ShLogPr lg = rat::cmn::NullLog::create()) const;
		void calc_cable_field(rat::cmn::ShLogPr lg = rat::cmn::NullLog::create()) const;
		void calc_grid(rat::cmn::ShLogPr lg = rat::cmn::NullLog::create()) const;
		void calc_harmonics(rat::cmn::ShLogPr lg = rat::cmn::NullLog::create()) const;
		void calc_length(rat::cmn::ShLogPr lg = rat::cmn::NullLog::create()) const;
		void export_freecad_cable(rat::cmn::ShLogPr lg = rat::cmn::NullLog::create()) const;
		void serialize(const std::string &fname) const;
};

// constructor
CloverRT::CloverRT(){
	
}

// factory
ShCloverRTPr CloverRT::create(){
	return std::make_shared<CloverRT>();
}

// geometry setup function
rat::mdl::ShModelPr CloverRT::setup_geometry(const bool is_block) const{
	// GEOMETRY SETUP	
	// calculate thickness of cables
	const rat::fltp cl_dcable = cl_Ntpca_*dtape_; // [m]
	const rat::fltp rt_dcable = rt_Ntpca_*dtape_; // [m]

	// calculate filling fraction
	rat::fltp cl_ffill = 1.0; if(is_block)cl_ffill = cl_dcable/(cl_dcable+2*dinsu_);
	rat::fltp rt_ffill = 1.0; if(is_block)rt_ffill = rt_dcable/(rt_dcable+2*dinsu_);

	// construct conductor
	rat::mat::ShConductorPr rebco_tape = rat::mat::Database::rebco_fujikura_tape_cern();
	rat::mat::ShParallelConductorPr cl_con = rat::mat::ParallelConductor::create();;
	cl_con->add_conductor(cl_ffill, rebco_tape);
	rat::mat::ShParallelConductorPr rt_con = rat::mat::ParallelConductor::create();;
	rt_con->add_conductor(rt_ffill, rebco_tape);

	// calculate remaining
	const rat::fltp cl_dpack = cl_num_turns_*(cl_dcable + 2*dinsu_); // thickness of cloverleaf [m]
	const rat::fltp rt_dpack = rt_num_turns_*(rt_dcable + 2*dinsu_); // thickness of racetrack [m]

	// create model
	rat::mdl::ShModelGroupPr pole1 = rat::mdl::ModelGroup::create(); pole1->set_name("p1");
	rat::mdl::ShModelGroupPr pole2 = rat::mdl::ModelGroup::create(); pole2->set_name("p2");
	pole2->add_rotation(1,0,0,arma::Datum<rat::fltp>::pi);
	pole2->add_reverse();

	// create cloverleaf coil
	// calculate parameters
	const rat::fltp height = wcable_ + dplate_;

	// create clover path
	rat::mdl::ShPathCloverPr path_clover = rat::mdl::PathClover::create(
		ellstr1_+2*cl_dpack,ellstr2_+2*cl_dpack,
		height,0,ell_trans_,str12_,str34_,element_size_);
	path_clover->set_bending_radius(bending_radius_);
	path_clover->add_translation(0,0,dmid_);
	path_clover->add_flip();
	//path_clover->add_reverse();
	path_clover->set_bridge_angle(bridge_angle_);

	// allocate coil
	rat::mdl::ShModelCoilPr coil_clover = NULL;

	// for block coil
	if(is_block){
		// create block cross section
		rat::mdl::ShCrossRectanglePr cross_clover = rat::mdl::CrossRectangle::create(0,cl_dpack,-dinsu_-wcable_,dinsu_,element_size_/2);

		// create block coil
		coil_clover = rat::mdl::ModelCoil::create(path_clover, cross_clover);
		coil_clover->set_number_turns(cl_num_turns_);
		//coil_clover->add_reverse();
	}

	// for cable coil
	else{
		// create cable
		rat::mdl::ShPathCablePr path_cable = rat::mdl::PathCable::create(path_clover);
		path_cable->set_turn_step(cl_dcable + 2*dinsu_);
		path_cable->set_num_turns(std::round(cl_num_turns_));
		path_cable->set_idx_incr(cl_idx_incr_);
		path_cable->set_idx_start(cl_idx_start_);
		path_cable->set_num_add(cl_num_add_);
		path_cable->set_offset(dinsu_);
		//path_cable->add_reverse();

		// create cable cross section
		rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(0,cl_dcable,-wcable_,0,element_size_/2);

		// create coil based on cable
		coil_clover = rat::mdl::ModelCoil::create(path_cable, cross_rect);
		coil_clover->set_enable_current_sharing(true);
	}
	
	// set circuit, coolant and conductor
	coil_clover->set_name("c1"); 
	coil_clover->set_operating_current(Iop_);
	coil_clover->set_operating_temperature(Top_);
	coil_clover->set_input_conductor(cl_con);
	coil_clover->add_flip();

	// add to model
	pole1->add_model(coil_clover); pole2->add_model(coil_clover);

	// create inner racetrack coil
	// coil parameters
	const rat::fltp radius = ellstr2_/2+cl_dpack-rt_dpack;

	// create racetrack path
	rat::mdl::ShPathRectanglePr path_racetrack = rat::mdl::PathRectangle::create(
		2*radius,ellstr1_-2*rt_dpack-2*endspace_,radius,element_size_);
	path_racetrack->set_offset(rt_dpack);
	path_racetrack->set_bending_radius(bending_radius_);
	path_racetrack->add_translation(0,0,dmid_+wcable_+dplate_);

	// allocate block coil
	rat::mdl::ShModelCoilPr coil_racetrack = NULL;

	// for block coil
	if(is_block){
		// create cross section
		rat::mdl::ShCrossRectanglePr cross_racetrack = rat::mdl::CrossRectangle::create(0,rt_dpack,-dinsu_,wcable_+dinsu_,element_size_/2);

		// create block coil
		coil_racetrack = rat::mdl::ModelCoil::create(path_racetrack, cross_racetrack);
		coil_racetrack->set_number_turns(rt_num_turns_); 
	}

	// for cable coil
	else{
		// create cable
		rat::mdl::ShPathCablePr path_cable = rat::mdl::PathCable::create(path_racetrack);
		path_cable->set_turn_step(rt_dcable + 2*dinsu_);
		path_cable->set_num_turns(std::round(rt_num_turns_));
		path_cable->set_idx_incr(rt_idx_incr_);
		path_cable->set_idx_start(rt_idx_start_);
		path_cable->set_num_add(rt_num_add_);
		path_cable->set_offset(dinsu_);

		// create cable cross section
		rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(0,rt_dcable,0,wcable_,element_size_/2);

		// create coil based on cable
		coil_racetrack = rat::mdl::ModelCoil::create(path_cable, cross_rect);
		coil_racetrack->set_enable_current_sharing(true);
	}

	// set circuit, coolant and conductor
	coil_racetrack->set_name("r1"); 
	coil_racetrack->set_operating_current(Iop_);
	coil_racetrack->set_operating_temperature(Top_);
	coil_racetrack->set_input_conductor(rt_con);

	// add to model
	pole1->add_model(coil_racetrack);
	pole2->add_model(coil_racetrack);

	// magnet lower pole
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create(); 
	model->add_model(pole1); 
	model->add_model(pole2);

	// return model
	return model;
}

// get beam path
rat::mdl::ShPathGroupPr CloverRT::get_beampath() const{
// axis of magnet for field quality calculation
	const rat::fltp ellpath = ellstr1_*2.0;
	rat::mdl::ShPathGroupPr path_beam = rat::mdl::PathGroup::create();
	path_beam->add_path(rat::mdl::PathStraight::create(ellpath,element_size_));
	path_beam->add_translation(0,-ellpath/2,0);

	// add bending
	path_beam->add_transformation(rat::mdl::TransBend::create(rat::cmn::Extra::unit_vec('z'), rat::cmn::Extra::unit_vec('x'), bending_radius_));

	// return path
	return path_beam;
}

// calculate harmonics along beampath
rat::mdl::ShHarmonicsDataPr CloverRT::calc_harmonics_core(const bool is_block, rat::cmn::ShLogPr lg) const{
	// settings
	const rat::fltp reference_radius = 10e-3;
	const bool compensate_curvature = true;

	// create model
	rat::mdl::ShModelPr model = setup_geometry(is_block);

	// create calculation
	rat::mdl::ShCalcHarmonicsPr harmonics_calculator = rat::mdl::CalcHarmonics::create(model, get_beampath(), reference_radius, compensate_curvature);

	// calculate harmonics and get data
	return harmonics_calculator->calculate_harmonics(RAT_CONST(0.0),lg);
}

// optimization of the block coil
void CloverRT::optimize_block(rat::cmn::ShLogPr lg){
	// create header
	lg->msg(2,"%s%s--- BLOCK OPTIMIZATION ---%s\n",KBLD,KGRN,KNRM);

	// set to block coil
	const bool is_block = true;

	// create a newton raphson solver
	rat::cmn::ShNewtonRaphsonPr nr = rat::cmn::NewtonRaphson::create();

	// create system function
	rat::cmn::NRSysFun sysfun = [&](const arma::Col<rat::fltp> &x){
		// set x
		ellstr2_ = x(0); cl_num_turns_ = 1e2*x(1); rt_num_turns_ = 1e2*x(2);

		// calculate harmonics
		rat::mdl::ShHarmonicsDataPr harm = calc_harmonics_core(is_block);

		// getting integrated harmonics
		arma::Row<rat::fltp> An,Bn;
		harm->get_harmonics(An,Bn);

		// return 
		return (arma::Col<rat::fltp>{Bn(1)+B1req_,Bn(3)*5,Bn(5)*10}).eval();
	};

	// set system function and start values
	nr->set_systemfun(sysfun);	
	nr->set_initial(arma::Col<rat::fltp>{ellstr2_, RAT_CONST(1e-2)*cl_num_turns_, RAT_CONST(1e-2)*rt_num_turns_});
	nr->set_delta(0.1e-4);
	nr->set_use_central_diff(false);
	nr->set_tolx(1e-12);
	nr->set_tolfun(1e-5);

	// use finite difference
	nr->set_finite_difference();

	// solve system
	nr->solve(lg);

	// get result and set to coils using the system function
	sysfun(nr->get_result());

	std::cout<<nr->get_result()<<std::endl;

	// done
	lg->msg(-2,"\n");
}

// optimization
void CloverRT::optimize_cable(rat::cmn::ShLogPr lg){
	// create header
	lg->msg(2,"%s%s--- CABLE OPTIMIZATION ---%s\n",KBLD,KGRN,KNRM);

	// set to cable based coil
	const bool is_block = false;
	cl_num_turns_ = std::round(cl_num_turns_);
	rt_num_turns_ = std::round(rt_num_turns_);

	// create a newton raphson solver
	rat::cmn::ShNewtonRaphsonPr nr = rat::cmn::NewtonRaphson::create();

	// create system function
	rat::cmn::NRSysFun sysfun = [&](const arma::Col<rat::fltp> &x){
		// set x
		ellstr2_ = x(0); dplate_ = x(1); dmid_ = x(2);

		// calculate harmonics
		rat::mdl::ShHarmonicsDataPr harm = calc_harmonics_core(is_block);

		// getting integrated harmonics
		arma::Row<rat::fltp> An,Bn;
		harm->get_harmonics(An,Bn);

		// return 
		return (arma::Col<rat::fltp>{Bn(1)+B1req_,Bn(3)*5,Bn(5)*10}).eval();
	};

	// set system function and start values
	nr->set_systemfun(sysfun);	
	nr->set_initial(arma::Col<rat::fltp>{ellstr2_, dplate_, dmid_});
	nr->set_delta(0.1e-4);
	nr->set_use_central_diff(false);
	nr->set_tolx(1e-12);
	nr->set_tolfun(1e-5);

	// use finite difference
	nr->set_finite_difference();

	// solve system
	nr->solve(lg);

	std::cout<<nr->get_result()<<std::endl;

	// get result and set to coils using the system function
	sysfun(nr->get_result());

	// done
	lg->msg(-2,"\n");
}

// calculate field on coil
void CloverRT::calc_block_field(rat::cmn::ShLogPr lg) const{
	// set to block
	const bool is_block = true;

	// get model
	rat::mdl::ShModelPr model = setup_geometry(is_block);

	// mlfmm calculation
	rat::mdl::ShCalcMlfmmPr mlfmm = rat::mdl::CalcMlfmm::create(model);
	mlfmm->set_target_meshes();
	mlfmm->calculate_write({0},datdir_,lg);
}

// calculate field on coil
void CloverRT::calc_cable_field(rat::cmn::ShLogPr lg) const{
	// set to cable
	const bool is_block = false;

	// get model
	rat::mdl::ShModelPr model = setup_geometry(is_block);

	// mlfmm calculation
	rat::mdl::ShCalcMlfmmPr mlfmm = rat::mdl::CalcMlfmm::create(model);
	mlfmm->set_target_meshes();
	mlfmm->calculate_write({0},datdir_,lg);
}

// calculate field on grid
void CloverRT::calc_grid(rat::cmn::ShLogPr lg) const{
	// set to cable
	const bool is_block = false;

	// get model
	rat::mdl::ShModelPr model = setup_geometry(is_block);

	// Create a grid of 140x160x50 points in the volume defined by intervals:
	// x=[-0.14,0.14], y=[-0.16,0.16], z=[-0.05,0.05];
	rat::mdl::ShCartesianGridDataPr grid = rat::mdl::CartesianGridData::create(-0.17,0.17,140, -0.3,0.3,200, -0.05,0.05,50);

	// mlfmm calculation
	rat::mdl::ShCalcMlfmmPr mlfmm = rat::mdl::CalcMlfmm::create(model);
	mlfmm->add_target(grid);
	mlfmm->calculate_write({0},datdir_,lg);
}

// harmonics calculation
void CloverRT::calc_harmonics(rat::cmn::ShLogPr lg) const{
	// set to cable
	const bool is_block = false;

	// create harmonic coil calculation
	rat::mdl::ShHarmonicsDataPr harm = calc_harmonics_core(is_block, lg);

	// display harmonics
	harm->display(lg);

	// write
	harm->export_vtk()->write(datdir_/"harmonics.vtt");
}

// conductor length calculation
void CloverRT::calc_length(rat::cmn::ShLogPr lg) const{
	// set to cable
	const bool is_block = false;

	// get model
	rat::mdl::ShModelPr model = setup_geometry(is_block);

	// perform length calculation
	const rat::fltp output_time = 0.0;
	const rat::mdl::ShCalcLengthPr length_calculator = rat::mdl::CalcLength::create(model);
	const rat::mdl::ShLengthDataPr length = length_calculator->calculate_length(output_time, lg);
	length->display(lg);

	// create meshes
	// rat::mdl::ShMeshPrList meshes = model->create_mesh();

	// number of turns
	// lg->msg(2,"%sResults:%s\n",KBLU,KNRM);
	// lg->msg(2,"%sNumber of Turns:%s\n",KBLU,KNRM);
	// lg->msg("Cloverleaf: %2.5f\n",cl_num_turns_);
	// lg->msg("Racetrack: %2.5f\n",rt_num_turns_);
	// lg->msg(-2,"\n");

	// // display length
	// lg->msg(2,"%sCalculated lengths:%s\n",KBLU,KNRM);
	// lg->msg("%s%10s %6s%s\n",KBLD,"name","length",KNRM);
	// for(arma::uword i=0;i<meshes.n_elem;i++)
	// 	lg->msg("%10s %2.6e\n",
	// 		meshes(i)->get_name().c_str(),
	// 		meshes(i)->calc_ell());
	// lg->msg(-2,"\n");
	
	// // end of results
	// lg->msg(-2,"\n");
}

// freecad export for the cable part
void CloverRT::export_freecad_cable(rat::cmn::ShLogPr lg) const{
	// create header
	lg->msg(2,"%s%s--- WRITING FREECAD ---%s\n",KBLD,KGRN,KNRM);

	// set to cable
	const bool is_block = true;

	// get model
	rat::mdl::ShModelPr model = setup_geometry(is_block);

	// write a freecad macro
	rat::cmn::ShFreeCADPr fc = rat::cmn::FreeCAD::create("clover_rt_cable.FCMacro","clover_cable");
	fc->set_num_sub(1);	model->export_freecad(fc);

	// done
	lg->msg(-2,"\n");
}

void CloverRT::serialize(const std::string &fname) const{
	// set to cable
	const bool is_block = false;

	// get model
	rat::mdl::ShModelPr model = setup_geometry(is_block);

	// write to output file
	rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create();
	slzr->flatten_tree(model); slzr->export_json(fname);
}

// main
int main(){
	// INPUT SETTINGS
	// switchboard for calculation types
	const bool run_block_optimisation = true; // optimize block coil
	const bool run_cable_optimisation = true; // optimize cable coil (after block)
	const bool run_coil_field = true; // calculate field on the coil
	const bool run_grid = true; // calculate field on a grid of points surrounding the coil
	const bool run_harmonics = true; // run harmonic coil calculation
	const bool run_freecad = true; // export the geometry to a python freecad macro
	const bool run_length = true;
	const bool run_json_export = true; // export geometry to json file

	// logger
	// the default log displays the status in the
	// terminal window in which the code is called
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create model
	ShCloverRTPr clrt = CloverRT::create();

	// optimize field quality
	if(run_block_optimisation)clrt->optimize_block(lg);	
	if(run_cable_optimisation)clrt->optimize_cable(lg);	

	// write to json
	if(run_json_export)clrt->serialize("clover_rt.json");	

	// Calculate field on coil mesh
	if(run_coil_field){
		// clrt->calc_block_field(lg);
		clrt->calc_cable_field(lg);
	}

	// calculate grid
	if(run_grid)clrt->calc_grid(lg);

	// calculate harmonics
	if(run_harmonics)clrt->calc_harmonics(lg);

	// calculate length
	if(run_length)clrt->calc_length(lg);

	// write a freecad macro
	if(run_freecad)clrt->export_freecad_cable();
}