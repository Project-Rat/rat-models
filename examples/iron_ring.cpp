// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for common
#include "rat/common/gmshfile.hh"

// header files for distmesh-cpp
#include "rat/dmsh/dfrrectangle.hh"
#include "rat/dmsh/dfdiff.hh"
#include "rat/dmsh/dfrectangle.hh"

// header files for models
#include "crossdmsh.hh"
#include "pathaxis.hh"
#include "calcmesh.hh"
#include "modelcoil.hh"
#include "modelnl.hh"
#include "serializer.hh"
#include "modelgroup.hh"
#include "crossrectangle.hh"
#include "modelsolenoid.hh"
#include "hbcurvevlv.hh"

// main
int main(){
	// INPUT SETTINGS
	// switch board
	const bool run_calc_mesh = true;
	const bool run_json_export = true;
	
	// output directory
	const boost::filesystem::path output_dir = "./iron_ring/";

	// settings
	const rat::fltp inner_radius = 0.02; 
	const rat::fltp dcoil = 0.01;
	const rat::fltp height = 0.04;
	const rat::fltp coil_element_size = 0.002;
	const rat::fltp iron_element_size = 0.005;
	const rat::fltp J = 400e6;
	const arma::uword num_sections = 4;
	

	// YOKE GEOMETRY SETUP
	// setup the distance function
	const rat::dm::ShDistFunPr df = rat::dm::DFDiff::create(
		rat::dm::DFCircle::create(0.12,0,0),
		rat::dm::DFCircle::create(0.1,0,0));

	// create cross section with distance function
	const rat::mdl::ShCrossDMshPr cdmsh = rat::mdl::CrossDMsh::create(iron_element_size,df,rat::dm::DFOnes::create());

	// create path
	const rat::mdl::ShPathAxisPr pth = rat::mdl::PathAxis::create('y','z',0.02,0,0,0,iron_element_size);

	// with non-linear solver
	#ifdef ENABLE_NL_SOLVER
		// create hb curve
		const rat::mdl::ShHBCurvePr hb_curve = rat::mdl::HBCurveVLV::create(500.0,1.5);

		// create yoke
		const rat::mdl::ShModelNLPr yoke = rat::mdl::ModelNL::create(pth,cdmsh,hb_curve);
		yoke->add_translation(0.11,0,0);
	
	// without non-linear solver
	#else
		// create modelcoil
		const rat::mdl::ShModelMeshPr yoke = rat::mdl::ModelMesh::create(pth,cdmsh);
		yoke->add_translation(0.11,0,0);
	#endif


	// COIL GEOMETRY SETUP'
	// create clover path
	const rat::mdl::ShModelSolenoidPr coil = rat::mdl::ModelSolenoid::create(
		inner_radius, dcoil, height, coil_element_size, num_sections);
	coil->set_use_current_density();
	coil->set_operating_current_density(J);


	// create a model to combine the yoke and the coil
	const rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create({yoke,coil});


	// CALCULATION
	// mesh calculation
	if(run_calc_mesh){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({0},output_dir,rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT));
	}

	// export json
	if(run_json_export){
		rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model);
		slzr->export_json(output_dir/"model.json");
	}
}