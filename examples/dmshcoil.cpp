// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for distmesh-cpp
#include "rat/dmsh/dfrrectangle.hh"
#include "rat/dmsh/dfdiff.hh"
#include "rat/dmsh/dfrectangle.hh"

// header files for FX-Models
#include "crossdmsh.hh"
#include "pathstraight.hh"
#include "modelcoil.hh"
#include "pathcircle.hh"
#include "calcmesh.hh"
#include "serializer.hh"

// DESCRIPTION
// Example showing how the 2D mesher can be used to 
// draw a mesh for a coil. This allows for making coils
// with irregular cross sections. Refer to distmesh-cpp 
// for more information on how to setup the mesh.

// main
int main(){
	// INPUT SETTINGS
	// switch board
	const bool run_calc_mesh = true;
	const bool run_json_export = true;
	
	// geometry and physics
	const rat::fltp operating_current = RAT_CONST(10000.0);

	// output directory
	const boost::filesystem::path output_dir = "./dmshcoil/";

	// settings
	const rat::fltp element_size = 2e-3; // size of the elements [m]
	const rat::fltp radius = 0.05; // radius of the coil [m]

	// GEOMETRY SETUP
	// distance function: subtract two rounded rectangles
	const rat::dm::ShDistFunPr df = rat::dm::DFDiff::create(
		rat::dm::DFRRectangle::create(-0.02,0.02,-0.01,0.01,2e-3),
		rat::dm::DFRRectangle::create(-0.01,0.01,-0.005,0.005,2e-3));
	
	// create cross section with distance function
	const rat::mdl::ShCrossDMshPr cdmsh = rat::mdl::CrossDMsh::create(element_size,df,rat::dm::DFOnes::create());

	// create path
	const rat::mdl::ShPathCirclePr pth = rat::mdl::PathCircle::create(radius,4,element_size);

	// create modelcoil
	const rat::mdl::ShModelCoilPr model = rat::mdl::ModelCoil::create(pth,cdmsh);
	model->set_operating_current(operating_current);

	// mesh calculation
	if(run_calc_mesh){
		// create mesh calculation
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({0},output_dir,rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT));
	}

	// export json
	if(run_json_export){
		rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model);
		slzr->export_json(output_dir/"model.json");
	}

}