// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>

// header files for models
#include "pathlemniscate.hh"
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "modelgroup.hh"
#include "calcmesh.hh"

// main
int main(){
	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = true; // calculate field on surface of coil
	
	// data storage directory and filename
	const boost::filesystem::path output_dir = "./lemniscate/";
	
	// output time
	const rat::fltp output_time = RAT_CONST(0.0);

	// coil geometry
	const rat::fltp a = RAT_CONST(0.05);
	const rat::fltp b = RAT_CONST(0.05);
	const rat::fltp c = RAT_CONST(0.08);
	const rat::fltp d = RAT_CONST(0.03);
	const rat::fltp lambda = RAT_CONST(8.0);
	const rat::fltp coil_thickness = RAT_CONST(1e-2);
	const rat::fltp coil_height = RAT_CONST(1e-2);
	const rat::fltp longitudinal_element_size = RAT_CONST(4e-3);
	const rat::fltp cross_element_size = RAT_CONST(1e-3);
	const rat::fltp num_turns = RAT_CONST(5.0);
	const rat::fltp operating_current = RAT_CONST(1000.0);
	const arma::uword num_sections = RAT_CONST(4llu);


	// GEOMETRY SETUP
	// path
	const rat::fltp offset = coil_thickness;
	const rat::mdl::ShPathLemniscatePr lemniscate_path = rat::mdl::PathLemniscate::create(a,b,c,d,lambda,longitudinal_element_size,offset,num_sections);

	// cross section
	const rat::mdl::ShCrossRectanglePr cross_section = rat::mdl::CrossRectangle::create(
		RAT_CONST(0.0), coil_thickness, -coil_height/2, coil_height/2, cross_element_size); 

	// create coil
	const rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(lemniscate_path, cross_section);
	coil->set_number_turns(num_turns);
	coil->set_operating_current(operating_current);
	coil->set_name("lemniscate coil");

	// create model
	const rat::mdl::ShModelGroupPr tree = rat::mdl::ModelGroup::create(coil);
	
	// CALCULATION AND OUTPUT
	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(tree);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({output_time},output_dir,lg);
	}

}