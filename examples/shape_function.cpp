// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for models
#include "rat/common/elements.hh"
#include "rat/mlfmm/mlfmm.hh"
#include "rat/mlfmm/settings.hh"
#include "rat/mlfmm/soleno.hh"

#include "modelsolenoid.hh"
#include "linedata.hh"
#include "pathaxis.hh"

// main
int main(){
	// settings
	const rat::fltp D = RAT_CONST(0.001); // box side length
	const arma::sword num_gauss = 4;
	const rat::fltp dxtrap = RAT_CONST(0e-3);
	const rat::fltp current = 1000.0;
	const rat::fltp inner_radius = 0.1;
	const rat::fltp dcoil = 0.01;
	const rat::fltp height = 0.1;
	const rat::fltp element_size = 5e-3;
	const rat::fltp time = 0.0;
	const arma::uword num_turns = 100;
	const arma::uword num_exp = 7;

	// solenoid
	rat::mdl::MeshSettings mesh_stngs;
	const rat::mdl::ShModelSolenoidPr slnd = 
		rat::mdl::ModelSolenoid::create(inner_radius,dcoil,height,element_size,4);
	const rat::mdl::ShMeshDataPr mesh = slnd->create_meshes({},mesh_stngs).front();
	const arma::Mat<rat::fltp>& Rn = mesh->get_nodes();
	const arma::Mat<arma::uword>& n = mesh->get_elements();

	// areas
	const arma::Row<rat::fltp> cis_areas = mesh->calc_cis_areas();
	const arma::Row<rat::fltp> cis_total_area = mesh->calc_cis_total_area(cis_areas);
	const arma::uword num_base = mesh->get_num_base_elements();

	// create gauss points
	const arma::Mat<rat::fltp> gp = rat::cmn::Hexahedron::create_gauss_points(num_gauss);
	const arma::Mat<rat::fltp> Rqh = gp.rows(0,2);
	const arma::Row<rat::fltp> wg = gp.row(3);
	
	// shape function derivative
	const arma::Mat<rat::fltp> N = rat::cmn::Hexahedron::shape_function_derivative(Rqh);
	const arma::Mat<rat::fltp> Vq = rat::cmn::Hexahedron::nedelec_hdiv_shape_function(Rqh);

	// allocate sources
	arma::Mat<rat::fltp> Rs(3,Rqh.n_cols*n.n_cols);
	arma::Mat<rat::fltp> Is(3,Rqh.n_cols*n.n_cols);

	// walk over elements
	for(arma::uword i=0;i<n.n_cols;i++){
		// get nodes
		const arma::Mat<rat::fltp>::fixed<3,8> Rnn = Rn.cols(n.col(i));

		// calculate jacobian and its inverse
		const arma::Mat<rat::fltp> J = rat::cmn::Hexahedron::shape_function_jacobian(Rnn,N);
		const arma::Row<rat::fltp> Jdet = rat::cmn::Hexahedron::jacobian2determinant(J);
		const arma::Mat<rat::fltp> Jinv = rat::cmn::Hexahedron::invert_jacobian(J,Jdet);

		// quad to cart functions for vector spaces
		const arma::Mat<rat::fltp> V = rat::cmn::Hexahedron::quad2cart_contravariant_piola(Vq, J, Jdet);
		// const arma::Mat<rat::fltp> V = rat::cmn::Hexahedron::quad2cart_covariant_piola(Vq, Jinv);

		// divide current over the cross sectional area
		const rat::fltp current1 = num_turns*current*cis_areas(i)/cis_total_area(i/num_base);
		const rat::fltp current2 = num_turns*current*cis_areas((i+num_base)%cis_areas.n_elem)/cis_total_area((i/num_base + 1)%cis_total_area.n_elem);

		// direction vector
		Rs.cols(i*Rqh.n_cols,(i+1)*Rqh.n_cols-1) = rat::cmn::Hexahedron::quad2cart(Rnn,Rqh);
		Is.cols(i*Rqh.n_cols,(i+1)*Rqh.n_cols-1) = (Jdet%wg)%(-current1*V.rows(4*3,(4+1)*3-1) + current2*V.rows(5*3,(5+1)*3-1)).eval().each_row();
	}

	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);
		
	// current sources
	const rat::fmm::ShCurrentSourcesPr src = rat::fmm::CurrentSources::create(Rs,Is,
		arma::Row<rat::fltp>(Rs.n_cols,arma::fill::ones),
		arma::Row<rat::fltp>(Rs.n_cols,arma::fill::zeros));
	src->set_van_Lanen(false); // point sources

	// target points
	const rat::fmm::ShMgnTargetsPr tar = rat::fmm::MgnTargets::create(Rs);
	tar->set_field_type('B',3);
	
	// settings for MLFMM
	const rat::fmm::ShSettingsPr stngs = rat::fmm::Settings::create();
	stngs->set_num_exp(num_exp);

	// multipole method
	const rat::fmm::ShMlfmmPr mlfmm = rat::fmm::Mlfmm::create(src,tar,stngs);
	mlfmm->setup(lg); mlfmm->calculate(lg);
		
	// get the field
	const arma::Mat<rat::fltp> B = tar->get_field('B');





	// get field
	// const arma::Mat<rat::fltp> Bfmm = line->get_field('B');

	// // target points
	// // calculate field on axis
	// const rat::mdl::ShLineDataPr line = rat::mdl::LineData::create(
	// 	rat::mdl::PathAxis::create('z','x',0.4,0.01,0,0,4e-3)->create_frame(rat::mdl::MeshSettings()));
	
	// // get target points
	// // const arma::Mat<rat::fltp> Rt = line->get_target_coords();

	// const rat::fmm::ShSettingsPr stngs = rat::fmm::Settings::create();
	// stngs->set_num_exp(num_exp);

	// // multipole method
	// const rat::fmm::ShMlfmmPr mlfmm = rat::fmm::Mlfmm::create(src,line,stngs);
	// mlfmm->setup(lg); mlfmm->calculate(lg);
	
	// // get field
	// const arma::Mat<rat::fltp> Bfmm = line->get_field('B');

	// // soleno calculation for comparison setup soleno calculation
	// const rat::fmm::ShSolenoPr sol = rat::fmm::Soleno::create();
	// sol->set_solenoid(inner_radius,inner_radius+dcoil,-height/2,height/2,current,num_turns,5);

	// // get target points
	// const arma::Mat<rat::fltp> Rt = line->get_target_coords();

	// // calculate at target points
	// const arma::Mat<rat::fltp> Bsol = sol->calc_B(Rt);

	

	// std::cout<<arma::join_vert(Bfmm,Bsol).t()<<std::endl;


	// std::cout<<arma::join_vert(Rc,current*V1.rows(4*3,(4+1)*3-1) - current*V1.rows(5*3,(5+1)*3-1))<<std::endl;
	// std::cout<<arma::join_vert(Rc,V2.rows(4*3,(4+1)*3-1) - V2.rows(5*3,(5+1)*3-1))<<std::endl;


	return 0;
}