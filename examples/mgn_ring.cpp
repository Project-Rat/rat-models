// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// DESCRIPTION
// Minimalist example of a solenoid model

// header files for common
#include "rat/common/log.hh"

// header files for model
#include "pathcircle.hh"
#include "crossrectangle.hh"
#include "modelbar.hh"
#include "calcgrid.hh"
#include "calcplane.hh"
#include "calcmesh.hh"
#include "cartesiangriddata.hh"
#include "pathcircle.hh"

// main function
int main(){
	// SETTINGS
	// switchboard
	const bool run_grid = true;
	const bool run_mesh = true;
	const bool run_plane = true;

	// output files
	const boost::filesystem::path output_dir = "./mgn_ring/";

	// output time
	const rat::fltp output_time = RAT_CONST(0.0);

	// model geometric input parameters
	const rat::fltp Rin = RAT_CONST(10e-3); // thickness of the bar [m]
	const rat::fltp Rout = RAT_CONST(20e-3);
	const rat::fltp ell = RAT_CONST(10e-3);
	const rat::fltp delem = RAT_CONST(1e-3); // element size [m]
	const arma::uword num_sections = 4;

	// model operating parameters
	const rat::fltp Mn = RAT_CONST(1.0)/arma::Datum<rat::fltp>::mu_0;;


	// GEOMETRY
	// create a circular path object
	rat::mdl::ShPathCirclePr circle = rat::mdl::PathCircle::create(Rin, num_sections, delem);
	circle->set_offset(Rout-Rin);
	
	// create a rectangular cross section object
	rat::mdl::ShCrossRectanglePr rectangle = rat::mdl::CrossRectangle::create(0, Rout-Rin, -ell/2, ell/2, delem);

	// create a coil object
	rat::mdl::ShModelBarPr bar = rat::mdl::ModelBar::create(circle, rectangle);
	bar->set_magnetisation({0,Mn,0}); // [longitudinal, normal, transverse]


	// CALCULATION AND OUTPUT
	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);


	// create mesh calculation
	if(run_mesh){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(bar);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({output_time},output_dir,lg);
	}

	// create grid calculation
	if(run_plane){
		// create grid calculation
		const rat::mdl::ShCalcPlanePr plane = rat::mdl::CalcPlane::create(bar,'y',0.1,0.1,800,800);

		// calculate and write vtk output files to specified directory
		plane->calculate_write({output_time},output_dir,lg);
	}

	// create grid calculation
	if(run_grid){
		// settings
		const rat::fltp grid_size = RAT_CONST(0.1);
		const arma::uword grid_num_steps = 150;

		// create grid calculation
		const rat::mdl::ShCalcGridPr grid = rat::mdl::CalcGrid::create(
			bar,grid_size,grid_size,grid_size,grid_num_steps,grid_num_steps,grid_num_steps);

		// calculate and write vtk output files to specified directory
		grid->calculate_write({output_time},output_dir,lg);
	}


}
