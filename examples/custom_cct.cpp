// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// DESCRIPTION
// In this example the custom CCT class is used to 
// create a combined function magnet with both
// dipole and quadrupole components.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// command line parser
#include <tclap/CmdLine.h>

// header for common
#include "rat/common/log.hh"

// materials headers
#include "rat/mat/database.hh"
#include "rat/mat/rutherfordcable.hh"

// header files for distmesh-cpp
#include "rat/dmsh/dfrrectangle.hh"
#include "rat/dmsh/dfdiff.hh"
#include "rat/dmsh/dfrectangle.hh"

// header files for Models
#include "driveinterp.hh"
#include "driveac.hh"
#include "pathcctcustom.hh"
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "modelgroup.hh"
#include "transbend.hh"
#include "cartesiangriddata.hh"
#include "serializer.hh"
#include "calcmesh.hh"
#include "calcharmonics.hh"
#include "calcgrid.hh"
#include "modelroot.hh"
#include "calcgroup.hh"
#include "calcpolargrid.hh"
#include "version.hh"
#include "crossdmsh.hh"
#include "hbcurvevlv.hh"
#include "modelnl.hh"

// main
int main(int argc, char * argv[]){
	// COMMAND LINE INPUT
	// create tclap object
	TCLAP::CmdLine cmd("Cos-Theta Coil-End Example",' ',rat::cmn::Extra::create_version_tag(
		MAT_PROJECT_VER_MAJOR,MAT_PROJECT_VER_MINOR,MAT_PROJECT_VER_PATCH));

	// filename input argument
	TCLAP::UnlabeledValueArg<std::string> output_path_argument(
		"od","Output dir",false,"./custom_cct/", "string", cmd);

	// parse the command line arguments
	cmd.parse(argc, argv);

	// data storage directory and filename
	const boost::filesystem::path output_dir = output_path_argument.getValue();


	// INPUT SETTINGS
	// calculation settings
	const bool enable_yoke = true; // enable yoke if nlsolver available
	const bool run_coil_field = true;
	const bool run_harmonics = true;
	const bool run_polar_grid = true;
	const bool run_json_export = true;

	// output time, has no effect unless current drives are time dependent
	const rat::fltp output_time = RAT_CONST(0.0);

	// geometry settings
	const rat::fltp bend_radius = RAT_CONST(0.15); // [m] bend radius of the magnet (set 0 for no bending)
	const rat::fltp nt = RAT_CONST(40.0); // [#] number of turns
	const rat::fltp radius = RAT_CONST(0.025); // [m] inner coil radius
	const rat::fltp dradius = RAT_CONST(7e-3); // [m] increment of radius
	const rat::fltp omega = RAT_CONST(6e-3); // [m] winding pitch 
	const arma::uword nnpt = 120llu; // [#] number of nodes per turn (determines discretization)
	const arma::uword num_layer = 2llu; // [#] number of layers (should be even for good CCT)

	// strength of the different components
	const rat::fltp dipole_amplitude = RAT_CONST(30e-3); // [m] amplitude of the dipole winding oscillations
	const rat::fltp quad_amplitude = RAT_CONST(7e-3); // [m] amplitude of the quadrupole winding oscillations
	const rat::fltp quad_offset = RAT_CONST(2.2e-3); // [m] amplitude of the quadrupole winding

	// operating conditions
	const rat::fltp operating_current = RAT_CONST(480.0); // [A] operating current
	const rat::fltp operating_temperature = RAT_CONST(4.5); // [K] operating temperature

	// conductor settings
	const rat::fltp dstr = RAT_CONST(0.825e-3); // [m] strand diameter 
	const rat::fltp ddstr = RAT_CONST(1.0e-3); // [m] strand diameter incl insulation 
	const arma::uword nd = 2llu; // [#] number of wires across cable thickness
	const arma::uword nw = 5llu; // [#] number of wires across cable width
	const rat::fltp fcu2sc = RAT_CONST(1.9); // copper to superconductor ratio fcu = fcu2sc/(1.0 + fcu2sc), fsc = 1.0/(1.0 + fcu2sc)
	const rat::fltp element_size = RAT_CONST(2e-3); // size of the elements [m]

	// yoke settings
	#ifdef ENABLE_NL_SOLVER
	const rat::fltp yoke_inner_radius = radius + num_layer*dradius;
	const rat::fltp yoke_outer_radius = yoke_inner_radius + 2e-2;
	const rat::fltp yoke_element_size = 8e-3;
	const rat::fltp yoke_ell = 5e-2;
	#endif



	// GEOMETRY SETUP
	// create model tree
	// although it is possible to export the model 
	// directly to json and open it in the GUI
	// the model tree allows for inclusion of the calculations
	const rat::mdl::ShModelGroupPr tree = rat::mdl::ModelGroup::create();
	tree->set_name("Model Tree"); tree->set_tree_open();
	const rat::mdl::ShCalcGroupPr calc = rat::mdl::CalcGroup::create();
	calc->set_name("Calculation Tree"); calc->set_tree_open();
	const rat::mdl::ShModelRootPr root = rat::mdl::ModelRoot::create(tree,calc);
	root->set_name("Custom CCT");

	// calculate cable dimensions
	const arma::uword num_strands = nd*nw;
	const rat::fltp dcable = nd*ddstr; // [m] thickness of cable  (slot width)
	const rat::fltp wcable = nw*ddstr; // [m] width of cable  (slot depth)

	// create custom CCT path without harmonics this 
	// is a nested spiral canceling its own field
	const rat::mdl::ShPathCCTCustomPr path_cct = rat::mdl::PathCCTCustom::create();
	path_cct->set_range(-nt/2,nt/2); // number of turns are centered around zero
	path_cct->set_rho(radius);
	path_cct->set_num_nodes_per_turn(nnpt); 
	path_cct->set_num_layers(num_layer);
	path_cct->set_rho_increment(dradius);

	// set winding pitch
	path_cct->set_omega(omega); // a constant pitch along the length of the magnet (can also set a drive)
	// path_cct->set_use_normal_omega(); // when this is enabled omega is calulated locally based on the spacing between the turns in the normal direction

	// add dipole along the full length of the magnet. 
	// We are using the interpolation drive here
	// but effectively we are setting a constant value
	const rat::mdl::ShCCTHarmonicPr dipole = rat::mdl::CCTHarmonicDrive::create(
		1,false,rat::mdl::DriveInterp::create({-nt/2,nt/2}, {-dipole_amplitude,-dipole_amplitude}), false);
	dipole->set_normalize_length(false); // when enabled the magnet is mapped to an interval of [-0.5, 0.5] otherwise the turns are used directly
	path_cct->add_harmonic(dipole);

	// add quadrupole changing from minus 
	// to plus along the length of the magnet
	const rat::mdl::ShCCTHarmonicPr quadrupole = rat::mdl::CCTHarmonicDrive::create(
		2,false,rat::mdl::DriveAC::create(quad_amplitude,1,0.0,quad_offset),false); // note the frequency of 1 here, because normalize length is enabled this means one full cycle per coil
	quadrupole->set_normalize_length(true); // when enabled the magnet is mapped to an interval of [-0.5, 0.5] otherwise the turns are used directly
	path_cct->add_harmonic(quadrupole);

	// bending of the CCT path is done internally
	// this ensures that the spacing of the turns on the
	// inner radius of the bend is maintained constant
	if(bend_radius!=0){
		path_cct->set_use_radius();
		path_cct->set_bending_radius(bend_radius);
	}

	// create path along axis of the coil
	const rat::mdl::ShPathAxisPr pth_axis = rat::mdl::PathAxis::create('z','y',0.5,0,0,0,1e-3);

	// add bending to the path and axis
	if(bend_radius!=0)pth_axis->add_transformation(
		rat::mdl::TransBend::create(rat::cmn::Extra::unit_vec('y'), rat::cmn::Extra::unit_vec('x'), bend_radius));

	// create cross section
	const rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(
		-dcable/2,dcable/2,0,wcable,element_size/2);

	// construct conductor
	const rat::mat::ShRutherfordCablePr nbti_cable = rat::mat::RutherfordCable::create();
	nbti_cable->set_strand(rat::mat::Database::nbti_wire_lhc(dstr,fcu2sc));
	nbti_cable->set_num_strands(nd*nw);
	nbti_cable->set_width(wcable);
	nbti_cable->set_thickness(dcable);
	nbti_cable->set_keystone(0);
	nbti_cable->set_pitch(0);
	nbti_cable->set_dinsu(0);
	nbti_cable->set_fcabling(1.0);
	nbti_cable->setup();

	// create the coil
	rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(path_cct, cross_rect, nbti_cable);
	coil->set_name("Custom CCT");
	coil->set_number_turns(num_strands);
	coil->set_operating_current(operating_current);
	coil->set_operating_temperature(operating_temperature);
	tree->add_model(coil);

	// create a simple iron yoke
	#ifdef ENABLE_NL_SOLVER
	if(enable_yoke){
		// setup the distance function
		const rat::dm::ShDistFunPr df = rat::dm::DFDiff::create(
			rat::dm::DFCircle::create(yoke_outer_radius,0,0),
			rat::dm::DFCircle::create(yoke_inner_radius,0,0));

		// create cross section with distance function
		const rat::mdl::ShCrossDMshPr cdmsh = rat::mdl::CrossDMsh::create(yoke_element_size,df,rat::dm::DFOnes::create());

		// create path
		const rat::mdl::ShPathAxisPr pth = rat::mdl::PathAxis::create('z','y',yoke_ell,0,0,0,2*yoke_element_size);

		// add bending to the path and axis
		if(bend_radius!=0)pth->add_transformation(
			rat::mdl::TransBend::create(rat::cmn::Extra::unit_vec('y'), rat::cmn::Extra::unit_vec('x'), bend_radius));

		// create hb curve
		const rat::mdl::ShHBCurvePr hb_curve = rat::mdl::HBCurveVLV::create(500.0,2.15);

		// create modelcoil
		const rat::mdl::ShModelNLPr yoke = rat::mdl::ModelNL::create(pth,cdmsh,hb_curve);

		// add the yoke
		tree->add_model(yoke);
	}
	#endif


	// CALCULATION AND OUTPUT
	// the default log displays the status in the
	// terminal window in which the code is called
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create a cache for solve storage
	// this speeds up consecutive non-linear calculations
	// by taking the previous solution from the cache
	const rat::mdl::ShSolverCachePr cache = rat::mdl::SolverCache::create();

	// mesh calculation
	if(run_coil_field){
		// create calculation
		const rat::mdl::ShCalcMeshPr mesh_calculator = rat::mdl::CalcMesh::create(tree);
		calc->add_calculation(mesh_calculator);

		// calculate harmonics and get data
		mesh_calculator->calculate_write({output_time},output_dir,lg,cache);
	}

	// calculate harmonics
	if(run_harmonics){
		// settings
		const rat::fltp reference_radius = 2*radius/3;

		// calculates harmonics taking into account the thickness of the slice
		const bool compensate_curvature = true; 

		// create calculation
		const rat::mdl::ShCalcHarmonicsPr harmonics_calculator = rat::mdl::CalcHarmonics::create(tree, pth_axis, reference_radius, compensate_curvature);
		calc->add_calculation(harmonics_calculator);

		// calculate harmonics and get data
		harmonics_calculator->calculate_write({output_time},output_dir,lg,cache);
	}

	// polar grid calculation
	if(run_polar_grid){
		// calculate outer radius
		const rat::fltp coil_outer_radius = radius+(num_layer*dradius)+bend_radius/5;

		// create calculation
		const rat::mdl::ShCalcPolarGridPr polar_grid_calculator = rat::mdl::CalcPolarGrid::create(tree,'y',
			bend_radius-coil_outer_radius, bend_radius+coil_outer_radius,60llu,
			-arma::Datum<rat::fltp>::pi/2,arma::Datum<rat::fltp>::pi/2,180llu,
			-radius-coil_outer_radius, radius+coil_outer_radius,60llu,
			{-bend_radius,0,0});
		calc->add_calculation(polar_grid_calculator);

		// calculate harmonics and get data
		polar_grid_calculator->calculate_write({output_time},output_dir,lg,cache);
	}

	// export json
	if(run_json_export){
		// create serializer
		const rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(root);

		// write to output file
		slzr->export_json(output_dir/"model.json");
	}

	// done
	return 0;
}