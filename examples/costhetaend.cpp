// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// command line parser
#include <tclap/CmdLine.h>

// rat-models header files
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "modelgroup.hh"
#include "modelarray.hh"
#include "pathsuperellipse.hh"
#include "pathgroup.hh"
#include "calcmesh.hh"
#include "version.hh"

// description:
// demonstration of a cosine theta coil end.


// main
int main(int argc, char * argv[]){

	// COMMAND LINE INPUT
	// create tclap object
	TCLAP::CmdLine cmd("Cos-Theta Coil-End Example",' ',rat::cmn::Extra::create_version_tag(
		MAT_PROJECT_VER_MAJOR,MAT_PROJECT_VER_MINOR,MAT_PROJECT_VER_PATCH));

	// filename input argument
	TCLAP::UnlabeledValueArg<std::string> output_path_argument(
		"od","Output dir",false,"./costhetaend/", "string", cmd);

	// parse the command line arguments
	cmd.parse(argc, argv);


	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = true; // calculate field on surface of coil
	const bool run_json_export = true; // export geometry to json file

	// data storage directory and filename
	const boost::filesystem::path output_dir = output_path_argument.getValue();

	// settings
	const rat::fltp radius = 0.025;
	const rat::fltp ba = 0.02;
	const rat::fltp phi = 0.0;
	const rat::fltp arclength = arma::Datum<rat::fltp>::pi/2;
	const rat::fltp element_size = 1e-3;
	const rat::fltp beta = 0.0; // angle between legs
	const rat::fltp dcable = 1e-3;
	const rat::fltp wcable = 4e-3;
	const arma::uword num_turns_internal = 1;
	const arma::uword n1 = 3; // order of sides
	const arma::uword n2 = 2; // order of end
	const rat::fltp operating_current = 250.0;
	const rat::fltp operating_temperature = 20.0;
	const arma::uword num_poles = 1llu;

	// GEOMETRY
	// const perimeter end
	const rat::mdl::ShPathSuperEllipsePr ellipse1 = rat::mdl::PathSuperEllipse::create(
		radius, ba, beta, phi, arclength, element_size, num_poles, n1, n2);
	const rat::mdl::ShPathSuperEllipsePr ellipse2 = rat::mdl::PathSuperEllipse::create(
		radius, ba, beta, phi, arclength, element_size, num_poles, n1, n2);
	
	// transformations for second half
	ellipse2->add_reflect_xz(); ellipse2->add_reverse();

	// combine paths into a group
	const rat::mdl::ShPathGroupPr group = rat::mdl::PathGroup::create();
	group->add_path(ellipse1); group->add_path(ellipse2);

	// create a rectangular cross section
	// the cable is centered around the origin
	const rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(
		-dcable/2,dcable/2,-wcable/2,wcable/2,element_size);

	// create inner and outer coils
	const rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(group, cross_rect);
	coil->set_name("cos-theta end"); 
	coil->set_number_turns(num_turns_internal);
	coil->set_operating_current(operating_current);
	coil->set_operating_temperature(operating_temperature);

	// create model that combines coils
	const rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create(coil);


	// CALCULATION AND OUTPUT
	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(rat::mdl::ModelGroup::create(model));

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({RAT_CONST(0.0)},output_dir,lg);
	}
	
	// export json
	if(run_json_export){
		const rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model);
		slzr->export_json(output_dir/"model.json");
	}
}
