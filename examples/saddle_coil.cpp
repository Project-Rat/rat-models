// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// materials headers
#include "rat/mat/database.hh"
#include "rat/mat/rutherfordcable.hh"

// header files for common
#include "rat/common/log.hh"
#include "rat/common/serializer.hh"
#include "rat/common/freecad.hh"
#include "rat/common/gmshfile.hh"

// header files for models
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "modelmirror.hh"
#include "pathcable.hh"
#include "pathrectangle.hh"
#include "transbend.hh"
#include "calcmesh.hh"
#include "modelroot.hh"
#include "calcgroup.hh"
#include "modelgroup.hh"
#include "calcquench0d.hh"
#include "protectioncircuit.hh"

// DESCRIPTION
// This example shows how to create a simple saddle coil

// main
int main(){
	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = false; // calculate field on surface of coil
	const bool run_freecad = false; // export model to freecad
	const bool run_json_export = false; // export geometry to json file
	const bool run_quench_model = true; // run quench model

	// data storage directory and filename
	const boost::filesystem::path output_dir = "./saddle/";
	
	// output time
	const rat::fltp output_time = RAT_CONST(0.0);

	// coil geometry
	const arma::uword num_turns = 24llu; // number of turns in the coil
	const rat::fltp strand_diameter = RAT_CONST(0.833e-3); // [m] strand diameter 
	const rat::fltp width = RAT_CONST(0.25); // [m] width of the coil  (before bending)
	const rat::fltp height = RAT_CONST(0.4); // [m] height of the coil  (before bending)
	const rat::fltp radius = RAT_CONST(0.1); // [m] cylinder radius on which the coil is bend 
	const rat::fltp element_size = RAT_CONST(4e-3); // [m] size of the elements  
	const rat::fltp bend_radius = RAT_CONST(0.12);

	// cable settings
	const arma::uword num_strand_width = 16llu; // number of strand in the cable width
	const arma::uword num_strand_thickness = 2llu; // number of strands in the cable thickness
	const rat::fltp wcable = num_strand_width*strand_diameter; // [m] width of the cable 
	const rat::fltp dcable = num_strand_thickness*strand_diameter; // [m] thickness of the cable 
	const rat::fltp dinsu = RAT_CONST(0.1e-3); // [m] thickness of the insulation around each cable 

	// material properties
	const rat::fltp fcu2sc = RAT_CONST(1.9); // copper to superconductor fraction
	const rat::fltp RRR = RAT_CONST(100.0); // triple-RRR of the copper stabilizer

	// operating conditions
	const rat::fltp operating_current = RAT_CONST(8000); // [A] current in coil
	const rat::fltp operating_temperature = RAT_CONST(1.9); // [K] temperature at coil 
	const rat::fltp protection_voltage = RAT_CONST(10.0); // [V]
	const rat::fltp heater_delay_time = RAT_CONST(40e-3);

	// GEOMETRY SETUP
	// create unified path
	rat::mdl::ShPathRectanglePr path_rect = rat::mdl::PathRectangle::create(width,height,radius,element_size);

	// add bending
	path_rect->add_transformation(rat::mdl::TransBend::create(rat::cmn::Extra::unit_vec('y'), rat::cmn::Extra::unit_vec('z'), bend_radius));

	// create cable
	rat::mdl::ShPathCablePr path_cable = rat::mdl::PathCable::create(path_rect);
	path_cable->set_turn_step(dcable + 2*dinsu);
	path_cable->set_num_turns(num_turns);
	path_cable->set_idx_incr(1); // increment in the corner
	path_cable->set_idx_start(0);
	
	// create cross section
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(0,dcable,-wcable/2,wcable/2,1.2e-3);

	// create coil
	rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(path_cable, cross_rect);
	coil->set_name("Saddle");
	coil->add_translation({0,0,bend_radius});
	coil->set_operating_current(operating_current);
	coil->set_operating_temperature(operating_temperature);
	coil->set_circuit_index(1);

	// construct conductor
	rat::mat::ShRutherfordCablePr nbti_cable = rat::mat::RutherfordCable::create();
	nbti_cable->set_strand(rat::mat::Database::nbti_wire_lhc(strand_diameter,fcu2sc,RRR));
	nbti_cable->set_num_strands(num_strand_width*num_strand_thickness);
	nbti_cable->set_width(wcable);
	nbti_cable->set_thickness(dcable);
	nbti_cable->set_keystone(0);
	nbti_cable->set_pitch(0);
	nbti_cable->set_dinsu(0);
	nbti_cable->set_fcabling(1.0);
	nbti_cable->set_name("NbTi Cable");
	nbti_cable->set_classification("ltscables");
	nbti_cable->setup();
	coil->set_input_conductor(nbti_cable);

	// create mirror
	const rat::mdl::ShModelMirrorPr mirror = rat::mdl::ModelMirror::create(coil);


	// create model tree
	const rat::mdl::ShModelGroupPr tree = rat::mdl::ModelGroup::create(mirror);
	tree->set_name("Model Tree"); tree->set_tree_open();
	const rat::mdl::ShCalcGroupPr calc = rat::mdl::CalcGroup::create();
	calc->set_name("Calculation Tree"); calc->set_tree_open();
	const rat::mdl::ShModelRootPr root = rat::mdl::ModelRoot::create(tree,calc);
	root->set_name("Saddle Coil");

	
	// CALCULATION AND OUTPUT
	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(tree);
		calc->add_calculation(mesh);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({output_time},output_dir,lg);
	}

	// quench model
	if(run_quench_model){
		// create quench calculator
		const rat::mdl::ShCalcQuench0DPr adiabatic_quench = rat::mdl::CalcQuench0D::create(tree);
		calc->add_calculation(adiabatic_quench);

		// create a circuit
		const rat::mdl::ShProtectionCircuitPr circuit = rat::mdl::ProtectionCircuit::create();
		circuit->set_circuit_id(1);
		circuit->set_current_drive(rat::mdl::DriveDC::create(operating_current));
		circuit->set_protection_current(operating_current);
		circuit->set_protection_voltage(protection_voltage);
		circuit->set_heater_delay_time(heater_delay_time);
		circuit->set_use_varistor(false);
		adiabatic_quench->add_circuit(circuit);

		// run quench calculation
		adiabatic_quench->calculate_write({output_time},output_dir,lg);
	}


	// write a freecad macro
	if(run_freecad){
		mirror->export_freecad(rat::cmn::FreeCAD::create("saddle.FCMacro","saddle"));
	}

	// export json
	if(run_json_export){
		// write to output file
		rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create();
		slzr->flatten_tree(root); slzr->export_json(output_dir/"model.json");
	}

}