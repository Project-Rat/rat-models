// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files from Common
#include "rat/common/log.hh"

// header files for Models
#include "modelcoil.hh"
#include "modelgroup.hh"
#include "pathaxis.hh"
#include "pathccttable.hh"
#include "crossrectangle.hh"
#include "calcmesh.hh"
#include "calcharmonics.hh"

// main
int main(){
	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = true; // calculate field in volume of mesh
	const bool run_harmonics = true; // calculate harmonics along aperture
		
	// input path
	const boost::filesystem::path output_dir("./ccttable");
	const boost::filesystem::path pth("../data/CCTTables/QC1L1_input.csv");

	// conductor geometryde
	const arma::uword nd = 2; // number of wires in width of slot
	const arma::uword nw = 4; // number of wires in depth of slot
	const rat::fltp ddstr = 1.0e-3; // space allocated for insulated wire [m]
	const rat::fltp dcable = nd*ddstr; // thickness of cable [m] (width of slot)
	const rat::fltp wcable = nw*ddstr; // width of cable [m] (depth of slot)
	const rat::fltp ell_coil = 1.0;
	

	// calculation settings
	const rat::fltp output_time = 0.0;
	const bool compensate_curvature = false;
	const rat::fltp reference_radius = 10e-3;

	// create a path
	const rat::mdl::ShPathPr cct_path = rat::mdl::PathCCTTable::create(pth);

	// create cable cross section
	// note that the cable is centered at least
	// in the thickness direction
	const rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(
		-dcable/2,dcable/2,0,wcable,dcable/nd,wcable/nw);

	// create coil
	const rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(cct_path, cross_rect);
	coil->set_operating_current(400);
	coil->set_number_turns(nd*nw);

	// create model
	const rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create(coil);

	// axis
	const rat::mdl::ShPathAxisPr axis = rat::mdl::PathAxis::create('z','y',ell_coil+0.4,0,0,0,2e-3);

	// creat log
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({output_time},output_dir,lg);
	}

	// harmonics calculation
	if(run_harmonics){
		// create calculation
		const rat::mdl::ShCalcHarmonicsPr harmonics_calculator = 
			rat::mdl::CalcHarmonics::create(model, axis, reference_radius, compensate_curvature);

		// calculate harmonics and get data
		harmonics_calculator->calculate_write({output_time},output_dir,lg);
	}
}