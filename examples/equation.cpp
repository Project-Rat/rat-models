// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for models
#include "patheq.hh"
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "calcmesh.hh"

// main
int main(){
	// settings
	const rat::fltp operating_current = RAT_CONST(1000.0);
	const boost::filesystem::path output_dir = "./equation";

	// create path from equations
	const rat::mdl::ShPathEqPr path = rat::mdl::PathEq::create(
		"x", "linspace(0[s],10[s],5*181)", 
		"0.05[m]*sin(x*2*pi[rad/s])", 
		"0.05[m]*cos(x*2*pi[rad/s])", 
		"x*0.01*[m/s] + 0.03[m]*sin(2*x*2*pi[rad/s])");

	// create cross section
	const rat::mdl::ShCrossRectanglePr crss = rat::mdl::CrossRectangle::create(
		-1e-3,1e-3,0,5e-3,1e-3);

	// create coil
	const rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(path,crss);
	coil->set_operating_current(operating_current);

	// calculate mesh
	const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(coil);

	// logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// perform mesh calculation
	mesh->calculate_write({0.0},output_dir,lg);
}