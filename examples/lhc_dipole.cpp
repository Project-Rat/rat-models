// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// command line parser
#include <tclap/CmdLine.h>

// distmesh
#include "rat/dmsh/dfcircle.hh"
#include "rat/dmsh/dfdiff.hh"
#include "rat/dmsh/dfunion.hh"
#include "rat/dmsh/dfpolygon.hh"
#include "rat/dmsh/dfrectangle.hh"
#include "rat/dmsh/dfscale.hh"
#include "rat/dmsh/dfarrayangular.hh"

// rat-common headers
#include "rat/common/log.hh"

// rat-models header files
#include "version.hh"
#include "pathcostheta.hh"
#include "crosswedge.hh"
#include "modelcoil.hh"
#include "modelgroup.hh"
#include "modeltoroid.hh"
#include "calcmesh.hh"
#include "modelcylinder.hh"
#include "pathattach.hh"
#include "pathstraight.hh"
#include "crossdmsh.hh"
#include "pathaxis.hh"
#include "hbcurvevlv.hh"
#include "modelnl.hh"

// NOTE
// note this is just an approximation based on info I have been able to find from literature
// I was not able to find all necessary information so some guess work was unavoidable

// main
int main(int argc, char * argv[]){

	// COMMAND LINE INPUT
	// create tclap object
	TCLAP::CmdLine cmd("Cos-Theta Coil-End Example",' ',rat::cmn::Extra::create_version_tag(
		MAT_PROJECT_VER_MAJOR,MAT_PROJECT_VER_MINOR,MAT_PROJECT_VER_PATCH));

	// filename input argument
	TCLAP::UnlabeledValueArg<std::string> output_path_argument(
		"od","Output dir",false,"./lhc_dipole/", "string", cmd);

	// parse the command line arguments
	cmd.parse(argc, argv);

	// data storage directory and filename
	const boost::filesystem::path output_dir = output_path_argument.getValue();


	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = true; // calculate field on surface of coil
	const bool run_json_export = true; // export geometry to json file

	// settings
	const rat::fltp lead_length = RAT_CONST(0.15);
	const rat::fltp element_size = RAT_CONST(4e-3);
	const rat::fltp yoke_element_size = RAT_CONST(8.0e-3);
	const rat::fltp operating_current = RAT_CONST(11000); // [A]
	const rat::fltp operating_temperature = RAT_CONST(1.9);
	const rat::fltp ell_straight = 0.2;

	// GEOMETRY
	// layer 1
	// combine paths into a group
	const rat::mdl::ShPathCosThetaPr layer_path1 = rat::mdl::PathCosTheta::create({
		rat::mdl::CosThetaBlock::create(1,  5, 28e-3,  0.0*arma::Datum<rat::fltp>::tau/360,  0.0*arma::Datum<rat::fltp>::tau/360, 1.736e-3, 2.064e-3, 15.1e-3, ell_straight + 0.06, 50*arma::Datum<rat::fltp>::tau/360, 0.115e-3),
		rat::mdl::CosThetaBlock::create(1,  5, 28e-3, 22.0*arma::Datum<rat::fltp>::tau/360, 25.0*arma::Datum<rat::fltp>::tau/360, 1.736e-3, 2.064e-3, 15.1e-3, ell_straight + 0.04, 60*arma::Datum<rat::fltp>::tau/360, 0.115e-3),
		rat::mdl::CosThetaBlock::create(1,  3, 28e-3, 48.0*arma::Datum<rat::fltp>::tau/360, 48.0*arma::Datum<rat::fltp>::tau/360, 1.736e-3, 2.064e-3, 15.1e-3, ell_straight + 0.02, 70*arma::Datum<rat::fltp>::tau/360, 0.115e-3),
		rat::mdl::CosThetaBlock::create(1,  2, 28e-3, 67.0*arma::Datum<rat::fltp>::tau/360, 68.0*arma::Datum<rat::fltp>::tau/360, 1.736e-3, 2.064e-3, 15.1e-3, ell_straight, 80*arma::Datum<rat::fltp>::tau/360, 0.115e-3),
	}, element_size);

	// attach current leads
	const rat::mdl::ShPathAttachPr path_attach1 = rat::mdl::PathAttach::create(
		{layer_path1, rat::mdl::PathStraight::create(lead_length,element_size)},1);

	// cable cross section
	const rat::mdl::ShCrossWedgePr cross_wedge1 = rat::mdl::CrossWedge::create(1.736e-3,2.064e-3,-15.1e-3,0.0,2,10);
	cross_wedge1->set_surface_offset(0.0);

	// create inner and outer coils
	const rat::mdl::ShModelCoilPr coil1 = rat::mdl::ModelCoil::create(path_attach1, cross_wedge1);
	coil1->set_name("Layer 1"); 
	coil1->set_number_turns(1);
	coil1->set_operating_current(operating_current);
	coil1->set_operating_temperature(operating_temperature);


	// layer 2
	// combine paths into a group
	const rat::mdl::ShPathCosThetaPr layer_path2 = rat::mdl::PathCosTheta::create({
		rat::mdl::CosThetaBlock::create(1,  9, 44e-3,  0.0*arma::Datum<rat::fltp>::tau/360,  0.0*arma::Datum<rat::fltp>::tau/360, 1.362e-3, 1.598e-3, 15.1e-3, ell_straight + 0.05, 50*arma::Datum<rat::fltp>::tau/360, 0.100e-3),
		rat::mdl::CosThetaBlock::create(1, 16, 44e-3, 22.0*arma::Datum<rat::fltp>::tau/360, 25.0*arma::Datum<rat::fltp>::tau/360, 1.362e-3, 1.598e-3, 15.1e-3, ell_straight, 60*arma::Datum<rat::fltp>::tau/360, 0.100e-3),
	}, element_size);
	layer_path2->add_reflect_yz();
	layer_path2->add_reverse();

	// attach current leads
	const rat::mdl::ShPathAttachPr path_attach2 = rat::mdl::PathAttach::create(
		{rat::mdl::PathStraight::create(lead_length,element_size), layer_path2},2);

	// cable cross section
	const rat::mdl::ShCrossWedgePr cross_wedge2 = rat::mdl::CrossWedge::create(1.362e-3,1.598e-3,-15.1e-3,0.0,2,10);
	cross_wedge2->set_surface_offset(0.0);

	// create inner and outer coils
	const rat::mdl::ShModelCoilPr coil2 = rat::mdl::ModelCoil::create(path_attach2, cross_wedge2);
	coil2->set_name("Layer 2"); 
	coil2->set_number_turns(1);
	coil2->set_operating_current(operating_current);
	coil2->set_operating_temperature(operating_temperature);


	// Magnet poles
	// create model that combines coils
	const rat::mdl::ShModelToroidPr aperture = rat::mdl::ModelToroid::create({coil1,coil2},2);
	aperture->set_name("Angular Array");
	aperture->set_coil_azymuth('y'); aperture->set_coil_radial('x');
	aperture->set_toroid_axis('z'); aperture->set_toroid_radial('x');
	aperture->add_model(rat::mdl::ModelCylinder::create(28e-3, 0.1, element_size));
	aperture->set_alternate();

	// array for second aperture
	const rat::mdl::ShModelToroidPr coilset = rat::mdl::ModelToroid::create(aperture,2);
	coilset->set_radius(194.52e-3/2);
	coilset->set_coil_azymuth('y'); coilset->set_coil_radial('x');
	coilset->set_toroid_axis('z'); coilset->set_toroid_radial('x');

	// YOKE GEOMETRY SETUP
	// points defining double aperture shape
	arma::Mat<rat::fltp> v = arma::reshape(1e-3*arma::Col<rat::fltp>{
		0.802792499198631, 57.01887755102044,
		18.116090928518076, 57.01887755102044,
		18.162855540121882, 66.10153061224491,
		26.351858638743522, 74.17500000000004,
		36.58551447804257, 83.76224489795919,
		49.35744951383697, 91.83571428571432,
		65.17428037183464, 97.89081632653065,
		71.83304145742073, 105.45969387755105,
		124.28215140506478, 105.45969387755105,
		137.4359974356235, 88.80816326530612},2,10);
	v = arma::join_horiz(v,
		arma::fliplr(arma::join_vert(v.row(0),-v.row(1))),
		arma::join_vert(-v.row(0),-v.row(1)).eval().tail_cols(v.n_cols-1),
		arma::fliplr(arma::join_vert(-v.row(0),v.row(1))));

	// create distance function for all the holes
	const rat::dm::ShDistFunPr df_holes = rat::dm::DFUnion::create({
		rat::dm::DFCircle::create(101.18e-3,97.260e-3,0),
		rat::dm::DFCircle::create(101.18e-3,-97.260e-3,0),
		rat::dm::DFCircle::create(32.18e-3,0,180e-3),
		rat::dm::DFCircle::create(32.18e-3,0,-180e-3),
		rat::dm::DFCircle::create(16e-3,58e-3,220e-3),
		rat::dm::DFCircle::create(16e-3,-58e-3,220e-3),
		rat::dm::DFCircle::create(16e-3,58e-3,-220e-3),
		rat::dm::DFCircle::create(16e-3,-58e-3,-220e-3),
		rat::dm::DFCircle::create(8e-3,93e-3,175e-3),
		rat::dm::DFCircle::create(8e-3,-93e-3,175e-3),
		rat::dm::DFCircle::create(8e-3,93e-3,-175e-3),
		rat::dm::DFCircle::create(8e-3,-93e-3,-175e-3),
		rat::dm::DFCircle::create(8e-3,182e-3,120e-3),
		rat::dm::DFCircle::create(8e-3,-182e-3,120e-3),
		rat::dm::DFCircle::create(8e-3,182e-3,-120e-3),
		rat::dm::DFCircle::create(8e-3,-182e-3,-120e-3),
		rat::dm::DFPolygon::create(v.t()),
		rat::dm::DFArrayAngular::create(rat::dm::DFRectangle::create(-25e-3,100e-3,-25e-3,25e-3),2,2,272e-3, 53.0*arma::Datum<rat::fltp>::tau/360),
		rat::dm::DFArrayAngular::create(rat::dm::DFRectangle::create(-25e-3,100e-3,-25e-3,25e-3),2,2,272e-3, (180.0-53.0)*arma::Datum<rat::fltp>::tau/360),
		rat::dm::DFArrayAngular::create(rat::dm::DFRectangle::create(-10e-3,100e-3,-10e-3,10e-3),2,2,272e-3, 0.0*arma::Datum<rat::fltp>::tau/360),
		rat::dm::DFArrayAngular::create(rat::dm::DFRectangle::create(-10e-3,100e-3,-17e-3,17e-3),2,2,272e-3, 90.0*arma::Datum<rat::fltp>::tau/360)});

	// setup the distance function
	const rat::dm::ShDistFunPr df = rat::dm::DFDiff::create(
		{rat::dm::DFCircle::create(272e-3,0,0), df_holes});

	// create cross section with distance function
	const rat::mdl::ShCrossDMshPr cdmsh = rat::mdl::CrossDMsh::create(yoke_element_size,df,rat::dm::DFScale::create(df_holes,yoke_element_size,0.3,5*yoke_element_size));
	cdmsh->set_num_node_limit(1e5);

	// create path
	//rat::mdl::ShPathStraightPr pth = rat::mdl::PathStraight::create(ell,element_size);
	const rat::mdl::ShPathAxisPr pth = rat::mdl::PathAxis::create('z','y',ell_straight,0,0,0,3*yoke_element_size);

	// with non-linear solver
	#ifdef ENABLE_NL_SOLVER
		// create hb curve
		const rat::mdl::ShHBCurvePr hb_curve = rat::mdl::HBCurveVLV::create(500.0,1.5);

		// create yoke as non-linear mesh
		const rat::mdl::ShModelNLPr yoke = rat::mdl::ModelNL::create(pth,cdmsh,hb_curve);

	// no non-linear solver present
	#else
		// create yoke as regular mesh
		const rat::mdl::ShModelMeshPr yoke = rat::mdl::ModelMesh::create(pth,cdmsh);
	#endif

	// // create yoke as regular mesh
	// const rat::mdl::ShModelMeshPr yoke = rat::mdl::ModelMesh::create(pth,cdmsh);

	// create overarching model
	const rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create({coilset,yoke});


	// CALCULATION AND OUTPUT
	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(rat::mdl::ModelGroup::create(model));

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({RAT_CONST(0.0)},output_dir,lg);
	}
	
	// export json
	if(run_json_export){
		const rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model);
		slzr->export_json(output_dir/"model.json");
	}
}
