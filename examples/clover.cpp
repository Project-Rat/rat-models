// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for common
#include "rat/common/log.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/freecad.hh"

// header files for materials
#include "rat/mat/database.hh"

// header files for distmesh
#include "rat/dmsh/distfun.hh"
#include "rat/dmsh/dfdiff.hh"
#include "rat/dmsh/dfrrectangle.hh"

// header files for Rat-Models
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "pathclover.hh"
#include "pathcable.hh"
#include "modelgroup.hh"
#include "cartesiangriddata.hh"
#include "calcharmonics.hh"
#include "linedata.hh"
#include "pathaxis.hh"
#include "crossdmsh.hh"
#include "calcmlfmm.hh"
#include "calcinductance.hh"
#include "modelmesh.hh"
#include "lengthdata.hh"
#include "serializer.hh"
#include "calcpath.hh"
#include "calcmesh.hh"
#include "calcgrid.hh"
#include "calclength.hh"
#include "calcfreecad.hh"
#include "solvercache.hh"

// DESCRIPTION
// This example shows how to create a small single layer 
// cloverleaf coil including both magnet poles. Cloverleaf coils
// are probably a good solution for creating a dipole with high
// temperature superconducting (HTS) tape.

// main
int main(){
	// INPUT SETTINGS
	// switchboard for calculation types
	const bool run_coil_field = true; // calculate field on the coil
	const bool run_inductance = true; // calculate inductance matrix
	const bool run_grid = true; // calculate field on a grid of points surrounding the coil
	const bool run_harmonics = false; // run harmonic coil calculation
	const bool run_line = false; // line calculation at aperture center
	const bool run_length = true; // line calculation at aperture center
	const bool run_freecad = true; // export the geometry to a python freecad macro
	const bool run_json_export = false; // export geometry to json file
	const bool force_density_export = true;

	// output directory
	const boost::filesystem::path output_dir = "./clover/";

	// output time
	const rat::fltp output_time = RAT_CONST(0.0);

	// operating conditions
	const rat::fltp Iop = 2000; // operating current [A]
	const rat::fltp Top = 20; // operating temperature [K]

	// coil parameters
	const bool is_block = true; // is the coil a block or a cable?
	const rat::fltp ellstr1 = 0.140; // straight section length along aperture [m]
	const rat::fltp ellstr2 = 0.060; // straight section length accross aperture [m]
	const rat::fltp bridge_angle = arma::Datum<rat::fltp>::pi*2*7.0/360;

	// winding parameters
	// these are only used when the coil is modelled 
	// as a cable (i.e. is_block==false);
	const arma::uword idx_incr = 1; // location at which the turn is incremented to the next
	const arma::uword idx_start = 2; // section in which the cable starts 
	const arma::sword num_add = 2; // number of extra sections to add/remove at end

	// cable settings
	const rat::fltp wcable = 12e-3; // width of the cable (or coil pack) [m]
	const rat::fltp dcable = 0.2e-3; // thickness of the cable [m]
	const rat::fltp dinsu = 0e-3/2; // thickness of insulation layer around each cable [m]
	const arma::uword num_turns = 60; // number of turns
	const rat::fltp element_size = 1.5e-3; // size of the used elements [m]

	// the cloverleaf coil-end spline parameters
	const rat::fltp ell_trans = 0; // transition length [m]
	const rat::fltp height = 20e-3; // height of the bridge [m]
	const rat::fltp str12 = 0.37*35e-3;	// strength for determining control point positions [m]
	const rat::fltp str34 = 0.37*14e-3; // strength for determining control point positions [m]

	// calculate remaining parameters
	const rat::fltp dpack = num_turns*(dcable + 2*dinsu); // thickness of the coil pack

	// former
	// const rat::fltp dformer = 10e-3; //12e-3;
	// const rat::fltp dring = 2e-3;


	const rat::fltp dformer = 0; //12e-3;
	const rat::fltp dring = 0;

	// add little iron yoke
	const bool iron_yoke = false;

	

	// GEOMETRY SETUP
	// The models defining the coil geometry are setup using a nodegraph
	// that allows reusing the same object multiple times. For this cloverleaf
	// example the structure is given as:
	//     PathClover -> PathCable -|         |-> Pole1 -|  
	// Circuit, Coolant, Mat -|-> Coil -|          |-> Model
	//                   CrossRect -|         |-> Pole2 -|
	// * make sure your text editor uses a mono-spaced font

	// construct conductor
	rat::mat::ShConductorPr rebco_cu = rat::mat::Database::rebco_fujikura_tape_cern();

	// Create clover path 
	// this is the basis which determines the shape of the coil
	// the PathClover class is actually a wrapper automatically 
	// setting all the sections based on the input parameters.
	// By default all coils are created in the XY-plane.
	// rat::mdl::ShPathCloverPr path_clover = rat::mdl::PathClover::create(ellstr1,ellstr2,
	// 	height,dpack,ell_trans,str12,str34,element_size);
	// path_clover->set_bending_radius(0.4);

	// create clover path
	rat::mdl::ShPathCloverPr path_clover = rat::mdl::PathClover::create(
		ellstr1+2*dpack,ellstr2+2*dpack,height,0,ell_trans,str12,str34,element_size);
	path_clover->add_flip();
	path_clover->set_bridge_angle(bridge_angle);

	// create magnet poles
	rat::mdl::ShModelGroupPr pole1 = rat::mdl::ModelGroup::create(); pole1->set_name("p1");
	rat::mdl::ShModelGroupPr pole2 = rat::mdl::ModelGroup::create(); pole2->set_name("p2");

	// these transformations flip the second pole
	pole2->add_rotation(1,0,0,arma::Datum<rat::fltp>::pi);
	pole2->add_reverse();

	// create coil using cable detail this takes the cloverleaf 
	// basepath and winds a cable around up to the number of turns
	if(is_block==false){
		// create cable
		rat::mdl::ShPathCablePr path_cable = rat::mdl::PathCable::create(path_clover);
		path_cable->set_turn_step(dcable + 2*dinsu);
		path_cable->set_num_turns(num_turns);
		path_cable->set_idx_incr(idx_incr);
		path_cable->set_idx_start(idx_start);
		path_cable->set_num_add(num_add);
		path_cable->set_offset(dinsu+dring);

		// create cable cross section
		rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(0,dcable,-wcable/2,wcable/2,element_size);

		// create coil based on cable
		rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(path_cable, cross_rect); coil->set_name("c1");
		coil->set_enable_current_sharing(true);
		
		// set number of gauss points to use for this coil
		coil->set_num_gauss(2);
		
		// add a translation in Z to lift the coil above the mid-plane
		coil->add_translation(0,0,8e-3);

		// set circuit
		coil->set_operating_current(Iop);
		coil->set_operating_temperature(Top);
		coil->set_input_conductor(rebco_cu);
		coil->add_flip();

		// add coil to each pole
		pole1->add_model(coil); pole2->add_model(coil);
	}

	// create coil as block
	else{
		// create block cross section
		rat::mdl::ShCrossRectanglePr cross_coil = rat::mdl::CrossRectangle::create(dring,dring+dpack,-wcable/2,wcable/2,element_size);

		// create block coil
		rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(path_clover, cross_coil); coil->set_name("c1");
		
		// need to set number of turns as the coil pack represents all turns
		coil->set_number_turns(num_turns);
		
		// set number of gauss points to use for this coil
		coil->set_num_gauss(2);

		// add a translation in Z to lift the coil above the mid-plane
		coil->add_translation(0,0,8e-3);
		
		// set circuit
		coil->set_operating_current(Iop);
		coil->set_operating_temperature(Top);
		coil->set_input_conductor(rebco_cu);
		coil->add_flip();

		// add coil to each pole
		pole1->add_model(coil);	pole2->add_model(coil);
	}

	// create former
	if(dformer>0){
		// create block cross section
		rat::mdl::ShCrossRectanglePr cross_former = rat::mdl::CrossRectangle::create(-dformer,0,-wcable/2,wcable/2,element_size);

		// add former
		rat::mdl::ShModelMeshPr former = rat::mdl::ModelMesh::create(path_clover, cross_former); former->set_name("fm");
		
		// add a translation in Z to lift the coil above the mid-plane
		former->add_translation(0,0,8e-3);
		former->add_flip();

		// add coil to each pole
		pole1->add_model(former); pole2->add_model(former);
	}

	// copper ring
	if(dring>0){
		// create block cross section
		rat::mdl::ShCrossRectanglePr cross_iring = rat::mdl::CrossRectangle::create(0,dring,-wcable/2,wcable/2,element_size);

		// add former
		rat::mdl::ShModelMeshPr iring = rat::mdl::ModelMesh::create(path_clover, cross_iring); iring->set_name("ir");
		
		// add a translation in Z to lift the coil above the mid-plane
		iring->add_translation(0,0,8e-3);
		iring->add_flip();

		// add coil to each pole
		pole1->add_model(iring); pole2->add_model(iring);

		// create block cross section
		rat::mdl::ShCrossRectanglePr cross_oring = rat::mdl::CrossRectangle::create(dring+dpack,2*dring+dpack,-wcable/2,wcable/2,1.2e-3);

		// add former
		rat::mdl::ShModelMeshPr oring = rat::mdl::ModelMesh::create(path_clover, cross_oring); oring->set_name("or");
		
		// add a translation in Z to lift the coil above the mid-plane
		oring->add_translation(0,0,8e-3);
		oring->add_flip();

		// add coil to each pole
		pole1->add_model(oring); pole2->add_model(oring);
	}

	// create a model that holds both poles
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
	model->add_model(pole1); model->add_model(pole2);

	// add iron yoke
	// distance function: subtract two rounded rectangles
	rat::dm::ShDistFunPr df = rat::dm::DFDiff::create(
		rat::dm::DFRRectangle::create(-0.09,0.09,-0.05,0.05,15e-3),
		rat::dm::DFRRectangle::create(-0.06,0.06,-0.02,0.02,8e-3));

	// yoke
	if(iron_yoke){
		// create cross section with distance function
		rat::mdl::ShCrossDMshPr cdmsh = rat::mdl::CrossDMsh::create(5e-3,df,rat::dm::DFOnes::create());

		// create path
		rat::mdl::ShPathAxisPr pth = rat::mdl::PathAxis::create('y','z',0.1,0,0,0,5e-3);

		// create mesh object
		rat::mdl::ShModelMeshPr yoke = rat::mdl::ModelMesh::create(pth,cdmsh);

		// add to model
		model->add_model(yoke);
	}

	// the axis of the magnet is set here 
	// it is a 0.5 m long line along y with 
	// main harmonics pointing along z. The line 
	// is centered at the origin and is spaced 
	// with 1 mm elements.
	rat::mdl::ShPathPr pth_axis = rat::mdl::PathAxis::create('y','z',0.5,0,0,0,element_size);



	// CALCULATION AND OUTPUT
	// Create log
	// the default log displays the status in the
	// terminal window in which the code is called
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create a cache for solve storage (useful for repeated iron calculations only)
	const rat::mdl::ShSolverCachePr cache = rat::mdl::SolverCache::create();


	// calculate field on axis
	if(run_line){
		// create mesh calculatoin
		const rat::mdl::ShCalcPathPr pth = rat::mdl::CalcPath::create(model, pth_axis);

		// calculate and write vtk output files to specified directory
		pth->calculate_write({output_time},output_dir,lg,cache);
	}

	// create mesh calculation
	if(run_coil_field){
		// create mesh calculatoin
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model);

		// calculate and write vtk output files to specified directory
		mesh->calculate_write({output_time},output_dir,lg,cache);
	}

	// create grid calculation
	if(run_grid){
		// create grid calculation
		const rat::mdl::ShCalcGridPr grid = rat::mdl::CalcGrid::create(model,0.3,0.4,0.1,150,200,50);

		// calculate and write vtk output files to specified directory
		grid->calculate_write({output_time},output_dir,lg,cache);
	}

	// calculate harmonics
	if(run_harmonics){
		// create harmonic coil calculation along axis 
		const rat::fltp reference_radius = 10e-3;
		rat::mdl::ShCalcHarmonicsPr harmonics_calculator = rat::mdl::CalcHarmonics::create(model,pth_axis, reference_radius);

		// calculate harmonics and get data
		harmonics_calculator->calculate_write({output_time},output_dir,lg,cache);
	}

	// inductance calculation
	if(run_inductance){
		// create inductance calculation
		const rat::mdl::ShCalcInductancePr inductance_calculator = rat::mdl::CalcInductance::create(model);

		// calculate and show matrix in terminal
		inductance_calculator->calculate_write({output_time},output_dir,lg,cache);
	}

	// export json
	if(run_json_export){
		// create serializer
		const rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model);

		// write to output file
		slzr->export_json(output_dir/"model.json");
	}

	// calculate conductor length
	if(run_length){
		// create inductance calculation
		const rat::mdl::ShCalcLengthPr length_calculator = rat::mdl::CalcLength::create(model);
		const rat::mdl::ShLengthDataPr length = length_calculator->calculate_length(output_time, lg);
		length->display(lg);
	}

	// export json
	if(run_json_export){
		const rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model);
		slzr->export_json(output_dir/"model.json");
	}

	// // write a freecad macro
	// if(run_freecad){
	// 	const rat::cmn::ShFreeCADPr fc = rat::cmn::FreeCAD::create(output_dir/"model.FCMacro","clover");
	// 	fc->set_num_sub(1); model->export_freecad(fc);
	// }

	// freecad builder
	if(run_freecad){
		const rat::mdl::ShCalcFreeCADPr fc = rat::mdl::CalcFreeCAD::create(model, output_dir/"freecad");
		fc->calculate_write({output_time},output_dir,lg,cache);
	}


	// write force density table
	if(force_density_export){
		// settings
		const boost::filesystem::path output_file = output_dir/"force_density_table.csv";

		// create mesh calculation
		const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model);

		// calculate the field on the mesh and extract mesh data
		const std::list<rat::mdl::ShMeshDataPr> meshes = mesh->calculate_mesh(output_time, lg, cache);

		// allocate field arrays
		arma::field<arma::Mat<rat::fltp> > Rn(1,meshes.size());
		arma::field<arma::Mat<rat::fltp> > Fn(1,meshes.size());

		// gather nodes and force density from all meshes
		arma::uword idx = 0llu;
		for(auto it = meshes.begin();it!=meshes.end();it++,idx++){
			Rn(idx) = (*it)->get_nodes();
			Fn(idx) = (*it)->calc_force_density();
		}

		// combine and convert to cubic cm
		const arma::Mat<rat::fltp> M = arma::join_vert(rat::cmn::Extra::field2mat(Rn), RAT_CONST(1e-6)*rat::cmn::Extra::field2mat(Fn)).t();

		// header
		arma::field<std::string> header(1,6);
		header(0) = "x [m]"; header(1) = "y [m]"; header(2) = "z [m]";
		header(3) = "Fdx [N/cm^3]"; header(4) = "Fdy [N/cm^3]"; header(5) = "Fdz [N/cm^3]";

		// export data
		M.save(arma::csv_name(output_file.string(),header));
	}

}