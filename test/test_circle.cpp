// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>
#include <iostream>
#include <cmath>

#include "rat/common/error.hh"
#include "crosscircle.hh"
#include "area.hh"

// main
int main(){
	// settings
	rat::fltp radius = 0.05;
	rat::fltp xc = 0.01;
	rat::fltp yc = -0.04;
	rat::fltp dl = 5e-3;

	// create rectangle object
	rat::mdl::ShCrossCirclePr circ = rat::mdl::CrossCircle::create(xc,yc,radius,dl);
		
	// create area
	rat::mdl::ShAreaPr area = circ->create_area(rat::mdl::MeshSettings());

	// check area
	rat::fltp A = area->get_area();	
	rat::fltp Ach = arma::Datum<rat::fltp>::pi*radius*radius;
	if(std::abs(A - Ach)/Ach>1e-2){
		rat_throw_line("unexpected surface area");
	}

	// check perimeter
	rat::fltp p = area->get_perimeter_length();
	rat::fltp pch = 2*arma::Datum<rat::fltp>::pi*radius;
	if(std::abs(p - pch)/pch>1e-2){
		rat_throw_line("unexpected perimeter length");
	}

}