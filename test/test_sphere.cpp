// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>
#include <iostream>
#include <cmath>

#include "rat/common/error.hh"
#include "modelsphere.hh"

// main
int main(){
	// settings
	const rat::fltp radius = RAT_CONST(0.05);
	const rat::fltp element_size = RAT_CONST(0.003);
	const rat::fltp tol = RAT_CONST(1e-2);
	
	// create area
	const rat::mdl::ShModelSpherePr sphere = rat::mdl::ModelSphere::create(radius, element_size);

	// calculate volume
	rat::fltp V = sphere->calc_total_volume();
	rat::fltp Vch = (RAT_CONST(4.0)/3)*arma::Datum<rat::fltp>::pi*radius*radius*radius;
	if(std::abs(V - Vch)/Vch>tol){
		rat_throw_line("unexpected volume: " + std::to_string(V) + "/" + std::to_string(Vch));
	}

	// calculate surface area
	rat::fltp S = sphere->calc_total_surface_area();
	rat::fltp Sch = 4*arma::Datum<rat::fltp>::pi*radius*radius;
	if(std::abs(S - Sch)/Sch>tol){
		rat_throw_line("unexpected surface area: " + std::to_string(S) + "/" + std::to_string(Sch));
	}
}