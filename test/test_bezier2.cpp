// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

#include "rat/mlfmm/extra.hh"

// header files for models
#include "pathbezier.hh"

// main
int main(){
	// settings
	const rat::fltp tol = 1e-6;
	const arma::Row<rat::fltp> D{0.0,7e-1,4.2,-21,42};

	// check
	const arma::uword max_depth = D.n_cols-1;

	// create points
	const arma::Mat<rat::fltp> P = rat::mdl::PathBezier::create_control_points(D);

	// combine points to form second half of the curve
	const arma::Mat<rat::fltp> P12 = arma::join_horiz(P, RAT_CONST(1.0)-arma::fliplr(P));

	// get regular times in cylindrical coordinates
	const arma::Row<rat::fltp> t{0.0,1.0};

	// calculate bezier spline
	const arma::field<arma::Mat<rat::fltp> > C = rat::mdl::PathBezier::create_bezier_tn(t,P12,max_depth+1);

	// output 
	arma::Row<rat::fltp> C1(max_depth+1),C2(max_depth+1);
	for(arma::uword i=0;i<max_depth+1;i++){
		C1(i) = C(i)(0);
		C2(i) = C(i)(C(i).n_elem-1);
		if(i%2==0 && i!=0)C2(i)*=-1;
	}

	// check output
	if(arma::any(arma::abs(D.tail_cols(max_depth) - C1.tail_cols(max_depth))>tol))rat_throw_line("tolerance not achieved at first end");
	if(arma::any(arma::abs(D.tail_cols(max_depth) - C2.tail_cols(max_depth))>tol))rat_throw_line("tolerance not achieved at second end");
}