// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header for rat-common
#include "rat/common/extra.hh"

// header files for models
#include "pathsuperellipse.hh"
#include "pathfrenet.hh"
#include "pathunfold.hh"

// main
int main(){
	// settings
	const arma::uword num_poles = 1;
	const rat::fltp radius = 0.025;
	const rat::fltp ba = 0.05;
	const rat::fltp beta = 10.0*arma::Datum<rat::fltp>::tau/360;
	const rat::fltp phi = 5.0*arma::Datum<rat::fltp>::tau/360; 
	const rat::fltp arclength = arma::Datum<rat::fltp>::pi/2;
	const rat::fltp element_size = 1e-3;
	const rat::fltp n1 = 2.0;
	const rat::fltp n2 = 2.0;
	const rat::fltp tol = 0.1*arma::Datum<rat::fltp>::tau/360;
	
	// create superellipse constant perimeter end
	const rat::mdl::ShPathSuperEllipsePr se = rat::mdl::PathSuperEllipse::create(
		radius, ba, beta, phi, arclength, element_size, num_poles, n1, n2);

	// create an unfolder
	const rat::mdl::ShPathUnfoldPr uf1 = rat::mdl::PathUnfold::create(se);

	// create an unfolder through Frenet Serret path
	const rat::mdl::ShPathUnfoldPr uf2 = rat::mdl::PathUnfold::create(rat::mdl::PathFrenet::create(se));

	// create frame
	const rat::mdl::ShFramePr frm1 = uf1->create_frame(rat::mdl::MeshSettings());
	const rat::mdl::ShFramePr frm2 = uf2->create_frame(rat::mdl::MeshSettings());

	// combine sections
	frm1->combine(); frm2->combine();

	// get coordinates
	const arma::Mat<rat::fltp>& R1 = frm1->get_coords(0);
	const arma::Mat<rat::fltp>& R2 = frm2->get_coords(0);

	// get normalized direction vector from finite difference
	arma::Mat<rat::fltp> L1 = arma::diff(R1,1,1);
	arma::Mat<rat::fltp> L2 = arma::diff(R2,1,1);
	L1.each_row()/=rat::cmn::Extra::vec_norm(L1);
	L2.each_row()/=rat::cmn::Extra::vec_norm(L2);

	// these vectors should all be in the same direction
	const arma::Row<rat::fltp> angles1 = arma::acos(rat::cmn::Extra::dot(L1.head_cols(L1.n_cols-1), L1.tail_cols(L1.n_cols-1)));
	const arma::Row<rat::fltp> angles2 = arma::acos(rat::cmn::Extra::dot(L2.head_cols(L2.n_cols-1), L2.tail_cols(L2.n_cols-1)));

	// check output
	if(arma::accu(angles1)>tol)rat_throw_line("unfolded frenet-serret frame is not straight within tolerance");
	if(arma::accu(angles2)>tol)rat_throw_line("unfolded frenet-serret frame is not straight within tolerance");
}