// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>
#include <iostream>
#include <cmath>

#include "rat/common/error.hh"

#include "crossrectangle.hh"
#include "area.hh"
#include "pathcircle.hh"
#include "modelcoil.hh"

// main
int main(){
	// settings
	const rat::fltp inner_radius = 0.05;
	const rat::fltp outer_radius = 0.08;
	const rat::fltp height = 0.05;
	const rat::fltp element_size = 0.003;
	const arma::uword num_sections = 4;

	// create rectangle object
	const rat::mdl::ShCrossRectanglePr crss = rat::mdl::CrossRectangle::create(
		0,outer_radius-inner_radius,-height/2,
		height/2,element_size);

	// circular path
	const rat::mdl::ShPathCirclePr base = rat::mdl::PathCircle::create(
		inner_radius,num_sections,element_size);
	base->set_offset(outer_radius-inner_radius);

	// create area
	const rat::mdl::ShModelMeshPr coil = rat::mdl::ModelMesh::create(base, crss);

	// calculate volume
	rat::fltp V = coil->calc_total_volume();
	rat::fltp Vch = arma::Datum<rat::fltp>::pi*(outer_radius*outer_radius - inner_radius*inner_radius)*height;
	if(std::abs(V - Vch)/Vch>1e-2)rat_throw_line("unexpected volume");
	

	// calculate surface area
	rat::fltp S = coil->calc_total_surface_area();
	rat::fltp Sch = 2*arma::Datum<rat::fltp>::pi*(inner_radius + outer_radius)*height + 
		2*arma::Datum<rat::fltp>::pi*(outer_radius*outer_radius - inner_radius*inner_radius);
	if(std::abs(S - Sch)/Sch>1e-2)rat_throw_line("unexpected surface area");
}