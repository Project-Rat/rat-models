// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>
#include <iostream>
#include <cmath>

#include "rat/common/error.hh"
#include "crossrectangle.hh"
#include "area.hh"

// main
int main(){
	// settings
	const rat::fltp normal_inner = RAT_CONST(0.01);
	const rat::fltp normal_outer = RAT_CONST(0.04);
	const rat::fltp trans_inner = -RAT_CONST(0.01);
	const rat::fltp trans_outer = RAT_CONST(0.02);
	const arma::uword normal_num_elements = 5llu;
	const arma::uword trans_num_elements = 10llu;

	// create rectangle object
	const rat::fltp normal_element_size = (normal_outer-normal_inner)/normal_num_elements;
	const rat::fltp transverse_element_size = (trans_outer-trans_inner)/trans_num_elements;
	const rat::mdl::ShCrossRectanglePr rect = rat::mdl::CrossRectangle::create(
		normal_inner,normal_outer,trans_inner,trans_outer,
		normal_element_size,transverse_element_size);

	// create area
	const rat::mdl::ShAreaPr area = rect->create_area(rat::mdl::MeshSettings());
	// rat::mdl::ShPerimeterPr peri = rect->create_perimeter();

	// check area
	const rat::fltp A = area->get_area();	
	const rat::fltp Ach = (normal_outer-normal_inner)*(trans_outer-trans_inner);
	if(std::abs(A - Ach)>RAT_CONST(1e-6))rat_throw_line("unexpected surface area");
	
	// check perimeter
	const rat::fltp p = area->get_perimeter_length();
	const rat::fltp pch = 2*(normal_outer - normal_inner + trans_outer - trans_inner);
	if(std::abs(p - pch)>RAT_CONST(1e-6))rat_throw_line("unexpected perimeter length");
	
	// check number of surface elements
	const arma::uword Ns = area->get_num_elements();
	const arma::uword Nsch = normal_num_elements*trans_num_elements;
	if(Ns!=Nsch)rat_throw_line("unexpected number of elements");
	
	// check number of perimeter elements
	const arma::uword Np = area->get_perimeter().n_cols;
	const arma::uword Npch = 2*(normal_num_elements + trans_num_elements);
	if(Np!=Npch)rat_throw_line("unexpected number of elements in perimeter");
	
}