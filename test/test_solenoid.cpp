// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>
#include <iostream>
#include <cmath>

#include "rat/common/log.hh"
#include "rat/common/error.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/extra.hh"

#include "rat/mlfmm/soleno.hh"
#include "rat/mlfmm/settings.hh"

#include "pathcircle.hh"
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "linedata.hh"
#include "calcinductance.hh"
#include "pathaxis.hh"
#include "calcmlfmm.hh"

// main
int main(){
	// parameters
	const rat::fltp inner_radius = 0.1;
	const rat::fltp outer_radius = 0.11;
	const rat::fltp element_size = 2.5e-3;
	const rat::fltp height = 0.1;
	const arma::uword num_sections = 4;
	const arma::sword num_gauss = 2;
	const arma::uword num_exp = 7;
	// const bool volume_elements = false;

	// operating settings
	const rat::fltp num_turns = 1000;
	const rat::fltp operating_current = 400;

	// tolerance for comparison
	const rat::fltp tol = 1e-2;

	// create circle
	const rat::mdl::ShPathCirclePr path_circle = rat::mdl::PathCircle::create(inner_radius,num_sections,element_size);

	// create cross section
	const rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(
		0,outer_radius-inner_radius,-height/2,height/2,element_size);

	// create coil
	const rat::mdl::ShModelCoilPr model_coil = rat::mdl::ModelCoil::create(path_circle, cross_rect);
	model_coil->set_number_turns(num_turns);
	model_coil->set_operating_current(operating_current);
	// model_coil->set_use_volume_elements(volume_elements);
	model_coil->set_num_gauss(num_gauss);

	// calculate field on axis
	const rat::mdl::ShLineDataPr line = rat::mdl::LineData::create(
		rat::mdl::PathAxis::create('z','x',0.4,0.01,0,0,4e-3)->create_frame(rat::mdl::MeshSettings()));
		
	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);
		
	// create multipole method
	const rat::fltp time = 0.0;
	const rat::mdl::ShCalcMlfmmPr mlfmm = rat::mdl::CalcMlfmm::create(model_coil);
	mlfmm->get_settings()->set_num_exp(num_exp);
	mlfmm->add_target(line);
	mlfmm->calculate(time,lg);

	// soleno calculation for comparison setup soleno calculation
	const rat::fmm::ShSolenoPr sol = rat::fmm::Soleno::create();
	sol->set_solenoid(inner_radius,outer_radius,-height/2,height/2,operating_current,num_turns,5llu);

	// get target points
	const arma::Mat<rat::fltp> Rt = line->get_target_coords();

	// calculate at target points
	const arma::Mat<rat::fltp> Bsol = sol->calc_B(Rt);

	// get field
	if(!line->has('B'))rat_throw_line("does not have flux density");
	if(!line->has_field())rat_throw_line("field not calculated");
	const arma::Mat<rat::fltp> Bfmm = line->get_field('B');

	// compare soleno to direct
	const rat::fltp Bsol2fmm = arma::max(arma::max(arma::abs(Bsol-Bfmm)/arma::max(rat::cmn::Extra::vec_norm(Bsol)),1));
	const bool Baccepted = Bsol2fmm<=tol;

	// now we calculate the inductance
	const rat::mdl::ShInductanceDataPr ind = rat::mdl::CalcInductance::create(model_coil)->calculate_inductance(0,lg);
	const rat::fltp Lfmm = ind->get_inductance();
	const rat::fltp Efmm = ind->get_stored_energy();

	// stored energy with soleno
	const rat::fltp Lsol = arma::accu(sol->calc_M());
	const rat::fltp Esol = RAT_CONST(0.5)*Lsol*operating_current*operating_current;

	// compare soleno to direct
	const rat::fltp Esol2fmm = std::abs(Esol-Efmm)/Esol;
	const bool Eaccepted = Esol2fmm<=tol;
	
	// check indutcance
	const rat::fltp Lsol2fmm = std::abs(Lsol-Lfmm)/Lsol;
	const bool Laccepted = Lsol2fmm<=tol;

	// show results in log
	lg->msg(2,"%s%sResult%s\n",KBLD,KGRN,KNRM);
	lg->msg(2,"%sField Table%s\n",KBLU,KNRM);
	lg->msg("%4s | %26s | %26s | %26s\n","","coordinate","multipole method","soleno");
	lg->msg("%4s | %8s %8s %8s | %8s %8s %8s | %8s %8s %8s\n","id","x","y","z","Bx","By","Bz","Bx","By","Bz");
	lg->msg("%4s | %8s %8s %8s | %8s %8s %8s | %8s %8s %8s\n","[#]","[m]","[m]","[m]","[T]","[T]","[T]","[T]","[T]","[T]");
	lg->msg("="); for(arma::uword i=0;i<90;i++)lg->msg(0,"="); lg->msg("\n");
	for(arma::uword i=0;i<Bfmm.n_cols;i++)
		lg->msg("%4i | %8.5f %8.5f %8.5f | %8.5f %8.5f %8.5f | %8.5f %8.5f %8.5f\n",
			i,Rt(0,i),Rt(1,i),Rt(2,i),Bfmm(0,i),Bfmm(1,i),Bfmm(2,i),Bsol(0,i),Bsol(1,i),Bsol(2,i));
	lg->msg("="); for(arma::uword i=0;i<90;i++)lg->msg(0,"="); lg->msg("\n");
	lg->msg(-2,"\n");

	lg->msg(2,"%sCompare Field%s\n",KBLU,KNRM);
	lg->msg("difference           : %s%8g%s [%]\n",KYEL,100*Bsol2fmm,KNRM);
	lg->msg("tolerance            : %s%8g%s [%]\n",KYEL,100*tol,KNRM);
	lg->msg("accepted             : %s%s%s\n",Baccepted ? KGRN : KRED,Baccepted ? "OK" : "NOT OK",KNRM);
	lg->msg(-2,"\n");

	lg->msg(2,"%sCompare Inductance%s\n",KBLU,KNRM);
	lg->msg("inductance fmm       : %s%8g%s [H]\n",KYEL,Lfmm,KNRM);
	lg->msg("inductance sln       : %s%8g%s [H]\n",KYEL,Lsol,KNRM);
	lg->msg("inductance difference: %s%8g%s [pct]\n",KYEL,100*Lsol2fmm,KNRM);
	lg->msg("inductance tolerance : %s%8g%s [pct]\n",KYEL,100*tol,KNRM);
	lg->msg("inductance accepted  : %s%s%s\n",Laccepted ? KGRN : KRED,Laccepted ? "OK" : "NOT OK",KNRM);
	lg->msg("energy fmm           : %s%8g%s [J]\n",KYEL,Efmm,KNRM);
	lg->msg("energy soleno        : %s%8g%s [J]\n",KYEL,Esol,KNRM);
	lg->msg("energy difference    : %s%8g%s [pct]\n",KYEL,100*Esol2fmm,KNRM);
	lg->msg("energy tolerance     : %s%8g%s [pct]\n",KYEL,100*tol,KNRM);
	lg->msg("energy accepted      : %s%s%s\n",Eaccepted ? KGRN : KRED,Eaccepted ? "OK" : "NOT OK",KNRM);
	lg->msg(-4,"\n");

	// check
	if(!Baccepted)rat_throw_line("difference in magnetic field exceeds tolerance");
	if(!Eaccepted)rat_throw_line("difference in energy exceeds tolerance");
	if(!Laccepted)rat_throw_line("difference in inductance exceeds tolerance");
}