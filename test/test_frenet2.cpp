// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header for rat-common
#include "rat/common/extra.hh"

// header files for models
#include "pathcctcustom.hh"
#include "drivercct.hh"
#include "pathunfold.hh"

// main
int main(){
	// settings
	const arma::uword num_poles =3;
	const arma::uword num_layer = 2;
	const rat::fltp radius = 0.03;
	const rat::fltp alpha = 55*arma::Datum<rat::fltp>::tau/360.0;
	const rat::fltp element_size = 1e-3;
	const rat::fltp dcable = 1e-3;
	const rat::fltp wcable = 4e-3;
	const rat::fltp num_turns = 10.0;
	const arma::uword num_turns_internal = 10;
	const rat::fltp operating_current = 250.0;
	const rat::fltp operating_temperature = 20.0;
	const rat::fltp omega = 6.0e-3;
	const arma::uword nnpt = 720; 
	const rat::fltp rincr = 6e-3;
	const rat::fltp bd = 0.0;
	const rat::fltp bh = 0.0;
	const bool is_skew = false;
	const bool is_reverse = false;
	const rat::fltp tol = 0.1*arma::Datum<rat::fltp>::tau/360;

	// build radius drive
	const rat::mdl::ShDriveRCCTPr rho_drive = rat::mdl::DriveRCCT::create(num_poles, radius, alpha, is_skew, bd);

	// sextupole harmonic component
	const bool use_skew_angle = true;
	const rat::mdl::ShCCTHarmonicDrivePr sextupole_drive = rat::mdl::CCTHarmonicDrive::create(
		num_poles,is_skew,rat::mdl::DriveDC::create(alpha),use_skew_angle);
	sextupole_drive->set_b(bh);

	// custom cct 
	const rat::mdl::ShPathCCTCustomPr path_cct = rat::mdl::PathCCTCustom::create();
	path_cct->add_harmonic(sextupole_drive);
	path_cct->set_range(-num_turns/2,num_turns/2,-0.5,0.5);
	path_cct->set_num_nodes_per_turn(nnpt);
	path_cct->set_normalize_length(true);
	path_cct->set_omega(rat::mdl::DriveDC::create(omega));
	path_cct->set_rho(rho_drive);
	path_cct->set_use_local_radius(false);
	const arma::uword num_layers = 1;
	const rat::fltp rho_increment = 6e-3;
	path_cct->set_num_layers(num_layers);
	path_cct->set_rho_increment(rho_increment);
	path_cct->set_same_alpha(false);
	path_cct->set_use_frenet_serret(true);
	path_cct->set_is_reverse(is_reverse);

	// create an unfolder
	const rat::mdl::ShPathUnfoldPr uf = rat::mdl::PathUnfold::create(path_cct);

	// create frame
	const rat::mdl::ShFramePr frm = uf->create_frame(rat::mdl::MeshSettings());

	// combine sections
	frm->combine();
	
	// get coordinates
	const arma::Mat<rat::fltp>& R = frm->get_coords(0);

	// get normalized direction vector from finite difference
	arma::Mat<rat::fltp> L = arma::diff(R,1,1);
	L.each_row()/=rat::cmn::Extra::vec_norm(L);

	// these vectors should all be in the same direction
	const arma::Row<rat::fltp> angles = arma::acos(rat::cmn::Extra::dot(L.head_cols(L.n_cols-1), L.tail_cols(L.n_cols-1)));

	// check output
	if(arma::accu(angles)>tol)rat_throw_line("unfolded frenet-serret frame is not straight");
}