// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>

// common headers
#include "rat/common/error.hh"

// models headers
#include "calctracks.hh"
#include "emitternewton.hh"

// main
int main(){
	// settings
	const arma::uword num_particles = 100;
	const rat::fltp start_time = RAT_CONST(0.0);
	const rat::fltp bg_flux_density = RAT_CONST(10.0);
	const rat::fltp rreltol = RAT_CONST(1e-7); // tracking tolerance
	const rat::fltp reltol = RAT_CONST(1e-6);

	// set seed for random number generator
	arma::arma_rng::set_seed(1001);

	// walk over cases
	for(arma::uword i=0;i<2;i++){
		// create track calculation
		const rat::mdl::ShCalcTracksPr calc_tracks = rat::mdl::CalcTracks::create(rat::mdl::ModelGroup::create());
		calc_tracks->set_rreltol(rreltol);
		
		// set track type based on i
		if(i==0)calc_tracks->set_track_type(rat::mdl::CalcTracks::TrackingType::NEWTON);
		if(i==1)calc_tracks->set_track_type(rat::mdl::CalcTracks::TrackingType::RELATIVISTIC);

		// background field
		calc_tracks->set_background(rat::mdl::Background::create({0,0,bg_flux_density}));

		// create beams
		const rat::mdl::ShEmitterNewtonPr beam1 = rat::mdl::EmitterNewton::create();
		beam1->set_velocity(1e8);
		beam1->set_sigma_velocity(5e7);
		beam1->set_charge(-1);
		beam1->set_corelation_xx(1.0);
		beam1->set_corelation_yy(1.0);
		beam1->set_sigma_x(0.0);
		beam1->set_sigma_y(0.0);
		beam1->set_sigma_xa(0.0);
		beam1->set_sigma_ya(0.0);
		beam1->set_mass(1); // atomic masses
		beam1->set_lifetime(501);
		beam1->set_start_idx(250);
		beam1->set_num_particles(num_particles);
		calc_tracks->add_emitter(beam1);

		const rat::mdl::ShEmitterNewtonPr beam2 = rat::mdl::EmitterNewton::create();
		beam2->set_velocity(1e8);
		beam2->set_sigma_velocity(5e7);
		beam2->set_charge(1);
		beam2->set_corelation_xx(1.0);
		beam2->set_corelation_yy(1.0);
		beam2->set_sigma_x(0.0);
		beam2->set_sigma_y(0.0);
		beam2->set_sigma_xa(0.0);
		beam2->set_sigma_ya(0.0);
		beam2->set_mass(1); // atomic masses
		beam2->set_lifetime(501);
		beam2->set_start_idx(250);
		beam2->set_num_particles(num_particles);
		calc_tracks->add_emitter(beam2);

		// create logger
		const rat::cmn::ShLogPr lg = rat::cmn::Log::create();

		// create a cache for solve storage
		const rat::mdl::ShSolverCachePr cache = rat::mdl::SolverCache::create();

		// track particles
		const rat::mdl::ShTrackDataPr data = calc_tracks->calculate_tracks(start_time,lg,cache);
			
		// get list of particles
		const arma::field<rat::mdl::Particle>& particles = data->get_particles();
		
		// logger
		if(calc_tracks->get_track_type()==rat::mdl::CalcTracks::TrackingType::NEWTON){
			lg->msg(2,"%s%sNewton Results%s\n",KBLD,KGRN,KNRM);
		}
		else if(calc_tracks->get_track_type()==rat::mdl::CalcTracks::TrackingType::RELATIVISTIC){
			lg->msg(2,"%s%sRelativistic Results%s\n",KBLD,KGRN,KNRM);
		}
		else rat_throw_line("Must be NEWTON or RELATIVISTIC tracking");
			
		// table header
		lg->msg(2,"%sParticle Table%s\n",KBLU,KNRM);
		lg->msg("%4s %2s %8s %8s %8s %6s\n","id","ec","velocity","numeric","analytic","accept");

		// allocate errors
		arma::Col<rat::fltp> err(particles.n_elem);

		// walk over particles
		for(arma::uword i=0;i<particles.n_elem;i++){
			// get particle properties
			const rat::fltp charge_elementary = particles(i).get_charge();
			const rat::fltp charge_in_coulomb = charge_elementary*arma::Datum<rat::fltp>::ec;
			const rat::fltp mass_in_kg = particles(i).get_rest_mass()*RAT_CONST(1e9)*arma::Datum<rat::fltp>::eV/(arma::Datum<rat::fltp>::c_0*arma::Datum<rat::fltp>::c_0);
			const arma::Mat<rat::fltp> velocity = particles(i).get_track_velocities();
			const arma::Mat<rat::fltp> curvature = particles(i).get_track_curvature();

			// check if velocity is in plane
			if(arma::any(velocity.row(2)>1e-6))rat_throw_line("velocity must be in XY plane");

			// get norm
			const arma::Row<rat::fltp> velocity_norm = rat::cmn::Extra::vec_norm(velocity);
			const arma::Row<rat::fltp> curvature_radius = 1.0/rat::cmn::Extra::vec_norm(curvature);

			// check if radius of curvature and velocity are constant
			if(arma::any(arma::abs(velocity_norm - velocity_norm.front())/std::abs(velocity_norm.front())>reltol))
				rat_throw_line("velocity should be constant in a static magnetic field");
			if(arma::any(arma::abs(curvature_radius - curvature_radius.front())/std::abs(curvature_radius.front())>reltol))
				rat_throw_line("curvature radius should be constant in a static magnetic field");

			// we can now safely work with start velocity
			const rat::fltp start_velocity_norm = velocity_norm.front();
			const rat::fltp start_curvature_radius = curvature_radius.front();

			// calculate relativistic mass [kg]
			rat::fltp expected_radius_of_curvature;

			// newtonian radius of curvature calculation
			if(calc_tracks->get_track_type()==rat::mdl::CalcTracks::TrackingType::NEWTON){
				expected_radius_of_curvature = mass_in_kg*start_velocity_norm/(std::abs(charge_in_coulomb)*bg_flux_density);
			}

			// relativistic radius of curvature calculation
			else if(calc_tracks->get_track_type()==rat::mdl::CalcTracks::TrackingType::RELATIVISTIC){
				expected_radius_of_curvature = 
					((RAT_CONST(1.0)/std::sqrt(RAT_CONST(1.0) - 
					(start_velocity_norm*start_velocity_norm)/(arma::Datum<rat::fltp>::c_0*
					arma::Datum<rat::fltp>::c_0)))*mass_in_kg)*start_velocity_norm/
					(std::abs(charge_in_coulomb)*bg_flux_density);
			}

			// unknown track type
			else rat_throw_line("Must be NEWTON or RELATIVISTIC tracking");

			// calculate the relative error
			err(i) = (std::abs(start_curvature_radius - expected_radius_of_curvature)/expected_radius_of_curvature);

			// create table
			lg->msg("%4i %+1i %8.2e %8.2e %8.2e %s%6s%s\n",
				i,int(charge_elementary),start_velocity_norm,
				start_curvature_radius,expected_radius_of_curvature,
				err(i)>reltol ? KRED : KGRN,
				err(i)>reltol ? "NOT OK" : "OK",
				KNRM);
		}

		// check all tolerances
		if(arma::any(err>reltol))rat_throw_line("one or more particles have an unexpected radius of curvature");

		// done
		lg->msg(-2,"\n");
		lg->msg(-2);
	}


}