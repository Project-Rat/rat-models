// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for distmesh-cpp
#include "rat/dmsh/dfrrectangle.hh"
#include "rat/dmsh/dfdiff.hh"
#include "rat/dmsh/dfrectangle.hh"

// header files for models
#include "crossdmsh.hh"
#include "pathaxis.hh"
#include "modelnl.hh"
#include "calcmesh.hh"
#include "modelsolenoid.hh"
#include "drivelinear.hh"
#include "modelgroup.hh"
#include "calcinductance.hh"

// main
int main(){
	// SETTINGS
	// coil geometry
	const rat::fltp radius = 0.05;
	const rat::fltp thickness = 0.01;
	const rat::fltp height = 0.01;
	const rat::fltp element_size = 0.002; 
	const rat::fltp yoke_thickness = 0.04;
	const rat::fltp yoke_spacing = 0.005;
	const rat::fltp yoke_element_size = 0.015;
	const rat::fltp yoke_radius = yoke_thickness/10;
	const rat::fltp num_turns = 100.0;
	const rat::fltp tol = 1e-2;

	// output directory
	const boost::filesystem::path output_dir = "./test/";

	// create coil
	const rat::mdl::ShModelSolenoidPr solenoid = 
		rat::mdl::ModelSolenoid::create(radius,thickness,height,element_size);
	solenoid->set_number_turns(num_turns);
	solenoid->set_circuit_index(1);

	// setup the distance function for the yoke
	const rat::fltp yoke_inner = radius + thickness + yoke_spacing;
	const rat::fltp yoke_outer = yoke_inner + yoke_thickness;
	const rat::fltp yoke_lower = height/2 + yoke_spacing;
	const rat::fltp yoke_higher = yoke_lower + yoke_thickness;
	const rat::dm::ShDistFunPr df = rat::dm::DFDiff::create(
		rat::dm::DFRRectangle::create(-yoke_outer,yoke_outer,-yoke_higher,yoke_higher,yoke_radius),
		rat::dm::DFRRectangle::create(-yoke_inner,yoke_inner,-yoke_lower,yoke_lower,yoke_radius));

	// create cross section with distance function
	const rat::mdl::ShCrossDMshPr cdmsh = rat::mdl::CrossDMsh::create(yoke_element_size,df,rat::dm::DFOnes::create());

	// create path
	const rat::fltp yoke_length = 2*(radius + thickness) + yoke_spacing;
	const rat::mdl::ShPathAxisPr pth = rat::mdl::PathAxis::create('y','z',yoke_length,0,0,0,yoke_element_size);

	// create hb curve
	const rat::mdl::ShHBCurvePr hb_curve = rat::mdl::HBCurveVLV::create(500.0,2.15);

	// create yoke as non-linear mesh
	const rat::mdl::ShModelNLPr yoke = rat::mdl::ModelNL::create(pth,cdmsh,hb_curve);

	// create a model to combine the yoke and the coil
	const rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create({solenoid,yoke});


	// create a cache for solve storage
	const rat::mdl::ShSolverCachePr cache = rat::mdl::SolverCache::create();

	// create log
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// output times
	const arma::Row<rat::fltp> tt = arma::linspace<arma::Row<rat::fltp> >(0,40.0,10);

	// create a circuit
	const rat::mdl::ShDrivePr current_drive = rat::mdl::DriveLinear::create(RAT_CONST(0.0),RAT_CONST(1.0));
	const rat::mdl::ShCircuitPr circuit = rat::mdl::Circuit::create(1,current_drive);

	// // create mesh calculation
	// const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model);
	// mesh->add_circuit(circuit);

	// // calculate and write vtk output files to specified directory
	// mesh->calculate_write(tt,output_dir,rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT),cache);


	// walk over time steps
	arma::Row<rat::fltp> stored_energy(tt.n_elem);
	arma::Row<rat::fltp> power(tt.n_elem);
	for(arma::uword i=0;i<tt.n_elem;i++){
		// calculate stored energy
		const rat::mdl::ShCalcInductancePr inductance_calculation = 
			rat::mdl::CalcInductance::create(model);
		inductance_calculation->add_circuit(circuit);

		// integrate stored energy
		const rat::mdl::ShInductanceDataPr data = inductance_calculation->calculate_inductance(tt(i),lg,cache);

		// get energy directly from inductance calculation
		stored_energy(i) = data->get_stored_energy();

		// power delivered by power supply
		const rat::fltp I = current_drive->get_scaling(tt(i));
		const rat::fltp dIdt = current_drive->get_scaling(tt(i),0.0,1);
		const rat::fltp local_inductance = data->get_inductance();

		// calculate power delivered from L*dIdt = V and P = VI
		power(i) = I*local_inductance*dIdt;
	}

	// integrate power
	const arma::Row<rat::fltp> delivered_energy = rat::cmn::Extra::cumtrapz(tt,power,1);

	std::cout<<arma::join_vert(tt,stored_energy,delivered_energy).t()<<std::endl;

	// compare the two
	if(arma::any(arma::abs(delivered_energy - stored_energy)/arma::max(delivered_energy)>tol))
		rat_throw_line("delivered energy does not agree with stored energy");

}