// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>
#include <iostream>
#include <cmath>

#include "rat/common/log.hh"
#include "rat/common/error.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/extra.hh"

#include "rat/mlfmm/soleno.hh"
#include "rat/mlfmm/settings.hh"

#include "pathcircle.hh"
#include "modeledges.hh"
#include "linedata.hh"
#include "calcinductance.hh"
#include "pathaxis.hh"
#include "calcmlfmm.hh"

// main
int main(){
	// settings
	const rat::fltp inner_radius = RAT_CONST(0.1);
	const rat::fltp outer_radius = RAT_CONST(0.11);
	const rat::fltp element_size = RAT_CONST(2.5e-3);
	const rat::fltp height = RAT_CONST(0.1);
	const rat::fltp num_turns = RAT_CONST(1000.0);
	const rat::fltp operating_current = RAT_CONST(400.0);
	const arma::uword num_sections = 4;
	const arma::sword num_gauss = 2;
	const arma::uword num_exp = 7;
	const bool use_path_vectors = false; // take L,N and D from path instead of calculating from edge coordinates
	const bool refine_edges = false;

	// tolerance for comparison
	const rat::fltp tol = RAT_CONST(1e-2);

	// create paths all circles are counter clockwise and in XY plane
	const rat::mdl::ShPathCirclePr circle1 = rat::mdl::PathCircle::create(
		inner_radius,num_sections,element_size,outer_radius-inner_radius);
	circle1->add_translation(0,0,-height/2);
	const rat::mdl::ShPathCirclePr circle2 = rat::mdl::PathCircle::create(
		outer_radius,num_sections,element_size);
	circle2->add_translation(0,0,-height/2);
	const rat::mdl::ShPathCirclePr circle3 = rat::mdl::PathCircle::create(
		outer_radius,num_sections,element_size);
	circle3->add_translation(0,0,height/2);
	const rat::mdl::ShPathCirclePr circle4 = rat::mdl::PathCircle::create(
		inner_radius,num_sections,element_size,outer_radius-inner_radius);
	circle4->add_translation(0,0,height/2);

	// create coil
	const rat::mdl::ShModelEdgesPr edges_coil = rat::mdl::ModelEdges::create(
		circle1,circle2,circle3,circle4,
		std::ceil((outer_radius-inner_radius)/element_size),
		std::ceil(height/element_size));
	edges_coil->set_num_turns(num_turns);
	edges_coil->set_operating_current(operating_current);
	edges_coil->set_num_gauss(num_gauss);
	edges_coil->set_use_path_vectors(use_path_vectors);
	edges_coil->set_refine_edges(refine_edges);

	// calculate field on axis
	const rat::mdl::ShLineDataPr line = rat::mdl::LineData::create(
		rat::mdl::PathAxis::create('z','x',0.4,0.01,0,0,4e-3)->create_frame(rat::mdl::MeshSettings()));
		
	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);
		
	// create multipole method
	const rat::fltp time = 0.0;
	const rat::mdl::ShCalcMlfmmPr mlfmm = rat::mdl::CalcMlfmm::create(edges_coil);
	mlfmm->get_settings()->set_num_exp(num_exp);
	mlfmm->add_target(line);
	mlfmm->calculate(time,lg);

	// soleno calculation for comparison setup soleno calculation
	const rat::fmm::ShSolenoPr sol = rat::fmm::Soleno::create();
	sol->set_solenoid(inner_radius,outer_radius,-height/2,height/2,operating_current,num_turns,5llu);

	// get target points
	const arma::Mat<rat::fltp> Rt = line->get_target_coords();

	// calculate at target points
	const arma::Mat<rat::fltp> Bsol = sol->calc_B(Rt);

	// get field
	if(!line->has('B'))rat_throw_line("does not have flux density");
	if(!line->has_field())rat_throw_line("field not calculated");
	const arma::Mat<rat::fltp> Bfmm = line->get_field('B');

	// compare soleno to direct
	const rat::fltp Bsol2fmm = arma::max(arma::max(arma::abs(Bsol-Bfmm)/arma::max(rat::cmn::Extra::vec_norm(Bsol)),1));
	const bool Baccepted = Bsol2fmm<=tol;

	// now we calculate the inductance
	const rat::mdl::ShInductanceDataPr ind = rat::mdl::CalcInductance::create(edges_coil)->calculate_inductance(0,lg);
	const rat::fltp Lfmm = ind->get_inductance();
	const rat::fltp Efmm = ind->get_stored_energy();

	// stored energy with soleno
	const rat::fltp Lsol = arma::accu(sol->calc_M());
	const rat::fltp Esol = RAT_CONST(0.5)*Lsol*operating_current*operating_current;

	// compare soleno to direct
	const rat::fltp Esol2fmm = std::abs(Esol-Efmm)/Esol;
	const bool Eaccepted = Esol2fmm<=tol;
	
	// check indutcance
	const rat::fltp Lsol2fmm = std::abs(Lsol-Lfmm)/Lsol;
	const bool Laccepted = Lsol2fmm<=tol;

	// show results in log
	lg->msg(2,"%s%sResult%s\n",KBLD,KGRN,KNRM);
	lg->msg(2,"%sField Table%s\n",KBLU,KNRM);
	lg->msg("%4s | %26s | %26s | %26s\n","","coordinate","multipole method","soleno");
	lg->msg("%4s | %8s %8s %8s | %8s %8s %8s | %8s %8s %8s\n","id","x","y","z","Bx","By","Bz","Bx","By","Bz");
	lg->msg("%4s | %8s %8s %8s | %8s %8s %8s | %8s %8s %8s\n","[#]","[m]","[m]","[m]","[T]","[T]","[T]","[T]","[T]","[T]");
	lg->msg("="); for(arma::uword i=0;i<90;i++)lg->msg(0,"="); lg->msg("\n");
	for(arma::uword i=0;i<Bfmm.n_cols;i++)
		lg->msg("%4i | %8.5f %8.5f %8.5f | %8.5f %8.5f %8.5f | %8.5f %8.5f %8.5f\n",
			i,Rt(0,i),Rt(1,i),Rt(2,i),Bfmm(0,i),Bfmm(1,i),Bfmm(2,i),Bsol(0,i),Bsol(1,i),Bsol(2,i));
	lg->msg("="); for(arma::uword i=0;i<90;i++)lg->msg(0,"="); lg->msg("\n");
	lg->msg(-2,"\n");

	lg->msg(2,"%sCompare Field%s\n",KBLU,KNRM);
	lg->msg("difference           : %s%8g%s [%]\n",KYEL,100*Bsol2fmm,KNRM);
	lg->msg("tolerance            : %s%8g%s [%]\n",KYEL,100*tol,KNRM);
	lg->msg("accepted             : %s%s%s\n",Baccepted ? KGRN : KRED,Baccepted ? "OK" : "NOT OK",KNRM);
	lg->msg(-2,"\n");

	lg->msg(2,"%sCompare Inductance%s\n",KBLU,KNRM);
	lg->msg("inductance fmm       : %s%8g%s [H]\n",KYEL,Lfmm,KNRM);
	lg->msg("inductance sln       : %s%8g%s [H]\n",KYEL,Lsol,KNRM);
	lg->msg("inductance difference: %s%8g%s [pct]\n",KYEL,100*Lsol2fmm,KNRM);
	lg->msg("inductance tolerance : %s%8g%s [pct]\n",KYEL,100*tol,KNRM);
	lg->msg("inductance accepted  : %s%s%s\n",Laccepted ? KGRN : KRED,Laccepted ? "OK" : "NOT OK",KNRM);
	lg->msg("energy fmm           : %s%8g%s [J]\n",KYEL,Efmm,KNRM);
	lg->msg("energy soleno        : %s%8g%s [J]\n",KYEL,Esol,KNRM);
	lg->msg("energy difference    : %s%8g%s [pct]\n",KYEL,100*Esol2fmm,KNRM);
	lg->msg("energy tolerance     : %s%8g%s [pct]\n",KYEL,100*tol,KNRM);
	lg->msg("energy accepted      : %s%s%s\n",Eaccepted ? KGRN : KRED,Eaccepted ? "OK" : "NOT OK",KNRM);
	lg->msg(-4,"\n");

	// check
	if(!Baccepted)rat_throw_line("difference in magnetic field exceeds tolerance");
	if(!Eaccepted)rat_throw_line("difference in energy exceeds tolerance");
	if(!Laccepted)rat_throw_line("difference in inductance exceeds tolerance");
}