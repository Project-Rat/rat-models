// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>
#include <iostream>
#include <cmath>

// elliptical integral library from boost
#include <boost/math/special_functions/ellint_1.hpp>
#include <boost/math/special_functions/ellint_2.hpp>

// rat models headers
#include "pathcircle.hh"
#include "modelcoil.hh"
#include "crosspoint.hh"
#include "calcmesh.hh"
#include "modelgroup.hh"


// main
int main(){
	// model geometric input parameters
	const rat::fltp radius1 = RAT_CONST(40e-3); // coil inner radius [m]
	const rat::fltp radius2 = RAT_CONST(40e-3); // coil inner radius [m]
	const rat::fltp delem = RAT_CONST(2e-3); // element size [m]
	const rat::fltp thickness = RAT_CONST(1e-3); // loop cross area [m]
	const rat::fltp width = RAT_CONST(1e-3); // loop cross area [m]
	const rat::fltp separation_distance = RAT_CONST(10e-3); // distance between the loops [m]
	const rat::fltp operating_current1 = RAT_CONST(100.0); // operating current for coil 1 [A]
	const rat::fltp operating_current2 = RAT_CONST(100.0); // operating current for coil 2 [A]
	const arma::uword num_sections = 4llu; // number of coil sections
	const rat::fltp tol = RAT_CONST(1e-3); // relative tolerance
	const arma::uword num_exp = 7llu; // number of expansions used for multipoles
	const bool large_ilist = true; // using large interaction list for multipole method
	const arma::sword num_gauss = 2ll; // number of gauss points used for line elements
	const rat::fltp time = RAT_CONST(0.0); // calculation time [s]

	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create a circular path object
	const rat::mdl::ShPathCirclePr circle1 = rat::mdl::PathCircle::create(radius1, num_sections, delem);
	const rat::mdl::ShPathCirclePr circle2 = rat::mdl::PathCircle::create(radius2, num_sections, delem);

	// create a rectangular cross section object
	const rat::mdl::ShCrossPointPr point = rat::mdl::CrossPoint::create(0,0,thickness,width);

	// create a coil object
	const rat::mdl::ShModelCoilPr coil1 = rat::mdl::ModelCoil::create(circle1, point);
	coil1->set_number_turns(1.0); 
	coil1->set_num_gauss(num_gauss);
	coil1->add_translation({0,0,-separation_distance/2});
	coil1->set_operating_current(operating_current1);

	// create a coil object
	const rat::mdl::ShModelCoilPr coil2 = rat::mdl::ModelCoil::create(circle2, point);
	coil2->set_number_turns(1.0); 
	coil2->set_num_gauss(num_gauss);
	coil2->add_translation({0,0,separation_distance/2});
	coil2->set_operating_current(operating_current2);

	// create the model
	const rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create({coil1,coil2});

	// calculate the mutual forces
	const rat::mdl::ShCalcMeshPr calc_mesh = rat::mdl::CalcMesh::create(model);
	calc_mesh->get_settings()->set_num_exp(num_exp);
	calc_mesh->get_settings()->set_large_ilist(large_ilist);

	// run the mesh calculation
	const std::list<rat::mdl::ShMeshDataPr> meshes = calc_mesh->calculate_mesh(time,lg);
	const arma::Col<rat::fltp>::fixed<3> F1 = meshes.front()->calc_lorentz_force();
	const arma::Col<rat::fltp>::fixed<3> F2 = meshes.back()->calc_lorentz_force();
	const arma::Col<rat::fltp>::fixed<3> Ftot = F1 + F2;

	// analytical calculation
	double B0 = (operating_current1*arma::Datum<rat::fltp>::mu_0)/(2*radius1);

	double al = std::abs(radius2/radius1);
	double be = std::abs(separation_distance/radius1);
	double ga = separation_distance/radius2;

	double q = ((1.0+al)*(1.0+al) + be*be);
	double k = std::sqrt(4*al/q);

	double K = boost::math::ellint_1(k);
	double E = boost::math::ellint_2(k);

	// const rat::fltp Bz = B0*(1.0/(arma::Datum<double>::pi*std::sqrt(q)))*(E*(1.0-al*al-be*be)/(q-4*al)+K);
	const rat::fltp Br = B0*(ga/(arma::Datum<rat::fltp>::pi*std::sqrt(q)))*(E*(1.0+al*al+be*be)/(q-4*al)-K);
	const rat::fltp force_analytical = Br*operating_current2*2*arma::Datum<rat::fltp>::pi*radius2;

	const rat::fltp err = std::abs(F1(2) - force_analytical)/force_analytical;

	lg->msg(2,"%s%sResult%s\n",KBLD,KGRN,KNRM);
	lg->msg(2,"%sCompare Field%s\n",KBLU,KNRM);
	lg->msg("force analytical     : %s%8g%s [N]\n",KYEL,force_analytical,KNRM);
	lg->msg("force rat1           : %s%8g%s [N]\n",KYEL,F1(2),KNRM);
	lg->msg("force rat2           : %s%8g%s [N]\n",KYEL,F2(2),KNRM);
	lg->msg("difference           : %s%8g%s [%]\n",KYEL,100*err,KNRM);
	lg->msg("tolerance            : %s%8g%s [%]\n",KYEL,100*tol,KNRM);
	lg->msg("accepted             : %s%s%s\n",err<=tol ? KGRN : KRED,err<=tol ? "OK" : "NOT OK",KNRM);
	lg->msg(-2,"\n");
	lg->msg(-2,"\n");

	// check force balance
	if(arma::any(arma::abs(F1 + F2)/force_analytical>(tol)))
		rat_throw_line("force balance broken");

	// loops attract eachother
	if(err>tol)
		rat_throw_line("force does not match reference force");
}