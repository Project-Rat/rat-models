// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <iomanip>

#include "rat/common/typedefs.hh"

#include "emitterbeam.hh"
#include "calctracks.hh"
#include "modelcube.hh"
#include "background.hh"

// main
int main(){
	// test
	const rat::fltp relative_tolerance = 1e-7;

	// tracking settings
	const rat::mdl::CalcTracks::TrackingType track_type = rat::mdl::CalcTracks::TrackingType::RELATIVISTIC;
	const rat::cmn::RungeKutta::IntegrationMethod integration_method = rat::cmn::RungeKutta::IntegrationMethod::DORMAND_PRINCE;

	// muons
	const rat::fltp rest_mass = RAT_CONST(105.66e6); // [eV/C^2]
	const rat::fltp bg_flux_density = RAT_CONST(3.3); // [T]
	const rat::fltp momentum = RAT_CONST(10.0e9); // [eV/C]
	const rat::fltp charge_elementary = -RAT_CONST(1.0); // e

	// conversions
	const rat::fltp ec = arma::Datum<rat::fltp>::ec; // [C] Coulombs per elementary charge = 1.602176634e-19
	const rat::fltp eV = arma::Datum<rat::fltp>::eV; // [J/eV] joule per electron-volt = 1.602176634e-19
	const rat::fltp c_0 = arma::Datum<rat::fltp>::c_0; // [m/s] speed of light = 299792458.0

	// calculate relevant properties
	const rat::fltp beam_energy = std::sqrt(momentum*momentum + rest_mass*rest_mass);
	const rat::fltp gamma = beam_energy/rest_mass;
	const rat::fltp velocity = c_0*std::sqrt(RAT_CONST(1.0) - RAT_CONST(1.0)/(gamma*gamma));
	const rat::fltp charge_in_coulomb = charge_elementary*ec;
	const rat::fltp mass_in_kg = rest_mass*eV/(c_0*c_0);

	// radius of curvature
	const rat::fltp expected_radius_of_curvature = 
		((RAT_CONST(1.0)/std::sqrt(RAT_CONST(1.0) - 
		(velocity*velocity)/(c_0*c_0)))*mass_in_kg)*velocity/
		(std::abs(charge_in_coulomb)*bg_flux_density);

	// create a logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create();


	// create test beam
	const rat::mdl::ShEmitterBeamPr beam_emitter = rat::mdl::EmitterBeam::create(
		RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(1.0),RAT_CONST(1.0));
	beam_emitter->set_charge(charge_elementary);
	beam_emitter->set_rest_mass(RAT_CONST(1e-9)*rest_mass); // in GeV
	beam_emitter->set_beam_momentum(RAT_CONST(1e-9)*momentum); // must set in GeV/C
	beam_emitter->set_is_momentum();
	beam_emitter->set_num_particles(1);
	beam_emitter->set_lifetime(101);
	beam_emitter->set_start_idx(50);

	// create a particle track calculation and track
	const rat::mdl::ShCalcTracksPr track_calc = rat::mdl::CalcTracks::create(
		rat::mdl::ModelGroup::create(), 
		rat::mdl::ModelCube::create(10.0,10.0,0.2,0.05), 
		{beam_emitter});
	track_calc->set_background(rat::mdl::Background::create({RAT_CONST(0.0),RAT_CONST(0.0),bg_flux_density}));
	track_calc->set_enable_dynamic(false);
	// track_calc->set_rabstol(1e-8);
	// track_calc->set_pabstol(1.0);
	// track_calc->set_rreltol(1e-4);
	// track_calc->set_preltol(1e-4);
	track_calc->set_track_type(track_type);
	track_calc->set_integration_method(integration_method);
	const rat::fltp start_time = RAT_CONST(0.0);
	const rat::mdl::ShTrackDataPr track_data = track_calc->calculate_tracks(start_time, lg);

	// get radius of curvature
	const arma::Row<rat::fltp> radius_of_curvature = RAT_CONST(1.0)/rat::cmn::Extra::vec_norm(track_data->get_particle(0).get_track_curvature());
	const rat::fltp track_radius_of_curvature = arma::mean(radius_of_curvature);
	const rat::fltp stddev_radius_curvature = arma::stddev(radius_of_curvature);

	// error in radius of curvature
	const rat::fltp relative_error = std::abs(track_radius_of_curvature - expected_radius_of_curvature)/std::abs(expected_radius_of_curvature);

	// report results
	lg->msg(2,"%sResults:%s\n",KGRN,KNRM);
	lg->msg(2,"%sRadius of Curvature:%s\n",KBLU,KNRM);
	lg->msg("expected radius of curvature      : %s%12.6e%s [m]\n", KYEL, expected_radius_of_curvature, KNRM);
	lg->msg("mean tracking radius of curvature : %s%12.6e%s [m]\n", KYEL, track_radius_of_curvature, KNRM);
	lg->msg("standard deviation along track    : %s%12.6e%s [m]\n", KYEL, stddev_radius_curvature, KNRM);
	lg->msg("relative error                    : %s%12.6e%s [pct]\n", KYEL,100*relative_error, KNRM);
	lg->msg(-4,"\n");

	// check tolerances
	if(relative_error>relative_tolerance)rat_throw_line("track curvature is out of tolerance");
}