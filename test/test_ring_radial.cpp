// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// specific headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// model headers
#include "modelbar.hh"
#include "pathaxis.hh"
#include "calcpath.hh"
#include "pathcircle.hh"
#include "crossrectangle.hh"

// analytical field on axis of a radially magnetised finite cylinder
// Yoshihisa Iwashita, "Axial Magnetic field produced by radially 
// magnetized permanent magnet ring", Proceedings of the 1994 
// International Linac Conference, Tsukuba, Japan
arma::Row<rat::fltp> analytic_radially_magnetised_ring_axis(
	const arma::Row<rat::fltp> &z, const rat::fltp M, const rat::fltp Rin, 
	const rat::fltp Rout, const rat::fltp height){

	// calculate helper variables
	arma::Row<rat::fltp> r0 = arma::sqrt(1+arma::pow(z/Rout,2));
	arma::Row<rat::fltp> b0 = arma::sqrt(1+arma::pow(z/Rin,2));
	arma::Row<rat::fltp> r1 = arma::sqrt(1+arma::pow((z+height)/Rout,2));
	arma::Row<rat::fltp> b1 = arma::sqrt(1+arma::pow((z+height)/Rin,2));

	// calculate field
	arma::Row<rat::fltp> Bz = -(arma::Datum<rat::fltp>::mu_0*M/2)*(1/r1-1/b1-1/r0+
		1/b0+arma::log((1+r0)%(1+b1)/((1+b0)%(1+r1))));
	
	// return calculate value
	return Bz;
}

// main
int main(){
	// settings
	const rat::fltp inner_radius = 0.1;
	const rat::fltp outer_radius = 0.12;
	const rat::fltp height = 0.02;
	const arma::uword num_sections = 4;
	const rat::fltp element_size = 2e-3;
	const rat::fltp radial_magnetization = 1.0/arma::Datum<rat::fltp>::mu_0;
	const rat::fltp tolerance = 0.5e-2;

	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();	

	// create circle
	const rat::mdl::ShPathCirclePr path_circle = rat::mdl::PathCircle::create(inner_radius,num_sections,element_size);

	// create cross section
	const rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(
		0,outer_radius-inner_radius,-height/2,height/2,element_size);

	// ring model
	const rat::mdl::ShModelBarPr ring = rat::mdl::ModelBar::create(path_circle, cross_rect);	
	ring->set_magnetisation({0,radial_magnetization,0});

	// axis
	const rat::mdl::ShPathAxisPr axis = rat::mdl::PathAxis::create('z','x',4*outer_radius,{0,0,0},element_size);

	// line calculation
	const rat::mdl::ShCalcPathPr calc = rat::mdl::CalcPath::create(ring,axis);

	// run calculation
	const rat::fltp time = RAT_CONST(0.0);
	const rat::mdl::ShLineDataPr data = calc->calculate_path(time, lg);

	// get coordinates
	const arma::Mat<rat::fltp> Rt = data->get_target_coords();

	// get results
	const arma::Mat<rat::fltp> Hfmm = data->get_field('H');	
	const arma::Mat<rat::fltp> Bfmm = data->get_field('B');	
	const arma::Row<rat::fltp> Bzfmm = Bfmm.row(2);

	// report analytic solution
	lg->msg(2,"%s%sANALYTIC CALCULATION%s\n",KBLD,KGRN,KNRM);

	// analytical magnetised ring
	const arma::Row<rat::fltp> z = Rt.row(2);
	const arma::Row<rat::fltp> Bz = analytic_radially_magnetised_ring_axis(z-height/2, radial_magnetization, inner_radius, outer_radius, height);

	// in air 
	const arma::Row<rat::fltp> Hz = Bz/arma::Datum<rat::fltp>::mu_0;

	// calculate difference between analytic and mlfmm
	const arma::Row<rat::fltp> Bdiff = arma::abs(Bfmm.row(2) - Bz)/(arma::max(Bz)-arma::min(Bz));
	const arma::Row<rat::fltp> Hdiff = arma::abs(Hfmm.row(2) - Hz)/(arma::max(Hz)-arma::min(Hz));

	// report difference
	lg->msg(-2,"\n");

	// report result
	lg->msg(2,"%s%sRESULTS%s\n",KBLD,KGRN,KNRM);
	lg->msg(2,"%sField Table%s\n",KBLU,KNRM);
	lg->msg("%4s | %8s | %16s | %16s\n","","coord","multipole method","analytic");
	lg->msg("%4s | %8s | %8s %8s | %8s %8s\n","id","z","Hz","Bz","Hz","Bz");
	lg->msg("%4s | %8s | %8s %8s | %8s %8s\n","[#]","[m]","[T]","[T]","[MA/m]","[MA/m]");
	lg->msg("="); for(arma::uword i=0;i<54;i++)lg->msg(0,"="); lg->msg("\n");
	for(arma::uword i=0;i<Bfmm.n_cols;i++)
		lg->msg("%4i | %8.5f | %8.5f %8.5f | %8.5f %8.5f\n",
			i,Rt(2,i),Bfmm(2,i),Bz(i),Hfmm(2,i)/1e6,Hz(i)/1e6);
	lg->msg("="); for(arma::uword i=0;i<54;i++)lg->msg(0,"="); lg->msg("\n");
	lg->msg(-2,"\n");

	// check accuracy of magnetic field
	lg->msg("difference magnetic field with fmm: %s%2.2f pct%s\n",KYEL,1e2*arma::max(Hdiff),KNRM);
	if(arma::max(Hdiff)<tolerance){
		lg->msg("= accuracy magnetic field: %sOK%s\n",KGRN,KNRM);
	}else{
		lg->msg("= accuracy magnetic field: %sNOT OK%s\n",KRED,KNRM);
		rat_throw_line("difference in magnetic field exceeds tolerance");
	}
	lg->newl();

	// check accuracy of flux density
	lg->msg("difference flux density with fmm: %s%2.2f pct%s\n",KYEL,1e2*arma::max(Bdiff),KNRM);
	if(arma::max(Bdiff)<tolerance){
		lg->msg("= accuracy flux density: %sOK%s\n",KGRN,KNRM);
	}else{
		lg->msg("= accuracy flux density: %sNOT OK%s\n",KRED,KNRM);
		rat_throw_line("difference in flux density exceeds tolerance");
	}
	lg->msg(-2,"\n");

	// return
	return 0;
}