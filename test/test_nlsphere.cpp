// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for distmesh-cpp
#include "rat/common/extra.hh"

// header files for models
#include "modelnlsphere.hh"
#include "hbcurvevlv.hh"
#include "calcpoints.hh"
#include "calcmesh.hh"
#include "modelnlfile.hh"

// main
int main(){
	// check if NL solver enabled
	#ifndef ENABLE_NL_SOLVER
	rat_throw_line("this test requires the nlsolver");
	#endif

	// INPUT SETTINGS
	// settings
	const rat::fltp time = RAT_CONST(0.0); // [s]
	const rat::fltp sphere_radius = RAT_CONST(0.02); // [m] 
	const rat::fltp element_size = RAT_CONST(4e-3); // [m]
	const rat::fltp Bzbg = RAT_CONST(0.8); // [T]
	const rat::fltp tol = RAT_CONST(1e-3);
	
	// gfun settings
	const arma::uword max_iter = 100;

	// create hb curve
	const rat::mdl::ShHBCurvePr hb_curve = rat::mdl::HBCurveVLV::create(500.0,2.0);

	// create sphere
	const rat::mdl::ShModelNLSpherePr sphere = rat::mdl::ModelNLSphere::create(hb_curve);
	sphere->set_radius(sphere_radius);
	sphere->set_element_size(element_size);
	sphere->set_use_tetrahedrons();

	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// solve using the GFUN method with only a single element
	const rat::fltp damping = 0.4;
	const rat::nl::ShHBDataPr hb_data = hb_curve->create_hb_data();
	const rat::fltp Hext = Bzbg/arma::Datum<rat::fltp>::mu_0;
	const rat::fltp C = -RAT_CONST(1.0)/3; // for a sphere (https://farside.ph.utexas.edu/teaching/jk1/Electromagnetism/node61.html)
	rat::fltp H = Hext, chi;
	for(arma::uword i=0;i<max_iter;i++){
		chi = arma::as_scalar(hb_data->calc_susceptibility({std::abs(H)}));
		const rat::fltp Ha = -Hext/(C*chi-RAT_CONST(1.0));
		H += damping*(Ha - H);
	}
	const rat::fltp Bgfun = arma::Datum<rat::fltp>::mu_0*(H + chi*H);

	// calculate field at point
	const rat::mdl::ShCalcPointsPr calc_points = rat::mdl::CalcPoints::create(sphere);
	// calc_points->get_settings()->set_memory_efficient_s2m(false);
	calc_points->set_coords(rat::cmn::Extra::null_vec());
	calc_points->set_background(rat::mdl::Background::create({0,0,Bzbg}));
	const rat::mdl::ShMeshDataPr point_data = calc_points->calculate_points(time,lg);

	// ok the sphere is currently not fully right orientation mesh
	const rat::fltp Bzcalc = point_data->get_field('B')(2);
	const rat::fltp Hzcalc = point_data->get_field('H')(2);
	
	std::cout<<Bzcalc<<std::endl;
	std::cout<<Bgfun<<std::endl;
	std::cout<<std::abs(Bgfun - Bzcalc)/std::abs(Bgfun)<<std::endl;

	// check difference
	if(std::abs(Bgfun - Bzcalc)/std::abs(Bgfun)>tol)
		rat_throw_line("flux density at sphere center out of tolerance");

	// done
	return 0;
}