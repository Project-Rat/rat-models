// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>
#include <iostream>
#include <cmath>

#include "rat/common/log.hh"
#include "rat/common/error.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/extra.hh"

#include "rat/mlfmm/soleno.hh"
#include "rat/mlfmm/settings.hh"

#include "pathcircle.hh"
#include "modelcoil.hh"
#include "linedata.hh"
#include "pathaxis.hh"
#include "crossline.hh"
#include "calcmlfmm.hh"
#include "calcinductance.hh"

// analytical equation
arma::Row<rat::fltp> analytic_shell_axis(
	const arma::Row<rat::fltp> &z, const rat::fltp R, const rat::fltp L, const rat::fltp I){

	// analytical equation
	const arma::Row<rat::fltp> Bz = (arma::Datum<rat::fltp>::mu_0*I/(2*L))*(
		z/arma::sqrt(R*R + z%z) + (L-z)/arma::sqrt(R*R+(L-z)%(L-z)));

	// return calculate value
	return Bz;
}

// main
int main(){
	// model geometric input parameters
	const rat::fltp radius = 40e-3; // coil inner radius [m]
	const rat::fltp delem = 2e-3; // element size [m]
	const rat::fltp thickness = 1e-4; // loop cross area [m^2]
	const rat::fltp Iop = 100; // operating current [A]
	const rat::fltp ell = 0.2; // length of axis [m]
	const arma::uword num_sections = 4; // number of coil sections
	const rat::fltp tol = 1e-2; // relative tolerance
	const rat::fltp hcoil = 40e-3; // length of coil [m]
	const arma::uword num_exp = 7;
	const rat::fltp num_turns = 1.0;
	const arma::sword num_gauss = 4;

	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create a circular path object
	rat::mdl::ShPathCirclePr circle = rat::mdl::PathCircle::create(radius, num_sections, delem);

	// create a rectangular cross section object
	rat::mdl::ShCrossLinePr line = rat::mdl::CrossLine::create(0,-hcoil/2,0,hcoil/2,thickness,delem);

	// create a coil object
	rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(circle, line);
	coil->set_number_turns(num_turns); 
	coil->set_operating_current(Iop);
	coil->set_num_gauss(num_gauss);

	// create axis
	rat::mdl::ShPathAxisPr path = rat::mdl::PathAxis::create('z','x',ell,0,0,0,delem);

	// create mlfmm settings
	rat::fmm::ShSettingsPr fmm_settings = rat::fmm::Settings::create();
	fmm_settings->set_num_exp(num_exp);

	// create line calculation
	rat::mdl::ShLineDataPr cline = rat::mdl::LineData::create(path->create_frame(rat::mdl::MeshSettings()));
	cline->set_field_type('B',3);

	// create multipole method calculation
	rat::mdl::ShCalcMlfmmPr mlfmm = rat::mdl::CalcMlfmm::create(coil);
	mlfmm->add_target(cline);
	mlfmm->get_settings()->set_num_exp(num_exp);
	mlfmm->calculate(0,lg);

	// get result
	arma::Mat<rat::fltp> Rl = cline->get_target_coords();
	arma::Mat<rat::fltp> Bfmm = cline->get_field('B');
	arma::Row<rat::fltp> Bz = analytic_shell_axis(Rl.row(2)+hcoil/2,radius,hcoil,Iop);
	arma::Row<rat::fltp> err = arma::abs(Bfmm.row(2)-Bz)/arma::max(arma::abs(Bz));

	//std::cout<<arma::join_horiz(Bfmm.row(2).t(),Bz.t(),err.t())<<std::endl;

	// compare results
	if(arma::any(arma::abs(Bfmm.row(2)-Bz)/arma::max(arma::abs(Bz))>tol))rat_throw_line("axial field is outside tolerance");

	// now we calculate the inductance
	const rat::mdl::ShInductanceDataPr ind = rat::mdl::CalcInductance::create(coil)->calculate_inductance(0,lg);
	const rat::fltp Lfmm = ind->get_inductance();
	const rat::fltp Efmm = ind->get_stored_energy();

	// setup soleno calculation
	const rat::fmm::ShSolenoPr sol = rat::fmm::Soleno::create();
	sol->set_solenoid(radius-1e-9,radius+1e-9,-hcoil/2,hcoil/2,Iop,num_turns,1);
	const rat::fltp Lsol = sol->calc_M()(0);

	std::cout<<Lfmm<<std::endl;
	std::cout<<Lsol<<std::endl;

	// check inductance
	if((Lfmm-Lsol)/Lsol>tol)rat_throw_line("inductance is outside of expected range");
}