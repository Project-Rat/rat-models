// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// rat models headers
#include "modelbar.hh"
#include "crosscircle.hh"
#include "calcmesh.hh"
#include "pathaxis.hh"
#include "modelmirror.hh"


// main
int main(){
	// settings
	const rat::fltp tolerance = 5e-2; // analytical equation is an approximation, so we can not take a super tight tolerance here
	const rat::fltp bar_length = RAT_CONST(0.01);
	const rat::fltp bar_radius = RAT_CONST(0.002);
	const rat::fltp element_size = RAT_CONST(1e-3);
	const rat::fltp Ml = RAT_CONST(1.0)/arma::Datum<rat::fltp>::mu_0;
	const rat::fltp Mn = RAT_CONST(0.0);
	const rat::fltp Md = RAT_CONST(0.0);
	const rat::fltp time = RAT_CONST(0.0);
	const rat::fltp zoffset = bar_length/2 + 0.005;
	const arma::sword num_gauss_surface = 4;

	// model
	const rat::mdl::ShCrossCirclePr circle = rat::mdl::CrossCircle::create(bar_radius,element_size);
	circle->set_radial_vectors(false);
	const rat::mdl::ShPathAxisPr axis = rat::mdl::PathAxis::create('z','y',bar_length,{0,0,0},element_size); 
	const rat::mdl::ShModelBarPr bar = rat::mdl::ModelBar::create(axis, circle);
	bar->add_translation(0,0,zoffset);
	bar->set_magnetisation({Ml,Mn,Md}); // in LND coordinate system
	bar->set_num_gauss_surface(num_gauss_surface);
	const rat::mdl::ShModelMirrorPr mirror = rat::mdl::ModelMirror::create(bar,true);
	mirror->set_reflection_xy();

	// calculation
	const rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(mirror);

	// create log
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// run the mesh calculation
	const std::list<rat::mdl::ShMeshDataPr> meshes = mesh->calculate_mesh(time,lg);
	const arma::Col<rat::fltp>::fixed<3> F1 = meshes.front()->calc_lorentz_force();
	const arma::Col<rat::fltp>::fixed<3> F2 = meshes.back()->calc_lorentz_force();
	const arma::Col<rat::fltp>::fixed<3> Ftot = F1 + F2;

	std::cout<<F1<<std::endl;
	std::cout<<F2<<std::endl;

	// check output
	if(arma::any(Ftot>tolerance*arma::abs(F1).max()))
		rat_throw_line("total force on bar magnets must be zero");

	// conversion of variables
	const rat::fltp Br = Ml*arma::Datum<rat::fltp>::mu_0;
	const rat::fltp R = bar_radius;
	const rat::fltp x = 2*zoffset-bar_length;
	const rat::fltp L = bar_length;
	
	// cheedket equation for analytical force between two magnetized cylinders
	const rat::fltp axial_force_cheedket = (arma::Datum<rat::fltp>::pi*Br*Br*R*R/(2*arma::Datum<rat::fltp>::mu_0))*
		(2*(L+x)/std::sqrt((L+x)*(L+x)+R*R) - (2*L+x)/std::sqrt((2*L+x)*(2*L+x)+R*R) - x/std::sqrt(x*x+R*R));

	// correction for larger radii
	const rat::fltp axial_force_cheedket_corrected = axial_force_cheedket*(1.0 - R*R/((L+x)*(L+x)+R*R));

	// compare
	const rat::fltp axial_force_rat = F1(2);
	if(std::abs(axial_force_rat - axial_force_cheedket_corrected)>tolerance*axial_force_cheedket_corrected)
		rat_throw_line("calculated force does not match analytical approximation");

	// return
	return 0;
}
