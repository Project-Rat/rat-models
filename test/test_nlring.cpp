// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for distmesh-cpp
#include "rat/dmsh/dfrrectangle.hh"
#include "rat/dmsh/dfdiff.hh"
#include "rat/dmsh/dfrectangle.hh"

// header files for models
#include "crossdmsh.hh"
#include "crosspoint.hh"
#include "modelnl.hh"
#include "modelcoil.hh"
#include "crossrectangle.hh"
#include "pathaxis.hh"
#include "hbcurvevlv.hh"
#include "modelsolenoid.hh"
#include "modelgroup.hh"
#include "pathrectangle.hh"
#include "calcmlfmm.hh"

// main
int main(){
	// check if NL solver enabled
	#ifndef ENABLE_NL_SOLVER
	rat_throw_line("this test requires the nlsolver");
	#endif

	// INPUT SETTINGS
	// settings
	const rat::fltp inner_radius = 0.02; 
	const rat::fltp dcoil = 0.01;
	const rat::fltp height = 0.04;
	const rat::fltp coil_element_size = 0.002;
	const rat::fltp iron_element_size = 0.005;
	const rat::fltp J = 400e6;
	const arma::uword num_sections = 4;
	const rat::fltp time = 0.0;
	const rat::fltp ring_thickness = 0.02;
	const rat::fltp ring_radius = 0.1;
	const rat::fltp wire_gauge = 0.5e-3;
	const rat::fltp wire_element_size = 0.2e-3;
	const rat::fltp wire_radius = 0.5e-3;

	// YOKE GEOMETRY SETUP
	// setup the distance function
	const rat::dm::ShDistFunPr df = rat::dm::DFDiff::create(
		rat::dm::DFCircle::create(ring_radius+ring_thickness,0,0),
		rat::dm::DFCircle::create(ring_radius,0,0));

	// create cross section with distance function
	const rat::mdl::ShCrossDMshPr cdmsh = rat::mdl::CrossDMsh::create(iron_element_size,df,rat::dm::DFOnes::create());

	// create path
	const rat::mdl::ShPathAxisPr pth = rat::mdl::PathAxis::create('y','z',ring_thickness,0,0,0,iron_element_size);

	// create hb curve
	const rat::mdl::ShHBCurvePr hb_curve = rat::mdl::HBCurveVLV::create(500.0,2.0);

	// create modelcoil
	const rat::mdl::ShModelNLPr yoke = rat::mdl::ModelNL::create(pth,cdmsh,hb_curve);
	yoke->add_translation({ring_radius+ring_thickness/2,0,0});

	// COIL GEOMETRY SETUP'
	// create clover path
	const rat::mdl::ShModelSolenoidPr coil = rat::mdl::ModelSolenoid::create(
		inner_radius, dcoil, height, coil_element_size, num_sections);
	coil->set_use_current_density();
	coil->set_operating_current_density(J);

	// create a model to combine the yoke and the coil
	const rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create({yoke,coil});

	// create wire
	const rat::mdl::ShCrossPointPr crss = rat::mdl::CrossPoint::create();
	const rat::mdl::ShPathRectanglePr path = rat::mdl::PathRectangle::create(ring_thickness+wire_gauge, ring_thickness+wire_gauge, wire_radius, wire_element_size);
	const rat::mdl::ShModelCoilPr wire = rat::mdl::ModelCoil::create(path,crss);
	wire->add_translation({2*ring_radius + ring_thickness,0,0});

	// create meshes
	const std::list<rat::mdl::ShMeshDataPr> meshes = wire->create_meshes({},rat::mdl::MeshSettings());
	const rat::mdl::ShMeshDataPr wiremesh = meshes.front();

	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// calculate field on these planes
	const rat::mdl::ShCalcMlfmmPr mlfmm = rat::mdl::CalcMlfmm::create(model);
	mlfmm->add_target(wiremesh);
	mlfmm->calculate(time, lg);

	// get flux
	const rat::fltp phi = wiremesh->calculate_flux();
	const rat::fltp average_flux_density = phi/(ring_thickness*ring_thickness);
	const rat::fltp reference_value = -RAT_CONST(1.454);
	const rat::fltp tolerance = RAT_CONST(1.0); // [%]

	// check result
	if(std::abs(average_flux_density - reference_value)/std::abs(reference_value)>=tolerance)
		rat_throw_line("average flux density on the return side of the ring does not match the reference value");
}