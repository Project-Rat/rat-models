// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>
#include <iostream>
#include <cmath>

// distmesh headers
#include "rat/dmsh/dfrectangle.hh"
#include "rat/dmsh/dfdiff.hh"

// rat models headers
#include "modelcoil.hh"
#include "calcmesh.hh"
#include "crossdmsh.hh"
#include "modelgroup.hh"
#include "modelrectangle.hh"
#include "pathaxis.hh"
#include "modelnl.hh"

// actuator from 
// Vinh Le Vanh, "A Magnetic Vector Potential Volume Integral Formulation for 
// Nonlinear Magnetostatic Problems", IEEE TRANSACTIONS ON MAGNETICS, VOL. 52, 
// NO. 3, MARCH 2016

// main
int main(){
	// settings
	const rat::fltp tolerance = 10e-2; // quite high, need to improve forces on iron pieces!
	const rat::fltp element_size = RAT_CONST(3e-3);
	const rat::fltp thickness = RAT_CONST(20e-3);
	const rat::fltp ell_straight = RAT_CONST(60e-3);
	const rat::fltp ell_height = RAT_CONST(30e-3);
	const rat::fltp gap = RAT_CONST(5e-3);
	const rat::fltp mur = RAT_CONST(1000.0);
	const rat::fltp Js = RAT_CONST(1.0); // [T]
	const rat::fltp time = RAT_CONST(0.0);
	const arma::sword num_gauss_volume = 2;
	const arma::sword num_gauss_surface = 2;
	const arma::uword num_exp = 7llu;
	const bool large_ilist = true;

	const rat::mdl::ShModelNLPr iron1 = rat::mdl::ModelNL::create(
		rat::mdl::PathAxis::create('x','y',thickness,{0,0,0},element_size),
		rat::mdl::CrossDMsh::create(element_size, rat::dm::DFDiff::create(
			rat::dm::DFRectangle::create(-ell_straight/2-thickness,ell_straight/2+thickness,-thickness/2,thickness/2 + ell_height),
			rat::dm::DFRectangle::create(-ell_straight/2,ell_straight/2,thickness/2,thickness/2 + ell_height + 10e-3)),
			rat::dm::DFOnes::create()),
		rat::mdl::HBCurveVLV::create(mur,Js));
	iron1->set_num_gauss_volume(num_gauss_volume);
	iron1->set_num_gauss_surface(num_gauss_surface);
	iron1->set_name("Iron I");

	const rat::mdl::ShModelNLPr iron2 = rat::mdl::ModelNL::create(
		rat::mdl::PathAxis::create('x','y',thickness,{0,0,0},element_size),
		rat::mdl::CrossDMsh::create(element_size, 
			rat::dm::DFRectangle::create(-ell_straight/2-thickness,ell_straight/2+thickness,thickness/2 + ell_height + gap, 1.5*thickness + ell_height + gap),
			rat::dm::DFOnes::create()),
		rat::mdl::HBCurveVLV::create(mur,Js));
	iron2->set_num_gauss_volume(num_gauss_volume);
	iron2->set_num_gauss_surface(num_gauss_surface);
	iron2->set_name("Iron II");

	// coil
	const rat::mdl::ShModelRectanglePr rect = rat::mdl::ModelRectangle::create();
	rect->set_arc_radius(2.5e-3);
	rect->set_length(22.5e-3);
	rect->set_width(22.5e-3);
	rect->set_coil_thickness(5e-3);
	rect->set_coil_width(20e-3);
	rect->set_element_size(2e-3);
	rect->set_operating_current(1000.0);
	rect->set_number_turns(1.0);
	rect->set_name("Coil");

	// modelgroup
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create({rect,iron1,iron2});
	model->set_name("Model Tree");

	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// calculate the mutual forces
	const rat::mdl::ShCalcMeshPr calc_mesh = rat::mdl::CalcMesh::create(model);
	calc_mesh->get_settings()->set_num_exp(num_exp);
	calc_mesh->get_settings()->set_large_ilist(large_ilist);

	// run the mesh calculation
	const std::list<rat::mdl::ShMeshDataPr> meshes = calc_mesh->calculate_mesh(time,lg);
	const arma::Col<rat::fltp>::fixed<3> F1 = meshes.front()->calc_lorentz_force();
	const arma::Col<rat::fltp>::fixed<3> F2 = (*std::next(meshes.begin()))->calc_lorentz_force();
	const arma::Col<rat::fltp>::fixed<3> F3 = meshes.back()->calc_lorentz_force();
	const arma::Col<rat::fltp>::fixed<3> Ftot = F1 + F2 + F3;

	// reference force
	const rat::fltp reference_force = RAT_CONST(5.8); // [N] from paper

	// report results
	const rat::fltp relative_error = std::max(std::abs(F2(1) - reference_force)/reference_force, std::abs(-F3(1) - reference_force)/reference_force);
	lg->msg(2,"%sResults:%s\n",KGRN,KNRM);
	lg->msg("expected force                    : %s%6.2f%s [N]\n", KYEL, reference_force, KNRM);
	lg->msg("force on part A                   : %s%6.2f%s [N]\n", KYEL, F2(1), KNRM);
	lg->msg("force on part B                   : %s%6.2f%s [N]\n", KYEL, F3(1), KNRM);
	lg->msg("relative error                    : %s%6.2f%s [pct]\n", KYEL, 100*relative_error, KNRM);
	lg->msg("test tolerance                    : %s%6.2f%s [pct]\n", KYEL, 100*tolerance, KNRM);
	lg->msg("result acceptable                 : %s%6s%s\n", relative_error<tolerance ? KGRN : KRED, relative_error<tolerance ? "YES" : "NO", KNRM);;
	lg->msg(-2,"\n");

	// check
	if(relative_error>tolerance)
		rat_throw_line("calculated force does not agree with reference force");

	// std::cout<<F1<<std::endl;
	// std::cout<<F2<<std::endl;
	// std::cout<<F3<<std::endl;
}