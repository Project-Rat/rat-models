// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// model headers
#include "pathaxis.hh"
#include "crosscircle.hh"
#include "modelsolenoid.hh"
#include "modelbar.hh"
#include "modelgroup.hh"
#include "calcmesh.hh"
#include "serializer.hh"

// main
// table from team23 document
int main(){
	// general settings
	const rat::fltp force_balance_tolerance = 0.5e-2;
	const rat::fltp reference_force_tolerance = 5e-2;
	const rat::fltp coil_length = 1.524e-3;
	const rat::fltp element_size = 0.2e-3;
	const rat::fltp mandrel_thickness = 0.127e-3;
	const rat::fltp num_turns = 280.0;
	const rat::fltp time = 0.0;
	const arma::uword coil_num_sections = 4llu;
	const arma::sword num_gauss_surface = 2ll;
	// const rat::fltp neodymium_magnetization = 1.22/arma::Datum<rat::fltp>::mu_0;
	const rat::fltp samarium_cobalt_magnetization = 1.02/arma::Datum<rat::fltp>::mu_0;

	// MLFMM accuracy settings
	const arma::uword num_exp = 8llu;
	const bool large_ilist = true;

	// reproduce Table II right column
	{
		// large magnet, large coil
		const rat::fltp coil_inner_diameter = 3.048e-3;
		const rat::fltp coil_outer_diameter = 3.9624e-3;
		const rat::fltp magnet_diameter = 2.9972e-3;
		const rat::fltp magnet_length = 1.6e-3;
		const rat::fltp coil_resistance = 57.0;
		const rat::fltp wire_length = 3.0; 
		
		// walk over current values
		const rat::fltp operating_current = 100e-3; // [A]
		const rat::fltp delta = 0.254e-3; // [m]
		const rat::fltp sigma = 0.0; // co-axial
		const rat::fltp reference_force = 2.269;
		const rat::fltp magnetization = samarium_cobalt_magnetization;

		// create the solenoid in origin
		const rat::mdl::ShModelSolenoidPr solenoid = rat::mdl::ModelSolenoid::create(
			coil_inner_diameter/2, (coil_outer_diameter-coil_inner_diameter)/2, 
			coil_length, element_size, coil_num_sections);
		solenoid->set_number_turns(num_turns);
		solenoid->set_operating_current(operating_current);

		// create the bar magnet
		const rat::mdl::ShModelBarPr bar = rat::mdl::ModelBar::create(
			rat::mdl::PathAxis::create('z','y',magnet_length,{0,0,0},element_size),
			rat::mdl::CrossCircle::create(magnet_diameter/2,element_size));
		bar->set_magnetisation({-magnetization,0,0}); // along L, north is below, so minus sign
		bar->add_translation({0.0, sigma, coil_length/2 + mandrel_thickness + delta + magnet_length/2});
		bar->set_num_gauss_surface(num_gauss_surface);

		// create the model
		const rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create({solenoid,bar});
		// rat::mdl::Serializer::create(model)->export_json("team23.json");

		// create log
		const rat::cmn::ShLogPr lg = rat::cmn::Log::create();

		// calculate the mutual forces
		const rat::mdl::ShCalcMeshPr calc_mesh = rat::mdl::CalcMesh::create(model);
		calc_mesh->get_settings()->set_num_exp(num_exp);
		calc_mesh->get_settings()->set_large_ilist(large_ilist);

		// run the mesh calculation
		const std::list<rat::mdl::ShMeshDataPr> meshes = calc_mesh->calculate_mesh(time,lg);
		const arma::Col<rat::fltp>::fixed<3> F1 = meshes.front()->calc_lorentz_force()/(9.81*1e-3);
		const arma::Col<rat::fltp>::fixed<3> F2 = meshes.back()->calc_lorentz_force()/(9.81*1e-3);
		const arma::Col<rat::fltp>::fixed<3> Ftot = F1 + F2;

		std::cout<<F1(2)<<" "<<F2(2)<<" "<<reference_force<<std::endl;

		// check force balance
		if(arma::any(arma::abs(F1 + F2)>(force_balance_tolerance*reference_force)))
			rat_throw_line("force balance broken");
		if(std::abs(F2(2) - reference_force)>(reference_force_tolerance*reference_force))
			rat_throw_line("force does not match reference force");
	}

	{
		// configuration B: small magnet, large coil
		const rat::fltp coil_inner_diameter = 3.048e-3;
		const rat::fltp coil_outer_diameter = 3.9624e-3;
		const rat::fltp magnet_diameter = 1.6e-3;
		const rat::fltp magnet_length = 0.8128e-3;
		const rat::fltp coil_resistance = 32.0;
		const rat::fltp wire_length = 1.68; 
		
		// walk over current values
		const rat::fltp operating_current = 100e-3; // [A]
		const rat::fltp delta = 0.254e-3; // [m]
		const rat::fltp sigma = 0.0; // co-axial
		const rat::fltp reference_force = 0.472;
		const rat::fltp magnetization = samarium_cobalt_magnetization;

		// create the solenoid in origin
		const rat::mdl::ShModelSolenoidPr solenoid = rat::mdl::ModelSolenoid::create(
			coil_inner_diameter/2, (coil_outer_diameter-coil_inner_diameter)/2, 
			coil_length, element_size, coil_num_sections);
		solenoid->set_number_turns(num_turns);
		solenoid->set_operating_current(operating_current);

		// create the bar magnet
		const rat::mdl::ShModelBarPr bar = rat::mdl::ModelBar::create(
			rat::mdl::PathAxis::create('z','y',magnet_length,{0,0,0},element_size),
			rat::mdl::CrossCircle::create(magnet_diameter/2,element_size));
		bar->set_magnetisation({-magnetization,0,0}); // along L, north is below, so minus sign
		bar->add_translation({0.0, sigma, coil_length/2 + mandrel_thickness + delta + magnet_length/2});
		bar->set_num_gauss_surface(num_gauss_surface);

		// create the model
		const rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create({solenoid,bar});
		// rat::mdl::Serializer::create(model)->export_json("team23.json");

		// create log
		const rat::cmn::ShLogPr lg = rat::cmn::Log::create();

		// calculate the mutual forces
		const rat::mdl::ShCalcMeshPr calc_mesh = rat::mdl::CalcMesh::create(model);
		calc_mesh->get_settings()->set_num_exp(num_exp);
		calc_mesh->get_settings()->set_large_ilist(large_ilist);

		// run the mesh calculation
		const std::list<rat::mdl::ShMeshDataPr> meshes = calc_mesh->calculate_mesh(time,lg);
		const arma::Col<rat::fltp>::fixed<3> F1 = meshes.front()->calc_lorentz_force()/(9.81*1e-3);
		const arma::Col<rat::fltp>::fixed<3> F2 = meshes.back()->calc_lorentz_force()/(9.81*1e-3);
		const arma::Col<rat::fltp>::fixed<3> Ftot = F1 + F2;

		std::cout<<F1(2)<<" "<<F2(2)<<" "<<reference_force<<std::endl;

		// check force balance
		if(arma::any(arma::abs(F1 + F2)>(force_balance_tolerance*reference_force)))
			rat_throw_line("force balance broken");

		if(std::abs(F2(2) - reference_force)>(reference_force_tolerance*reference_force))
			rat_throw_line("force does not match reference force");
	}

}