// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>

#include "pathbezier.hh"

arma::Mat<rat::fltp> cubic_bezier(
	const arma::Row<rat::fltp> &t, 
	const arma::Mat<rat::fltp> &P){
	// check
	assert(P.n_cols==4);

	// get counters
	const arma::uword num_dim = P.n_rows;
	const arma::uword num_times = t.n_elem;

	// allkocate output
	arma::Mat<rat::fltp> Rc(num_dim,num_times);

	// walk over dimensions
	for(arma::uword i=0;i<num_dim;i++)
		Rc.row(i) = (1-t)%(1-t)%(1-t)*P(i,0) + 
			3*t%(1-t)%(1-t)*P(i,1) + 
			3*t%t%(1-t)*P(i,2) + t%t%t*P(i,3);

	// return bezier
	return Rc;
}



// main
int main(){
	// settings
	const arma::uword num_times = 100;
	const arma::uword rand_seed = 1001;
	const arma::uword num_dim = 5;
	const arma::uword order = 4; // must be 4
	const rat::fltp tol = 1e-5;

	// random number generator seed
	arma::arma_rng::set_seed(rand_seed);

	// random control points
	const arma::Mat<rat::fltp> P(num_dim,order,arma::fill::randu);

	// times on interval between zero and one
	const arma::Row<rat::fltp> t = 
		arma::linspace<arma::Row<rat::fltp> >(
		RAT_CONST(0.0),RAT_CONST(1.0),num_times);

	// calculate from cubic bezier check function
	const arma::Mat<rat::fltp> Rc = cubic_bezier(t,P);

	// calculate from casteljau algorithm
	const arma::Mat<rat::fltp> Rb = 
		rat::mdl::PathBezier::create_bezier_tn(t,P,1)(0);

	// compare
	// std::cout<<arma::join_horiz(Rc.t(), Rb.t())<<std::endl;

	// check
	if(arma::any(arma::vectorise(arma::abs(Rc - Rb)/arma::abs(Rc)>tol)))
		rat_throw_line("Cubic bezier spline is not within tolerance");
}