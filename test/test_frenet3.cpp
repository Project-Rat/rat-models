// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header for rat-common
#include "rat/common/extra.hh"

// header files for models
#include "pathunfold.hh"
#include "pathbezier.hh"
#include "pathfrenet.hh"

// main
int main(){
	// settings
	const rat::fltp ell_trans = 0e-3; // transition length [m]
	const rat::fltp element_size = 1e-3;
	const rat::fltp tol = 0.1*arma::Datum<rat::fltp>::tau/360;
	
	// cloverleaf coil end
	const rat::fltp height = 20e-3; // bridge height [m]
	const arma::Col<rat::fltp>::fixed<3> R0 = rat::cmn::Extra::null_vec(); // start point xyz [m]
	const arma::Col<rat::fltp>::fixed<3> L0 = rat::cmn::Extra::unit_vec('y'); // start direction xyz [m] (must be normalized)
	const arma::Col<rat::fltp>::fixed<3> N0 = rat::cmn::Extra::unit_vec('x'); // start normal vector xyz [m] (must be normalized)
	const arma::Col<rat::fltp>::fixed<3> R1 = {0,0,height}; // end point xyz [m]
	const arma::Col<rat::fltp>::fixed<3> L1 = -rat::cmn::Extra::unit_vec('x'); // end direction xyz [m] (must be normalized)
	const arma::Col<rat::fltp>::fixed<3> N1 = rat::cmn::Extra::unit_vec('y'); // end normal vector xyz [m] (must be normalized)
	const rat::fltp str1 = 0.37*35e-3; // [m]
	const rat::fltp str2 = 0.37*35e-3; // [m]
	const rat::fltp str3 = 0.37*14e-3; // [m]
	const rat::fltp str4 = 0.37*14e-3; // [m]
	const rat::fltp offset = 0; // [m]

	// control points in UV-planes
	const arma::Mat<rat::fltp> Puv1 = arma::reshape(arma::Col<rat::fltp>{
		0.0,0.0, 1*str1,0, 2*str1,0, 3*str1,0, 4*str1,1*str3, 
		5*str1,2*str3, 6*str1,4*str3, 6*str1,10*str3},2,8);
	const arma::Mat<rat::fltp> Puv2 = arma::reshape(arma::Col<rat::fltp>{
		0.0,0.0, 1*str2,0, 2*str2,0, 3*str2,0, 4*str2,1*str4, 
		5*str2,2*str4, 6*str2,4*str4, 6*str2,10*str4},2,8);

	// create unified path
	rat::mdl::ShPathBezierPr path_bezier = rat::mdl::PathBezier::create(
		R0,L0,N0, R1,L1,N1, Puv1,Puv2,ell_trans,ell_trans,element_size,offset);
	path_bezier->set_num_sections(4);
	path_bezier->set_analytic_frame(true);

	// create an unfolder
	const rat::mdl::ShPathUnfoldPr uf1 = rat::mdl::PathUnfold::create(path_bezier);

	// create an unfolder through Frenet Serret path
	const rat::mdl::ShPathUnfoldPr uf2 = rat::mdl::PathUnfold::create(rat::mdl::PathFrenet::create(path_bezier));

	// create frame
	const rat::mdl::ShFramePr frm1 = uf1->create_frame(rat::mdl::MeshSettings());
	const rat::mdl::ShFramePr frm2 = uf2->create_frame(rat::mdl::MeshSettings());

	// combine sections
	frm1->combine(); frm2->combine();

	// get coordinates
	const arma::Mat<rat::fltp>& Rf1 = frm1->get_coords(0);
	const arma::Mat<rat::fltp>& Rf2 = frm2->get_coords(0);

	// get normalized direction vector from finite difference
	arma::Mat<rat::fltp> Lf1 = arma::diff(Rf1,1,1);
	arma::Mat<rat::fltp> Lf2 = arma::diff(Rf2,1,1);
	Lf1.each_row()/=rat::cmn::Extra::vec_norm(Lf1);
	Lf2.each_row()/=rat::cmn::Extra::vec_norm(Lf2);

	// these vectors should all be in the same direction
	const arma::Row<rat::fltp> angles1 = arma::acos(rat::cmn::Extra::dot(Lf1.head_cols(Lf1.n_cols-1), Lf1.tail_cols(Lf1.n_cols-1)));
	const arma::Row<rat::fltp> angles2 = arma::acos(rat::cmn::Extra::dot(Lf2.head_cols(Lf2.n_cols-1), Lf2.tail_cols(Lf2.n_cols-1)));

	// check output
	if(arma::accu(angles1)>tol)rat_throw_line("unfolded frenet-serret frame is not straight within tolerance");
	if(arma::accu(angles2)>tol)rat_throw_line("unfolded frenet-serret frame is not straight within tolerance");
}