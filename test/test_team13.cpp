// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// specific headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// model headers
#include "crossrectangle.hh"
#include "modelrectangle.hh"
#include "modelnl.hh"
#include "pathaxis.hh"
#include "hbcurve.hh"
#include "modelmirror.hh"
#include "crosspoint.hh"
#include "pathrectangle.hh"
#include "modelarray.hh"
#include "calcline.hh"
#include "calcmlfmm.hh"
#include "pathgroup.hh"
#include "patharc.hh"
#include "pathstraight.hh"
#include "hbcurvetable.hh"

// main
int main(){
	// check if NL solver enabled
	#ifndef ENABLE_NL_SOLVER
	rat_throw_line("this test requires the nlsolver");
	#endif

	// SETTINGS
	// tolerance
	const rat::fltp tolerance = 6; // [%]

	// coil
	const rat::fltp time = 0.0;
	const rat::fltp coil_width = 150e-3;
	const rat::fltp coil_length = 150e-3;
	const rat::fltp coil_radius = 25e-3;
	const rat::fltp coil_thickness = 25e-3;
	const rat::fltp coil_cable_width = 100e-3;
	const rat::fltp coil_current = 1000.0; // ampere turns
	const rat::fltp coil_element_size = 2.5e-3;

	// strips
	// const rat::fltp extra_gap = 0.47e-3; // is this parameter G?
	const rat::fltp extra_gap = 0.0; // is this parameter G?
	const rat::fltp strip_width = 50e-3;
	const rat::fltp strip_thickness = 3.2e-3;
	const rat::fltp strip_height = 126.4e-3;
	const rat::fltp strip_length = 120e-3;
	const rat::fltp strip_offset = 40e-3;
	const rat::fltp strip_shift = 4.2e-3;
	const rat::fltp strip_element_size = 3.2e-3;
	const rat::fltp flux_wire_elem_size = 0.2e-3;
	const rat::fltp strip_radius = 1.0e-3;
	const rat::fltp radius_element_size = 2e-3;
	const rat::fltp wire_gauge = 0.0; // extra spacing needed around steel for softness of surface (to be improved)
	// const rat::fltp wire_gauge = 0.47e-3; // extra spacing needed around steel for softness of surface (to be improved)
	const rat::fltp softening_factor = 0.0; // <- need to think how to improve
	const bool use_tetrahedrons = false;

	// GEOMETRY
	// create the racetrack
	const rat::mdl::ShModelRectanglePr coil = rat::mdl::ModelRectangle::create();
	coil->set_arc_radius(coil_radius);
	coil->set_length(coil_length);
	coil->set_width(coil_width);
	coil->set_coil_thickness(coil_thickness);
	coil->set_coil_width(coil_cable_width);
	coil->set_element_size(coil_element_size);
	coil->set_number_turns(1);
	coil->set_operating_current(coil_current);
	coil->set_circuit_index(1);
	coil->set_name("Coil");

	// create hb-curve
	const rat::mdl::ShHBCurveTablePr hb = rat::mdl::HBCurveTable::create();
	hb->set_team13_2();
	// hb->set_armco_cold_rolled();

	// create a cross section for the strip
	const rat::mdl::ShCrossRectanglePr rect = rat::mdl::CrossRectangle::create(
		-strip_thickness/2,strip_thickness/2,-strip_width/2,strip_width/2,strip_element_size);

	// axial path
	const rat::mdl::ShPathAxisPr axis1 = rat::mdl::PathAxis::create(
		'z','y',strip_height,rat::cmn::Extra::null_vec(),strip_element_size);

	// create the non-linear magnetic steel strips
	const rat::mdl::ShModelNLPr strip1 = rat::mdl::ModelNL::create(axis1,rect,hb);
	strip1->set_softening(softening_factor);
	strip1->set_name("strip1");
	strip1->set_refine2tetrahedrons(use_tetrahedrons);
	
	// // axial path
	// const rat::mdl::ShPathAxisPr axis2 = rat::mdl::PathAxis::create(
	// 	'x','y',strip_length,{strip_shift/2+strip_length/2,strip_offset,(strip_height-strip_thickness)/2},strip_element_size);

	// // create the non-linear magnetic steel strips
	// const rat::mdl::ShModelNLPr strip2 = rat::mdl::ModelNL::create(axis2,rect,hb);
	// strip2->set_softening(softening_factor);
	// strip2->set_name("strip2");
	// strip2->set_refine2tetrahedrons(use_tetrahedrons);
	
	// // axial path
	// const rat::mdl::ShPathAxisPr axis3 = rat::mdl::PathAxis::create(
	// 	'x','y',strip_thickness,{strip_shift/2+strip_length+strip_thickness/2,strip_offset,(strip_height-strip_thickness)/2},strip_element_size);

	// // create the non-linear magnetic steel strips
	// const rat::mdl::ShModelNLPr strip3 = rat::mdl::ModelNL::create(axis3,rect,hb);
	// strip3->set_softening(softening_factor);
	// strip3->set_name("strip3");
	// strip3->set_refine2tetrahedrons(use_tetrahedrons);

	// // axial path
	// const rat::mdl::ShPathAxisPr axis4 = rat::mdl::PathAxis::create(
	// 	'z','y',(strip_height-2*strip_thickness)/2,{strip_shift/2+strip_length+strip_thickness/2,strip_offset,(strip_height-2*strip_thickness)/4},strip_element_size);

	// // create the non-linear magnetic steel strips
	// const rat::mdl::ShModelNLPr strip4 = rat::mdl::ModelNL::create(axis4,rect,hb);
	// strip4->set_softening(softening_factor);
	// strip4->set_name("strip4");
	// strip4->set_refine2tetrahedrons(use_tetrahedrons);

	// grouped path
	const rat::mdl::ShPathGroupPr path_group = rat::mdl::PathGroup::create(
		{strip_shift/2+extra_gap,strip_offset,(strip_height - strip_thickness)/2},{1,0,0},{0,0,1});
	path_group->add_path(rat::mdl::PathStraight::create(strip_length-strip_radius,strip_element_size));
	path_group->add_path(rat::mdl::PathArc::create(strip_thickness/2 + strip_radius, arma::Datum<rat::fltp>::pi/2, radius_element_size, strip_thickness/2));
	path_group->add_path(rat::mdl::PathStraight::create(strip_height/2-strip_thickness-strip_radius,strip_element_size));

	// create the non-linear magnetic steel strips
	const rat::mdl::ShModelNLPr ustrip = rat::mdl::ModelNL::create(path_group,rect,hb);
	ustrip->set_softening(softening_factor);
	ustrip->set_name("U-strip");
	ustrip->set_refine2tetrahedrons(use_tetrahedrons);

	// create mirror
	const rat::mdl::ShModelMirrorPr mirror1 = rat::mdl::ModelMirror::create();
	// mirror1->add_model(strip2); mirror1->add_model(strip3); mirror1->add_model(strip4);
	mirror1->add_model(ustrip);
	mirror1->set_plane_vector(rat::cmn::Extra::unit_vec('z'));
	mirror1->set_name("z-mirror");

	// create mirror
	const rat::mdl::ShModelMirrorPr mirror2 =rat::mdl:: ModelMirror::create();
	mirror2->add_model(mirror1);
	mirror2->set_plane_vector(rat::cmn::Extra::unit_vec('x'));
	mirror2->set_keep_original(false);
	mirror2->set_name("x-mirror");

	const rat::mdl::ShModelMirrorPr mirror3 = rat::mdl::ModelMirror::create();
	mirror3->add_model(mirror2);
	mirror3->set_plane_vector(rat::cmn::Extra::unit_vec('y'));
	mirror3->set_keep_original(false);
	mirror3->set_name("y-mirror");

	// create a group for all the magnetic steel
	const rat::mdl::ShModelGroupPr steel = rat::mdl::ModelGroup::create(std::list<rat::mdl::ShModelPr>{strip1,mirror1,mirror3});
	steel->set_color({15.0/255,76.0/255,82.0/255},true);
	steel->set_name("Steel");

	// create model
	const rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create(std::list<rat::mdl::ShModelPr>{coil,steel});
	model->set_name("Model Tree");

	// // show table
	// std::cout<<hb->get_table()<<std::endl;

	// create flux loops
	const arma::Col<rat::fltp> xpos2{2.1e-3+extra_gap,10.0e-3,20.0e-3,30.0e-3,40.0e-3,50.0e-3,60.0e-3,80.0e-3,100.0e-3,110.0e-3,122.1e-3-strip_radius};
	const rat::mdl::ShModelGroupPr wires = rat::mdl::ModelGroup::create();
	const rat::mdl::ShCrossPointPr crss = rat::mdl::CrossPoint::create();
	const rat::mdl::ShPathRectanglePr path = rat::mdl::PathRectangle::create(strip_thickness+wire_gauge, strip_width+wire_gauge, strip_thickness/32, flux_wire_elem_size);
	for(arma::uword i=0;i<7;i++){
		const rat::mdl::ShModelCoilPr wire = rat::mdl::ModelCoil::create(path,crss);
		wire->add_translation({0,0,i*10e-3});
		wires->add_model(wire);
	}
	for(arma::uword i=0;i<xpos2.n_elem;i++){
		const rat::mdl::ShModelCoilPr wire = rat::mdl::ModelCoil::create(path,crss);
		wire->add_rotation({0,0,0},{0,1,0},arma::Datum<rat::fltp>::pi/2,{xpos2(i), strip_offset,strip_height/2 - strip_thickness/2});
		wires->add_model(wire);
	}
	for(arma::uword i=0;i<7;i++){
		const rat::mdl::ShModelCoilPr wire = rat::mdl::ModelCoil::create(path,crss);
		wire->add_rotation({0,0,0},{0,1,0},arma::Datum<rat::fltp>::pi,{123.7e-3+extra_gap,strip_offset,60.0e-3 - arma::sword(i)*10e-3 - (i==0 ? wire_gauge/2 + strip_radius : 0.0)});
		wires->add_model(wire);
	}
	const arma::uword num_wires = wires->num_models();

	// export json
	rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(rat::mdl::ModelGroup::create({model,wires}));
	slzr->export_json("team13.json");

	// scenarios
	const rat::mdl::ShCircuitPr circuit1 = rat::mdl::Circuit::create(1,rat::mdl::DriveDC::create(1000.0));
	const rat::mdl::ShCircuitPr circuit2 = rat::mdl::Circuit::create(1,rat::mdl::DriveDC::create(3000.0));
	
	// general solver data
	// create a cache for solve storage
	const rat::mdl::ShSolverCachePr cache = rat::mdl::SolverCache::create();

	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create();
	


	// CALCULATE FIELD IN AIR 1000AT
	// start and end point for table 2
	const arma::Col<rat::fltp>::fixed<3> P1{10.0e-3,20.0e-3,55.0e-3};
	const arma::Col<rat::fltp>::fixed<3> P2{110.0e-3,20.0e-3,55.0e-3};
	const arma::uword num_points = 11;

	// calculate line for table 2
	const rat::mdl::ShCalcLinePr calc_line1 = rat::mdl::CalcLine::create(model,P1,P2,num_points);
	calc_line1->add_circuit(circuit1);
	const rat::mdl::ShLineDataPr data1 = calc_line1->calculate_line(time,lg,cache);

	// get magnetic field on line and take magnitude
	const arma::Row<rat::fltp> numerical_flux_density_1000AT = rat::cmn::Extra::vec_norm(data1->get_field('B'));
	const arma::Mat<rat::fltp> target_coordinates_1000AT = data1->get_target_coords();
	

	// CALCULATE FIELD IN AIR 3000AT
	// calculate line for table 2
	const rat::mdl::ShCalcLinePr calc_line2 = rat::mdl::CalcLine::create(model,P1,P2,num_points);
	calc_line2->add_circuit(circuit2);
	const rat::mdl::ShLineDataPr data2 = calc_line2->calculate_line(time,lg,cache);

	// get magnetic field on line and take magnitude
	const arma::Row<rat::fltp> numerical_flux_density_3000AT = rat::cmn::Extra::vec_norm(data2->get_field('B'));
	const arma::Mat<rat::fltp> target_coordinates_3000AT = data2->get_target_coords();


	// CALCULATE FIELD IN IRON 1000AT
	// calculate area of the strip
	// const rat::fltp strip_area = (strip_width+wire_gauge)*(strip_thickness+wire_gauge);
	const rat::fltp strip_area = strip_width*strip_thickness;

	// create meshes
	const std::list<rat::mdl::ShMeshDataPr> meshes1 = wires->create_meshes({},rat::mdl::MeshSettings());
	if(meshes1.size()!=num_wires)rat_throw_line("number of meshes does not match number of wires");

	// calculate field on these planes
	const rat::mdl::ShCalcMlfmmPr mlfmm1 = rat::mdl::CalcMlfmm::create(model);
	mlfmm1->add_circuit(circuit1);
	for(const auto& mesh : meshes1)mlfmm1->add_target(mesh);
	mlfmm1->calculate(time, lg, cache);
	
	// allocate flux density
	arma::Row<rat::fltp> numerical_averaged_flux_density_1000AT(num_wires);

	// walk over meshes and calculate averaged flux
	auto it1 = meshes1.begin(); arma::uword cnt = 0;
	for(arma::uword i=0;i<7;i++,it1++,cnt++)
		numerical_averaged_flux_density_1000AT(cnt) = (*it1)->calculate_flux()/strip_area;
	for(arma::uword i=0;i<xpos2.n_elem;i++,it1++,cnt++)
		numerical_averaged_flux_density_1000AT(cnt) = (*it1)->calculate_flux()/strip_area;
	for(arma::uword i=0;i<7;i++,it1++,cnt++)
		numerical_averaged_flux_density_1000AT(cnt) = (*it1)->calculate_flux()/strip_area;

	// sanity check
	if(it1!=meshes1.end())rat_throw_line("not all wires were included in table");
	if(cnt!=num_wires)rat_throw_line("counter does not match number of meshes");


	// CALCULATE FIELD IN IRON 3000AT
	// create meshes
	const std::list<rat::mdl::ShMeshDataPr> meshes2 = wires->create_meshes({},rat::mdl::MeshSettings());
	if(meshes2.size()!=num_wires)rat_throw_line("number of meshes does not match number of wires");

	// calculate field on these planes
	const rat::mdl::ShCalcMlfmmPr mlfmm2 = rat::mdl::CalcMlfmm::create(model);
	mlfmm2->add_circuit(circuit2);
	for(const auto& mesh : meshes2)mlfmm2->add_target(mesh);
	mlfmm2->calculate(time, lg, cache);
	
	// allocate flux density
	arma::Row<rat::fltp> numerical_averaged_flux_density_3000AT(num_wires);

	// walk over meshes and calculate averaged flux
	auto it2 = meshes2.begin(); cnt = 0;
	for(arma::uword i=0;i<7;i++,it2++,cnt++)
		numerical_averaged_flux_density_3000AT(cnt) = (*it2)->calculate_flux()/strip_area;
	for(arma::uword i=0;i<xpos2.n_elem;i++,it2++,cnt++)
		numerical_averaged_flux_density_3000AT(cnt) = (*it2)->calculate_flux()/strip_area;
	for(arma::uword i=0;i<7;i++,it2++,cnt++)
		numerical_averaged_flux_density_3000AT(cnt) = (*it2)->calculate_flux()/strip_area;


	// sanity check
	if(!numerical_averaged_flux_density_3000AT.is_finite())rat_throw_line("averaged flux density is not finite");
	if(it2!=meshes2.end())rat_throw_line("not all wires were included in table");
	if(cnt!=num_wires)rat_throw_line("counter does not match number of meshes");


	// COMPARE AND REPORT RESULTS
	// measured flux densities at specified points
	const arma::Row<rat::fltp> reference_flux_density_1000AT = {
		0.03439189189189188, 0.020675675675675666, 0.016351351351351344, 0.014391891891891877, 0.013108108108108099, 
		0.012094594594594582, 0.011013513513513498, 0.008648648648648644, 0.00574324324324324, 0.0029054054054053896, 
		0.0014189189189189183};
	const arma::Row<rat::fltp> reference_flux_density_3000AT = {
		0.06249999999999998, 0.042905405405405384, 0.03655405405405404, 0.03344594594594593, 0.03128378378378377, 
		0.029256756756756742, 0.026216216216216198, 0.020472972972972958, 0.012905405405405392, 0.0070945945945945915, 
		0.004459459459459442};

	// calculate relative error
	const arma::Row<rat::fltp> error1_1000AT = RAT_CONST(100.0)*arma::abs(numerical_flux_density_1000AT - reference_flux_density_1000AT)/reference_flux_density_1000AT;
	const arma::Row<rat::fltp> error1_3000AT = RAT_CONST(100.0)*arma::abs(numerical_flux_density_3000AT - reference_flux_density_3000AT)/reference_flux_density_3000AT;

	// header
	lg->msg(2,"%s%sCOMPARISON WITH REFERENCE DATA%s\n",KBLD,KGRN,KNRM);

	// table for 1000AT
	const arma::uword table_width1 = 45;
	lg->msg(2,"%sFlux Density in Air (1000AT)%s\n",KBLU,KNRM);
	lg->msg("y = %2.3f [m], z = %2.3f [m]\n",P1(1),P1(2));
	lg->msg("%4s %8s %11s %11s %8s\n","id", "x [m]", "|Brat| [T]", "|Bref| [T]","diff [%]");
	lg->msg("="); for(arma::uword i=0;i<table_width1;i++)lg->msg(0,"="); lg->msg("\n");
	for(arma::uword i=0;i<num_points;i++){
		lg->msg("%04llu %8.3f %11.4e %11.4e %8.4f\n",i+1,
			target_coordinates_1000AT(0,i),numerical_flux_density_1000AT(i),
			reference_flux_density_1000AT(i),error1_1000AT(i));
	}
	lg->msg("="); for(arma::uword i=0;i<table_width1;i++)lg->msg(0,"="); lg->msg("\n");

	// done
	lg->msg(-2,"\n");

	// table for 3000AT
	lg->msg(2,"%sFlux Density in Air (3000AT)%s\n",KBLU,KNRM);
	lg->msg("y = %2.3f [m], z = %2.3f [m]\n",P1(1),P1(2));
	lg->msg("%4s %8s %11s %11s %8s\n","id", "x [m]", "|Brat| [T]", "|Bref| [T]","diff [%]");
	lg->msg("="); for(arma::uword i=0;i<table_width1;i++)lg->msg(0,"="); lg->msg("\n");
	for(arma::uword i=0;i<num_points;i++){
		lg->msg("%04llu %8.3f %11.4e %11.4e %8.4f\n",i+1,
			target_coordinates_3000AT(0,i),numerical_flux_density_3000AT(i),
			reference_flux_density_3000AT(i),error1_3000AT(i));
	}
	lg->msg("="); for(arma::uword i=0;i<table_width1;i++)lg->msg(0,"="); lg->msg("\n");

	// done
	lg->msg(-2,"\n");

	// expected values from measurement
	// data table for 1000 AT
	const arma::Row<rat::fltp> reference_average_flux_density_1000AT{
	 	1.381889763779528, 1.3700787401574805, 1.3346456692913387, 1.2795275590551183, 
	 	1.188976377952756, 1.0551181102362206, 0.6653543307086616, 0.2509529860228717, 
	 	0.4510800508259212, 0.5559085133418044, 0.6353240152477763, 0.7020330368487928, 
	 	0.7528589580686151, 0.813214739517154, 0.9085133418043202, 0.9561626429479033, 
	 	0.9606299212598426, 0.9645669291338583, 0.9685039370078742, 0.9685039370078742, 
	 	0.9763779527559057, 0.9803149606299212, 0.984251968503937, 0.9881889763779528, 
	 	0.9881889763779528};

	// data table for 3000 AT
	const arma::Row<rat::fltp> reference_average_flux_density_3000AT{
		1.8700787401574803, 1.8661417322834648, 1.8425196850393704, 1.8031496062992127, 
		1.7244094488188977, 1.5590551181102363, 0.9815756035578147, 0.39763779527559073, 
		0.7598425196850395, 0.9488188976377954, 1.0944881889763782, 1.2204724409448822, 
		1.3346456692913387, 1.4370078740157481, 1.590551181102362, 1.6377952755905514, 
		1.6338582677165352, 1.6181102362204727, 1.6062992125984255, 1.614173228346457, 
		1.614173228346457, 1.6181102362204727, 1.6259842519685042, 1.6299212598425197, 
		1.6338582677165352};

	// calculate relative error
	const arma::Row<rat::fltp> error2_1000AT = RAT_CONST(100.0)*arma::abs(numerical_averaged_flux_density_1000AT - reference_average_flux_density_1000AT)/reference_average_flux_density_1000AT;
	const arma::Row<rat::fltp> error2_3000AT = RAT_CONST(100.0)*arma::abs(numerical_averaged_flux_density_3000AT - reference_average_flux_density_3000AT)/reference_average_flux_density_3000AT;

	// header
	lg->msg(2,"%sFlux Density in steel (1000AT)%s\n",KBLU,KNRM);

	// table for 3000AT
	const arma::uword table_width2 = 36;
	lg->msg("%4s %11s %11s %8s\n","id", "|Brat| [T]", "|Bref| [T]", "diff [%]");
	lg->msg("="); for(arma::uword i=0;i<table_width2;i++)lg->msg(0,"="); lg->msg("\n");
	for(arma::uword i=0;i<num_wires;i++){
		lg->msg("%04llu %11.4e %11.4e %s%8.4f%s\n",i+1,
			numerical_averaged_flux_density_1000AT(i),
			reference_average_flux_density_1000AT(i),error2_1000AT(i)>tolerance ? KRED : KNRM, error2_1000AT(i), KNRM);
	}
	lg->msg("="); for(arma::uword i=0;i<table_width2;i++)lg->msg(0,"="); lg->msg("\n");

	// done
	lg->msg(-2,"\n");

	
	// header
	lg->msg(2,"%sFlux Density in steel (3000AT)%s\n",KBLU,KNRM);

	// table for 3000AT
	lg->msg("%4s %11s %11s %8s\n","id", "|Brat| [T]", "|Bref| [T]", "diff [%]");
	lg->msg("="); for(arma::uword i=0;i<table_width2;i++)lg->msg(0,"="); lg->msg("\n");
	for(arma::uword i=0;i<num_wires;i++){
		lg->msg("%04llu %11.4e %11.4e %s%8.4f%s\n",i+1,
			numerical_averaged_flux_density_3000AT(i),
			reference_average_flux_density_3000AT(i),error2_3000AT(i)>tolerance ? KRED : KNRM,error2_3000AT(i),KNRM);
	}
	lg->msg("="); for(arma::uword i=0;i<table_width2;i++)lg->msg(0,"="); lg->msg("\n");

	// done
	lg->msg(-2,"\n");

	// header
	lg->msg(2,"%sSummary%s\n",KBLU,KNRM);

	// check for nans
	if(!numerical_averaged_flux_density_3000AT.is_finite())rat_throw_line("flux at 3000 AT contains nans");
	if(!numerical_averaged_flux_density_1000AT.is_finite())rat_throw_line("flux at 1000 AT contains nans");

	// report status
	const rat::fltp max_error1_1000AT = arma::max(error1_1000AT);
	const rat::fltp max_error1_3000AT = arma::max(error1_3000AT);
	const rat::fltp max_error2_1000AT = arma::max(error2_1000AT);
	const rat::fltp max_error2_3000AT = arma::max(error2_3000AT);
	lg->msg("flux density in air 1000AT: %s%s%s\n",max_error1_1000AT<tolerance ? KGRN : KRED, max_error1_1000AT<tolerance ? "OK" : "NOT OK", KNRM);
	lg->msg("flux density in air 3000AT: %s%s%s\n",max_error1_3000AT<tolerance ? KGRN : KRED, max_error1_3000AT<tolerance ? "OK" : "NOT OK", KNRM);
	lg->msg("averaged flux density in steel 1000AT: %s%s%s\n",max_error2_1000AT<tolerance ? KGRN : KRED, max_error2_1000AT<tolerance ? "OK" : "NOT OK", KNRM);
	lg->msg("averaged flux density in steel 3000AT: %s%s%s\n",max_error2_3000AT<tolerance ? KGRN : KRED, max_error2_3000AT<tolerance ? "OK" : "NOT OK", KNRM);

	// throw error in case of failure
	if(max_error1_1000AT>=tolerance)rat_throw_line("flux density in air 1000AT failed");
	if(max_error1_3000AT>=tolerance)rat_throw_line("flux density in air 3000AT failed");
	if(max_error2_1000AT>=tolerance)rat_throw_line("averaged flux density in steel 1000AT failed");
	if(max_error2_3000AT>=tolerance)rat_throw_line("averaged flux density in steel 3000AT failed");

	// done
	lg->msg(-2,"\n");


	// done
	lg->msg(-2);
}
