// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>
#include <iostream>
#include <cmath>

#include "modelcube.hh"
#include "modelsolenoid.hh"
#include "modelgroup.hh"
#include "maxwellfieldmap.hh"
#include "driveac.hh"
#include "circuit.hh"
#include "calcpoints.hh"
#include "modelbar.hh"
#include "pathaxis.hh"
#include "crossrectangle.hh"
#include "modelnlsphere.hh"
#include "hbcurvevlv.hh"

// main
int main(){
	// parameters
	const arma::uword num_targets = 1000llu;
	const rat::fltp inner_radius = 0.1;
	const rat::fltp dcoil = 0.01;
	const rat::fltp height = 0.01; 
	const rat::fltp element_size = 2e-3;
	const arma::uword num_sections = 4llu;
	const rat::fltp spacing = 5e-3;
	const rat::fltp amplitude = 1000.0;
	const rat::fltp bg_field_str = 1.0;
	const rat::fltp frequency = 0.1;
	const rat::fltp abstol = 1e-6;
	const rat::fltp reltol = 1e-2;

	// set seed
	arma::arma_rng::set_seed(1001);

	// times
	const arma::Row<rat::fltp> times = arma::linspace<arma::Row<rat::fltp> >(0.0, 1.0/frequency, 5);

	// cases to cover
	// create first solenoid
	const rat::mdl::ShModelSolenoidPr solenoid1 = rat::mdl::ModelSolenoid::create(
		inner_radius, dcoil, height, element_size, num_sections);
	solenoid1->set_circuit_index(1);
	solenoid1->set_name("Solenoid I");

	// create second larger solenoid
	const rat::mdl::ShModelSolenoidPr solenoid2 = rat::mdl::ModelSolenoid::create(
		inner_radius+spacing+dcoil, dcoil, height, element_size, num_sections); 
	solenoid2->set_circuit_index(2);
	solenoid2->add_rotation({1,0,0},arma::Datum<rat::fltp>::pi/2);
	solenoid2->set_name("Solenoid II");

	// create third even larger solenoid without circuit
	const rat::mdl::ShModelSolenoidPr solenoid3 = rat::mdl::ModelSolenoid::create(
		inner_radius+2*spacing+2*dcoil, dcoil, height, element_size, num_sections); 
	solenoid3->set_circuit_index(0);
	solenoid3->set_operating_current(1000);
	solenoid3->add_rotation({0,1,0},arma::Datum<rat::fltp>::pi/2);
	solenoid3->set_name("Solenoid III");

	// add some bar magnet to see if the field map can be thrown off
	const rat::mdl::ShModelBarPr bar = rat::mdl::ModelBar::create(
		rat::mdl::PathAxis::create('z','x',30e-3,{0,0,0},2e-3), 
		rat::mdl::CrossRectangle::create(-5e-3,5e-3,-5e-3,5e-3,2e-3));
	bar->set_magnetisation({1.0/arma::Datum<rat::fltp>::mu_0,0,0});
	bar->set_name("Bar");

	// iron ball
	const rat::mdl::ShHBCurvePr hb_curve = rat::mdl::HBCurveVLV::create(500.0,2.0);
	const rat::mdl::ShModelNLSpherePr nl_sphere = rat::mdl::ModelNLSphere::create(hb_curve);
	nl_sphere->set_radius(20e-3);
	nl_sphere->set_element_size(4e-3);
	nl_sphere->set_use_tetrahedrons();
	nl_sphere->set_name("NL Sphere");
	
	// static background field
	const rat::mdl::ShBackgroundPr bg1 = rat::mdl::Background::create(); 
	bg1->set_bg_field('z', bg_field_str);

	// dynamic background field
	const rat::mdl::ShBackgroundPr bg2 = rat::mdl::Background::create();
	bg2->set_bg_drive('z', rat::mdl::DriveAC::create(bg_field_str, frequency));

	// create circuits
	const rat::mdl::ShCircuitPr circuit1 = rat::mdl::Circuit::create(
		1, rat::mdl::DriveAC::create(amplitude,frequency,0.0,0.0));
	const rat::mdl::ShCircuitPr circuit2 = rat::mdl::Circuit::create(
		2, rat::mdl::DriveAC::create(amplitude,frequency,arma::Datum<rat::fltp>::pi/2,0.0));
	circuit1->set_name("Circuit I");
	circuit2->set_name("Circuit II");

	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// create fmm settings
	const rat::fmm::ShSettingsPr fmm_stngs = rat::fmm::Settings::create();
	fmm_stngs->set_num_exp(7);

	// create a cache for solve storage
	const rat::mdl::ShSolverCachePr cache = rat::mdl::SolverCache::create();


	// walk over cases
	for(arma::uword j=0;j<8;j++){
		// allocate
		rat::mdl::ShModelGroupPr model;
		std::list<rat::mdl::ShCircuitPr> circuits;
		rat::mdl::ShBackgroundPr bgs = NULL;

		// test cases
		if(j==0){
			lg->msg(2,"%sRunning Case 0: No Field%s\n",KGRN,KNRM);
			model = rat::mdl::ModelGroup::create();
		}

		else if(j==1){
			lg->msg(2,"%sRunning Case I: Static Background Field%s\n",KGRN,KNRM);
			model = rat::mdl::ModelGroup::create(); bgs = bg1;
		}

		else if(j==2){
			lg->msg(2,"%sRunning Case II: Transient Background Field%s\n",KGRN,KNRM);
			model = rat::mdl::ModelGroup::create(); bgs = bg2;
		}

		else if(j==3){
			lg->msg(2,"%sRunning Case III: Static Solenoid, Bar and Background%s\n",KGRN,KNRM);
			model = rat::mdl::ModelGroup::create({solenoid3,bar}); bgs = bg1;
		}

		else if(j==4){
			lg->msg(2,"%sRunning Case IV: Transient Solenoids, but no Circuit%s\n",KGRN,KNRM);
			model = rat::mdl::ModelGroup::create({solenoid1,solenoid2});
		}

		else if(j==5){
			lg->msg(2,"%sRunning Case V: Transient Solenoids, Bg and Bar%s\n",KGRN,KNRM);
			model = rat::mdl::ModelGroup::create({solenoid1,solenoid2,bar}); bgs = bg2;
			circuits.push_back(circuit1); circuits.push_back(circuit2);
		}

		else if(j==6){
			lg->msg(2,"%sRunning Case VI: NL Sphere in Static Background%s\n",KGRN,KNRM);
			model = rat::mdl::ModelGroup::create(nl_sphere); bgs = bg1;
		}

		else if(j==7){
			lg->msg(2,"%sRunning Case VII: NL Sphere in Transient Background%s\n",KGRN,KNRM);
			model = rat::mdl::ModelGroup::create(nl_sphere); bgs = bg2;
		}

		else if(j==8){
			lg->msg(2,"%sRunning Case VIII: Transient Solenoids, Background and NL Sphere%s\n",KGRN,KNRM);
			model = rat::mdl::ModelGroup::create({solenoid1,solenoid2,nl_sphere}); bgs = bg2;
			circuits.push_back(circuit1); circuits.push_back(circuit2);
		}

		// unknown case
		else rat_throw_line("unknown case");


		// tracking model
		const rat::fltp cube_size = 3*(inner_radius + 2*dcoil + spacing);
		const rat::mdl::ShModelCubePr tracking_cube = rat::mdl::ModelCube::create(cube_size,cube_size,cube_size,cube_size/50);
		tracking_cube->set_name("Tracking Cube");


		// create settings for mesh setup
		const rat::mdl::MeshSettings msh_stngs;

		// create fieldmap
		const bool enable_dynamic = true;
		const rat::mdl::ShMaxwellFieldMapPr fieldmap = rat::mdl::MaxwellFieldMap::create();
		fieldmap->set_fmm_settings(fmm_stngs);
		fieldmap->set_solver_cache(cache);
		fieldmap->setup(enable_dynamic, times(0), circuits, 
			model->create_meshes({}, msh_stngs), 
			tracking_cube->create_meshes({},msh_stngs), bgs, lg);

		// store timings
		arma::Row<rat::fltp> time_fldmap(times.n_elem);
		arma::Row<rat::fltp> time_mlfmm(times.n_elem);
		arma::Row<rat::fltp> rms_error(times.n_elem);

		// calculate
		lg->msg(2,"%sCalculate%s\n",KBLU,KNRM);

		// walk over timer steps
		for(arma::uword i=0;i<times.n_elem;i++){
			// report
			lg->msg(2, "%stime: %06.2f [s]%s\n",KCYN,times(i),KNRM);

			// current time
			const rat::fltp time = times(i);

			// create target points within the cube at random positions
			const arma::Mat<rat::fltp> Rt = (arma::randu(3,num_targets) - 0.5)*cube_size;

			// set timer
			arma::wall_clock timer; timer.tic();

			// calculate field on target points using the grid
			const std::pair<arma::Mat<rat::fltp>, arma::Mat<rat::fltp> > EB = fieldmap->calc_fields(Rt,time,lg);

			// get time
			time_fldmap(i) = timer.toc();

			// get field
			const arma::Mat<rat::fltp>& B1 = EB.second;

			// compare to direct FMM calculation
			lg->msg("Run MLFMM calculation without fieldmap\n");
			const rat::mdl::ShCalcPointsPr calc_points = rat::mdl::CalcPoints::create(model);
			for(const auto& circuit : circuits)calc_points->add_circuit(circuit);
			if(bgs)calc_points->set_background(bgs);
			calc_points->set_coords(Rt);

			// set timer
			timer.tic();

			// calculate
			const rat::mdl::ShMeshDataPr data = calc_points->calculate_points(time,rat::cmn::NullLog::create(),cache);

			// get time
			time_mlfmm(i) = timer.toc();

			// get the field
			const arma::Mat<rat::fltp> B2 = data->get_field('B');

			// compare fields
			const arma::Row<rat::fltp> dB = (rat::cmn::Extra::vec_norm(B1) - rat::cmn::Extra::vec_norm(B2));

			// calculate rms error
			rms_error(i) = std::sqrt(arma::accu(arma::square(dB))/dB.n_elem)/(std::sqrt(arma::accu(arma::square(B2))/B2.n_elem)+abstol);

			// time done
			lg->msg(-2,"\n");
		}

		// calculation done
		lg->msg(-2);


		// output table
		lg->msg(2,"%sResults%s\n",KBLU,KNRM);
		lg->msg("%5s, %9s, %9s, %9s\n","t [s]","tfmm [s]","tmap [s]","err");
		lg->hline(38);
		for(arma::uword i=0;i<times.n_elem;i++)
			lg->msg("%05.2f, %09.2e, %09.2e, %09.2e\n",times(i),time_mlfmm(i),time_fldmap(i),rms_error(i));
		lg->hline(38);

		// show error to user
		bool is_ok = rms_error.is_finite() && rms_error.max()<reltol;
		lg->msg("error: %9.2e (%s%s%s)\n",rms_error.max(), is_ok ? KGRN : KRED, is_ok ? "OK" : "NOT OK", KNRM);

		// check and throw error if not ok
		if(!rms_error.is_finite())
			rat_throw_line("error is not finite");
		if(rms_error.max()>reltol)
			rat_throw_line("error out of tolerance");

		// results done
		lg->msg(-2);

		// case done
		lg->msg(-2,"\n");
	}
}



