// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "transreflect.hh"

#include "rat/common/extra.hh"
#include "rat/common/elements.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	TransReflect::TransReflect(){
		set_name("Reflection");
	}

	// constructor with vector input
	TransReflect::TransReflect(
		const arma::Col<fltp>::fixed<3> &plane_vector) : TransReflect(){
		set_plane_vector(plane_vector);
	}

	// constructor with vector input
	TransReflect::TransReflect(
		const arma::Col<fltp>::fixed<3> &origin, 
		const arma::Col<fltp>::fixed<3> &plane_vector) : TransReflect(plane_vector){
		set_origin(origin);
	}

	// factory
	ShTransReflectPr TransReflect::create(){
		//return ShTransReflectPr(new TransReflect);
		return std::make_shared<TransReflect>();
	}

	// factory with vector input
	ShTransReflectPr TransReflect::create(
		const arma::Col<fltp>::fixed<3> &plane_vector){
		//return ShTransReflectPr(new TransReflect(vec));
		return std::make_shared<TransReflect>(plane_vector);
	}

	// factory with vector input
	ShTransReflectPr TransReflect::create(
		const arma::Col<fltp>::fixed<3> &origin, 
		const arma::Col<fltp>::fixed<3> &plane_vector){
		//return ShTransReflectPr(new TransReflect(vec));
		return std::make_shared<TransReflect>(origin, plane_vector);
	}


	// get reflection matrix
	arma::Mat<fltp>::fixed<3,3> TransReflect::create_reflection_matrix() const{
		// normalize plane vector
		const arma::Col<fltp>::fixed<3> vnorm = 
			plane_vector_.each_row()/cmn::Extra::vec_norm(plane_vector_);

		// setup matrix
		arma::Mat<fltp>::fixed<3,3> M;

		// fill matrix
		M.each_row() = vnorm.t();
		M.each_col() %= vnorm;
		M *= -2.0; M.diag() += 1.0;

		// return matrix
		return M;
	}

	// set origin and vector
	void TransReflect::set_origin(
		const arma::Col<fltp>::fixed<3> &origin){
		origin_ = origin;
	}

	// set plane vector
	void TransReflect::set_plane_vector(
		const arma::Col<fltp>::fixed<3> &plane_vector){
		plane_vector_ = plane_vector;
	}

	// get origin
	arma::Col<fltp>::fixed<3> TransReflect::get_origin() const{
		return origin_;
	}

	// get plane vector
	arma::Col<fltp>::fixed<3> TransReflect::get_plane_vector() const{
		return plane_vector_;
	}

	// // set vector
	// void TransReflect::set_reflection(const arma::Col<fltp>::fixed<3> vec){
	// 	set_reflection(vec(0),vec(1),vec(2));
	// }

	// // set vector
	// void TransReflect::set_reflection(
	// 	const fltp vx,const fltp vy,const fltp vz){
	// 	// normalize
	// 	fltp ell = std::sqrt(vx*vx + vy*vy + vz*vz);
	// 	fltp vxx = vx/ell, vyy = vy/ell, vzz = vz/ell;

	// 	// setup matrix
	// 	M_.row(0) = arma::Row<fltp>::fixed<3>{1-2*vxx*vxx,-2*vxx*vyy,-2*vxx*vzz};
	// 	M_.row(1) = arma::Row<fltp>::fixed<3>{-2*vxx*vyy,1-2*vyy*vyy,-2*vyy*vzz};
	// 	M_.row(2) = arma::Row<fltp>::fixed<3>{-2*vxx*vzz,-2*vyy*vzz,1-2*vzz*vzz};
	// }

	// reflection in xy-plane where z = 0
	void TransReflect::set_reflection_xy(){
		set_origin({RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0)});
		set_plane_vector({RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(1.0)});
	}

	// reflection in yz-plane where x = 0
	void TransReflect::set_reflection_yz(){
		set_origin({RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0)});
		set_plane_vector({RAT_CONST(1.0),RAT_CONST(0.0),RAT_CONST(0.0)});
	}

	// reflection in xz-plane where y = 0
	void TransReflect::set_reflection_xz(){
		set_origin({RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0)});
		set_plane_vector({RAT_CONST(0.0),RAT_CONST(1.0),RAT_CONST(0.0)});
	}

	// apply to coordinates only
	void TransReflect::apply_coords(arma::Mat<fltp> &R, const fltp /*time*/) const{
		R = (create_reflection_matrix()*(R.each_col() - origin_)).eval().each_col() + origin_;
	}

	// mesh transformation
	void TransReflect::apply_area_coords(arma::Mat<fltp> &R, const fltp /*time*/) const{
		R.row(1) *= -1;
	}

	// apply transformation to coordinates and vectors
	void TransReflect::apply_vectors(
		const arma::Mat<fltp> &/*R*/, 
		arma::Mat<fltp> &V, 
		const char str, 
		const fltp /*time*/) const{
		
		// apply to vectors (invert direction)
		V = create_reflection_matrix()*V;

		// inverse direction to keep righthandedness
		if(str=='D')V *= -1;
	}

	// apply transformation to mesh
	void TransReflect::apply_mesh(
		arma::Mat<arma::uword> &n, 
		const arma::uword n_dim,
		const arma::uword /*num_nodes*/, 
		const fltp /*time*/) const{

		// volume meshes
		if(n_dim==3){
			if(n.n_rows==8){
				// flip face 1
				n.rows(0,n.n_rows/2-1) = arma::flipud(n.rows(0,n.n_rows/2-1));

				// flip face 2
				n.rows(n.n_rows/2,n.n_rows-1) = arma::flipud(n.rows(n.n_rows/2,n.n_rows-1));
			}
			else if(n.n_rows==4)n=cmn::Tetrahedron::invert_elements(n);
		}

		// surface meshes
		else if(n_dim==2){
			if(n.n_rows==4)n = cmn::Quadrilateral::invert_elements(n);
			else if(n.n_rows==3)n = cmn::Triangle::invert_elements(n);
		}

		// done
		return;
	}

	// apply transformation to mesh
	void TransReflect::apply_area_mesh(
		arma::Mat<arma::uword> &n, 
		const arma::uword /*num_nodes*/, 
		const fltp /*time*/) const{
		// flip face 1
		n.rows(0,n.n_rows/2-1) = arma::flipud(n.rows(0,n.n_rows/2-1));

		// flip face 2
		n.rows(n.n_rows/2,n.n_rows-1) = arma::flipud(n.rows(n.n_rows/2,n.n_rows-1));

		// flip elements
		if(n.n_rows==4)n = arma::flipud(n);
	}


	// check validity
	bool TransReflect::is_valid(const bool enable_throws) const{
		if(arma::as_scalar(cmn::Extra::vec_norm(plane_vector_))<1e-12){if(enable_throws){rat_throw_line("plane vector has no length");} return false;};
		return true;
	}


	// get type
	std::string TransReflect::get_type(){
		return "rat::mdl::transreflect";
	}

	// method for serialization into json
	void TransReflect::serialize(Json::Value &js, cmn::SList &list) const{
		Trans::serialize(js,list);
		js["type"] = get_type();
		js["origin_x"] = origin_(0);
		js["origin_y"] = origin_(1);
		js["origin_z"] = origin_(2);
		js["plane_vector_x"] = plane_vector_(0);
		js["plane_vector_y"] = plane_vector_(1);
		js["plane_vector_z"] = plane_vector_(2);
	}

	// method for deserialisation from json
	void TransReflect::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		Trans::deserialize(js,list,factory_list,pth);
		set_origin({
			js["origin_x"].ASFLTP(),
			js["origin_y"].ASFLTP(),
			js["origin_z"].ASFLTP()});
		set_plane_vector({
			js["plane_vector_x"].ASFLTP(),
			js["plane_vector_y"].ASFLTP(),
			js["plane_vector_z"].ASFLTP()});
	}

}}