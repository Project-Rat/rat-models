// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "drive.hh"

// code specific to Rat
namespace rat{namespace mdl{


	// get scaling for arrays
	arma::Row<fltp> Drive::get_scaling_vec(
		const arma::Row<fltp> &positions, const fltp time, 
		const arma::uword derivative) const{
		
		// allocate values for each position
		arma::Row<fltp> values(positions.n_elem);

		// walk over positions and get the respective value
		for(arma::uword i=0;i<positions.n_elem;i++)
			values(i)=get_scaling(positions(i), time, derivative);

		// return values
		return values;
	}

	// get inflection points
	arma::Col<fltp> Drive::get_inflection_points()const{
		return arma::Col<fltp>{};
	}

	// getters
	fltp Drive::get_v1() const{
		return -RAT_CONST(0.5);
	}

	fltp Drive::get_v2() const{
		return RAT_CONST(0.5);
	}

	// get type
	std::string Drive::get_type(){
		return "rat::mdl::drive";
	}

	// method for serialization into json
	void Drive::serialize(
		Json::Value &js, 
		cmn::SList &list) const{
		Node::serialize(js, list);
		js["type"] = get_type();
	}

	// method for deserialisation from json
	void Drive::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		Node::deserialize(js,list,factory_list,pth);
	}


}}