// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calclength.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcLength::CalcLength(){
		set_name("Length"); 
	}

	CalcLength::CalcLength(const ShModelPr &model) : CalcLength(){
		set_model(model);
	}

	// factory methods
	ShCalcLengthPr CalcLength::create(){
		return std::make_shared<CalcLength>();
	}

	ShCalcLengthPr CalcLength::create(const ShModelPr &model){
		return std::make_shared<CalcLength>(model);
	}

	// setters
	void CalcLength::set_group_circuits(const bool group_circuits){
		group_circuits_ = group_circuits;
	}

	// getters
	bool CalcLength::get_group_circuits()const{
		return group_circuits_;
	}

	// calculate with inductance data output
	ShLengthDataPr CalcLength::calculate_length(
		const fltp time, 
		const cmn::ShLogPr &lg, 
		const ShSolverCachePr& /*cache*/){

		// when calculating the settings must be valid
		is_valid(true);

		// check input model
		if(model_==NULL)rat_throw_line("model not set");
		model_->is_valid(true);

		// settings for mesh creation
		MeshSettings mesh_settings;
		mesh_settings.time = time;
		mesh_settings.low_poly = false;
		mesh_settings.combine_sections = true;

		// create meshes
		const std::list<ShMeshDataPr> meshes = model_->create_meshes({},mesh_settings);

		// get number of edges
		const arma::uword num_coils = meshes.size();

		// allocate conductor length array
		arma::field<std::string> coil_names(1,num_coils);
		arma::Row<arma::uword> circuit_index(num_coils);
		arma::Row<fltp> volume(num_coils);
		arma::Row<fltp> num_turns(num_coils);
		arma::Row<fltp> ell_gen(num_coils);
		arma::Row<fltp> ell(num_coils);	

		// walk over coils
		arma::uword idx = 0;
		for(auto it=meshes.begin();it!=meshes.end();it++,idx++){
			// get mesh
			const ShMeshDataPr& mesh = (*it);

			// get name
			coil_names(idx) = mesh->get_name();

			// calulate elemental volume
			const arma::Row<fltp> element_volume = mesh->calc_volume();

			// calculate total volume
			volume(idx) = arma::accu(element_volume);

			// get number turns
			num_turns(idx) = mesh->get_number_turns();

			// get length from frame
			ell_gen(idx) = arma::accu(mesh->get_ell_frame());

			// get circuit
			circuit_index(idx) = mesh->get_circuit_index();

			// calculate ell from volume
			ell(idx) = num_turns(idx)*mesh->calc_ell();
		}


		// sort by circuit
		if(group_circuits_){
			// make list of circuit names
			std::map<arma::uword, ShCircuitPr> id2circuit;
			for(auto it=circuits_.begin();it!=circuits_.end();it++)
				id2circuit[(*it).second->get_circuit_id()] = (*it).second;

			// sort by circuit
			const arma::Col<arma::uword> sort_id = arma::sort_index(circuit_index.t());
			circuit_index = circuit_index.cols(sort_id);
			coil_names = cmn::Extra::sort_field(coil_names, sort_id);
			volume = volume.cols(sort_id);
			num_turns = num_turns.cols(sort_id);
			ell_gen = ell_gen.cols(sort_id);
			ell = ell.cols(sort_id);

			// find unique parts
			const arma::Mat<arma::uword> M = cmn::Extra::find_sections(circuit_index);

			// walk over sections
			for(arma::uword i=0;i<M.n_cols;i++){
				// get start and end
				const arma::uword idx1 = M(0,i);
				const arma::uword idx2 = M(1,i);

				// get circuit index
				const arma::uword cidx = circuit_index(idx1);

				// try to find circuit in list
				auto it2 = id2circuit.find(cidx);
				if(it2==id2circuit.end())coil_names(idx1) = "Circuit" + std::to_string(cidx);
				else coil_names(idx1) = (*it2).second->get_name();		
				volume(idx1) = arma::accu(volume.cols(idx1,idx2));
				num_turns(idx1) = arma::accu(num_turns.cols(idx1,idx2));
				ell_gen(idx1) = arma::accu(ell_gen.cols(idx1,idx2));
				ell(idx1) = arma::accu(ell.cols(idx1,idx2));
			}

			// keep only first
			coil_names = cmn::Extra::sort_field(coil_names, M.row(0).t());
			volume = volume.cols(M.row(0));
			num_turns = num_turns.cols(M.row(0));
			ell_gen = ell_gen.cols(M.row(0));
			ell = ell.cols(M.row(0));
		}

		// create length data
		const ShLengthDataPr ld = LengthData::create(coil_names, volume, num_turns, ell_gen, ell);

		// show in log
		ld->display(lg);

		// return length data object
		return ld;
	}

	// generalized calculation
	std::list<ShDataPr> CalcLength::calculate(
		const fltp time, 
		const cmn::ShLogPr &lg, 
		const ShSolverCachePr& cache){
		return {calculate_length(time,lg,cache)};
	}

	// is valid
	bool CalcLength::is_valid(const bool /*enable_throws*/) const{
		return true;
	}


	// serialization type
	std::string CalcLength::get_type(){
		return "rat::mdl::calclength";
	}

	// serialization
	void CalcLength::serialize(Json::Value &js, cmn::SList &list) const{
		CalcLeaf::serialize(js,list);
		js["type"] = get_type();
		js["group_circuits"] = group_circuits_;
	}
		
	// deserialization
	void CalcLength::deserialize(const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		CalcLeaf::deserialize(js,list,factory_list,pth);
		set_group_circuits(js["group_circuits"].asBool());
	}

}}