// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathcctcustom.hh"

// common headers
#include "rat/common/extra.hh"

// model headers
#include "transbend.hh"
#include "pathcct.hh"
#include "darboux.hh"
#include "pathbezier.hh"
#include "drivercct.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// function for acquiring number of poles
	arma::uword CCTHarmonic::get_num_poles() const{
		return num_poles_; 
	}

	void CCTHarmonic::set_num_poles(const arma::uword num_poles){
		num_poles_ = num_poles;
	}

	// void CCTHarmonic::set_reference_radius(const rat::fltp reference_radius){
	// 	reference_radius_ = reference_radius;
	// }

	// function for acquiring number of poles
	bool CCTHarmonic::get_is_skew() const{
		return is_skew_; 
	}

	void CCTHarmonic::set_is_skew(const bool is_skew){
		is_skew_ = is_skew;
	}

	void CCTHarmonic::set_use_skew_angle(const bool use_skew_angle){
		use_skew_angle_ = use_skew_angle;
	}
	
	bool CCTHarmonic::get_use_skew_angle()const{
		return use_skew_angle_;
	}

	// fltp CCTHarmonic::get_reference_radius()const{
	// 	return reference_radius_;
	// }

	// vallidity check
	bool CCTHarmonic::is_valid(const bool enable_throws) const{
		if(num_poles_<=0){if(enable_throws){rat_throw_line("number of poles be larger than zero");} return false;};
		if(b_<=-RAT_CONST(1.0)){if(enable_throws){rat_throw_line("b can not be smaller than minus one");} return false;};
		return true;
	}

	void CCTHarmonic::set_b(const fltp b){
		b_ = b;
	}
	
	fltp CCTHarmonic::get_b()const{
		return b_;
	}

	// skew angle to amplitude conversion
	arma::field<arma::Row<fltp> > CCTHarmonic::skew2amplitude(
		const arma::field<arma::Row<fltp> > &rho, 
		const arma::field<arma::Row<fltp> > &alpha, 
		const arma::uword num_poles){
		
		// allocate
		arma::field<arma::Row<fltp> > amplitude(4);

		// convert number of poles to double
		const fltp np = fltp(num_poles);

		// calculate tangens of alpha and derivative
		const arma::Row<fltp> ff = arma::tan(alpha(0));
		const arma::Row<fltp> vf = alpha(1)%arma::square(1.0/arma::cos(alpha(0)));
		const arma::Row<fltp> af = arma::square(1.0/arma::cos(alpha(0)))%(2*arma::square(alpha(1))%arma::tan(alpha(0))+alpha(2));
		const arma::Row<fltp> jf = arma::square(1.0/arma::cos(alpha(0)))%(2*alpha(1)%(arma::tan(alpha(0))%(2*arma::square(alpha(1))%arma::tan(alpha(0))+3*alpha(2))+arma::square(alpha(1))%arma::square(1.0/arma::cos(alpha(1))))+alpha(3));

		// second function
		const arma::Row<fltp> gg = rho(0)/np;
		const arma::Row<fltp> vg = rho(1)/np;
		const arma::Row<fltp> ag = rho(2)/np;
		const arma::Row<fltp> jg = rho(3)/np;

		// derivative of two functions
		amplitude(0) = ff%gg;
		amplitude(1) = vf%gg + ff%vg; 
		amplitude(2) = af%gg + 2*vf%vg + ff%ag;
		amplitude(3) = jf%gg + af%vg + 2*af%vg + 2*vf%ag + vf%ag + ff%jg;

		// return the amplitude and derivatives
		return amplitude;
	}


	// calculate axial offset
	arma::field<arma::Row<fltp> > CCTHarmonic::calc_z_core(
		const arma::field<arma::Row<fltp> > &amplitude, 
		const arma::Row<fltp> &theta)const{

		// allocate z and derivatives thereoff
		arma::field<arma::Row<fltp> > z(4);

		// convert number of poles to float
		const fltp np = fltp(num_poles_);

		// precalculate
		const arma::Row<fltp> sin_np_theta = arma::sin(np*theta);
		const arma::Row<fltp> cos_np_theta = arma::cos(np*theta);

		// get derivatives from amplitude
		const arma::Row<fltp>& hh = amplitude(0);
		const arma::Row<fltp>& vh = amplitude(1);
		const arma::Row<fltp>& ah = amplitude(2);
		const arma::Row<fltp>& jh = amplitude(3);

		// regular sine and cosine
		if(b_==0){
			if(is_skew_){
				const arma::Row<fltp> gg = cos_np_theta;
				const arma::Row<fltp> vg = -np*sin_np_theta;
				const arma::Row<fltp> ag = -np*np*cos_np_theta;
				const arma::Row<fltp> jg = np*np*np*sin_np_theta;

				// derivative of two functions
				z(0) = hh%gg;
				z(1) = vh%gg + hh%vg; 
				z(2) = ah%gg + 2*vh%vg + hh%ag;
				z(3) = jh%gg + ah%vg + 2*ah%vg + 2*vh%ag + vh%ag + hh%jg;
			}

			// normal harmonics
			else{
				const arma::Row<fltp> gg = sin_np_theta;
				const arma::Row<fltp> vg = np*cos_np_theta;
				const arma::Row<fltp> ag = -np*np*sin_np_theta;
				const arma::Row<fltp> jg = -np*np*np*cos_np_theta;

				// derivative of two functions
				z(0) = hh%gg;
				z(1) = vh%gg + hh%vg; 
				z(2) = ah%gg + 2*vh%vg + hh%ag;
				z(3) = jh%gg + ah%vg + 2*ah%vg + 2*vh%ag + vh%ag + hh%jg;
			}
		}

		// flattened tops
		else{
			// const fltp b2 = b_; const fltp b4 = b2*b2;


			// skew harmonics
			if(is_skew_){
				const arma::Row<fltp> ff = RAT_CONST(1.0)/sqrt(b_*arma::square(cos_np_theta)+1);
				const arma::Row<fltp> vf = (b_*np*cos_np_theta%sin_np_theta)/arma::pow(b_*arma::square(cos_np_theta)+1,3.0/2);
				const arma::Row<fltp> af = -(b_*(np*np)*(b_*arma::pow(sin_np_theta,4)+2*arma::square(sin_np_theta)-b_-1))/arma::pow(b_*arma::square(cos_np_theta)+1,5.0/2);
				const arma::Row<fltp> jf = -(b_*std::pow(np,3)*cos_np_theta%(b_*(arma::square(cos_np_theta)%(b_*(arma::square(cos_np_theta)-6)-10)+9)+4)%sin_np_theta)/arma::pow(b_*arma::square(cos_np_theta)+1,7.0/2);

				const arma::Row<fltp> gg = cos_np_theta;
				const arma::Row<fltp> vg = -np*sin_np_theta;
				const arma::Row<fltp> ag = -np*np*cos_np_theta;
				const arma::Row<fltp> jg = np*np*np*sin_np_theta;

				// z(0) = ff%gg;
				// z(1) = vf%gg + ff%vg; 
				// z(2) = af%gg + 2*vf%vg + ff%ag;
				// z(3) = jf%gg + af%vg + 2*af%vg + 2*vf%ag + vf%ag + ff%jg;

				// derivative of three functions
				z(0) = ff%gg%hh;
				z(1) = ff%gg%vh+(ff%vg+vf%gg)%hh;
				z(2) = ff%gg%ah+2*(ff%vg+vf%gg)%vh+(ff%ag+2*vf%vg+af%gg)%hh;
				z(3) = ff%gg%jh+3*(ff%vg+vf%gg)%ah+3*(ff%ag+2*vf%vg+af%gg)%vh+(ff%jg+3*(vf%ag+af%vg)+jf%gg)%hh;
			}

			// normal harmonics
			else{
				const arma::Row<fltp> ff = RAT_CONST(1.0)/arma::sqrt(b_*arma::square(sin_np_theta)+1);
				const arma::Row<fltp> vf = -(b_*np*cos_np_theta%sin_np_theta)/arma::pow(b_*arma::square(sin_np_theta)+1,3.0/2);
				const arma::Row<fltp> af = -(b_*(np*np)*(b_*arma::pow(cos_np_theta,4)+2*arma::square(cos_np_theta)-b_-1))/arma::pow(b_*arma::square(sin_np_theta)+1,5.0/2);
				const arma::Row<fltp> jf = (b_*std::pow(np,3)*cos_np_theta%(b_*(arma::square(cos_np_theta)%(b_*(arma::square(cos_np_theta)+4)+10)-5*b_-1)+4)%sin_np_theta)/arma::pow(b_*arma::square(sin_np_theta)+1,7.0/2);

				const arma::Row<fltp> gg = sin_np_theta;
				const arma::Row<fltp> vg = np*cos_np_theta;
				const arma::Row<fltp> ag = -np*np*sin_np_theta;
				const arma::Row<fltp> jg = -np*np*np*cos_np_theta;

				// z(0) = ff%gg;
				// z(1) = vf%gg + ff%vg;
				// z(2) = af%gg + 2*vf%vg + ff%ag;
				// z(3) = jf%gg + af%vg + 2*af%vg + 2*vf%ag + vf%ag + ff%jg;

				// derivative of three functions
				z(0) = ff%gg%hh;
				z(1) = ff%gg%vh+(ff%vg+vf%gg)%hh;
				z(2) = ff%gg%ah+2*(ff%vg+vf%gg)%vh+(ff%ag+2*vf%vg+af%gg)%hh;
				z(3) = ff%gg%jh+3*(ff%vg+vf%gg)%ah+3*(ff%ag+2*vf%vg+af%gg)%vh+(ff%jg+3*(vf%ag+af%vg)+jf%gg)%hh;
			}
		}

		// // check 90 deg case
		// const arma::Col<arma::uword> idx = arma::find(alpha==arma::Datum<fltp>::pi/2);
		// for(arma::uword i=0;i<z.n_elem;i++)z(i)(idx).fill(0.0);

		// check output
		#ifndef NDEBUG
		for(arma::uword i=0;i<z.n_elem;i++)
			assert(z(i).is_finite());
		#endif

		// return coordinates
		return z;
	}

	// // setup coordinates and orientation vectors
	// void CCTHarmonic::radius_zmod(
	// 	arma::field<arma::Row<rat::fltp> >&c, 
	// 	const arma::field<arma::Row<rat::fltp> >& rho)const{

	// 	// check if this feature is enabled
	// 	if(reference_radius_==0.0)return;

	// 	// scaling function
	// 	const arma::Row<fltp> hh = arma::pow(rho(0)/reference_radius_,rat::fltp(num_poles_)/2.0);
	// 	const arma::Row<fltp> vh = (rat::fltp(num_poles_)*arma::pow(rho(0)/reference_radius_,rat::fltp(num_poles_)/2)%rho(1))/(2*rho(0));
	// 	const arma::Row<fltp> ah = (rat::fltp(num_poles_)*arma::pow(rho(0)/reference_radius_,rat::fltp(num_poles_)/2)%(2*rho(0)%rho(2)+(rat::fltp(num_poles_)-2)*arma::square(rho(1))))/(4*arma::square(rho(0)));
	// 	const arma::Row<fltp> jh = (rat::fltp(num_poles_)*arma::pow(rho(0)/reference_radius_,rat::fltp(num_poles_)/2)%(4*arma::square(rho(0))%rho(3)+(6*rat::fltp(num_poles_)-12)*rho(0)%rho(1)%rho(2)+((rat::fltp(num_poles_)*rat::fltp(num_poles_))-6*rat::fltp(num_poles_)+8)*arma::pow(rho(1),3)))/(8*arma::pow(rho(0),3));
			
	// 	// const arma::Row<fltp> hh = arma::pow(rho(0),rat::fltp(num_poles_)/2.0);
	// 	// const arma::Row<fltp> vh = (num_poles_*arma::pow(rho(0),rat::fltp(num_poles_)/2-1)%rho(1))/2;
	// 	// const arma::Row<fltp> ah = (num_poles_*arma::pow(rho(0),rat::fltp(num_poles_)/2-2)%(2*rho(0)%rho(2)+(rat::fltp(num_poles_)-2)*arma::square(rho(1))))/4;
	// 	// const arma::Row<fltp> jh = (num_poles_*arma::pow(rho(0),rat::fltp(num_poles_)/2-3)%(4*arma::square(rho(0))%rho(3) + (6*rat::fltp(num_poles_)-12)*rho(0)%rho(1)%rho(2) + (rat::fltp(num_poles_)*rat::fltp(num_poles_)-6*rat::fltp(num_poles_)+8)*arma::pow(rho(1),3)))/8;

	// 	// copy previous values
	// 	const arma::Row<fltp> gg = c(0); const arma::Row<fltp> vg = c(1);
	// 	const arma::Row<fltp> ag = c(2); const arma::Row<fltp> jg = c(3);

	// 	// derivative of two functions
	// 	c(0) = hh%gg;
	// 	c(1) = vh%gg + hh%vg; 
	// 	c(2) = ah%gg + 2*vh%vg + hh%ag;
	// 	c(3) = jh%gg + ah%vg + 2*ah%vg + 2*vh%ag + vh%ag + hh%jg;

	// 	// done
	// 	return;
	// }

	// get type
	std::string CCTHarmonic::get_type(){
		return "rat::mdl::cctharmonic";
	}

	// get normalization setting
	bool CCTHarmonic::get_normalize_length() const{
		return normalize_length_;
	}

	// set normalization setting
	void CCTHarmonic::set_normalize_length(const bool normalize_length){
		normalize_length_ = normalize_length;
	}

	// theta fun
	arma::Row<fltp> CCTHarmonic::calc_position(
		const arma::Row<fltp> &theta, 
		const fltp thetamin, 
		const fltp thetamax,
		const bool normalize_length){
		if(normalize_length){
			return (theta-thetamin)/(thetamax-thetamin) - RAT_CONST(0.5);
		}else{
			return theta/arma::Datum<fltp>::tau;
		}
	}

	// calculate dposition dtheta
	fltp CCTHarmonic::calc_dposition(
		const fltp thetamin, const fltp thetamax, 
		const bool normalize_length){
		if(normalize_length){
			return RAT_CONST(1.0)/(thetamax-thetamin);
		}else{
			return RAT_CONST(1.0)/arma::Datum<fltp>::tau;
		}
	}

	// method for serialization into json
	void CCTHarmonic::serialize(
		Json::Value &js, cmn::SList &list) const{
		
		// parent objects
		Node::serialize(js,list);

		// type
		js["type"] = get_type();

		// harmonic
		js["num_poles"] = static_cast<int>(num_poles_);
		js["is_skew"] = is_skew_;

		// use angle
		js["use_skew_angle"] = use_skew_angle_; 

		// normalization
		js["normalize_length"] = normalize_length_;

		// // reference radius
		// js["reference_radius"] = reference_radius_;

		// stumpness
		js["b2"] = b_;
	}

	// method for deserialisation from json
	void CCTHarmonic::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		
		// parent objects
		Node::deserialize(js,list,factory_list,pth);

		// harmonic
		set_num_poles(js["num_poles"].asUInt64());
		set_is_skew(js["is_skew"].asBool());

		// use angle 
		set_use_skew_angle(js["use_skew_angle"].asBool()); 

		// normalization
		set_normalize_length(js["normalize_length"].asBool());

		// reference radius
		// set_reference_radius(js["reference_radius"].ASFLTP());

		// stumpness
		if(js.isMember("b2"))set_b(js["b2"].ASFLTP());
		if(js.isMember("b"))set_b(js["b"].ASFLTP()*js["b"].ASFLTP()); // backwards compatibility v2.013.0
	}



	// default constructor
	CCTHarmonicInterp::CCTHarmonicInterp(){
		set_name("Harmonic");
	}

	// constructor
	CCTHarmonicInterp::CCTHarmonicInterp(
		const arma::uword num_poles, const bool is_skew, 
		const arma::Row<fltp> &turn, const arma::Row<fltp> &a,
		const bool normalize_length) : CCTHarmonicInterp(){

		// set to self
		num_poles_ = num_poles; is_skew_ = is_skew;
		turn_ = turn; a_ = a;
		normalize_length_ = normalize_length;
	}

	// factory
	ShCCTHarmonicInterpPr CCTHarmonicInterp::create(){
		return std::make_shared<CCTHarmonicInterp>();
	}

	// factory
	ShCCTHarmonicInterpPr CCTHarmonicInterp::create(
		const arma::uword num_poles, const bool is_skew, 
		const arma::Row<fltp> &turn, const arma::Row<fltp> &a,
		const bool normalize_length){
		return std::make_shared<CCTHarmonicInterp>(num_poles,is_skew,turn,a,normalize_length);
	}

	// calculate offset in z
	arma::field<arma::Row<fltp> > CCTHarmonicInterp::calc_z(
		const arma::Row<fltp> &theta, const arma::field<arma::Row<fltp> > &rho,
		const fltp thetamin, const fltp thetamax, const fltp /*time*/) const{

		// calculate position
		const arma::Row<fltp> position = calc_position(theta, thetamin, thetamax, normalize_length_);
		// const fltp dposition_dtheta = calc_dposition(thetamin, thetamax, normalize_length_);
		
		// amplitude
		arma::field<arma::Row<fltp> > amplitude(4);

		// get amplitude and angle
		if(use_skew_angle_){
			// get angle from drive and derivatives
			arma::field<arma::Row<fltp> > alpha(4);
			arma::Col<fltp> a;
			cmn::Extra::interp1(turn_.t(),a_.t(),position.t(),a,"linear",true);
			alpha(0) = a.t();
			for(arma::uword j=1;j<4;j++)
				alpha(j) = arma::Row<fltp>(position.n_elem,arma::fill::zeros);

			// convert skew angle to amplitude
			amplitude = skew2amplitude(rho,alpha,num_poles_);

		}else{
			arma::Col<fltp> a;
			cmn::Extra::interp1(turn_.t(),a_.t(),position.t(),a,"linear",true);
			amplitude(0) = a.t();
			for(arma::uword j=1;j<4;j++)
				amplitude(j) = arma::Row<fltp>(position.n_elem,arma::fill::zeros);
		}
		
		// use common function for calculating z
		return calc_z_core(amplitude, theta);
	}

	// get type
	std::string CCTHarmonicInterp::get_type(){
		return "rat::mdl::cctharmonicinterp";
	}

	// method for serialization into json
	void CCTHarmonicInterp::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		CCTHarmonic::serialize(js,list);
		js["type"] = get_type();
		js["turn"] = Node::serialize_matrix(turn_);
		js["amplitude"] = Node::serialize_matrix(a_);
	}

	// method for deserialisation from json
	void CCTHarmonicInterp::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent objects
		CCTHarmonic::deserialize(js,list,factory_list,pth);
		turn_ = Node::deserialize_matrix(js["turn"]);
		a_ = Node::deserialize_matrix(js["amplitude"]);
	}





	// constructor
	CCTHarmonicEquation::CCTHarmonicEquation(
		const arma::uword num_poles, 
		const bool is_skew, const CCTCustomFun &fun){
		fun_ = fun; num_poles_ = num_poles; is_skew_ = is_skew;
	}

	// factory
	ShCCTHarmonicEquationPr CCTHarmonicEquation::create(
		const arma::uword num_poles, const bool is_skew, const CCTCustomFun &fun){
		return std::make_shared<CCTHarmonicEquation>(num_poles,is_skew,fun);
	}

	// calculate offset in z
	arma::field<arma::Row<fltp> > CCTHarmonicEquation::calc_z(
		const arma::Row<fltp> &theta, const arma::field<arma::Row<fltp> > &rho,
		const fltp thetamin, const fltp thetamax, const fltp time) const{

		// calculate position
		const arma::Row<fltp> position = calc_position(theta, thetamin, thetamax, normalize_length_);
		// const fltp dposition_dtheta = calc_dposition(thetamin, thetamax, normalize_length_);

		// amplitude
		arma::field<arma::Row<fltp> > amplitude(4);

		// get amplitude and angle
		if(use_skew_angle_){
			// get angle from drive and derivatives
			arma::field<arma::Row<fltp> > alpha(4);
			alpha(0) = fun_(position, time);
			for(arma::uword j=1;j<4;j++)
				alpha(j) = arma::Row<fltp>(position.n_elem,arma::fill::zeros);

			// convert skew angle to amplitude
			amplitude = skew2amplitude(rho,alpha,num_poles_);

		}else{
			amplitude(0) = fun_(position, time);
			for(arma::uword j=1;j<4;j++)
				amplitude(j) = arma::Row<fltp>(position.n_elem,arma::fill::zeros);
		}
		
		// use common function for calculating z
		return calc_z_core(amplitude, theta);
	}


	// default constructor
	CCTHarmonicDrive::CCTHarmonicDrive(){
		set_name("Harmonic");
	}

	// constructor
	CCTHarmonicDrive::CCTHarmonicDrive(
		const arma::uword num_poles, 
		const bool is_skew, 
		const ShDrivePr &drive, 
		const bool use_skew_angle){

		// set  to self
		set_num_poles(num_poles); set_is_skew(is_skew); 
		set_drive(drive); set_use_skew_angle(use_skew_angle);

		// decide on name
		if(num_poles_==1)set_name("Dipole");
		else if(num_poles_==2)set_name("Quadrupole");
		else if(num_poles_==3)set_name("Hexapole");
		else if(num_poles_==4)set_name("Octupole");
		else if(num_poles_==5)set_name("Decapole");
		else set_name("Manypole");
	}

	// constructor
	CCTHarmonicDrive::CCTHarmonicDrive(
		const arma::uword num_poles, 
		const bool is_skew, 
		const fltp amplitude, 
		const bool use_skew_angle) : 
		CCTHarmonicDrive(num_poles,is_skew,DriveDC::create(amplitude),use_skew_angle){
	}

	// factory
	ShCCTHarmonicDrivePr CCTHarmonicDrive::create(){
		return std::make_shared<CCTHarmonicDrive>();
	}

	// factory
	ShCCTHarmonicDrivePr CCTHarmonicDrive::create(
		const arma::uword num_poles, 
		const bool is_skew, 
		const ShDrivePr &drive, 
		const bool use_skew_angle){
		return std::make_shared<CCTHarmonicDrive>(num_poles,is_skew,drive,use_skew_angle);
	}

	// factory
	ShCCTHarmonicDrivePr CCTHarmonicDrive::create(
		const arma::uword num_poles, 
		const bool is_skew, 
		const fltp amplitude, 
		const bool use_skew_angle){
		return std::make_shared<CCTHarmonicDrive>(num_poles,is_skew,amplitude,use_skew_angle);
	}
		
	// setters
	const ShDrivePr& CCTHarmonicDrive::get_drive() const{
		assert(drive_!=NULL);
		return drive_;
	}


	// getters
	void CCTHarmonicDrive::set_drive(const ShDrivePr &drive){
		assert(drive!=NULL);
		drive_ = drive;
	}

	// calculate offset in z and its derivatives
	arma::field<arma::Row<fltp> > CCTHarmonicDrive::calc_z(
		const arma::Row<fltp> &theta, 
		const arma::field<arma::Row<fltp> > &rho,
		const fltp thetamin, const fltp thetamax, 
		const fltp time) const{

		// calculate position
		const arma::Row<fltp> position = calc_position(theta, thetamin, thetamax, normalize_length_);
		const fltp dposition_dtheta = calc_dposition(thetamin,thetamax, normalize_length_);

		// amplitude
		arma::field<arma::Row<fltp> > amplitude(4);

		// get amplitude and angle
		if(use_skew_angle_){
			// get angle from drive and derivatives
			arma::field<arma::Row<fltp> > alpha(4);
			for(arma::uword j=0;j<4;j++)
				alpha(j) = std::pow(dposition_dtheta,j)*drive_->get_scaling_vec(position,time,j);

			// convert skew angle to amplitude
			amplitude = skew2amplitude(rho,alpha,num_poles_);

			// static std::mutex lock; lock.lock();
			// std::cout<<normalize_length_<<std::endl;
			// std::cout<<position<<std::endl;
			// lock.unlock();

		}else{
			for(arma::uword j=0;j<4;j++)
				amplitude(j) = std::pow(dposition_dtheta,j)*drive_->get_scaling_vec(position,time,j);
		}

		// calcultae z
		arma::field<arma::Row<fltp> > c = calc_z_core(amplitude, theta);

		// use common function for calculating z
		return c; //calc_z_core(amplitude, theta);
	}

	// vallidity check
	bool CCTHarmonicDrive::is_valid(const bool enable_throws) const{
		if(!CCTHarmonic::is_valid(enable_throws))return false;
		if(drive_==NULL){if(enable_throws){rat_throw_line("drive points to NULL");} return false;};
		if(!drive_->is_valid(enable_throws))return false;
		return true;
	}

	// get type
	std::string CCTHarmonicDrive::get_type(){
		return "rat::mdl::cctharmonicdrive";
	}

	// method for serialization into json
	void CCTHarmonicDrive::serialize(
		Json::Value &js, cmn::SList &list) const{
		
		// parent objects
		CCTHarmonic::serialize(js,list);

		// type
		js["type"] = get_type();

		// serialize drive
		js["harmonic_drive"] = cmn::Node::serialize_node(get_drive(), list);
	}

	// method for deserialisation from json
	void CCTHarmonicDrive::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		
		// parent objects
		CCTHarmonic::deserialize(js,list,factory_list,pth);

		// drive
		if(js.isMember("harmonic_drive"))
			set_drive(cmn::Node::deserialize_node<Drive>(js["harmonic_drive"], list, factory_list, pth));

		// backwards compatibility for version 2.013.0
		if(js.isMember("drive")){
			const ShDrivePr drive = cmn::Node::deserialize_node<Drive>(js["drive"], list, factory_list, pth);
			drive->rescale(1.0/num_poles_);
			set_drive(drive);
		}
	}




	// default constructor
	PathCCTCustom::PathCCTCustom(){
		set_name("Custom CCT");
	}

	// factory
	ShPathCCTCustomPr PathCCTCustom::create(){
		return std::make_shared<PathCCTCustom>();
	}

	// set range
	void PathCCTCustom::set_range(const fltp nt1, const fltp nt2){
		set_nt1(nt1); set_nt2(nt2); set_nt1nrm(nt1); set_nt2nrm(nt2);
	}

	// set range
	void PathCCTCustom::set_range(const fltp nt1, const fltp nt2, const fltp nt1nrm, const fltp nt2nrm){
		set_nt1(nt1); set_nt2(nt2); set_nt1nrm(nt1nrm); set_nt2nrm(nt2nrm);
	}

	// get normalization setting
	bool PathCCTCustom::get_normalize_length() const{
		return normalize_length_;
	}

	// set normalization setting
	void PathCCTCustom::set_normalize_length(const bool normalize_length){
		normalize_length_ = normalize_length;
	}

	

	// set range
	void PathCCTCustom::set_nt1(const fltp nt1){
		nt1_ = nt1;
	}

	// set range
	void PathCCTCustom::set_nt2(const fltp nt2){
		nt2_ = nt2;
	}

	void PathCCTCustom::set_nt3(const fltp nt3){
		nt3_ = nt3;
	}

	// set range
	void PathCCTCustom::set_nt4(const fltp nt4){
		nt4_ = nt4;
	}

	// set range for drive normalization
	void PathCCTCustom::set_nt1nrm(const fltp nt1nrm){
		nt1nrm_ = nt1nrm;
	}

	// set range for drive normalization
	void PathCCTCustom::set_nt2nrm(const fltp nt2nrm){
		nt2nrm_ = nt2nrm;
	}

	// set range for drive normalization
	void PathCCTCustom::set_num_sect_per_turn(const arma::uword num_sect_per_turn){
		num_sect_per_turn_ = num_sect_per_turn;
	}

	// set range
	fltp PathCCTCustom::get_nt1() const{
		return nt1_;
	}

	// set range
	fltp PathCCTCustom::get_nt2() const{
		return nt2_;
	}

	// set range
	fltp PathCCTCustom::get_nt3() const{
		return nt3_;
	}

	// set range
	fltp PathCCTCustom::get_nt4() const{
		return nt4_;
	}


	// set range
	fltp PathCCTCustom::get_nt1nrm() const{
		return nt1nrm_;
	}

	// set range
	fltp PathCCTCustom::get_nt2nrm() const{
		return nt2nrm_;
	}

	// set the direction of the layer
	void PathCCTCustom::set_is_reverse(const bool is_reverse){
		is_reverse_ = is_reverse;
	}

	// set the direction of the layer
	bool PathCCTCustom::get_is_reverse() const{
		return is_reverse_;
	}

	// set the solenoid option
	void PathCCTCustom::set_is_solenoid(const bool is_solenoid){
		is_solenoid_ = is_solenoid;
	}

	// get the solenoid option
	bool PathCCTCustom::get_is_solenoid() const{
		return is_solenoid_;
	}

	// set the solenoid option
	void PathCCTCustom::set_is_reverse_solenoid(const bool is_reverse_solenoid){
		is_reverse_solenoid_ = is_reverse_solenoid;
	}

	// get the solenoid option
	bool PathCCTCustom::get_is_reverse_solenoid() const{
		return is_reverse_solenoid_;
	}

	// set the direction of the layer
	void PathCCTCustom::set_bending_arc_length(const fltp bending_arc_length){
		bending_arc_length_ = bending_arc_length;
	}

	// set the direction of the layer
	void PathCCTCustom::set_bending_radius(const fltp bending_radius){
		bending_radius_ = bending_radius;
	}

	// get the bending arc length
	fltp PathCCTCustom::get_bending_arc_length() const{
		return bending_arc_length_;
	}

	// get the bending radius
	fltp PathCCTCustom::get_bending_radius() const{
		return bending_radius_;
	}

	// add harmonic
	arma::uword PathCCTCustom::add_harmonic(const ShCCTHarmonicPr &harm){
		// set new source list
		const arma::uword index = harmonics_.size()+1;
		harmonics_.insert({index,harm}); return index;
	}

	// clear harmonics
	void PathCCTCustom::clear_harmonics(){
		harmonics_.clear();
	}

	// delete harmonic
	bool PathCCTCustom::delete_harmonic(const arma::uword index){
		auto it = harmonics_.find(index);
		if(it==harmonics_.end())return false;
		(*it).second = NULL; return true;
	}

	arma::uword PathCCTCustom::num_harmonics()const{
		return harmonics_.size();
	}

	// get harmonic
	const ShCCTHarmonicPr& PathCCTCustom::get_harmonic(const arma::uword index) const{
		auto it = harmonics_.find(index);
		if(it==harmonics_.end())rat_throw_line("index does not exist");
		return (*it).second;
	}

	// set omega drive
	void PathCCTCustom::set_omega(const ShDrivePr &omega){
		omega_ = omega;
	}

	void PathCCTCustom::set_use_layer_jumps(const bool use_layer_jumps){
		use_layer_jumps_ = use_layer_jumps;
	}

	void PathCCTCustom::set_enable_lead1(const bool enable_lead1){
		enable_lead1_ = enable_lead1;
	}

	void PathCCTCustom::set_enable_lead2(const bool enable_lead2){
		enable_lead2_ = enable_lead2;
	}

	void PathCCTCustom::set_enable_lead3(const bool enable_lead3){
		enable_lead3_ = enable_lead3;
	}

	void PathCCTCustom::set_enable_lead4(const bool enable_lead4){
		enable_lead4_ = enable_lead4;
	}

	void PathCCTCustom::set_leadness1(const fltp leadness1){
		leadness1_ = leadness1;
	}

	void PathCCTCustom::set_leadness2(const fltp leadness2){
		leadness2_ = leadness2;
	}

	void PathCCTCustom::set_leadness3(const fltp leadness3){
		leadness3_ = leadness3;
	}

	void PathCCTCustom::set_leadness4(const fltp leadness4){
		leadness4_ = leadness4;
	}

	void PathCCTCustom::set_zlead1(const fltp zlead1){
		zlead1_ = zlead1;
	}

	void PathCCTCustom::set_zlead2(const fltp zlead2){
		zlead2_ = zlead2;
	}

	void PathCCTCustom::set_zlead3(const fltp zlead3){
		zlead3_ = zlead3;
	}

	void PathCCTCustom::set_zlead4(const fltp zlead4){
		zlead4_ = zlead4;
	}

	void PathCCTCustom::set_theta_lead1(const fltp theta_lead1){
		theta_lead1_ = theta_lead1;
	}

	void PathCCTCustom::set_theta_lead2(const fltp theta_lead2){
		theta_lead2_ = theta_lead2;
	}

	void PathCCTCustom::set_theta_lead3(const fltp theta_lead3){
		theta_lead3_ = theta_lead3;
	}

	void PathCCTCustom::set_theta_lead4(const fltp theta_lead4){
		theta_lead4_ = theta_lead4;
	}

	// set constant pitch
	void PathCCTCustom::set_omega(const fltp omega){
		omega_ = DriveDC::create(omega);
	}

	// set radius drive
	void PathCCTCustom::set_rho(const ShDrivePr &rho){
		rho_ = rho;
	}

	// set radius drive
	void PathCCTCustom::set_normal_scaling(const ShDrivePr &normal_scaling){
		normal_scaling_ = normal_scaling;
	}

	// set radius drive
	void PathCCTCustom::set_twist(const ShDrivePr &twist){
		twist_ = twist;
	}

	// void PathCCTCustom::set_new_orientation(const bool new_orientation){
	// 	new_orientation_ = new_orientation;
	// }

	// set constant radius
	void PathCCTCustom::set_rho(const fltp rho){
		rho_ = DriveDC::create(rho);
	}

	void PathCCTCustom::set_bending_origin(const bool bending_origin){
		bending_origin_ = bending_origin;
	}

	void PathCCTCustom::set_use_radius(const bool use_radius){
		use_radius_ = use_radius;
	}

	void PathCCTCustom::set_same_alpha(const bool same_alpha){
		same_alpha_ = same_alpha;
	}

	void PathCCTCustom::set_use_frenet_serret(const bool use_frenet_serret){
		use_frenet_serret_ = use_frenet_serret;
	}

	void PathCCTCustom::set_use_binormal(const bool use_binormal){
		use_binormal_ = use_binormal;
	}

	void PathCCTCustom::set_use_field_style_omega(const bool use_field_style_omega){
		use_field_style_omega_ = use_field_style_omega;
	}

	// get omega drive
	const ShDrivePr& PathCCTCustom::get_omega()const{
		return omega_;
	}

	// get radius drive
	const ShDrivePr& PathCCTCustom::get_rho()const{
		return rho_;
	}

	// get twist drive
	const ShDrivePr& PathCCTCustom::get_twist()const{
		return twist_;
	}

	// get radius drive
	const ShDrivePr& PathCCTCustom::get_normal_scaling()const{
		return normal_scaling_;
	}

	// set number of nodes for each turn
	void PathCCTCustom::set_num_nodes_per_turn(const arma::uword num_nodes_per_turn){
		assert(num_nodes_per_turn>0);
		num_nodes_per_turn_ = num_nodes_per_turn;
	}

	// set number of nodes for each turn
	arma::uword PathCCTCustom::get_num_nodes_per_turn()const{
		return num_nodes_per_turn_;
	}

	// set number of layers
	void PathCCTCustom::set_num_layers(const arma::uword num_layers){
		num_layers_ = num_layers;
	}

	// get number of layers
	arma::uword PathCCTCustom::get_num_layers() const{
		return num_layers_;
	}

	// set radius increment between layers
	void PathCCTCustom::set_rho_increment(const fltp rho_increment){
		rho_increment_ = rho_increment;
	}

	// get radius increment between layers
	fltp PathCCTCustom::get_rho_increment()const{
		return rho_increment_;
	}

	void PathCCTCustom::set_use_omega_normal(const bool use_omega_normal){
		use_omega_normal_ = use_omega_normal;
	}

	void PathCCTCustom::set_use_local_radius(const bool use_local_radius){
		use_local_radius_ = use_local_radius;
	}

	bool PathCCTCustom::get_use_omega_normal() const{
		return use_omega_normal_;
	}

	bool PathCCTCustom::get_use_layer_jumps()const{
		return use_layer_jumps_;
	}

	bool PathCCTCustom::get_enable_lead1()const{
		return enable_lead1_;
	}
	
	bool PathCCTCustom::get_enable_lead2()const{
		return enable_lead2_;
	}

	bool PathCCTCustom::get_enable_lead3()const{
		return enable_lead3_;
	}
	
	bool PathCCTCustom::get_enable_lead4()const{
		return enable_lead4_;
	}

	fltp PathCCTCustom::get_leadness1()const{
		return leadness1_;
	}

	fltp PathCCTCustom::get_leadness2()const{
		return leadness2_;
	}

	fltp PathCCTCustom::get_leadness3()const{
		return leadness1_;
	}

	fltp PathCCTCustom::get_leadness4()const{
		return leadness2_;
	}

	fltp PathCCTCustom::get_zlead1()const{
		return zlead1_;
	}

	fltp PathCCTCustom::get_zlead2()const{
		return zlead2_;
	}

	fltp PathCCTCustom::get_zlead3()const{
		return zlead1_;
	}

	fltp PathCCTCustom::get_zlead4()const{
		return zlead2_;
	}

	fltp PathCCTCustom::get_theta_lead1()const{
		return theta_lead1_;
	}

	fltp PathCCTCustom::get_theta_lead2()const{
		return theta_lead2_;
	}

	fltp PathCCTCustom::get_theta_lead3()const{
		return theta_lead1_;
	}

	fltp PathCCTCustom::get_theta_lead4()const{
		return theta_lead2_;
	}

	bool PathCCTCustom::get_bending_origin()const{
		return bending_origin_;
	}

	bool PathCCTCustom::get_use_radius()const{
		return use_radius_;
	}

	bool PathCCTCustom::get_use_local_radius()const{
		return use_local_radius_;
	}

	bool PathCCTCustom::get_same_alpha()const{
		return same_alpha_;
	}

	bool PathCCTCustom::get_use_frenet_serret()const{
		return use_frenet_serret_;
	}

	bool PathCCTCustom::get_use_binormal()const{
		return use_binormal_;
	}

	arma::uword PathCCTCustom::get_num_sect_per_turn()const{
		return num_sect_per_turn_;
	}

	// bool PathCCTCustom::get_new_orientation()const{
	// 	return new_orientation_;
	// }

	// // theta fun
	// arma::Row<fltp> PathCCTCustom::calc_position(
	// 	const arma::Row<fltp> &theta, 
	// 	const fltp thetamin, 
	// 	const fltp thetamax)const{
	// 	if(normalize_length_){
	// 		return (theta-thetamin)/(thetamax-thetamin) - RAT_CONST(0.5);
	// 	}else{
	// 		return theta/arma::Datum<fltp>::tau;
	// 	}
	// }

	// // calculate dposition dtheta
	// fltp PathCCTCustom::calc_dposition(
	// 	const fltp thetamin, const fltp thetamax)const{
	// 	if(normalize_length_){
	// 		return RAT_CONST(1.0)/(thetamax-thetamin);
	// 	}else{
	// 		return RAT_CONST(1.0)/arma::Datum<fltp>::tau;
	// 	}
	// }



	// function for interpolating matrix
	arma::Mat<rat::fltp> PathCCTCustom::interp_matrix(
		const arma::Col<fltp> &X, 
		const arma::Mat<fltp> &M, 
		const arma::Col<fltp> &Xi){

		// allocate output
		arma::Mat<rat::fltp> Mi(Xi.n_elem, M.n_cols);
		
		// walk over dimensions
		for(arma::uword i=0;i<M.n_cols;i++){
			arma::Col<rat::fltp> Mic;
			arma::interp1(X, M.col(i), Xi, Mic);
			Mi.col(i) = Mic;
		}

		// output matrix
		return Mi;
	}

	// setup coordinates and orientation vectors
	ShFramePr PathCCTCustom::create_frame(const MeshSettings &stngs) const{
		// check validity
		is_valid(true);
		is_valid_drives(true);

		// number of poles
		// arma::uword num_poles_max = 1;
		// for(auto it=harmonics_.begin();it!=harmonics_.end();it++)
		// 	num_poles_max = std::max(num_poles_max,(*it).second->get_num_poles());
		// find least common multiple
		// {
		// 	arma::Row<arma::uword> np(harmonics_.size()); arma::uword idx = 0;
		// 	for(auto it=harmonics_.begin();it!=harmonics_.end();it++)np(idx++) = (*it).second->get_num_poles();
		// 	num_poles_max = cmn::Extra::lcm(np);
		// }


		// calculate number of sections per half
		// const arma::uword num_sect_per_pole = 4;
		// const arma::uword num_sect_per_turn_min = 8; // Avoid FreeCAD 90 deg bending
		// const arma::uword num_sect_per_turn = std::max(num_sect_per_turn_min, num_sect_per_pole*num_poles_max);
		const fltp sectsize = arma::Datum<fltp>::tau/num_sect_per_turn_;

		// get nt3 and nt4
		bool use_inner = nt3_==RAT_CONST(0.0) && nt4_==RAT_CONST(0.0);
		const rat::fltp nt3 = use_inner ? nt1_ : nt3_;
		const rat::fltp nt4 = use_inner ? nt2_ : nt4_;

		// theta at which the leads are clipped
		const fltp thetamin_clip1 = nt1_*arma::Datum<fltp>::tau + (enable_lead1_ ? theta_lead1_ : 0.0);
		const fltp thetamax_clip1 = nt2_*arma::Datum<fltp>::tau - (enable_lead2_ ? theta_lead2_ : 0.0);
		const fltp thetamin_clip2 = nt3*arma::Datum<fltp>::tau + (enable_lead1_ ? theta_lead1_ : 0.0);
		const fltp thetamax_clip2 = nt4*arma::Datum<fltp>::tau - (enable_lead2_ ? theta_lead2_ : 0.0);

		// calculate maximum theta
		// we need to generate the coil geometry for both 
		// the interval nt1 to nt2 and nt1nrm to nt2nrm
		// otherwise we can not integrate the pitch to
		// get the required axial coordinates
		const fltp thetamin1 = std::floor(std::min(nt1_,nt1nrm_))*arma::Datum<fltp>::tau;
		const fltp thetamax1 = std::ceil(std::max(nt2_,nt2nrm_))*arma::Datum<fltp>::tau;
		const fltp thetamin2 = std::floor(std::min(nt3,nt1nrm_))*arma::Datum<fltp>::tau;
		const fltp thetamax2 = std::ceil(std::max(nt4,nt2nrm_))*arma::Datum<fltp>::tau;
		const fltp thetamin = std::min(thetamin1,thetamin2);
		const fltp thetamax = std::max(thetamax1,thetamax2);

		// normalized theta min and theta max
		// the drives will be running from -0.5 to 0.5 on this interval
		const fltp thetaminnrm = nt1nrm_*arma::Datum<fltp>::tau;
		const fltp thetamaxnrm = nt2nrm_*arma::Datum<fltp>::tau;

		// get number of sections
		const arma::sword num_int1 = arma::sword(std::floor(thetamin/sectsize));
		const arma::sword num_int2 = arma::sword(std::ceil(thetamax/sectsize));

		// calculate number of sections
		const arma::uword num_sections_overall = num_int2 - num_int1;

		// allocate theta
		// arma::Row<fltp> theta_start(num_sections);
		// arma::Row<fltp> theta_end(num_sections);
		arma::Row<fltp> theta = arma::linspace<arma::Row<fltp> >(num_int1*sectsize,num_int2*sectsize,num_sections_overall + 1);

		// calculate number of nodes
		// for this section
		arma::uword num_nodes_section = std::max(2llu,num_nodes_per_turn_/num_sect_per_turn_+1);
		while((num_nodes_section-1)%stngs.element_divisor!=0)num_nodes_section++;

		// allocate list of frames
		std::list<ShFramePr> frame_list;

		// store magnet length and radius
		arma::Row<fltp> magnet_length(num_layers_,arma::fill::zeros);
		arma::Row<fltp> magnet_radius(num_layers_,arma::fill::zeros);

		// lead positions
		fltp zlead1e = RAT_CONST(0.0), zlead2e = RAT_CONST(0.0);

		// walk over layers
		for(arma::uword k=0;k<num_layers_;k++){
			// const 
			arma::uword num_sections = num_sections_overall;

			// reverse
			const bool rev = (int(is_reverse_) + k)%2==1;
			const fltp srev = rev ? -RAT_CONST(1.0) : RAT_CONST(1.0);

			// calculate start position
			// const arma::uword nn= arma::uword(std::abs(nt1_))*num_nodes_per_turn_;
			// const arma::Row<fltp> turns = arma::linspace<arma::Row<fltp> >(0,nt1_,nn);
			// const arma::Row<fltp> omegas = omega_->get_scaling_vec(normalize_length_ ? turns/(nt2_ - nt1_) : turns);
			//arma::accu(arma::diff(turns,1,1)%(omegas.head_cols(nn-1) + omegas.tail_cols(nn-1))/2);

			// integrated offset
			fltp zoffset = 0;

			// allocate
			arma::field<arma::Mat<fltp> > R(1,num_sections);
			arma::field<arma::Mat<fltp> > V(1,num_sections);
			arma::field<arma::Mat<fltp> > A(1,num_sections); 
			arma::field<arma::Mat<fltp> > J(1,num_sections);
			arma::field<arma::Row<fltp> > angle(1,num_sections);
			arma::field<arma::Row<fltp> > omega(1,num_sections);
			arma::field<arma::field<arma::Row<fltp> > > rho(1,num_sections); // radius and its derivatives
			arma::field<arma::Row<fltp> > nscale(1,num_sections);
			arma::field<arma::Row<fltp> > twisting(1,num_sections);
			arma::field<arma::Row<fltp> > dtwisting(1,num_sections);
			arma::field<arma::Row<fltp> > theta_section(1,num_sections);
			arma::field<arma::Row<fltp> > turn(1,num_sections);

			// allocate section and turn indexes
			arma::Row<arma::uword> section_idx(num_sections);
			arma::Row<arma::uword> turn_idx(num_sections); 
			arma::Row<arma::uword> num_nodes(num_sections); 

			// magnet radius per section
			arma::Row<fltp> magnet_radius_sect(num_sections);


			// z and its derivatives
			arma::field<arma::Row<fltp> > z(1,num_sections);
			arma::field<arma::Row<fltp> > zp(1,num_sections); // pitch only
			arma::field<arma::Row<fltp> > vz(1,num_sections);
			arma::field<arma::Row<fltp> > az(1,num_sections);
			arma::field<arma::Row<fltp> > jz(1,num_sections);

			// walk over sections
			cmn::parfor(0,num_sections,true,[&](arma::uword i, int /*cpu*/) {
			// for(arma::uword i=0;i<num_sections;i++){
				// calculate section
				const fltp theta1 = theta(i);
				const fltp theta2 = theta(i+1);
				assert(theta2>theta1);

				// create theta (taking into account last section
				theta_section(i) = arma::linspace<arma::Row<fltp> >(theta1, theta2, num_nodes_section);

				// calculate position
				const arma::Row<fltp> position = CCTHarmonic::calc_position(theta_section(i), thetaminnrm, thetamaxnrm, normalize_length_);
				const fltp dposition_dtheta = CCTHarmonic::calc_dposition(thetaminnrm, thetamaxnrm, normalize_length_);

				// position
				// position scales with theta/dposition_dtheta;

				// get radius and its derivatives with respect to theta
				rho(i).set_size(4,1);

				// the local radius is defined as function of Z, but we do not know Z here
				// so for calculating the angle-amplitude relation we have no choice 
				// but to use the radius at the center of the coil
				if(use_local_radius_){
					const fltp radius_at_center = k*rho_increment_ + rho_->get_scaling(0.0, stngs.time);
					rho(i)(0) = arma::Row<fltp>(position.n_elem,arma::fill::value(radius_at_center));
					rho(i)(1) = arma::Row<fltp>(position.n_elem,arma::fill::zeros);
					rho(i)(2) = arma::Row<fltp>(position.n_elem,arma::fill::zeros);
					rho(i)(3) = arma::Row<fltp>(position.n_elem,arma::fill::zeros);
				}else{
					rho(i)(0) = k*rho_increment_ + rho_->get_scaling_vec(position, stngs.time);
					rho(i)(1) = dposition_dtheta*rho_->get_scaling_vec(position, stngs.time, 1);
					rho(i)(2) = dposition_dtheta*dposition_dtheta*rho_->get_scaling_vec(position, stngs.time,2);
					rho(i)(3) = dposition_dtheta*dposition_dtheta*dposition_dtheta*rho_->get_scaling_vec(position, stngs.time, 3);
				}

				// reference radius
				arma::field<arma::Row<fltp> > rho_ref = rho(i);
				rho_ref(0) = rho(i)(0) - (same_alpha_ ? 0.0 : k*rho_increment_);

				// check radius
				if(arma::any(rho(i)(0)<=0))rat_throw_line("radius must be positive");

				// get pitch
				omega(i) = omega_->get_scaling_vec(position, stngs.time);

				// get twist
				twisting(i) = srev*twist_->get_scaling_vec(position, stngs.time);
				dtwisting(i) = srev*twist_->get_scaling_vec(position, stngs.time, 1);

				// check radius
				// if(arma::any(omega(i)<0))rat_throw_line("pitch can not be negative");

				// check radius
				magnet_radius_sect(i) = arma::max(rho(i)(0));

				// create functionf for calculating z,vz,az,jz as function of theta
				const ZFunType zfun = [this,&rho_ref,&stngs,rev,thetaminnrm,thetamaxnrm](const arma::Row<fltp>&theta){
					// allocate output
					arma::field<arma::Row<fltp> > C(4);
					for(arma::uword i=0;i<4;i++)C(i).zeros(theta.n_elem);

					// walk over harmonics
					for(auto it=harmonics_.begin();it!=harmonics_.end();it++){
						const arma::field<arma::Row<fltp> > zfld = (*it).second->calc_z(
							theta, rho_ref, thetaminnrm, thetamaxnrm, stngs.time);
						for(arma::uword i=0;i<4;i++)C(i)+=zfld(i);
					}

					// reverse axial direction if needed
					if(rev)for(arma::uword i=0;i<4;i++)C(i)*=-1;

					// return output
					return C;
				};

				// create array
				const arma::field<arma::Row<fltp> > C = zfun(theta_section(i));
				z(i) = std::move(C(0)); vz(i) = std::move(C(1)); 
				az(i) = std::move(C(2)); jz(i) = std::move(C(3));
				zp(i).zeros(num_nodes_section);

				// // adjust radius for RCCT
				// const bool enable_rcct_ = true;
				// if(enable_rcct_){
				// 	// number of poles
				// 	arma::uword np = (*harmonics_.begin()).second->get_num_poles(); // the first harmonic determines which is main!
				// 	const fltp radius = 0.03;

				// 	// only take theta into account for half a pole
				// 	const fltp theta_max = arma::Datum<fltp>::pi/np;
				// 	const arma::Row<fltp> theta_mod = theta_section(i) - arma::floor(theta_section(i)/theta_max)*theta_max;

				// 	// calc radius
				// 	rho(i) = DriveRCCT::calc_radius_cone(zfun, theta_mod, radius, np);
				// }

				// calculate angle
				angle(i) = arma::atan2(vz(i),rho_ref(0));

				// normal scaling
				nscale(i) = normal_scaling_->get_scaling_vec(position, stngs.time);

				// check
				assert(nscale(i).n_elem==position.n_elem);
			});

			// get maximum radius
			magnet_radius(k) = arma::max(magnet_radius_sect);

			// allocate
			arma::field<arma::Row<fltp> > pitch_req(1,num_sections+num_sect_per_turn_);

			// field style
			if(use_field_style_omega_){
				for(arma::uword i=0;i<num_sections;i++)
					pitch_req(i) = omega(i);
				for(arma::uword i=0;i<num_sect_per_turn_;i++)
					pitch_req(num_sections+num_sect_per_turn_-i-1) = pitch_req(num_sections-i-1);
			}

			// calculate pitch between turns
			// with automatic shifting
			else if(num_sections>num_sect_per_turn_){
				// use pitch in the normal direction
				if(use_omega_normal_){
					// walk over sections
					for(arma::uword i=0;i<num_sections-num_sect_per_turn_;i++){
						const arma::Row<fltp> pitch1 = omega(i)/arma::cos(angle(i));
						const arma::Row<fltp> pitch2 = omega(i+num_sect_per_turn_)/arma::cos(angle(i+num_sect_per_turn_));
						const arma::Row<fltp> delta = z(i+num_sect_per_turn_) - z(i);
						pitch_req(i+num_sect_per_turn_) = arma::abs((pitch1+pitch2)/2 - delta);
					}
				}

				// otherwise use omega averaged between the two turns
				else{
					if(num_sections>num_sect_per_turn_){
						for(arma::uword i=0;i<num_sections-num_sect_per_turn_;i++)
							pitch_req(i+num_sect_per_turn_) = (omega(i) + omega(i+num_sect_per_turn_))/2;
					}
				}

				// repeat first and last turn
				for(arma::uword i=0;i<num_sect_per_turn_;i++)
					pitch_req(i) = pitch_req(i+num_sect_per_turn_);
				for(arma::uword i=0;i<num_sect_per_turn_;i++)
					pitch_req(num_sections+num_sect_per_turn_-i-1) = pitch_req(num_sections-i-1);
			}

			// no averaging between consecutive turns
			else{
				if(use_omega_normal_){
					for(arma::uword i=0;i<num_sections;i++){
						pitch_req(i+num_sect_per_turn_) = omega(i)/arma::cos(angle(i));
					}
				}else{
					for(arma::uword i=0;i<num_sections;i++){
						pitch_req(i+num_sect_per_turn_) = omega(i);
					}
				}

				// repeat first and last turn
				for(arma::uword i=0;i<num_sect_per_turn_;i++)
					pitch_req(i) = pitch_req(i+num_sect_per_turn_);
				for(arma::uword i=0;i<num_sect_per_turn_;i++)
					pitch_req(num_sections+num_sect_per_turn_-i-1) = pitch_req(num_sections-i-1);
			}

			// walk over sections and integrate pitch
			// to shift each turn
			for(arma::uword i=0;i<num_sections;i++){
				// calculate turn number
				turn(i) = theta_section(i)/arma::Datum<fltp>::tau;

				// get indices
				const arma::uword idx1 = i;
				const arma::uword idx2 = i+num_sect_per_turn_;

				// calculate mid sections
				fltp max_pitch_mid = RAT_CONST(0.0);

				// walk over turn and find where the pitch is maximum
				for(arma::uword j=idx1+1;j<=idx2-1;j++){
					if(pitch_req(j).empty())rat_throw_line("pitch is empty.");
					max_pitch_mid = std::max(max_pitch_mid,arma::max(pitch_req(j)));
				}

				// check for single turn
				arma::Row<fltp> pitch(num_nodes_section,arma::fill::zeros);

				// calculate locally maximum
				for(arma::uword j=0;j<num_nodes_section;j++){
					if(pitch_req(idx2).empty())pitch(j) = max_pitch_mid;
					else pitch(j) = std::max(max_pitch_mid, 
						arma::as_scalar(arma::max(arma::join_horiz(
						pitch_req(idx1).tail_cols(num_nodes_section-j),
						pitch_req(idx2).head_cols(j)))));
				}

				// calculate local offset
				const arma::Row<fltp> dz = 
					arma::join_horiz(arma::Row<fltp>{0},
					arma::cumsum(arma::diff(turn(i),1,1)%(
					pitch.head_cols(num_nodes_section-1) + 
					pitch.tail_cols(num_nodes_section-1))/2));

				// offset z coordinates with the pitch
				zp(i) += dz + zoffset;
				z(i) += dz + zoffset; vz(i) += pitch/arma::Datum<fltp>::tau; 

				// increment offset
				zoffset += dz.back();
			}

			// get offset
			fltp zmid = 0;

			// find section containing theta mid
			for(arma::uword i=0;i<num_sections;i++){
				if(theta_section(i).front()<=0 && theta_section(i).back()>=0){
					zmid = cmn::Extra::interp1(theta_section(i), zp(i), RAT_CONST(0.0)); break;
				}
			}

			// find z-position of nt1nrm and nt2nrm
			// this should be done better!
			fltp zminnrm = zmid-RAT_CONST(0.5), zmaxnrm = zmid+RAT_CONST(0.5);

			// // find section for which nt1nrm/nt2nrm is within the turn interval
			// for(arma::uword i=0;i<num_sections;i++){
			// 	if(nt1nrm_>turn(i).front() && nt1nrm_<=turn(i).back())
			// 		zminnrm = cmn::Extra::interp1(turn(i), zp(i), nt1nrm_);
			// 	if(nt2nrm_>turn(i).front() && nt2nrm_<=turn(i).back())
			// 		zmaxnrm = cmn::Extra::interp1(turn(i), zp(i), nt2nrm_);
			// }

			// fix radius after calculating the z-coordinate
			// this prevents intersection of the turns without
			// introducing higher order multipole components
			if(use_local_radius_){
				// recalculate radius
				for(arma::uword i=0;i<num_sections;i++){
					// check
					if(zmaxnrm==zminnrm)rat_throw_line("zmax can not be equal to zmin");

					// position normalized length on interval zmin to zmax
					arma::Row<fltp> position = (z(i)-zminnrm)/(zmaxnrm-zminnrm) - RAT_CONST(0.5);
					assert(position.is_finite());

					// calculate shift in position
					const fltp dposition_dz = RAT_CONST(1.0)/(zmaxnrm - zminnrm);
					const arma::Row<fltp>& dz_dtheta = vz(i);

					// multiply/shift with number of turns
					if(!normalize_length_)position = nt1nrm_ + (position+RAT_CONST(0.5))*(nt2nrm_ - nt1nrm_);
					assert(position.is_finite());

					// create time array with same length as position
					const arma::Row<fltp> time = arma::Row<fltp>(position.n_elem, arma::fill::value(stngs.time));
					assert(time.is_finite());

					// run through scaling
					for(arma::uword j=0;j<4;j++){
						rho(i)(j) = (j==0 ? k*rho_increment_ : RAT_CONST(0.0)) + 
							arma::pow(dposition_dz*dz_dtheta,rat::fltp(j))%
							rho_->get_scaling_vec(position,stngs.time,j);
						assert(rho(i)(j).is_finite());
					}
				}
			}

			// reverse winding
			const fltp dir = 
				(is_reverse_solenoid_ ? -RAT_CONST(1.0) : RAT_CONST(1.0))* // reverse the solenoidal winding direction
				(is_solenoid_ && k%2==1 ? -RAT_CONST(1.0) : RAT_CONST(1.0)); // reverse the winding direction for even layers when it is a solenoid

			// walk over sections to calculate coordinates
			cmn::parfor(0,num_sections,true,[&](arma::uword i, int /*cpu*/) {
				// pre-calculate trigonometric functions
				const arma::Row<fltp> cos_theta = arma::cos(theta_section(i));
				const arma::Row<fltp> sin_theta = arma::sin(theta_section(i));

				// create coordinates for circle
				const arma::Row<fltp> x = rho(i)(0)%cos_theta;
				const arma::Row<fltp> y = dir*rho(i)(0)%sin_theta;
				const arma::Row<fltp> vx = -rho(i)(0)%sin_theta + rho(i)(1)%cos_theta;
				const arma::Row<fltp> vy = dir*(rho(i)(0)%cos_theta + rho(i)(1)%sin_theta);
				const arma::Row<fltp> ax = -rho(i)(0)%cos_theta - 2*rho(i)(1)%sin_theta + rho(i)(2)%cos_theta;
				const arma::Row<fltp> ay = dir*(-rho(i)(0)%sin_theta + 2*rho(i)(1)%cos_theta + rho(i)(2)%sin_theta);
				const arma::Row<fltp> jx = rho(i)(0)%sin_theta - 3*rho(i)(1)%cos_theta - 3*rho(i)(2)%sin_theta + rho(i)(3)%cos_theta;
				const arma::Row<fltp> jy = dir*(-rho(i)(0)%cos_theta - 3*rho(i)(1)%sin_theta + 3*rho(i)(2)%cos_theta + rho(i)(3)%sin_theta);

				// store position vectors
				R(i) = arma::join_vert(x,y,z(i));

				// convert to 3XN matrices
				V(i) = arma::join_vert(vx,vy,vz(i));
				A(i) = arma::join_vert(ax,ay,az(i));
				J(i) = arma::join_vert(jx,jy,jz(i));

				// check size
				assert(V(i).n_rows==3);	assert(A(i).n_rows==3);
				assert(R(i).n_rows==3);	assert(J(i).n_rows==3);

				// check
				assert(V(i).is_finite()); assert(A(i).is_finite()); assert(J(i).is_finite());

				// set section and turn
				section_idx(i) = i%num_sect_per_turn_;
				turn_idx(i) = i/num_sect_per_turn_;
			});

			// apply offset
			for(arma::uword i=0;i<num_sections;i++){
				R(i).row(2) -= zmid; zp(i) -= zmid; z(i) -= zmid;
			}
			zminnrm -= zmid; zmaxnrm -= zmid;

			// perform clipping
			const rat::fltp thetamin_clip = k%2==0 ? thetamin_clip1 : thetamin_clip2;
			const rat::fltp thetamax_clip = k%2==0 ? thetamax_clip1 : thetamax_clip2;

			// limit the arrays to the sections that should still be included within the clip
			{
				// find first and last indices
				arma::uword idx_first = 0, idx_last = 0;
				for(arma::uword i=0;i<num_sections;i++){
					if(theta_section(i).back()>thetamin_clip){idx_first = i; break;}
				}
				for(arma::uword i=num_sections;i>0;i--){
					if(theta_section(i-1).front()<thetamax_clip){idx_last = i-1; break;}
				}
				if(idx_first>idx_last)
					rat_throw_line("no sections available to clip");

				// perform clip
				z = z.cols(idx_first, idx_last);
				zp = zp.cols(idx_first, idx_last);
				R = R.cols(idx_first, idx_last);
				V = V.cols(idx_first, idx_last);
				A = A.cols(idx_first, idx_last);
				J = J.cols(idx_first, idx_last);
				section_idx.cols(idx_first, idx_last);
				turn_idx.cols(idx_first, idx_last);
				theta_section = theta_section.cols(idx_first, idx_last);
				rho = rho.cols(idx_first, idx_last);
				nscale = nscale.cols(idx_first, idx_last);
				twisting = twisting.cols(idx_first, idx_last);
				dtwisting = dtwisting.cols(idx_first,idx_last);
				num_sections = idx_last - idx_first + 1;
			}

			// find index for the clip within the first section
			const arma::Col<arma::uword> idx_first_vec = arma::find(theta_section(0)>thetamin_clip+1e-14,1,"first");
			
			// index lies within section
			if(!idx_first_vec.empty()){
				// settings
				const fltp theta_tolerance = arma::Datum<fltp>::tau/num_nodes_per_turn_/4; // sliver element tolerance

				// get first index that we want to include in the section
				const arma::uword idx_first = arma::as_scalar(idx_first_vec);

				// create interpolation array based on the theta 
				// position of the clip and the rest of the first section
				arma::Row<rat::fltp> theta_interp1 = arma::join_horiz(
					arma::Row<fltp>{thetamin_clip}, 
					theta_section(0).cols(idx_first, theta_section(0).n_elem-1));
				
				// check if the spacing between the proposed angles is above tolerance
				// this prevents making sliver elements
				for(arma::uword i=0;i<theta_interp1.n_elem-1;i++)
					if(std::abs(theta_interp1(i)-theta_interp1(i+1))<theta_tolerance)
						theta_interp1(i) = theta_interp1(i+1); // make non-unique

				// drop non-unique theta
				theta_interp1 = arma::unique(theta_interp1);

				// perform interpolation at new theta
				z(0) = interp_matrix(theta_section(0).t(), z(0).t(), theta_interp1.t()).t();
				zp(0) = interp_matrix(theta_section(0).t(), zp(0).t(), theta_interp1.t()).t();
				R(0) = interp_matrix(theta_section(0).t(), R(0).t(), theta_interp1.t()).t();
				V(0) = interp_matrix(theta_section(0).t(), V(0).t(), theta_interp1.t()).t();
				A(0) = interp_matrix(theta_section(0).t(), A(0).t(), theta_interp1.t()).t();
				J(0) = interp_matrix(theta_section(0).t(), J(0).t(), theta_interp1.t()).t();
				nscale(0) = interp_matrix(theta_section(0).t(), nscale(0).t(), theta_interp1.t()).t();
				twisting(0) = interp_matrix(theta_section(0).t(), twisting(0).t(), theta_interp1.t()).t();
				dtwisting(0) = interp_matrix(theta_section(0).t(), dtwisting(0).t(), theta_interp1.t()).t();
				theta_section(0) = theta_interp1;
			}

			// no index found
			else{
				// drop first section entirely
				R(0).clear(); V(0).clear(); A(0).clear(); J(0).clear(); 
				z(0).clear(); theta_section(0).clear();
			}

			// clip last section
			const arma::uword lsid = theta_section.n_elem-1;

			// find index at which the clip occurs within the last section
			const arma::Col<arma::uword> idx_last_vec = arma::find(theta_section(lsid)<thetamax_clip-1e-14,1,"last");

			// index lies within section
			if(!idx_last_vec.empty()){
				// settings
				const fltp theta_tolerance = arma::Datum<fltp>::tau/num_nodes_per_turn_/4; // sliver element tolerance

				// create interpolation array based on the theta 
				// position of the clip and the rest of the first section
				const arma::uword idx_last = arma::as_scalar(idx_last_vec);
				arma::Row<rat::fltp> theta_interp2 = arma::join_horiz(
					theta_section(lsid).cols(0, idx_last),
					arma::Row<fltp>{thetamax_clip});

				// check if the spacing between the proposed angles is above tolerance
				// this prevents making sliver elements
				for(arma::uword i=0;i<theta_interp2.n_elem-1;i++)
					if(std::abs(theta_interp2(i)-theta_interp2(i+1))<theta_tolerance)
						theta_interp2(i) = theta_interp2(i+1); // make non-unique

				// drop non-unique theta
				theta_interp2 = arma::unique(theta_interp2); 

				// perform interpolation at enw theat
				R(lsid) = interp_matrix(theta_section(lsid).t(), R(lsid).t(), theta_interp2.t()).t();
				z(lsid) = interp_matrix(theta_section(lsid).t(), z(lsid).t(), theta_interp2.t()).t();
				zp(lsid) = interp_matrix(theta_section(lsid).t(), zp(lsid).t(), theta_interp2.t()).t();
				V(lsid) = interp_matrix(theta_section(lsid).t(), V(lsid).t(), theta_interp2.t()).t();
				A(lsid) = interp_matrix(theta_section(lsid).t(), A(lsid).t(), theta_interp2.t()).t();
				J(lsid) = interp_matrix(theta_section(lsid).t(), J(lsid).t(), theta_interp2.t()).t();
				nscale(lsid) = interp_matrix(theta_section(lsid).t(), nscale(lsid).t(), theta_interp2.t()).t();
				twisting(lsid) = interp_matrix(theta_section(lsid).t(), twisting(lsid).t(), theta_interp2.t()).t();
				dtwisting(lsid) = interp_matrix(theta_section(lsid).t(), dtwisting(lsid).t(), theta_interp2.t()).t();
				theta_section(lsid) = theta_interp2;
			}

			// no index found
			else{
				// drop last section entirely
				R(lsid).clear(); V(lsid).clear(); A(lsid).clear(); J(lsid).clear(); 
				z(lsid).clear(); theta_section(lsid).clear();
			}

			// check if we completely removed section
			{
				const arma::uword idx_start = arma::uword(R(0).n_cols<=1);
				const arma::uword idx_end = R.n_cols-1-arma::uword(R(lsid).n_cols<=1);
				R = R.cols(idx_start, idx_end); 
				V = V.cols(idx_start, idx_end); A = A.cols(idx_start, idx_end); J = J.cols(idx_start, idx_end);
				z = z.cols(idx_start, idx_end); zp = zp.cols(idx_start, idx_end); 
				theta_section = theta_section.cols(idx_start, idx_end);
				rho = rho.cols(idx_start,idx_end);
				nscale = nscale.cols(idx_start,idx_end);
				section_idx = section_idx.cols(idx_start, idx_end);
				turn_idx = turn_idx.cols(idx_start, idx_end);
				num_sections = idx_end - idx_start + 1;
			}

			// magnet length from first and last z
			magnet_length(k) = zp(zp.n_elem-1).back() - zp(0).front();

			// check sections
			assert(num_sections==R.n_elem);

			// check
			assert(section_idx.n_elem==R.n_elem);
			assert(turn_idx.n_elem==R.n_elem);

			// create first current lead
			if(((k%2==0 && enable_lead1_) || (k%2==1 && enable_lead3_)) && (!use_layer_jumps_ || k==0)){
				// get settings
				const rat::fltp zlead1 = k%2==0 ? zlead1_ : zlead3_;
				const rat::fltp theta_lead1 = k%2==0 ? theta_lead1_ : theta_lead3_;
				const rat::fltp leadness1 = k%2==0 ? leadness1_ : leadness3_;

				// convert coordinate to cylindrical coordinates
				const fltp theta0 = theta_section(0)(0); 
				const fltp rho0 = rho(0)(0)(0);
				const fltp z0 = R(0)(2,0);

				// velocity in theta
				const arma::Col<fltp>::fixed<3> L0 = V(0).col(0).each_row()/cmn::Extra::vec_norm(V(0).col(0));
				// const fltp vx0 = V(0)(0,2), vy0 = V(0)(0,1), vz0 = V(0)(0,2);
				const fltp vtheta0 = -std::sin(theta0)*L0(0) + std::cos(theta0)*L0(1); const fltp vz0 = L0(2);

				// calculate element size in the current leads
				const fltp element_size = magnet_radius(k)*arma::Datum<fltp>::tau/num_nodes_per_turn_;

				// persistent lead-end position
				if(k==0)zlead1e = zp(0).front();

				// twisting
				const fltp twist0 = twisting(0)(0);

				// create points in theta-z plane
				const fltp theta1 = theta0 - theta_lead1; const fltp z1 = zlead1e - zlead1; 
				const fltp theta2 = theta0 - theta_lead1; const fltp z2 = zlead1e - RAT_CONST(0.6)*zlead1;
				const fltp theta3 = theta0 - leadness1*vtheta0/rho0; const fltp z3 = z0 - leadness1*vz0;

				// current leads for frenet serret frame
				// the lead will not be perfectly within the cylinder
				if(false && use_frenet_serret_){
					// coordinate of lead end
					const rat::fltp velocity = arma::as_scalar(cmn::Extra::vec_norm(V(0).col(0)));
					arma::Col<fltp>::fixed<3> R1{rho0*std::cos(theta1), rho0*std::sin(theta1), zlead1e - zlead1};
					arma::Col<fltp>::fixed<3> V1{0,0,zlead1*velocity/rho0};
					arma::Col<fltp>::fixed<3> A1{-rho0*std::sin(theta1), rho0*std::cos(theta1),0};
					arma::Col<fltp>::fixed<3> J1{-rho0*std::sin(theta1), rho0*std::cos(theta1),0};
					arma::Col<fltp>::fixed<3> Jp1{-rho0*std::sin(theta1), rho0*std::cos(theta1),0};
					arma::Col<fltp>::fixed<3> Jpp1{-rho0*std::sin(theta1), rho0*std::cos(theta1),0};
					arma::Col<fltp>::fixed<3> Jppp1{-rho0*std::sin(theta1), rho0*std::cos(theta1),0};

					arma::Col<fltp>::fixed<3> Jp2 = (J(0).col(1) - J(0).col(0))/(theta_section(0)(1) - theta_section(0)(0));
					arma::Col<fltp>::fixed<3> Jpp2 = (J(0).col(2) - 2*J(0).col(1) + J(0).col(0))/std::pow(theta_section(0)(1) - theta_section(0)(0),2);
					arma::Col<fltp>::fixed<3> Jppp2 = (J(0).col(3) - 3*J(0).col(2) + 3*J(0).col(1) - J(0).col(0))/std::pow(theta_section(0)(1) - theta_section(0)(0),3);

					// frenet serret case
					const rat::fltp f = leadness1/rho0;
					const arma::Mat<fltp>::fixed<3,6> P1 = PathBezier::create_control_points(
						arma::join_horiz(arma::join_horiz(R1,V1,A1,J1),Jp1,Jpp1,Jppp1));
					arma::Mat<fltp>::fixed<3,6> P2 = PathBezier::create_control_points(
						arma::join_horiz(arma::join_horiz(R(0).col(0), -f*V(0).col(0), f*A(0).col(0), -f*J(0).col(0)), f*Jp2, f*Jpp2, f*Jppp2));

					// joint points
					const arma::Mat<fltp>::fixed<3,12> P = arma::join_horiz(P1,arma::fliplr(P2));

					// get regular times in cylindrical coordinates
					const arma::Row<fltp> t = PathBezier::regular_times(P, element_size, 0.0, 500);

					// calculate bezier spline
					const arma::field<arma::Mat<fltp> > C = PathBezier::create_bezier_tn(t,P,4);

					// attach lead
					const arma::uword num_cols = R(0).n_cols-1;
					R(0) = arma::join_horiz(C(0), R(0).tail_cols(num_cols)); V(0) = arma::join_horiz(C(1), V(0).tail_cols(num_cols));
					A(0) = arma::join_horiz(C(2), A(0).tail_cols(num_cols)); J(0) = arma::join_horiz(C(3), J(0).tail_cols(num_cols));

					// attach theta and radius
					theta_section(0) = arma::join_horiz(arma::atan(C(0).row(1)/C(0).row(0)), theta_section(0).tail_cols(num_cols));
					nscale(0) = arma::Row<fltp>(theta_section(0).n_elem,arma::fill::ones);
					twisting(0) = arma::Row<fltp>(theta_section(0).n_elem,arma::fill::ones);
					dtwisting(0) = arma::Row<fltp>(theta_section(0).n_elem,arma::fill::ones);
					for(arma::uword i=0;i<4;i++)rho(0)(i) = arma::join_horiz(arma::hypot(C(i).row(0),C(i).row(1)), rho(0)(i).tail_cols(num_cols));
				}
				
				// current leads for normal frame
				// the lead is kept within the coil cylinder
				else{
					// create control points
					const arma::Mat<fltp>::fixed<3,4> P{rho0,theta1*rho0,z1, rho0,theta2*rho0,z2, rho0,theta3*rho0,z3, rho0,theta0*rho0,z0};
					const arma::Mat<fltp>::fixed<1,4> Pt{0.0,0.0,twist0,twist0};

					// get regular times in cylindrical coordinates
					const arma::Row<fltp> t = PathBezier::regular_times(P, element_size, 0.0, 500);

					// calculate bezier spline
					const arma::field<arma::Mat<fltp> > C = PathBezier::create_bezier_tn(t,arma::join_vert(P,Pt),4);

					// convert to single coordinates
					arma::Row<fltp> rhol = C(0).row(0); const arma::Row<fltp> thetal = C(0).row(1)/rho0, zl = C(0).row(2);
					arma::Row<fltp> drhol = C(1).row(0); const arma::Row<fltp> dthetal = C(1).row(1)/rho0, dzl = C(1).row(2);
					arma::Row<fltp> ddrhol = C(2).row(0); const arma::Row<fltp> ddthetal = C(2).row(1)/rho0, ddzl = C(2).row(2);
					arma::Row<fltp> dddrhol = C(3).row(0); const arma::Row<fltp> dddthetal = C(3).row(1)/rho0, dddzl = C(3).row(2);

					// fix radius
					if(use_local_radius_){
						// position normalized length on interval zmin to zmax
						arma::Row<fltp> position = (zl-zminnrm)/(zmaxnrm-zminnrm) - RAT_CONST(0.5);
						const fltp dposition_dz = RAT_CONST(1.0)/(zmaxnrm - zminnrm);	
						const arma::Row<fltp>& dz_dtheta = dzl;
						if(!normalize_length_)position = nt1nrm_ + (position+RAT_CONST(0.5))*(nt2nrm_ - nt1nrm_);

						// re-calculate radius based on z position
						rhol = rho_->get_scaling_vec(position,stngs.time) + k*rho_increment_;
						drhol = arma::pow(dposition_dz*dz_dtheta,1)%rho_->get_scaling_vec(position,stngs.time,1);
						ddrhol = arma::pow(dposition_dz*dz_dtheta,2)%rho_->get_scaling_vec(position,stngs.time, 2);
						dddrhol = arma::pow(dposition_dz*dz_dtheta,3)%rho_->get_scaling_vec(position,stngs.time, 3);
					}

					// build bezier spline in cartesian coordinates
					const arma::uword num_cols = R(0).n_cols-1;
					R(0) = arma::join_horiz(arma::join_vert(
						rhol%arma::cos(thetal), rhol%arma::sin(thetal), zl), R(0).tail_cols(num_cols));
					V(0) = arma::join_horiz(arma::join_vert(
						(-dthetal%arma::sin(thetal) + drhol%arma::cos(thetal))*rho0, 
						(dthetal%arma::cos(thetal) + drhol%arma::sin(thetal))*rho0, dzl), V(0).tail_cols(num_cols));
					A(0) = arma::join_horiz(arma::join_vert(
						(-ddthetal%arma::sin(thetal) + ddrhol%arma::cos(thetal))*rho0, 
						(ddthetal%arma::cos(thetal) + ddrhol%arma::sin(thetal))*rho0, ddzl), A(0).tail_cols(num_cols));
					J(0) = arma::join_horiz(arma::join_vert(
						(-dddthetal%arma::sin(thetal) + dddrhol%arma::cos(thetal))*rho0, 
						(dddthetal%arma::cos(thetal) + dddrhol%arma::sin(thetal))*rho0, dddzl), J(0).tail_cols(num_cols));

					// attach theta and radius
					theta_section(0) = arma::join_horiz(thetal, theta_section(0).tail_cols(num_cols));
					nscale(0) = arma::join_horiz(arma::Row<fltp>(thetal.n_elem,arma::fill::value(nscale(0).front())), nscale(0).tail_cols(num_cols));
					twisting(0) = arma::join_horiz(arma::Row<fltp>(thetal.n_elem,arma::fill::value(twisting(0).front())), twisting(0).tail_cols(num_cols));
					dtwisting(0) = arma::join_horiz(arma::Row<fltp>(thetal.n_elem,arma::fill::value(dtwisting(0).front())), dtwisting(0).tail_cols(num_cols));
					for(arma::uword i=0;i<4;i++)rho(0)(i) = arma::join_horiz(C(i).row(0), rho(0)(i).tail_cols(num_cols));
				}
			}

			// create second current lead
			if(((k%2==0 && enable_lead2_) || (k%2==1 && enable_lead4_)) && (!use_layer_jumps_ || k==num_layers_-1)){
				// get settings
				const rat::fltp zlead2 = k%2==0 ? zlead2_ : zlead4_;
				const rat::fltp theta_lead2 = k%2==0 ? theta_lead2_ : theta_lead4_;
				const rat::fltp leadness2 = k%2==0 ? leadness2_ : leadness4_;

				// get indices of last node of last section
				const arma::uword idx_sect_last = theta_section.n_elem-1;
				const arma::uword idx_last = theta_section(idx_sect_last).n_elem-1;

				// convert coordinate to cylindrical coordinates
				const fltp theta0 = theta_section(idx_sect_last)(idx_last); 
				const fltp rho0 = rho(idx_sect_last)(0)(idx_last);
				const fltp z0 = R(idx_sect_last)(2,idx_last);

				// velocity in theta
				const arma::Col<fltp>::fixed<3> L0 = V(idx_sect_last).col(idx_last).each_row()/cmn::Extra::vec_norm(V(idx_sect_last).col(idx_last));
				// const fltp vx0 = V(0)(0,2), vy0 = V(0)(0,1), vz0 = V(0)(0,2);
				const fltp vtheta0 = -std::sin(theta0)*L0(0) + std::cos(theta0)*L0(1); const fltp vz0 = L0(2);

				// twisting
				const fltp twist0 = twisting(idx_sect_last)(idx_last);

				// calculate element size in the current leads
				const fltp element_size = magnet_radius(k)*arma::Datum<fltp>::tau/num_nodes_per_turn_;

				// persistent lead-end position
				if(k==0)zlead2e = zp(idx_sect_last).back();

				// create points
				const fltp theta1 = theta0 + theta_lead2; const fltp z1 = zlead2e + zlead2; 
				const fltp theta2 = theta0 + theta_lead2; const fltp z2 = zlead2e + RAT_CONST(0.6)*zlead2;
				const fltp theta3 = theta0 + leadness2*vtheta0/rho0; const fltp z3 = z0 + leadness2_*vz0;

				// combine points for bezier
				arma::Mat<fltp>::fixed<3,4> P{
					rho0,theta1*rho0,z1, rho0,theta2*rho0,z2,
					rho0,theta3*rho0,z3, rho0,theta0*rho0,z0};
				arma::Mat<fltp>::fixed<1,4> Pt{0.0,0.0,twist0,twist0};

				// reverse points
				P = arma::fliplr(P); Pt = arma::fliplr(Pt);

				// get regular times in cylindrical coordinates
				const arma::Row<fltp> t = PathBezier::regular_times(P, element_size, 0.0, 500);

				// convert to single coordinates
				const arma::field<arma::Mat<fltp> > C = PathBezier::create_bezier_tn(t,arma::join_vert(P,Pt),4);

				// convert to single coordinates
				arma::Row<fltp> rhol = C(0).row(0); const arma::Row<fltp> thetal = C(0).row(1)/rho0, zl = C(0).row(2);
				arma::Row<fltp> drhol = C(1).row(0); const arma::Row<fltp> dthetal = C(1).row(1)/rho0, dzl = C(1).row(2);
				arma::Row<fltp> ddrhol = C(2).row(0); const arma::Row<fltp> ddthetal = C(2).row(1)/rho0, ddzl = C(2).row(2);
				arma::Row<fltp> dddrhol = C(3).row(0); const arma::Row<fltp> dddthetal = C(3).row(1)/rho0, dddzl = C(3).row(2);

				// fix radius
				if(use_local_radius_){
					// position normalized length on interval zmin to zmax
					arma::Row<fltp> position = (zl-zminnrm)/(zmaxnrm-zminnrm) - RAT_CONST(0.5);
					const fltp dposition_dz = RAT_CONST(1.0)/(zmaxnrm - zminnrm);	
					const arma::Row<fltp>& dz_dtheta = dzl;
					if(!normalize_length_)position = nt1nrm_ + (position+RAT_CONST(0.5))*(nt2nrm_ - nt1nrm_);

					// re-calculate radius based on z position
					rhol = rho_->get_scaling_vec(position,stngs.time) + k*rho_increment_;
					drhol = arma::pow(dposition_dz*dz_dtheta,1)%rho_->get_scaling_vec(position,stngs.time, 1);
					ddrhol = arma::pow(dposition_dz*dz_dtheta,2)%rho_->get_scaling_vec(position,stngs.time, 2);
					dddrhol = arma::pow(dposition_dz*dz_dtheta,3)%rho_->get_scaling_vec(position,stngs.time, 3);
				}

				// build bezier spline in cartesian coordinates
				const arma::uword num_cols = R(idx_sect_last).n_cols-1;
				R(idx_sect_last) = arma::join_horiz(R(idx_sect_last).head_cols(num_cols), arma::join_vert(
					rhol%arma::cos(thetal), rhol%arma::sin(thetal), zl));
				V(idx_sect_last) = arma::join_horiz(V(idx_sect_last).head_cols(num_cols), arma::join_vert(
					(-dthetal%arma::sin(thetal) + drhol%arma::cos(thetal))*rho0, 
					(dthetal%arma::cos(thetal) + drhol%arma::sin(thetal))*rho0, dzl));
				A(idx_sect_last) = arma::join_horiz(A(idx_sect_last).head_cols(num_cols), arma::join_vert(
					(-ddthetal%arma::sin(thetal) + ddrhol%arma::cos(thetal))*rho0, 
					(ddthetal%arma::cos(thetal) + ddrhol%arma::sin(thetal))*rho0, ddzl));
				J(idx_sect_last) = arma::join_horiz(J(idx_sect_last).head_cols(num_cols), arma::join_vert(
					(-dddthetal%arma::sin(thetal) + dddrhol%arma::cos(thetal))*rho0, 
					(dddthetal%arma::cos(thetal) + dddrhol%arma::sin(thetal))*rho0, dddzl));

				// attach theta and radius
				theta_section(idx_sect_last) = arma::join_horiz(theta_section(idx_sect_last).head_cols(num_cols), thetal);
				nscale(idx_sect_last) = arma::join_horiz(nscale(idx_sect_last).head_cols(num_cols), arma::Row<fltp>(thetal.n_elem,arma::fill::value(nscale(idx_sect_last).back())));
				twisting(idx_sect_last) = arma::join_horiz(twisting(idx_sect_last).head_cols(num_cols), arma::Row<fltp>(thetal.n_elem,arma::fill::value(twisting(idx_sect_last).back())));
				dtwisting(idx_sect_last) = arma::join_horiz(dtwisting(idx_sect_last).head_cols(num_cols), arma::Row<fltp>(thetal.n_elem,arma::fill::value(dtwisting(idx_sect_last).back())));
				for(arma::uword i=0;i<4;i++)rho(idx_sect_last)(i) = arma::join_horiz(rho(idx_sect_last)(i).head_cols(num_cols), C(i).row(0));
			}

			// create layer jumps
			if(use_layer_jumps_){

			}

			// get outer layer inner radius as a reference for the bending
			// fltp max_magnet_radius = 0;
			// for(arma::uword i=0;i<num_sections;i++){
			// 	max_magnet_radius = std::max(max_magnet_radius, arma::max(rho(i)(0)) + (num_layers_-k-1)*rho_increment_);
			// }

			// get largest radius for the magnet
			const fltp max_magnet_radius = magnet_radius(k) + (num_layers_ - k)*rho_increment_;

			// allocate bending parameters to be used by bending transformation
			rat::fltp bending_radius = 0.0, bending_offset = 0.0, bending_post = 0.0;

			// bending with radius
			if(use_radius_ && bending_radius_!=RAT_CONST(0.0)){
				const fltp sgn = cmn::Extra::sign(bending_radius_);
				bending_radius = bending_radius_-sgn*max_magnet_radius;
				bending_offset = -sgn*max_magnet_radius;
				if(!bending_origin_)bending_post = bending_radius_;				
			}

			// bending with arc-length
			else if(!use_radius_ && bending_arc_length_!=RAT_CONST(0.0)){
				// check if the magnet has an actual length
				const fltp max_magnet_length = magnet_length.max();
				if(max_magnet_length!=0){
					bending_radius = max_magnet_length/bending_arc_length_;
					const fltp sgn = cmn::Extra::sign(bending_radius);
					bending_offset = -sgn*max_magnet_radius;	
					if(!bending_origin_)bending_post = bending_radius+sgn*max_magnet_radius;
				}
			}

			// axial vector for later calculating the radial vector
			arma::field<arma::Mat<fltp> > Vax(1,R.n_elem);
			arma::field<arma::Mat<fltp> > Vrad(1,R.n_elem);

			// apply bending transformation
			if(bending_radius!=0.0){
				for(arma::uword i=0;i<num_sections;i++){
					assert(i<R.n_elem);

					const rat::fltp br1 = bending_radius;
					const rat::fltp br2 = br1*br1;
					const rat::fltp br3 = br2*br1;

					// convert to singles
					const arma::Row<fltp> x = R(i).row(0) - bending_offset, y = R(i).row(1), z = R(i).row(2);
					const arma::Row<fltp> vx = V(i).row(0), vy = V(i).row(1), vz = V(i).row(2);
					const arma::Row<fltp> ax = A(i).row(0), ay = A(i).row(1), az = A(i).row(2);
					const arma::Row<fltp> jx = J(i).row(0), jy = J(i).row(1), jz = J(i).row(2);

					// transform position and derivatives 
					// both x and z are functions of theta
					// this makes the derivatives longer and longer
					const arma::Row<fltp> xp = (br1 + x)%arma::cos(z/br1);
					const arma::Row<fltp> zp = (br1 + x)%arma::sin(z/br1);

					const arma::Row<fltp> vxp = vx%arma::cos(z/br1) - (br1 + x)%arma::sin(z/br1)%(vz/br1);
					const arma::Row<fltp> vzp = vx%arma::sin(z/br1) + (br1 + x)%arma::cos(z/br1)%(vz/br1);

					const arma::Row<fltp> axp = -(((br1*x+br2)%az + 2*br1*vx%vz)%arma::sin(z/br1) + ((x+br1)%arma::square(vz)-br2*ax)%arma::cos(z/br1))/br2;
					const arma::Row<fltp> azp = -(((x+br1)%arma::square(vz)-br2*ax)%arma::sin(z/br1) + ((-br1*x-br2)%az-2*br1*vx%vz)%arma::cos(z/br1))/br2;

					const arma::Row<fltp> jxp = -(((br2*x+br3)%jz+3*br2*vx%az+(-x-br1)%arma::pow(vz,3)+3*br2*ax%vz)%arma::sin(z/br1)+((3*br1*x+3*br2)%vz%az+3*br1*vx%arma::square(vz)-br3*jx)%arma::cos(z/br1))/br3;
					const arma::Row<fltp> jzp = -(((3*br1*x+3*br2)%vz%az+3*br1*vx%arma::square(vz)-br3*jx)%arma::sin(z/br1)+((-br2*x-br3)%jz-3*br2*vx%az+(x+br1)%arma::pow(vz,3)-3*br2*ax%vz)%arma::cos(z/br1))/br3;

					// store position vectors
					R(i) = arma::join_vert(xp - bending_post,y,zp);

					// set axial and radial vectors
					Vax(i) = arma::join_vert(
						-arma::sin(z/br1), 
						arma::Row<fltp>(z.n_elem,arma::fill::zeros), 
						arma::cos(z/br1));
					Vrad(i) = arma::join_vert(
						arma::cos(z/br1)%arma::cos(theta_section(i)), 
						dir*arma::sin(theta_section(i)), 
						arma::sin(z/br1)%arma::cos(theta_section(i)));

					// convert to 3XN matrices
					V(i) = arma::join_vert(vxp,vy,vzp);
					A(i) = arma::join_vert(axp,ay,azp);
					J(i) = arma::join_vert(jxp,jy,jzp);
				}
			}else{
				// set axial vector directly
				for(arma::uword i=0;i<R.n_elem;i++){
					Vax(i).zeros(R(i).n_rows, R(i).n_cols); Vax(i).row(2).fill(1.0);
					Vrad(i) = arma::join_vert(
						arma::cos(theta_section(i)), 
						dir*arma::sin(theta_section(i)), 
						arma::Row<fltp>(theta_section(i).n_elem,arma::fill::zeros));
				}
			}

			// allocate frame orientation
			arma::field<arma::Mat<fltp> > L(1,R.n_elem);
			arma::field<arma::Mat<fltp> > N(1,R.n_elem); 
			arma::field<arma::Mat<fltp> > D(1,R.n_elem);

			// darboux version
			if(use_frenet_serret_){
				// reverse
				// const bool flip = new_orientation_ ? (is_reverse_ ? k%2==0 : k%2==1) : false;

				// walk over sections
				for(arma::uword i=0;i<num_sections;i++){
					// create darboux generators
					const bool analytic = true; // we have J so lets use it
					Darboux db(V(i),A(i),J(i));
					if(arma::any(twisting(i)!=0))db.set_additional_twist(twisting(i),dtwisting(i)); 
					db.setup(analytic);

					// // get start radial vector
					// const arma::Col<fltp>::fixed<3> Dstart = -cmn::Extra::normalize(Vrad(i).head_cols(1)); //cmn::Extra::cross(V(i).col(0),{0,0,1}); 
					// const arma::Col<fltp>::fixed<3> Dend = -cmn::Extra::normalize(Vrad(i).tail_cols(1)); //cmn::Extra::cross(V(i).col(V(i).n_cols-1),{0,0,1}); 

					// // if the acceleration is very small at start or end
					// // we use the transverse vector there
					// const bool bad_start = arma::as_scalar(cmn::Extra::vec_norm(A(i).col(0)))<1e-5;
					// const bool bad_end = arma::as_scalar(cmn::Extra::vec_norm(A(i).col(A(i).n_cols-1)))<1e-5;
					// if(bad_start)db.set_first_transverse(Dstart);
					// if(bad_end)db.set_last_transverse(Dend);

					// make sure that the radial vector is the transverse vector
					// db.correct_sign(Dalign);

					// get vectors
					L(i) = db.get_longitudinal();
					N(i) = db.get_normal();
					D(i) = db.get_transverse(use_binormal_);

					// override vector when acceleration is zero
					const arma::Col<arma::uword> idx = arma::find(
						cmn::Extra::vec_norm(A(i))<RAT_CONST(1e-5) || 
						cmn::Extra::vec_norm(J(i))<RAT_CONST(1e-5));
					// D(i).cols(idx) = (flip?-1.0:1.0)*Vrad(i).cols(idx);
					D(i).cols(idx) = Vrad(i).cols(idx);
					N(i).cols(idx) = cmn::Extra::cross(L(i).cols(idx),D(i).cols(idx));

					// correct first independent of whether the point is valid here
					if(arma::as_scalar(cmn::Extra::dot(D(i).col(0),Vrad(i).col(0)))<0){
						D(i).col(0)*=-1; N(i).col(0)*=-1;
					}

					// normalize
					for(arma::uword j=0;j<idx.n_elem;j++){
						D(i).col(idx(j))/=arma::as_scalar(cmn::Extra::vec_norm(D(i).col(idx(j))));
						N(i).col(idx(j))/=arma::as_scalar(cmn::Extra::vec_norm(N(i).col(idx(j))));
					}

					// reverse
					// if(flip){D(i)*=-1; N(i)*=-1;}

					// get alignment vector for first section
					const arma::Col<fltp>::fixed<3> Dalign = 
						i==0 ? D(0).col(0) : D(i-1).col(D(i-1).n_cols-1);

					// check sign flips
					const arma::Row<fltp> sf = 
						cmn::Extra::dot(arma::join_horiz(
						Dalign,D(i).head_cols(D(i).n_cols-1)),D(i));

					// flip D when curvature changes from positive to negative
					int flipstate = 1;
					for(arma::uword j=0;j<D(i).n_cols;j++){
						// check whether flip is needed
						if(sf(j)<0)flipstate*=-1;

						// flip vectors (if needed)
						D(i).col(j)*=flipstate;
						N(i).col(j)*=flipstate;

						// stability fix
						if(std::abs(sf(j))<1e-7 && j!=0){
							D(i).col(j)=D(i).col(j-1);
							N(i).col(j)=N(i).col(j-1);
						}
					}

					// store alignment of the last transverse vector
					// Dalign = D(i).tail_cols(1);

					// check size
					assert(L(i).n_rows==3); assert(N(i).n_rows==3);	assert(D(i).n_rows==3);
					assert(R(i).is_finite()); assert(N(i).is_finite()); 
					assert(L(i).is_finite()); assert(D(i).is_finite()); 
					assert(N(i).n_cols==nscale(i).n_elem);

					// use binormal vector instead of darboux
					// if(use_binormal_)D(i) = cmn::Extra::cross(N(i),L(i));
				}
			}

			else{
				// walk over sections
				for(arma::uword i=0;i<num_sections;i++){
					// check v
					assert(arma::all(cmn::Extra::vec_norm(V(i))!=0));

					// store longitudinal vectors
					L(i) = V(i).each_row()/cmn::Extra::vec_norm(V(i));

					// calculate radial vector
					D(i) = Vrad(i);

					// create normal vectors
					N(i) = cmn::Extra::cross(L(i),D(i));


					// check length
					assert(arma::all(cmn::Extra::vec_norm(D(i))!=0));
					assert(arma::all(cmn::Extra::vec_norm(N(i))!=0));

					// normalize
					D(i).each_row()/=cmn::Extra::vec_norm(D(i));
					N(i).each_row()/=cmn::Extra::vec_norm(N(i));

					// add twisting
					for(arma::uword j=0;j<twisting(i).n_elem;j++){
						// this can be more efficient see pathcct
						if(twisting(i)(j)!=0){
							const arma::Mat<fltp>::fixed<3,3> M = cmn::Extra::create_rotation_matrix(L(i).col(j),twisting(i)(j));
							N(i).col(j) = M*N(i).col(j); D(i).col(j) = M*D(i).col(j);
						}
					}

					// check
					assert(L(i).n_rows==3); assert(N(i).n_rows==3);	assert(D(i).n_rows==3);
					assert(R(i).is_finite()); assert(N(i).is_finite()); 
					assert(L(i).is_finite()); assert(D(i).is_finite()); 
					assert(N(i).n_cols==nscale(i).n_elem);
				}
			}

			// add normal scaling
			for(arma::uword i=0;i<num_sections;i++){
				assert(N(i).n_cols==nscale(i).n_elem);
				N(i).each_row()%=nscale(i);
			}

			#ifndef NDEBUG
			for(arma::uword i=0;i<num_sections;i++){
				// check size
				assert(L(i).n_rows==3); assert(N(i).n_rows==3);	assert(D(i).n_rows==3);
				assert(R(i).is_finite()); assert(L(i).is_finite());
				assert(N(i).is_finite()); assert(D(i).is_finite());

				// check handedness
				assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N(i),L(i)),D(i))>RAT_CONST(0.0)));
			}
			#endif

			// // allocate list of frames
			// std::list<ShFramePr> frame_sublist;

			// create frame
			const ShFramePr frame = Frame::create(R,L,N,D);

			// set section and turn indices
			frame->set_location(section_idx,turn_idx,num_sections);

			// // add to list
			// frame_sublist.push_back(frame);

			// // create frame
			// const ShFramePr frame_combined = Frame::create(frame_sublist);

			// reverse
			if(rev){frame->reverse(); frame->flip();}

			// add frame to list
			frame_list.push_back(frame);
		}

		// combine frames
		const ShFramePr gen = Frame::create(frame_list);

		// transformations
		gen->apply_transformations(get_transformations(), stngs.time);

		// create frame
		return gen;
	}


	// re-index nodes after deleting
	void PathCCTCustom::reindex(){
		Transformations::reindex();
		std::map<arma::uword, ShCCTHarmonicPr> new_harmonics; arma::uword idx = 1;
		for(auto it=harmonics_.begin();it!=harmonics_.end();it++)
			if((*it).second!=NULL)new_harmonics.insert({idx++, (*it).second});
		harmonics_ = new_harmonics;
	}

	// check drives
	bool PathCCTCustom::is_valid_drives(const bool enable_throws)const{
		// number of poles
		// arma::uword num_poles_max = 1;
		// for(auto it=harmonics_.begin();it!=harmonics_.end();it++)
		// 	num_poles_max = std::max(num_poles_max,(*it).second->get_num_poles());

		// calculate number of sections per half
		// const arma::uword num_sect_per_turn = 4*num_poles_max;
		const fltp sectsize = arma::Datum<fltp>::tau/num_sect_per_turn_;

		// theta at which the leads are clipped
		const fltp thetamin_clip = nt1_*arma::Datum<fltp>::tau + (enable_lead1_ ? theta_lead1_ : 0.0);
		const fltp thetamax_clip = nt2_*arma::Datum<fltp>::tau - (enable_lead2_ ? theta_lead2_ : 0.0);

		// calculate maximum theta
		const fltp thetamin = std::min(RAT_CONST(0.0),thetamin_clip);
		const fltp thetamax = std::max(RAT_CONST(0.0),thetamax_clip);
		
		// normalized theta min and theta max
		// the drives will be running from -0.5 to 0.5 on this interval
		const fltp thetaminnrm = nt1nrm_*arma::Datum<fltp>::tau;
		const fltp thetamaxnrm = nt2nrm_*arma::Datum<fltp>::tau;

		// get number of sections
		const arma::sword num_int1 = arma::sword(std::floor(thetamin/sectsize));
		const arma::sword num_int2 = arma::sword(std::ceil(thetamax/sectsize));

		// calculate number of sections
		const arma::uword num_sections = num_int2 - num_int1;

		// allocate theta
		const arma::Row<fltp> theta = arma::linspace<arma::Row<fltp> >(num_int1*sectsize,num_int2*sectsize,num_sections+1);

		const arma::Row<fltp> position = CCTHarmonic::calc_position(theta, thetaminnrm, thetamaxnrm, normalize_length_);

		// check interpolation
		if(!use_local_radius_)if(arma::any(rho_->get_scaling_vec(position)<=0)){if(enable_throws){rat_throw_line("radius must be positive along full length of coil");} return false;};
		// if(arma::any(omega_->get_scaling_vec(CCTHarmonic::calc_position(theta, thetaminnrm, thetamaxnrm, normalize_length_))<0)){
		// 	if(enable_throws){rat_throw_line("pitch must be positive along full length of coil");} return false;};

		// all ok
		return true;
	}


	// validity check
	bool PathCCTCustom::is_valid(const bool enable_throws) const{
		// check transformations
		if(!Transformations::is_valid(enable_throws))return false;
		
		// check harmonics
		for(auto it=harmonics_.begin();it!=harmonics_.end();it++){
			const ShCCTHarmonicPr &harmonic = (*it).second;
			if(harmonic==NULL)rat_throw_line("harmonic list contains NULL");
			if(!harmonic->is_valid(enable_throws))return false;
		}

		// cehck drives
		if(omega_==NULL){if(enable_throws){rat_throw_line("omega drive points to NULL");} return false;};
		if(!omega_->is_valid(enable_throws))return false;
		if(rho_==NULL){if(enable_throws){rat_throw_line("rho drive points to NULL");} return false;};
		if(!rho_->is_valid(enable_throws))return false;
		if(normalize_length_){
			if(nt1nrm_>=nt2nrm_){if(enable_throws){rat_throw_line("nt1nrm must be larger than nt2nrm when normalization enabled");} return false;};
		}

		// check settings
		if(num_layers_<=0){if(enable_throws){rat_throw_line("number of layers must be larger than zero");} return false;};
		if(num_nodes_per_turn_<=0){if(enable_throws){rat_throw_line("number of nodes per turn must be larger than zero");} return false;};
		if(num_sect_per_turn_<=0){if(enable_throws){rat_throw_line("number of sections per turn must be larger than zero");} return false;};
		if(nt2_<=nt1_){if(enable_throws){rat_throw_line("nt1 must be smaller than nt2");} return false;};

		// check current leads
		if(enable_lead1_){
			if(leadness1_<=0){if(enable_throws){rat_throw_line("leadness for first lead must be larger than zero (when enabled)");} return false;};
		}
		if(enable_lead2_){
			if(leadness2_<=0){if(enable_throws){rat_throw_line("leadness for second lead must be larger than zero (when enabled)");} return false;};
		}
		if(enable_lead3_){
			if(leadness3_<=0){if(enable_throws){rat_throw_line("leadness for third lead must be larger than zero (when enabled)");} return false;};
		}
		if(enable_lead4_){
			if(leadness4_<=0){if(enable_throws){rat_throw_line("leadness for fourth lead must be larger than zero (when enabled)");} return false;};
		}
		
		// check clipping
		const fltp thetamin_clip = nt1_*arma::Datum<fltp>::tau + (enable_lead1_ ? theta_lead1_ : 0.0);
		const fltp thetamax_clip = nt2_*arma::Datum<fltp>::tau - (enable_lead2_ ? theta_lead2_ : 0.0);
		if(thetamax_clip<=thetamin_clip){if(enable_throws){rat_throw_line("coil including leads will have negative number of turns");} return false;};
		
		// drive check (takes too much time)
		// if(!is_valid_drives(enable_throws))return false;

		// all ok
		return true;
	}

	// get type
	std::string PathCCTCustom::get_type(){
		return "rat::mdl::pathcctcustom";
	}

	// method for serialization into json
	void PathCCTCustom::serialize(
		Json::Value &js, cmn::SList &list) const{
		// parent objects
		Transformations::serialize(js,list);

		// type
		js["type"] = get_type();

		// geometry
		js["nt1"] = nt1_;
		js["nt2"] = nt2_;
		js["nt3"] = nt3_;
		js["nt4"] = nt4_;
		js["nt1nrm"] = nt1nrm_;
		js["nt2nrm"] = nt2nrm_;
		js["num_nodes_per_turn"] = static_cast<unsigned int>(num_nodes_per_turn_);
		js["num_sect_per_turn"] = static_cast<unsigned int>(num_sect_per_turn_);
		js["normalize_length"] = normalize_length_;
		js["use_omega_normal"] = use_omega_normal_;
		js["use_field_style_omega"] = use_field_style_omega_;
		js["use_local_radius"] = use_local_radius_;
		js["use_frenet_serret"] = use_frenet_serret_;
		js["use_binormal"] = use_binormal_;

		// arrays
		js["omega"] = cmn::Node::serialize_node(omega_, list);
		js["rho"] = cmn::Node::serialize_node(rho_, list);
		js["twist"] = cmn::Node::serialize_node(twist_, list);
		js["normal_scaling"] = cmn::Node::serialize_node(normal_scaling_, list);
		js["bending_origin"] = bending_origin_;
		js["use_radius"] = use_radius_;
		js["bending_arc_length"] = bending_arc_length_;
		js["bending_radius"] = bending_radius_;
		js["is_reverse"] = is_reverse_;
		js["is_reverse_solenoid"] = is_reverse_solenoid_;
		js["is_solenoid"] = is_solenoid_;

		// flip behaviour (provides backwards compatibility with with models before v2.016.4)
		// js["new_orientation"] = new_orientation_;

		// layers
		js["num_layers"] = static_cast<int>(num_layers_);
		js["rho_increment"] = rho_increment_;

		// lead settings
		js["use_layer_jumps"] = use_layer_jumps_;
		js["enable_lead1"] = enable_lead1_;
		js["enable_lead2"] = enable_lead2_;
		js["enable_lead3"] = enable_lead3_;
		js["enable_lead4"] = enable_lead4_;
		js["leadness1"] = leadness1_;
		js["leadness2"] = leadness2_;
		js["leadness3"] = leadness3_;
		js["leadness4"] = leadness4_;
		js["zlead1"] = zlead1_;
		js["zlead2"] = zlead2_;
		js["zlead3"] = zlead3_;
		js["zlead4"] = zlead4_;
		js["theta_lead1"] = theta_lead1_;
		js["theta_lead2"] = theta_lead2_;
		js["theta_lead3"] = theta_lead3_;
		js["theta_lead4"] = theta_lead4_;

		// same angle or same amplitudes
		js["same_alpha"] = same_alpha_;

		// harmonics
		for(auto it = harmonics_.begin();it!=harmonics_.end();it++)
			js["harmonics"].append(cmn::Node::serialize_node((*it).second, list));
	}

	// method for deserialisation from json
	void PathCCTCustom::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent objects
		Transformations::deserialize(js,list,factory_list,pth);

		// geometry
		nt1_ = js["nt1"].ASFLTP();
		nt2_ = js["nt2"].ASFLTP();
		if(js.isMember("nt3"))nt3_ = js["nt3"].ASFLTP(); else nt3_ = RAT_CONST(0.0); // backwards compatibility v2.015.0
		if(js.isMember("nt4"))nt4_ = js["nt4"].ASFLTP(); else nt4_ = RAT_CONST(0.0);
		nt1nrm_ = js["nt1nrm"].ASFLTP();
		nt2nrm_ = js["nt2nrm"].ASFLTP();
		num_nodes_per_turn_ = js["num_nodes_per_turn"].asUInt64();
		set_is_reverse(js["is_reverse"].asBool());
		set_is_reverse_solenoid(js["is_reverse_solenoid"].asBool());
		set_is_solenoid(js["is_solenoid"].asBool());
		// set_new_orientation(js["new_orientation"].asBool()); // (provides backwards compatibility with with models before v2.016.4)

		// layers
		set_num_layers(js["num_layers"].asUInt64());
		set_rho_increment(js["rho_increment"].ASFLTP());

		// arrays
		set_omega(cmn::Node::deserialize_node<Drive>(js["omega"], list, factory_list, pth));
		set_rho(cmn::Node::deserialize_node<Drive>(js["rho"], list, factory_list, pth));
		if(js.isMember("twist"))set_twist(cmn::Node::deserialize_node<Drive>(js["twist"], list, factory_list, pth));
		if(js.isMember("normal_scaling"))set_normal_scaling(cmn::Node::deserialize_node<Drive>(js["normal_scaling"], list, factory_list, pth));
		set_bending_origin(js["bending_origin"].asBool());
		set_use_radius(js["use_radius"].asBool());
		set_bending_arc_length(js["bending_arc_length"].ASFLTP());
		set_bending_radius(js["bending_radius"].ASFLTP());
		set_normalize_length(js["normalize_length"].asBool());
		set_use_omega_normal(js["use_omega_normal"].asBool());
		set_use_field_style_omega(js["use_field_style_omega"].asBool());
		set_use_local_radius(js["use_local_radius"].asBool());
		set_use_frenet_serret(js["use_frenet_serret"].asBool());
		set_use_binormal(js["use_binormal"].asBool());

		// lead settings
		// leads have backwards compatibility with v2.015.0
		set_use_layer_jumps(js["use_layer_jumps"].asBool());
		set_enable_lead1(js["enable_lead1"].asBool());
		set_enable_lead2(js["enable_lead2"].asBool());
		if(js.isMember("enable_lead3"))set_enable_lead3(js["enable_lead3"].asBool()); else set_enable_lead3(js["enable_lead1"].asBool());
		if(js.isMember("enable_lead4"))set_enable_lead4(js["enable_lead4"].asBool()); else set_enable_lead4(js["enable_lead2"].asBool());
		set_leadness1(js["leadness1"].ASFLTP());
		set_leadness2(js["leadness2"].ASFLTP());
		if(js.isMember("leadness3"))set_leadness3(js["leadness3"].ASFLTP()); else set_leadness3(js["leadness1"].ASFLTP());
		if(js.isMember("leadness4"))set_leadness4(js["leadness4"].ASFLTP()); else set_leadness4(js["leadness2"].ASFLTP());
		set_zlead1(js["zlead1"].ASFLTP());
		set_zlead2(js["zlead2"].ASFLTP());
		if(js.isMember("zlead3"))set_zlead3(js["zlead3"].ASFLTP()); else set_zlead3(js["zlead1"].ASFLTP());
		if(js.isMember("zlead4"))set_zlead4(js["zlead4"].ASFLTP()); else set_zlead4(js["zlead2"].ASFLTP());
		set_theta_lead1(js["theta_lead1"].ASFLTP());
		set_theta_lead2(js["theta_lead2"].ASFLTP());
		if(js.isMember("theta_lead3"))set_theta_lead3(js["theta_lead3"].ASFLTP()); else set_theta_lead3(js["theta_lead1"].ASFLTP());
		if(js.isMember("theta_lead4"))set_theta_lead4(js["theta_lead4"].ASFLTP()); else set_theta_lead4(js["theta_lead2"].ASFLTP());

		// same angle or same amplitudes
		set_same_alpha(js["same_alpha"].asBool());

		// subnodes
		for(auto it = js["harmonics"].begin();it!= js["harmonics"].end();it++)
			add_harmonic(cmn::Node::deserialize_node<CCTHarmonic>(*it, list, factory_list, pth));

		// number of sections per turn
		if(js.isMember("num_sect_per_turn")){
			num_sect_per_turn_ = js["num_sect_per_turn"].asUInt64();
		}

		// backwards compatibility v2.014.0
		else{
			// number of poles
			arma::uword num_poles_max = 1;
			for(auto it=harmonics_.begin();it!=harmonics_.end();it++)
				num_poles_max = std::max(num_poles_max,(*it).second->get_num_poles());

			// calculate number of sections per half
			const arma::uword num_sect_per_pole = 4;
			const arma::uword num_sect_per_turn_min = 8; // Avoid FreeCAD 90 deg bending
			num_sect_per_turn_ = std::max(num_sect_per_turn_min, num_sect_per_pole*num_poles_max);
		}

	}

}}
