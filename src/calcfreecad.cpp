// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calcfreecad.hh"

// model headers
#include "splitable.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcFreeCAD::CalcFreeCAD(){
		set_name("Export FreeCAD"); 
	}

	CalcFreeCAD::CalcFreeCAD(const ShModelPr &model) : CalcFreeCAD(){
		set_model(model);
	}

	CalcFreeCAD::CalcFreeCAD(
		const ShModelPr &model, 
		const boost::filesystem::path& output_dir) : CalcFreeCAD(model){
		set_output_dir(output_dir);
	}

	// factory methods
	ShCalcFreeCADPr CalcFreeCAD::create(){
		return std::make_shared<CalcFreeCAD>();
	}

	ShCalcFreeCADPr CalcFreeCAD::create(const ShModelPr &model){
		return std::make_shared<CalcFreeCAD>(model);
	}

	ShCalcFreeCADPr CalcFreeCAD::create(
		const ShModelPr &model, 
		const boost::filesystem::path& output_dir){
		return std::make_shared<CalcFreeCAD>(model,output_dir);
	}

	// setters
	void CalcFreeCAD::set_model(const ShModelPr &model){
		model_ = model;
	}

	void CalcFreeCAD::set_output_dir(const boost::filesystem::path& output_dir){
		output_dir_ = output_dir;
	}

	void CalcFreeCAD::set_scaling_factor(const fltp scaling_factor){
		scaling_factor_ = scaling_factor;
	}

	void CalcFreeCAD::set_parallel_build(const bool parallel_build){
		parallel_build_ = parallel_build;
	}

	void CalcFreeCAD::set_run_build(const bool run_build){
		run_build_ = run_build;
	}

	void CalcFreeCAD::set_combine_models(const bool combine_models){
		combine_models_ = combine_models;
	}

	void CalcFreeCAD::set_create_union(const bool create_union){
		create_union_ = create_union;
	}

	void CalcFreeCAD::set_no_graphics(const bool no_graphics){
		no_graphics_ = no_graphics;
	}

	void CalcFreeCAD::set_num_subdivide(const arma::uword num_subdivide){
		num_subdivide_ = num_subdivide;
	}

	void CalcFreeCAD::set_num_refine_max(const arma::uword num_refine_max){
		num_refine_max_ = num_refine_max;
	}

	void CalcFreeCAD::set_num_node_max(const arma::uword num_node_max){
		num_node_max_ = num_node_max;
	}

	void CalcFreeCAD::set_override_ruled_surface(const bool override_ruled_surface){
		override_ruled_surface_ = override_ruled_surface;
	}

	void CalcFreeCAD::set_enable_refine(const bool enable_refine){
		enable_refine_ = enable_refine;
	}

	void CalcFreeCAD::set_enable_gui_update(const bool enable_gui_update){
		enable_gui_update_ = enable_gui_update;
	}

	void CalcFreeCAD::set_enable_step_file(const bool enable_step_file){
		enable_step_file_ = enable_step_file;
	}

	void CalcFreeCAD::set_enable_fcstd_file(const bool enable_fcstd_file){
		enable_fcstd_file_ = enable_fcstd_file;
	}

	


	// getters
	const boost::filesystem::path& CalcFreeCAD::get_output_dir()const{
		return output_dir_;
	}

	const ShModelPr& CalcFreeCAD::get_model()const{
		return model_;
	}

	fltp CalcFreeCAD::get_scaling_factor()const{
		return scaling_factor_;
	}

	bool CalcFreeCAD::get_parallel_build()const{
		return parallel_build_;
	}

	bool CalcFreeCAD::get_run_build()const{
		return run_build_;
	}

	bool CalcFreeCAD::get_combine_models()const{
		return combine_models_;
	}

	bool CalcFreeCAD::get_create_union()const{
		return create_union_;
	}

	bool CalcFreeCAD::get_no_graphics()const{
		return no_graphics_;
	}

	arma::uword CalcFreeCAD::get_num_subdivide()const{
		return num_subdivide_;
	}

	arma::uword CalcFreeCAD::get_num_refine_max()const{
		return num_refine_max_;
	}

	arma::uword CalcFreeCAD::get_num_node_max()const{
		return num_node_max_;
	}

	bool CalcFreeCAD::get_override_ruled_surface()const{
		return override_ruled_surface_;
	}

	bool CalcFreeCAD::get_enable_refine()const{
		return enable_refine_;
	}

	bool CalcFreeCAD::get_enable_gui_update()const{
		return enable_gui_update_;
	}

	bool CalcFreeCAD::get_enable_step_file()const{
		return enable_step_file_;
	}

	bool CalcFreeCAD::get_enable_fcstd_file()const{
		return enable_fcstd_file_;
	}


	// calculate with inductance data output
	void CalcFreeCAD::calculate_freecad(
		const fltp time, 
		const cmn::ShLogPr &lg, 
		const ShSolverCachePr& /*cache*/){

		// check validity
		is_valid(true);

		// get date and time
		auto timdata = std::time(nullptr);
		auto localtime = *std::localtime(&timdata);
		std::ostringstream date_stream,time_stream;
		date_stream << std::put_time(&localtime, "%d-%m-%Y");
		time_stream << std::put_time(&localtime, "%H:%M:%S");
		const std::string date_str = date_stream.str();
		const std::string time_str = time_stream.str();

		// general info
		lg->msg("%s%s=== Starting FreeCAD Export ===%s\n",KBLD,KGRN,KNRM);
		lg->msg(2,"%s%sGENERAL INFO%s\n",KBLD,KGRN,KNRM);
		lg->msg("date: %s%s%s %s(dd-mm-yyyy)%s\n",KYEL,date_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("time: %s%s%s %s(hh:mm:ss)%s\n",KYEL,time_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("model name: %s%s%s\n",KYEL,model_->get_name().c_str(),KNRM);
		lg->msg("path name: %s%s%s\n",KYEL,get_name().c_str(),KNRM);
		lg->msg(-2,"\n");

		// create directory
		boost::filesystem::create_directories(output_dir_);

		// serializer
		const mdl::ShSerializerPr slzr = mdl::Serializer::create();

		// get name
		std::string fname = model_->get_name();
		std::replace(fname.begin(), fname.end(), ' ', '_');

		// combine
		// const boost::filesystem::path fcmacro_file = output_dir_/"model.FCMacro";
		const boost::filesystem::path macro_file = output_dir_/(fname + ".FCMacro");
		const boost::filesystem::path fcmodel_file = output_dir_/(fname + ".FCStd");
		const boost::filesystem::path step_file = output_dir_/(fname + ".step");

		// parallel building of the model
		if(parallel_build_){
			// show meshes
			lg->msg(2,"%s%sWRITING PARALLEL BUILD SCRIPT(S)%s\n",KBLD,KGRN,KNRM);
			lg->msg(2,"%sFollowing Model Tree%s\n",KBLU,KNRM);

			// command file
			std::ofstream cmnd; 
			cmnd.open((output_dir_/"commands.txt").string(), std::ios::binary);

			// build freecad models
			create_freecad(cmnd, model_, time, output_dir_, slzr, lg);

			// close command file
			cmnd.close();

			// build done
			lg->msg(-4,"\n");

			// check cancel
			if(lg->is_cancelled())return;

			// run freecad scripts and wait to finish
			// this requires the parallel command to be installed on your distribution!
			if(run_build_){
				// show meshes
				lg->msg(2,"%s%sRUN PARALLEL BUILD(S)%s\n",KBLD,KGRN,KNRM);
				lg->msg(2,"%sCalling on command line (requires 'parallel' command)%s\n",KBLU,KNRM);

				// call build script on command line
				const std::string cmnd_str = "parallel -j-2 < " + (output_dir_/"commands.txt").string();
				lg->msg("%s\n",cmnd_str.c_str());
				std::system(cmnd_str.c_str());

				// build done
				lg->msg(-4,"\n");

				// check cancel
				if(lg->is_cancelled())return;
			}

			// combine models
			if(combine_models_){
				// show meshes
				lg->msg(2,"%s%sCOMBINE%s\n",KBLD,KGRN,KNRM);
				lg->msg(2,"%sFollowing Model Tree%s\n",KBLU,KNRM);

				// open stream
				std::ofstream script(macro_file.string(), std::ios::binary);

				// create command script
				combine_freecad(script,model_,time,output_dir_,slzr,lg);

				// ensure doc is uodated
				script<<"doc.recompute()\n";

				// save freecad file
				script<<"doc.saveAs('"<<fcmodel_file.generic_string()<<"')\n";

				// export step file
				script<<"Import.export(export,'"<<step_file.generic_string()<<"')\n";

				// done
				script<<"exit()\n";

				// close stream
				script.close();

				// build done
				lg->msg(-4,"\n");

				// check cancel
				if(lg->is_cancelled())return;
			}
		}

		// serial building of the model
		else{
			// show meshes
			lg->msg(2,"%s%sWRITING SERIAL BUILD SCRIPT%s\n",KBLD,KGRN,KNRM);
			lg->msg(2,"%sFollowing Model Tree%s\n",KBLU,KNRM);

			// create freecad file
			const cmn::ShFreeCADPr fc = create_freecad_file(macro_file, model_->get_name());

			// create a single macro containning all the parts
			Model::export_freecad(model_, fc);

			// save to step file
			if(enable_fcstd_file_)fc->save_doc(fcmodel_file);
			if(enable_step_file_)fc->save_step_file(step_file);

			// build done
			lg->msg(-4,"\n");

			// check cancel
			if(lg->is_cancelled())return;
		}

		// run the macro
		if(run_build_){
			// show meshes
			lg->msg(2,"%s%sBUILDING%s\n",KBLD,KGRN,KNRM);
			lg->msg(2,"%sCalling FreeCAD%s\n",KBLU,KNRM);

			// call build script on command line
			const std::string cmnd_str = get_freecad_command() + (enable_gui_update_ ? " " : " -c ") + macro_file.string();
			lg->msg("%s\n",cmnd_str.c_str());
			std::system(cmnd_str.c_str());

			// build done
			lg->msg(-4,"\n");
		}

		// done
		return;
	}

	std::string CalcFreeCAD::get_freecad_command(){
		#ifdef __APPLE__
		return "/Applications/FreeCAD.app/Contents/MacOS/FreeCAD";
		#else
		return "freecad";
		#endif
	}


	// generalized calculation
	std::list<ShDataPr> CalcFreeCAD::calculate(
		const fltp time, 
		const cmn::ShLogPr &lg, 
		const ShSolverCachePr& cache){
		calculate_freecad(time,lg,cache);
		return {};
	}

	// create freecad object and propagate settings
	cmn::ShFreeCADPr CalcFreeCAD::create_freecad_file(
		const boost::filesystem::path& fpath, 
		const std::string& model_name)const{
		const cmn::ShFreeCADPr fc = cmn::FreeCAD::create(fpath, model_name);
		fc->set_no_graphics(no_graphics_ || parallel_build_); 
		fc->set_enable_gui_update(enable_gui_update_);
		fc->set_create_union(create_union_);
		fc->set_override_ruled_surface(override_ruled_surface_);
		fc->set_enable_refine(enable_refine_);
		fc->set_num_max(num_node_max_);
		fc->set_scale(scaling_factor_);
		fc->set_num_sub(num_subdivide_);
		fc->set_num_refine_max(num_refine_max_);
		fc->set_enable_refine();
		return fc;
	}

	// export to FreeCAD
	void CalcFreeCAD::export_freecad(
		std::ofstream& cmnd,
		const mdl::ShModelPr& model, 
		const fltp time,
		const boost::filesystem::path &output_dir, 
		const std::string &fname,
		const cmn::ShLogPr& /*lg*/)const{

		// create directory
		boost::filesystem::create_directories(output_dir);

		// output file
		const boost::filesystem::path macro_file = output_dir/(fname + ".FCMacro");
		const boost::filesystem::path fcmodel_file = output_dir/(fname + ".FCStd");
		const boost::filesystem::path step_file = output_dir/(fname + ".step");

		// settings for constructing meshes
		mdl::MeshSettings stngs;
		stngs.time = time;
		stngs.low_poly = false;
		stngs.combine_sections = false;
		stngs.visual_tolerance = RAT_CONST(0.0);
		stngs.visual_radius = RAT_CONST(0.0);
		stngs.is_calculation = true;

		// create mesh objects
		const std::list<mdl::ShMeshDataPr> meshes = model->create_meshes({},stngs);

		// ensure unique names
		std::map<std::string, std::list<mdl::ShMeshDataPr> > list;
		for(auto it = meshes.begin();it!=meshes.end();it++){
			// get mesh
			const mdl::ShMeshDataPr& mesh = (*it);

			// count the number of times this name exists
			list[mesh->get_name()].push_back(mesh);
		}

		// set names
		for(auto it1 = list.begin();it1!=list.end();it1++){
			const std::string name = (*it1).first;
			const std::list<mdl::ShMeshDataPr>& mesh_list = (*it1).second;
			if(mesh_list.size()>1){
				arma::uword cnt = 0;
				for(auto it2 = mesh_list.begin();it2!=mesh_list.end();it2++,cnt++)
					(*it2)->set_name(name + "_" + std::to_string(cnt));
			}
		}

		// create freecad file
		{
			const cmn::ShFreeCADPr fc = create_freecad_file(macro_file, model->get_name());
			
			// write step file for each mesh
			for(auto it = meshes.begin();it!=meshes.end();it++){
				// get mesh
				const mdl::ShMeshDataPr& mesh = (*it);

				// export the mesh to freecad
				mesh->export_freecad(fc);
			}

			// save to step file
			fc->save_doc(fcmodel_file);
			fc->save_step_file(step_file);

			// remove existing freecad file so it doesn't create a second filename
			boost::filesystem::remove(fcmodel_file);
		}

		// add macro file to the command execution list
		cmnd<<get_freecad_command()<<" -c \""<<macro_file.string()<<"\"\n";
	}

	// recursive freecad creation
	void CalcFreeCAD::create_freecad(
		std::ofstream& cmnd,
		const mdl::ShModelPr& model, 
		const fltp time,
		const boost::filesystem::path& output_dir, 
		const mdl::ShSerializerPr& slzr, 
		const cmn::ShLogPr& lg,
		const arma::uword depth)const{

		// show model tree
		lg->msg("");
		for(arma::uword i=0;i<depth;i++)lg->msg(0,"==");
		if(depth!=0)lg->msg(0," ");
		lg->msg(0,"%s\n",model->get_name().c_str());

		// cast to splitable
		const mdl::ShSplitablePr base = std::dynamic_pointer_cast<mdl::Splitable>(model);

		// create filename
		std::string name = model->get_name();
		std::replace(name.begin(), name.end(), ' ', '_');

		// check if splitable model
		if(base!=NULL){
			// split
			std::list<mdl::ShModelPr> models = base->split(slzr);

			// recursive call
			for(auto it=models.begin();it!=models.end();it++)
				create_freecad(cmnd,*it,time,depth==0 ? output_dir : output_dir/name,slzr,lg,depth+1);

		}else{
			// convert leaf model into freecad file
			export_freecad(cmnd,model,time,output_dir,name,lg);
		}
	}

	// recursive freecad creation
	void CalcFreeCAD::combine_freecad_core(
		std::ofstream& script, 
		std::list<boost::filesystem::path>& dependencies,
		const arma::uword depth,
		const mdl::ShModelPr& model, 
		const fltp time,
		const boost::filesystem::path& output_dir,
		const mdl::ShSerializerPr& slzr,
		const cmn::ShLogPr& lg)const{

		// check cancel
		if(lg->is_cancelled())return;

		// show model tree
		lg->msg("");
		for(arma::uword i=0;i<depth;i++)lg->msg(0,"==");
		if(depth!=0)lg->msg(0," ");
		lg->msg(0,"%s\n",model->get_name().c_str());

		// cast to splitable
		const mdl::ShSplitablePr base = std::dynamic_pointer_cast<mdl::Splitable>(model);

		// create filename
		std::string name = model->get_name();
		std::replace(name.begin(), name.end(), ' ', '_');

		// check if splitable model
		if(base!=NULL){
			// split
			const std::list<mdl::ShModelPr> models = base->split(slzr);

			// create a a group for this model
			script<<"group"<<depth+1<<" = doc.addObject('App::DocumentObjectGroup','"<<model->get_name()<<"')\n";
			script<<"group"<<depth<<".addObject(group"<<(depth+1)<<")\n";

			// recursive call
			for(auto it=models.begin();it!=models.end();it++){
				// recursive call
				combine_freecad_core(script,dependencies,depth+1,*it,time,depth==0 ? output_dir : output_dir/name,slzr,lg);

				// check cancel
				if(lg->is_cancelled())return;
			}
			
			// // create parts list for this level
			// if(models.size()>1){
			// 	script<<"group"<<depth+1<<" = doc.addObject('App::DocumentObjectGroup','"<<model->get_name()<<"')\n";
			// 	script<<"group"<<depth<<".addObject(group"<<(depth+1)<<")\n";

			// 	// recursive call
			// 	for(auto it=models.begin();it!=models.end();it++)
			// 		combine_freecad_core(script,dependencies,depth+1,*it,depth==0 ? output_dir : output_dir/name,slzr,lg);
			// }else{
			// 	// recursive call
			// 	for(auto it=models.begin();it!=models.end();it++)
			// 		combine_freecad_core(script,dependencies,depth,*it,depth==0 ? output_dir : output_dir/name,slzr,lg);
			// }

		}else{
			// add step file
			script<<"part = doc.addObject('Part::Feature','"<<model->get_name()<<"')\n"; 
			script<<"part.Shape = Part.read('"<<(output_dir/name).generic_string()<<".step')\n";
			
			// set color
			if(!parallel_build_){
				if(!no_graphics_){
					script<<"part.ViewObject.LineColor = (1.0,1.0,1.0,0.0)\n";
					script<<"part.ViewObject.LineWidth = 0.5\n";
					script<<"part.ViewObject.ShapeColor = ("<<
						model->get_color()(0)<<","<<model->get_color()(1)<<","<<model->get_color()(2)<<",0.0)\n";
				}
			}

			// add to group
			script<<"group"<<std::to_string(depth)<<".addObject(part)\n";

			// add to export list
			script<<"export.append(part)\n";

			// add to dependency list
			dependencies.push_back(output_dir/(name + ".step"));
		}

		// done
		return;
	}

	// create combined freecad output file
	void CalcFreeCAD::combine_freecad(
		std::ofstream& script,
		const mdl::ShModelPr& model,
		const fltp time,
		const boost::filesystem::path& output_dir, 
		const mdl::ShSerializerPr& slzr,
		const cmn::ShLogPr& lg)const{

		// check cancel
		if(lg->is_cancelled())return;

		// create a stream
		std::list<boost::filesystem::path> dependencies;

		// start new document and load required libraries
		script<<"import FreeCAD,Draft,Part,Import\n";
		script<<"doc = App.newDocument('Combine')\n";
		script<<std::fixed<<std::setprecision(12);

		// create base group
		script<<"group0 = doc.addObject('App::DocumentObjectGroup','model')\n";

		// create export list
		script<<"export = []\n";

		// call recursive part
		combine_freecad_core(script, dependencies, 0, model, time, output_dir, slzr, lg);

		// recompute
		script<<"doc.recompute()\n";

		// done
		return;
	}

	// boolean
	void CalcFreeCAD::create_freecad_combine(
		std::ofstream& cmnd,
		const std::string& part_name,
		const std::list<boost::filesystem::path> &input_files, 
		const boost::filesystem::path &output_dir, 
		const cmn::ShLogPr& lg)const{

		// check cancel
		if(lg->is_cancelled())return;

		// create paths
		const boost::filesystem::path macro_file = output_dir/(part_name + ".FCMacro");
		const boost::filesystem::path step_file =  output_dir/(part_name + ".step");

		// create freecad script
		std::ofstream script(macro_file.string(), std::ios::binary);

		// start new document and load required libraries
		script<<"import FreeCAD,Draft,Part,Import\n";
		script<<"doc = App.newDocument('Combine')\n";
		script<<"all=[]\n";

		// load part A
		for(auto it=input_files.begin();it!=input_files.end();it++){
			script<<"part = doc.addObject('Part::Feature','part')\n"; 
			script<<"part.Shape = Part.read('"<<(*it).generic_string()<<"')\n";
			script<<"all.append(part)\n";
		}

		// recompute
		script<<"doc.recompute()\n";

		// combine with multi-fuse
		script<<"op = doc.addObject('Part::MultiFuse','Fusion')\n";
		script<<"op.Shapes = all\n";

		// recompute
		script<<"doc.recompute()\n";

		// take shape and store in feature
		script<<"F = doc.addObject('Part::Feature','"<<part_name<<"')\n";
		script<<"F.Shape = Part.getShape(op,'',needSubElement=False,refine=True)\n";
		
		// recompute
		script<<"doc.recompute()\n";

		// remove the operation
		// script<<"doc.removeObject(op.Name)\n";

		// export to output file
		script<<"Import.export([F],'"<<step_file.generic_string()<<"')\n";

		// close program
		script<<"exit()\n";

		// script done
		script.close();

		// make a command
		cmnd<<get_freecad_command()<<" -c \""<<macro_file.string()<<"\"\n";
	}

	// boolean
	void CalcFreeCAD::create_freecad_subtract(
		std::ofstream& cmnd,
		const std::string& part_name,
		const std::list<boost::filesystem::path> &input_files, 
		const boost::filesystem::path &output_dir, 
		const cmn::ShLogPr& /*lg*/)const{

		// create paths
		const boost::filesystem::path macro_file = output_dir/(part_name + ".FCMacro");
		const boost::filesystem::path step_file =  output_dir/(part_name + ".step");

		// create freecad script
		std::ofstream script(macro_file.string(), std::ios::binary);

		// start new document and load required libraries
		script<<"import FreeCAD,Draft,Part,Import\n";
		script<<"doc = App.newDocument('Combine')\n";
		script<<"all=[]\n";

		script<<"base = doc.addObject('Part::Feature','part')\n"; 
		script<<"base.Shape = Part.read('"<<(*input_files.begin()).generic_string()<<"')\n";

		// load parts
		for(auto it=std::next(input_files.begin());it!=input_files.end();it++){
			// load cutting tool
			script<<"part = doc.addObject('Part::Feature','part')\n"; 
			script<<"part.Shape = Part.read('"<<(*it).generic_string()<<"')\n";

			// combine with cut
			script<<"op = doc.addObject('Part::Cut','Cut')\n";
			script<<"op.Base = base\n";
			script<<"op.Tool = part\n";

			// update base
			script<<"base = op\n";
		}

		// recompute
		script<<"doc.recompute()\n";

		// take shape and store in feature
		script<<"F = doc.addObject('Part::Feature','"<<part_name<<"')\n";
		script<<"F.Shape = Part.getShape(base,'',needSubElement=False,refine=True)\n";
		
		// recompute
		script<<"doc.recompute()\n";

		// remove the operation
		// script<<"doc.removeObject(op.Name)\n";

		// export to output file
		script<<"Import.export([F],'"<<step_file.generic_string()<<"')\n";

		// close program
		script<<"exit()\n";

		// close script
		script.close();

		// add to freecad
		cmnd<<get_freecad_command()<<" -c \""<<macro_file.string()<<"\"\n";
	}

	// boolean
	void CalcFreeCAD::create_freecad_intersect(
		std::ofstream& cmnd,
		const std::string& part_name,
		const std::list<boost::filesystem::path> &input_files, 
		const boost::filesystem::path &output_dir, 
		const cmn::ShLogPr& /*lg*/)const{

		// create paths
		const boost::filesystem::path macro_file = output_dir/(part_name + ".FCMacro");
		const boost::filesystem::path step_file =  output_dir/(part_name + ".step");

		// create freecad script
		std::ofstream script(macro_file.string(), std::ios::binary);

		// start new document and load required libraries
		script<<"import FreeCAD,Draft,Part,Import\n";
		script<<"doc = App.newDocument('Combine')\n";
		script<<"all=[]\n";

		// load part A
		for(auto it=input_files.begin();it!=input_files.end();it++){
			script<<"part = doc.addObject('Part::Feature','part')\n"; 
			script<<"part.Shape = Part.read('"<<(*it).generic_string()<<"')\n";
			script<<"all.append(part)\n";
		}

		// recompute
		script<<"doc.recompute()\n";

		// combine with multi-fuse
		script<<"op = doc.addObject('Part::MultiCommon','Common')\n";
		script<<"op.Shapes = all\n";

		// recompute
		script<<"doc.recompute()\n";

		// take shape and store in feature
		script<<"F = doc.addObject('Part::Feature','"<<part_name<<"')\n";
		script<<"F.Shape = Part.getShape(op,'',needSubElement=False,refine=True)\n";
		
		// recompute
		script<<"doc.recompute()\n";

		// remove the operation
		// script<<"doc.removeObject(op.Name)\n";

		// export to output file
		script<<"Import.export([F],'"<<step_file.generic_string()<<"')\n";

		// close program
		script<<"exit()\n";

		// script done
		script.close();

		// make a command
		cmnd<<get_freecad_command()<<" -c \""<<macro_file.string()<<"\"\n";
	}

	// is valid
	bool CalcFreeCAD::is_valid(const bool enable_throws) const{
		if(output_dir_.empty()){if(enable_throws){rat_throw_line("output directory not set");} return false;}
		if(scaling_factor_<=0){if(enable_throws){rat_throw_line("scale factor must be larger than zero");} return false;}
		if(num_node_max_<=1){if(enable_throws){rat_throw_line("num node max must be larger than one");} return false;}
		if(num_refine_max_<=1){if(enable_throws){rat_throw_line("num refine max must be larger than zero");} return false;}
		if(enable_gui_update_ && no_graphics_){if(enable_throws){rat_throw_line("can not update gui when graphics is disabled");} return false;}
		return true;
	}

	// serialization
	std::string CalcFreeCAD::get_type(){
		return "rat::mdl::calcfreecad";
	}

	void CalcFreeCAD::serialize(Json::Value &js, cmn::SList &list) const{
		Calc::serialize(js,list);
		js["type"] = get_type();
		js["model"] = cmn::Node::serialize_node(model_, list);
		js["output_directory"] = output_directory_.string();
		js["scaling_factor"] = scaling_factor_;
		js["parallel_build"] = parallel_build_;
		js["run_build"] = run_build_;
		js["combine_models"] = combine_models_;
		js["create_union"] = create_union_;
		js["no_graphics"] = no_graphics_;
		js["enable_refine"] = enable_refine_;
		js["num_subdivide"] = static_cast<int>(num_subdivide_);
		js["num_refine_max"] = static_cast<int>(num_refine_max_);
		js["num_node_max"] = static_cast<int>(num_node_max_);
		js["override_ruled_surface"] = override_ruled_surface_;
		js["enable_gui_update"] = enable_gui_update_;
		js["enable_step_file"] = enable_step_file_;
		js["enable_fcstd_file"] = enable_fcstd_file_;
	}
	
	void CalcFreeCAD::deserialize(const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		Calc::deserialize(js,list,factory_list,pth);
		set_model(cmn::Node::deserialize_node<Model>(js["model"], list, factory_list, pth));
		scaling_factor_ = js["scaling_factor"].ASFLTP();
		parallel_build_ = js["parallel_build"].asBool();
		run_build_ = js["run_build"].asBool();
		combine_models_ = js["combine_models"].asBool();
		create_union_ = js["create_union"].asBool();
		no_graphics_ = js["no_graphics"].asBool();
		num_subdivide_ = js["num_subdivide"].asUInt64();
		num_refine_max_ = js["num_refine_max"].asUInt64();
		num_node_max_ = js["num_node_max"].asUInt64();
		enable_refine_ = js["enable_refine"].asBool();
		override_ruled_surface_ = js["override_ruled_surface"].asBool();
		enable_gui_update_ = js["enable_gui_update"].asBool();
		enable_step_file_ = js["enable_step_file"].asBool();
		enable_fcstd_file_ = js["enable_fcstd_file"].asBool();
	}

}}