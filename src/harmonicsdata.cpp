// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "harmonicsdata.hh"

#include "rat/common/error.hh"

#include "path.hh"
#include "pathgroup.hh"
#include "pathstraight.hh"
#include "model.hh"
#include "vtkunstr.hh"
#include "vtktable.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	HarmonicsData::HarmonicsData(){
		set_output_type("harmonics");
	}

	// constructor
	HarmonicsData::HarmonicsData(
		const ShFramePr &frame, 
		const fltp reference_radius, 
		const bool compensate_curvature, 
		const bool use_magnetic_field,
		const arma::uword num_theta) : HarmonicsData(){
		setup(frame,reference_radius,compensate_curvature,use_magnetic_field,num_theta);
	}

	// factory
	ShHarmonicsDataPr HarmonicsData::create(){
		return std::make_shared<HarmonicsData>();
	}

	// factory
	ShHarmonicsDataPr HarmonicsData::create(
		const ShFramePr &frame, 
		const fltp reference_radius, 
		const bool compensate_curvature, 
		const bool use_magnetic_field,
		const arma::uword num_theta){
		return std::make_shared<HarmonicsData>(frame,reference_radius,compensate_curvature,use_magnetic_field,num_theta);
	}

	// set max harmonic required
	void HarmonicsData::set_num_max(const arma::uword num_max){
		num_max_ = num_max;
	}

	// get max harmonic required
	arma::uword HarmonicsData::get_num_max() const{
		return num_max_;
	}

	// get reference radius
	fltp HarmonicsData::get_reference_radius() const{
		return reference_radius_;
	}

	// harmonics calculation
	void HarmonicsData::setup(
		const ShFramePr &frame, 
		const fltp reference_radius, 
		const bool compensate_curvature, 
		const bool use_magnetic_field,
		const arma::uword num_theta){

		// check settings
		if(reference_radius<=0)rat_throw_line("radius must be larger than zero");
		if(frame==NULL)rat_throw_line("frame points to null");

		// output field required
		if(use_magnetic_field){
			set_field_type('H',3);
		}else{
			set_field_type('B',3);
		}

		// store reference radius
		reference_radius_ = reference_radius;

		// create circle
		theta_ = arma::linspace<arma::Row<fltp> >
			(0,((static_cast<fltp>(num_theta)-RAT_CONST(1.0))/static_cast<fltp>(num_theta))*2*arma::Datum<fltp>::pi,num_theta);
		const arma::Row<fltp> rho = arma::Row<fltp>(num_theta,arma::fill::ones)*reference_radius_;
		const arma::Row<fltp> u = rho%arma::cos(theta_);
		const arma::Row<fltp> v = rho%arma::sin(theta_);

		// get line properties
		Rb_ = frame->get_coords(0);
		Lb_ = frame->get_direction(0);
		Nb_ = frame->get_normal(0);
		Db_ = frame->get_transverse(0);

		// calculate length
		const arma::Row<fltp> dl = arma::sqrt(arma::sum(arma::pow(arma::diff(Rb_,1,1),2),0));
		ell_ = arma::join_horiz(arma::Row<fltp>{0},arma::cumsum(dl));

		// number of nodes in basepath
		const arma::uword num_ell = Rb_.n_cols;

		// allocate position matrices
		arma::Mat<fltp> x(num_ell,num_theta);
		arma::Mat<fltp> y(num_ell,num_theta);
		arma::Mat<fltp> z(num_ell,num_theta);
		
		// walk over nodes
		for(arma::uword i=0;i<num_ell;i++){
			x.row(i) = Rb_(0,i) + Db_(0,i)*v + Nb_(0,i)*u;
			y.row(i) = Rb_(1,i) + Db_(1,i)*v + Nb_(1,i)*u;
			z.row(i) = Rb_(2,i) + Db_(2,i)*v + Nb_(2,i)*u;
		}

		// get length
		const arma::Mat<fltp> ell_diff = arma::sqrt(
			arma::diff(x,1,0)%arma::diff(x,1,0) + 
			arma::diff(y,1,0)%arma::diff(y,1,0) + 
			arma::diff(z,1,0)%arma::diff(z,1,0)); 
		
		// effective length of each target point
		// used to compensate the curvature
		if(compensate_curvature){
			ellt_ = arma::vectorise((arma::join_vert(arma::Row<fltp>(num_theta,arma::fill::zeros),ell_diff) + 
				arma::join_vert(ell_diff,arma::Row<fltp>(num_theta,arma::fill::zeros)))/2).t();
		}else{
			arma::Col<fltp> ellav = arma::diff(ell_.t(),1,0);
			ellav = (arma::join_vert(arma::Col<fltp>{0}, ellav) + arma::join_vert(ellav,arma::Col<fltp>{0}))/2; 
			arma::Mat<fltp> ellt(num_ell,num_theta); ellt.each_col() = ellav;
			ellt_ = arma::vectorise(ellt).t();
		}

		// convert to coordinates
		// Rt_.set_size(3,num_ell*num_theta);
		// Rt_.row(0) = arma::vectorise(x).t();
		// Rt_.row(1) = arma::vectorise(y).t();
		// Rt_.row(2) = arma::vectorise(z).t();
		Rt_ = arma::join_vert(arma::vectorise(x).t(), arma::vectorise(y).t(), arma::vectorise(z).t());

		// set number of targets
		num_targets_ = Rt_.n_cols;
		assert(num_targets_>0);
	}

	// create line elements connecting the target points
	arma::Mat<arma::uword> HarmonicsData::create_line_elements() const{
		// get counters
		const arma::uword num_theta = theta_.n_elem;
		const arma::uword num_ell = ell_.n_elem;

		// allocate
		arma::Mat<arma::uword> n(2,num_theta*num_ell);

		// create line elements
		for(arma::uword i=0;i<num_ell;i++){
			// connect intermediate
			for(arma::uword j=0;j<num_theta-1;j++)
				n.col(i*num_theta + j) = arma::Col<arma::uword>::fixed<2>{j*num_ell+i,(j+1)*num_ell+i};

			// connect last to first
			n.col(i*num_theta + num_theta-1) = 
				arma::Col<arma::uword>::fixed<2>{(num_theta-1)*num_ell+i,i};
		}

		// return matrix with nodes
		return n;
	}

	// post process
	void HarmonicsData::post_process(){
		// check if calculation was done
		if(!has_field())rat_throw_line("no field was calculated");

		// get counters
		const arma::uword num_theta = theta_.n_elem;
		const arma::uword num_ell = ell_.n_elem;

		// get results
		arma::Mat<fltp> B;
		if(has('H')){
			B = get_field('H')*arma::Datum<fltp>::mu_0;
		}else if(has('B')){
			B = get_field('B');
		}else{
			rat_throw_line("magnetic field was not calculated");
		}

		// check field
		assert(B.n_cols==num_ell*num_theta);

		// calculate quasi-integrated harmonics
		B.each_row() %= ellt_;

		// reshape to matrices
		const arma::Mat<fltp> Bx = arma::reshape(B.row(0),num_ell,num_theta);
		const arma::Mat<fltp> By = arma::reshape(B.row(1),num_ell,num_theta);
		const arma::Mat<fltp> Bz = arma::reshape(B.row(2),num_ell,num_theta);
		
		// get in plane components of the field
		// we ignore the out of plane component here
		// this is why it is called pseudo-harmonics
		arma::Mat<fltp> Bu(num_ell,num_theta);
		arma::Mat<fltp> Bv(num_ell,num_theta);
		arma::Mat<fltp> Bw(num_ell,num_theta);
		for(arma::uword i=0;i<num_ell;i++){
			// allocate
			arma::Mat<fltp> Bt(3,num_theta); 
			arma::Mat<fltp> Dext(3,num_theta);
			arma::Mat<fltp> Next(3,num_theta); 
			arma::Mat<fltp> Lext(3,num_theta);

			// get magetic field
			Bt.row(0) = Bx.row(i); 
			Bt.row(1) = By.row(i); 
			Bt.row(2) = Bz.row(i);
			
			// get orientation vectors
			Dext.each_col() = Db_.col(i); 
			Next.each_col() = Nb_.col(i);
			Lext.each_col() = Lb_.col(i);
			
			// calculate dot products
			Bu.row(i) = cmn::Extra::dot(Bt,Next); 
			Bv.row(i) = cmn::Extra::dot(Bt,Dext);
			Bw.row(i) = cmn::Extra::dot(Bt,Lext);
		}

		// calculate radial component
		const arma::Mat<fltp> Br = 
			Bu.each_row()%arma::cos(theta_) + 
			Bv.each_row()%arma::sin(theta_);

		// check radial field
		assert(Br.is_finite());

		// fourier transform (applies to columns)
		const arma::Mat<std::complex<fltp> > M = arma::fft(Br.t()).st();
		An_ = (2.0/num_theta)*arma::real(M.cols(0,num_theta/2-1));
		Bn_ = (2.0/num_theta)*arma::imag(M.cols(0,num_theta/2-1));

		// divide by (average) length to keep units of [T]
		arma::Col<fltp> ell = arma::diff(ell_.t(),1,0);
		ell = (arma::join_vert(arma::Col<fltp>{0}, ell) + 
			arma::join_vert(ell,arma::Col<fltp>{0}))/2; 
		An_.each_col()/=ell; Bn_.each_col()/=ell;

		// calculate axial field
		Ba_ = (arma::mean(Bw,1)/ell).t();
	}

	// display harmonics in terminal/log
	void HarmonicsData::display(const cmn::ShLogPr &lg) const{
		// header
		lg->msg(2,"%s%sTERMINAL OUTPUT%s\n",KBLD,KGRN,KNRM);
		
		// walk over time steps
		lg->msg(2,"%sintegrated harmonic content%s\n",KBLU,KNRM);
		lg->msg("%s%3s %8s %8s %8s %8s%s\n",KBLD,"n","An","Bn","an","bn",KNRM);
		arma::Row<fltp> An,Bn; get_harmonics(An,Bn);
		const arma::uword idx = arma::index_max(arma::max(arma::abs(An),arma::abs(Bn)));
		const fltp ABmax = std::max(std::abs(An(idx)),std::abs(Bn(idx)));
		const arma::Row<fltp> an = 1e4*An/ABmax; 
		const arma::Row<fltp> bn = 1e4*Bn/ABmax;
		assert(num_max_<Bn.n_elem);
		for(arma::uword i=1;i<=num_max_;i++){
			if(idx==i){
				if(std::abs(An(idx))>std::abs(Bn(idx)))lg->msg("%03i %s%+8.2e%s %+8.2e %+08.1f %+08.1f\n",i,KYEL,An(i),KNRM,Bn(i),an(i),bn(i));
				else lg->msg("%03i %+8.2e %s%+8.2e%s %+08.1f %+08.1f\n",i,An(i),KYEL,Bn(i),KNRM,an(i),bn(i));
			}
			else{
				lg->msg("%03i %+8.2e %+8.2e %+08.1f %+08.1f\n",i,An(i),Bn(i),an(i),bn(i));
			}
		}
		lg->msg("* in [Tm] at radius %2.2f [mm]\n",1e3*reference_radius_);
		lg->msg(-2,"\n");
		lg->msg(-2,"\n");
	}


	// get harmonic content
	void HarmonicsData::get_harmonics(
		arma::Row<fltp> &ell, 
		arma::Mat<fltp> &An, 
		arma::Mat<fltp> &Bn) const{

		// check if harmonics where calculated
		if(An_.is_empty())rat_throw_line("harmonics not calculated");
		if(Bn_.is_empty())rat_throw_line("harmonics not calculated");

		// copy matrices to output
		ell = ell_; An = An_.cols(0,num_max_); Bn = Bn_.cols(0,num_max_);
	}

	// getting integrated harmonics
	void HarmonicsData::get_harmonics(
		arma::Row<fltp> &An, 
		arma::Row<fltp> &Bn) const{

		// check if harmonics where calculated
		if(An_.is_empty())rat_throw_line("harmonics not calculated");
		if(Bn_.is_empty())rat_throw_line("harmonics not calculated");

		// integrate along ell
		An = arma::trapz(ell_,An_.cols(0,num_max_),0);
		Bn = arma::trapz(ell_,Bn_.cols(0,num_max_),0); 
	}

	// get fringe field
	const arma::Row<fltp>& HarmonicsData::get_axial_field()const{
		return Ba_;
	}

	const arma::Row<fltp>& HarmonicsData::get_ell() const{
		return ell_;
	}

	// write surface to VTK file
	ShVTKTablePr HarmonicsData::export_vtk_table() const{
		// check if harmonics where calculated
		if(An_.is_empty())rat_throw_line("harmonics not calculated");
		if(Bn_.is_empty())rat_throw_line("harmonics not calculated");

		// get counters
		const arma::uword num_ell = ell_.n_elem;

		// create table
		const ShVTKTablePr vtk_table = VTKTable::create(num_ell);

		// add length
		vtk_table->set_data(ell_.t(),"ell");
		
		// add harmonic content
		for(arma::uword i=1;i<=num_max_;i++){
			std::string data_name; 
			if(i<10)data_name = "A0" + std::to_string(i) + " [T]";
			else data_name = "A" + std::to_string(i) + " [T]";
			vtk_table->set_data(An_.col(i),data_name);
		}
		
		// add harmonic content
		for(arma::uword i=1;i<=num_max_;i++){
			std::string data_name; 
			if(i<10)data_name = "B0" + std::to_string(i) + " [T]";
			else data_name = "B" + std::to_string(i) + " [T]";
			vtk_table->set_data(Bn_.col(i),data_name);
		}

		// return table object
		return vtk_table;
	}

	// export points/lines to vtk file
	ShVTKObjPr HarmonicsData::export_vtk() const{
		// get counters
		const arma::uword num_theta = theta_.n_elem;
		const arma::uword num_ell = ell_.n_elem;

		// create vtk data object
		ShVTKUnstrPr vtk_data = VTKUnstr::create();

		// check if harmonics where calculated
		if(An_.is_empty())rat_throw_line("harmonics not calculated");
		if(Bn_.is_empty())rat_throw_line("harmonics not calculated");

		// export node coordinates
		vtk_data->set_nodes(Rt_);

		// create line elements
		const arma::uword line_type = 3;
		const arma::Mat<arma::uword> n = create_line_elements();
		vtk_data->set_elements(n,line_type);

		// magnetic field data
		arma::Mat<fltp> B;
		if(has('H')){
			B = get_field('H')*arma::Datum<fltp>::mu_0;
		}else if(has('B')){
			B = get_field('B');
		}else{
			rat_throw_line("magnetic field was not calculated");
		}
		vtk_data->set_nodedata(B,"Mgn. Flux Density [T]");

		// harmonic content
		arma::Mat<fltp> MA(num_theta*num_ell,num_max_+1);
		arma::Mat<fltp> MB(num_theta*num_ell,num_max_+1);
		for(arma::uword j=0;j<num_ell;j++){
			for(arma::uword k=0;k<num_theta;k++){
				MA.row(k*num_ell + j) = An_.submat(j,0,j,num_max_);
				MB.row(k*num_ell + j) = Bn_.submat(j,0,j,num_max_);
			}
		}

		// convert harmonics to VTK
		// skew harmonics
		for(arma::uword j=1;j<=num_max_;j++){
			std::string name; 
			if(j<10)name = "A0" + std::to_string(j) + " [T]";
			else name = "A" + std::to_string(j) + " [T]";
			vtk_data->set_nodedata(MA.col(j).t(),name);
		}

		// main harmonics
		for(arma::uword j=1;j<=num_max_;j++){
			std::string name; 
			if(j<10)name = "B0" + std::to_string(j) + " [T]";
			else name = "B" + std::to_string(j) + " [T]";
			vtk_data->set_nodedata(MB.col(j).t(),name);
		}

		// return data object
		return vtk_data;
	}

	// // export a vtk with times
	// void HarmonicsData::write(cmn::ShLogPr lg){
	// 	// check if data directory set
	// 	if(output_dir_.empty())return;

	// 	// header
	// 	lg->msg(2,"%s%sWRITING OUTPUT FILES%s\n",KBLD,KGRN,KNRM);
		
	// 	// check output directory
	// 	if(output_times_.is_empty())rat_throw_line("output times are not set");

	// 	// create output directory
	// 	boost::filesystem::create_directory(output_dir_);

	// 	// output filename
	// 	std::string fname  = output_fname_;

	// 	// set time
	// 	set_time(output_times_(0));

	// 	// display harmonics at first time
	// 	display(lg);

	// 	// report
	// 	lg->msg(2,"%s%sVISUALISATION TOOLKIT%s\n",KBLD,KGRN,KNRM);

	// 	// settings report
	// 	display_settings(lg);

	// 	// report
	// 	lg->msg(2, "%swriting harmonics geometry%s\n",KBLU,KNRM);
	// 	lg->msg("%s%4s %8s %s%s\n",KBLD,"id","time","filename",KNRM);

	// 	// walk over timesteps
	// 	for(arma::uword i=0;i<output_times_.n_elem;i++){
	// 		// set time
	// 		set_time(output_times_(i));

	// 		// get data at this time
	// 		ShVTKUnstrPr vtk_harm = export_vtk_coord();

	// 		// extend filename with index
	// 		if(output_times_.n_elem!=1)fname = indexed_output_fname(output_fname_,i);

	// 		// show in log
	// 		lg->msg("%04llu %8.2e %s\n",i,get_time(),fname.c_str());

	// 		// write data to file
	// 		vtk_harm->write(output_dir_/(output_fname_ + "_hmsh"));
	// 	}
	// 	lg->msg(-2,"\n");

	// 	lg->msg(2, "%swriting harmonics table%s\n",KBLU,KNRM);
	// 	lg->msg("%s%4s %8s %s%s\n",KBLD,"id","time","filename",KNRM);

	// 	// walk over timesteps
	// 	for(arma::uword i=0;i<output_times_.n_elem;i++){
	// 		// set time
	// 		set_time(output_times_(i));

	// 		// get data at this time
	// 		ShVTKTablePr vtk_table = export_vtk_table();

	// 		// extend filename with index
	// 		if(output_times_.n_elem!=1)fname = indexed_output_fname(output_fname_,i);

	// 		// show in log
	// 		lg->msg("%04llu %8.2e %s\n",i,get_time(),fname.c_str());

	// 		// write data to file
	// 		vtk_table->write(output_dir_/(output_fname_+"_htb"));
	// 	}

	// 	// return
	// 	lg->msg(-6,"\n"); 
	// }

	// // get type
	// std::string HarmonicsData::get_type(){
	// 	return "rat::mdl::calcharmonics";
	// }

	// // method for serialization into json
	// void HarmonicsData::serialize(Json::Value &js, cmn::SList &list) const{
	// 	// serialize fieldmap
	// 	CalcFieldMap::serialize(js,list);

	// 	// properties
	// 	js["type"] = get_type();
	// 	js["radius"] = radius_;
	// 	js["num_theta"] = (unsigned int)num_theta;
	// 	js["compensate_curvature"] = compensate_curvature_;

	// 	// subnodes
	// 	js["base"] = cmn::Node::serialize_node(base_, list);
	// }

	// // method for deserialisation from json
	// void HarmonicsData::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
	// 	// serialize fieldmap
	// 	CalcFieldMap::deserialize(js,list,factory_list,pth);

	// 	// properties
	// 	radius_ = js["radius"].ASFLTP();
	// 	num_theta = js["num_theta"].asUInt64();
	// 	compensate_curvature_ = js["compensate_curvature"].asBool();

	// 	// subnodes
	// 	base_ = cmn::Node::deserialize_node<Path>(js["base"], list, factory_list, pth);
	// }


}}
