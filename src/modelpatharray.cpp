// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelpatharray.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	ModelPathArray::ModelPathArray(){
		set_name("Path Array");
	}

	// constructor with setting of the base path
	ModelPathArray::ModelPathArray(
		const ShModelPr &model, 
		const ShPathPr &pth, 
		const arma::uword num_ell, 
		const arma::uword num_parts,
		const arma::sword num_shift,
		const fltp ell_velocity) : ModelPathArray(){

		// set to self
		add_model(model); set_input_path(pth); 
		set_num_parts(num_parts); set_num_ell(num_ell);
		set_ell_velocity(ell_velocity);
		set_num_shift(num_shift);
	}

	// constructor with setting of the base path
	ModelPathArray::ModelPathArray(
		const std::list<ShModelPr> &model_list,
		const ShPathPr &pth,
		const arma::uword num_ell,
		const arma::uword num_parts,
		const arma::sword num_shift,
		const fltp ell_velocity) : ModelPathArray(){

		// set to self
		add_models(model_list); set_input_path(pth);
		set_num_parts(num_parts); set_num_ell(num_ell);
		set_ell_velocity(ell_velocity);
		set_num_shift(num_shift);
	}

	// factory
	ShModelPathArrayPr ModelPathArray::create(){
		return std::make_shared<ModelPathArray>();
	}

	// factory with setting of the base path
	ShModelPathArrayPr ModelPathArray::create(
		const ShModelPr &model,
		const ShPathPr &pth,
		const arma::uword num_ell,
		const arma::uword num_parts,
		const arma::sword num_shift,
		const fltp ell_velocity){
		return std::make_shared<ModelPathArray>(model,pth,num_ell,num_parts,num_shift,ell_velocity);
	}

	// factory with setting of the base path
	ShModelPathArrayPr ModelPathArray::create(
		const std::list<ShModelPr> &model_list,
		const ShPathPr &pth,
		const arma::uword num_ell,
		const arma::uword num_parts,
		const arma::sword num_shift,
		const fltp ell_velocity){
		return std::make_shared<ModelPathArray>(model_list,pth,num_ell,num_parts,num_shift,ell_velocity);
	}


	// setters
	void ModelPathArray::set_num_ell(const arma::uword num_ell){
		num_ell_ = num_ell;
	}

	void ModelPathArray::set_num_parts(const arma::uword num_parts){
		num_parts_ = num_parts;
	}
	
	void ModelPathArray::set_ell_shift(const fltp ell_shift){
		ell_shift_ = ell_shift;
	}

	void ModelPathArray::set_ell_velocity(const fltp ell_velocity){
		ell_velocity_ = ell_velocity;
	}

	void ModelPathArray::set_num_shift(const arma::sword num_shift){
		num_shift_ = num_shift;
	}


	// getters
	arma::uword ModelPathArray::get_num_ell()const{
		return num_ell_;
	}

	arma::uword ModelPathArray::get_num_parts()const{
		return num_parts_;
	}

	fltp ModelPathArray::get_ell_shift()const{
		return ell_shift_;
	}

	fltp ModelPathArray::get_ell_velocity()const{
		return ell_velocity_;
	}

	arma::sword ModelPathArray::get_num_shift()const{
		return num_shift_;
	}


	// splitting the modelgroup
	std::list<ShModelPr> ModelPathArray::split(const ShSerializerPr &slzr) const{
		// check validity
		is_valid(true);

		// settings
		const fltp time = RAT_CONST(0.0);

		// create new model list
		std::list<ShModelPr> model_list;

		// create a frame
		const ShFramePr base_frame = input_path_->create_frame(MeshSettings());
		
		// combine sections on frame
		base_frame->combine();

		// check loop
		const bool is_loop = base_frame->is_loop();

		// get frame coordinates and orientation
		const arma::Mat<fltp>& Rf = base_frame->get_coords(0);
		// const arma::Mat<fltp>& Lf = base_frame->get_direction(0);
		const arma::Mat<fltp>& Nf = base_frame->get_normal(0);
		const arma::Mat<fltp>& Df = base_frame->get_transverse(0);

		// get length coordinates of the frame
		const arma::Row<fltp> ell = arma::join_horiz(
			arma::Row<fltp>{RAT_CONST(0.0)},
			arma::cumsum(cmn::Extra::vec_norm(arma::diff(Rf,1,1))));

		// walk over positions
		const arma::uword num_parts = num_parts_==0 ? num_ell_ : num_parts_;
		for(arma::uword i=0;i<num_parts;i++){
			// allocate
			arma::Col<fltp>::fixed<3> Rc, Lc, Nc, Dc;

			// length coordinate
			fltp ellc = (arma::sword(i)+num_shift_)*ell.back()/num_ell_ + ell_shift_ + ell_velocity_*time;
			if(is_loop)ellc = ellc - ell.back()*std::floor(ellc/ell.back());

			// walk over xyz
			for(arma::uword j=0;j<3;j++){
				Rc(j) = cmn::Extra::interp1(ell.t(), Rf.row(j).t(), ellc, "linear", true);
				// Lc(j) = cmn::Extra::interp1(ell.t(), Lf.row(j).t(), ellc, "linear", true);
				Nc(j) = cmn::Extra::interp1(ell.t(), Nf.row(j).t(), ellc, "linear", true);
				Dc(j) = cmn::Extra::interp1(ell.t(), Df.row(j).t(), ellc, "linear", true);
			}

			// normalize orientation vectors
			// Lc.each_row()/=cmn::Extra::vec_norm(Lc);
			Nc.each_row()/=cmn::Extra::vec_norm(Nc);
			Dc.each_row()/=cmn::Extra::vec_norm(Dc);
			Lc = cmn::Extra::cross(Dc,Nc);
			Lc.each_row()/=cmn::Extra::vec_norm(Lc);

			// force orthogonal as the interpolation can cause it not to be
			Nc = cmn::Extra::cross(Lc,Dc);		

			// check
			assert(Rc.is_finite()); assert(Nc.is_finite());
			assert(Dc.is_finite()); assert(Lc.is_finite());

			// create rotation
			// create matrix
			const arma::Mat<fltp>::fixed<3,3> M = arma::join_horiz(Lc,Dc,Nc);
			assert(cmn::Extra::is_pure_rotation_matrix(M));
			const ShTransRotatePr rotation = TransRotate::create(M);
			rotation->set_offset(Rc);

			// create name for this carriage
			std::stringstream str;
			str<<get_name()<<"_"<<std::setfill('0')<<std::setw(2)<<i;
			
			// create new model list
			std::list<ShModelPr> submodels;

			// walk over models
			for(auto it=models_.begin();it!=models_.end();it++){
				// create copy
				ShModelPr model_copy = slzr->copy((*it).second);

				// set tree closed
				model_copy->set_tree_open(false);

				// set color from parent
				if(use_custom_color_)model_copy->set_color(color_, use_custom_color_);

				// add model to list
				submodels.push_back(model_copy);
			}

			// split models
			const ShModelGroupPr mg = ModelGroup::create(submodels);
			mg->set_name(str.str());

			// rotate and move mesh into place
			mg->add_transformation(rotation);

			// add my transformations
			for(auto it2=trans_.begin();it2!=trans_.end();it2++)
				mg->add_transformation(slzr->copy((*it2).second));

			// add to list
			model_list.push_back(mg);
		}

		// return array
		return model_list;



		// create new model list
		// std::list<ShMeshDataPr> mesh_list;

		// create a frame
		// const ShFramePr base_frame = input_path_->create_frame(MeshSettings());
		
		// combine sections on frame
		// base_frame->combine();

		// check loop
		// const bool is_loop = base_frame->is_loop();

		// get frame coordinates and orientation
		// const arma::Mat<fltp>& Rf = base_frame->get_coords(0);
		// const arma::Mat<fltp>& Lf = base_frame->get_direction(0);
		// const arma::Mat<fltp>& Nf = base_frame->get_normal(0);
		// const arma::Mat<fltp>& Df = base_frame->get_transverse(0);

		// get length coordinates of the frame
		// const arma::Row<fltp> ell = arma::join_horiz(
		// 	arma::Row<fltp>{RAT_CONST(0.0)},
		// 	arma::cumsum(cmn::Extra::vec_norm(arma::diff(Rf,1,1))));

		// // create new model list
		// std::list<ShModelPr> model_list;

		// // walk over positions
		// const arma::uword num_parts = num_parts_==0 ? num_ell_ : num_parts_;
		// for(arma::uword i=0;i<num_parts;i++){
		// 	// add to mesh list
		// 	const ShModelPathArrayPr ptharray = ModelPathArray::create(ModelGroup::split(slzr), input_path_, num_ell_, 1, num_shift_ + i, ell_velocity_);
		// 	ptharray->set_name(get_name() + "_" + std::to_string(i));
		// 	model_list.push_back(ptharray);
		// }

		// // return list of models
		// return model_list;
	}

	// mesh generation
	std::list<ShMeshDataPr> ModelPathArray::create_meshes_core(
		const std::list<arma::uword> &trace, 
		const MeshSettings &stngs)const{
		// check validity
		is_valid(true);

		// create a frame
		const ShFramePr base_frame = input_path_->create_frame(MeshSettings());
		
		// combine sections on frame
		base_frame->combine();

		// check loop
		const bool is_loop = base_frame->is_loop();

		// get frame coordinates and orientation
		const arma::Mat<fltp>& Rf = base_frame->get_coords(0);
		// const arma::Mat<fltp>& Lf = base_frame->get_direction(0);
		const arma::Mat<fltp>& Nf = base_frame->get_normal(0);
		const arma::Mat<fltp>& Df = base_frame->get_transverse(0);

		// get length coordinates of the frame
		const arma::Row<fltp> ell = arma::join_horiz(
			arma::Row<fltp>{RAT_CONST(0.0)},
			arma::cumsum(cmn::Extra::vec_norm(arma::diff(Rf,1,1))));

		// calculate number of parts
		const arma::uword num_parts = num_parts_==0 ? num_ell_ : num_parts_;

		// allocate list of meshes
		arma::field<std::list<ShMeshDataPr> > mesh_list_fld(num_parts);

		// walk over parts
		cmn::parfor(0,num_parts,stngs.use_parallel_groups,[&](
			const arma::uword i, const arma::uword /*thread*/){

			// allocate
			arma::Col<fltp>::fixed<3> Rc, Lc, Nc, Dc;

			// length coordinate
			fltp ellc = (arma::sword(i)+num_shift_)*ell.back()/num_ell_ + ell_shift_ + ell_velocity_*stngs.time;
			if(is_loop)ellc = ellc - ell.back()*std::floor(ellc/ell.back());

			// walk over xyz
			for(arma::uword j=0;j<3;j++){
				Rc(j) = cmn::Extra::interp1(ell.t(), Rf.row(j).t(), ellc, "linear", true);
				// Lc(j) = cmn::Extra::interp1(ell.t(), Lf.row(j).t(), ellc, "linear", true);
				Nc(j) = cmn::Extra::interp1(ell.t(), Nf.row(j).t(), ellc, "linear", true);
				Dc(j) = cmn::Extra::interp1(ell.t(), Df.row(j).t(), ellc, "linear", true);
			}

			// normalize orientation vectors
			// Lc.each_row()/=cmn::Extra::vec_norm(Lc);
			Nc.each_row()/=cmn::Extra::vec_norm(Nc);
			Dc.each_row()/=cmn::Extra::vec_norm(Dc);
			Lc = cmn::Extra::cross(Dc,Nc);
			Lc.each_row()/=cmn::Extra::vec_norm(Lc);

			// force orthogonal as the interpolation can cause it not to be
			Nc = cmn::Extra::cross(Lc,Dc);

			// check that all vectors are finite
			assert(Rc.is_finite()); assert(Nc.is_finite());
			assert(Dc.is_finite()); assert(Lc.is_finite());

			// create rotation
			// create matrix
			const arma::Mat<fltp>::fixed<3,3> M = arma::join_horiz(Lc,Dc,Nc);
			assert(cmn::Extra::is_pure_rotation_matrix(M));
			const ShTransRotatePr rotation = TransRotate::create(M);
			rotation->set_offset(Rc);

			// create meshes
			std::list<ShMeshDataPr> meshes = ModelGroup::create_meshes_core(trace, stngs);

			// map coordinates
			for(auto it=meshes.begin();it!=meshes.end();it++){
				// get mesh
				const ShMeshDataPr& mesh = (*it);
				
				// rotate and move mesh into place
				mesh->apply_transformation(rotation, stngs.time);
			}

			// add to mesh list
			mesh_list_fld(i).splice(mesh_list_fld(i).end(), meshes);
		});

		// combine meshes
		std::list<ShMeshDataPr> mesh_list;
		for(arma::uword i=0;i<num_parts;i++)
			mesh_list.splice(mesh_list.end(), mesh_list_fld(i));

		// return array
		return mesh_list;
	}

	// vallidity check
	bool ModelPathArray::is_valid(const bool enable_throws) const{
		if(!InputPath::is_valid(enable_throws))return false;
		if(!ModelGroup::is_valid(enable_throws))return false;
		if(num_ell_<=0){if(enable_throws){rat_throw_line("number of copies in ell must be larger than zero");} return false;};
		if(num_parts_>num_ell_){if(enable_throws){rat_throw_line("number of parts can not exceed grid size");} return false;};
		return true;
	}

	// get type
	std::string ModelPathArray::get_type(){
		return "rat::mdl::modelpatharray";
	}

	// method for serialization into json
	void ModelPathArray::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		ModelGroup::serialize(js,list);
		InputPath::serialize(js,list);

		// type
		js["type"] = get_type();

		// settings
		js["num_ell"] = static_cast<unsigned int>(num_ell_);
		js["num_parts"] = static_cast<unsigned int>(num_parts_);
		js["num_shift"] = static_cast<int>(num_shift_);
		js["ell_shift"] = ell_shift_;
		js["ell_velocity"] = ell_velocity_;
	}

	// method for deserialisation from json
	void ModelPathArray::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent objects
		ModelGroup::deserialize(js,list,factory_list,pth);
		InputPath::deserialize(js,list,factory_list,pth);

		// settings
		set_num_ell(js["num_ell"].asUInt64());
		set_num_parts(js["num_parts"].asUInt64());
		set_num_shift(js["num_shift"].asInt64());
		set_ell_shift(js["ell_shift"].ASFLTP());
		set_ell_velocity(js["ell_velocity"].ASFLTP());
	}

}}
