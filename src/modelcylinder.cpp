// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelcylinder.hh"

#include "rat/common/error.hh"

#include "crosscircle.hh"
#include "pathaxis.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	ModelCylinder::ModelCylinder(){
		set_name("Cylinder");
	}

	// default constructor
	ModelCylinder::ModelCylinder(
		const fltp radius,
		const fltp height,
		const fltp element_size) : ModelCylinder(){

		// set to self
		set_radius(radius); 
		set_height(height);
		set_element_size(element_size);
	}

	// factory
	ShModelCylinderPr ModelCylinder::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<ModelCylinder>();
	}

	// factory
	ShModelCylinderPr ModelCylinder::create(
		const fltp radius,
		const fltp height,
		const fltp element_size){
		return std::make_shared<ModelCylinder>(
			radius, height, element_size);
	}

	// get base
	ShPathPr ModelCylinder::get_input_path() const{
		// check input
		is_valid(true);

		// create axis
		const ShPathAxisPr axis = PathAxis::create('z','x',height_,cmn::Extra::null_vec(),element_size_);

		// return the racetrack
		return axis;
	}

	// get cross
	ShCrossPr ModelCylinder::get_input_cross() const{
		// check input
		is_valid(true);

		// create circular cross section
		const ShCrossCirclePr circle = CrossCircle::create(radius_, element_size_);

		// return the circle
		return circle;
	}

	// set properties
	void ModelCylinder::set_radius(const fltp radius){
		radius_ = radius;
	}

	void ModelCylinder::set_height(const fltp height){
		height_ = height;
	}

	void ModelCylinder::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}


	// get properties
	fltp ModelCylinder::get_radius()const{
		return radius_;
	}

	fltp ModelCylinder::get_height()const{
		return height_;
	}

	fltp ModelCylinder::get_element_size()const{
		return element_size_;
	}


	// check input
	bool ModelCylinder::is_valid(const bool enable_throws) const{
		if(!ModelMeshWrapper::is_valid(enable_throws))return false;
		if(radius_<=0){if(enable_throws){rat_throw_line("radius must be larger than zero");} return false;};
		if(height_<=0){if(enable_throws){rat_throw_line("height must be larger than zero");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string ModelCylinder::get_type(){
		return "rat::mdl::modelcylinder";
	}

	// method for serialization into json
	void ModelCylinder::serialize(
		Json::Value &js, cmn::SList &list) const{

		// parent
		ModelMeshWrapper::serialize(js,list);

		// set type
		js["type"] = get_type();
		
		// set properties
		js["radius"] = radius_;
		js["height"] = height_;
		js["element_size"] = element_size_;
	}

	// method for deserialisation from json
	void ModelCylinder::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		ModelMeshWrapper::deserialize(js,list,factory_list,pth);

		// get properties
		radius_ = js["radius"].ASFLTP();
		height_ = js["height"].ASFLTP();
		element_size_ = js["element_size"].ASFLTP();
	}

}}