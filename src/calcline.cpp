// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calcline.hh"

// mlfmm headers
#include "rat/mlfmm/mlfmm.hh"
#include "rat/mlfmm/multisources.hh"

// model headers
#include "crosspoint.hh"
#include "crosscircle.hh"
#include "pathgroup.hh"
#include "pathstraight.hh"
#include "meshdata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcLine::CalcLine(){
		set_name("Line"); 

		// settins
		stngs_->set_num_exp(5);
		stngs_->set_large_ilist(true);
	}

	CalcLine::CalcLine(const ShModelPr &model) : CalcLine(){
		set_model(model);
	}
	
	CalcLine::CalcLine(
		const ShModelPr &model, 
		const arma::Col<fltp>::fixed<3> &start_point, 
		const arma::Col<fltp>::fixed<3> &end_point, 
		const arma::uword num_steps) : CalcLine(model){
		set_start_point(start_point);
		set_end_point(end_point);
		set_num_steps(num_steps);
	}

	// factory methods
	ShCalcLinePr CalcLine::create(){
		return std::make_shared<CalcLine>();
	}

	ShCalcLinePr CalcLine::create(const ShModelPr &model){
		return std::make_shared<CalcLine>(model);
	}

	ShCalcLinePr CalcLine::create(
		const ShModelPr &model, 
		const arma::Col<fltp>::fixed<3> &start_point, 
		const arma::Col<fltp>::fixed<3> &end_point, 
		const arma::uword num_steps){
		return std::make_shared<CalcLine>(model, start_point, end_point, num_steps);
	}

	// get mesh enabled
	bool CalcLine::get_visibility() const{
		return visibility_;
	}

	// set mesh enabled
	void CalcLine::set_visibility(const bool visibility){
		visibility_ = visibility;
	}

	// get number of steps
	void CalcLine::set_num_steps(const arma::uword num_steps){
		num_steps_ = num_steps;
	}

	// get number of steps
	arma::uword CalcLine::get_num_steps() const{
		return num_steps_;
	}

	// set start and end point
	void CalcLine::set_start_point(const arma::Col<fltp>::fixed<3> &start_point){
		start_point_ = start_point;
	}

	void CalcLine::set_end_point(const arma::Col<fltp>::fixed<3> &end_point){
		end_point_ = end_point;
	}

	// get start and end point
	arma::Col<fltp>::fixed<3> CalcLine::get_start_point() const{
		return start_point_;
	}

	// get end point of line
	arma::Col<fltp>::fixed<3> CalcLine::get_end_point() const{
		return end_point_;
	}

	// base path
	ShPathPr CalcLine::create_base() const{
		// vector
		arma::Col<fltp>::fixed<3> L = end_point_ - start_point_;
		const fltp ell = arma::as_scalar(rat::cmn::Extra::vec_norm(L));
		L/=ell;

		// normal vector
		arma::Col<fltp>::fixed<3> N = rat::cmn::Extra::cross(
			L, arma::Col<fltp>::fixed<3>{RAT_CONST(1.0),RAT_CONST(0.0),RAT_CONST(0.0)});
		if(arma::as_scalar(rat::cmn::Extra::vec_norm(N))<RAT_CONST(1e-3)*ell)
			N = rat::cmn::Extra::cross(L, arma::Col<fltp>::fixed<3>{RAT_CONST(0.0),RAT_CONST(1.0),RAT_CONST(0.0)});
		arma::Col<fltp>::fixed<3> D = -rat::cmn::Extra::cross(L, N);

		// normalize
		N.each_row() /= cmn::Extra::vec_norm(N);
		D.each_row() /= cmn::Extra::vec_norm(D);

		// create path
		ShPathGroupPr base = PathGroup::create(start_point_, L, N);
		base->add_path(PathStraight::create(ell, ell/(num_steps_-1)));

		// return base path
		return base;
	}

	// calculate with inductance data output
	ShLineDataPr CalcLine::calculate_line(
		const fltp time, 
		const cmn::ShLogPr &lg, 
		const ShSolverCachePr& cache){

		// when calculating the settings must be valid
		is_valid(true);

		// get date and time
		auto timdata = std::time(nullptr);
		auto localtime = *std::localtime(&timdata);
		std::ostringstream date_stream,time_stream;
		date_stream << std::put_time(&localtime, "%d-%m-%Y");
		time_stream << std::put_time(&localtime, "%H:%M:%S");
		const std::string date_str = date_stream.str();
		const std::string time_str = time_stream.str();

		// general info
		lg->msg("%s=== Starting Line Calculation ===%s\n",KGRN,KNRM);
		lg->msg(2,"%sGENERAL INFO%s\n",KGRN,KNRM);
		lg->msg("date: %s%s%s %s(dd-mm-yyyy)%s\n",KYEL,date_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("time: %s%s%s %s(hh:mm:ss)%s\n",KYEL,time_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("model name: %s%s%s\n",KYEL,model_->get_name().c_str(),KNRM);
		lg->msg("path name: %s%s%s\n",KYEL,get_name().c_str(),KNRM);
		lg->msg(-2,"\n");

		// create base path
		ShPathPr base = create_base();

		// create frame
		ShLineDataPr line = LineData::create(base->create_frame(MeshSettings()));
		line->set_field_type("ABHM",{3,3,3,3});
		line->set_time(time);

		// check input model
		if(model_==NULL)rat_throw_line("model not set");
		model_->is_valid(true);

		// settings for mesh creation
		MeshSettings mesh_settings;
		mesh_settings.time = time;
		mesh_settings.low_poly = false;
		mesh_settings.combine_sections = true;

		// create meshes
		const std::list<ShMeshDataPr> meshes = model_->create_meshes({},mesh_settings);

		// set circuit currents
		apply_circuit_currents(time, meshes);
		
		// show meshes
		lg->msg(2,"%sGEOMETRY SETUP%s\n",KGRN,KNRM);
		MeshData::display(lg, meshes);
		lg->msg(-2,"\n"); 

		// run calculation
		calculate_field(meshes,line,time,lg,cache);

		// // create multilevel fast multipole method object
		// fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(stngs_);

		// // set background field
		// if(bg_!=NULL)mlfmm->set_background(bg_->create_fmm_background(time));
		
		// // combine sources and targets
		// fmm::ShMultiSourcesPr src = fmm::MultiSources::create();

		// // set meshes as sources for MLFMM
		// for(auto it=meshes.begin();it!=meshes.end();it++)src->add_sources((*it)->create_sources());

		// // add sources and targets to the calculation
		// mlfmm->set_sources(src);
		// mlfmm->set_targets(line);

		// // setup multipole method
		// mlfmm->setup(lg);

		// // run multipole method
		// mlfmm->calculate(lg);

		// return the line data
		return line;
	}

	// generalized calculation
	std::list<ShDataPr> CalcLine::calculate(
		const fltp time, 
		const cmn::ShLogPr &lg, 
		const ShSolverCachePr& cache){
		return {calculate_line(time,lg,cache)};
	}

	// creation of calculation data objects
	std::list<ShMeshDataPr> CalcLine::create_meshes(
		const std::list<arma::uword> &trace, 
		const MeshSettings &stngs)const{

		// trace
		if(!trace.empty())rat_throw_line("trace must be empty for leaf");

		// mesh enabled
		if(!visibility_ || !enable_)return{};
		if(!is_valid(stngs.enable_throws))return{};

		// create base path
		ShPathPr base = create_base();

		// allocate cross section
		ShCrossPr crss;

		// point cross section
		if(stngs.visual_radius==RAT_CONST(0.0))crss = CrossPoint::create(
			RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(1e-6),RAT_CONST(1e-6));

		// circular cross section
		else crss = CrossCircle::create(stngs.visual_radius,stngs.visual_radius/2);
		

		// create frame
		ShFramePr frame = base->create_frame(stngs);

		// create 2d area mesh
		ShAreaPr area = crss->create_area(stngs);

		// drop nodes
		if(stngs.low_poly)frame->simplify(stngs.visual_tolerance, crss->get_bounding());

		// combine
		if(stngs.combine_sections)frame->combine();

		// mesh data
		ShMeshDataPr mesh_data = MeshData::create(frame, area);

		// set temperature
		mesh_data->set_operating_temperature(0);

		// set time
		mesh_data->set_time(stngs.time);

		// set name (is appended by models later)
		mesh_data->append_name(myname_);

		// flag this mesh as calculation
		mesh_data->set_calc_mesh();
		
		// mesh data object
		return {mesh_data};
	}

	// is valid
	bool CalcLine::is_valid(const bool enable_throws) const{
		if(!CalcLeaf::is_valid(enable_throws))return false;
		if(num_steps_<=0){if(enable_throws){rat_throw_line("number of steps must be positive");} return false;};
		if(arma::as_scalar(rat::cmn::Extra::vec_norm(end_point_ - start_point_)<1e-12)){if(enable_throws){rat_throw_line("start and end point can not coincide");} return false;};
		return true;
	}


	// serialization
	std::string CalcLine::get_type(){
		return "rat::mdl::calcline";
	}

	void CalcLine::serialize(Json::Value &js, cmn::SList &list) const{
		CalcLeaf::serialize(js,list);
		js["type"] = get_type();
		js["visibility"] = visibility_;
		js["num_steps"] = static_cast<int>(num_steps_);
		js["start_point_x"] = start_point_(0); 
		js["start_point_y"] = start_point_(1); 
		js["start_point_z"] = start_point_(2);
		js["end_point_x"] = end_point_(0); 
		js["end_point_y"] = end_point_(1); 
		js["end_point_z"] = end_point_(2);
	}
	
	void CalcLine::deserialize(const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		CalcLeaf::deserialize(js,list,factory_list,pth);
		visibility_ = js["visibility"].asBool();
		num_steps_ = js["num_steps"].asUInt64();
		start_point_(0) = js["start_point_x"].ASFLTP(); 
		start_point_(1) = js["start_point_y"].ASFLTP(); 
		start_point_(2) = js["start_point_z"].ASFLTP();
		end_point_(0) = js["end_point_x"].ASFLTP(); 
		end_point_(1) = js["end_point_y"].ASFLTP(); 
		end_point_(2) = js["end_point_z"].ASFLTP();
	}

}}