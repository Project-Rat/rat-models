// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "background.hh"

#include "rat/common/node.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	Background::Background(){
		set_name("Background");
		bg_drive_.set_size(3);
		set_bg_drive('x', DriveDC::create(RAT_CONST(0.0)));
		set_bg_drive('y', DriveDC::create(RAT_CONST(0.0)));
		set_bg_drive('z', DriveDC::create(RAT_CONST(0.0)));
	}
	
	// constructor
	Background::Background(
		const ShDrivePr &drivex, 
		const ShDrivePr &drivey, 
		const ShDrivePr &drivez) : Background(){
		set_bg_drive({drivex,drivey,drivez});
	}

	Background::Background(
		const arma::Col<fltp>::fixed<3> &bg_field) : Background(){
		set_bg_field('x', bg_field(0));
		set_bg_field('y', bg_field(1));
		set_bg_field('z', bg_field(2));
	}


	// factory
	ShBackgroundPr Background::create(){
		return std::make_shared<Background>();
	}
	
	// factory
	ShBackgroundPr Background::create(
		const ShDrivePr &drivex, 
		const ShDrivePr &drivey, 
		const ShDrivePr &drivez){
		return std::make_shared<Background>(drivex,drivey,drivez);
	}

	// factory
	ShBackgroundPr Background::create(
		const arma::Col<fltp>::fixed<3> &bg_field){
		return std::make_shared<Background>(bg_field);
	}


	// set drive
	void Background::set_bg_drive(
		const ShDrivePr &drivex, 
		const ShDrivePr &drivey, 
		const ShDrivePr &drivez){
		set_bg_drive({drivex,drivey,drivez});
	}


	// set drive
	void Background::set_bg_drive(const arma::field<ShDrivePr> &drive){
		if(drive.n_elem!=3)rat_throw_line("supplied drive must have dimension of three");
		bg_drive_ = drive;
	}

	// set drive for background field
	void Background::set_bg_drive(const char dir, const rat::mdl::ShDrivePr &drive){
		if(drive==NULL)rat_throw_line("drive points to NULL");
		bg_drive_(rat::cmn::Extra::xyz2idx(dir)) = drive;
	}

	// set background field
	void Background::set_bg_field(const char dir, const rat::fltp strength){
		bg_drive_(rat::cmn::Extra::xyz2idx(dir)) = DriveDC::create(strength);
	}

	// get background field
	arma::Col<fltp>::fixed<3> Background::get_background_field(const fltp time)const{
		return {get_bg_drive('x')->get_scaling(time),
			get_bg_drive('y')->get_scaling(time),
			get_bg_drive('z')->get_scaling(time)};
	}

	// get background field
	arma::Col<fltp>::fixed<3> Background::get_dbackground_field(const fltp time)const{
		return {get_bg_drive('x')->get_scaling(time,RAT_CONST(0.0),1),
			get_bg_drive('y')->get_scaling(time,RAT_CONST(0.0),1),
			get_bg_drive('z')->get_scaling(time,RAT_CONST(0.0),1)};
	}

	// get background field
	fltp Background::get_background_field(const char dir, const fltp time)const{
		return get_bg_drive(dir)->get_scaling(time);
	}

	// get background field
	fltp Background::get_dbackground_field(const char dir, const fltp time)const{
		return get_bg_drive(dir)->get_scaling(time,RAT_CONST(0.0),1);
	}

	// set drive for background field
	ShDrivePr Background::get_bg_drive(const char dir)const{
		return bg_drive_(rat::cmn::Extra::xyz2idx(dir));
	}

	// set drive for background field
	arma::field<ShDrivePr> Background::get_bg_drive()const{
		return bg_drive_;
	}

	// check if background field is static 
	// (this is the case if all drives are DC Drives)
	bool Background::is_static()const{
		for(arma::uword i=0;i<bg_drive_.n_elem;i++)
			if(!std::dynamic_pointer_cast<DriveDC>(bg_drive_(i)))return false;
		return true;
	}

	// get background input for MLFMM
	fmm::ShBackgroundPr Background::create_fmm_background(const fltp time)const{
		// create background field 
		fmm::ShBackgroundPr bg = fmm::Background::create();
		bg->set_magnetic(get_background_field(time)/arma::Datum<rat::fltp>::mu_0);

		// return the background object
		return bg;
	}

	// get background input for MLFMM
	fmm::ShBackgroundPr Background::create_fmm_dbackground(const fltp time)const{
		// create background field 
		fmm::ShBackgroundPr bg = fmm::Background::create();
		bg->set_magnetic(get_dbackground_field(time)/arma::Datum<rat::fltp>::mu_0);

		// return the background object
		return bg;
	}
	
	// serialization
	std::string Background::get_type(){
		return "rat::mdl::background";
	}

	void Background::serialize(
		Json::Value &js, 
		cmn::SList &list) const{
		// parent
		Node::serialize(js,list);
			
		// type
		js["type"] = get_type();

		// serialize drives
		js["bg_drive_x"] = cmn::Node::serialize_node(get_bg_drive('x'), list);
		js["bg_drive_y"] = cmn::Node::serialize_node(get_bg_drive('y'), list);
		js["bg_drive_z"] = cmn::Node::serialize_node(get_bg_drive('z'), list);
	}
	
	void Background::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent
		Node::deserialize(js,list,factory_list,pth);

		// drives
		set_bg_drive('x', cmn::Node::deserialize_node<Drive>(js["bg_drive_x"], list, factory_list, pth));
		set_bg_drive('y', cmn::Node::deserialize_node<Drive>(js["bg_drive_y"], list, factory_list, pth));
		set_bg_drive('z', cmn::Node::deserialize_node<Drive>(js["bg_drive_z"], list, factory_list, pth));
	}

}}