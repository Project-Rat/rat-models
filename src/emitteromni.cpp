// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "emitteromni.hh"

// rat-common headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	EmitterOmni::EmitterOmni(){
		set_name("Omni Emitter");

		// protons are default
		set_proton();

		// set beam energy
		is_momentum_ = false;
		beam_energy_ = rest_mass_ + 0.1;
		beam_momentum_ = 0.1;
	}

	// constructor
	EmitterOmni::EmitterOmni(
		const fltp rest_mass, 
		const fltp beam_energy, 
		const fltp charge, 
		const arma::uword num_particles, 
		const arma::Col<fltp>::fixed<3> &R, 
		const arma::uword lifetime) : EmitterOmni(){
		set_rest_mass(rest_mass);
		set_beam_energy(beam_energy);
		set_charge(charge);
		set_num_particles(num_particles);
		set_spawn_coord(R);
		set_lifetime(lifetime);
	}

	// factory
	ShEmitterOmniPr EmitterOmni::create(){
		return std::make_shared<EmitterOmni>();
	}
	
	// factory
	ShEmitterOmniPr EmitterOmni::create(
		const fltp rest_mass, 
		const fltp beam_energy, 
		const fltp charge, 
		const arma::uword num_particles, 
		const arma::Col<fltp>::fixed<3> &R, 
		const arma::uword lifetime){
		return std::make_shared<EmitterOmni>(
			rest_mass, beam_energy, charge, 
			num_particles, R, lifetime);
	}

	// set protons
	void EmitterOmni::set_proton(){
		rest_mass_ = 0.938; // [GeV/C^2] 
		charge_ = 1; // elementary charge
	}

	// set electrons
	void EmitterOmni::set_electron(){
		rest_mass_ = 0.51099895000e-3; // [GeV/C^2] 
		charge_ = -1; // elementary charge
	}


	// setters
	void EmitterOmni::set_seed(const unsigned int seed){
		seed_ = seed;
	}

	void EmitterOmni::set_is_momentum(const bool is_momentum){
		is_momentum_ = is_momentum;
	}

	void EmitterOmni::set_rest_mass(const fltp rest_mass){
		rest_mass_ = rest_mass;
	}

	void EmitterOmni::set_charge(const fltp charge){
		charge_ = charge;
	}

	void EmitterOmni::set_beam_energy(const fltp beam_energy){
		beam_energy_ = beam_energy;
	}

	void EmitterOmni::set_sigma_energy(const fltp sigma_beam_energy){
		sigma_beam_energy_ = sigma_beam_energy;
	}

	void EmitterOmni::set_beam_momentum(const fltp beam_momentum){
		beam_momentum_ = beam_momentum;
	}

	void EmitterOmni::set_sigma_momentum(const fltp sigma_momentum){
		sigma_momentum_ = sigma_momentum;
	}

	void EmitterOmni::set_lifetime(const arma::uword lifetime){
		lifetime_ = lifetime; 
	}

	void EmitterOmni::set_num_particles(const arma::uword num_particles){
		num_particles_ = num_particles;
	}

	void EmitterOmni::set_spawn_coord(const arma::Col<fltp>::fixed<3> &R){
		R_ = R;
	}

	void EmitterOmni::set_start_idx(const arma::uword start_idx){
		start_idx_ = start_idx;
	}

	void EmitterOmni::set_spawn_radius(const fltp spawn_radius){
		spawn_radius_ = spawn_radius;
	}



	// getters
	arma::uword EmitterOmni::get_num_particles() const{
		return num_particles_;
	}

	bool EmitterOmni::get_is_momentum()const{
		return is_momentum_;
	}

	fltp EmitterOmni::get_rest_mass() const{
		return rest_mass_;
	}

	fltp EmitterOmni::get_charge()const{
		return charge_;
	}

	fltp EmitterOmni::get_beam_energy()const{
		return beam_energy_;
	}

	fltp EmitterOmni::get_sigma_energy()const{
		return sigma_beam_energy_;
	}

	arma::uword EmitterOmni::get_lifetime()const{
		return lifetime_;
	}

	arma::uword EmitterOmni::get_start_idx()const{
		return start_idx_;
	}

	arma::Col<fltp>::fixed<3> EmitterOmni::get_spawn_coord()const{
		return R_;
	}

	// set seed for random number generator
	unsigned int EmitterOmni::get_seed()const{
		return seed_;
	}

	// get the spawn radius
	fltp EmitterOmni::get_spawn_radius()const{
		return spawn_radius_;
	}

	fltp EmitterOmni::get_beam_momentum()const{
		return beam_momentum_;
	}

	fltp EmitterOmni::get_sigma_momentum()const{
		return sigma_momentum_;
	}

	// particle creation
	arma::field<Particle> EmitterOmni::spawn_particles(const fltp time) const{
		// error check
		is_valid(true);

		// create a random number generator
		// we use this instead of armadillo generator to ensure each thread
		// has a reproduceable mesh without race conditions 
		std::mt19937 generator(seed_); // Standard mersenne_twister_engine seeded with seed
		std::uniform_real_distribution<fltp> distribution(0.0, 1.0);

		// create random beam energies
		arma::Row<fltp> beam_energy = is_momentum_ ? 
			arma::sqrt(arma::square(beam_momentum_ + sigma_momentum_*arma::randn<arma::Row<fltp> >(num_particles_)) + rest_mass_*rest_mass_).eval() : 
			(beam_energy_ + sigma_beam_energy_*arma::randn<arma::Row<fltp> >(num_particles_)).eval();

		// clamp energies
		beam_energy.clamp(rest_mass_,arma::Datum<fltp>::inf);

		// velocity
		const arma::Row<fltp> gamma = beam_energy/rest_mass_;
		const arma::Row<fltp> velocity = arma::sqrt(1.0 - 1.0/(gamma%gamma));

		// spherical coordinates uniform random distribution
		// the random distribution is chosen such that the surface density
		// on the sphere is uniform
		const arma::Row<fltp> theta = 2*arma::Datum<fltp>::pi*arma::Row<fltp>(num_particles_,arma::fill::randu);
		const arma::Row<fltp> phi = arma::acos(2*arma::Row<fltp>(num_particles_,arma::fill::randu)-1.0);
		
		// convert to cartesian coordinates
		arma::Mat<fltp> Rs = arma::join_vert(
			arma::cos(theta)%arma::sin(phi),
			arma::sin(theta)%arma::sin(phi),
			arma::cos(phi));

		// allocate spawn coordinates
		arma::Mat<fltp> R(3,num_particles_,arma::fill::zeros);

		// generate random position within a sphere
		// this should give a uniform distribution of points
		if(spawn_radius_>0){
			for(arma::uword i=0;i<num_particles_;i++){
				for(;;){
					// create random number for rejection method
					for(arma::uword j=0;j<R.n_rows;j++)R(j,i) = spawn_radius_*(2*distribution(generator) - 1);
					if(arma::as_scalar(cmn::Extra::vec_norm(R.col(i)))<spawn_radius_)break;
				}
			}
		}

		// offset
		R.each_col() += R_;

		// apply transformations 
		for(auto it=trans_.begin();it!=trans_.end();it++){
			(*it).second->apply_vectors(R,Rs,'L',time);
			(*it).second->apply_coords(R,time);
		}

		// allocate list of particles
		arma::field<Particle> particles(num_particles_);

		// create all particles
		for(arma::uword i=0;i<num_particles_;i++){
			// set maximum number of steps
			particles(i).set_num_steps(lifetime_);

			// set start coordinate and velocity
			particles(i).set_startcoord(R.col(i),Rs.col(i)*velocity(i));

			// set start index
			particles(i).set_start_index(start_idx_);

			// set mass
			particles(i).set_rest_mass(rest_mass_);
			particles(i).set_charge(charge_);

			// setup internal storage
			particles(i).setup();
			if(start_idx_==0)particles(i).terminate_start();
			if(start_idx_==lifetime_-1)particles(i).terminate_end();
		}

		// return list of particles
		return particles;
	}

	// create specific mesh
	std::list<ShMeshDataPr> EmitterOmni::create_meshes(
		const std::list<arma::uword> &trace, 
		const MeshSettings &stngs) const{
		
		// check if valid
		if(!trace.empty())rat_throw_line("max trace depth reached but trace not empty");
		if(!is_valid(stngs.enable_throws))return{};

		// draw ellipse
		const arma::uword num_elements = 127;
		const arma::uword num_nodes = num_elements+1;
		const arma::Row<fltp> theta = arma::linspace<arma::Row<fltp> >(0,2*arma::Datum<fltp>::pi,num_nodes);
		const arma::Row<fltp> u = std::max(spawn_radius_, RAT_CONST(0.05)*stngs.typical_scale)*arma::cos(theta);
		const arma::Row<fltp> v = std::max(spawn_radius_, RAT_CONST(0.05)*stngs.typical_scale)*arma::sin(theta);
		const arma::Row<fltp> w(num_nodes, arma::fill::zeros);

		// combine and flip
		arma::Mat<fltp> Rc = arma::join_vert(u,v,w);
		Rc = arma::join_horiz(Rc, 
			arma::join_vert(Rc.row(0), Rc.row(2), Rc.row(1)), 
			arma::join_vert(Rc.row(2), Rc.row(0), Rc.row(1))); 

		// apply transformations
		for(auto it=trans_.begin();it!=trans_.end();it++)
			(*it).second->apply_coords(Rc, stngs.time);
		
		// create coords
		arma::Mat<arma::uword> n = arma::join_vert(
			arma::regspace<arma::Row<arma::uword> >(0,num_nodes-2),
			arma::regspace<arma::Row<arma::uword> >(1,num_nodes-1));
		n = arma::join_horiz(n,n+num_nodes,n+2*num_nodes);

		// create mesh
		ShMeshDataPr mesh_data = MeshData::create(Rc, n, n);

		// draw mesh
		// set temperature
		mesh_data->set_operating_temperature(0);

		// set time
		mesh_data->set_time(stngs.time);

		// set name (is appended by models later)
		mesh_data->append_name(myname_);

		// flag this mesh as calculation
		mesh_data->set_calc_mesh();
		
		// mesh data object
		return {mesh_data};
	}


	// validity check
	bool EmitterOmni::is_valid(const bool enable_throws) const{
		if(rest_mass_<0){if(enable_throws){rat_throw_line("rest mass must be larger than zero");} return false;};
		if(is_momentum_ ? beam_energy_<=0 : beam_energy_<=rest_mass_){if(enable_throws){rat_throw_line("beam energy must exceed rest mass");} return false;};
		if(lifetime_<=0){if(enable_throws){rat_throw_line("lifetime must be larger than zero");} return false;};
		if(start_idx_>=lifetime_){if(enable_throws){rat_throw_line("start index must be less than lifetime");} return false;};
		return true;
	}

	// get type
	std::string EmitterOmni::get_type(){
		return "rat::mdl::emitteromni";
	}

	// method for serialization into json
	void EmitterOmni::serialize(
		Json::Value &js, cmn::SList &list) const{
		// parent
		Emitter::serialize(js,list);
		
		// properties
		js["type"] = get_type();

		// rng
		js["seed"] = static_cast<unsigned int>(seed_);

		// beam parameters
		js["num_particles"] = static_cast<unsigned int>(num_particles_);
		js["is_momentum"] = is_momentum_;
		js["beam_energy"] = beam_energy_;
		js["sigma_beam_energy"] = sigma_beam_energy_;
		js["beam_momentum"] = beam_momentum_;
		js["sigma_momentum"] = sigma_momentum_;
		js["rest_mass"] = rest_mass_;
		js["charge"] = charge_;

		// position orientation
		js["Rx"] = R_(0);
		js["Ry"] = R_(1);
		js["Rz"] = R_(2);

		// spawn radius
		js["spawn_radius"] = spawn_radius_;

		// tracking
		js["lifetime"] = static_cast<unsigned int>(lifetime_);
		js["start_idx"] = static_cast<unsigned int>(start_idx_);
	}

	// method for deserialisation from json
	void EmitterOmni::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent
		Emitter::deserialize(js,list,factory_list,pth);

		// rng
		seed_ = js["seed"].asUInt64();

		// beam parameters
		num_particles_ = js["num_particles"].asUInt64();
		rest_mass_ = js["rest_mass"].ASFLTP();
		is_momentum_ = js["is_momentum"].asBool();
		beam_energy_ = js["beam_energy"].ASFLTP();
		sigma_beam_energy_ = js["sigma_beam_energy"].ASFLTP();
		if(js.isMember("beam_momentum")){
			beam_momentum_ = js["beam_momentum"].ASFLTP();
			sigma_momentum_ = js["sigma_momentum"].ASFLTP();
		}else{
			if(is_momentum_){
				beam_momentum_ = beam_energy_;
				sigma_momentum_ = sigma_beam_energy_;
			}
		}
		charge_ = js["charge"].ASFLTP();

		// position orientation
		R_(0) = js["Rx"].ASFLTP();
		R_(1) = js["Ry"].ASFLTP();
		R_(2) = js["Rz"].ASFLTP();

		// tracking
		lifetime_ = js["lifetime"].asUInt64();
		start_idx_ = js["start_idx"].asUInt64();

		// spawn radius
		set_spawn_radius(js["spawn_radius"].ASFLTP());
	}

}}