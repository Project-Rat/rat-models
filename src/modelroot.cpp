// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelroot.hh"


// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ModelRoot::ModelRoot(){
		model_tree_ = ModelGroup::create();
		calc_tree_ =CalcGroup::create();
	}
	
	// constructor
	ModelRoot::ModelRoot(
		const ShModelGroupPr &model_tree, 
		const ShCalcGroupPr &calc_tree){
		set_model_tree(model_tree);
		set_calc_tree(calc_tree);
	}

	// factory
	ShModelRootPr ModelRoot::create(){
		return std::make_shared<ModelRoot>();
	}
	
	// factory
	ShModelRootPr ModelRoot::create(
		const ShModelGroupPr &model_tree, 
		const ShCalcGroupPr &calc_tree){
		return std::make_shared<ModelRoot>(model_tree, calc_tree);
	}

	// setters
	void ModelRoot::set_t1(const fltp t1){
		t1_ = t1;
	}

	void ModelRoot::set_t2(const fltp t2){
		t2_ = t2;
	}

	// getters
	fltp ModelRoot::get_t1() const{
		return t1_;
	}

	fltp ModelRoot::get_t2() const{
		return t2_;
	}


	// set calculation tree
	void ModelRoot::set_calc_tree(const ShCalcGroupPr &calc_tree){
		calc_tree_ = calc_tree;
	}

	// get calculation tree
	ShCalcGroupPr ModelRoot::get_calc_tree() const{
		return calc_tree_;
	}

	// set model tree
	void ModelRoot::set_model_tree(const ShModelGroupPr &model_tree){
		model_tree_ = model_tree;
	}

	// get model tree
	ShModelGroupPr ModelRoot::get_model_tree() const{
		return model_tree_;
	}

	// create a mesh of the stored coils
	std::list<ShMeshDataPr> ModelRoot::create_meshes(
		const std::list<arma::uword> &trace, 
		const MeshSettings &stngs)const{

		// check enabled
		if(!get_enable())return{};

		// get trace index
		arma::uword myindex = 0; 
		std::list<arma::uword> next_trace = trace;
		if(!trace.empty()){
			myindex = trace.front();
			next_trace.pop_front();
		}

		// allocate output mesh list
		std::list<ShMeshDataPr> meshes;

		// create meshes in parallel
		std::future<std::list<ShMeshDataPr> > model_tree_future;
		std::future<std::list<ShMeshDataPr> > calc_tree_future;
		if(trace.empty() || myindex==1){
			model_tree_future = std::async(std::launch::async,[this,&stngs,&next_trace](){
				return model_tree_->create_meshes(next_trace,stngs);
			});
		}
		if(trace.empty() || myindex==2){
			calc_tree_future = std::async(std::launch::async,[this,&stngs,&next_trace](){
				return calc_tree_->create_meshes(next_trace,stngs);
			});
		}

		// create a list of meshes from the model
		if(trace.empty() || myindex==1){
			std::list<ShMeshDataPr> meshes1 = model_tree_future.get();
			for(auto it=meshes1.begin();it!=meshes1.end();it++){
				const ShMeshDataPr& mesh_data = (*it);

				// append trace
				mesh_data->append_trace_id(1);
				
				// override color if enabled
				if(use_custom_color_ && !mesh_data->get_use_custom_color()){
					mesh_data->set_use_custom_color(use_custom_color_);
					mesh_data->set_color(color_);
				}

				// set conductor
				if(conductor_!=NULL && mesh_data->get_material()==NULL)
					mesh_data->set_material(conductor_);
			}
			meshes.splice(meshes.end(), meshes1);
		}

		// create a list of meshes from the calculation tree
		if(trace.empty() || myindex==2){
			std::list<ShMeshDataPr> meshes2 = calc_tree_future.get();
			for(auto it=meshes2.begin();it!=meshes2.end();it++)(*it)->append_trace_id(2);
			meshes.splice(meshes.end(), meshes2);
		}

		// combine into single coilmesh and return
		return meshes;
	}

	// check validity
	bool ModelRoot::is_valid(const bool enable_throws) const{
		return model_tree_->is_valid(enable_throws) && calc_tree_->is_valid(enable_throws);
	}

	// serialization
	std::string ModelRoot::get_type(){
		return "rat::mdl::modelroot";
	}

	// serialization
	void ModelRoot::serialize(Json::Value &js, cmn::SList &list) const{
		Model::serialize(js,list);
		js["type"] = get_type();

		// serialize tree
		js["model_tree"] = cmn::Node::serialize_node(model_tree_, list);
		js["calc_tree"] = cmn::Node::serialize_node(calc_tree_, list);
		js["t1"] = t1_; js["t2"] = t2_;
	}

	// deserialization
	void ModelRoot::deserialize(const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		Model::deserialize(js,list,factory_list,pth);
		set_model_tree(cmn::Node::deserialize_node<ModelGroup>(js["model_tree"], list, factory_list, pth));
		set_calc_tree(cmn::Node::deserialize_node<CalcGroup>(js["calc_tree"], list, factory_list, pth));
		if(js.isMember("t1"))set_t1(js["t1"].ASFLTP());
		if(js.isMember("t2"))set_t2(js["t2"].ASFLTP());
		
		// resynchronise models over all calculations
		// this is normally not needed, but some models 
		// got desynced due to bugs in the GUI in v2.017.6
		calc_tree_->set_model(model_tree_);
	}

}}