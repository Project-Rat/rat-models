/* Raccoon - An Electro-Magnetic and Thermal network 
solver accelerated with the Multi-Level Fast Multipole Method
Copyright (C) 2020  Jeroen van Nugteren*/

// include header
#include "serializer.hh"

// serializable objects
#include "rat/mlfmm/settings.hh"
#include "crosscircle.hh"
#include "crossdmsh.hh"
#include "crossrectangle.hh"
#include "crosspoint.hh"
#include "crossline.hh"
#include "crossline2.hh"
#include "crosspath.hh"
#include "crossgroup.hh"
#include "modelcoil.hh"
#include "modelmesh.hh"
#include "modeltoroid.hh"
#include "modelgroup.hh"
#include "modelarray.hh"
#include "modelbar.hh"
#include "modelimport.hh"
#include "modelmgnsphere.hh"
#include "modelplasma.hh"
#include "pathaxis.hh"
#include "pathcable.hh"
#include "pathconnect.hh"
#include "pathconnect2.hh"
#include "pathdash.hh"
#include "pathmap.hh"
#include "pathmerge.hh"
#include "pathcircle.hh"
#include "pathellipse.hh"
#include "pathclover.hh"
#include "pathrectangle.hh"
#include "pathtrapezoid.hh"
#include "pathflared.hh"
#include "pathdshape.hh"
#include "patharc.hh"
#include "pathellipticalarc.hh"
#include "pathcct.hh"
#include "pathcctcustom.hh"
#include "pathoffset.hh"
#include "pathoffsetdrive.hh"
#include "pathsub.hh"
#include "pathplasma.hh"
#include "pathgroup.hh"
#include "pathstraight.hh"
#include "pathpolygon.hh"
#include "pathspiral.hh"
#include "pathpoint.hh"
#include "pathcurl.hh"
#include "pathccttable.hh"
#include "pathunfold.hh"
#include "pathstaircable.hh"
#include "pathrail.hh"
#include "pathfile.hh"
#include "pathfile2.hh"
#include "pathxyz.hh"
#include "pathlemniscate.hh"
#include "transbend.hh"
#include "transreflect.hh"
#include "transflip.hh"
#include "transreverse.hh"
#include "transrotate.hh"
#include "transtranslate.hh"
#include "driveac.hh"
#include "drivedc.hh"
#include "drivetrapz.hh"
#include "drivestep.hh"
#include "driveinterp.hh"
#include "drivelinear.hh"
#include "drivercct.hh"
#include "drivescurve.hh"
#include "driverandom.hh"
#include "calcinductance.hh"
#include "calcline.hh"
#include "calclength.hh"
#include "calcpath.hh"
#include "calcgroup.hh"
#include "calcplane.hh"
#include "calcgrid.hh"
#include "calcpolargrid.hh"
#include "calcpoints.hh"
#include "calcmesh.hh"
#include "calcharmonics.hh"
#include "calcspharm.hh"
#include "calctracks.hh"
#include "calcfreecad.hh"
#include "emitterbeam.hh"
#include "emittercoordscan.hh"
#include "emitteromni.hh"
#include "emitternewton.hh"
#include "modelsolenoid.hh"
#include "modelracetrack.hh"
#include "modelrectangle.hh"
#include "modeltrapezoid.hh"
#include "modelcct.hh"
#include "modeldshape.hh"
#include "modelstick.hh"
#include "modelmirror.hh"
#include "modelroot.hh"
#include "modelsphere.hh"
#include "modelflared.hh"
#include "modelclover.hh"
#include "modelpoint.hh"
#include "modelpatharray.hh"
#include "modeledges.hh"
#include "modelclip.hh"
#include "area.hh"
#include "frame.hh"
#include "driveptrain.hh"
#include "pathfrenet.hh"
#include "circuit.hh"
#include "protectioncircuit.hh"
#include "background.hh"
#include "modelbarcube.hh"
#include "modelring.hh"
#include "modelline.hh"
#include "modelcube.hh"
#include "modelcylinder.hh"
#include "modeldoughnut.hh"
#include "pathattach.hh"
#include "driveinterpbcr.hh"
#include "drivefourier.hh"
#include "pathbezier.hh"
#include "pathsuperellipse.hh"
#include "patharray.hh"
#include "mesh.hh"
#include "pathprofile.hh"
#include "pathcostheta.hh"
#include "crosswedge.hh"
#include "calcquench0d.hh"
// #include "patheq.hh"
#ifdef ENABLE_NL_SOLVER
#include "modelnl.hh"
#include "modelnlsphere.hh"
#include "modelnlfile.hh"
#include "hbcurvetable.hh"
#include "hbcurvefile.hh"
#include "hbcurvevlv.hh"
#endif

// code specific to Raccoon
namespace rat{namespace mdl{
	
	// constructor
	Serializer::Serializer(){
		register_constructors();
	}

	// constructor
	Serializer::Serializer(const ShModelPr &model){
		flatten_tree(model); register_constructors();
	}

	// constructor
	Serializer::Serializer(const boost::filesystem::path &fname){
		import_json(fname); register_constructors();
	}

	// factory
	ShSerializerPr Serializer::create(){
		return std::make_shared<Serializer>();
	}

	// factory
	ShSerializerPr Serializer::create(const ShModelPr &model){
		return std::make_shared<Serializer>(model);
	}

	// factory
	ShSerializerPr Serializer::create(const boost::filesystem::path &fname){
		return std::make_shared<Serializer>(fname);
	}

	// registry
	void Serializer::register_constructors(){
		// factories from MLFMM
		register_factory<fmm::Settings>();

		// cross sections
		register_factory<CrossCircle>();
		register_factory<CrossDMsh>();
		register_factory<CrossRectangle>();
		register_factory<CrossWedge>();
		register_factory<CrossPoint>();
		register_factory<CrossLine>();
		register_factory<CrossLine2>();
		register_factory<CrossPath>();
		register_factory<CrossGroup>();

		// model nodes
		register_factory<ModelArray>();
		register_factory<ModelPathArray>();
		register_factory<ModelCoil>();
		register_factory<ModelMesh>();
		register_factory<ModelToroid>();
		register_factory<ModelGroup>();
		register_factory<ModelSolenoid>();
		register_factory<ModelRacetrack>();
		register_factory<ModelRectangle>();
		register_factory<ModelTrapezoid>();
		register_factory<ModelRoot>();
		register_factory<ModelBar>();
		register_factory<ModelMirror>();
		register_factory<ModelStick>();
		register_factory<ModelImport>();
		register_factory<ModelSphere>();
		register_factory<ModelMgnSphere>();
		register_factory<ModelDShape>();
		register_factory<ModelFlared>();
		register_factory<ModelClover>();
		register_factory<ModelCCTBase>();
		register_factory<ModelCCT>();
		register_factory<ModelCCTAmpl>();
		register_factory<ModelBarCube>();
		register_factory<ModelRing>();
		register_factory<ModelLine>();
		register_factory<ModelPoint>();
		register_factory<ModelCube>();
		register_factory<ModelCylinder>();
		register_factory<ModelDoughnut>();
		register_factory<ModelPlasma>();
		register_factory<ModelClip>();
		register_factory<ModelEdges>();


		// path nodes
		register_factory<PathAxis>();
		register_factory<PathArray>();
		register_factory<PathCable>();
		register_factory<PathCircle>();
		register_factory<PathEllipse>();
		register_factory<PathClover>();
		register_factory<PathDash>();
		register_factory<PathRectangle>();
		register_factory<PathTrapezoid>();
		register_factory<PathFlared>();
		register_factory<PathDShape>();
		register_factory<PathArc>();
		register_factory<PathEllipticalArc>();
		register_factory<PathCCT>();
		register_factory<PathCCTCustom>();
		register_factory<PathCCTTable>();
		register_factory<CCTHarmonicDrive>(); 
		register_factory<PathOffset>(); 
		register_factory<PathRail>(); 
		register_factory<PathOffsetDrive>(); 
		register_factory<PathSub>(); 
		register_factory<PathPlasma>(); 
		register_factory<PathStraight>(); 
		register_factory<PathGroup>(); 
		register_factory<PathFrenet>(); 
		register_factory<PathPolygon>(); 
		register_factory<PathSpiral>(); 
		register_factory<PathPoint>(); 
		register_factory<PathAttach>(); 
		register_factory<PathCurl>(); 
		register_factory<PathConnect>(); 
		register_factory<PathConnect2>(); 
		register_factory<PathMap>(); 
		register_factory<PathMerge>(); 
		register_factory<PathBezier>(); 
		register_factory<PathSuperEllipse>(); 
		register_factory<PathUnfold>(); 
		register_factory<PathStairCable>(); 
		register_factory<PathFile>(); 
		register_factory<PathFile2>(); 
		register_factory<PathXYZ>(); 
		register_factory<PathProfile>(); 
		register_factory<PathCosTheta>(); 
		register_factory<PathLemniscate>(); 
		// register_factory<PathEq>(); 
		register_factory<CosThetaBlock>(); 

		

		// harmonic for custom cct
		register_factory<CCTHarmonicInterp>();

		// transformations
		register_factory<Transformations>();
		register_factory<TransBend>();
		register_factory<TransFlip>();
		register_factory<TransReflect>();
		register_factory<TransReverse>();
		register_factory<TransRotate>();
		register_factory<TransTranslate>();

		// drives
		register_factory<DriveAC>();
		register_factory<DriveDC>();
		register_factory<DriveTrapz>();
		register_factory<DriveStep>();
		register_factory<DriveInterp>();
		register_factory<DriveInterpBCR>();
		register_factory<DrivePTrain>();
		register_factory<DriveLinear>();
		register_factory<DriveFourier>();
		register_factory<DriveRCCT>();
		register_factory<DriveSCurve>();
		register_factory<DriveRandom>();

		// calculation
		register_factory<CalcInductance>();
		register_factory<CalcGroup>();
		register_factory<CalcLine>();
		register_factory<CalcPath>();
		register_factory<CalcPlane>();
		register_factory<CalcGrid>();
		register_factory<CalcPolarGrid>();
		register_factory<CalcMesh>();
		register_factory<CalcLength>();
		register_factory<CalcHarmonics>();
		register_factory<CalcSpHarm>();
		register_factory<CalcPoints>();
		register_factory<CalcTracks>();
		register_factory<CalcFreeCAD>();
		register_factory<Circuit>();
		register_factory<CalcQuench0D>();
		register_factory<ProtectionCircuit>();
		register_factory<Background>();

		// emitters
		register_factory<EmitterBeam>();
		register_factory<EmitterOmni>();
		register_factory<EmitterNewton>();
		register_factory<EmitterCoordScan>();

		// data objects
		register_factory<Mesh>();

		// nl solver objects
		#ifdef ENABLE_NL_SOLVER
		register_factory<HBCurveTable>();
		register_factory<HBCurveFile>();
		register_factory<HBCurveVLV>();
		register_factory<ModelNL>();
		register_factory<ModelNLSphere>();
		register_factory<ModelNLFile>();
		#endif
	}

}}