// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathcurl.hh"

// common headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathCurl::PathCurl(){
		set_name("Curl");
	}

	// constructor with input of base path
	PathCurl::PathCurl(const ShPathPr &input_path) : PathCurl(){
		set_input_path(input_path);
	}

	// factory methods
	ShPathCurlPr PathCurl::create(){
		return std::make_shared<PathCurl>();
	}

	// factory with input of base path
	ShPathCurlPr PathCurl::create(const ShPathPr &input_path){
		return std::make_shared<PathCurl>(input_path);
	}


	// setters
	void PathCurl::set_disable_rotation(const bool disable_rotation){
		disable_rotation_ = disable_rotation;
	}

	void PathCurl::set_first_radius(const fltp first_radius){
		first_radius_ = first_radius;
	}
	
	void PathCurl::set_first_height(const fltp first_height){
		first_height_ = first_height;
	}
	
	void PathCurl::set_second_radius(const fltp second_radius){
		second_radius_ = second_radius;
	}
	
	void PathCurl::set_second_height(const fltp second_height){
		second_height_ = second_height;
	}


	//  getters
	bool PathCurl::get_disable_rotation()const{
		return disable_rotation_;
	}

	fltp PathCurl::get_first_radius() const{
		return first_radius_;
	}

	fltp PathCurl::get_first_height() const{
		return first_height_;
	}

	fltp PathCurl::get_second_radius() const{
		return second_radius_;
	}

	fltp PathCurl::get_second_height() const{
		return second_height_;
	}


	// update function
	ShFramePr PathCurl::create_frame(const MeshSettings &stngs) const{
		// check input
		is_valid(true);

		// create frame
		ShFramePr frame = input_path_->create_frame(stngs);

		// access arrays
		arma::field<arma::Mat<fltp> > R = frame->get_coords();
		arma::field<arma::Mat<fltp> > L = frame->get_direction();
		arma::field<arma::Mat<fltp> > N = frame->get_normal();
		arma::field<arma::Mat<fltp> > D = frame->get_transverse();
		arma::field<arma::Mat<fltp> > B = frame->get_block();

		// apply layer jump
		if(first_height_!=0){
			// calculate S-curve in 2D
			arma::Row<fltp> ell = arma::join_horiz(
				arma::Row<fltp>::fixed<1>{RAT_CONST(0.0)}, 
				arma::cumsum(cmn::Extra::vec_norm(arma::diff(R(0),1,1))));

			// calculate angle
			const fltp alpha = std::acos((first_radius_ - std::abs(first_height_))/first_radius_);

			// calculate length
			const fltp length = std::sin(alpha)*first_radius_;

			// shift ell
			ell = length - ell;
			ell(arma::find(ell<0)).fill(0.0);

			// distance from end
			const arma::Row<fltp> beta = arma::asin(ell/first_radius_);
			const arma::Row<fltp> height = cmn::Extra::sign(first_height_)*first_radius_*(RAT_CONST(1.0) - arma::cos(beta));

			// offset
			R(0) += D(0).each_row()%height;

			// rotate
			if(!disable_rotation_){
				for(arma::uword j=0;j<ell.n_elem;j++){
					const arma::Mat<fltp>::fixed<3,3> M = cmn::Extra::create_rotation_matrix(N(0).col(j),-cmn::Extra::sign(first_height_)*beta(j));
					D(0).col(j) = M*D(0).col(j); L(0).col(j) = M*L(0).col(j);
				}
			}
		}

		// apply layer jump
		if(second_height_!=0){
			// calculate S-curve in 2D
			arma::Row<fltp> ell = arma::join_horiz(
				arma::Row<fltp>::fixed<1>{RAT_CONST(0.0)}, 
				arma::cumsum(cmn::Extra::vec_norm(arma::diff(R(R.n_elem-1),1,1))));

			// calculate angle
			const fltp alpha = std::acos((second_radius_ - std::abs(second_height_))/second_radius_);

			// calculate length
			const fltp length = std::sin(alpha)*second_radius_;

			// shift ell
			ell -= ell.back() - length;
			ell(arma::find(ell<0)).fill(0.0);

			// distance from end
			const arma::Row<fltp> beta = arma::asin(ell/second_radius_);
			const arma::Row<fltp> height = cmn::Extra::sign(second_height_)*second_radius_*(RAT_CONST(1.0) - arma::cos(beta));
			
			// offset
			R(R.n_elem-1) += D(D.n_elem-1).each_row()%height;

			// rotate
			if(!disable_rotation_){
				for(arma::uword j=0;j<ell.n_elem;j++){
					const arma::Mat<fltp>::fixed<3,3> M = cmn::Extra::create_rotation_matrix(N(N.n_elem-1).col(j),cmn::Extra::sign(second_height_)*beta(j));
					D(D.n_elem-1).col(j) = M*D(D.n_elem-1).col(j); L(L.n_elem-1).col(j) = M*L(L.n_elem-1).col(j);
				}
			}
		}

		// check handedness
		#ifndef NDEBUG
		for(arma::uword i=0;i<N.n_elem;i++)
			assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N(i),L(i)),D(i))>RAT_CONST(0.0)));
		#endif

		// create frame
		const ShFramePr curled_frame = Frame::create(R,L,N,D,B);

		// apply transformations
		curled_frame->apply_transformations(get_transformations(), stngs.time);

		// create frame and return
		return curled_frame;
	}

	// vallidity check
	bool PathCurl::is_valid(const bool enable_throws) const{
		if(!InputPath::is_valid(enable_throws))return false;
		if(!Transformations::is_valid(enable_throws))return false;
		if(first_radius_<=0){if(enable_throws){rat_throw_line("first radius must be larger than zero");} return false;};
		if(second_radius_<=0){if(enable_throws){rat_throw_line("second radius must be larger than zero");} return false;};
		return true;
	}


	// get type
	std::string PathCurl::get_type(){
		return "rat::mdl::pathcurl";
	}

	// method for serialization into json
	void PathCurl::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents first 
		// this allows the type to be overridden
		Path::serialize(js,list);
		InputPath::serialize(js,list);
		Transformations::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["first_radius"] = first_radius_;
		js["first_height"] = first_height_;
		js["second_radius"] = second_radius_;
		js["second_height"] = second_height_;
		js["disable_rotation"] = disable_rotation_;
	}

	// method for deserialisation from json
	void PathCurl::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent objects
		Path::deserialize(js,list,factory_list,pth);
		InputPath::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);

		// properties
		set_first_radius(js["first_radius"].ASFLTP());
		set_first_height(js["first_height"].ASFLTP());
		set_second_radius(js["second_radius"].ASFLTP());
		set_second_height(js["second_height"].ASFLTP());
		set_disable_rotation(js["disable_rotation"].asBool());
	}

}}
