// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathrutherford.hh"

// rat-common headers
#include "rat/common/extra.hh"


// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathRutherford::PathRutherford(){
		set_name("Rutherford path");
	}

	// constructor with input of base path
	PathRutherford::PathRutherford(ShPathPr base){
		set_base(base);
	}

	// factory methods
	ShPathRutherfordPr PathRutherford::create(){
		return std::make_shared<PathRutherford>();
	}

	// factory with input of base path
	ShPathRutherfordPr PathRutherford::create(ShPathPr base){
		return std::make_shared<PathRutherford>(base);
	}

	// set base path
	void PathRutherford::set_base(ShPathPr base){
		if(base==NULL)rat_throw_line("base points to zero");
		base_ = base;
	}

	// update function
	ShFramePr PathRutherford::create_frame(const MeshSettings &stngs) const{
		// check input
		if(base_==NULL)rat_throw_line("base is not set");
		
		// create frame
		ShFramePr frame = base_->create_frame(stngs);
		arma::field<arma::Mat<fltp> > Rf = frame->get_coords();
		const arma::uword num_section = Rf.n_elem;

		// calculate length array
		fltp ell_accu = 0;
		arma::field<arma::Row<fltp> > ell(Rf.n_elem);
		for(arma::uword i=0;i<num_section;i++){
			ell(i) = arma::cumsum(rat::cmn::Extra::vec_norm(
				Rf(i).tail_cols(Rf(i).n_cols-1)-Rf(i).head_cols(Rf(i).n_cols-1))) + ell_accu;
			ell_accu = ell(i)(ell(i).n_elem-1);
		}

		// calculate properties
		fltp width = u2_-u1_, thickness = v2_-v1_;
		fltp omega = 2*thickness*(thickness+width)/
			std::sqrt(-fltp(num_strand_)*num_strand_*thickness*
			thickness + 4*thickness*thickness + 
			8*thickness*width + 4*width*width);
		fltp pitch = num_strand_*omega;
		fltp alpha = std::atan((2*width + 2*thickness)/pitch);
		fltp dw = width/std::tan(alpha);
		fltp dt = thickness/std::tan(alpha);

		// draw single pitch
		arma::Row<fltp> w = {0,dw,dw+dt,2*dw+dt,2*dw+2*dt};
		arma::Row<fltp> u = {0,dw,dw,0,0};
		// arma::Row<fltp> v = {0,0,dt,dt,0};
		
		// create strands
		arma::field<arma::Mat<fltp> > Rs(num_strand_,num_section);

		// walk over sections
		for(arma::uword k=0;k<num_section;k++){
			// calculate local position within pitch
			arma::Row<fltp> local_ell(ell(k)); local_ell.zeros();
			for(arma::uword j=0;j<ell(k).n_elem;j++)
				local_ell(j) = std::fmod(ell(k)(j),pitch);

			// walk over strands
			for(arma::uword i=0;i<num_strand_;i++){
				// allocate
				arma::Col<fltp> us,vs;

				// interpolate
				arma::interp1(w.t(),u.t(),local_ell.t(),us);
				arma::interp1(w.t(),u.t(),local_ell.t(),vs);

				// combine
				Rs(k,i) = arma::join_vert(us.t(),vs.t(),ell(i));
			}
		}

		// num_turns = 1.0/num_strand_;
		return frame;
	}

	// get type
	std::string PathRutherford::get_type(){
		return "rat::mdl::pathrutherford";
	}

	// method for serialization into json
	void PathRutherford::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents first 
		// this allows the type to be overridden
		Transformations::serialize(js,list);

		// subnodes
		js["base"] = cmn::Node::serialize_node(base_, list);
	}

	// method for deserialisation from json
	void PathRutherford::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent objects
		Transformations::deserialize(js,list,factory_list,pth);

		// subnodes
		base_ = cmn::Node::deserialize_node<Path>(js["base"], list, factory_list, pth);
	}

}}
