// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// only avail
#ifdef ENABLE_NL_SOLVER

// include header file
#include "hbcurvevlv.hh"

// code specific to Raccoon
namespace rat{namespace mdl{

	// constructor
	HBCurveVLV::HBCurveVLV(){
		// set name
		set_name("HB-Curve Vinh Le-Van");
	}

	HBCurveVLV::HBCurveVLV(
		const fltp mur, const fltp Js, 
		const fltp ff, const fltp H2, 
		const arma::uword num_points) : HBCurveVLV(){
		set_relative_permeability(mur); 
		set_saturation_field(Js);
		set_num_points(num_points);
		set_filling_fraction(ff);
		set_upper_magnetic_field(H2);
	}

	// factory
	ShHBCurveVLVPr HBCurveVLV::create(){
		return std::make_shared<HBCurveVLV>();
	}

	ShHBCurveVLVPr HBCurveVLV::create(
		const fltp mur, const fltp Js, 
		const fltp ff, const fltp H2, 
		const arma::uword num_points){
		return std::make_shared<HBCurveVLV>(mur,Js,ff,H2,num_points);
	}


	// setters
	void HBCurveVLV::set_relative_permeability(const fltp mur){
		mur_ = mur;
	}

	void HBCurveVLV::set_saturation_field(const fltp Js){
		Js_ = Js;
	}

	void HBCurveVLV::set_filling_fraction(const fltp ff){
		ff_ = ff;
	}

	void HBCurveVLV::set_num_points(const arma::uword num_points){
		num_points_ = num_points;
	}

	void HBCurveVLV::set_upper_magnetic_field(const fltp H2){
		H2_ = H2;
	}

	// getters
	fltp HBCurveVLV::get_relative_permeability()const{
		return mur_;
	}

	fltp HBCurveVLV::get_saturation_field()const{
		return Js_;
	}

	fltp HBCurveVLV::get_filling_fraction()const{
		return ff_;
	}

	arma::uword HBCurveVLV::get_num_points()const{
		return num_points_;
	}

	fltp HBCurveVLV::get_upper_magnetic_field()const{
		return H2_;
	}

	arma::Mat<fltp> HBCurveVLV::get_table()const{
		// create interpolation table based on fit
		const arma::Col<fltp> Hinterp = 
			arma::linspace<arma::Col<fltp> >(RAT_CONST(0.0),H2_,num_points_);
		const arma::Col<fltp> Binterp = 
			arma::Datum<fltp>::mu_0*Hinterp + 
			(2*Js_/arma::Datum<fltp>::pi)*arma::atan(
			arma::Datum<fltp>::pi*(mur_ - RAT_CONST(1.0))*
			arma::Datum<fltp>::mu_0*Hinterp/(2*Js_));
		return arma::join_horiz(Hinterp,Binterp);
	}

	// create hb data
	nl::ShHBDataPr HBCurveVLV::create_hb_data()const{
		// check validity
		is_valid(true);

		// create interpolation table based on fit
		const arma::Mat<fltp> HB = get_table();
		assert(HB.n_cols==2);
		
		// create data object
		return nl::HBData::create(HB.col(0),HB.col(1),ff_);
	}
	

	// vallidity check
	bool HBCurveVLV::is_valid(const bool enable_throws) const{
		if(ff_<=RAT_CONST(0.0)){if(enable_throws){rat_throw_line("filling fraction can not be less than zero");} return false;};
		return true;
	}

	// get type
	std::string HBCurveVLV::get_type(){
		return "rat::mdl::hbcurvevlv";
	}

	// method for serialization into json
	void HBCurveVLV::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents first 
		// this allows the type to be overridden
		HBCurve::serialize(js,list);

		// properties
		js["type"] = get_type();

		// serialize hb curve
		js["ff"] = ff_;
		js["mur"] = mur_;
		js["Js"] = Js_;
		js["num_points"] = static_cast<int>(num_points_);
		js["ff"] = ff_;
		js["H2"] = H2_;
	}

	// method for deserialisation from json
	void HBCurveVLV::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		
		// parent objects
		HBCurve::deserialize(js,list,factory_list,pth);

		// serialize hb curve
		set_filling_fraction(js["ff"].ASFLTP());
		set_relative_permeability(js["mur"].ASFLTP());
		set_saturation_field(js["Js"].ASFLTP());
		set_num_points(js["num_points"].asUInt64());
		set_upper_magnetic_field(js["H2"].ASFLTP());
	}


}}

#endif