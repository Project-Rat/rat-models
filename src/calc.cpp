// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calc.hh"
#include "vtkpvdata.hh"
#include "data.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// set active time
	void Calc::set_time(const fltp time){
		time_ = time;
	}

	// setters
	void Calc::set_t1(const fltp t1){
		t1_ = t1;
	}

	void Calc::set_t2(const fltp t2){
		t2_ = t2;
	}

	void Calc::set_num_times(const arma::uword num_times){
		num_times_ = num_times;
	}

	void Calc::set_output_dir(const boost::filesystem::path &output_dir){
		output_dir_ = output_dir;
	}

	// get active time
	fltp Calc::get_time()const{
		return time_;
	}

	// getters
	fltp Calc::get_t1()const{
		return t1_;
	}

	fltp Calc::get_t2()const{
		return t2_;
	}

	arma::uword Calc::get_num_times()const{
		return num_times_;
	}

	const boost::filesystem::path& Calc::get_output_dir()const{
		return output_dir_;
	}

	// calculate and write
	void Calc::calculate_write(
		const cmn::ShLogPr &lg, 
		const ShSolverCachePr& cache){
		calculate_write(arma::linspace<arma::Row<fltp> >(t1_, t2_, num_times_),output_dir_,lg,cache);
	}

	// calculate and write
	void Calc::calculate_write(
		const arma::Row<fltp> &output_times, 
		const boost::filesystem::path &output_dir, 
		const cmn::ShLogPr &lg, 
		const ShSolverCachePr &cache){

		// check cancelling
		if(lg->is_cancelled())return;

		// create directory
		if(!output_dir.empty())
			boost::filesystem::create_directory(output_dir);

		// create paraview data
		mdl::ShVTKPvDataPr pvd = mdl::VTKPvData::create();
		
		// output directory
		if(output_times.empty())rat_throw_line("output times are not set");

		// display the rat logo
		lg->msg("%s%sRUNNING CALCULATIONS AND WRITING OUTPUT%s\n",KBLD,KGRN,KNRM);

		// walk over output times
		for(arma::uword j=0;j<output_times.n_elem;j++){
			// calculate this time step
			std::list<ShDataPr> output_objects = calculate(output_times(j), lg, cache);

			// check cancelling
			if(lg->is_cancelled())return;

			// check if output is needed
			if(!output_dir.empty()){
				// header
				lg->msg(2,"%s%sWRITING VTK FILES%s\n",KBLD,KGRN,KNRM);

				// indexing 
				std::map<std::string, arma::uword> count_objects;

				// write mesh VTK files
				for(auto it = output_objects.begin();it!=output_objects.end();it++){
					// check cancelling
					if(lg->is_cancelled())return;

					// get calculation data
					ShDataPr data = (*it);
					const std::string output_type = data->get_output_type();

					// increment index
					if(count_objects.find(output_type)==count_objects.end())count_objects[output_type]=0;
					const arma::uword myidx = count_objects[output_type]++;

					// export to vtk
					const mdl::ShVTKObjPr vtk = data->export_vtk();

					// create name
					boost::filesystem::path fname = mdl::VTKPvData::create_fname(
						output_type,j,myidx).replace_extension(vtk->get_filename_ext());

					// write mesh to vtk file
					vtk->write(output_dir/fname,lg);

					// add to paraview data
					pvd->add_file(output_type, data->get_name(), output_times(j), myidx, fname);
				}

				// end file writing
				lg->msg(-2,"\n");
			}
		}

		// write pvd file
		pvd->write(output_dir);
	}

	// calculation function use time from self
	std::list<ShDataPr> Calc::calculate(
		const cmn::ShLogPr &lg,
		const ShSolverCachePr& cache){
		return calculate(time_, lg, cache);
	}

	// calculation meshes
	std::list<ShMeshDataPr> Calc::create_meshes(
		const std::list<arma::uword> &/*trace*/, 
		const MeshSettings &/*stngs*/) const{
		return {};
	}

	// get type
	std::string Calc::get_type(){
		return "rat::mdl::calc";
	}

	// method for serialization into json
	void Calc::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Node::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["time"] = time_;
		js["t1"] = t1_;
		js["t2"] = t2_;
		js["num_times"] = static_cast<int>(num_times_);
		js["output_dir"] = output_dir_.string();
	}

	// method for deserialisation from json
	void Calc::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		Node::deserialize(js,list,factory_list,pth);
		set_time(js["time"].ASFLTP());
		set_t1(js["t1"].ASFLTP());
		set_t2(js["t2"].ASFLTP());
		set_num_times(js["num_times"].asUInt64());
		output_dir_ = boost::filesystem::path(js["output_dir"].asString());
	}

}}