// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathlemniscate.hh"

#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathLemniscate::PathLemniscate(){
		set_name("Lemniscate");
	}

	// constructor
	PathLemniscate::PathLemniscate(
		const fltp a, const fltp b, 
		const fltp c, const fltp d,
		const fltp lambda,
		const fltp element_size,
		const fltp offset,
		const arma::uword num_sections) : PathLemniscate(){

		// set to self
		set_a(a); set_b(b); set_c(c); set_d(d); 
		set_lambda(lambda);
		set_element_size(element_size);
		set_offset(offset);
		set_num_sections(num_sections);
	}

	// factory
	ShPathLemniscatePr PathLemniscate::create(){
		return std::make_shared<PathLemniscate>();
	}

	// factory with dimensional input
	ShPathLemniscatePr PathLemniscate::create(
		const fltp a, const fltp b, 
		const fltp c, const fltp d,
		const fltp lambda,
		const fltp element_size,
		const fltp offset,
		const arma::uword num_sections){
		return std::make_shared<PathLemniscate>(a,b,c,d,lambda,element_size,offset,num_sections);
	}


	// setters
	void PathLemniscate::set_a(const fltp a){
		a_ = a;
	}

	void PathLemniscate::set_b(const fltp b){
		b_ = b;
	}

	void PathLemniscate::set_c(const fltp c){
		c_ = c;
	}

	void PathLemniscate::set_alpha(const fltp alpha){
		alpha_ = alpha;
	}

	void PathLemniscate::set_beta(const fltp beta){
		beta_ = beta;
	}

	void PathLemniscate::set_gamma(const fltp gamma){
		gamma_ = gamma;
	}

	void PathLemniscate::set_d(const fltp d){
		d_ = d;
	}

	void PathLemniscate::set_lambda(const fltp lambda){
		lambda_ = lambda;
	}

	void PathLemniscate::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	void PathLemniscate::set_offset(const fltp offset){
		offset_ = offset;
	}

	void PathLemniscate::set_num_sections(const arma::uword num_sections){
		num_sections_ = num_sections;
	}

	// getters
	fltp PathLemniscate::get_a() const{
		return a_;
	}

	fltp PathLemniscate::get_b() const{
		return b_;
	}

	fltp PathLemniscate::get_c() const{
		return c_;
	}

	fltp PathLemniscate::get_alpha() const{
		return alpha_;
	}

	fltp PathLemniscate::get_beta() const{
		return beta_;
	}

	fltp PathLemniscate::get_gamma() const{
		return gamma_;
	}

	fltp PathLemniscate::get_d() const{
		return d_;
	}

	fltp PathLemniscate::get_lambda() const{
		return lambda_;
	}

	fltp PathLemniscate::get_element_size() const{
		return element_size_;
	}

	fltp PathLemniscate::get_offset() const{
		return offset_;
	}

	arma::uword PathLemniscate::get_num_sections() const{
		return num_sections_;
	}

	arma::Row<fltp> PathLemniscate::calc_s(const arma::Row<fltp>& theta)const{
		return lambda_/(lambda_+RAT_CONST(1.0)-arma::cos(2*theta));
	}

	arma::Row<fltp> PathLemniscate::calc_p(const arma::Row<fltp>& theta)const{
		return RAT_CONST(1.0) + d_*arma::sin(2*theta);
	}

	// precalculations
	arma::Mat<fltp> PathLemniscate::create_coords(const arma::Row<fltp>& theta)const{
		// precalculation
		const arma::Row<fltp> s = calc_s(theta); const arma::Row<fltp> p = calc_p(theta);

		// lemniscate function
		const arma::Mat<fltp> R = arma::join_vert(
			a_*s%p%arma::sin(theta)+alpha_,
			b_*arma::sin(theta)+beta_,
			c_*s%arma::cos(theta)+gamma_);

		// return the coordinates
		return R;
	}

	// get frame
	ShFramePr PathLemniscate::create_frame(const MeshSettings &stngs) const{
		// check if is valid
		is_valid(true);

		// determine number of points
		const arma::uword num_points_ell = 100llu;

		// allocate
		arma::field<arma::Mat<fltp> > R(1,num_sections_);
		arma::field<arma::Mat<fltp> > L(1,num_sections_);
		arma::field<arma::Mat<fltp> > N(1,num_sections_);
		arma::field<arma::Mat<fltp> > D(1,num_sections_);

		// theta sections
		const arma::Row<fltp> theta_section = arma::linspace<arma::Row<fltp> >(RAT_CONST(0.0),arma::Datum<fltp>::tau,num_sections_+1);

		// walk over sections
		for(arma::uword i=0;i<num_sections_;i++){

			// number of nodes
			arma::uword num_nodes_section = std::max(static_cast<arma::uword>(std::ceil(arma::accu(cmn::Extra::vec_norm(arma::diff(create_coords(
				arma::linspace<arma::Row<fltp> >(theta_section(i),theta_section(i+1),num_points_ell)),1,1)))/element_size_)),2llu);
			while((num_nodes_section-1)%stngs.element_divisor!=0)num_nodes_section++;

			// create theta
			const arma::Row<fltp> theta = arma::linspace<arma::Row<fltp> >(theta_section(i),theta_section(i+1),num_nodes_section);

			// precalculation
			const arma::Row<fltp> s = calc_s(theta); const arma::Row<fltp> p = calc_p(theta);

			// lemniscate function
			R(i) = create_coords(theta);

			// longitudinal winding direction
			L(i) = cmn::Extra::normalize(arma::join_vert(
				(arma::cos(theta)*a_*lambda_)/(lambda_+1.0-arma::cos(2*theta))-
					lambda_*(arma::sin(theta)*a_*2)%arma::sin(2*theta)/arma::square(lambda_+1.0-arma::cos(2*theta))+
					(arma::cos(theta)*a_*lambda_/(lambda_+1.0-arma::cos(2*theta))*d_)%arma::sin(2*theta)+
					(arma::sin(theta)*a_*lambda_)/(lambda_+1.0-arma::cos(2*theta))%(d_*2*arma::cos(2*theta))-
					(arma::sin(theta)*lambda_*d_*a_*2)%arma::square(arma::sin(2*theta))/arma::square(lambda_+1.0-arma::cos(2*theta)),
					b_*arma::cos(theta),
				-(lambda_*arma::cos(theta)*c_*2%arma::sin(2*theta)/arma::square(lambda_+1.0-arma::cos(2*theta))+
					c_*lambda_/(lambda_+1.0-arma::cos(2*theta))%arma::sin(theta))));

			// normal winding direction
			N(i) = cmn::Extra::normalize(arma::join_vert(
				s%p%arma::sin(theta),
				arma::Row<fltp>(num_nodes_section,arma::fill::zeros),
				s%arma::cos(theta)));

			// orthogonalise
			N(i) = cmn::Extra::normalize(cmn::Extra::cross(L(i),cmn::Extra::cross(N(i),L(i))));

			// transverse winding direction
			D(i) = cmn::Extra::cross(N(i),L(i));
		}

		// create frame
		const ShFramePr frame = Frame::create(R,L,N,D);

		// apply transformations
		frame->apply_transformations(get_transformations(), stngs.time);

		// return frame
		return frame;
	}

	// validity check
	bool PathLemniscate::is_valid(const bool enable_throws) const{
		if(num_sections_<=0){if(enable_throws){rat_throw_line("number of sections must be positive");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be positive");} return false;};
		return true;
	}

	// get type
	std::string PathLemniscate::get_type(){
		return "rat::mdl::pathlemniscate";
	}

	// method for serialization into json
	void PathLemniscate::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Path::serialize(js,list);
		Transformations::serialize(js,list);

		// set type
		js["type"] = get_type();

		// properties
		js["a"] = a_;
		js["b"] = b_;
		js["c"] = c_;
		js["d"] = d_;
		js["lambda"] = lambda_;
		js["num_sections"] = static_cast<int>(num_sections_);
		js["element_size"] = element_size_;
		js["offset"] = offset_;
	}

	// method for deserialisation from json
	void PathLemniscate::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		Path::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);
		
		// properties
		set_a(js["a"].ASFLTP());
		set_b(js["b"].ASFLTP());
		set_c(js["c"].ASFLTP());
		set_d(js["d"].ASFLTP());
		set_lambda(js["lambda"].ASFLTP());
		set_element_size(js["element_size"].ASFLTP());
		set_offset(js["offset"].ASFLTP());
		set_num_sections(js["num_sections"].asUInt64());
	}


}}