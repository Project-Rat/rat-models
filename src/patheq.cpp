// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "patheq.hh"

// rat-common headers
#include "rat/common/extra.hh"
#include "rat/common/error.hh"

// rat-math headers
#include "rat/math/parser.hh"

// rat-modes headers
#include "darboux.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathEq::PathEq(){
		set_name("Eq Path");
	}

	PathEq::PathEq(
		const std::string& variable,
		const math::Equation& values,
		const math::Equation& eqx, 
		const math::Equation& eqy, 
		const math::Equation& eqz) : PathEq(){
		variable_ = variable; values_ = values;
		eqx_ = eqx; eqy_ = eqy; eqz_ = eqz;
	}

	// factory
	ShPathEqPr PathEq::create(){
		return std::make_shared<PathEq>();
	}

	ShPathEqPr PathEq::create(
		const std::string& variable,
		const math::Equation& values,
		const math::Equation& eqx, 
		const math::Equation& eqy, 
		const math::Equation& eqz){
		return std::make_shared<PathEq>(variable, values, eqx, eqy, eqz);
	}


	// get frame
	ShFramePr PathEq::create_frame(const MeshSettings &stngs) const{
		// parser 
		const math::ShParserPr parser = math::Parser::create();
		parser->set_variable(variable_, values_);

		// evaluate variable
		const arma::Row<fltp> time = parser->evaluate(values_,math::Unit::s);

		// evaluate equations
		arma::Mat<fltp> x,y,z;
		try{
			x = parser->evaluate(eqx_,math::Unit::m);
		}catch(const rat_error& err){
			rat_throw_line("error during evaluating equation for x: " + std::string(err.what()));
		}

		try{
			y = parser->evaluate(eqy_,math::Unit::m);
		}catch(const rat_error& err){
			rat_throw_line("error during evaluating equation for y: " + std::string(err.what()));
		}

		try{
			z = parser->evaluate(eqz_,math::Unit::m);
		}catch(const rat_error& err){
			rat_throw_line("error during evaluating equation for z: " + std::string(err.what()));
		}

		// create derivatives
		math::Equation eqdx, eqddx, eqdddx, eqdy, eqddy, eqdddy, eqdz, eqddz, eqdddz;
		try{
			eqdx = parser->calc_derivative(eqx_,variable_);
		}catch(const rat_error& err){
			rat_throw_line("error during calculation of first derivative of x: " + std::string(err.what()));
		}

		try{
			eqddx = parser->calc_derivative(eqdx,variable_);
		}catch(const rat_error& err){
			rat_throw_line("error during calculation of second derivative of x: " + std::string(err.what()));
		}

		try{
			eqdddx = parser->calc_derivative(eqddx,variable_);
		}catch(const rat_error& err){
			rat_throw_line("error during calculation of third derivative of x: " + std::string(err.what()));
		}
		
		try{
			eqdy = parser->calc_derivative(eqy_,variable_);
		}catch(const rat_error& err){
			rat_throw_line("error during calculation of first derivative of y: " + std::string(err.what()));
		}

		try{
			eqddy = parser->calc_derivative(eqdy,variable_);
		}catch(const rat_error& err){
			rat_throw_line("error during calculation of second derivative of y: " + std::string(err.what()));
		}

		try{
			eqdddy = parser->calc_derivative(eqddy,variable_);
		}catch(const rat_error& err){
			rat_throw_line("error during calculation of third derivative of y: " + std::string(err.what()));
		}

		try{
			eqdz = parser->calc_derivative(eqz_,variable_);
		}catch(const rat_error& err){
			rat_throw_line("error during calculation of first derivative of z: " + std::string(err.what()));
		}

		try{
			eqddz = parser->calc_derivative(eqdz,variable_);
		}catch(const rat_error& err){
			rat_throw_line("error during calculation of second derivative of z: " + std::string(err.what()));
		}

		try{
			eqdddz = parser->calc_derivative(eqddz,variable_);
		}catch(const rat_error& err){
			rat_throw_line("error during calculation of third derivative of z: " + std::string(err.what()));
		}

		// evaluate equations
		arma::Mat<fltp> dx, dy, dz, ddx, ddy, ddz, dddx, dddy, dddz;
		try{
			dx = parser->evaluate(eqdx,math::Unit::m/math::Unit::s);
		}catch(const rat_error& err){
			rat_throw_line("error during evaluation of the first derivative of x: " + std::string(err.what()));
		}

		try{
			dy = parser->evaluate(eqdy,math::Unit::m/math::Unit::s);
		}catch(const rat_error& err){
			rat_throw_line("error during evaluation of the first derivative of y: " + std::string(err.what()));
		}

		try{
			dz = parser->evaluate(eqdz,math::Unit::m/math::Unit::s);
		}catch(const rat_error& err){
			rat_throw_line("error during evaluation of the first derivative of z: " + std::string(err.what()));
		}

		try{
			ddx = parser->evaluate(eqddx,math::Unit::m/(math::Unit::s^2));
		}catch(const rat_error& err){
			rat_throw_line("error during evaluation of the second derivative of x: " + std::string(err.what()));
		}

		try{
			ddy = parser->evaluate(eqddy,math::Unit::m/(math::Unit::s^2));
		}catch(const rat_error& err){
			rat_throw_line("error during evaluation of the second derivative of y: " + std::string(err.what()));
		}

		try{
			ddz = parser->evaluate(eqddz,math::Unit::m/(math::Unit::s^2));
		}catch(const rat_error& err){
			rat_throw_line("error during evaluation of the second derivative of z: " + std::string(err.what()));
		}

		try{
			dddx = parser->evaluate(eqdddx,math::Unit::m/(math::Unit::s^3));
		}catch(const rat_error& err){
			rat_throw_line("error during evaluation of the third derivative of x: " + std::string(err.what()));
		}

		try{
			dddy = parser->evaluate(eqdddy,math::Unit::m/(math::Unit::s^3));
		}catch(const rat_error& err){
			rat_throw_line("error during evaluation of the third derivative of y: " + std::string(err.what()));
		}

		try{
			dddz = parser->evaluate(eqdddz,math::Unit::m/(math::Unit::s^3));
		}catch(const rat_error& err){
			rat_throw_line("error during evaluation of the third derivative of z: " + std::string(err.what()));
		}

		// check output
		if(x.n_rows!=1)rat_throw_line("x-equation does not evaluate to one row");
		if(y.n_rows!=1)rat_throw_line("y-equation does not evaluate to one row");
		if(z.n_rows!=1)rat_throw_line("z-equation does not evaluate to one row");
		if(dx.n_rows!=1)rat_throw_line("dx-equation does not evaluate to one row");
		if(dy.n_rows!=1)rat_throw_line("dy-equation does not evaluate to one row");
		if(dz.n_rows!=1)rat_throw_line("dz-equation does not evaluate to one row");
		if(ddx.n_rows!=1)rat_throw_line("ddx-equation does not evaluate to one row");
		if(ddy.n_rows!=1)rat_throw_line("ddy-equation does not evaluate to one row");
		if(ddz.n_rows!=1)rat_throw_line("ddz-equation does not evaluate to one row");
		if(dddx.n_rows!=1)rat_throw_line("dddx-equation does not evaluate to one row");
		if(dddy.n_rows!=1)rat_throw_line("dddy-equation does not evaluate to one row");
		if(dddz.n_rows!=1)rat_throw_line("dddz-equation does not evaluate to one row");

		// extend scalars
		if(x.n_cols==1)x = arma::Row<fltp>(time.n_elem, arma::fill::value(x(0)));
		if(y.n_cols==1)y = arma::Row<fltp>(time.n_elem, arma::fill::value(y(0)));
		if(z.n_cols==1)z = arma::Row<fltp>(time.n_elem, arma::fill::value(z(0)));
		if(dx.n_cols==1)dx = arma::Row<fltp>(time.n_elem, arma::fill::value(dx(0)));
		if(dy.n_cols==1)dy = arma::Row<fltp>(time.n_elem, arma::fill::value(dy(0)));
		if(dz.n_cols==1)dz = arma::Row<fltp>(time.n_elem, arma::fill::value(dz(0)));
		if(ddx.n_cols==1)ddx = arma::Row<fltp>(time.n_elem, arma::fill::value(ddx(0)));
		if(ddy.n_cols==1)ddy = arma::Row<fltp>(time.n_elem, arma::fill::value(ddy(0)));
		if(ddz.n_cols==1)ddz = arma::Row<fltp>(time.n_elem, arma::fill::value(ddz(0)));
		if(dddx.n_cols==1)dddx = arma::Row<fltp>(time.n_elem, arma::fill::value(dddx(0)));
		if(dddy.n_cols==1)dddy = arma::Row<fltp>(time.n_elem, arma::fill::value(dddy(0)));
		if(dddz.n_cols==1)dddz = arma::Row<fltp>(time.n_elem, arma::fill::value(dddz(0)));

		// check lengths
		if(x.n_cols!=y.n_cols)rat_throw_line("number of columns of x and y equations does not match");
		if(x.n_cols!=z.n_cols)rat_throw_line("number of columns of x and z equations does not match");
		if(x.n_cols!=dx.n_cols)rat_throw_line("number of columns of x and y equations does not match");
		if(x.n_cols!=dy.n_cols)rat_throw_line("number of columns of x and y equations does not match");
		if(x.n_cols!=dz.n_cols)rat_throw_line("number of columns of x and z equations does not match");
		if(x.n_cols!=ddx.n_cols)rat_throw_line("number of columns of x and y equations does not match");
		if(x.n_cols!=ddy.n_cols)rat_throw_line("number of columns of x and y equations does not match");
		if(x.n_cols!=ddz.n_cols)rat_throw_line("number of columns of x and z equations does not match");
		if(x.n_cols!=dddx.n_cols)rat_throw_line("number of columns of x and y equations does not match");
		if(x.n_cols!=dddy.n_cols)rat_throw_line("number of columns of x and y equations does not match");
		if(x.n_cols!=dddz.n_cols)rat_throw_line("number of columns of x and z equations does not match");
		

		// check finite
		if(!x.is_finite())rat_throw_line("x-coordinates are not finite");
		if(!y.is_finite())rat_throw_line("y-coordinates are not finite");
		if(!z.is_finite())rat_throw_line("z-coordinates are not finite");
		if(!dx.is_finite())rat_throw_line("dx-coordinates are not finite");
		if(!dy.is_finite())rat_throw_line("dy-coordinates are not finite");
		if(!dz.is_finite())rat_throw_line("dz-coordinates are not finite");
		if(!ddx.is_finite())rat_throw_line("ddx-coordinates are not finite");
		if(!ddy.is_finite())rat_throw_line("ddy-coordinates are not finite");
		if(!ddz.is_finite())rat_throw_line("ddz-coordinates are not finite");
		if(!dddx.is_finite())rat_throw_line("dddx-coordinates are not finite");
		if(!dddy.is_finite())rat_throw_line("dddy-coordinates are not finite");
		if(!dddz.is_finite())rat_throw_line("dddz-coordinates are not finite");

		// combine values into coordinate matrix
		const arma::Mat<fltp> R = arma::join_vert(x,y,z);
		const arma::Mat<fltp> V = arma::join_vert(dx,dy,dz);
		const arma::Mat<fltp> A = arma::join_vert(ddx,ddy,ddz);
		const arma::Mat<fltp> J = arma::join_vert(dddx,dddy,dddz);

		// run through frenet serret frame
		const bool use_analytic = true;
		Darboux db(V,A,J);
		db.setup(use_analytic);

		// get orientation vectors from darboux frame
		const arma::Mat<fltp>& L = db.get_longitudinal();
		const arma::Mat<fltp> N = db.get_normal();
		const arma::Mat<fltp>& D = db.get_transverse();

		// create frame
		const ShFramePr frame = Frame::create(R, L, N, D);

		// transformations
		frame->apply_transformations(get_transformations(), stngs.time);

		// create frame
		return frame;
	}

	// validity check
	bool PathEq::is_valid(const bool enable_throws) const{
		return true;
	}

	void PathEq::set_eqx(const math::Equation& eqx){
		eqx_ = eqx;
	}

	void PathEq::set_eqy(const math::Equation& eqy){
		eqy_ = eqy;
	}	

	void PathEq::set_eqz(const math::Equation& eqz){
		eqz_ = eqz;
	}


	// get type
	std::string PathEq::get_type(){
		return "rat::mdl::patheq";
	}

	// method for serialization into json
	void PathEq::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Path::serialize(js,list);
		Transformations::serialize(js,list);
		js["eqx"] = eqx_;
		js["eqy"] = eqy_;
		js["eqz"] = eqz_;
	}

	// method for deserialisation from json
	void PathEq::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent
		Path::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);
		eqx_ = js["eqx"];
		eqy_ = js["eqy"];
		eqz_ = js["eqz"];
	}


}}