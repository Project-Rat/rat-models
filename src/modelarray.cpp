// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelarray.hh"

// rat-common headers
#include "rat/common/error.hh"
#include "rat/common/parfor.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	ModelArray::ModelArray(){
		set_name("Array");
	}

	// constructor with setting of the base path
	ModelArray::ModelArray(const ShModelPr &base_model) : ModelArray(){
		add_model(base_model);
	}

	// constructor with setting of the base path
	ModelArray::ModelArray(const ShModelPr &base_model,
		const arma::uword num_x, const arma::uword num_y, const arma::uword num_z, 
		const fltp dx, const fltp dy, const fltp dz, const bool centered){
		add_model(base_model);
		set_grid_size(num_x,num_y,num_z);
		set_grid_spacing(dx,dy,dz);
		set_centered(centered);
	}

	// factory
	ShModelArrayPr ModelArray::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<ModelArray>();
	}

	// factory with setting of the base path
	ShModelArrayPr ModelArray::create(const ShModelPr &base_model){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<ModelArray>(base_model);
	}

	// factory with setting of the base path
	ShModelArrayPr ModelArray::create(const ShModelPr &base_model,
		const arma::uword num_x, const arma::uword num_y, const arma::uword num_z, 
		const fltp dx, const fltp dy, const fltp dz, const bool centered){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<ModelArray>(base_model,num_x,num_y,num_z,dx,dy,dz,centered);
	}

	// set grid size
	void ModelArray::set_grid_size(
		const arma::uword num_x, const arma::uword num_y, const arma::uword num_z){
		num_x_ = num_x; num_y_ = num_y; num_z_ = num_z;
	}

	// set grid spacing
	void ModelArray::set_grid_spacing(
		const fltp dx, const fltp dy, const fltp dz){
		dx_ = dx; dy_ = dy; dz_ = dz;
	}

	// set number of copies in x-direction
	void ModelArray::set_num_x(const arma::uword num_x){
		num_x_ = num_x;
	}

	// set number of copies in y-direction
	void ModelArray::set_num_y(const arma::uword num_y){
		num_y_ = num_y;
	}

	// set number of copies in z-direction
	void ModelArray::set_num_z(const arma::uword num_z){
		num_z_ = num_z;
	}

	// set shift distance in x-direction
	void ModelArray::set_dx(const fltp dx){
		dx_ = dx;
	}

	// set shift distance in y-direction
	void ModelArray::set_dy(const fltp dy){
		dy_ = dy;
	}
	
	// set shift distance in z-direction
	void ModelArray::set_dz(const fltp dz){
		dz_ = dz;
	}

	// set centering
	void ModelArray::set_centered(const bool centered){
		centered_ = centered;
	}

	// set alternation
	void ModelArray::set_alternate(const bool alternate){
		alternate_ = alternate;
	}

	// get number of copies in x-direction
	arma::uword ModelArray::get_num_x()const{
		return num_x_;
	}

	// get number of copies in y-direction
	arma::uword ModelArray::get_num_y()const{
		return num_y_;
	}

	// get number of copies in z-direction
	arma::uword ModelArray::get_num_z()const{
		return num_z_;
	}

	// get shift distance in x-direction
	fltp ModelArray::get_dx()const{
		return dx_;
	}

	// get shift distance in y-direction
	fltp ModelArray::get_dy()const{
		return dy_;
	}

	// get shift distance in z-direction
	fltp ModelArray::get_dz()const{
		return dz_;
	}
	
	// get cent
	bool ModelArray::get_centered()const{
		return centered_;
	}

	// set alternation
	bool ModelArray::get_alternate() const{
		return alternate_;
	}

	// set basemodel
	// void ModelArray::set_base_model(ShModelPr base_model){
	// 	if(base_model==NULL)rat_throw_line("base model points to zero");
	// 	base_model_ = base_model;
	// }

	// get number of coils stored
	// in this model and all child models/coils
	// arma::uword ModelArray::get_num_objects() const{
	// 	return num_x_*num_y_*num_z_*base_model_->get_num_objects();
	// }

	// splitting the modelgroup
	std::list<ShModelPr> ModelArray::split(const ShSerializerPr &slzr) const{
		// check validity
		is_valid(true);

		// create new model list
		std::list<ShModelPr> model_list;

		// walk over models
		arma::uword cnt = 0;
		for(arma::uword i=0;i<num_x_;i++){
			for(arma::uword j=0;j<num_y_;j++){
				for(arma::uword k=0;k<num_z_;k++,cnt++){
					for(auto it=models_.begin();it!=models_.end();it++){
						// create copy
						const ShModelPr model_copy = slzr->copy((*it).second);

						// create a copy of the conductor
						if(conductor_!=NULL)model_copy->set_input_conductor(slzr->copy(conductor_));

						// set tree closed
						model_copy->set_tree_open(false);

						// alternation
						if(alternate_ && (i+j+k)%2==1){
							model_copy->add_transformation(TransReverse::create()); 
							// model_copy->add_flip();
						}

						// calculate offset
						arma::Col<fltp>::fixed<3> offset{i*dx_,j*dy_,k*dz_};

						// apply centering
						if(centered_)offset -= arma::Col<fltp>::fixed<3>{
							(num_x_-1)*dx_/2,(num_y_-1)*dy_/2,(num_z_-1)*dz_/2};

						// rotate and translate
						if(arma::any(offset!=0))
							model_copy->add_transformation(TransTranslate::create(offset));

						// add my transformations
						for(auto it2=trans_.begin();it2!=trans_.end();it2++)
							model_copy->add_transformation(slzr->copy((*it2).second));

						// expand name
						std::stringstream tname;
						tname << "Cl" << std::setfill('0') << 
							std::setw(2) << cnt << "_" << model_copy->get_name();
						model_copy->set_name(tname.str());

						// set color from parent
						if(use_custom_color_)model_copy->set_color(color_, use_custom_color_);

						// get model
						model_list.push_back(model_copy);
					}
				}
			}
		}

		// return list of models
		return model_list;
	}

	// mesh generation
	std::list<ShMeshDataPr> ModelArray::create_meshes_core(
		const std::list<arma::uword> &trace, 
		const MeshSettings &stngs)const{

		// check enabled
		if(!get_enable())return{};

		// check validity
		if(!is_valid(stngs.enable_throws))return{};

		// number of copies
		const arma::uword num_total = num_x_*num_y_*num_z_;

		// walk over all dimensions
		arma::uword cnt = 0;
		arma::Mat<arma::uword> ijk(num_total,3);
		for(arma::uword i=0;i<num_x_;i++){
			for(arma::uword j=0;j<num_y_;j++){
				for(arma::uword k=0;k<num_z_;k++){
					ijk(cnt,0) = i; ijk(cnt,1) = j; ijk(cnt,2) = k; cnt++;
				}
			}
		}

		// sanity check
		assert(cnt==num_total);

		// allocate output list of coil meshes
		arma::field<std::list<ShMeshDataPr> > meshes(num_total);

		// walk over coils again and collect magnet signals
		cmn::parfor(0,cnt,stngs.use_parallel_groups,[&](int idx, int /*cpu*/){
			// get ijk
			const arma::uword i=ijk(idx,0), j=ijk(idx,1), k=ijk(idx,2);

			// create set of base meshes
			meshes(idx) = ModelGroup::create_meshes_core(trace, stngs);

			// walk over sub coils
			for(auto it = meshes(idx).begin();it!=meshes(idx).end();it++){
				// alternation
				if(alternate_ && (i+j+k)%2==1){
					(*it)->apply_transformation(TransReverse::create(),stngs.time); 
					// (*it)->apply_transformation(TransFlip::create(),stngs.time);
				}

				// calculate offset
				arma::Col<fltp>::fixed<3> offset{i*dx_,j*dy_,k*dz_};

				// apply centering
				if(centered_)offset -= arma::Col<fltp>::fixed<3>{
					(num_x_-1)*dx_/2,(num_y_-1)*dy_/2,(num_z_-1)*dz_/2};

				// rotate coil into position
				(*it)->apply_transformation(TransTranslate::create(offset),stngs.time);

				// add name
				(*it)->append_name("n" + std::to_string(idx));
			}
		});

		// flatten array
		std::list<ShMeshDataPr> mesh_list;
		for(arma::uword i=0;i<meshes.n_elem;i++)
			mesh_list.splice(mesh_list.end(), meshes(i));

		// return array
		return mesh_list;
	}

	// vallidity check
	bool ModelArray::is_valid(const bool enable_throws) const{
		if(num_x_<=0){if(enable_throws){rat_throw_line("number of copies in x must be larger than zero");} return false;};
		if(num_y_<=0){if(enable_throws){rat_throw_line("number of copies in y must be larger than zero");} return false;};
		if(num_z_<=0){if(enable_throws){rat_throw_line("number of copies in z must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string ModelArray::get_type(){
		return "rat::mdl::modelarray";
	}

	// method for serialization into json
	void ModelArray::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		// Nameable::serialize(js,list);
		// Transformations::serialize(js,list);
		ModelGroup::serialize(js,list);

		// type
		js["type"] = get_type();

		// settings
		js["centered"] = centered_;
		js["alternate"] = alternate_;

		// subnodes
		js["num_x"] = static_cast<int>(num_x_);
		js["num_y"] = static_cast<int>(num_y_);
		js["num_z"] = static_cast<int>(num_z_);

		// subnodes
		js["dx"] = dx_;
		js["dy"] = dy_;
		js["dz"] = dz_;
	}

	// method for deserialisation from json
	void ModelArray::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent objects
		// Nameable::deserialize(js,list,factory_list,pth);
		// Transformations::deserialize(js,list,factory_list,pth);
		ModelGroup::deserialize(js,list,factory_list,pth);

		// settings
		set_centered(js["centered"].asBool());
		set_alternate(js["alternate"].asBool());
		set_num_x(js["num_x"].asUInt64());
		set_num_y(js["num_y"].asUInt64());
		set_num_z(js["num_z"].asUInt64());
		set_dx(js["dx"].ASFLTP());
		set_dy(js["dy"].ASFLTP());
		set_dz(js["dz"].ASFLTP());
	}

}}