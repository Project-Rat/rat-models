// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelracetrack.hh"

#include "rat/common/error.hh"

#include "crossrectangle.hh"
#include "pathgroup.hh"
#include "pathrectangle.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	ModelRacetrack::ModelRacetrack(){
		set_name("Racetrack");
	}

	// factory
	ShModelRacetrackPr ModelRacetrack::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<ModelRacetrack>();
	}

	// set arc radius
	void ModelRacetrack::set_arc_radius(const fltp arc_radius){
		arc_radius_ = arc_radius;
	}

	// set length
	void ModelRacetrack::set_length(const fltp length){
		length_ = length;
	}

	// set coil thickness
	void ModelRacetrack::set_coil_thickness(const fltp coil_thickness){
		coil_thickness_ = coil_thickness;
	}

	// set coil width
	void ModelRacetrack::set_coil_width(const fltp coil_width){
		coil_width_ = coil_width;
	}

	// set axial element size
	void ModelRacetrack::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	// get arc radius
	fltp ModelRacetrack::get_arc_radius()const{
		return arc_radius_;
	}

	// get length length
	fltp ModelRacetrack::get_length()const{
		return length_;
	}

	// get coil thickness
	fltp ModelRacetrack::get_coil_thickness()const{
		return coil_thickness_;
	}

	// get coil width
	fltp ModelRacetrack::get_coil_width()const{
		return coil_width_;
	}

	// get axial element size
	fltp ModelRacetrack::get_element_size()const {
		return element_size_;
	}

	// get base
	ShPathPr ModelRacetrack::get_input_path() const{
		// check input
		is_valid(true);

		// create circular path
		ShPathRectanglePr racetrack = PathRectangle::create(
			2*arc_radius_, length_, arc_radius_, element_size_);
		racetrack->set_offset(coil_thickness_);

		// return the racetrack
		return racetrack;
	}

	// get cross
	ShCrossPr ModelRacetrack::get_input_cross() const{
		// check input
		is_valid(true);

		// create rectangular cross
		ShCrossRectanglePr rectangle = CrossRectangle::create(
			0, coil_thickness_, -coil_width_/2, coil_width_/2, element_size_, element_size_);

		// return the rectangle
		return rectangle;
	}

	// check input
	bool ModelRacetrack::is_valid(const bool enable_throws) const{
		if(!ModelCoilWrapper::is_valid(enable_throws))return false;
		if(arc_radius_<=0){if(enable_throws){rat_throw_line("arc radius must be larger than zero");} return false;};
		if(length_<=2*arc_radius_){if(enable_throws){rat_throw_line("length must be larger than zero");} return false;};
		if(coil_thickness_<=0){if(enable_throws){rat_throw_line("thickness must be larger than zero");} return false;};
		if(coil_width_<=0){if(enable_throws){rat_throw_line("coil width must be larger than zero");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string ModelRacetrack::get_type(){
		return "rat::mdl::modelracetrack";
	}

	// method for serialization into json
	void ModelRacetrack::serialize(
		Json::Value &js, cmn::SList &list) const{

		// parent
		ModelCoilWrapper::serialize(js,list);

		// set type
		js["type"] = get_type();

		// set properties
		js["arc_radius"] = arc_radius_;
		js["length"] = length_;
		js["coil_thickness"] = coil_thickness_;
		js["coil_width"] = coil_width_;
		js["element_size"] = element_size_;
	}

	// method for deserialisation from json
	void ModelRacetrack::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		ModelCoilWrapper::deserialize(js,list,factory_list,pth);

		// set properties
		arc_radius_ = js["arc_radius"].ASFLTP();
		length_ = js["length"].ASFLTP();
		coil_thickness_ = js["coil_thickness"].ASFLTP();
		coil_width_ = js["coil_width"].ASFLTP();
		element_size_ = js["element_size"].ASFLTP();
	}

}}