// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "driveptrain.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	DrivePTrain::DrivePTrain(){

	}

	// constructor
	DrivePTrain::DrivePTrain(
		const arma::uword num_pulses,
		const fltp amplitude, const fltp increment,
		const fltp tpulse, const fltp tstart, 
		const fltp tdelay){
		set_num_pulses(num_pulses);
		set_amplitude(amplitude); set_increment(increment); 
		set_tstart(tstart); set_tdelay(tdelay); 
		set_tpulse(tpulse);
		set_name("Pulsed Train");
	}

	// factory
	ShDrivePTrainPr DrivePTrain::create(){
		return std::make_shared<DrivePTrain>();
	}

	// factory
	ShDrivePTrainPr DrivePTrain::create(
		const arma::uword num_pulses,
		const fltp amplitude, const fltp increment,
		const fltp tpulse, const fltp tstart, 
		const fltp tdelay){
		return std::make_shared<DrivePTrain>(num_pulses, amplitude, increment, tpulse, tstart, tdelay);
	}


	// setters
	void DrivePTrain::set_amplitude(const fltp amplitude){
		amplitude_ = amplitude;
	}

	void DrivePTrain::set_tstart(const fltp tstart){
		tstart_ = tstart;
	}

	void DrivePTrain::set_tpulse(const fltp tpulse){
		tpulse_ = tpulse;
	}

	void DrivePTrain::set_tdelay(const fltp tdelay){
		tdelay_ = tdelay;
	}

	void DrivePTrain::set_increment(const fltp increment){
		increment_ = increment;
	}

	void DrivePTrain::set_num_pulses(const arma::uword num_pulses){
		num_pulses_ = num_pulses;
	}

	// get scaling
	fltp DrivePTrain::get_scaling(
		const fltp position,
		const fltp /*time*/,
		const arma::uword derivative) const{
		if(derivative==0){
			const arma::Mat<fltp> ti = arma::reshape(get_inflection_points(), 2, num_pulses_);
			assert(arma::all(ti.row(1)>ti.row(0)));
			fltp scale = RAT_CONST(0.0); 
			arma::Row<arma::uword> idx = arma::find(position>ti.row(0) && position<=ti.row(1)).t();
			if(!idx.is_empty()){
				assert(idx.n_elem==1);
				scale = amplitude_ + arma::as_scalar(idx)*increment_;
			}
			return scale;
		}
		return RAT_CONST(0.0);
	}

	// get times at which an inflection occurs
	arma::Col<fltp> DrivePTrain::get_inflection_points()const{
		// gather times
		arma::Col<fltp> tinflection(2*num_pulses_);
		for(arma::uword i=0;i<num_pulses_;i++){
			tinflection(2*i) = tstart_ + i*(tdelay_+tpulse_);
			tinflection(2*i+1) = tstart_ + i*(tdelay_+tpulse_) + tpulse_;
		}

		// return times
		return tinflection;
	}

	// apply scaling for the input settings
	void DrivePTrain::rescale(const fltp scale_factor){
		amplitude_ *= scale_factor;
		increment_ *= scale_factor;
	}

	// get type
	std::string DrivePTrain::get_type(){
		return "rat::mdl::driveptrain";
	}

	// method for serialization into json
	void DrivePTrain::serialize(
		Json::Value &js, 
		cmn::SList &list) const{
		Drive::serialize(js,list);
		js["type"] = get_type();
		js["amplitude"] = amplitude_;
		js["tstart"] = tstart_;
		js["tpulse"] = tpulse_;
		js["tdelay"] = tdelay_;
		js["increment"] = increment_;
		js["num_pulses"] = static_cast<int>(num_pulses_);
	}

	// method for deserialisation from json
	void DrivePTrain::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		Drive::deserialize(js,list,factory_list,pth);
		set_amplitude(js["amplitude"].ASFLTP());
		set_tstart(js["tstart"].ASFLTP());
		set_tpulse(js["tpulse"].ASFLTP());
		set_tdelay(js["tdelay"].ASFLTP());
		set_increment(js["increment"].ASFLTP());
		set_num_pulses(js["num_pulses"].asUInt64());
	}

}}