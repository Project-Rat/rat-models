// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "crosscircle.hh"

#include "rat/common/error.hh"
#include "rat/common/extra.hh"
#include "rat/common/elements.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructors
	CrossCircle::CrossCircle(){
		set_name("Circle");
		set_radius(4e-3);
		set_element_size(1e-3);
	}

	// constructors
	CrossCircle::CrossCircle(const fltp radius, const fltp dl) : CrossCircle(){
		set_radius(radius);
		set_element_size(dl);
		set_center(0,0);
	}

	// constructors
	CrossCircle::CrossCircle(const fltp uc, const fltp vc, const fltp radius, const fltp dl) : CrossCircle(){
		set_radius(radius); 
		set_element_size(dl);
		set_center(uc,vc);
	}

	// factory methods
	ShCrossCirclePr CrossCircle::create(){
		return std::make_shared<CrossCircle>();
	}

	// factory methods
	ShCrossCirclePr CrossCircle::create(const fltp radius, const fltp dl){
		return std::make_shared<CrossCircle>(radius,dl);
	}

	// factory methods
	ShCrossCirclePr CrossCircle::create(
		const fltp uc, const fltp vc,
		const fltp radius, const fltp dl){
		return std::make_shared<CrossCircle>(uc,vc,radius,dl);
	}

	// set radius
	void CrossCircle::set_radius(const fltp radius){
		radius_ = radius;
	}

	// set element size
	void CrossCircle::set_element_size(const fltp dl){
		dl_ = dl;
	}

	// set center position u and v
	void CrossCircle::set_center(const fltp nc, const fltp tc){
		set_nc(nc); set_tc(tc);
	}

	// normal center coordinate
	void CrossCircle::set_nc(const fltp nc){
		nc_ = nc;
	}

	// transverse center coordinate
	void CrossCircle::set_tc(const fltp tc){
		tc_ = tc;
	}

	// set radius
	void CrossCircle::set_radial_vectors(const bool radial_vectors){
		radial_vectors_ = radial_vectors;
	}

	// set no core feature
	void CrossCircle::set_enable_core(const bool enable_core){
		enable_core_ = enable_core;
	}

	// set quadrant one enabled
	void CrossCircle::set_enable_quadrant1(const bool enable_quadrant1){
		enable_quadrant1_ = enable_quadrant1;
	}

	// set quadrant two enabled
	void CrossCircle::set_enable_quadrant2(const bool enable_quadrant2){
		enable_quadrant2_ = enable_quadrant2;
	}

	// set quadrant three enabled
	void CrossCircle::set_enable_quadrant3(const bool enable_quadrant3){
		enable_quadrant3_ = enable_quadrant3;
	}

	// set quadrant four enabled
	void CrossCircle::set_enable_quadrant4(const bool enable_quadrant4){
		enable_quadrant4_ = enable_quadrant4;
	}

	// enable quadrants
	void CrossCircle::set_enable_quadrant(
		const bool enable_quadrant1, 
		const bool enable_quadrant2, 
		const bool enable_quadrant3, 
		const bool enable_quadrant4,
		const bool enable_core){
		set_enable_quadrant1(enable_quadrant1);
		set_enable_quadrant2(enable_quadrant2);
		set_enable_quadrant3(enable_quadrant3);
		set_enable_quadrant4(enable_quadrant4);
		set_enable_core(enable_core);
	}


	// set core width
	void CrossCircle::set_core_width(const fltp core_width){
		core_width_ = core_width;
	}

	// set core height
	void CrossCircle::set_core_height(const fltp core_height){
		core_height_ = core_height;
	}

	// get normal coordinate of center
	fltp CrossCircle::get_nc() const{
		return nc_;
	}

	// get transverse coordinate of center
	fltp CrossCircle::get_tc() const{
		return tc_;
	}

	// get circle radius
	fltp CrossCircle::get_radius() const{
		return radius_;
	}

	// get element size
	fltp CrossCircle::get_element_size() const{
		return dl_;
	}

	// get radial vectors
	bool CrossCircle::get_radial_vectors() const{
		return radial_vectors_;
	}

	// get no core feature
	bool CrossCircle::get_enable_core()const{
		return enable_core_;
	}

	// get quadrant one enabled
	bool CrossCircle::get_enable_quadrant1()const{
		return enable_quadrant1_;
	}

	// get quadrant two enabled
	bool CrossCircle::get_enable_quadrant2()const{
		return enable_quadrant2_;
	}

	// get quadrant three enabled
	bool CrossCircle::get_enable_quadrant3()const{
		return enable_quadrant3_;
	}

	// get quadrant four enabled
	bool CrossCircle::get_enable_quadrant4()const{
		return enable_quadrant4_;
	}

	// get core width
	fltp CrossCircle::get_core_width()const{
		return core_width_;
	}

	// get core height
	fltp CrossCircle::get_core_height()const{
		return core_height_;
	}

	// check if any part of the mesh is enabled
	bool CrossCircle::any_enabled()const{
		return enable_core_ || enable_quadrant1_ || enable_quadrant2_ || enable_quadrant3_ || enable_quadrant4_;
	}



	// get bounding box
	arma::Mat<fltp>::fixed<2,2> CrossCircle::get_bounding() const{
		arma::Mat<fltp>::fixed<2,2> bnd;
		bnd.col(0) = arma::Col<fltp>::fixed<2>{nc_-radius_, nc_+radius_};
		bnd.col(1) = arma::Col<fltp>::fixed<2>{tc_-radius_, tc_+radius_};
		return bnd;
	}

	// create area data object
	ShAreaPr CrossCircle::create_area(const MeshSettings &stngs) const{
		// check validity
		is_valid(true);

		// calculate size of central rectangle
		const fltp drect = 2*radius_/3;
		const fltp core_width = core_width_==RAT_CONST(0.0) ? drect : core_width_;
		const fltp core_height = core_height_==RAT_CONST(0.0) ? drect : core_height_;

		// calculate number of elements
		arma::uword nrect = std::max(2llu,static_cast<arma::uword>(2*std::ceil(arma::Datum<fltp>::tau*radius_/(8*dl_))));
		arma::uword nrad = std::max(1llu,static_cast<arma::uword>(std::ceil(std::abs(radius_ - std::sqrt(2)*std::min(core_width,core_height)/2)/dl_)));

		// limit number of elements 
		// when low poly requested
		if(stngs.low_poly){
			const fltp dl = 2*arma::Datum<fltp>::pi*radius_/32;
			nrect = std::min(nrect, std::max(2llu,static_cast<arma::uword>(std::ceil(2*arma::Datum<fltp>::pi*radius_/(4*dl)))));
			nrad = std::min(nrad, std::max(2llu,static_cast<arma::uword>(std::ceil((radius_-std::sqrt(2.0)*drect/2)/dl))));
		}

		// create rectangle
		// allocate coordinates
		arma::Mat<fltp> ur(nrect+1,nrect+1);
		arma::Mat<fltp> vr(nrect+1,nrect+1);

		// set values
		ur.each_row() = arma::linspace<arma::Row<fltp> >(-core_height/2,core_height/2,nrect+1);
		vr.each_col() = arma::linspace<arma::Col<fltp> >(-core_width/2,core_width/2,nrect+1);

		// allocate 
		arma::Mat<fltp> ucc(nrad+1,nrect*4);
		arma::Mat<fltp> vcc(nrad+1,nrect*4);

		// get sides
		const arma::field<arma::Col<fltp> > urp{ur.col(nrect), arma::flipud(ur.row(nrect).t()), arma::flipud(ur.col(0)), ur.row(0).t()};
		const arma::field<arma::Col<fltp> > vrp{vr.col(nrect), arma::flipud(vr.row(nrect).t()), arma::flipud(vr.col(0)), vr.row(0).t()};
		
		// array for radial coordinates
		const arma::Row<fltp> tt = arma::linspace<arma::Row<fltp> >(RAT_CONST(0.0),RAT_CONST(1.0),nrad+1);

		// draw quarter circle in cylindrical coordinates
		const arma::Col<fltp> theta = arma::linspace<arma::Col<fltp> >(-arma::Datum<fltp>::pi/4, arma::Datum<fltp>::pi/4,nrect+1);
		const arma::Col<fltp> rho = arma::linspace<arma::Col<fltp> >(radius_,radius_,nrect+1);

		// walk over quadrants
		for(arma::uword j=0;j<4;j++){
			// convert to cartesian coordinates
			const arma::Col<fltp> uc = rho%arma::cos(theta + j*arma::Datum<fltp>::pi/2); 
			const arma::Col<fltp> vc = rho%arma::sin(theta + j*arma::Datum<fltp>::pi/2);

			// build right quadrant
			arma::Mat<fltp> u1(nrad+1,nrect+1);
			arma::Mat<fltp> v1(nrad+1,nrect+1);

			// create radial spoke
			for(arma::uword i=0;i<nrad+1;i++){
				u1.row(i) = ((RAT_CONST(1.0)-tt(i))*urp(j) + tt(i)*uc).t();
				v1.row(i) = ((RAT_CONST(1.0)-tt(i))*vrp(j) + tt(i)*vc).t();
			}

			// store
			ucc.cols(j*nrect, (j+1)*nrect-1) = u1.cols(0,nrect-1);
			vcc.cols(j*nrect, (j+1)*nrect-1) = v1.cols(0,nrect-1);
		}

		// count number of nodes
		const arma::uword num_nodes_circle = (nrad+1)*nrect*4;

		// create matrix of node indices for the circular area
		arma::Mat<arma::uword> node_idx_circle = 
			arma::regspace<arma::Mat<arma::uword> >(0,num_nodes_circle-1);
		node_idx_circle.reshape(nrad+1,4*nrect);

		// connect start to end
		node_idx_circle = arma::join_horiz(node_idx_circle,node_idx_circle.col(0));

		// number of elements
		const arma::uword num_elements_circle = nrad*nrect*4;

		// create coordinates
		arma::Mat<fltp> Rc(2,num_nodes_circle);
		Rc.row(0) = arma::reshape(ucc,1,num_nodes_circle);
		Rc.row(1) = arma::reshape(vcc,1,num_nodes_circle);

		// generate internal element node indexes
		arma::Mat<arma::uword> nc(4,num_elements_circle);
		nc.row(0) = arma::reshape(node_idx_circle.submat(0, 0, nrad-1, 4*nrect-1), 1, num_elements_circle);
		nc.row(1) = arma::reshape(node_idx_circle.submat(1, 0, nrad, 4*nrect-1), 1, num_elements_circle);
		nc.row(2) = arma::reshape(node_idx_circle.submat(1, 1, nrad, 4*nrect), 1, num_elements_circle);
		nc.row(3) = arma::reshape(node_idx_circle.submat(0, 1, nrad-1, 4*nrect), 1, num_elements_circle);

		// count number of nodes
		const arma::uword num_nodes_rectangle = (nrect-1)*(nrect-1);

		// now create rectangle (only select central nodes as edges are part of circle)
		arma::Mat<fltp> Rr(2,num_nodes_rectangle);
		Rr.row(0) = arma::reshape(ur.submat(1,1,nrect-1,nrect-1),1,num_nodes_rectangle);
		Rr.row(1) = arma::reshape(vr.submat(1,1,nrect-1,nrect-1),1,num_nodes_rectangle);

		// create matrix of node indices for the rectangular area
		arma::Mat<arma::uword> node_idx_rect = num_nodes_circle + 
			arma::regspace<arma::Mat<arma::uword> >(0,(nrect-1)*(nrect-1)-1);
		node_idx_rect.reshape(nrect-1,nrect-1);

		// expand rectangle
		arma::Mat<arma::uword> node_idx_rect_ext(nrect+1,nrect+1,arma::fill::zeros);
		node_idx_rect_ext.submat(1,1,nrect-1,nrect-1) = node_idx_rect;
		
		// connect edges
		node_idx_rect_ext.col(nrect) = node_idx_circle.submat(0,0,0,nrect).t();
		node_idx_rect_ext.row(nrect) = arma::fliplr(node_idx_circle.submat(0,1*nrect,0,2*nrect));
		node_idx_rect_ext.col(0) = arma::flipud(node_idx_circle.submat(0,2*nrect,0,3*nrect).t());
		node_idx_rect_ext.row(0) = node_idx_circle.submat(0,3*nrect,0,4*nrect);
		
		// calculate number of elements in rectangle
		arma::uword num_elements_rectangle = nrect*nrect;

		// generate internal element node indexes
		arma::Mat<arma::uword> nr(4,num_elements_rectangle);
		nr.row(0) = arma::reshape(node_idx_rect_ext.submat(0, 0, nrect-1, nrect-1), 1, num_elements_rectangle);
		nr.row(1) = arma::reshape(node_idx_rect_ext.submat(1, 0, nrect, nrect-1), 1, num_elements_rectangle);
		nr.row(2) = arma::reshape(node_idx_rect_ext.submat(1, 1, nrect, nrect), 1, num_elements_rectangle);
		nr.row(3) = arma::reshape(node_idx_rect_ext.submat(0, 1, nrect-1, nrect), 1, num_elements_rectangle);

		// select quadrants
		const arma::Row<arma::uword> quadrant_enabled{enable_quadrant1_,enable_quadrant2_,enable_quadrant3_,enable_quadrant4_};
		arma::Row<arma::uword> idx = arma::vectorise(arma::repmat(arma::regspace<arma::Row<arma::uword> >(0,3),nrect*nrad,1)).t();
		if(radius_<core_width_/2)idx = arma::shift(idx,nrect*nrad/2);
		nc = nc.cols(arma::find(cmn::Extra::ismember(idx, arma::find(quadrant_enabled).t())));

		// connect meshes
		arma::Mat<arma::uword> n = enable_core_ ? arma::join_horiz(nc,arma::flipud(nr)) : nc;
		arma::Mat<fltp> Rn = enable_core_ ? arma::join_horiz(Rc,Rr) : Rc;
		
		// drop disconnected nodes
		arma::Row<arma::uword> cnt(Rn.n_cols, arma::fill::zeros);
		for(arma::uword i=0;i<n.n_elem;i++)cnt(n(i))++;
		const arma::Row<arma::uword> node_reindex = arma::join_horiz(arma::Row<arma::uword>{0},arma::cumsum(cnt>0));
		n = arma::reshape(node_reindex(arma::vectorise(n)), n.n_rows, n.n_cols);
		Rn.shed_cols(arma::find(cnt==0));

		// get orientation
		arma::Mat<fltp> N, D; 
		if(radial_vectors_){
			// calculate radial vectors
			const arma::Row<fltp> rho = cmn::Extra::vec_norm(Rn);
			N = Rn.each_row()/rho;
			D = arma::flipud(Rn).eval().each_row()/cmn::Extra::vec_norm(Rn);
			D.row(0) *= -1;

			// center point
			const arma::Col<arma::uword> center_point_idx = arma::find(rho<RAT_CONST(1e-14));
			for(arma::uword i=0;i<center_point_idx.n_elem;i++){
				N.col(center_point_idx(i)) = arma::Col<fltp>::fixed<2>{1,0};
				D.col(center_point_idx(i)) = arma::Col<fltp>::fixed<2>{0,1};
			}
		}else{
			N.zeros(2,Rn.n_cols); N.row(0).fill(RAT_CONST(1.0));
			D.zeros(2,Rn.n_cols); D.row(1).fill(RAT_CONST(1.0));
		}

		// reverse cw vs ccw
		if(radius_>core_width_/2)n = arma::flipud(n);

		// only perform scaling when using a circle
		if(arma::all(quadrant_enabled) && enable_core_){
			// scale to area = pi*radius^2
			const fltp scale_factor = std::sqrt(arma::Datum<fltp>::pi*radius_*radius_/Area::create(Rn,N,D,n)->get_area());
			
			// apply scale factor
			Rn *= scale_factor;
		}

		// apply center position offset
		Rn.row(0) += nc_; Rn.row(1) += tc_;
		
		// count total number of nodes and elements
		// const arma::uword num_nodes = num_nodes_rectangle + num_nodes_circle;
		// const arma::uword num_elements = num_elements_rectangle + num_elements_circle;

		// circle area
		const ShAreaPr circle_area = Area::create(Rn,N,D,n);

		// set center coord
		if(arma::accu(quadrant_enabled)==1 && enable_core_==false){
			const fltp center_radius = ((enable_quadrant1_ || enable_quadrant3_ ? core_width_/2 : core_height_/2) + radius_)/2;
			const fltp center_angle = arma::as_scalar(arma::find(quadrant_enabled))*arma::Datum<fltp>::pi/2;
			if(radius_>core_width_/2){
				circle_area->set_center_coord({center_radius*std::cos(center_angle),center_radius*std::sin(center_angle)});
			}else{
				circle_area->set_center_coord({std::sqrt(2.0)*center_radius*std::cos(center_angle + arma::Datum<fltp>::pi/4),std::sqrt(2.0)*center_radius*std::sin(center_angle + arma::Datum<fltp>::pi/4)});
			}
		}else{
			circle_area->set_center_coord({nc_,tc_});
		}

		// std::cout<<circle_area->get_center_coord()<<std::endl;

		// setup
		circle_area->setup_perimeter();
		circle_area->setup_corner_nodes();

		// mesh smoothing
		circle_area->smoothen(num_smooth_iter_,smooth_dampen_);

		// // set surface mesh
		// circle_area->set_perimeter();
		// circle_area->set_internal();
		// circle_area->set_corner_nodes({});

		// check areas and periphery within percent of perfect circle
		// assert(std::abs(carea->calculate_area()-arma::Datum<fltp>::pi*radius_*radius_)/(arma::Datum<fltp>::pi*radius_*radius_)<5e-2);
		// assert(std::abs(carea->calculate_periphery()-2*arma::Datum<fltp>::pi*radius_)/(2*arma::Datum<fltp>::pi*radius_)<5e-2);

		// return positive
		return circle_area;
	}

	// // surface mesh
	// ShPerimeterPr CrossCircle::create_perimeter() const{
	// 	// create area mesh
	// 	ShAreaPr area_mesh = create_area(MeshSettings());

	// 	// extract periphery and return
	// 	return area_mesh->extract_perimeter();
	// }

	// validity check
	bool CrossCircle::is_valid(const bool enable_throws) const{
		if(radius_<=0){if(enable_throws){rat_throw_line("radius must be larger than zero");} return false;};
		if(dl_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		if(dl_<=RAT_CONST(1e-2)*radius_){if(enable_throws){rat_throw_line("element size too small");} return false;};
		if(!any_enabled()){if(enable_throws){rat_throw_line("all parts of the mesh are disabled");} return false;};
		if(core_width_/2>2*radius_ && enable_core_){if(enable_throws){rat_throw_line("half core width can not exceed radius when core is enabled");} return false;};
		if(radius_>core_width_/2 && radius_<core_height_/2){if(enable_throws){rat_throw_line("half core width smaller than radius but half core height larger than radius");} return false;};
		if(radius_<core_width_/2 && radius_>core_height_/2){if(enable_throws){rat_throw_line("half core width larger than radius but half core height smaller than radius");} return false;};
		return true;
	}

	// get type
	std::string CrossCircle::get_type(){
		return "rat::mdl::crosscircle";
	}

	// method for serialization into json
	void CrossCircle::serialize(Json::Value &js, cmn::SList &list) const{
		Cross::serialize(js,list);
		js["type"] = get_type();
		js["radial_vectors"] = radial_vectors_;
		js["radius"] = radius_;
		js["nc"] = nc_; js["tc"] = tc_; js["dl"] = dl_;
		js["core_width"] = core_width_;
		js["core_height"] = core_height_;
		js["enable_core"] = enable_core_;
		js["enable_quadrant1"] = enable_quadrant1_;
		js["enable_quadrant2"] = enable_quadrant2_;
		js["enable_quadrant3"] = enable_quadrant3_;
		js["enable_quadrant4"] = enable_quadrant4_;
	}

	// method for deserialisation from json
	void CrossCircle::deserialize(const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		Cross::deserialize(js,list,factory_list,pth);
		set_radius(js["radius"].ASFLTP());
		set_center(js["nc"].ASFLTP(),js["tc"].ASFLTP());
		set_element_size(js["dl"].ASFLTP());
		set_radial_vectors(js["radial_vectors"].asBool());
		set_core_width(js["core_width"].ASFLTP());
		set_core_height(js["core_height"].ASFLTP());
		if(js.isMember("enable_core"))set_enable_core(js["enable_core"].asBool());
		if(js.isMember("enable_quadrant1"))set_enable_quadrant1(js["enable_quadrant1"].asBool());
		if(js.isMember("enable_quadrant2"))set_enable_quadrant2(js["enable_quadrant2"].asBool());
		if(js.isMember("enable_quadrant3"))set_enable_quadrant3(js["enable_quadrant3"].asBool());
		if(js.isMember("enable_quadrant4"))set_enable_quadrant4(js["enable_quadrant4"].asBool());
	}

}}