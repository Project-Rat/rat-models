// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathclover.hh"

// rat-common headers
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathClover::PathClover(){
		set_name("Cloverleaf");
	}

	// constructor with dimension input
	PathClover::PathClover(
		const fltp ellstr1, const fltp ellstr2, 
		const fltp height, const fltp dpack, const fltp ell_trans, 
		const fltp str12, const fltp str34, const fltp element_size) : PathClover(){

		// set to self
		set_ellstr1(ellstr1); set_ellstr2(ellstr2);
		set_height(height); set_dpack(dpack); set_ell_trans(ell_trans);
		set_str12(str12); set_str34(str34); set_element_size(element_size);
	}

	// factory
	ShPathCloverPr PathClover::create(){
		//return ShPathCloverPr(new PathClover);
		return std::make_shared<PathClover>();
	}

	// factory with dimension input
	ShPathCloverPr PathClover::create(
		const fltp ellstr1, const fltp ellstr2, 
		const fltp height, const fltp dpack, const fltp ell_trans, 
		const fltp str12, const fltp str34, const fltp element_size){
		return std::make_shared<PathClover>(
			ellstr1,ellstr2,height,dpack,
			ell_trans,str12,str34,element_size);
	}

	// set planar winding feature
	void PathClover::set_planar_winding(const bool planar_winding){
		planar_winding_ = planar_winding;
	}

	// set first length
	void PathClover::set_ellstr1(const fltp ellstr1){
		ellstr1_ = ellstr1;
	}

	// set second length
	void PathClover::set_ellstr2(const fltp ellstr2){
		ellstr2_ = ellstr2;
	}

	// set bridge height
	void PathClover::set_height(const fltp height){
		height_ = height;
	}

	// set spacing needed for winding pack
	void PathClover::set_dpack(const fltp dpack){
		dpack_ = dpack;
	}

	// set spacing needed for winding pack
	void PathClover::set_ell_trans(const fltp ell_trans){
		ell_trans_ = ell_trans;
	}

	// set first and second strength
	void PathClover::set_str12(const fltp str12){
		str12_ = str12;
	}

	void PathClover::set_str34(const fltp str34){
		str34_ = str34;
	}

	// set element size 
	void PathClover::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	// set bending radius
	void PathClover::set_bending_radius(const fltp bending_radius){
		bending_radius_ = bending_radius;
	}

	// set bridge angle
	void PathClover::set_bridge_angle(const fltp bridge_angle){
		bridge_angle_ = bridge_angle;
	}


	// set planar winding feature
	bool PathClover::get_planar_winding() const{
		return planar_winding_;
	}

	// get first straight section length
	fltp PathClover::get_ellstr1() const{
		return ellstr1_;
	}

	// get second straight section length
	fltp PathClover::get_ellstr2() const{
		return ellstr2_;
	}

	// set bridge height
	fltp PathClover::get_height() const{
		return height_;
	}

	// set spacing needed for winding pack
	fltp PathClover::get_dpack() const{
		return dpack_;
	}

	// set spacing needed for winding pack
	fltp PathClover::get_ell_trans() const{
		return ell_trans_;
	}

	// set first and second strength
	fltp PathClover::get_str12() const{
		return str12_;
	}

	fltp PathClover::get_str34() const{
		return str34_;
	}

	// set element size 
	fltp PathClover::get_element_size() const{
		return element_size_;
	}

	// set bending radius
	fltp PathClover::get_bending_radius() const{
		return bending_radius_;
	}

	// set bridge angle
	fltp PathClover::get_bridge_angle() const{
		return bridge_angle_;
	}

	// get frame
	ShFramePr PathClover::create_frame(const MeshSettings &stngs) const{
		// check input
		is_valid(true);

		// create unified path
		const ShPathGroupPr pathgroup = PathGroup::create(
			arma::Col<fltp>::fixed<3>{ellstr2_/2,0,0},
			cmn::Extra::unit_vec('y'),
			cmn::Extra::unit_vec('x'));

		// start and end vectors for cloverleaf end
		const arma::Col<fltp>::fixed<3> R0 = {0,-ell_trans_+dpack_,0};
		const arma::Col<fltp>::fixed<3> L0 = cmn::Extra::unit_vec('y');
		const arma::Col<fltp>::fixed<3> N0 = cmn::Extra::unit_vec('x');
		const arma::Col<fltp>::fixed<3> R1 = {-(ell_trans_+dpack_)*std::cos(bridge_angle_),-(ell_trans_+dpack_)*std::sin(bridge_angle_),height_};
		const arma::Col<fltp>::fixed<3> L1 = {-std::cos(bridge_angle_),-std::sin(bridge_angle_),0};
		// const arma::Col<fltp>::fixed<3> N1 = {std::sin(bridge_angle_),std::cos(bridge_angle_),0};
		const arma::Col<fltp>::fixed<3> N1 = {-std::sin(bridge_angle_),std::cos(bridge_angle_),0};

		// strengths
		const fltp str1 = str12_; const fltp str2 = str12_;
		const fltp str3 = str34_; const fltp str4 = str34_;

		// control points in UV plane
		const arma::Mat<fltp> Puv1 = arma::reshape(arma::Col<fltp>{
			0.0,0.0, 1*str1,0, 2*str1,0, 3*str1,0, 4*str1,1*str3, 
			5*str1,2*str3, 6*str1,4*str3, 6*str1,10*str3},2,8);
		const arma::Mat<fltp> Puv2 = arma::reshape(arma::Col<fltp>{
			0.0,0.0, 1*str2,0, 2*str2,0, 3*str2,0, 4*str2,1*str4, 
			5*str2,2*str4, 6*str2,4*str4, 6*str2,10*str4},2,8);

		// create quintic bezier spline with constant perimeter
		const ShPathBezierPr path_clover1 = PathBezier::create(
			R0,L0,N0, R1,L1,N1, Puv1,Puv2, ell_trans_,ell_trans_,element_size_,dpack_);
		path_clover1->set_planar_winding(planar_winding_);
		path_clover1->set_num_sections(num_sections_);
		path_clover1->set_analytic_frame(true);

		// create quintic bezier spline with constant perimeter
		// ShPathBezierPr path_clover2 = PathBezier::create(
		// 	R1,-L1,-N1, -R0,L0,N0, str1,str2,str3,str4, ell_trans_,ell_trans_,element_size_,0);
		// path_clover2->set_planar_winding(true);
		const ShPathGroupPr path_clover2 = PathGroup::create();
		path_clover2->add_path(path_clover1);
		path_clover2->add_reflect_yz();
		path_clover2->add_reverse();
		//path_clover2->add_flip();

		// calculate clover straight section length
		const fltp ellstr1 = ellstr1_/2 + dpack_ - ell_trans_;
		const fltp ellstr2 = ellstr2_/2 + dpack_ - ell_trans_;

		// allocate
		ShPathPr path_straight1A, path_straight1B, path_straight2;
		
		// create straight section
		if(bending_radius_==0){
			path_straight1A = PathStraight::create(ellstr1, element_size_);
			path_straight1B = path_straight1A;
		}

		// when straight section is curved
		else{
			const fltp arc_length = ellstr1/bending_radius_;
			path_straight1A = PathArc::create(bending_radius_-ellstr2_/2, -arc_length, element_size_);
			path_straight1B = PathArc::create(bending_radius_+ellstr2_/2, arc_length, element_size_);
		}

		// create bridged section
		if(bridge_angle_==0){
			path_straight2 = PathStraight::create(ellstr2, element_size_);
		}

		// 
		else{
			path_straight2 = PathArc::create(ellstr2/std::sin(bridge_angle_), -bridge_angle_, element_size_);
		}

		// add sections to the path group
		pathgroup->add_path(path_straight1A); 
		pathgroup->add_path(path_clover1); 
		pathgroup->add_path(path_straight2);
		pathgroup->add_path(path_straight2); 
		pathgroup->add_path(path_clover2); 
		pathgroup->add_path(path_straight1B);
		pathgroup->add_path(path_straight1B); 
		pathgroup->add_path(path_clover1); 
		pathgroup->add_path(path_straight2);
		pathgroup->add_path(path_straight2); 
		pathgroup->add_path(path_clover2); 
		pathgroup->add_path(path_straight1A);

		// move path
		//pathgroup->add_translation(ellstr2_/2,0,0);

		// create frame
		const ShFramePr frame = pathgroup->create_frame(stngs);

		// apply transformations
		frame->apply_transformations(get_transformations(), stngs.time);

		// return frame
		return frame;
	}

	// validity check
	bool PathClover::is_valid(const bool enable_throws) const{
		if(ellstr1_<=0){if(enable_throws){rat_throw_line("first straight section length must be larger than zero");} return false;};
		if(ellstr2_<=0){if(enable_throws){rat_throw_line("second straight section length must be larger than zero");} return false;};
		if(dpack_<0){if(enable_throws){rat_throw_line("pack thickness must be larger than or equal to zero");} return false;};
		if(ell_trans_<0){if(enable_throws){rat_throw_line("transition length must be larger than or equal to zero");} return false;};
		if(str12_<=0){if(enable_throws){rat_throw_line("first strength must be larger then zero");} return false;};
		if(str34_<=0){if(enable_throws){rat_throw_line("second strength must be larger then zero");} return false;};
		if(bending_radius_-ellstr2_/2<=0 && bending_radius_!=0){if(enable_throws){rat_throw_line("banding radius is too small");} return false;};
		return true;
	}

	// get type
	std::string PathClover::get_type(){
		return "rat::mdl::pathclover";
	}

	// method for serialization into json
	void PathClover::serialize(
		Json::Value &js, 
		cmn::SList &list) const{
		Transformations::serialize(js,list);
		js["type"] = get_type();
		js["ell_trans"] = ell_trans_;
		js["dpack"] = dpack_;
		js["height"] = height_; 
		js["str12"] = str12_; 
		js["str34"] = str34_; 
		js["element_size"] = element_size_; 
		js["ellstr1"] = ellstr1_; 
		js["ellstr2"] = ellstr2_; 
		js["bending_radius"] = bending_radius_; 
		js["bridge_angle"] = bridge_angle_;
	}

	// method for deserialisation from json
	void PathClover::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		Transformations::deserialize(js,list,factory_list,pth);
		set_ell_trans(js["ell_trans"].ASFLTP());
		set_dpack(js["dpack"].ASFLTP());
		set_height(js["height"].ASFLTP()); 
		set_str12(js["str12"].ASFLTP()); 
		set_str34(js["str34"].ASFLTP()); 
		set_element_size(js["element_size"].ASFLTP()); 
		set_ellstr1(js["ellstr1"].ASFLTP());
		set_ellstr2(js["ellstr2"].ASFLTP()); 
		set_bending_radius(js["bending_radius"].ASFLTP()); 
		set_bridge_angle(js["bridge_angle"].ASFLTP());
	}

}}
