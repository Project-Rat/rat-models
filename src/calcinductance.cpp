// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calcinductance.hh"

// mlfmm headers
#include "rat/mlfmm/mlfmm.hh"
#include "rat/mlfmm/multisources.hh"
#include "rat/mlfmm/multitargets2.hh"

// nonlinear solver headers
#ifdef ENABLE_NL_SOLVER
#include "rat/nl/nonlinsolver.hh"
#endif

// model headers
#include "nldata.hh"
#include "coildata.hh"
#include "bardata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcInductance::CalcInductance(){
		set_name("Inductance");
	}

	// constructor
	CalcInductance::CalcInductance(const ShModelPr &model) : CalcInductance(){
		set_model(model);
	}

	// factory
	ShCalcInductancePr CalcInductance::create(){
		return std::make_shared<CalcInductance>();
	}

	// factory
	ShCalcInductancePr CalcInductance::create(const ShModelPr &model){
		return std::make_shared<CalcInductance>(model);
	}

	// set parallel calculation
	void CalcInductance::set_use_parallel(const bool use_parallel){
		use_parallel_ = use_parallel;
	}

	// set group circuits
	void CalcInductance::set_group_circuits(const bool group_circuits){
		group_circuits_ = group_circuits;
	}
	
	// get group circuits
	bool CalcInductance::get_group_circuits() const{
		return group_circuits_;
	}

	// use only linear
	void CalcInductance::set_use_only_linear(const bool use_only_linear){
		use_only_linear_ = use_only_linear;
	}

	// use only linear
	bool CalcInductance::get_use_only_linear()const{
		return use_only_linear_;
	}

	// inductance data
	std::list<ShDataPr> CalcInductance::calculate(
		const fltp time, 
		const cmn::ShLogPr &lg, 
		const ShSolverCachePr& cache){
		return {calculate_inductance(time,lg,cache)};
	}

	// calculate mutual inductance between ms1 and ms2
	ShInductanceDataPr CalcInductance::calculate_inductance(
		const fltp time, 
		const cmn::ShLogPr &lg, 
		#ifdef ENABLE_NL_SOLVER
		const ShSolverCachePr& cache)const{
		#else
		const ShSolverCachePr& /*cache*/)const{
		#endif

		// get date and time
		auto timdata = std::time(nullptr);
		auto localtime = *std::localtime(&timdata);
		std::ostringstream date_stream,time_stream;
		date_stream << std::put_time(&localtime, "%d-%m-%Y");
		time_stream << std::put_time(&localtime, "%H:%M:%S");
		const std::string date_str = date_stream.str();
		const std::string time_str = time_stream.str();

		// general info
		lg->msg("%s=== Starting Inductance Calculation ===%s\n",KGRN,KNRM);
		lg->msg(2,"%sGENERAL INFO%s\n",KGRN,KNRM);
		lg->msg("date: %s%s%s %s(dd-mm-yyyy)%s\n",KYEL,date_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("time: %s%s%s %s(hh:mm:ss)%s\n",KYEL,time_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("model name: %s%s%s\n",KYEL,model_->get_name().c_str(),KNRM);
		lg->msg("path name: %s%s%s\n",KYEL,get_name().c_str(),KNRM);
		lg->msg(-2,"\n");

		// create timer
		arma::wall_clock timer;

		// set timer
		timer.tic();

		// check input model
		if(model_==NULL)rat_throw_line("model not set");
		model_->is_valid(true);

		// settings for mesh creation
		MeshSettings mesh_settings;
		mesh_settings.time = time;
		mesh_settings.low_poly = false;
		mesh_settings.combine_sections = true;

		// create meshes
		std::list<ShMeshDataPr> meshes = model_->create_meshes({},mesh_settings);
		
		// set circuit currents
		apply_circuit_currents(time, meshes);

		// show meshes
		lg->msg(2,"%sGEOMETRY SETUP%s\n",KGRN,KNRM);
		MeshData::display(lg, meshes);
		lg->msg(-2,"\n"); 


		// currentsources for calculating vector potential
		fmm::ShMultiSourcesPr src_a = fmm::MultiSources::create();

		// charge sources for calculating scalar potential
		fmm::ShMultiSourcesPr src_v = fmm::MultiSources::create();

		// bound current sources
		fmm::ShMultiSourcesPr src_b = fmm::MultiSources::create();

		// general targets
		fmm::ShMultiTargets2Pr tar = fmm::MultiTargets2::create();

		// filter out nonlinear meshes and bar meshes for later magnetization calculation
		#ifdef ENABLE_NL_SOLVER
		std::list<ShNLDataPr> nl_meshes;
		#endif
		std::list<ShBarDataPr> bar_meshes;
		std::list<ShCoilDataPr> coil_meshes;

		// walk over meshes
		for(auto it=meshes.begin();it!=meshes.end();it++){
			// calculation meshes
			if((*it)->get_calc_mesh())continue;

			// coil data
			const ShCoilDataPr coil_data = std::dynamic_pointer_cast<CoilData>(*it);
			if(coil_data!=NULL){
				// create current sources
				coil_data->set_softening(1e-7);
				src_a->add_sources(coil_data->create_currents());
				
				// add to targets
				tar->add_targets(coil_data);

				// add to coil meshes list
				coil_meshes.push_back(coil_data);
			}

			// bar data
			const ShBarDataPr bar_data = std::dynamic_pointer_cast<BarData>(*it);
			if(bar_data!=NULL){
				// create fictitious magnetic charges
				const fmm::ShSourcesPr surf_charges = bar_data->create_surface_charges();
				if(surf_charges!=NULL)src_v->add_sources(surf_charges);
				const fmm::ShMgnChargesPr vol_charges = bar_data->create_vol_charges();
				if(vol_charges!=NULL)src_v->add_sources(vol_charges);

				// create bound currents for vector potential calcultion
				const fmm::ShSourcesPr bnd_surf_currents = bar_data->create_bnd_surf_currents();
				if(bnd_surf_currents!=NULL)src_b->add_sources(bnd_surf_currents);
				const fmm::ShSourcesPr bnd_vol_currents = bar_data->create_bnd_vol_currents();
				if(bnd_vol_currents!=NULL)src_b->add_sources(bnd_vol_currents);

				// add to targets
				tar->add_targets(bar_data);

				// add to bar meshes list
				bar_meshes.push_back(bar_data);
			}

			// NL data
			#ifdef ENABLE_NL_SOLVER
			const ShNLDataPr nl_data = std::dynamic_pointer_cast<NLData>(*it);
			if(nl_data!=NULL){
				// add to targets
				tar->add_targets(nl_data);

				// add to nl meshes list
				nl_meshes.push_back(nl_data);
			}
			#endif
		}

		// check for cancel
		if(lg->is_cancelled())return{};

		// for nonlinear materials
		#ifdef ENABLE_NL_SOLVER
		// non-linear targets
		rat::fmm::ShMgnTargetsPr nltar;

		// allocate solution vector
		arma::Col<rat::fltp> Ae;

		// allocate solver
		nl::ShNonLinSolverPr nl_solver;

		// check if any non-linear meshes and there are field sources or background field
		if(!use_only_linear_ && !nl_meshes.empty() && (src_a->get_num_source_objects()!=0 || src_v->get_num_source_objects()!=0 || bg_!=NULL)){
			// create nonlinear solver
			nl_solver = nl::NonLinSolver::create();
			nl_solver->set_use_gpu(stngs_->get_fmm_enable_gpu());
			nl_solver->set_gpu_devices(stngs_->get_gpu_devices());
			nl_solver->set_lg(lg); // maybe this should be a direct transver later

			// combine meshes
			arma::uword idx = 0, nodeshift = 0;
			arma::field<arma::Mat<fltp> > Rnfld(1,nl_meshes.size());
			arma::field<arma::Mat<arma::uword> > nfld(1,nl_meshes.size());
			arma::field<arma::Row<arma::uword> > n2part(1,nl_meshes.size());
			arma::field<nl::ShHBDataPr> hb(1,nl_meshes.size());
			for(auto it=nl_meshes.begin();it!=nl_meshes.end();it++){
				Rnfld(idx) = (*it)->get_nodes();
				nfld(idx) = (*it)->get_elements() + nodeshift;
				hb(idx) = (*it)->get_hb_data();
				if(hb(idx)==NULL)rat_throw_line("HB Curve was not supplied for: " + (*it)->get_name());
				n2part(idx) = arma::Row<arma::uword>(nfld(idx).n_cols,arma::fill::value(idx));
				nodeshift += Rnfld(idx).n_cols; idx++; 
			}

			// convert to matrices
			arma::Mat<fltp> Rn = cmn::Extra::field2mat(Rnfld);
			arma::Mat<arma::uword> n = cmn::Extra::field2mat(nfld);

			// set mesh and hb curves to solver
			nl_solver->set_mesh(Rn, n);
			nl_solver->set_n2part(cmn::Extra::field2mat(n2part)); nl_solver->set_hb_curve(hb);

			// setup mesh by calculating the face and edge connectivity matrices
			nl_solver->setup_mesh();

			// calculate external field
			nltar = nl_solver->get_ext_targets();

			// allocate targets
			nltar->allocate();

			// check if any vector potential sources
			if(src_a->get_num_source_objects()!=0){
				// general info
				lg->msg("%s%s=== CURRENTS TO NLMESH ===%s\n",KBLD,KGRN,KNRM);

				// multipole method using vector potentials
				const fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(src_a, nltar, stngs_);
				mlfmm->set_no_allocate();

				// setup mlfmm and calculate
				mlfmm->setup(lg); 
				
				// check for cancel
				if(lg->is_cancelled())return{};

				// run calculation
				mlfmm->calculate(lg);

				// check for cancel
				if(lg->is_cancelled())return{};
			}

			// check if any scalar potential sources
			if(src_v->get_num_source_objects()!=0){
				// general info
				lg->msg("%s%s=== CHARGES TO NLMESH ===%s\n",KBLD,KGRN,KNRM);

				// multipole method using scalar potentials
				const fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(src_v, nltar, stngs_);
				mlfmm->set_no_allocate();

				// setup mlfmm and calculate
				mlfmm->setup(lg); 

				// check for cancel
				if(lg->is_cancelled())return{};

				// run calculation
				mlfmm->calculate(lg);

				// check for cancel
				if(lg->is_cancelled())return{};
			}

			// calculate background field
			if(bg_!=NULL)bg_->create_fmm_background(time)->calc_field(nltar);

			// set external field
			nl_solver->set_ext_field(nltar);

			// general info
			lg->msg("%s%s=== NONLINEAR SOLVE ===%s\n",KBLD,KGRN,KNRM);

			// run nonlinear solver
			Ae = nl_solver->solve(cache);

			// check for cancel
			if(lg->is_cancelled())return{};

			// calculate magnetization at elements
			const arma::Mat<fltp> Mg = nl_solver->calc_elem_magnetization(Ae.t()); 	
			const arma::Mat<fltp> Me = nl_solver->element_average(Mg);

			// walk over non-linear meshes
			arma::uword elem_shift = 0;
			for(auto it=nl_meshes.begin();it!=nl_meshes.end();it++){
				// get number of elements in this mesh
				const arma::uword num_elements = (*it)->get_num_elements();

				// set field to respective non-linear meshes
				(*it)->set_magnetisation(Me.cols(elem_shift,elem_shift+num_elements-1),false);

				// shift position
				elem_shift += num_elements;
			}

			// create sources
			for(auto it=nl_meshes.begin();it!=nl_meshes.end();it++){
				// create the bound currents to represent the non-linear mesh
				const fmm::ShSourcesPr bnd_surf_currents = (*it)->create_bnd_surf_currents();
				if(bnd_surf_currents!=NULL)src_b->add_sources(bnd_surf_currents);
				const fmm::ShCurrentSourcesPr bnd_vol_currents = (*it)->create_bnd_vol_currents();
				if(bnd_vol_currents!=NULL)src_b->add_sources(bnd_vol_currents);
			}

			// check for cancel
			if(lg->is_cancelled())return{};
		}
	
		#endif

		// report
		lg->msg("%s%s=== CALCULATING ENERGY ===%s\n",KBLD,KGRN,KNRM);

		// allocate energy table
		std::list<std::pair<std::string, fltp> > energy_table;

		// check number of target objects
		if(tar->get_num_target_objects()!=0){
			// allocate targets
			tar->setup_targets();
			tar->allocate();

			// coil currents
			if(src_a->get_num_source_objects()!=0){
				// general info
				lg->msg("%s%s=== COIL CURRENTS TO TARGETS ===%s\n",KBLD,KGRN,KNRM);

				// multipole method
				const fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(src_a, tar, stngs_);
				mlfmm->set_no_allocate();

				// setup mlfmm and calculate
				mlfmm->setup(lg); 

				// check for cancel
				if(lg->is_cancelled())return{};

				// run calculation
				mlfmm->calculate(lg);

				// check for cancel
				if(lg->is_cancelled())return{};
			}

			// bound surface currents for calculating vector potential
			if(src_b->get_num_source_objects()!=0){
				// general info
				lg->msg("%s%s=== BND CURRENTS TO TARGETS ===%s\n",KBLD,KGRN,KNRM);

				// create temporary target points
				const fmm::ShMgnTargetsPr tar_b = fmm::MgnTargets::create(tar->get_target_coords());
				tar_b->set_field_type("AB",{3,3});

				// multipole method
				const fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(src_b, tar_b, stngs_);

				// setup mlfmm and calculate
				mlfmm->setup(lg); 

				// check for cancel
				if(lg->is_cancelled())return{};

				// run calculation
				mlfmm->calculate(lg);

				// check for cancel
				if(lg->is_cancelled())return{};

				// get field and copy to real targets
				tar->add_field('A',tar_b->fmm::MgnTargets::get_field('A')); // copy field at gauss points
				tar->add_field('B',tar_b->fmm::MgnTargets::get_field('B')); // copy field at gauss points
			}
			
			// calculate background field
			if(bg_!=NULL)bg_->create_fmm_background(time)->calc_field(tar);

			// perform post processing
			tar->post_process();

			// walk over coils 
			for(auto it = coil_meshes.begin();it!=coil_meshes.end();it++){
				const ShCoilDataPr& coil_data = (*it);
				energy_table.push_back({coil_data->get_name(), coil_data->calculate_magnetic_energy()});
			}

			// walk over bar_meshes 
			for(auto it = bar_meshes.begin();it!=bar_meshes.end();it++){
				const ShBarDataPr& bar_data = (*it);
				energy_table.push_back({bar_data->get_name(), bar_data->calculate_magnetic_energy()});
			}

			// walk over bar_meshes 
			#ifdef ENABLE_NL_SOLVER
			for(auto it = nl_meshes.begin();it!=nl_meshes.end();it++){
				const ShNLDataPr& nl_data = (*it);
				energy_table.push_back({nl_data->get_name(), nl_data->calculate_magnetic_energy()});
			}
			// energy_table.push_back({"nonlinear", arma::accu(nl_solver->calc_coenergy(Ae.t()))});
			#endif
		}


		// report
		lg->msg("%s%s=== CALCULATING INDUCTANCE MATRIX ===%s\n",KBLD,KGRN,KNRM);

		// number of coils
		const arma::uword num_coils = coil_meshes.size();

		// general settings
		const fltp current_fraction = RAT_CONST(1e-4);

		// walk over coils
		arma::uword idx4 = 0;
		for(auto it=coil_meshes.begin();it!=coil_meshes.end();it++,idx4++){
			// set to vector potential calculation only
			(*it)->set_field_type('A',3);

			// get current
			const fltp coil_current = (*it)->get_operating_current();

			// calculate delta current
			const fltp delta_current = std::max(RAT_CONST(1e-3), current_fraction*coil_current);

			// check if current available
			(*it)->set_operating_current(delta_current);
		}

		// groups 
		std::map<arma::uword, std::list<ShCoilDataPr> > coil_groups;

		// list of coil names
		arma::field<std::string> group_names;
		arma::Col<arma::uword> circuit_indices;

		// in case of drives
		if(group_circuits_){
			// report
			lg->msg("grouping by circuit\n");

			// walk over meshes and extract drive
			for(auto it=coil_meshes.begin();it!=coil_meshes.end();it++)
				coil_groups[(*it)->get_circuit_index()].push_back(*it);

			// make list of circuit names
			std::map<arma::uword, ShCircuitPr> id2circuit;
			for(auto it=circuits_.begin();it!=circuits_.end();it++)
				id2circuit[(*it).second->get_circuit_id()] = (*it).second;

			// set group names
			arma::uword idx = 0;
			group_names.set_size(coil_groups.size());
			circuit_indices.set_size(coil_groups.size());
			for(auto it=coil_groups.begin();it!=coil_groups.end();it++,idx++){
				// get index and circuit
				const arma::uword circuit_index = (*it).first;

				// try to find circuit in list
				auto it2 = id2circuit.find(circuit_index);
				if(it2==id2circuit.end()){
					circuit_indices(idx) = (*it).first;
					group_names(idx) = "Circuit" + std::to_string(circuit_indices(idx));
				}
				else{
					group_names(idx) = (*it2).second->get_name();			
					circuit_indices(idx) = (*it2).second->get_circuit_id();
				}
			}
		}

		// in case of single coils
		else{
			// report
			lg->msg("using single coils\n");

			// set source and target coils
			arma::uword idx = 0;
			group_names.set_size(num_coils);
			for(auto it=coil_meshes.begin();it!=coil_meshes.end();it++,idx++){
				coil_groups[idx].push_back(*it);
				group_names(idx) = (*it)->get_name();
			}
		}

		// number of groups
		const arma::uword num_groups = coil_groups.size();

		// allocate inductance matrix
		arma::Mat<fltp> M(num_groups,num_groups,arma::fill::zeros);

		// first index
		arma::uword idx1=0;

		// walk over coils
		for(auto it1=coil_groups.begin();it1!=coil_groups.end();it1++,idx1++){
			// get mesh
			const std::list<ShCoilDataPr> &mesh1 = (*it1).second;

			// get circuit current
			const fltp dI1 = (*mesh1.begin())->get_operating_current();

			// sources this group
			fmm::ShMultiSourcesPr src = fmm::MultiSources::create();

			// targets for this group
			const fmm::ShMultiTargets2Pr tar = fmm::MultiTargets2::create();

			// add sources the calculation
			for(auto it=mesh1.begin();it!=mesh1.end();it++)src->add_sources((*it)->create_currents());

			// walk over groups
			for(auto it2=it1;it2!=coil_groups.end();it2++){
				// get mesh
				const std::list<ShCoilDataPr> &mesh2 = (*it2).second;

				// add targets
				for(auto it=mesh2.begin();it!=mesh2.end();it++)tar->add_targets(*it);
			}

			// there are non-linear meshes
			#ifdef ENABLE_NL_SOLVER
			if(nltar!=NULL && !use_only_linear_){
				// report
				lg->msg("%s%sCALCULATING FIELD ON NLMESH%s\n",KBLD,KGRN,KNRM);

				// create multipole method
				const fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(stngs_);

				// add to MLFMM
				mlfmm->set_sources(src);
				mlfmm->set_targets(nltar);

				// setup multipole method
				mlfmm->setup(lg);

				// escape
				if(lg->is_cancelled())return{};

				// run multipole method
				mlfmm->calculate(lg);

				// escape
				if(lg->is_cancelled())return{};

				// inductance through the iron yoke
				// calculate delta field from coil onto yoke
				nl_solver->set_ext_dfield(nltar);

				// report
				lg->msg("\n");

				// report
				lg->msg(2,"%s%sLINEAR SOLVE%s\n",KBLD,KGRN,KNRM);

				// solve iron linear system
				const arma::Col<fltp> dAe = nl_solver->dsolve(Ae);

				// report
				lg->msg(-2,"\n");

				// check for cancel
				if(lg->is_cancelled())return{};	

				// calculate magnetization at elements
				const arma::Mat<fltp> dMg = nl_solver->calc_elem_magnetization(dAe.t()); 	
				const arma::Mat<fltp> dMe = nl_solver->element_average(dMg);

				// walk over non-linear meshes
				arma::uword elem_shift = 0;
				for(auto it=nl_meshes.begin();it!=nl_meshes.end();it++){
					// get number of elements in this mesh
					const arma::uword num_elements = (*it)->get_num_elements();

					// set field to respective non-linear meshes
					(*it)->set_magnetisation(dMe.cols(elem_shift,elem_shift+num_elements-1),false);

					// shift position
					elem_shift += num_elements;
				}

				// create sources
				for(auto it=nl_meshes.begin();it!=nl_meshes.end();it++){
					// create the bound currents to represent the non-linear mesh
					const fmm::ShSourcesPr bnd_surf_currents = (*it)->create_bnd_surf_currents();
					if(bnd_surf_currents!=NULL)src->add_sources(bnd_surf_currents);
					const fmm::ShCurrentSourcesPr bnd_vol_currents = (*it)->create_bnd_vol_currents();
					if(bnd_vol_currents!=NULL)src->add_sources(bnd_vol_currents);
				}
			}
			#endif

			// create multilevel fast multipole method object
			{
				// report
				lg->msg("%s%sCALCULATE DFIELD ON COILS%s\n",KBLD,KGRN,KNRM);

				// create mlfmm
				const fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(stngs_);

				// add to MLFMM
				mlfmm->set_sources(src);
				mlfmm->set_targets(tar);

				// setup multipole method
				mlfmm->setup(lg);

				// escape
				if(lg->is_cancelled())return{};

				// run multipole method
				mlfmm->calculate(lg);

				// escape
				if(lg->is_cancelled())return{};
			}

			// report
			lg->msg("%s%sDERIVE INDUCTANCE%s\n",KBLD,KGRN,KNRM);

			// second index
			arma::uword idx2 = idx1;

			// walk over groups
			for(auto it2=it1;it2!=coil_groups.end();it2++,idx2++){
				// get mesh
				const std::list<ShCoilDataPr> &mesh2 = (*it2).second;

				// stored energy in this connection
				for(auto it3=mesh2.begin();it3!=mesh2.end();it3++){
					// calculate captured flux in second coil
					const fltp dphi2 = (*it3)->calculate_flux();

					// check flux
					if(!std::isfinite(dphi2))rat_throw_line("captured flux is not finite");

					// calculate inductance based on current in first coil
					M(idx1,idx2) += dphi2/dI1;

					// // self inductance
					// if(idx1==idx2){
					// 	const fltp self_inductance = (*it3)->calculate_element_self_inductance();
					// 	if(!std::isfinite(self_inductance))
					// 		rat_throw_line("self inductance is not finite");
					// 	M(idx1,idx2) += self_inductance;
					// }
				}

				// mirror
				if(idx1!=idx2){M(idx2,idx1) = M(idx1,idx2);} // E(idx2,idx1) = E(idx1,idx2);
			}

			// check for cancel
			if(lg->is_cancelled())return NULL;
		}

		// check matrix
		if(!M.is_finite())rat_throw_line("Inductance matrix is not finite");

		// get calculation time
		const fltp calculation_time = timer.toc();

		// create data object
		const ShInductanceDataPr ind_data = InductanceData::create(time,M,circuit_indices,energy_table,group_names,calculation_time);

		// create inductance object and return
		return ind_data;
	}

	// get type
	std::string CalcInductance::get_type(){
		return "rat::mdl::calcinductance";
	}

	// method for serialization into json
	void CalcInductance::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		CalcLeaf::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["use_parallel"] = use_parallel_;
		js["group_circuits"] = group_circuits_;
		js["use_only_linear"] = use_only_linear_;
	}

	// method for deserialisation from json
	void CalcInductance::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		CalcLeaf::deserialize(js,list,factory_list,pth);
		set_use_parallel(js["use_parallel"].asBool());
		set_group_circuits(js["group_circuits"].asBool());
		set_use_only_linear(js["use_only_linear"].asBool());
	}

}}