// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "mesh.hh"

#include "rat/common/elements.hh"
#include "rat/common/extra.hh"

#include "coildata.hh"
#include "bardata.hh"
#include "extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	Mesh::Mesh(){
		
	}

	// constructor with input
	Mesh::Mesh(
		const ShFramePr &frame, 
		const ShAreaPr &area, 
		const mat::ShConductorPr &material,
		const bool use_parallel) : Mesh(){
		setup(frame, area, use_parallel);
		set_material(material);
	}

	// manual mesh construction
	Mesh::Mesh(
		const arma::Mat<fltp> &R, 
		const arma::Mat<arma::uword> &n, 
		const arma::Mat<arma::uword> &s) : Mesh(){
		setup(R,n,s);
	}

	// factory
	ShMeshPr Mesh::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<Mesh>();
	}

	// factory with input
	ShMeshPr Mesh::create(
		const ShFramePr &frame, 
		const ShAreaPr &area,
		const mat::ShConductorPr &material,
		const bool use_parallel){
		return std::make_shared<Mesh>(frame,area,material,use_parallel);
	}

	// factory with input
	ShMeshPr Mesh::create(
		const arma::Mat<fltp> &R, 
		const arma::Mat<arma::uword> &n, 
		const arma::Mat<arma::uword> &s){
		return std::make_shared<Mesh>(R,n,s);
	}

	// get frame section lengths
	const arma::Row<fltp>& Mesh::get_ell_frame() const{
		return ell_frame_;
	}

	// setup function
	void Mesh::setup(const ShFramePr &frame, const ShAreaPr &area, const bool use_parallel){
		// check input
		assert(frame!=NULL); assert(area!=NULL);

		// hard-coded settings
		const bool cap_start = true;
		const bool cap_end = true;

		// get base mesh
		Rb_ = area->get_nodes();
		b_ = area->get_elements();
		p_ = area->get_perimeter();
		q_ = area->get_corner_nodes();
		base_area_ = area->get_area();
		num_base_elements_ = area->get_num_elements();
		num_base_nodes_ = area->get_num_nodes();
		num_base_perimeter_ = area->get_num_perimeter();

		// hidden thickness and width
		hidden_thickness_ = area->get_hidden_thickness();
		hidden_width_ = area->get_hidden_width();

		// get base mesh nodes and orientation
		const arma::Mat<fltp>& R2d = area->get_nodes();
		const arma::Mat<fltp>& N2d = area->get_normal();
		const arma::Mat<fltp>& D2d = area->get_transverse();

		// call internal loft function
		is_extruded_ = true;
		frame->create_loft(
			R_,L_,N_,D_,n_,s_,R2d,N2d,D2d,
			b_,p_,cap_start,cap_end,use_parallel);

		// // clockwise check
		// #ifndef NDEBUG
		// if(n_.n_rows==8){
		// 	// at this moment we want anti-clockwise mesh
		// 	if(arma::any(cmn::Hexahedron::is_clockwise(R_,n_)==true))
		// 		rat_throw_line("mesh is not anti-clockwise");
		// }
		// #endif

		// number of nodes in frame
		is_loop_ = frame->is_loop();
		is_closed_ = frame->is_closed();
		num_cis_frame_ = frame->get_num_nodes();
		section_ = frame->get_section();
		turn_ = frame->get_turn();
		ell_frame_ = frame->calc_ell();

		// combine
		num_cs_ = n_.n_cols/num_base_elements_;
		num_cis_ = arma::accu(num_cis_frame_-is_closed_);
		num_nodes_ = arma::accu((num_cis_frame_-is_closed_)*num_base_nodes_);
		num_cs_elements_ = num_cs_*num_base_elements_;
		num_cis_elements_ = num_cis_*num_base_elements_;

		// center line
		const arma::Col<fltp>::fixed<2> ntc = area->get_center_coord();
		Rc_.set_size(1,frame->get_num_sections());
		for(arma::uword i=0;i<frame->get_num_sections();i++){
			Rc_(i) = frame->get_coords(i) + 
				ntc(0)*frame->get_normal(i) + 
				ntc(1)*frame->get_transverse(i);
		}

		// determine dimensions for surface and volume mesh
		determine_dimensions();

		// check output
		assert(arma::max(arma::max(n_))<R_.n_cols); 
		assert(arma::max(arma::max(s_))<R_.n_cols);
		assert(num_nodes_==R_.n_cols);
		assert(L_.n_cols==num_nodes_); 
		assert(N_.n_cols==num_nodes_);
		assert(D_.n_cols==num_nodes_);
	}

	// split mesh into sections connected in series 
	// the split happens at the frame sections
	arma::field<ShMeshPr> Mesh::split_series()const{
		// check input
		assert(is_closed_.n_elem==num_cis_frame_.n_elem);
		assert(Rc_.n_elem==num_cis_frame_.n_elem);
		assert(section_.n_elem==num_cis_frame_.n_elem);
		assert(ell_frame_.n_elem==num_cis_frame_.n_elem);
		assert(num_cis_frame_.n_elem==num_cis_frame_.n_elem);

		// allocate
		arma::field<ShMeshPr> meshes(1,num_cis_frame_.n_elem);

		// get indices of start and end cis
		const arma::Row<arma::uword> cis_idx = arma::join_horiz(arma::Row<arma::uword>{0}, arma::cumsum(num_cis_frame_));
		const arma::Row<arma::uword> cs_idx = arma::join_horiz(arma::Row<arma::uword>{0}, arma::cumsum(num_cis_frame_-1));

		// walk over parts
		for(arma::uword i=0;i<num_cis_frame_.n_elem;i++){
			// get node, element and perimeter indices
			const arma::uword idx1 = cis_idx(i)*num_base_nodes_;
			const arma::uword idx2 = cis_idx(i+1)*num_base_nodes_-1;
			const arma::uword nidx1 = cs_idx(i)*num_base_elements_;
			const arma::uword nidx2 = cs_idx(i+1)*num_base_elements_-1;
			const arma::uword sidx1 = cs_idx(i)*num_base_perimeter_ + i*2*num_base_elements_;
			const arma::uword sidx2 = cs_idx(i+1)*num_base_perimeter_-1 + i*2*num_base_elements_;

			// create mesh data
			meshes(i) = Mesh::create(
				R_.cols(idx1,idx2), 
				n_.cols(nidx1,nidx2) - idx1, 
				s_.cols(sidx1,sidx2) - idx1);

			// orientation
			meshes(i)->set_longitudinal(L_.cols(idx1,idx2));
			meshes(i)->set_normal(N_.cols(idx1,idx2));
			meshes(i)->set_transverse(D_.cols(idx1,idx2));

			// set info
			meshes(i)->set_is_loop(num_cis_frame_.n_elem==1 ? is_loop_ : false);
			meshes(i)->set_is_extruded(is_extruded_);
			meshes(i)->set_num_base_elements(num_base_elements_);
			meshes(i)->set_num_base_nodes(num_base_nodes_);
			meshes(i)->set_num_base_perimeter(num_base_perimeter_);
			meshes(i)->set_num_cis(num_cis_frame_(i));
			meshes(i)->set_num_cs(num_cis_frame_(i)-1+is_closed_(i));
			meshes(i)->set_num_cis_elements(num_cis_frame_(i)*num_base_elements_);
			meshes(i)->set_num_cs_elements((num_cis_frame_(i)-1)*num_base_elements_);

			// section wise data
			meshes(i)->set_is_closed({is_closed_(i)});
			meshes(i)->set_center_line({Rc_(i)});
			meshes(i)->set_section({section_(i)});
			meshes(i)->set_turn({turn_(i)});
			meshes(i)->set_ell_frame({ell_frame_(i)});
			meshes(i)->set_num_cis_frame({num_cis_frame_(i)});

			// base mesh
			meshes(i)->set_base_mesh(b_);
			meshes(i)->set_base_area(base_area_);

			// hidden dimensions
			meshes(i)->set_hidden_width(hidden_width_);
			meshes(i)->set_hidden_thickness(hidden_thickness_);

			// transfer conductor
			meshes(i)->set_dir(dir_);
			meshes(i)->set_material(material_);
		}

		// return field of meshes
		return meshes;
	}

	// split mesh into parallel strips
	arma::field<ShMeshPr> Mesh::split_parallel()const{

		// number of strips
		const arma::uword num_strips = b_.n_cols;

		// allocate
		arma::field<ShMeshPr> meshes(1,num_strips);

		// node reindexing
		arma::Col<arma::uword> reindex_nodes(num_nodes_); 

		// walk over base mesh elements
		for(arma::uword i=0;i<num_strips;i++){
			// create element indices
			const arma::Col<arma::uword> element_idx = 
				arma::regspace<arma::Col<arma::uword> >(
				i, num_base_elements_, num_cs_elements_-1);
			assert(element_idx.n_elem==num_cs_);

			// get node indices
			arma::Mat<arma::uword> n = n_.cols(element_idx);

			// get base
			arma::Mat<arma::uword> b = b_.col(i);

			// dump unused nodes
			const arma::Col<arma::uword> used_nodes = arma::unique(arma::vectorise(n));

			// set array for re-indexing the elements
			reindex_nodes.zeros();
			reindex_nodes(used_nodes) = arma::regspace<arma::Col<arma::uword> >(0,used_nodes.n_elem-1);

			// reindex elements
			n = arma::reshape(reindex_nodes(arma::vectorise(n)),n.n_rows,n.n_cols);
			b = arma::reshape(reindex_nodes(arma::vectorise(b)),b.n_rows,b.n_cols);

			// number of base nodes
			const arma::uword num_base = b.n_elem;

			// reconstruct surface
			arma::Mat<arma::uword> s(num_base, num_cis_);
			for(arma::uword j=0;j<num_cis_;j++)s.col(j) = b + j*num_base;

			// cap?

			// node coordinates
			const arma::Mat<fltp> R = R_.cols(used_nodes);

			// calculate center line
			arma::field<arma::Mat<fltp> > Rc(num_cis_frame_.n_elem);
			arma::uword cnt = 0;
			for(arma::uword k=0;k<num_cis_frame_.n_elem;k++){
				Rc(k).set_size(3,num_cis_frame_(k));
				for(arma::uword j=0;j<num_cis_frame_(k);j++){
					Rc(k).col(j) = arma::mean(R.cols(cnt*num_base, (cnt+1)*num_base-1),1);
				}
				cnt+=num_cis_frame_(k)-1;
			}

			// create mesh data
			meshes(i) = Mesh::create(R,n,s);

			// orientation
			meshes(i)->set_longitudinal(L_.cols(used_nodes));
			meshes(i)->set_normal(N_.cols(used_nodes));
			meshes(i)->set_transverse(D_.cols(used_nodes));

			// set info
			meshes(i)->set_is_loop(is_loop_);
			meshes(i)->set_is_extruded(is_extruded_);
			meshes(i)->set_num_base_elements(1);
			meshes(i)->set_num_base_nodes(n.n_rows/2);
			meshes(i)->set_num_base_perimeter(n.n_rows/2);
			meshes(i)->set_num_cis(num_cis_);
			meshes(i)->set_num_cs(num_cs_);
			meshes(i)->set_num_cis_elements(num_cis_);
			meshes(i)->set_num_cs_elements(num_cs_);

			// section wise data
			meshes(i)->set_is_closed(is_closed_);
			meshes(i)->set_center_line(Rc);
			meshes(i)->set_section(section_);
			meshes(i)->set_turn(turn_);
			meshes(i)->set_ell_frame(ell_frame_);
			meshes(i)->set_num_cis_frame(num_cis_frame_);

			// base mesh
			meshes(i)->set_base_mesh(b);
			meshes(i)->set_base_area(base_area_); // this is not entirely correct

			// hidden dimensions
			meshes(i)->set_hidden_width(hidden_width_);
			meshes(i)->set_hidden_thickness(hidden_thickness_);

			// transfer conductor
			meshes(i)->set_dir(dir_);
			meshes(i)->set_material(material_);
		}

		return meshes;
	}


	// split meshdata into frame sections
	arma::field<ShMeshPr> Mesh::split(const bool series, const bool parallel)const{
		// check
		if(!series && !parallel)rat_throw_line("must split something");

		// single split
		if(series && !parallel)return split_series();
		if(parallel && !series)return split_parallel();

		// first split
		arma::field<ShMeshPr> sub = split_series();

		// create indices
		arma::Row<arma::uword> ndsub(sub.n_elem);
		for(arma::uword i=0;i<sub.n_elem;i++)
			ndsub(i) = sub(i)->get_num_base_elements();
		arma::Row<arma::uword> sub_index = arma::join_horiz(
			arma::Row<arma::uword>{0}, arma::cumsum<arma::Row<arma::uword> >(ndsub));

		// second split
		arma::field<ShMeshPr> dsub(1,arma::accu(ndsub));
		for(arma::uword i=0;i<sub.n_elem;i++)
			dsub.cols(sub_index(i),sub_index(i+1)-1) = sub(i)->split_parallel();
			
		// return double split
		return dsub;
	}

	// setup from nodes and elements
	void Mesh::setup(
		const arma::Mat<fltp> &R, 
		const arma::Mat<arma::uword> &n, 
		const arma::Mat<arma::uword> &s){

		// set mesh function
		set_mesh(R,n,s);

		// do not use L, N and D
		L_.zeros(R_.n_rows,R_.n_cols); 
		N_.zeros(R_.n_rows,R_.n_cols); 
		D_.zeros(R_.n_rows,R_.n_cols);

		// area data
		base_area_ = 0.0;
		hidden_thickness_ = 1.0;
		hidden_width_ = 1.0;
		num_base_elements_ = 0;
		num_base_nodes_ = 0;

		// number of nodes in frame
		is_loop_ = false;
		num_cis_frame_ = {};
		section_ = {};
		turn_ = {};
		ell_frame_ = 0.0;

		// combine
		num_cs_ = 0;
		num_cis_ = 0;
		num_cis_elements_ = 0;
		
		// frame data
		ell_frame_ = 0.0;

		// check output
		assert(arma::max(arma::max(n_))<R_.n_cols); 
		assert(arma::max(arma::max(s_))<R_.n_cols);
	}

	// set mesh function
	void Mesh::set_mesh(
		const arma::Mat<fltp> &R, 
		const arma::Mat<arma::uword> &n, 
		const arma::Mat<arma::uword> &s){

		// set to self
		R_ = R; n_ = n; s_ = s;

		// update
		num_nodes_ = R_.n_cols;
		num_cs_elements_ = n_.n_cols;

		// // clockwise check
		// #ifndef NDEBUG
		// if(n_.n_rows==8){
		// 	// at this moment we want anti-clockwise mesh
		// 	if(arma::any(cmn::Hexahedron::is_clockwise(R_,n_)==true))
		// 		rat_throw_line("mesh is not anti-clockwise");
		// }
		// #endif

		// determine dimensions for surface and volume mesh
		determine_dimensions();
	}

	// convert to tetrahedron
	void Mesh::refine_hexahedron_to_tetrahedron(){
		// refine
		cmn::Hexahedron::refine_mesh_to_tetrahedron(R_,n_);

		// rebuild surface
		s_ = cmn::Tetrahedron::extract_surface(n_);

		// recall setup
		setup(R_,n_,s_);
	}


	// set base number of elements
	void Mesh::set_num_base_elements(const arma::uword num_base_elements){
		num_base_elements_ = num_base_elements;
	}

	// set base number of nodes in base mesh
	void Mesh::set_num_base_nodes(const arma::uword num_base_nodes){
		num_base_nodes_ = num_base_nodes;
	}

	// set base number of nodes in perimeter
	void Mesh::set_num_base_perimeter(const arma::uword num_base_perimeter){
		num_base_perimeter_ = num_base_perimeter;
	}

	// set number of cable intersections
	void Mesh::set_num_cis(const arma::uword num_cis){
		num_cis_ = num_cis;
	}

	// set number of cable sections
	void Mesh::set_num_cs(const arma::uword num_cs){
		num_cs_ = num_cs;
	}

	// set number of elements in cable intersections
	void Mesh::set_num_cis_elements(const arma::uword num_cis_elements){
		num_cis_elements_ = num_cis_elements;
	}

	// set number of elements in cs
	void Mesh::set_num_cs_elements(const arma::uword num_cs_elements){
		num_cs_elements_ = num_cs_elements;
	}

	// set whether mesh is extruded
	void Mesh::set_is_extruded(const bool is_extruded){
		is_extruded_ = is_extruded;
	}

	// set whether mesh is a loop or not
	void Mesh::set_is_loop(const bool is_loop){
		is_loop_ = is_loop;
	}

	void Mesh::set_section(const arma::Row<arma::uword> &section){
		section_ = section;
	}

	void Mesh::set_turn(const arma::Row<arma::uword> &turn){
		turn_ = turn;
	}

	void Mesh::set_is_closed(const arma::Row<arma::uword> &is_closed){
		is_closed_ = is_closed;
	}

	void Mesh::set_ell_frame(const arma::Row<fltp> &ell_frame){
		ell_frame_ = ell_frame;
	}

	void Mesh::set_base_area(const fltp base_area){
		base_area_ = base_area;
	}

	// set base mesh
	void Mesh::set_base_mesh(const arma::Mat<arma::uword> &b){
		b_ = b;
	}

	// set base mesh
	void Mesh::set_perimeter(const arma::Mat<arma::uword> &p){
		p_ = p;
	}

	// hidden dim
	fltp Mesh::calc_hidden_dim()const{
		return hidden_thickness_*hidden_width_;
	}

	// get hidden thickness
	fltp Mesh::get_hidden_thickness()const{
		return hidden_thickness_;
	}

	// get hidden width
	fltp Mesh::get_hidden_width()const{
		return hidden_width_;
	}

	// get base area
	fltp Mesh::get_base_area()const{
		return base_area_;
	}

	// set frame number of intersections
	void Mesh::set_num_cis_frame(const arma::Row<arma::uword>& num_cis_frame){
		num_cis_frame_ = num_cis_frame;
	}

	// determine dimensions
	void Mesh::determine_dimensions(){
		// volume dimensionality
		if(n_.n_rows==1)n_dim_ = 0; // points
		if(n_.n_rows==2)n_dim_ = 1; // lines
		if(n_.n_rows==3)n_dim_ = 2; // triangles
		if(n_.n_rows==4){
			if(s_.n_rows==3)n_dim_ = 3; // tetrahedrons
			if(s_.n_rows==4)n_dim_ = 2; // quadrilaterals
		}
		if(n_.n_rows==8)n_dim_ = 3; // hexahedrons

		// surface dimensionality
		if(s_.n_rows==1)s_dim_ = 0; // points
		if(s_.n_rows==2)s_dim_ = 1; // lines
		if(s_.n_rows==3)s_dim_ = 2; // triangles
		if(s_.n_rows==4)s_dim_ = 2; // quadrilaterals
	}


	// get number of dimensions
	arma::uword Mesh::get_n_dim() const{
		return n_dim_;
	}

	// get number of dimensions
	arma::uword Mesh::get_s_dim() const{
		return s_dim_;
	}

	// calculate volume for each element
	arma::Row<fltp> Mesh::calc_volume() const{
		// volume dimensionality
		arma::Row<fltp> V;
		if(n_dim_==0)V = arma::Row<fltp>(n_.n_cols,arma::fill::ones);
		else if(n_dim_==1)V = cmn::Line::calc_length(R_,n_); // lines
		else if(n_dim_==2)V = cmn::Quadrilateral::calc_area(R_,n_); // quadrilaterals
		else if(n_dim_==3)V = cmn::Hexahedron::calc_volume(R_,n_); // hexahedrons
		else rat_throw_line("number of dimensions is larger than 3");
		assert(arma::all(V>=0));

		// return volume
		return calc_hidden_dim()*V;
	}

	// calculate total volume
	fltp Mesh::calc_total_volume() const{
		return arma::accu(calc_volume());
	}

	// calculate total surface area of the mesh
	fltp Mesh::calc_total_surface_area() const{
		return arma::sum(cmn::Quadrilateral::calc_area(R_,s_));
	}

	// get number of nodes in the 3D mesh
	arma::uword Mesh::get_num_nodes() const{
		return num_nodes_;
	}

	// get number of sections
	arma::uword Mesh::get_num_sections() const{
		return num_cis_frame_.n_elem;
	}

	// get number of nodes in the 2D base mesh
	arma::uword Mesh::get_num_base_nodes()const{
		return num_base_nodes_;
	}

	// get number of elements in the 2D base mesh
	arma::uword Mesh::get_num_base_elements()const{
		return num_base_elements_;
	}

	// get number of nodes in the perimeter of the 2D base mesh
	arma::uword Mesh::get_num_base_perimeter()const{
		return num_base_perimeter_;
	}


	// get number of cable intersections
	arma::uword Mesh::get_num_cis()const{
		return num_cis_;
	}

	// get number of cable sections
	arma::uword Mesh::get_num_cs()const{
		return num_cs_;
	}

	// get number of elements in all cable intersections
	arma::uword Mesh::get_num_cis_elements()const{
		return num_cis_elements_;
	}

	// get number of elements in all of cable sections
	arma::uword Mesh::get_num_cs_elements()const{
		return num_cs_elements_;
	}

	// get whether the mesh is looped
	bool Mesh::get_is_loop()const{
		return is_loop_;
	}

	// get number of cis in the frame
	const arma::Row<arma::uword>& Mesh::get_num_cis_frame()const{
		return num_cis_frame_;
	}

	// create element coordinates
	arma::Mat<fltp> Mesh::get_element_coords()const{
		arma::Mat<fltp> Re(3,n_.n_cols);
		for(arma::uword i=0;i<n_.n_cols;i++)
			Re.col(i) = arma::mean(R_.cols(n_.col(i)),1);
		return Re;
	}

	// get nodes
	const arma::Mat<fltp>& Mesh::get_nodes()const{
		return R_;
	}

	// get coordinates for section
	const arma::Mat<arma::uword>& Mesh::get_elements() const{
		return n_;
	}

	// get number of elements
	arma::uword Mesh::get_num_elements() const{
		return n_.n_cols;
	}

	// get coordinates for section
	const arma::Mat<arma::uword>& Mesh::get_surface_elements() const{
		return s_;
	}

	// get number of surface elements
	arma::uword Mesh::get_num_surface() const{
		return s_.n_cols;
	}

	// set longitudinal vector
	void Mesh::set_longitudinal(const arma::Mat<fltp> &L){
		assert(L.n_rows==3);
		L_ = L;
	}

	// set transverse vector
	void Mesh::set_transverse(const arma::Mat<fltp> &D){
		assert(D.n_rows==3);
		D_ = D;
	}

	// set normal vectors
	void Mesh::set_normal(const arma::Mat<fltp> &N){
		assert(N.n_rows==3);
		N_ = N;
	}

	// set elements
	void Mesh::set_elements(const arma::Mat<arma::uword> &n){
		n_ = n;
	}

	// set nodes of base mesh
	void Mesh::set_base_nodes(const arma::Mat<fltp>&Rb){
		Rb_ = Rb;
	}

	// set elements
	void Mesh::set_surface_elements(const arma::Mat<arma::uword> &s){
		s_ = s;
	}

	// set corner nodes
	void Mesh::set_edge_nodes(const arma::Col<arma::uword> &q){
		q_ = q;
	}

	// apply multiple transformations
	void Mesh::apply_transformations(const std::list<ShTransPr> &trans_list, const fltp time){
		// walk over transformations and apply
		for(auto it=trans_list.begin();it!=trans_list.end();it++)
			apply_transformation(*it,time);
	}

	// apply transformation
	void Mesh::apply_transformation(const ShTransPr &trans, const fltp time){
		// transform node orientation vectors
		trans->apply_vectors(R_,L_,'L',time); 
		trans->apply_vectors(R_,N_,'N',time);
		trans->apply_vectors(R_,D_,'D',time); 

		// transform node coordinates
		trans->apply_coords(R_,time);

		// transform mesh
		trans->apply_mesh(n_,n_dim_,get_num_nodes(),time); 
		trans->apply_mesh(s_,s_dim_,get_num_nodes(),time);

		// apply to center line
		for(arma::uword i=0;i<Rc_.n_elem;i++)
			trans->apply_coords(Rc_(i),time);
	}

	// get coordinates for section
	const arma::Mat<fltp>& Mesh::get_longitudinal() const{
		return L_;
	}

	// get coordinates for section
	const arma::Mat<fltp>& Mesh::get_normal() const{
		return N_;
	}

	// get coordinates for section
	const arma::Mat<fltp>& Mesh::get_transverse() const{
		return D_;
	}

	// calculate binormal vector
	arma::Mat<fltp> Mesh::calc_binormal()const{
		return cmn::Extra::cross(N_,L_);
	}

	// get size of the mesh
	arma::Col<fltp>::fixed<3> Mesh::get_lower_bound() const{
		return arma::min(R_,1);
	}

	// get size of the mesh
	arma::Col<fltp>::fixed<3> Mesh::get_upper_bound() const{
		return arma::max(R_,1);
	}

	// get cross mesh
	const arma::Mat<arma::uword>& Mesh::get_base_mesh()const{
		assert(b_.n_cols==num_base_elements_);
		return b_;
	}

	// get cross mesh
	const arma::Mat<arma::uword>& Mesh::get_base_perimeter()const{
		assert(p_.n_cols==num_base_perimeter_);
		return p_;
	}	

	// set hidden width
	void Mesh::set_hidden_width(const fltp hidden_width){
		hidden_width_ = hidden_width;
	}

	// set hidden thickness
	void Mesh::set_hidden_thickness(const fltp hidden_thickness){
		hidden_thickness_ = hidden_thickness;
	}

	// set center line
	void Mesh::set_center_line(const arma::field<arma::Mat<fltp> > &Rc){
		Rc_ = Rc;
	}

	// set center line
	const arma::field<arma::Mat<fltp> >& Mesh::get_center_line()const{
		return Rc_;
	}

	// calculate element areas at cable intersections
	// TODO for frame with multiple sections skip the joint
	arma::Row<fltp> Mesh::calc_cis_areas()const{
		// check input
		assert(num_cis_elements_>0);

		// allocate
		arma::Row<fltp> cis_area(num_cis_elements_);

		// calculation of cross areas
		// get elements from 2D mesh
		const arma::Mat<arma::uword>& n2d = get_base_mesh();
		
		// check input
		assert(arma::all(cmn::Extra::vec_norm(L_)>RAT_CONST(1e-14)));
		assert(arma::all(cmn::Extra::vec_norm(D_)>RAT_CONST(1e-14)));

		// calculate angle of cross section at nodes
		const arma::Row<fltp> cross_angle_nodes = cmn::Extra::vec_angle(L_,cmn::Extra::cross(D_,N_));
		assert(cross_angle_nodes.is_finite());

		// walk over cable intersections
		for(arma::uword i=0;i<num_cis_;i++){
			// get indices for this cross section
			const arma::uword idx1 = i*num_base_elements_;
			const arma::uword idx2 = (i+1)*num_base_elements_-1;

			// shift 2D mesh
			const arma::Mat<arma::uword> nc = cmn::Extra::modulus(n2d + i*num_base_nodes_, num_nodes_);

			// for quadrilaterals area 
			if(nc.n_rows==4){

				// get vectors spanning face
				const arma::Mat<fltp> V1 = R_.cols(nc.row(1)) - R_.cols(nc.row(0));
				const arma::Mat<fltp> V2 = R_.cols(nc.row(3)) - R_.cols(nc.row(0));
				const arma::Mat<fltp> V3 = R_.cols(nc.row(1)) - R_.cols(nc.row(2));
				const arma::Mat<fltp> V4 = R_.cols(nc.row(3)) - R_.cols(nc.row(2));

				// calculate cross products (only z component)
				const arma::Row<fltp> A1 = cmn::Extra::vec_norm(cmn::Extra::cross(V1,V2))/2;
				const arma::Row<fltp> A2 = cmn::Extra::vec_norm(cmn::Extra::cross(V3,V4))/2;

				// sum values and return
				cis_area.cols(idx1,idx2) = calc_hidden_dim()*(A1 + A2);
			}

			// for line elements area
			else if(nc.n_rows==2){

				// get edge vector
				const arma::Mat<fltp> V1 = R_.cols(nc.row(1)) - R_.cols(nc.row(0));

				// sum values and return
				cis_area.cols(idx1,idx2) = calc_hidden_dim()*cmn::Extra::vec_norm(V1);
			}

			// for point sources area is just 1
			// this ensures that the current is 
			// set fully to this element
			else if(nc.n_rows==1){
				cis_area.cols(idx1,idx2) = arma::Row<fltp>{calc_hidden_dim()};
			}

			else{
				rat_throw_line("element type not recognized");
			}

			// angle at elements
			arma::Row<fltp> cross_angle_elem(num_base_elements_);
			for(arma::uword j=0;j<num_base_elements_;j++)
				cross_angle_elem(j) = arma::as_scalar(arma::mean(cross_angle_nodes.cols(nc.col(j)),1));
			assert(cross_angle_elem.is_finite());

			// adjust for angle
			cis_area.cols(idx1,idx2) %= arma::cos(cross_angle_elem);
		}

		// retun areas
		return cis_area;
	}

	// calculate areas at cable sections
	arma::Row<fltp> Mesh::calc_cs_areas(const arma::Row<fltp>& cis_areas)const{
		return cis2cs(cis_areas);
	}

	// calculate areas at cable sections
	arma::Row<fltp> Mesh::calc_cs_areas()const{
		return calc_cs_areas(calc_cis_areas());
	}

	// calculate cis cross sectional total area
	arma::Row<fltp> Mesh::calc_cis_total_area(const arma::Row<fltp>& cis_areas)const{
		// check input
		assert(cis_areas.n_cols==num_cis_elements_);

		// allocate
		arma::Row<fltp> cis_total_area(num_cis_);

		// walk over cable intersections
		for(arma::uword i=0;i<num_cis_;i++){
			// get indices for this cross section
			const arma::uword idx1 = i*num_base_elements_;
			const arma::uword idx2 = (i+1)*num_base_elements_-1;

			// add areas for total area
			cis_total_area(i) = arma::accu(cis_areas.cols(idx1,idx2));
		}

		// return the area of the respective cross sections
		return cis_total_area;
	}

	// calculate cable section cross sectional area
	// this is averaged between the connected cis
	arma::Row<fltp> Mesh::calc_cs_total_area(
		const arma::Row<fltp>& cis_total_area)const{
		assert(cis_total_area.n_cols==num_cis_);
		return (cis_total_area.head_cols(num_cs_) + cis_total_area.tail_cols(num_cs_))/2;
	}
	
	// without area input
	arma::Row<fltp> Mesh::calc_cs_total_area()const{
		return calc_cs_total_area(calc_cis_total_area());
	}

	// interpolate properties from elements in 
	// cable intersections to cable sections
	arma::Mat<fltp> Mesh::cis2cs(
		const arma::Mat<fltp>& vcis)const{

		// check input
		assert(vcis.n_cols==num_cis_elements_);

		// allocate
		arma::Row<fltp> vcs(num_cs_elements_);
			
		// walk over cable sections
		for(arma::uword i=0;i<num_cs_;i++){
			// first cable intersection start and end index
			const arma::uword idx1 = (i*num_base_elements_)%num_cs_elements_;
			const arma::uword idx2 = ((i+1)*num_base_elements_-1)%num_cs_elements_;

			// second cable intersection stgart and end index
			const arma::uword idx3 = ((i+1)*num_base_elements_)%num_cs_elements_;
			const arma::uword idx4 = ((i+2)*num_base_elements_-1)%num_cs_elements_;

			// interpolate from cis to cs
			vcs.cols(idx1,idx2) = (vcis.cols(idx1,idx2) + vcis.cols(idx3,idx4))/2;
		}

		// return element current
		return vcs;	
	}

	// calculate cis cross sectional total area
	arma::Row<fltp> Mesh::calc_cis_total_area()const{
		return calc_cis_total_area(calc_cis_areas());
	}

	// calculate element sizes at cable intersections
	arma::Row<fltp> Mesh::calc_cis_delem()const{
		// allocate
		arma::Row<fltp> cis_delem(num_cis_elements_);

		// for line elements
		if(n_dim_==1){
			cis_delem.fill(std::max(hidden_thickness_/2, hidden_width_/2));
		}

		// for other elements
		else{
			// get 2d mesh
			const arma::Mat<arma::uword>& n2d = get_base_mesh();

			// walk over cable intersections
			for(arma::uword i=0;i<num_cis_;i++){
				// shift 2D mesh
				const arma::Mat<arma::uword> nc = cmn::Extra::modulus(n2d + i*num_base_nodes_,num_nodes_);

				// get nodes
				for(arma::uword j=0;j<num_base_elements_;j++){
					const arma::uword idx = i*num_base_elements_ + j;
					const arma::Mat<fltp> Rn2d = R_.cols(nc.col(j));
					const arma::Col<fltp> R0 = arma::mean(Rn2d,1);
					cis_delem(idx) = arma::min(cmn::Extra::vec_norm(Rn2d.each_col() - R0));
				}
			}
		}

		// return element size
		return cis_delem;
	}

	// calculate length from volume and area
	arma::Row<fltp> Mesh::calc_cs_length(
		const arma::Row<fltp>& volume, 
		const arma::Row<fltp>& cs_total_area)const{
		
		// check input
		assert(volume.n_elem==num_cs_elements_);
		assert(cs_total_area.n_elem==num_cs_);

		// allocate
		arma::Row<fltp> ell_cs(num_cs_);

		// walk over cable sections
		for(arma::uword i=0;i<num_cs_;i++){
			// get indices for this cross section
			const arma::uword idx1 = (i*num_base_elements_)%num_cs_elements_;
			const arma::uword idx2 = ((i+1)*num_base_elements_-1)%num_cs_elements_;

			ell_cs(i) = arma::accu(volume.cols(idx1,idx2))/cs_total_area(i);
		}

		// return section lengths
		return ell_cs;
	}

	// calculate cable section length
	arma::Row<fltp> Mesh::calc_cs_length()const{
		const arma::Row<fltp> volume = calc_volume();
		const arma::Row<fltp> cs_total_area = calc_cs_total_area();
		return calc_cs_length(volume, cs_total_area);
	}

	// calculate total length
	fltp Mesh::calc_ell(
		const arma::Row<fltp>& volume, 
		const arma::Row<fltp>& cs_total_area)const{
		return arma::accu(calc_cs_length(volume,cs_total_area));
	}

	// calculate total length
	fltp Mesh::calc_ell()const{
		return arma::accu(calc_cs_length());
	}


	// critical current calculation
	arma::Mat<fltp> Mesh::calc_curvature() const{
		// get nodes in area
		const arma::uword num_nodes = get_num_nodes();
		const arma::uword num_nodes_crss = num_base_nodes_;

		// allocate curvature
		arma::Mat<fltp> K(3,num_nodes);

		// walk over cross nodes
		for(arma::uword i=0;i<num_nodes_crss;i++){
			// indices of nodes
			const arma::Col<arma::uword> idx = 
				arma::regspace<arma::Col<arma::uword> >(
				i,num_nodes_crss,R_.n_cols-1);

			// coordinates
			arma::Mat<fltp> R = R_.cols(idx);
			if(is_loop_)R = arma::join_horiz(R.tail_cols(1), R, R.head_cols(1));

			// calculate velocity
			arma::Mat<fltp> V = arma::diff(R,1,1);
			if(!is_loop_){
				V = (arma::join_horiz(V.head_cols(1),V) + 
					arma::join_horiz(V,V.tail_cols(1)))/2;
			}
			
			// calculate accelerations
			arma::Mat<fltp> A = arma::diff(V,1,1);
			if(!is_loop_){
				A = (arma::join_horiz(A.head_cols(1),A) + 
					arma::join_horiz(A,A.tail_cols(1)))/2;
			}

			// average velocity
			if(is_loop_){
				V = (V.head_cols(V.n_cols-1) + V.tail_cols(V.n_cols-1))/2;
			}

			// acceleration in direction
			const arma::Mat<fltp> An = N_.cols(idx).eval().each_row()%cmn::Extra::dot(A,N_.cols(idx));
			const arma::Mat<fltp> Ad = D_.cols(idx).eval().each_row()%cmn::Extra::dot(A,D_.cols(idx));

			// calculate curvature for components separately
			const arma::Row<fltp> cn = cmn::Extra::vec_norm(cmn::Extra::cross(An,V))/arma::pow(cmn::Extra::vec_norm(V),3);
			const arma::Row<fltp> cd = cmn::Extra::vec_norm(cmn::Extra::cross(Ad,V))/arma::pow(cmn::Extra::vec_norm(V),3);

			// radius of curvature equation
			// https://math.libretexts.org/Bookshelves/Calculus/Supplemental_Modules_(Calculus)/Vector_Calculus/2%3A_Vector-Valued_Functions_and_Motion_in_Space/2.3%3A_Curvature_and_Normal_Vectors_of_a_Curve
			K.cols(idx) = N_.cols(idx).eval().each_row()%cn + D_.cols(idx).eval().each_row()%cd;
		}

		// return radius of curvature
		return K;
	}

	// export freecad
	arma::field<arma::Mat<fltp> > Mesh::get_edges(const bool full_perimeter) const{
		// check no edges case (for instance circular cross section)
		if(q_.empty())return{};

		// get edge list
		const arma::Col<arma::uword> p = full_perimeter ? p_.row(0).t() : q_;

		// get number of sections
		const arma::uword num_sections = num_cis_frame_.n_elem;
		const arma::uword num_edges = p.n_elem;

		// section indices
		const arma::Row<arma::uword> node_indices = arma::join_horiz(
			arma::Row<arma::uword>{0}, arma::cumsum<arma::Row<arma::uword> >(num_cis_frame_*num_base_nodes_));

		// allocate sections in edge matrices
		arma::field<arma::Mat<fltp> > Re(num_edges,num_sections);

		// walk over sections
		for(arma::uword i=0;i<num_sections;i++){
			// number of nodes in the section
			const arma::uword num_nodes_section = num_cis_frame_(i)*num_base_nodes_;

			// create indices for nodes
			arma::Row<arma::uword> idx = 
				arma::regspace<arma::Row<arma::uword> >(
				0,num_base_nodes_,num_nodes_section-1);

			// walk over edges
			// taking into account he offset for the corner
			// and the offset for the section
			for(arma::uword j=0;j<num_edges;j++)
				Re(j,i) = R_.cols(cmn::Extra::modulus(idx + p(j) + node_indices(i),R_.n_cols));
		}

		// return edge coordinates
		return Re;
	}

	// get xyz matrices
	void Mesh::create_xyz(
		arma::field<arma::Mat<fltp> > &x, 
		arma::field<arma::Mat<fltp> > &y, 
		arma::field<arma::Mat<fltp> > &z,
		arma::uword &num_edges,
		const bool full_perimeter) const{ 

		// get edges
		const arma::field<arma::Mat<fltp> > Re = get_edges(full_perimeter);

		// count number sectoins and edges
		const arma::uword num_sections = Re.n_cols;
		num_edges = Re.n_rows;

		// zero edges (for example in case of a circle)
		if(num_edges==0)return;

		// allocate
		x.set_size(1,num_sections);
		y.set_size(1,num_sections);
		z.set_size(1,num_sections);

		// walk over sections
		for(arma::uword i=0;i<num_sections;i++){
			// allocate edge matrix for this section
			x(i).set_size(num_edges,Re(0,i).n_cols);
			y(i).set_size(num_edges,Re(0,i).n_cols);
			z(i).set_size(num_edges,Re(0,i).n_cols);

			// walk over edges
			for(arma::uword j=0;j<num_edges;j++){
				// copy edge
				x(i).row(j) = Re(j,i).row(0);
				y(i).row(j) = Re(j,i).row(1);
				z(i).row(j) = Re(j,i).row(2);
			}
		}
	}

	// set direction
	void Mesh::set_dir(const rat::mat::Direction dir){
		dir_ = dir;
	}

	// set material
	void Mesh::set_material(const mat::ShConductorPr &material){
		material_ = material;
	}

	// get material properties
	bool Mesh::is_insulator()const{
		return material_->is_insulator(dir_);
	}

	// get direction
	rat::mat::Direction Mesh::get_dir()const{
		return dir_;
	}

	// get material
	const mat::ShConductorPr& Mesh::get_material() const{
		//assert(material_!=NULL);
		return material_;
	}

	// calculate the mass of this mesh
	fltp Mesh::calc_mass(const fltp temperature)const{
		// check if material set
		if(material_!=NULL){
			// calculate
			const fltp volume = calc_total_volume();
			const fltp density = material_->calc_density(temperature);

			// multiply
			return volume*density;
		}
		return RAT_CONST(0.0);
	}

	// calculate the energy density based on the temperature
	// energy density is given in [J m^-3]
	fltp Mesh::calc_thermal_energy(
		const fltp temperature, 
		const fltp delta_temperature) const{

		// check if material set
		if(material_!=NULL){
			const fltp volume = calc_total_volume();
			const fltp energy_density = 
				material_->calc_thermal_energy_density(
				temperature, delta_temperature);
			return volume*energy_density;
		}
		return RAT_CONST(0.0);
	}

	// calculate temperature increase
	fltp Mesh::calc_temperature_increase(
		const fltp initial_temperature, 
		const fltp added_heat,
		const fltp temperature_stepsize) const{

		// check if material set
		if(material_!=NULL){
			const fltp volume = calc_total_volume();
			const fltp temperature_increase = 
				material_->calc_temperature_increase(
				initial_temperature, volume, 
				added_heat, temperature_stepsize);
			return temperature_increase;
		}
		return RAT_CONST(0.0);
	}

	// calculate depth to center line
	arma::Row<fltp> Mesh::calc_depth()const{
		

		// allocate
		arma::Row<fltp> depth(get_num_surface(),arma::fill::zeros);

		// calculate face centroids
		const arma::Mat<fltp> Rf = cmn::Quadrilateral::calc_face_centroids(R_,s_);
		const arma::Mat<fltp> Nf = cmn::Quadrilateral::calc_face_normals(R_,s_);

		// walk over cable sections
		arma::uword idx1 = is_loop_ ? 0 : num_base_elements_;
		for(arma::uword j=0;j<num_cis_frame_.n_elem;j++){
			// number of intersections in this section
			const arma::uword num_cis = num_cis_frame_(j)-is_closed_(j);
			const arma::uword num_cs = num_cis-!is_closed_(j);

			// check center line
			assert(!Rc_(j).empty());
			assert(Rc_(j).is_finite());
			assert(Rc_(j).n_cols==num_cis_frame_(j));

			// walk over sections
			for(arma::uword i=0;i<num_cs;i++){
				// interpolate centerline to cable section
				const arma::Col<fltp>::fixed<3> Rc12 = (Rc_(j).col(i) + Rc_(j).col(i+1))/2;

				// calculate indices
				const arma::uword idx2 = idx1 + num_base_perimeter_ - 1;

				// calculate relative position
				const arma::Mat<fltp> dR = Rf.cols(idx1,idx2).eval().each_col() - Rc12;

				// calculate depth in the face normal direction
				depth.cols(idx1,idx2) = -cmn::Extra::dot(Nf.cols(idx1,idx2),dR);

				// shift
				idx1 += num_base_perimeter_;
			}

			// skip caps
			idx1 += 2*num_base_elements_;
		}

		// std::cout<<arma::join_horiz(Rf.t(),Nf.t(),depth.t())<<std::endl;

		// return thd depoth
		return depth;
	}

	// serialization
	std::string Mesh::get_type(){
		return "rat::mdl::mesh";
	}

	void Mesh::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Node::serialize(js,list);

		// type
		js["type"] = get_type();
		
		// material property
		js["dir"] = static_cast<int>(dir_);
		js["material"] = cmn::Node::serialize_node(material_, list);

		// area data
		js["base_area"] = base_area_;
		js["num_base_elements"] = static_cast<int>(num_base_elements_);
		js["num_base_nodes"] = static_cast<int>(num_base_nodes_);
		js["num_base_perimeter"] = static_cast<int>(num_base_perimeter_);
		js["num_base_perimeter"] = static_cast<int>(num_base_perimeter_);
		js["hidden_thickness"] = hidden_thickness_;
		js["hidden_width"] = hidden_width_;

		// frame data mostly section wise
		js["is_loop"] = is_loop_;
		for(arma::uword i=0;i<num_cis_frame_.n_elem;i++){
			js["is_closed"].append(static_cast<bool>(is_closed_(i)));
			js["section"].append(static_cast<int>(section_(i)));
			js["turn"].append(static_cast<int>(turn_(i)));
			js["num_cis_frame"].append(static_cast<int>(num_cis_frame_(i)));
			js["ell_frame"].append(ell_frame_(i));
		}

		// center line
		for(arma::uword i=0;i<Rc_.n_elem;i++){
			Json::Value jsrc;
			for(arma::uword j=0;j<Rc_(i).n_elem;j++)
				jsrc.append(Rc_(i)(j));
			js["Rc"].append(jsrc);
		}

		// mesh data
		js["is_extruded"] = is_extruded_;
		js["num_cs"] = static_cast<int>(num_cs_);
		js["num_cis"] = static_cast<int>(num_cis_);
		js["num_cs_elements"] = static_cast<int>(num_cs_elements_);
		js["num_cis_elements"] = static_cast<int>(num_cis_elements_);
		js["num_nodes"] = static_cast<int>(num_nodes_);

		// dimensionality of meshes
		js["n_dim"] = static_cast<int>(n_dim_);
		js["s_dim"] = static_cast<int>(s_dim_);

		// node coordinates
		for(arma::uword i=0;i<R_.n_elem;i++)js["R"].append(R_(i));

		// node orientation vectors
		for(arma::uword i=0;i<L_.n_elem;i++)js["L"].append(L_(i));
		for(arma::uword i=0;i<N_.n_elem;i++)js["N"].append(N_(i));
		for(arma::uword i=0;i<D_.n_elem;i++)js["D"].append(D_(i));

		// mesh element indexes
		for(arma::uword i=0;i<n_.n_elem;i++)js["n"].append(static_cast<int>(n_(i))); // volume (normally hex)
		for(arma::uword i=0;i<s_.n_elem;i++)js["s"].append(static_cast<int>(s_(i))); // surface (normally quad)
		for(arma::uword i=0;i<b_.n_elem;i++)js["b"].append(static_cast<int>(b_(i))); // base mesh
		for(arma::uword i=0;i<p_.n_elem;i++)js["p"].append(static_cast<int>(p_(i))); // base mesh perimeter
		for(arma::uword i=0;i<q_.n_elem;i++)js["q"].append(static_cast<int>(q_(i))); // corner nodes in 2D
	}
	
	void Mesh::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		Node::deserialize(js,list,factory_list,pth);

		// material property
		set_dir(mat::Direction(js["dir"].asInt()));
		set_material(cmn::Node::deserialize_node<mat::Conductor>(js["material"], list, factory_list, pth));

		// area data
		base_area_ = js["base_area"].ASFLTP();
		num_base_elements_ = js["num_base_elements"].asUInt64();
		num_base_nodes_ = js["num_base_nodes"].asUInt64();
		num_base_perimeter_ = js["num_base_perimeter"].asUInt64();
		num_base_perimeter_ = js["num_base_perimeter"].asUInt64();
		hidden_thickness_ = js["hidden_thickness"].ASFLTP();
		hidden_width_ = js["hidden_width"].ASFLTP();

		// frame data mostly section wise
		is_loop_ = js["is_loop"].asBool();

		// deserialize section wise data
		arma::uword idx;
		idx = 0; is_closed_.set_size(js["is_closed"].size()); for(auto it=js["is_closed"].begin();it!=js["is_closed"].end();it++)is_closed_(idx++) = (*it).asUInt64();
		idx = 0; section_.set_size(js["section"].size()); for(auto it=js["section"].begin();it!=js["section"].end();it++)section_(idx++) = (*it).asUInt64();
		idx = 0; turn_.set_size(js["turn"].size()); for(auto it=js["turn"].begin();it!=js["turn"].end();it++)turn_(idx++) = (*it).asUInt64();
		idx = 0; num_cis_frame_.set_size(js["num_cis_frame"].size()); for(auto it=js["num_cis_frame"].begin();it!=js["num_cis_frame"].end();it++)num_cis_frame_(idx++) = (*it).asUInt64();
		idx = 0; ell_frame_.set_size(js["ell_frame"].size()); for(auto it=js["ell_frame"].begin();it!=js["ell_frame"].end();it++)is_closed_(idx++) = (*it).asUInt64();

		// center line
		idx = 0;
		Rc_.set_size(1,js["Rc"].size());
		for(auto it=js["Rc"].begin();it!=js["Rc"].end();it++){
			const Json::Value jsrc = (*it);
			arma::uword idx2 = 0;
			Rc_(idx).set_size(3,jsrc.size()/3);
			for(auto it2=jsrc.begin();it2!=jsrc.end();it2++)
				Rc_(idx)(idx2++) = (*it2).ASFLTP();
			idx++;
		}

		// check
		assert(Rc_.n_elem==num_cis_frame_.n_elem);

		// mesh data
		is_extruded_ = js["is_extruded"].asBool();
		num_cs_ = js["num_cs"].asUInt64();
		num_cis_ = js["num_cis"].asUInt64();
		num_cs_elements_ = js["num_cs_elements"].asUInt64();
		num_cis_elements_ = js["num_cis_elements"].asUInt64();
		num_nodes_ = js["num_nodes"].asUInt64();

		// dimensionality of meshes
		n_dim_ = js["n_dim"].asUInt64();
		s_dim_ = js["s_dim"].asUInt64();

		// node coordinates
		idx = 0; R_.set_size(3,js["R"].size()/3); for(auto it=js["R"].begin();it!=js["R"].end();it++)R_(idx++) = (*it).ASFLTP();

		// node orientation vectors
		idx = 0; L_.set_size(3,js["L"].size()/3); for(auto it=js["L"].begin();it!=js["L"].end();it++)L_(idx++) = (*it).ASFLTP();
		idx = 0; N_.set_size(3,js["N"].size()/3); for(auto it=js["N"].begin();it!=js["N"].end();it++)N_(idx++) = (*it).ASFLTP();
		idx = 0; D_.set_size(3,js["D"].size()/3); for(auto it=js["D"].begin();it!=js["D"].end();it++)D_(idx++) = (*it).ASFLTP();

		// check
		if(R_.n_cols!=num_nodes_)rat_throw_line("deserialization of R yielded incorrect number of nodes");
		if(L_.n_cols!=num_nodes_)rat_throw_line("deserialization of L yielded incorrect number of nodes");
		if(N_.n_cols!=num_nodes_)rat_throw_line("deserialization of N yielded incorrect number of nodes");
		if(D_.n_cols!=num_nodes_)rat_throw_line("deserialization of D yielded incorrect number of nodes");

		// mesh element indexes
		idx = 0; const arma::uword nnr = std::exp2(n_dim_); n_.set_size(nnr,js["n"].size()/nnr); for(auto it=js["n"].begin();it!=js["n"].end();it++)n_(idx++) = (*it).asUInt64();
		idx = 0; const arma::uword snr = std::exp2(s_dim_); s_.set_size(snr,js["s"].size()/snr); for(auto it=js["s"].begin();it!=js["s"].end();it++)s_(idx++) = (*it).asUInt64();
		idx = 0; if(n_dim_!=0){const arma::uword bnr = std::exp2(n_dim_-1); b_.set_size(bnr,js["b"].size()/bnr); for(auto it=js["b"].begin();it!=js["b"].end();it++)b_(idx++) = (*it).asUInt64();}
		idx = 0; p_.set_size(2,js["p"].size()/2); for(auto it=js["p"].begin();it!=js["p"].end();it++)p_(idx++) = (*it).asUInt64();
		idx = 0; q_.set_size(js["q"].size()); for(auto it=js["q"].begin();it!=js["q"].end();it++)q_(idx++) = (*it).asUInt64();
	}

}}