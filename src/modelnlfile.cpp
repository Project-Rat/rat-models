// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// only available for non-linear solver
#ifdef ENABLE_NL_SOLVER

// include header
#include "modelnlfile.hh"

// rat models headers
#include "nldata.hh"

#include "rat/common/gmshfile.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ModelNLFile::ModelNLFile(const ShHBCurvePr &hb_curve){
		set_name("JSON File HB-Mesh");
		set_hb_curve(hb_curve);
	}

	// constructor
	ModelNLFile::ModelNLFile(
		const boost::filesystem::path& fname, 
		const fltp scale_factor,
		const ShHBCurvePr& hb_curve) : ModelNLFile(hb_curve){

		// set to self
		set_scale_factor(scale_factor);
		set_file_name(fname);
	}

	// factory immediately setting base and cross section
	ShModelNLFilePr ModelNLFile::create(const ShHBCurvePr &hb_curve){
		return std::make_shared<ModelNLFile>(hb_curve);
	}

	// factory immediately setting base and cross section
	ShModelNLFilePr ModelNLFile::create(
		const boost::filesystem::path& fname, 
		const fltp scale_factor,
		const ShHBCurvePr& hb_curve){
		return std::make_shared<ModelNLFile>(fname, scale_factor, hb_curve);
	}


	// setters
	void ModelNLFile::set_file_name(const boost::filesystem::path& fname){
		fname_ = fname;
	}

	void ModelNLFile::set_num_gauss_volume(const arma::sword num_gauss_volume){
		num_gauss_volume_ = num_gauss_volume;
	}

	void ModelNLFile::set_scale_factor(const fltp scale_factor){
		scale_factor_ = scale_factor;
	}

	void ModelNLFile::set_num_gauss_surface(const arma::sword num_gauss_surface){
		num_gauss_surface_ = num_gauss_surface;
	}
	
	void ModelNLFile::set_softening(const fltp softening){
		softening_ = softening;
	}

	void ModelNLFile::set_operating_temperature(const fltp operating_temperature){
		operating_temperature_ = operating_temperature;
	}


	// getters
	const boost::filesystem::path& ModelNLFile::get_file_name()const{
		return fname_;
	}

	fltp ModelNLFile::get_scale_factor()const{
		return scale_factor_;
	}

	fltp ModelNLFile::get_softening()const{
		return softening_;
	}

	arma::sword ModelNLFile::get_num_gauss_surface()const{
		return num_gauss_surface_;
	}

	arma::sword ModelNLFile::get_num_gauss_volume()const{
		return num_gauss_volume_;
	}

	fltp ModelNLFile::get_operating_temperature()const{
		return operating_temperature_;
	}


	// factory for mesh objects
	std::list<ShMeshDataPr> ModelNLFile::create_meshes(
		const std::list<arma::uword> &trace, 
		const MeshSettings &stngs)const{

		// trace
		if(!trace.empty())rat_throw_line("trace must be empty for leaf");

		// check enabled
		if(!get_enable())return{};

		// check model
		if(!is_valid(stngs.enable_throws))return{};

		// read json file
		const Json::Value js = cmn::Node::parse_json(fname_);

		// get nodes from json
		if(!js.isMember("Nodes"))rat_throw_line("json file needs nodes");
		const Json::Value& js_nodes = js["Nodes"];
		if(js_nodes.size()==0)rat_throw_line("there are no nodes");

		// get list of names
		const Json::Value::Members js_node_list = js_nodes.getMemberNames();

		// allocate nodes
		arma::Mat<fltp> Rn(3,js_nodes.size());

		// get nodes from json
		for(auto it=js_node_list.begin();it!=js_node_list.end();it++){
			arma::uword j=0;
			const arma::uword node_index = std::stoull(*it) - 1; // zero based indexing
			const Json::Value& js_node = js_nodes[*it];
			for(auto it2=js_node.begin();it2!=js_node.end();it2++,j++)
				Rn(j,node_index) = (*it2).ASFLTP();
		}

		// get tetrahedrons from json
		if(!js.isMember("Tetra4Elem"))rat_throw_line("json file needs tetrahedron elements");
		const Json::Value& js_tetrahedrons = js["Tetra4Elem"];
		if(js_tetrahedrons.size()==0)rat_throw_line("there are no first order tetrahedrons");

		// allocate elements
		arma::Mat<arma::uword> n(4,js_tetrahedrons.size());

		// get elements from json
		arma::uword i=0;
		for(auto it=js_tetrahedrons.begin();it!=js_tetrahedrons.end();it++,i++){
			arma::uword j=0;
			for(auto it2=(*it).begin();it2!=(*it).end();it2++,j++)
				n(j,i) = (*it2).asUInt64() - 1; // zero based indexing
		}

		// get triangles from json
		// if(!js.isMember("Tria3Elem"))rat_throw_line("json file needs tetrahedron elements");
		// const Json::Value& js_triangles = js["Tria3Elem"];

		// allocate elements
		// arma::Mat<arma::uword> s(3,js_triangles.size());

		// // get elements from json
		// i=0;
		// for(auto it=js_triangles.begin();it!=js_triangles.end();it++,i++){
		// 	arma::uword j=0;
		// 	for(auto it2=(*it).begin();it2!=(*it).end();it2++,j++)
		// 		s(j,i) = (*it2).asUInt64() - 1; // zero based indexing
		// }

		// // use zero based indexing
		// const cmn::ShGmshFilePr gmsh = cmn::GmshFile::create("test.gmsh");
		// gmsh->write_nodes(Rn);
		// gmsh->write_elements(s);

		// apply scaling
		Rn*=scale_factor_;

		// apply offset
		Rn.each_col()+=R0_;

		// merge nodes if they werent already
		const arma::Row<arma::uword> reindex = cmn::Extra::combine_nodes(Rn,combine_node_tolerance_);
		n = arma::reshape(reindex(arma::vectorise(n)),n.n_rows,n.n_cols);
		// s = arma::reshape(reindex(arma::vectorise(s)),s.n_rows,s.n_cols);

		// ensure clockwise
		const arma::Col<arma::uword> idx = arma::find(cmn::Tetrahedron::is_clockwise(Rn,n)==0);
		n.cols(idx) = cmn::Tetrahedron::invert_elements(n.cols(idx));
		if(arma::any(cmn::Tetrahedron::is_clockwise(Rn,n)==0))rat_throw_line("there are still non-clockwise elements");

		// extract surface triangles from volume mesh
		arma::Mat<arma::uword> s = cmn::Tetrahedron::extract_surface(Rn,n);
		// s = cmn::Triangle::invert_elements(s);

		// create mesh data object
		const ShNLDataPr nlmesh = NLData::create();

		// set to self
		nlmesh->setup(Rn,n,s);

		// create orientation
		arma::Mat<fltp> L(Rn.n_rows,Rn.n_cols,arma::fill::zeros); L.row(0).fill(RAT_CONST(1.0));
		arma::Mat<fltp> N(Rn.n_rows,Rn.n_cols,arma::fill::zeros); N.row(1).fill(RAT_CONST(1.0));
		arma::Mat<fltp> D(Rn.n_rows,Rn.n_cols,arma::fill::zeros); D.row(2).fill(RAT_CONST(1.0));

		// set orientation
		nlmesh->set_longitudinal(L); 
		nlmesh->set_normal(N); 
		nlmesh->set_transverse(D); 

		// determine dimensions for surface and volume mesh
		nlmesh->determine_dimensions();

		// set time
		nlmesh->set_time(stngs.time);

		// set temperature
		nlmesh->set_operating_temperature(operating_temperature_);
		nlmesh->set_temperature(operating_temperature_);

		// set name (is appended by models later)
		nlmesh->set_name(myname_);
		nlmesh->set_part_name(myname_);

		// apply transformations to mesh
		nlmesh->apply_transformations(get_transformations(),stngs.time);

		// set material
		nlmesh->set_material(conductor_);

		// copy properties
		if(hb_curve_!=NULL)if(hb_curve_->is_valid(false))nlmesh->set_hb_data(hb_curve_->create_hb_data());
		nlmesh->set_num_gauss(num_gauss_volume_, num_gauss_surface_);
		nlmesh->set_softening(softening_);

		// color
		if(use_custom_color_){
			nlmesh->set_use_custom_color(use_custom_color_);
			nlmesh->set_color(color_);
		}

		// return mesh data
		return {nlmesh};
	}

	// check validity
	bool ModelNLFile::is_valid(const bool enable_throws) const{
		// check mesh
		if(fname_.empty()){if(enable_throws){rat_throw_line("filename is not set");} return false;};
		// if(!ModelMesh::is_valid(enable_throws))return false;
		// check if path exists
		boost::system::error_code ec;
		if(!boost::filesystem::exists(fname_,ec)){if(enable_throws){rat_throw_line("mesh input file does not exist");} return false;};
		return true;
	}

	// get type
	std::string ModelNLFile::get_type(){
		return "rat::mdl::modelnlfile";
	}

	// method for serialization into json
	void ModelNLFile::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents first 
		// this allows the type to be overridden
		Model::serialize(js,list);

		// properties
		js["type"] = get_type();

		// serialize hb curve
		js["hb_curve"] = cmn::Node::serialize_node(hb_curve_, list);

		// file
		js["fname"] = fname_.generic_string();

		// number of gauss points
		js["num_gauss_volume"] = static_cast<int>(num_gauss_volume_);
		js["num_gauss_surface"] = static_cast<int>(num_gauss_surface_);

		// softening factor
		js["softening"] = softening_;

		// scaling
		js["scale_factor"] = scale_factor_;

		// temperature
		js["operating_temperature"] = operating_temperature_;
	}

	// method for deserialisation from json
	void ModelNLFile::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent objects
		Model::deserialize(js,list,factory_list,pth);

		// deserialize hb-curve
		hb_curve_ = cmn::Node::deserialize_node<HBCurve>(js["hb_curve"], list, factory_list, pth);

		// set file
		fname_ = Node::fix_filesep(js["fname"].asString());

		// check if path exists
		boost::system::error_code ec;
		if(!boost::filesystem::exists(fname_,ec)){
			if(boost::filesystem::exists(pth/fname_.filename(),ec))
				fname_ = pth/fname_.filename();
		}

		// number of gauss points
		if(js.isMember("num_gauss_volume"))set_num_gauss_volume(js["num_gauss_volume"].asInt64());
		if(js.isMember("num_gauss_surface"))set_num_gauss_surface(js["num_gauss_surface"].asInt64());

		// softening factor
		if(js.isMember("softening"))set_softening(js["softening"].ASFLTP());

		// temperature
		operating_temperature_ = js["operating_temperature"].ASFLTP();

		// scaling
		set_scale_factor(js["scale_factor"].ASFLTP());
	}

}}

#endif