// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathsub.hh"

// rat-common headers
#include "rat/common/error.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathSub::PathSub(){
		set_name("Sub");
	}

	// default constructor
	PathSub::PathSub(
		const ShPathPr& base, 
		const arma::uword idx1, 
		const arma::uword idx2) : PathSub(){
		set_input_path(base); set_idx(idx1,idx2);
	}

	// factory
	ShPathSubPr PathSub::create(){
		return std::make_shared<PathSub>();
	}

	// factory
	ShPathSubPr PathSub::create(
		const ShPathPr& base, 
		const arma::uword idx1, 
		const arma::uword idx2){
		return std::make_shared<PathSub>(base, idx1, idx2);
	}


	// set indexes
	void PathSub::set_idx(const arma::uword idx1, const arma::uword idx2){
		set_idx1(idx1); set_idx2(idx2);
	}

	void PathSub::set_idx1(const arma::uword idx1){
		idx1_ = idx1;
	}

	void PathSub::set_idx2(const arma::uword idx2){
		idx2_ = idx2;
	}

	// get indices
	arma::uword PathSub::get_idx1()const{
		return idx1_;
	}

	arma::uword PathSub::get_idx2()const{
		return idx2_;
	}

	// get frame
	ShFramePr PathSub::create_frame(const MeshSettings &stngs) const{
		// check if base path was set
		is_valid(true);

		// create frame
		const ShFramePr frame = input_path_->create_frame(stngs);

		// copy coordinates
		const arma::field<arma::Mat<fltp> > R = frame->get_coords();
		const arma::field<arma::Mat<fltp> > L = frame->get_direction();
		const arma::field<arma::Mat<fltp> > D = frame->get_transverse();
		const arma::field<arma::Mat<fltp> > N = frame->get_normal();
		const arma::field<arma::Mat<fltp> > B = frame->get_block();
		const arma::Row<arma::uword> section = frame->get_section();
		const arma::Row<arma::uword> turn = frame->get_turn();
		const arma::uword num_section_base = frame->get_num_section_base();

		// check indexes
		if(idx1_>idx2_)rat_throw_line("index1 must be smaller than index2");
		if(idx1_>=R.n_elem)rat_throw_line("index out of bounds: " + std::to_string(idx1_) + "/" + std::to_string(R.n_elem));
		if(idx2_>=R.n_elem)rat_throw_line("index out of bounds: " + std::to_string(idx2_) + "/" + std::to_string(R.n_elem));
		
		// create offset frame
		const ShFramePr sub_frame = Frame::create(
			R.cols(idx1_,idx2_),L.cols(idx1_,idx2_),
			N.cols(idx1_,idx2_),D.cols(idx1_,idx2_),
			B.cols(idx1_,idx2_));

		// conserve location
		sub_frame->set_location(
			section.cols(idx1_,idx2_), 
			turn.cols(idx1_,idx2_), 
			num_section_base);

		// apply transformations
		sub_frame->apply_transformations(get_transformations(), stngs.time);

		// create new frame
		return sub_frame;
	}

	// validity check
	bool PathSub::is_valid(const bool enable_throws)const{
		if(idx1_>idx2_){if(enable_throws){rat_throw_line("idx1 must be less than or equal to idx2");} return false;};
		if(!InputPath::is_valid(enable_throws))return false;
		return true;
	}

	// get type
	std::string PathSub::get_type(){
		return "rat::mdl::pathsub";
	}

	// method for serialization into json
	void PathSub::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		InputPath::serialize(js,list);

		// type
		js["type"] = get_type();

		// indices
		js["idx1"] = static_cast<int>(idx1_);
		js["idx2"] = static_cast<int>(idx2_);
	}

	// method for deserialisation from json
	void PathSub::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		InputPath::deserialize(js,list,factory_list,pth);

		// base backwards compatibility v2.015.3
		if(js.isMember("base")){
			set_input_path(cmn::Node::deserialize_node<Path>(js["base"],list,factory_list,pth));
		}

		// indices
		set_idx(js["idx1"].asUInt64(), js["idx2"].asUInt64());
	}


}}