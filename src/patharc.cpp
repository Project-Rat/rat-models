// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "patharc.hh"

// rat-common headers
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathArc::PathArc(){
		// set default name
		set_name("Arc");

		// set parameters
		set_radius(0.1); set_element_size(2e-3);
		set_arc_length(arma::Datum<fltp>::pi/2);
		set_offset(0.0);
	}

	// constructor with dimension input
	PathArc::PathArc(
		const fltp radius, const fltp arc_length,
		const fltp element_size, const fltp offset,
		const bool transverse) : PathArc(){

		// set parameters
		set_radius(radius); set_element_size(element_size);
		set_arc_length(arc_length); set_offset(offset);
		set_transverse(transverse);
	}

	// factory
	ShPathArcPr PathArc::create(){
		return std::make_shared<PathArc>();
	}

	// factory with dimension input
	ShPathArcPr PathArc::create(
		const fltp radius, const fltp arc_length,
		const fltp element_size, const fltp offset,
		const bool transverse){
		return std::make_shared<PathArc>(radius,arc_length,element_size,offset,transverse);
	}

	// set transverse
	void PathArc::set_transverse(const bool transverse){
		transverse_ = transverse;
	}

	// set path offset
	void PathArc::set_offset(const fltp offset){
		offset_ = offset;
	}

	// set element size
	void PathArc::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	// set element size
	void PathArc::set_arc_length(const fltp arc_length){
		arc_length_ = arc_length;
	}

	// set element size
	void PathArc::set_radius(const fltp radius){
		radius_ = radius;
	}

	// set transverse
	bool PathArc::get_transverse()const{
		return transverse_;
	}

	// set path offset
	fltp PathArc::get_offset()const{
		return offset_;
	}

	// set element size
	fltp PathArc::get_element_size()const{
		return element_size_;
	}

	// set element size
	fltp PathArc::get_arc_length()const{
		return arc_length_;
	}

	// set element size
	fltp PathArc::get_radius()const{
		return radius_;
	}

	// method for creating the frame
	ShFramePr PathArc::create_frame(const MeshSettings &stngs) const{
		// check if is valid
		is_valid(true);

		// decide which part of the circle to use
		fltp uoff,poff,rhos;
		if(arc_length_<0){
			rhos = -radius_;
			uoff = radius_; 
			poff = -std::min(RAT_CONST(0.0),offset_);
		}else{
			rhos = radius_; 
			uoff = -radius_; 
			poff = std::max(RAT_CONST(0.0),offset_);
		}

		// calculate number of nodes
		arma::uword num_nodes = std::max(2llu,static_cast<arma::uword>(std::ceil(std::abs(arc_length_)*(radius_+poff)/element_size_)+1));
		while((num_nodes-1)%stngs.element_divisor!=0)num_nodes++;

		// create cylindrical coordinates for circle
		const arma::Row<fltp> rhovec = arma::linspace<arma::Row<fltp> >(rhos,rhos,num_nodes);
		const arma::Row<fltp> thetavec = arma::linspace<arma::Row<fltp> >(0,arc_length_,num_nodes);

		// allocate
		arma::Mat<fltp> R(3,num_nodes,arma::fill::zeros);
		arma::Mat<fltp> L(3,num_nodes,arma::fill::zeros);
		arma::Mat<fltp> D(3,num_nodes,arma::fill::zeros);
		arma::Mat<fltp> N(3,num_nodes,arma::fill::zeros);

		// bend in transverse direction
		if(transverse_){
			// coordinates
			R.row(1) = rhovec%arma::sin(thetavec);
			R.row(2) = rhovec%arma::cos(thetavec)+uoff;

			// direction
			L.row(1) = arma::cos(thetavec);
			L.row(2) = -arma::sin(thetavec);

			// normal vector
			N.row(0).fill(RAT_CONST(1.0));

			// set remaining orientation vector
			D = cmn::Extra::cross(N,L);
		}

		// bend in normal direction
		else{
			// coordinates
			R.row(0) = rhovec%arma::cos(thetavec)+uoff;
			R.row(1) = rhovec%arma::sin(thetavec);

			// direction
			L.row(0) = -arma::sin(thetavec);
			L.row(1) = arma::cos(thetavec);
				
			// direction vector
			D.row(2).fill(RAT_CONST(1.0));

			// set remaining orientation vector
			N = cmn::Extra::cross(L,D);
		}

		// check handedness
		assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N,L),D)>RAT_CONST(0.0)));

		// create frame and return
		return Frame::create(R,L,N,D);
	}

	// validity check
	bool PathArc::is_valid(const bool enable_throws) const{
		if(radius_<=0){if(enable_throws){rat_throw_line("radius must be larger than zero");} return false;};
		if(arc_length_==0){if(enable_throws){rat_throw_line("arc length must be larger than zero");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string PathArc::get_type(){
		return "rat::mdl::patharc";
	}

	// method for serialization into json
	void PathArc::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Path::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["transverse"] = transverse_;
		js["offset"] = offset_;
		js["element_size"] = element_size_;
		js["radius"] = radius_;
		js["arc_length"] = arc_length_;
	}

	// method for deserialisation from json
	void PathArc::deserialize(const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent
		Path::deserialize(js,list,factory_list,pth);

		// properties
		set_transverse(js["transverse"].asBool());
		set_offset(js["offset"].ASFLTP());
		set_element_size(js["element_size"].ASFLTP());
		set_radius(js["radius"].ASFLTP());
		set_arc_length(js["arc_length"].ASFLTP());
	}

}}

