// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calcmlfmm.hh"

// model headers
#include "cartesiangriddata.hh"

// rat-common headers
#include "rat/common/error.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcMlfmm::CalcMlfmm(){
		set_name("MLFMM");
	}

	// constructor immediately setting base and cross section
	CalcMlfmm::CalcMlfmm(const ShModelPr &model) : CalcMlfmm(){
		set_model(model);
	}

	// factory with input
	ShCalcMlfmmPr CalcMlfmm::create(){
		return std::make_shared<CalcMlfmm>();
	}

	// factory with input
	ShCalcMlfmmPr CalcMlfmm::create(const ShModelPr &model){
		return std::make_shared<CalcMlfmm>(model);
	}

	// enable the calculation of field on the mesh
	void CalcMlfmm::set_target_meshes(const bool target_meshes){
		target_meshes_ = target_meshes;
	}

	// output meshes even if not used as targets
	void CalcMlfmm::set_output_meshes(const bool output_meshes){
		output_meshes_ = output_meshes;
	}

	// enable calculation of the field on the grid
	void CalcMlfmm::set_target_grid(const bool target_grid){
		target_grid_ = target_grid;
	}

	// enable calculation of the field on the grid
	void CalcMlfmm::set_target_surface(const bool target_surface){
		target_surface_ = target_surface;
	}

	// enable calculation of the field on the grid
	void CalcMlfmm::set_background(const ShBackgroundPr bg){
		bg_ = bg;
	}

	// add additional target
	void CalcMlfmm::add_target(const ShTargetDataPr &target){
		// check input
		if(target==NULL)rat_throw_line("model points to zero");

		// get number of models
		const arma::uword num_targets = targets_.n_elem;

		// allocate new source list
		ShTargetDataPrList new_targets(num_targets + 1);

		// set old and new sources
		for(arma::uword i=0;i<num_targets;i++)new_targets(i) = targets_(i);
		new_targets(num_targets) = target;
		
		// set new source list
		targets_ = new_targets;
	}

	// add additional target
	void CalcMlfmm::add_targets(const std::list<ShTargetDataPr> &target){
		for(auto it = target.begin();it!=target.end();it++){
			add_target(*it);
		}
	}

	// no time calculation t=0
	std::list<ShDataPr> CalcMlfmm::calculate(
		const fltp time, 
		const cmn::ShLogPr &lg, 
		const ShSolverCachePr& cache){
		
		// calculate
		std::list<ShTargetDataPr> target_data = calculate_targets(time,lg,cache);
		
		// convert to data 
		std::list<ShDataPr> data;
		for(auto it = target_data.begin();it!=target_data.end();it++)
			data.push_back(*it);

		// return data
		return data;
	}

	// run calculation
	std::list<ShTargetDataPr> CalcMlfmm::calculate_targets(
		const fltp time, 
		const cmn::ShLogPr &lg, 
		const ShSolverCachePr& cache){

		// header
		lg->msg(2,"%s%sSETTING UP MESHES%s\n",KGRN,KBLD,KNRM);

		// list of output objects
		// output objects
		std::list<ShTargetDataPr> output_objects;

		// check input model
		if(model_==NULL)rat_throw_line("model not set");
		model_->is_valid(true);
		
		// settings for mesh creation
		MeshSettings mesh_settings;
		mesh_settings.time = time;
		mesh_settings.low_poly = false;
		mesh_settings.combine_sections = true;

		// create a list of meshes
		std::list<ShMeshDataPr> meshes = model_->create_meshes({},mesh_settings);

		// set circuit currents
		apply_circuit_currents(time, meshes);
		
		// display function
		MeshData::display(lg,meshes);

		// end mesh setup
		lg->msg(-2,"\n");

		// combine sources and targets
		fmm::ShMultiTargets2Pr tar = fmm::MultiTargets2::create();

		// // set meshes as sources for MLFMM
		// for(auto it=meshes.begin();it!=meshes.end();it++)
		// 	src->add_sources((*it)->create_sources());
		
		// always add meshes on list of outputs
		if(target_meshes_ || output_meshes_){
			for(auto it=meshes.begin();it!=meshes.end();it++){
				const ShMeshDataPr& mesh = (*it);
				mesh->set_field_type("AHM",{3,3,3});
				output_objects.push_back(mesh);
			}
		}

		// add meshes to MLFMM targets
		if(target_meshes_){
			for(auto it=meshes.begin();it!=meshes.end();it++){
				// add target to MLFMM
				tar->add_targets(*it);	
			}
		}

		// mesh surface calculation
		// only if the mesh itself is not a target
		if(target_surface_ && !target_meshes_){
			for(auto it=meshes.begin();it!=meshes.end();it++){
				// create surface
				ShSurfaceDataPr surf = (*it)->create_surface();

				// add target to MLFMM
				tar->add_targets(surf);

				// add to list of outputs
				output_objects.push_back(surf);
			}
		}

		// add aditional targets
		tar->add_targets(targets_);
		for(arma::uword i=0;i<targets_.n_elem;i++)
			output_objects.push_back(targets_(i));

		// grid calculation
		mdl::ShCartesianGridDataPr grid;
		if(target_grid_){
			grid = mdl::CartesianGridData::create(meshes, 1.3, 2e-3, 5e6);
			grid->set_field_type("ABHM",{3,3,3,3});
			grid->append_name("enclosing grid");
			tar->add_targets(grid);
			output_objects.push_back(grid);
		}

		// run calculation
		calculate_field(meshes,tar,time,lg,cache);

		// mesh surface calculation
		// by extracting field values from the meshes
		if(target_surface_ && target_meshes_){
			for(auto it=meshes.begin();it!=meshes.end();it++){
				// create surface
				ShSurfaceDataPr surf = (*it)->create_surface();

				// add to list of outputs
				output_objects.push_back(surf);
			}
		}

		// return the output data
		return output_objects;
	}

	// get type
	std::string CalcMlfmm::get_type(){
		return "rat::mdl::calcmlfmm";
	}

	// method for serialization into json
	void CalcMlfmm::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		CalcLeaf::serialize(js,list);
		js["type"] = get_type();
	}

	// method for deserialisation from json
	void CalcMlfmm::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent
		CalcLeaf::deserialize(js,list,factory_list,pth);
	}


}}