// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "crossdmsh.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CrossDMsh::CrossDMsh(){
		set_name("Distmesh");
	}

	// constructors
	CrossDMsh::CrossDMsh(const fltp h0, 
		const dm::ShDistFunPr &fd, 
		const dm::ShDistFunPr &fh) : CrossDMsh(){
		set_distfun(fd); set_scalefun(fh); set_h0(h0);
	}

	// factory methods
	ShCrossDMshPr CrossDMsh::create(){
		return std::make_shared<CrossDMsh>();
	}

	// factory methods
	ShCrossDMshPr CrossDMsh::create(const fltp h0, const dm::ShDistFunPr &fd, const dm::ShDistFunPr &fh){
		return std::make_shared<CrossDMsh>(h0,fd,fh);
	}

	// get and set functions
	void CrossDMsh::set_distfun(const dm::ShDistFunPr &fd){
		fd_ = fd;
	}

	void CrossDMsh::set_scalefun(const dm::ShDistFunPr &fh){
		fh_ = fh;
	}

	void CrossDMsh::set_fixed(const arma::Mat<fltp> &pfix_ext){
		assert(pfix_ext.n_cols==2);
		pfix_ext_ = pfix_ext;
	}

	void CrossDMsh::set_h0(const fltp h0){
		h0_ = h0;
	}

	void CrossDMsh::set_dptol(const fltp dptol){
		dptol_ = dptol;
	}

	void CrossDMsh::set_max_iter(const arma::uword max_iter){
		max_iter_ = max_iter;
	}

	void CrossDMsh::set_num_node_limit(const arma::uword num_node_limit){
		num_node_limit_ = num_node_limit;
	}

	fltp CrossDMsh::get_h0()const{
		return h0_;
	}

	fltp CrossDMsh::get_dptol()const{
		return dptol_;
	}

	arma::uword CrossDMsh::get_max_iter()const{
		return max_iter_;
	}

	void CrossDMsh::set_rng_seed(const unsigned int rng_seed){
		rng_seed_ = rng_seed;
	}

	void CrossDMsh::set_rng_strength(const fltp rng_strength){
		rng_strength_ = rng_strength;
	}


	// getters
	const dm::ShDistFunPr& CrossDMsh::get_distfun()const{
		return fd_;
	}

	const dm::ShDistFunPr& CrossDMsh::get_scalefun()const{
		return fh_;
	}

	arma::uword CrossDMsh::get_num_node_limit()const{
		return num_node_limit_;
	}

	fltp CrossDMsh::get_rng_strength()const{
		return rng_strength_;
	}

	
	unsigned int CrossDMsh::get_rng_seed()const{
		return rng_seed_;
	}

	// get bounding box
	arma::Mat<fltp>::fixed<2,2> CrossDMsh::get_bounding() const{
		return fd_->get_bounding();
	}

	// create distmesher
	dm::ShDistMesh2DPr CrossDMsh::create_distmesher()const{
		// call distmesh2d
		const dm::ShDistMesh2DPr dmsh = dm::DistMesh2D::create();
		dmsh->set_num_node_limit(num_node_limit_);
		dmsh->set_rng_seed(rng_seed_);
		dmsh->set_rng_strength(rng_strength_);

		// setup distmesher
		dmsh->set_quad(quad_);
		if(quad_)dmsh->set_h0(std::sqrt(RAT_CONST(2.0))*h0_); else dmsh->set_h0(h0_);
		dmsh->set_dptol(dptol_);
		dmsh->set_maxiter(max_iter_);
		dmsh->set_distfun(fd_);
		if(fh_!=NULL)dmsh->set_scalefun(fh_);
		dmsh->set_fixed(pfix_ext_);

		// return mesher
		return dmsh;
	}

	// create the meshed area
	ShAreaPr CrossDMsh::create_area(const MeshSettings &/*stngs*/) const{
		// check validity
		is_valid(true);

		// create mesher
		const dm::ShDistMesh2DPr dmsh = create_distmesher();

		// run mesher
		dmsh->setup();

		// get nodes and elements
		const arma::Mat<fltp> Rn = dmsh->get_nodes().t();
		arma::Mat<arma::uword> n = dmsh->get_elements().t();

		// get orientation
		arma::Mat<fltp> N(2,Rn.n_cols,arma::fill::zeros); N.row(0).fill(RAT_CONST(1.0));
		arma::Mat<fltp> D(2,Rn.n_cols,arma::fill::zeros); D.row(1).fill(RAT_CONST(1.0));

		// circle area
		ShAreaPr area = Area::create(Rn,N,D,n);

		// setup
		area->setup_perimeter();
		area->setup_corner_nodes();

		// center point
		const arma::Col<fltp> Rnmean = arma::mean(Rn,1);
		area->set_center_coord(Rnmean);

		// return positive
		return area;
	}

	// // surface mesh
	// ShPerimeterPr CrossDMsh::create_perimeter() const{
	// 	// create area mesh
	// 	ShAreaPr area_mesh = create_area(MeshSettings());

	// 	// extract periphery and return
	// 	return area_mesh->extract_perimeter();
	// }

	bool CrossDMsh::is_valid(const bool enable_throws) const{
		if(h0_<=0){if(enable_throws){rat_throw_line("initial element size must be larger than zero");} return false;};
		if(dptol_<=0){if(enable_throws){rat_throw_line("tolerance must be larger than zero");} return false;};
		if(num_node_limit_<=0){if(enable_throws){rat_throw_line("node limit must be larger than zero");} return false;};
		if(fd_==NULL){if(enable_throws){rat_throw_line("distance function points to NULL");} return false;};
		if(!fd_->is_valid(enable_throws))return false;
		if(fh_!=NULL)if(!fh_->is_valid(enable_throws))return false;
		if(max_iter_<1){if(enable_throws){rat_throw_line("max iter must be larger than zero");} return false;};

		// check distance function bounding box
		const arma::Mat<fltp>::fixed<2,2> bbox = fd_->get_bounding();
		if(arma::any(bbox.row(0)>=bbox.row(1))){if(enable_throws){rat_throw_line("negatively or zero spaced bounding box");} return false;};

		return true;
	}

	// get type
	std::string CrossDMsh::get_type(){
		return "rat::mdl::crossdmsh";
	}

	// method for serialization into json
	void CrossDMsh::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Cross::serialize(js,list);

		// type
		js["type"] = get_type();

		// quad mesher enabled
		js["quad"] = quad_;

		// element size
		js["h0"] = h0_;

		// tolerance
		js["dptol"] = dptol_;

		// seed for random number generator
		js["rng_seed"] = rng_seed_;
		js["rng_strength"] = rng_strength_;

		// externally supplied fixed point list
		for(arma::uword i=0;i<pfix_ext_.n_rows;i++){
			js["pfix_x"].append(pfix_ext_(i,0));
			js["pfix_y"].append(pfix_ext_(i,1));
		}

		// serialize geometry and scaling distance functions
		js["geometryfun"] = cmn::Node::serialize_node(fd_,list);
		js["scalingfun"] = cmn::Node::serialize_node(fh_,list);

		// node number limit
		js["num_node_limit"] = static_cast<int>(num_node_limit_);

		// maximum number of iterations
		js["max_iter"] = static_cast<int>(max_iter_);
	}

	// method for deserialisation from json
	void CrossDMsh::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent
		Cross::deserialize(js,list,factory_list,pth);

		// quad mesher enabled 
		quad_ = js["quad"].asBool();

		// element size
		set_h0(js["h0"].ASFLTP());

		// point tolerance
		if(js.isMember("dptol"))set_dptol(js["dptol"].ASFLTP());

		// seed for rng
		set_rng_seed(js["rng_seed"].asUInt());
		set_rng_strength(js["rng_strength"].ASFLTP());

		// externally supplied fixed point list
		pfix_ext_.set_size(js["pfix_x"].size(),2);
		auto it1 = js["pfix_x"].begin(); auto it2 = js["pfix_y"].begin(); arma::uword idx = 0;
		for(;it1!=js["pfix_x"].end() && it2!=js["pfix_y"].end();it1++,it2++,idx++){
			pfix_ext_(idx,0) = (*it1).ASFLTP(); 
			pfix_ext_(idx,1) = (*it2).ASFLTP();
		}

		// deserialize distance and scaling functions
		fd_ = cmn::Node::deserialize_node<dm::DistFun>(js["geometryfun"], list, factory_list, pth);
		fh_ = cmn::Node::deserialize_node<dm::DistFun>(js["scalingfun"], list, factory_list, pth);

		// node number limit
		if(js.isMember("num_node_limit"))set_num_node_limit(js["num_node_limit"].asUInt64());

		// maximum number of iterations
		if(js.isMember("max_iter"))set_max_iter(js["max_iter"].asUInt64());
	}

}}