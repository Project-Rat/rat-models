// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathellipse.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathEllipse::PathEllipse(){
		set_name("Ellipse");
		semi_major_ = RAT_CONST(0.05);
		semi_minor_ = RAT_CONST(0.1);
		element_size_ = RAT_CONST(2e-3);
		offset_ = RAT_CONST(0.0);
	}

	// constructor with dimension input
	PathEllipse::PathEllipse(
		const fltp semi_major, const fltp semi_minor, const arma::uword num_sections,
		const fltp element_size, const fltp offset) : PathEllipse(){

		// set to self
		set_semi_major(semi_major);
		set_semi_minor(semi_minor);
		set_num_sections(num_sections);
		set_element_size(element_size);
		set_offset(offset);
	}

	// factory
	ShPathEllipsePr PathEllipse::create(){
		return std::make_shared<PathEllipse>();
	}

	// factory with dimension input
	ShPathEllipsePr PathEllipse::create(
		const fltp semi_major, const fltp semi_minor, const arma::uword num_sections,
		const fltp element_size, const fltp offset){
		return std::make_shared<PathEllipse>(semi_major,semi_minor,num_sections,element_size,offset);
	}

	// set transverse
	void PathEllipse::set_transverse(const bool transverse){
		transverse_ = transverse;
	}

	// set element size 
	void PathEllipse::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	// path offset
	void PathEllipse::set_offset(const fltp offset){
		offset_ = offset;
	}

	// set semi major
	void PathEllipse::set_semi_major(const fltp semi_major){
		semi_major_ = semi_major;
	}

	// set semi minor
	void PathEllipse::set_semi_minor(const fltp semi_minor){
		semi_minor_ = semi_minor;
	}

	// set num sections
	void PathEllipse::set_num_sections(const arma::uword num_sections){
		num_sections_ = num_sections;
	}

	// get transverse
	bool PathEllipse::get_transverse()const{
		return transverse_;
	}

	// get element size 
	fltp PathEllipse::get_element_size() const{
		return element_size_;
	}

	// get semi major
	fltp PathEllipse::get_semi_major() const{
		return semi_major_;
	}

	// get semi minor
	fltp PathEllipse::get_semi_minor() const{
		return semi_minor_;
	}

	// get num sections
	arma::uword PathEllipse::get_num_sections() const{
		return num_sections_;
	}

	// get offset
	fltp PathEllipse::get_offset() const{
		return offset_;
	}

	// get frame
	ShFramePr PathEllipse::create_frame(const MeshSettings &stngs) const{
		// check validity
		is_valid(true);

		// check input
		// if(semi_major_<=0)rat_throw_line("semi major must be larger than zero");
		// if(semi_minor_<=0)rat_throw_line("semi minor must be larger than zero");
		// if(element_size_<=0)rat_throw_line("element size must be larger than zero");
		// if(num_sections_<=0)rat_throw_line("number of sections must be larger than zero");

		// create unified path
		const ShPathGroupPr pathgroup = PathGroup::create();

		// add sections
		for(arma::uword i=0;i<num_sections_;i++){
			ShPathEllipticalArcPr arc = PathEllipticalArc::create(semi_major_,semi_minor_,
					arma::Datum<fltp>::tau/num_sections_*i,(arma::Datum<fltp>::tau/num_sections_)*(i+1),element_size_);
			arc->set_offset(offset_);
			arc->set_transverse(transverse_);
			pathgroup->add_path(arc);
		}

		// move
		if(!transverse_)pathgroup->add_translation(semi_minor_,0,0); else pathgroup->add_translation(0,0,semi_minor_);

		// create frame
		const ShFramePr frame = pathgroup->create_frame(stngs);

		// apply transformations
		frame->apply_transformations(get_transformations(), stngs.time);
		
		// return frame
		return frame;
	}

	// validity check
	bool PathEllipse::is_valid(const bool enable_throws) const{
		if(semi_major_<=0){if(enable_throws){rat_throw_line("semi major must be larger than zero");} return false;};
		if(semi_minor_<=0){if(enable_throws){rat_throw_line("semi minor must be larger than zero");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string PathEllipse::get_type(){
		return "rat::mdl::pathellipse";
	}

	// method for serialization into json
	void PathEllipse::serialize(Json::Value &js, cmn::SList &list) const{
		// parent nodes
		Transformations::serialize(js,list);

		// settings
		js["type"] = get_type();
		js["transverse"] = transverse_;
		js["semi_major"] = semi_major_;
		js["semi_minor"] = semi_minor_;
		js["element_size"] = element_size_;
		js["offset"] = offset_;
	}

	// method for deserialisation from json
	void PathEllipse::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent nodes
		Transformations::deserialize(js,list,factory_list,pth);

		// settings
		set_transverse(js["transverse"].asBool());
		set_semi_major(js["semi_major"].ASFLTP());
		set_semi_minor(js["semi_minor"].ASFLTP());
		set_element_size(js["element_size"].ASFLTP());
		set_offset(js["offset"].ASFLTP());
	}

}}