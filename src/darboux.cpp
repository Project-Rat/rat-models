// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "darboux.hh"

// rat-common headers
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	Darboux::Darboux(){

	}

	// constructor
	Darboux::Darboux(
		const arma::Mat<fltp> &V, 
		const arma::Mat<fltp> &A, 
		const arma::Mat<fltp> &J){
		V_ = V; A_ = A; J_ = J;
	}

	// factory
	ShDarbouxPr Darboux::create(){
		return std::make_shared<Darboux>();
	}

	// factory
	ShDarbouxPr Darboux::create(
		const arma::Mat<fltp> &V, 
		const arma::Mat<fltp> &A, 
		const arma::Mat<fltp> &J){
		return std::make_shared<Darboux>(V,A,J);
	}

	// additional twist
	void Darboux::set_additional_twist(
		const arma::Row<fltp>& zeta, 
		const arma::Row<fltp>& dzeta){
		zeta_ = zeta; dzeta_ = dzeta;
	}


	// calculate frame
	void Darboux::setup(const bool analytic_frame){
		// check velocity and acceleration
		assert(V_.n_rows==3); 

		// normalize velocity
		const arma::Row<fltp> normV = cmn::Extra::vec_norm(V_);
		T_ = V_.each_row()/normV; // previously called T

		// analytic version
		if(analytic_frame){
			// check jerk
			assert(J_.n_rows==3);
			assert(A_.n_rows==3); 
			assert(V_.n_cols==J_.n_cols);
			assert(V_.n_cols==A_.n_cols);

			// setup frame
			B_ = cmn::Extra::cross(V_,A_); 
			const arma::Row<fltp> normB = cmn::Extra::vec_norm(B_);

			// calculate 
			kappa_ = normB/(normV%normV%normV);
			tau_ = cmn::Extra::dot(B_,J_)/(normB%normB);

			// normalize B
			B_.each_row()/=normB;

			// apply rotation
			if(!zeta_.empty()){
				// rotate kappa
				const arma::Row<fltp> kappa_n = kappa_%arma::cos(zeta_);
				// const arma::Row<fltp> kappa_g = kappa_%arma::sin(zeta_);

				// rotate tau
				tau_ += dzeta_;

				// get normal vector
				const arma::Mat<fltp> N = cmn::Extra::cross(T_,B_);

				// rotate B
				B_ = B_.each_row()%arma::cos(zeta_) + N.each_row()%arma::sin(zeta_);

				// normal curvature and ignore geodesic curvature
				kappa_ = kappa_n;
			}

			// frame	
			D_ = tau_%T_.each_row() + kappa_%B_.each_row();

			// normalize darboux vectors
			D_.each_row() /= cmn::Extra::vec_norm(D_);

			// Scale frame
			D_.each_row() %= arma::sqrt(RAT_CONST(1.0)+arma::square(tau_/kappa_));
		}

		// numeric
		else{
			// not yet implemented
			if(!zeta_.empty())rat_throw_line("numerical darboux vectors does not support yet twisting.");

			// number of points
			const arma::uword num_times = V_.n_cols;

			// mean normal vector
			arma::Mat<fltp> N = T_.tail_cols(num_times-1) - T_.head_cols(num_times-1);

			// extend
			N = (arma::join_horiz(N.head_cols(1),N) + arma::join_horiz(N,N.tail_cols(1)))/2;

			// calculate binormal vector
			B_ = cmn::Extra::cross(T_,N); B_.each_row()/=cmn::Extra::vec_norm(B_);

			// create frame
			D_ = cmn::Extra::cross(N.head_cols(num_times-1),N.tail_cols(num_times-1));

			// extend
			D_ = (arma::join_horiz(D_.head_cols(1),D_) + arma::join_horiz(D_,D_.tail_cols(1)))/2;

			// normalize darboux vectors
			D_.each_row() /= cmn::Extra::vec_norm(D_);

			// calculate angle
			const arma::Row<fltp> alpha = arma::acos(cmn::Extra::dot(-T_,D_));

			// Scale frame
			D_.each_row() /= arma::sin(alpha);

			// set nan for values that have no normal vector
			const arma::Col<arma::uword> idx = arma::find(cmn::Extra::vec_norm(N)<1e-6);
			D_.cols(idx).fill(arma::Datum<fltp>::nan);
			B_.cols(idx).fill(arma::Datum<fltp>::nan);
			T_.cols(idx).fill(arma::Datum<fltp>::nan);
			
			// reverse
			// D_*=-1; B_*=-1;
		}

	}

	int Darboux::correct_sign(){
		return correct_sign(D_.col(0));
	}

	// correct sign flips
	int Darboux::correct_sign(const arma::Col<fltp>::fixed<3> &dstart){
		// check sign flips
		const arma::Row<fltp> sf = 
			cmn::Extra::dot(arma::join_horiz(
			dstart,D_.head_cols(D_.n_cols-1)),D_);

		// flip D when curvature changes from positive to negative
		int flipstate = 1;
		for(arma::uword i=0;i<D_.n_cols;i++){
			// check whether flip is needed
			if(sf(i)<0)flipstate*=-1;

			// flip vectors (if needed)
			D_.col(i)*=flipstate;
			B_.col(i)*=flipstate;

			// // stability fix
			// if(std::abs(sf(i))<1e-7 && i!=0){
			// 	D_.col(i)=D_.col(i-1);
			// 	B_.col(i)=B_.col(i-1);
			// }
		}

		return flipstate;
	}


	void Darboux::set_first_transverse(const arma::Col<fltp>::fixed<3> &D){
		assert(!D_.is_empty());
		D_.head_cols(1) = D; B_.head_cols(1) = D;
	}

	void Darboux::set_last_transverse(const arma::Col<fltp>::fixed<3> &D){
		assert(!D_.is_empty());
		D_.tail_cols(1) = D; B_.tail_cols(1) = D;
	}

	// get orientation vectors
	const arma::Mat<fltp>& Darboux::get_longitudinal() const{
		assert(!T_.is_empty());
		return T_;
	}

	// get darboux vector
	const arma::Mat<fltp>& Darboux::get_transverse(const bool use_binormal) const{
		assert(!D_.is_empty()); assert(!B_.is_empty());
		return use_binormal ? B_ : D_;
	}

	// get darboux vector
	const arma::Mat<fltp>& Darboux::get_darboux()const{
		assert(!D_.is_empty());
		return D_;
	}

	// get binormal vector
	const arma::Mat<fltp>& Darboux::get_binormal()const{
		assert(!B_.is_empty());
		return B_;
	}

	// get normal vector
	arma::Mat<fltp> Darboux::get_normal() const{
		return cmn::Extra::cross(T_,B_.each_row()/cmn::Extra::vec_norm(B_));
	}

}}