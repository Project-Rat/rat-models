// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calcleaf.hh"

// nonlinear solver headers
#ifdef ENABLE_NL_SOLVER
#include "rat/nl/nonlinsolver.hh"
#endif

// mlfmm headers
#include "rat/mlfmm/mlfmm.hh"

// model headers
#include "nldata.hh"
#include "coildata.hh"
#include "bardata.hh"
#include "extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcLeaf::CalcLeaf(){
		stngs_ = fmm::Settings::create();
	}

	// set and get input path
	void CalcLeaf::set_settings(const fmm::ShSettingsPr &stngs){
		stngs_ = stngs;
	}
	
	void CalcLeaf::set_background(const ShBackgroundPr &bg){
		bg_ = bg;
	}

	const ShBackgroundPr& CalcLeaf::get_background() const{
		return bg_;
	}

	// get settings
	const fmm::ShSettingsPr& CalcLeaf::get_settings() const{
		return stngs_;
	}

	// validity check
	bool CalcLeaf::is_valid(const bool enable_throws) const{
		if(stngs_==NULL){if(enable_throws){rat_throw_line("settings points to NULL");} return false;};
		if(!stngs_->is_valid(enable_throws)){if(enable_throws){rat_throw_line("settings are not valid");} return false;};
		return true;
	}

	// add model function
	void CalcLeaf::set_model(const ShModelPr &model){
		if(model==NULL)rat_throw_line("model is NULL pointer");
		model_ = model;
	}

	// add model function
	const ShModelPr& CalcLeaf::get_model(){
		return model_;
	}

	// calculate field on targets
	void CalcLeaf::calculate_field(
		const std::list<ShMeshDataPr>& meshes, 
		const fmm::ShTargetsPr& tar, 
		const fltp time, 
		const cmn::ShLogPr &lg, 
		#ifdef ENABLE_NL_SOLVER
		const ShSolverCachePr& cache)const{
		#else
		const ShSolverCachePr& /*cache*/)const{
		#endif

		// currentsources for calculating vector potential
		fmm::ShMultiSourcesPr src_a = fmm::MultiSources::create();

		// charge sources for calculating scalar potential
		fmm::ShMultiSourcesPr src_v = fmm::MultiSources::create();

		// bound current sources
		fmm::ShMultiSourcesPr src_b = fmm::MultiSources::create();

		// filter out nonlinear meshes and bar meshes for later magnetization calculation
		#ifdef ENABLE_NL_SOLVER
		std::list<ShNLDataPr> nl_meshes;
		#endif
		std::list<ShBarDataPr> bar_meshes;

		// walk over meshes
		for(auto it=meshes.begin();it!=meshes.end();it++){
			// calculation meshes
			if((*it)->get_calc_mesh())continue;

			// coil data
			const ShCoilDataPr coil_data = std::dynamic_pointer_cast<CoilData>(*it);
			if(coil_data!=NULL){
				// create current sources
				src_a->add_sources(coil_data->create_currents());
			}

			// bar data
			const ShBarDataPr bar_data = std::dynamic_pointer_cast<BarData>(*it);
			if(bar_data!=NULL){
				// create fictitious magnetic charges
				const fmm::ShSourcesPr surf_charges = bar_data->create_surface_charges();
				if(surf_charges!=NULL)src_v->add_sources(surf_charges);
				const fmm::ShMgnChargesPr vol_charges = bar_data->create_vol_charges();
				if(vol_charges!=NULL)src_v->add_sources(vol_charges);

				// create bound currents for vector potential calcultion
				const fmm::ShSourcesPr bnd_surf_currents = bar_data->create_bnd_surf_currents();
				if(bnd_surf_currents!=NULL)src_b->add_sources(bnd_surf_currents);
				const fmm::ShSourcesPr bnd_vol_currents = bar_data->create_bnd_vol_currents();
				if(bnd_vol_currents!=NULL)src_b->add_sources(bnd_vol_currents);
				const fmm::ShInterpPr interp = bar_data->create_magnetization_interp();
				if(interp!=NULL)src_a->add_sources(interp);

				// add to bar meshes list
				bar_meshes.push_back(bar_data);
			}

			// NL data
			#ifdef ENABLE_NL_SOLVER
			const ShNLDataPr nl_data = std::dynamic_pointer_cast<NLData>(*it);
			if(nl_data!=NULL){
				// add to nl meshes list
				nl_meshes.push_back(nl_data);

				// add to targets independent of what it is
				nl_data->set_field_type('M',3); nl_data->setup_targets(); nl_data->allocate();
			}
			#endif
		}

		// check for cancel
		if(lg->is_cancelled())return;

		// check if any nonlinear meshes present
		#ifdef ENABLE_NL_SOLVER
		if(!nl_meshes.empty() && (src_a->get_num_source_objects()!=0 || src_v->get_num_source_objects()!=0 || bg_!=NULL)){
			// create nonlinear solver
			const nl::ShNonLinSolverPr nl_solver = nl::NonLinSolver::create();
			nl_solver->set_use_gpu(stngs_->get_fmm_enable_gpu());
			nl_solver->set_gpu_devices(stngs_->get_gpu_devices());
			nl_solver->set_lg(lg); // maybe this should be a direct transver later

			// combine meshes
			arma::uword idx = 0, nodeshift = 0;
			arma::field<arma::Mat<fltp> > Rnfld(1,nl_meshes.size());
			arma::field<arma::Mat<arma::uword> > nfld(1,nl_meshes.size());
			arma::field<arma::Row<arma::uword> > n2part(1,nl_meshes.size());
			arma::field<nl::ShHBDataPr> hb(1,nl_meshes.size());
			for(auto it=nl_meshes.begin();it!=nl_meshes.end();it++){
				Rnfld(idx) = (*it)->get_nodes();
				nfld(idx) = (*it)->get_elements() + nodeshift;
				hb(idx) = (*it)->get_hb_data();
				if(hb(idx)==NULL)rat_throw_line("HB Curve was not supplied for: " + (*it)->get_name());
				n2part(idx) = arma::Row<arma::uword>(nfld(idx).n_cols,arma::fill::value(idx));
				nodeshift += Rnfld(idx).n_cols; idx++; 
			}

			// convert to matrices
			arma::Mat<fltp> Rn = cmn::Extra::field2mat(Rnfld);
			arma::Mat<arma::uword> n = cmn::Extra::field2mat(nfld);

			// set mesh and hb curves to solver
			nl_solver->set_mesh(Rn, n);
			nl_solver->set_n2part(cmn::Extra::field2mat(n2part)); nl_solver->set_hb_curve(hb);

			// setup mesh by calculating the face and edge connectivity matrices
			nl_solver->setup_mesh();

			// calculate external field
			const rat::fmm::ShMgnTargetsPr nltar = nl_solver->get_ext_targets();

			// allocate targets
			nltar->allocate();

			// check if any vector potential sources
			if(src_a->get_num_source_objects()!=0){
				// general info
				lg->msg("%s%s=== CURRENTS TO NLMESH ===%s\n",KBLD,KGRN,KNRM);

				// multipole method using vector potentials
				const fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(src_a, nltar, stngs_);
				mlfmm->set_no_allocate();

				// setup mlfmm and calculate
				mlfmm->setup(lg); 
				
				// check for cancel
				if(lg->is_cancelled())return;

				// run calculation
				mlfmm->calculate(lg);

				// check for cancel
				if(lg->is_cancelled())return;
			}

			// check if any scalar potential sources
			if(src_v->get_num_source_objects()!=0){
				// general info
				lg->msg("%s%s=== CHARGES TO NLMESH ===%s\n",KBLD,KGRN,KNRM);

				// multipole method using scalar potentials
				const fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(src_v, nltar, stngs_);
				mlfmm->set_no_allocate();

				// setup mlfmm and calculate
				mlfmm->setup(lg); 

				// check for cancel
				if(lg->is_cancelled())return;

				// run calculation
				mlfmm->calculate(lg);

				// check for cancel
				if(lg->is_cancelled())return;
			}

			// calculate background field
			if(bg_!=NULL)bg_->create_fmm_background(time)->calc_field(nltar);

			// set external field
			nl_solver->set_ext_field(nltar);

			// general info
			lg->msg("%s%s=== NONLINEAR SOLVE ===%s\n",KBLD,KGRN,KNRM);

			// run nonlinear solver
			const arma::Row<rat::fltp> Ae = nl_solver->solve(cache).t();

			// check for cancel
			if(lg->is_cancelled())return;

			// calculate magnetization at elements
			const arma::Mat<fltp> Mg = nl_solver->calc_elem_magnetization(Ae); 	
			const arma::Mat<fltp> Me = nl_solver->element_average(Mg);

			// walk over non-linear meshes
			arma::uword elem_shift = 0;
			for(auto it=nl_meshes.begin();it!=nl_meshes.end();it++){
				// get number of elements in this mesh
				const arma::uword num_elements = (*it)->get_num_elements();

				// set field to respective non-linear meshes
				(*it)->set_magnetisation(Me.cols(elem_shift,elem_shift+num_elements-1),false);

				// shift position
				elem_shift += num_elements;

				// // extrapolate values from gauss points for entire mesh
				// (*it)->set_magnetisation(cmn::Hexahedron::gauss2nodes(
				// 	(*it)->get_nodes(), (*it)->get_elements(), 
				// 	Mg.cols(elem_shift,elem_shift+num_elements*nl_solver->get_num_gauss_element()-1), 
				// 	nl_solver->get_elem_gauss_coords(), 
				// 	nl_solver->get_elem_gauss_weights(), 
				// 	(*it)->get_num_nodes()),true);

				// // shift position
				// elem_shift += num_elements*nl_solver->get_num_gauss_element();
			}

			// create sources
			for(auto it=nl_meshes.begin();it!=nl_meshes.end();it++){
				// create fictitious magnetic charges to represent the non-linear mesh
				const fmm::ShSourcesPr surf_charges = (*it)->create_surface_charges();
				if(surf_charges!=NULL)src_v->add_sources(surf_charges);

				const fmm::ShMgnChargesPr vol_charges = (*it)->create_vol_charges();
				if(vol_charges!=NULL)src_v->add_sources(vol_charges);

				// create the bound currents to represent the non-linear mesh
				const fmm::ShSourcesPr bnd_surf_currents = (*it)->create_bnd_surf_currents();
				if(bnd_surf_currents!=NULL)src_b->add_sources(bnd_surf_currents);

				const fmm::ShCurrentSourcesPr bnd_vol_currents = (*it)->create_bnd_vol_currents();
				if(bnd_vol_currents!=NULL)src_b->add_sources(bnd_vol_currents);

				// interpolation for the magnetization
				const fmm::ShInterpPr interp = (*it)->create_magnetization_interp();
				if(interp!=NULL)src_a->add_sources(interp);
			}

			// // create fictitious magnetic charges to represent the non-linear mesh
			// const fmm::ShMgnChargesPr surf_charges = nl_solver->create_surf_charges(Ae);
			// if(surf_charges!=NULL)src_v->add_sources(surf_charges);
			// const fmm::ShMgnChargesPr vol_charges = nl_solver->create_vol_charges(Ae);
			// if(vol_charges!=NULL)src_v->add_sources(vol_charges);

			// // create the bound currents to represent the non-linear mesh
			// const fmm::ShCurrentSourcesPr bnd_surf_currents = nl_solver->create_surf_currents(Ae);
			// if(bnd_surf_currents!=NULL)src_b->add_sources(bnd_surf_currents);
			// const fmm::ShCurrentSourcesPr bnd_vol_currents = nl_solver->create_vol_currents(Ae);
			// if(bnd_vol_currents!=NULL)src_b->add_sources(bnd_vol_currents);

			// // calculate magnetization at nodes
			// const arma::Mat<fltp> Mg = nl_solver->calc_elem_magnetization(Ae); 	
			// const arma::Mat<fltp> Me = nl_solver->element_average(Mg);
			// arma::Mat<fltp> Mn(Rn.n_rows,Rn.n_cols,arma::fill::zeros);
			// arma::Row<fltp> num_elem(Rn.n_cols,arma::fill::zeros);
			// for(arma::uword i=0;i<n.n_cols;i++){
			// 	for(arma::uword j=0;j<n.n_rows;j++){
			// 		Mn.col(n(j,i)) += Me.col(i);
			// 		num_elem.col(n(j,i)) += RAT_CONST(1.0);
			// 	}
			// }
			// Mn.each_row()/=num_elem;

			// // // calculate magnetization at nodes
			// // arma::Mat<fltp> Mn = nl_solver->calc_nodal_magnetization(Ae);

			// // interpolation of magnetization
			// const fmm::ShInterpPr interp = fmm::Interp::create();
			// interp->set_mesh(Rn,n);
			// interp->set_interpolation_values('M',Mn);
			// src_a->add_sources(interp);

			// check for cancel
			if(lg->is_cancelled())return;
		}
		#endif

		// allocate targets
		tar->setup_targets();
		tar->allocate();

		// coil currents
		if(src_a->get_num_source_objects()!=0){
			// general info
			lg->msg("%s%s=== COIL CURRENTS TO TARGETS ===%s\n",KBLD,KGRN,KNRM);

			// multipole method
			const fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(src_a, tar, stngs_);
			mlfmm->set_no_allocate();

			// setup mlfmm and calculate
			mlfmm->setup(lg); 

			// check for cancel
			if(lg->is_cancelled())return;

			// run calculation
			mlfmm->calculate(lg);

			// check for cancel
			if(lg->is_cancelled())return;
		}

		// bound magnetic charges for calculating magnetic field 
		if(src_v->get_num_source_objects()!=0){
			// general info
			lg->msg("%s%s=== CHARGES TO TARGETS ===%s\n",KBLD,KGRN,KNRM);

			// create temporary target points
			const fmm::ShMgnTargetsPr tar_h = fmm::MgnTargets::create(tar->get_target_coords());
			tar_h->set_field_type('H',3);

			// multipole method
			const fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(src_v, tar_h, stngs_);

			// setup mlfmm and calculate
			mlfmm->setup(lg); 

			// check for cancel
			if(lg->is_cancelled())return;

			// run calculation
			mlfmm->calculate(lg);

			// check for cancel
			if(lg->is_cancelled())return;

			// get field and copy to real targets
			tar->add_field('H',tar_h->fmm::MgnTargets::get_field('H')); // copy field at gauss points
		}

		// bound surface currents for calculating vector potential
		if(src_b->get_num_source_objects()!=0){
			// general info
			lg->msg("%s%s=== BND CURRENTS TO TARGETS ===%s\n",KBLD,KGRN,KNRM);

			// create temporary target points
			const fmm::ShMgnTargetsPr tar_b = fmm::MgnTargets::create(tar->get_target_coords());
			tar_b->set_field_type("AB",{3,3});

			// multipole method
			const fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(src_b, tar_b, stngs_);

			// setup mlfmm and calculate
			mlfmm->setup(lg); 

			// check for cancel
			if(lg->is_cancelled())return;

			// run calculation
			mlfmm->calculate(lg);

			// check for cancel
			if(lg->is_cancelled())return;

			// get field and copy to real targets
			tar->add_field('A',tar_b->fmm::MgnTargets::get_field('A')); // copy field at gauss points
			tar->add_field('B',tar_b->fmm::MgnTargets::get_field('B')); // copy field at gauss points
		}

		// calculate background field
		if(bg_!=NULL)bg_->create_fmm_background(time)->calc_field(tar);

		// perform post processing
		tar->post_process();
	}


	// get type
	std::string CalcLeaf::get_type(){
		return "rat::mdl::calcleaf";
	}

	// method for serialization into json
	void CalcLeaf::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		Calc::serialize(js,list);
		InputCircuits::serialize(js,list);

		// type
		js["type"] = get_type();

		// serialize settings
		js["stngs"] = cmn::Node::serialize_node(stngs_, list);
		js["model"] = cmn::Node::serialize_node(model_, list);
		js["bg"] = cmn::Node::serialize_node(bg_, list);
	}

	// method for deserialisation from json
	void CalcLeaf::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent objects
		Calc::deserialize(js,list,factory_list,pth);
		InputCircuits::deserialize(js,list,factory_list,pth);

		// deserialize settings
		if(js.isMember("stngs"))set_settings(cmn::Node::deserialize_node<fmm::Settings>(js["stngs"], list, factory_list, pth));
		set_model(cmn::Node::deserialize_node<Model>(js["model"], list, factory_list, pth));
		set_background(cmn::Node::deserialize_node<Background>(js["bg"], list, factory_list, pth));
	}


}}
