// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "polargriddata.hh"

// common headers
#include "rat/common/marchingcubes.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PolarGridData::PolarGridData(){
		// set for magnetic field calculation
		set_field_type("HB",{3,3}); 
		set_output_type("grid");
	}

	// constructor
	PolarGridData::PolarGridData(
		const char axis,
		const fltp rin, const fltp rout, const arma::uword num_rad,
		const fltp theta1, const fltp theta2, const arma::uword num_theta,
		const fltp zlow, const fltp zhigh, const arma::uword num_axial,
		const arma::Col<fltp>::fixed<3> offset){
			
		set_axis(axis);
		set_rin(rin); set_rout(rout); set_num_rad(num_rad);
		set_theta1(theta1); set_theta2(theta2); set_num_theta(num_theta);
		set_zlow(zlow); set_zhigh(zhigh); set_num_axial(num_axial);
		set_offset(offset);
	}

	// factory
	ShPolarGridDataPr PolarGridData::create(){
		return std::make_shared<PolarGridData>();
	}

	// factory
	ShPolarGridDataPr PolarGridData::create(
		const char axis,
		const fltp rin, const fltp rout, const arma::uword num_rad,
		const fltp theta1, const fltp theta2, const arma::uword num_theta,
		const fltp zlow, const fltp zhigh, const arma::uword num_axial,
		const arma::Col<fltp>::fixed<3> offset){
		return std::make_shared<PolarGridData>(
			axis,rin,rout,num_rad,theta1,theta2,
			num_theta,zlow,zhigh,num_axial,offset);
	}

	// setters
	void PolarGridData::set_offset(const arma::Col<fltp>::fixed<3> &offset){
		offset_ = offset;
	}

	void PolarGridData::set_axis(const char axis){
		axis_ = axis;
	}

	void PolarGridData::set_rin(const fltp rin){
		rin_ = rin;
	}
	
	void PolarGridData::set_rout(const fltp rout){
		rout_ = rout;
	}
	
	void PolarGridData::set_theta1(const fltp theta1){
		theta1_ = theta1;
	}

	void PolarGridData::set_theta2(const fltp theta2){
		theta2_ = theta2;
	}
	
	void PolarGridData::set_zlow(const fltp zlow){
		zlow_ = zlow;
	}
	
	void PolarGridData::set_zhigh(const fltp zhigh){
		zhigh_ = zhigh;
	}
	
	void PolarGridData::set_num_rad(const arma::uword num_rad){
		num_rad_ = num_rad;
	}

	void PolarGridData::set_num_theta(const arma::uword num_theta){
		num_theta_ = num_theta;
	}
	
	void PolarGridData::set_num_axial(const arma::uword num_axial){
		num_axial_ = num_axial;
	}
	
	// getters
	arma::Col<fltp>::fixed<3> PolarGridData::get_offset() const{
		return offset_;
	}

	char PolarGridData::get_axis()const{
		return axis_;
	}

	fltp PolarGridData::get_rin()const{
		return rin_;
	}

	fltp PolarGridData::get_rout()const{
		return rout_;
	}

	fltp PolarGridData::get_theta1()const{
		return theta1_;
	}

	fltp PolarGridData::get_theta2()const{
		return theta2_;
	}

	fltp PolarGridData::get_zlow()const{
		return zlow_;
	}

	fltp PolarGridData::get_zhigh()const{
		return zhigh_;
	}

	arma::uword PolarGridData::get_num_rad()const{
		return num_rad_;
	}

	arma::uword PolarGridData::get_num_theta()const{
		return num_theta_;
	}

	arma::uword PolarGridData::get_num_axial()const{
		return num_axial_;
	}

	// setup function
	void PolarGridData::setup_targets(){
		// arrays defining coordinates
		const arma::Row<fltp> rho = arma::linspace<arma::Row<fltp> >(rin_,rout_,num_rad_);
		const arma::Row<fltp> theta = arma::linspace<arma::Row<fltp> >(theta1_,theta2_,num_theta_);
		const arma::Row<fltp> zz = arma::linspace<arma::Row<fltp> >(zlow_,zhigh_,num_axial_);
		
		// set number of targets
		num_targets_ = num_rad_*num_theta_*num_axial_;

		// allocate coordinates
		Rt_.set_size(3,num_targets_);

		// walk over x
		for(arma::uword i=0;i<num_theta_;i++){
			for(arma::uword j=0;j<num_axial_;j++){
				// calculate indexes
				const arma::uword idx1 = i*num_rad_*num_axial_ + j*num_rad_;
				const arma::uword idx2 = i*num_rad_*num_axial_ + (j+1)*num_rad_ - 1;

				// convert to carthesian coordinates
				const arma::Row<fltp> x = rho*std::cos(theta(i));
				const arma::Row<fltp> y = rho*std::sin(theta(i));
				const arma::Row<fltp> z(num_rad_, arma::fill::value(zz(j)));

				// set coordinates
				if(axis_ == 'z')Rt_.cols(idx1,idx2) = arma::join_vert(x,y,z);
				else if(axis_ == 'x')Rt_.cols(idx1,idx2) = arma::join_vert(z,x,y);
				else if(axis_ == 'y')Rt_.cols(idx1,idx2) = arma::join_vert(x,z,y);
				else rat_throw_line("axis not recognized");
			}
		}

		// apply offset
		Rt_.each_col()+=offset_;
	}

	// get polar coordinates
	// returns rho; phi; z
	arma::Mat<fltp> PolarGridData::get_target_polar_coords()const{
		// arrays defining coordinates
		const arma::Row<fltp> rho = arma::linspace<arma::Row<fltp> >(rin_,rout_,num_rad_);
		const arma::Row<fltp> theta = arma::linspace<arma::Row<fltp> >(theta1_,theta2_,num_theta_);
		const arma::Row<fltp> zz = arma::linspace<arma::Row<fltp> >(zlow_,zhigh_,num_axial_);
		
		// set number of targets
		const arma::uword num_targets = num_rad_*num_theta_*num_axial_;

		// allocate coordinates
		arma::Row<fltp> rrho(num_targets);
		arma::Row<fltp> rtheta(num_targets);
		arma::Row<fltp> rzz(num_targets);
		
		// walk over x
		for(arma::uword i=0;i<num_theta_;i++){
			for(arma::uword j=0;j<num_axial_;j++){
				// calculate indexes
				const arma::uword idx1 = i*num_rad_*num_axial_ + j*num_rad_;
				const arma::uword idx2 = i*num_rad_*num_axial_ + (j+1)*num_rad_ - 1;

				// set values
				rrho.cols(idx1,idx2) = rho;
				rtheta.cols(idx1,idx2).fill(theta(i));
				rzz.cols(idx1,idx2).fill(zz(j));
			}
		}

		// return coordinates
		return arma::join_vert(rrho,rtheta,rzz);
	}

	// extract iso-surfaces
	ShMeshDataPr PolarGridData::extract_isosurface(
		const arma::Row<fltp> &values,
		const fltp iso_value, 
		const bool use_parallel) const{

		// // get values
		// arma::Row<fltp> values;
		// switch(component){
		// 	case 'x': values = get_field(field_type).row(0); break;
		// 	case 'y': values = get_field(field_type).row(1); break;
		// 	case 'z': values = get_field(field_type).row(2); break;
		// 	case 'm': values = cmn::Extra::vec_norm(get_field(field_type)); break;
		// 	default: rat_throw_line("component not recognized");
		// }

		// run marching cubes algorithm
		const cmn::ShMarchingCubesPr marching = cmn::MarchingCubes::create();
		marching->setup_hex2isotri();
		arma::Mat<rat::fltp> nodes = marching->polygonise(Rt_,values,create_elements(),iso_value);

		// create elements
		arma::Mat<arma::uword> elements = arma::reshape(
			arma::regspace<arma::Col<arma::uword> >(0,nodes.n_cols-1),3,nodes.n_cols/3);

		// combine nodes 
		const arma::Row<arma::uword> idx = cmn::Extra::combine_nodes(nodes);

		// re-index elements
		elements = arma::reshape(idx(arma::vectorise(elements)),elements.n_rows,elements.n_cols);

		// get rid of sliver elements
		elements = elements.cols(arma::find(cmn::Triangle::calc_area(nodes,elements)>0));
		
		// export to VTK
		const ShMeshDataPr md = MeshData::create(nodes, elements, elements);

		// clear the field type list of the surface
		md->clear_field_type();

		// transfer field
		if(has_field()){
			// transfer list of field types
			for(auto it = field_type_.begin();it!=field_type_.end();it++)
				md->add_field_type((*it).first, (*it).second);
		}

		// transfer orientation
		md->set_longitudinal(interpolate(get_azymuthal(), md->get_nodes(), use_parallel));
		md->set_normal(interpolate(get_radial(), md->get_nodes(), use_parallel));
		md->set_transverse(interpolate(get_axial(), md->get_nodes(), use_parallel));

		// allocate surface to take the data
		md->setup_targets();
		md->allocate(); 

		// transfer the data
		for(auto it = field_type_.begin();it!=field_type_.end();it++){
			arma::Mat<fltp> fld = interpolate(get_field((*it).first), md->get_nodes(), use_parallel);
			md->add_field((*it).first, fld);
		}

		// output meshdata
		return md;
	}

	// interpolate
	arma::Mat<fltp> PolarGridData::interpolate(
		const arma::Mat<fltp> &values, 
		const arma::Mat<fltp> &R, 
		const bool use_parallel) const{

		// check field
		assert(!values.has_nan());
		assert(values.is_finite());

		// apply offset
		const arma::Mat<fltp> Roff = R.each_col() - offset_;

		// get grid spacing
		const fltp drho = (rout_ - rin_)/(num_rad_-1);
		const fltp dz = (zhigh_ - zlow_)/(num_axial_-1);
		const fltp dtheta = (theta2_ - theta1_)/(num_theta_-1);

		// interpolated values
		arma::Mat<fltp> interpolated_values(values.n_rows, R.n_cols);

		// convert coords to cylindrical coordinate system
		arma::Row<fltp> theta, rho, zz;
		if(axis_=='x'){theta = arma::atan2(Roff.row(2),Roff.row(1)); rho = arma::hypot(Roff.row(1), Roff.row(2)); zz = Roff.row(0);}
		else if(axis_=='y'){theta = arma::atan2(Roff.row(2),Roff.row(0)); rho = arma::hypot(Roff.row(0), Roff.row(2)); zz = Roff.row(1);}
		else if(axis_=='z'){theta = arma::atan2(Roff.row(1),Roff.row(0)); rho = arma::hypot(Roff.row(0), Roff.row(1)); zz = Roff.row(2);}
		else rat_throw_line("axis not recognized");
		theta(arma::find(theta<RAT_CONST(0.0)))+=arma::Datum<fltp>::tau;

		// improve interval
		while(arma::any(theta<theta1_))theta(arma::find(theta<theta1_))+=arma::Datum<fltp>::tau;
		while(arma::any(theta>theta2_))theta(arma::find(theta>theta2_))-=arma::Datum<fltp>::tau;

		// walk over coordinates
		// walk over meshes
		cmn::parfor(0,R.n_cols,use_parallel,[&](int i, int /*cpu*/){
			// get coordinate and index of lower corner
			const fltp rtheta = theta(i) - theta1_, rrho = rho(i) - rin_, rz = zz(i) - zlow_;
			const arma::uword rhoi = std::min(num_rad_-2,arma::uword(rrho/drho)); 
			const arma::uword zi = std::min(num_axial_-2,arma::uword(rz/dz));
			const arma::uword thetai = std::min(num_theta_-2,arma::uword(rtheta/dtheta));

			// check indexes
			assert(thetai<num_theta_-1); assert(rhoi<num_rad_-1); assert(zi<num_axial_-1);

			// calculate relative position in box
			const arma::Col<fltp>::fixed<3> Rq{
				2*(rrho - rhoi*drho)/drho - RAT_CONST(1.0), 
				2*(rz - zi*dz)/dz - RAT_CONST(1.0),
				2*(rtheta - thetai*dtheta)/dtheta - RAT_CONST(1.0)};

			// values at corners
			// values are stored rad first, then axial, then azymuthal
			arma::Mat<fltp>::fixed<3,8> V;
			V.col(0) = values.col(thetai*num_rad_*num_axial_ + zi*num_rad_ + rhoi);
			V.col(1) = values.col(thetai*num_rad_*num_axial_ + zi*num_rad_ + rhoi+1);
			V.col(2) = values.col(thetai*num_rad_*num_axial_ + (zi+1)*num_rad_ + rhoi+1);
			V.col(3) = values.col(thetai*num_rad_*num_axial_ + (zi+1)*num_rad_ + rhoi);
			V.col(4) = values.col((thetai+1)*num_rad_*num_axial_ + zi*num_rad_ + rhoi);
			V.col(5) = values.col((thetai+1)*num_rad_*num_axial_ + zi*num_rad_ + rhoi+1);
			V.col(6) = values.col((thetai+1)*num_rad_*num_axial_ + (zi+1)*num_rad_ + rhoi+1);
			V.col(7) = values.col((thetai+1)*num_rad_*num_axial_ + (zi+1)*num_rad_ + rhoi);

			// interpolate
			// note that due to the elements being straight hexahedrons and the grid being
			// a round circle it may be possible for the coordinates to lie slightly outside of the element
			interpolated_values.col(i) = cmn::Hexahedron::quad2cart(V,Rq);
		});

		// check field
		assert(!interpolated_values.has_nan());
		assert(interpolated_values.is_finite());
		assert(interpolated_values.n_cols==R.n_cols);
		assert(interpolated_values.n_rows==values.n_rows);

		// return interpolated values
		return interpolated_values;
	}

	// write surface to VTK file
	ShVTKObjPr PolarGridData::export_vtk() const{
		// create vtk data
		const ShVTKUnstrPr vtk_data = VTKUnstr::create();

		// set target points as nodes
		vtk_data->set_nodes(Rt_);
		
		// create and set the elements
		vtk_data->set_elements(create_elements(),12); // 12 = hexahedron

		// set name
		vtk_data->set_name(get_name());

		// magnetic vector potential
		if(has('A') && has_field())vtk_data->set_nodedata(get_field('A'),"Vector Potential [Vs/m]");

		// magnetic flux density
		if(has('B') && has_field())vtk_data->set_nodedata(get_field('B'),"Mgn. Flux Density [T]");

		// magnetic field
		if(has('H') && has_field())vtk_data->set_nodedata(get_field('H'),"Magnetic Field [A/m]");

		// magnetisation
		if(has('M') && has_field())vtk_data->set_nodedata(get_field('M'),"Magnetisation [A/m]");

		// return data object
		return vtk_data;
	}

	// create elements
	arma::Mat<arma::uword> PolarGridData::create_elements()const{
		// create 2D element indices
		const arma::Mat<arma::uword> node_idx = arma::reshape(arma::regspace<arma::Col<arma::uword> >(0,num_rad_*num_axial_-1), num_rad_, num_axial_);
		
		// create 2D elements
		const arma::uword num_node_plane = num_axial_*num_rad_;
		const arma::uword num_element_plane = (num_axial_-1)*(num_rad_-1);
		arma::Mat<arma::uword> n2d(4,num_element_plane);
		for(arma::uword k=0;k<num_rad_-1;k++)
			for(arma::uword j=0;j<num_axial_-1;j++)
				n2d.col(k*(num_axial_-1)+j) = arma::Col<arma::uword>::fixed<4>{
					node_idx(k,j), node_idx(k,j+1), node_idx(k+1,j+1), node_idx(k+1,j)};

		// create 3D elements
		const arma::uword num_volume = num_element_plane*(num_theta_-1);
		arma::Mat<arma::uword> n(8,num_volume);
		for(arma::uword i=0;i<num_theta_-1;i++)
			n.cols(i*num_element_plane, (i+1)*num_element_plane-1) = arma::join_vert(n2d+i*num_node_plane, n2d+(i+1)*num_node_plane);

		// return element connectivity matrix
		return n;
	}

	// integrate along one direction
	ShCartesianGridDataPr PolarGridData::azymuthal_integral()const{
		
		// arrays defining coordinates
		const arma::Row<fltp> rho = arma::linspace<arma::Row<fltp> >(rin_,rout_,num_rad_);
		const arma::Row<fltp> theta = arma::linspace<arma::Row<fltp> >(theta1_,theta2_,num_theta_);
		// const arma::Row<fltp> zz = arma::linspace<arma::Row<fltp> >(zlow_,zhigh_,num_axial_);

		// create coordinate system for each slice
		const arma::Mat<fltp> L = arma::join_vert(
			-arma::sin(theta), arma::cos(theta),
			arma::Row<fltp>(num_theta_,arma::fill::zeros));
		const arma::Mat<fltp> N = arma::join_vert(
			arma::cos(theta), arma::sin(theta),
			arma::Row<fltp>(num_theta_,arma::fill::zeros));
		const arma::Mat<fltp> D = cmn::Extra::cross(L,N);

		// allocate 2D grid data
		const ShCartesianGridDataPr grid = CartesianGridData::create(rin_,rout_,num_rad_,zlow_,zhigh_,num_axial_,0,0,1);
		grid->set_field_type(field_type_);
		grid->setup_targets(); grid->allocate(); grid->set_is_integrated(); grid->set_is_polar();

		// walk over field types
		for(auto it=field_type_.begin();it!=field_type_.end();it++){
			// get field array
			const arma::Mat<fltp>& fld3d = M_.at((*it).first);
			
			// allocate flattened field
			arma::Mat<fltp> fld2d(3,num_rad_*num_axial_);

			// walk over radial coordinates
			for(arma::uword i=0;i<num_rad_;i++){
				// length array
				const arma::Row<fltp> ell = rho(i)*theta;

				// walk over axial coordinates
				for(arma::uword j=0;j<num_axial_;j++){
					// allocate integral 
					arma::Mat<fltp> fldint(3, num_theta_);

					// walk over theta
					for(arma::uword k=0;k<num_theta_;k++)
						fldint.col(k) = fld3d.col(k*num_axial_*num_rad_ + j*num_rad_ + i);
					
					// integrate and store
					fld2d(0,j*num_rad_ + i) = arma::as_scalar(arma::trapz(ell, cmn::Extra::dot(fldint, N), 1));
					fld2d(1,j*num_rad_ + i) = arma::as_scalar(arma::trapz(ell, cmn::Extra::dot(fldint, D), 1));
					fld2d(2,j*num_rad_ + i) = arma::as_scalar(arma::trapz(ell, cmn::Extra::dot(fldint, L), 1));
				}
			}

			// set field
			grid->add_field((*it).first, fld2d);
		}
			
		// return the 2D grid
		return grid;
	}


	// get azymuthal field component
	arma::Mat<fltp> PolarGridData::get_azymuthal()const{
		// arrays defining coordinates
		// const arma::Row<fltp> rho = arma::linspace<arma::Row<fltp> >(rin_,rout_,num_rad_);
		const arma::Row<fltp> theta = arma::linspace<arma::Row<fltp> >(theta1_,theta2_,num_theta_);
		// const arma::Row<fltp> zz = arma::linspace<arma::Row<fltp> >(zlow_,zhigh_,num_axial_);

		// create coordinate system for each slice
		arma::Mat<fltp> L;
		if(axis_=='z')
			L = arma::join_vert(
				-arma::sin(theta), 
				arma::cos(theta),
				arma::Row<fltp>(num_theta_,arma::fill::zeros));
		else if(axis_=='x')
			L = arma::join_vert(
				arma::Row<fltp>(num_theta_,arma::fill::zeros),
				-arma::sin(theta), 
				arma::cos(theta));
		else if(axis_=='y')
			L = arma::join_vert(
				-arma::sin(theta), 
				arma::Row<fltp>(num_theta_,arma::fill::zeros), 
				arma::cos(theta));
		else rat_throw_line("axis not recognized");

		// get field and extract component
		return arma::reshape(arma::repmat(L.t(),1,num_rad_*num_axial_).t(),3,L.n_cols*num_rad_*num_axial_);
	}

	// get radial field component
	arma::Mat<fltp> PolarGridData::get_radial()const{
		// arrays defining coordinates
		// const arma::Row<fltp> rho = arma::linspace<arma::Row<fltp> >(rin_,rout_,num_rad_);
		const arma::Row<fltp> theta = arma::linspace<arma::Row<fltp> >(theta1_,theta2_,num_theta_);
		// const arma::Row<fltp> zz = arma::linspace<arma::Row<fltp> >(zlow_,zhigh_,num_axial_);

		// create coordinate system for each slice
		arma::Mat<fltp> N;
		if(axis_=='z')
			N = arma::join_vert(
				arma::cos(theta), 
				arma::sin(theta),
				arma::Row<fltp>(num_theta_,arma::fill::zeros));
		else if(axis_=='x')
			N = arma::join_vert(
				arma::Row<fltp>(num_theta_,arma::fill::zeros),
				arma::cos(theta), 
				arma::sin(theta));
		else if(axis_=='y')
			N = arma::join_vert(
				arma::cos(theta), 
				arma::Row<fltp>(num_theta_,arma::fill::zeros), 
				arma::sin(theta));
		else rat_throw_line("axis not recognized");

		// get field and extract component
		return  arma::reshape(arma::repmat(N.t(),1,num_rad_*num_axial_).t(),3,N.n_cols*num_rad_*num_axial_);
	}

	// get axial field component
	arma::Mat<fltp> PolarGridData::get_axial()const{
		// create coordinate system for each slice
		const arma::Mat<fltp> D = arma::repmat(cmn::Extra::unit_vec(axis_),1,num_theta_);

		// get field and extract component
		return arma::reshape(arma::repmat(D.t(),1,num_rad_*num_axial_).t(),3,D.n_cols*num_rad_*num_axial_);
	}



}}