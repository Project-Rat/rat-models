// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "importdata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ImportData::ImportData(){
		set_output_type("import");
	}

	//constructor
	ImportData::ImportData(const boost::filesystem::path &fname){
		fname_ = fname; set_output_type("import");
	}

	// factory methods
	ShImportDataPr ImportData::create(){
		return std::make_shared<ImportData>();
	}

	// factory methods
	ShImportDataPr ImportData::create(const boost::filesystem::path &fname){
		return std::make_shared<ImportData>(fname);
	}

	// set scaling factor
	void ImportData::set_scaling_factor(const rat::fltp scaling_factor){
		scaling_factor_ = scaling_factor;
	}

	// translate
	void ImportData::set_translation(const arma::Col<fltp> &R0){
		R0_ = R0;
	}

	// create calculation data objects
	void ImportData::setup_targets(){
		// check if the stl file exists
		boost::system::error_code ec;
		if(!boost::filesystem::exists(fname_,ec))
			rat_throw_line("file does not exist: " + fname_.string());

		// input file
		std::ifstream myFile(fname_.c_str(), std::ios::in | std::ios::binary);

		// header
		unsigned int num_triangles;

		//read 80 byte header
		if(myFile.good()){
			char header_info[80] = "";
			myFile.read(header_info, 80);
		}else{
			rat_throw_line("file header not ok");
		}

		//read 4-byte ulong
		if(myFile.good()){
			char nTri[4];
			myFile.read (nTri, 4);
			std::memcpy(&num_triangles, nTri, sizeof(int));
		}else{
			rat_throw_line("file length not ok");
		}

		// allocate three coordinates
		arma::Mat<float> M(12,num_triangles); // VTK uses float

		//now read in all the triangles
		for(unsigned int i=0;i<num_triangles;i++){
			// check if file is still ok
			if(myFile.good()) {
				//read one 50-byte triangle
				char facet[50];
				myFile.read(facet, 50);

				// copy memory
				std::memcpy(M.memptr() + i*12, facet, 12*sizeof(float));
			}
		}	

		// close file
		myFile.close();

		// number of targets
		num_targets_ = 3*num_triangles;

		// get coordinate vectors
		Rt_ = scaling_factor_*arma::reshape(arma::conv_to<arma::Mat<fltp> >::from(M.rows(3,11)),3,num_targets_);

		// move the mesh
		Rt_.each_col() += R0_;

		// create mesh with these vectors
		n_ = arma::reshape(arma::regspace<arma::Col<arma::uword> >(0,num_triangles*3-1),3,num_triangles);

		// merge duplicate points
		for(arma::uword i=0;i<3;i++){
			const arma::Col<arma::uword> sort_idx = arma::stable_sort_index(Rt_.row(i));
			arma::Col<arma::uword> original_idx = arma::regspace<arma::Col<arma::uword> >(0,Rt_.n_cols-1);
			original_idx.rows(sort_idx) = original_idx;
			Rt_ = Rt_.cols(sort_idx); n_ = arma::reshape(original_idx(n_),3,num_triangles);
		}

		// find duplicate points
		arma::Row<arma::uword> is_unique = arma::sqrt(arma::sum(
			arma::square(arma::diff(Rt_,1,1)),0))>arma::Datum<float>::eps*1024;
		
		// reindex
		arma::Row<arma::uword> idx_n = arma::join_horiz(
			arma::Row<arma::uword>{0},arma::cumsum(is_unique));

		// remove duplicate points
		Rt_ = Rt_.cols(arma::find(is_unique));
		n_ = arma::reshape(idx_n(n_),3,num_triangles);

		// number of targets
		num_targets_ = Rt_.n_cols;
	}

	// VTK export
	ShVTKObjPr ImportData::export_vtk() const{
		// create unstructured mesh
		ShVTKUnstrPr vtk = VTKUnstr::create();

		// get element type
		const arma::uword element_type = VTKUnstr::get_element_type(n_.n_rows, 2llu);

		// set the mesh
		vtk->set_mesh(Rt_, n_, element_type);
		
		// tranfer name
		vtk->set_name(get_name());

		// add calculated vector potential
		if(has('A') && has_field())vtk->set_nodedata(get_field('A'),"Vector Potential [Vs/m]");
		
		// add calculated magnetic flux density
		if(has('B') && has_field())vtk->set_nodedata(get_field('B'),"Mgn. Flux Density [T]");
		
		// add calculated magnetisation
		if(has('M') && has_field())vtk->set_nodedata(get_field('M'),"Magnetisation [A/m]");
		
		// add calculated magnetic field
		if(has('H') && has_field())vtk->set_nodedata(get_field('H'),"Magnetic Field [A/m]");

		// return the unstructured mesh
		return vtk;
	}


}}