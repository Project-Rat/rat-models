// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathcostheta.hh"

#include "pathgroup.hh"
#include "pathstraight.hh"
#include "pathsuperellipse.hh"

// COSINE THETA BLOCK
// code specific to Rat
namespace rat{namespace mdl{
	// constructor
	CosThetaBlock::CosThetaBlock(){
		set_name("Cos-Theta Block");
	}

	// constructor
	CosThetaBlock::CosThetaBlock(
		const arma::uword num_poles,
		const arma::uword num_cables, 
		const fltp radius,
		const fltp phi, 
		const fltp alpha, 
		const fltp dinner, 
		const fltp douter,
		const fltp wcable,
		const fltp zend,
		const fltp beta,
		const fltp dinsu) : CosThetaBlock(){

		// set to self
		set_num_poles(num_poles); set_num_cables(num_cables); set_radius(radius); 
		set_alpha(alpha); set_phi(phi); set_dinner(dinner); set_douter(douter);
		set_wcable(wcable); set_zend(zend); set_beta(beta); set_dinsu(dinsu);
	}
	
	// factory
	ShCosThetaBlockPr CosThetaBlock::create(){
		return std::make_shared<CosThetaBlock>();
	}

	// factory
	ShCosThetaBlockPr CosThetaBlock::create(
		const arma::uword num_poles,
		const arma::uword num_cables,
		const fltp radius, 
		const fltp phi, 
		const fltp alpha,
		const fltp dinner,
		const fltp douter, 
		const fltp wcable,
		const fltp zend,
		const fltp beta,
		const fltp dinsu){
		return std::make_shared<CosThetaBlock>(
			num_poles, num_cables, radius, phi, alpha, 
			dinner, douter, wcable, zend, beta, dinsu);
	}

	// setters
	void CosThetaBlock::set_no_inter_turn_wedges(const bool no_inter_turn_wedges){
		no_inter_turn_wedges_ = no_inter_turn_wedges;
	}

	void CosThetaBlock::set_num_poles(const arma::uword num_poles){
		num_poles_ = num_poles;
	}

	void CosThetaBlock::set_num_cables(const arma::uword num_cables){
		num_cables_ = num_cables;
	}

	void CosThetaBlock::set_radius(const fltp radius){
		radius_ = radius;
	}

	void CosThetaBlock::set_phi(const fltp phi){
		phi_ = phi;
	}

	void CosThetaBlock::set_alpha(const fltp alpha){
		alpha_ = alpha;
	}

	void CosThetaBlock::set_dinner(const fltp dinner){
		dinner_ = dinner;
	}

	void CosThetaBlock::set_douter(const fltp douter){
		douter_ = douter;
	}

	void CosThetaBlock::set_wcable(const fltp wcable){
		wcable_ = wcable;
	}

	void CosThetaBlock::set_zend(const fltp zend){
		zend_ = zend;
	}

	void CosThetaBlock::set_beta(const fltp beta){
		beta_ = beta;
	}

	void CosThetaBlock::set_n1(const fltp n1){
		n1_ = n1;
	}

	void CosThetaBlock::set_n2(const fltp n2){
		n2_ = n2;
	}

	void CosThetaBlock::set_lambda(const fltp lambda){
		lambda_ = lambda;
	}

	void CosThetaBlock::set_dinsu(const fltp dinsu){
		dinsu_ = dinsu;
	}

	// getters
	bool CosThetaBlock::get_no_inter_turn_wedges()const{
		return no_inter_turn_wedges_;
	}

	arma::uword CosThetaBlock::get_num_poles()const{
		return num_poles_;
	}

	arma::uword CosThetaBlock::get_num_cables()const{
		return num_cables_;
	}

	fltp CosThetaBlock::get_radius()const{
		return radius_;
	}

	fltp CosThetaBlock::get_phi()const{
		return phi_;
	}

	fltp CosThetaBlock::get_alpha()const{
		return alpha_;
	}

	fltp CosThetaBlock::get_dinner()const{
		return dinner_;
	}

	fltp CosThetaBlock::get_douter()const{
		return douter_;
	}

	fltp CosThetaBlock::get_wcable()const{
		return wcable_;
	}

	fltp CosThetaBlock::get_zend()const{
		return zend_;
	}

	fltp CosThetaBlock::get_beta()const{
		return beta_;
	}

	fltp CosThetaBlock::get_n1()const{
		return n1_;
	}

	fltp CosThetaBlock::get_n2()const{
		return n2_;
	}

	fltp CosThetaBlock::get_lambda()const{
		return lambda_;
	}

	fltp CosThetaBlock::get_dinsu()const{
		return dinsu_;
	}

	// distance point to circle along a vector
	fltp CosThetaBlock::circle_distance(
		const fltp radius, 
		const arma::Col<fltp>::fixed<2>&p, 
		const arma::Col<fltp>::fixed<2>&v){

		// quadratic equation coefficients
		const fltp a = arma::as_scalar(cmn::Extra::dot(v,v));
		const fltp b = 2*arma::as_scalar(cmn::Extra::dot(p, v));
		const fltp c = arma::as_scalar(cmn::Extra::dot(p, p)) - radius*radius;

		// discriminant
		const fltp D = b*b - 4*a*c;

		// no solution
		if(D<0)return -1e99;

		// find the two possible values of t
		const fltp t1 = (-b+std::sqrt(D))/(2*a);
		const fltp t2 = (-b-std::sqrt(D))/(2*a);

		// choose the appropriate value of t
		return std::abs(t1)<std::abs(t2) ? t1 : t2;
	}

	// shift coordinates of first corner point such that the cable is touching the cylinder
	arma::Col<fltp>::fixed<2> CosThetaBlock::shift_coords_to_circle(
		const arma::Col<fltp>::fixed<2>&P1, 
		const arma::Col<fltp>::fixed<2>&D1, 
		const arma::Col<fltp>::fixed<2>&D12){
		
		// get cable properties
		const fltp dinner = get_insulated_dinner();

		// normal vector is perpendicular to D1
		const arma::Col<fltp>::fixed<2> N1{-D1(1), D1(0)};
		const arma::Col<fltp>::fixed<2> N12{-D12(1), D12(0)};

		// find second corner of the cable
		const arma::Col<fltp>::fixed<2> P2 = P1 + N12*dinner;

		// find the point where D12 is perpendicular to the circle
		// this is where the line segment could touch the circle
		const fltp alpha_perp = std::atan(D12(1)/D12(0));
		const arma::Col<fltp>::fixed<2> Pm = {radius_*std::cos(alpha_perp), radius_*std::sin(alpha_perp)};
		const fltp t4 = arma::as_scalar(cmn::Extra::dot(N12, Pm - P1));
		const fltp d12 = arma::as_scalar(cmn::Extra::vec_norm(P2 - P1));
		const arma::Col<fltp>::fixed<2> Pp = P1 + N12*t4;

		// distance between the tangent point and its projection on line P1,P2
		const fltp t3 = arma::as_scalar(cmn::Extra::dot(D1, Pm - Pp));
		
		// project points to circle sliding the cable along D1
		const fltp t1 = circle_distance(radius_, P1, D1);
		const fltp t2 = circle_distance(radius_, P2, D1);

		// find maximum time
		const fltp t = t4>0 && t4<d12 ? std::max(std::max(t1,t2),t3) : std::max(t1,t2);

		// find largest distance
		return P1 + t*D1;
	}

	// get keystone angle
	fltp CosThetaBlock::get_keystone_angle()const{
		return std::atan((douter_ - dinner_)/(2*wcable_));
	}

	// get insulated cable width
	fltp CosThetaBlock::get_insulated_width(){
		return wcable_ + 2*dinsu_;
	}

	// get insulated cable inner thickness
	fltp CosThetaBlock::get_insulated_dinner(){
		const fltp keystone_angle = get_keystone_angle();
		return dinner_ - 2*dinsu_*std::tan(keystone_angle) + 2*dinsu_/std::cos(keystone_angle);
	}

	// get insulated cable outer thickness
	fltp CosThetaBlock::get_insulated_douter(){
		const fltp keystone_angle = get_keystone_angle();
		return douter_ + 2*dinsu_*std::tan(keystone_angle) + 2*dinsu_/std::cos(keystone_angle);
	}

	// create cable frames
	void CosThetaBlock::create_cable_frames(arma::Mat<fltp>&R, arma::Mat<fltp>&D){
		// allocate
		R.set_size(2,num_cables_); D.set_size(2,num_cables_);

		// current angle and corner point
		R.col(0) = arma::Col<fltp>::fixed<2>{radius_*std::cos(phi_), radius_*std::sin(phi_)};

		// calculate keystone angle of cable
		const fltp keystone_angle = get_keystone_angle();
		const fltp dinner = get_insulated_dinner();

		// recursively calculate remaining coordinates
		for(arma::uword i=0;i<num_cables_;i++){
			// calculate angle
			const fltp alpha = alpha_ + i*2*keystone_angle;

			// calculate vectors
			const arma::Col<fltp>::fixed<2> D1{std::cos(alpha), std::sin(alpha)};
			D.col(i) = arma::Col<fltp>::fixed<2>{std::cos(alpha+keystone_angle), std::sin(alpha+keystone_angle)};
			const arma::Col<fltp> N12{-std::sin(alpha+keystone_angle), std::cos(alpha+keystone_angle)};

			// shift coordinate
			R.col(i) = shift_coords_to_circle(R.col(i), D1, D.col(i)) + N12*dinner/2;

			// calculate next coord
			if(i!=num_cables_-1)R.col(i+1) = R.col(i) + N12*dinner/2;
		}

		// move the frame to the outside
		R += D*(wcable_+dinsu_);

		// reverse order winding inside out
		R = arma::fliplr(R); D = arma::fliplr(D);
	}

	// shift coordinates of first corner point such that the cable is touching the cylinder
	arma::Col<fltp>::fixed<2> CosThetaBlock::shift_coords_to_radius(
		const arma::Col<fltp>::fixed<2>&P12, 
		const arma::Col<fltp>::fixed<2>&Dshift, 
		const fltp radius){

		// normal vector is perpendicular to D1
		// const arma::Col<fltp>::fixed<2> N1{-D1(1), D1(0)};
		// const arma::Col<fltp>::fixed<2> N12{-D12(1), D12(0)};

		// find second corner of the cable
		// const arma::Col<fltp>::fixed<2> P12 = P1 + N12*douter_/2;

		// project points to radius along D1
		const fltp delta_radius = radius - P12(1);
		const fltp t = delta_radius/Dshift(1);

		// project
		return P12 + t*Dshift;
	}


	// create nose coords for a given radius and beta for each turn
	// beta is the angle to D1
	void CosThetaBlock::create_nose(const arma::Row<fltp> &radius, const arma::Row<fltp> &beta, arma::Mat<fltp>&Rnose){
		// allocate
		Rnose.set_size(2,num_cables_);

		// calculate keystone angle of cable
		const fltp keystone_angle = get_keystone_angle();
		const fltp douter = get_insulated_douter();

		// first coordinate
		Rnose.col(0) = arma::Col<fltp>::fixed<2>{zend_, radius(0)};

		// calculate angles
		const arma::Row<fltp> beta_star = arma::Datum<fltp>::pi - beta;
		const arma::Mat<fltp> D1 = arma::join_vert(arma::cos(beta_star), arma::sin(beta_star));
		// const arma::Mat<fltp> D12 = arma::join_vert(arma::cos(beta+keystone_angle), arma::sin(beta+keystone_angle));
		const arma::Mat<fltp> N12 = arma::join_vert(arma::sin(beta_star+keystone_angle), -arma::cos(beta_star+keystone_angle));
		const arma::Mat<fltp> D2 = arma::join_vert(arma::cos(beta_star+2*keystone_angle), arma::sin(beta_star+2*keystone_angle));

		// recursively calculate remaining coordinates
		for(arma::uword i=0;i<num_cables_;i++){
			// // calculate angle
			// const fltp beta = beta_ + i*2*keystone_angle;

			// // normal vector
			// const arma::Col<fltp>::fixed<2> D1 = i==0 ? arma::Col<fltp>::fixed<2>{std::cos(beta(i)), std::sin(beta(i))} : 
			// 	arma::Col<fltp>::fixed<2>{std::cos(beta(i-1)+2*keystone_angle), std::sin(beta(i-1)+2*keystone_angle)};
			// const arma::Col<fltp>::fixed<2> D12 = {std::cos(beta(i)+keystone_angle), std::sin(beta(i)+keystone_angle)};
			// const arma::Col<fltp>::fixed<2> N12 = {std::sin(beta(i)+keystone_angle), -std::cos(beta(i)+keystone_angle)};

			// find coordinate on centerline of cable
			Rnose.col(i) += douter*N12.col(i)/2;

			// project coordinate back to radius along D1 of the previous cable
			Rnose.col(i) = shift_coords_to_radius(Rnose.col(i), i==0 ? D1.col(i) : D2.col(i-1), radius(i));

			// calculate next coord on other side of cable
			if(i!=num_cables_-1)Rnose.col(i+1) = Rnose.col(i) + N12.col(i)*douter/2;
		}
	}

	// check validity
	bool CosThetaBlock::is_valid(const bool enable_throws)const{
		if(num_poles_<1){if(enable_throws){rat_throw_line("number of poles must be at least 1 (dipole)");} return false;};
		if(num_cables_<=0){if(enable_throws){rat_throw_line("num_cables must be positive");} return false;};
		if(zend_<=0){if(enable_throws){rat_throw_line("zend must be positive");} return false;};
		if(lambda_<=0){if(enable_throws){rat_throw_line("lambda must be positive");} return false;};
		if(dinner_<=0){if(enable_throws){rat_throw_line("dinner must be positive");} return false;};
		if(douter_<=0){if(enable_throws){rat_throw_line("douter must be positive");} return false;};
		if(dinsu_<0){if(enable_throws){rat_throw_line("dinsu must be positive");} return false;};
		if(n1_<=0){if(enable_throws){rat_throw_line("n1 must be positive");} return false;};
		if(n2_<=0){if(enable_throws){rat_throw_line("n2 must be positive");} return false;};
		if(radius_<=0){if(enable_throws){rat_throw_line("radius must be positive");} return false;};
		if(beta_<0 || beta_>arma::Datum<fltp>::pi){if(enable_throws){rat_throw_line("beta must be between zero and pi");} return false;};
		if(2*get_keystone_angle()*num_cables_>arma::Datum<fltp>::pi/(2*num_poles_)){if(enable_throws){rat_throw_line("keystone angle too high");} return false;};
		return true;
	}

	// serialization
	std::string CosThetaBlock::get_type(){
		return "rat::mdl::costhetablock";
	}

	// serialize to json
	void CosThetaBlock::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		// parents
		cmn::Node::serialize(js,list);

		// override type
		js["type"] = get_type();

		// properties
		js["num_poles"] = static_cast<int>(num_poles_);
		js["num_cables"] = static_cast<int>(num_cables_);
		js["radius"] = radius_;
		js["phi"] = phi_;
		js["alpha"] = alpha_;
		js["dinner"] = dinner_;
		js["douter"] = douter_;
		js["wcable"] = wcable_;
		js["n1"] = static_cast<int>(n1_);
		js["n2"] = static_cast<int>(n2_);
		js["dinsu"] = dinsu_;
		js["zend"] = zend_;
		js["beta"] = beta_;
		js["lambda"] = lambda_;
	}

	// deserialize from json
	void CosThetaBlock::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		
		// parents
		cmn::Node::deserialize(js,list,factory_list,pth);
		
		// properties
		set_num_cables(js["num_cables"].asUInt64());
		set_radius(js["radius"].ASFLTP());
		set_phi(js["phi"].ASFLTP());
		set_alpha(js["alpha"].ASFLTP());
		set_dinner(js["dinner"].ASFLTP());
		set_douter(js["douter"].ASFLTP());
		set_wcable(js["wcable"].ASFLTP());
		set_n1(js["n1"].ASFLTP());
		set_n2(js["n2"].ASFLTP());
		set_dinsu(js["dinsu"].ASFLTP());
		set_num_poles(js["num_poles"].asUInt64());
		set_zend(js["zend"].ASFLTP());
		set_beta(js["beta"].ASFLTP());
		set_lambda(js["lambda"].ASFLTP());
	}

}}


// COSINE THETA PATH
// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathCosTheta::PathCosTheta(){
		set_name("Cos-Theta Path");
	}

	// constructor
	PathCosTheta::PathCosTheta(const std::list<ShCosThetaBlockPr>& blocks, const fltp element_size) : PathCosTheta(){
		for(auto it=blocks.begin();it!=blocks.end();it++)add_block(*it);
		set_element_size(element_size);
	}

	// factory
	ShPathCosThetaPr PathCosTheta::create(){
		return std::make_shared<PathCosTheta>();
	}

	// factory
	ShPathCosThetaPr PathCosTheta::create(const std::list<ShCosThetaBlockPr>& blocks, const fltp element_size){
		return std::make_shared<PathCosTheta>(blocks,element_size);
	}

	// setters
	void PathCosTheta::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	void PathCosTheta::set_use_lead_end(const bool use_lead_end){
		use_lead_end_ = use_lead_end;
	}

	// getters
	fltp PathCosTheta::get_element_size()const{
		return element_size_;
	}

	bool PathCosTheta::get_use_lead_end()const{
		return use_lead_end_;
	}
	

	// get input path
	const ShCosThetaBlockPr& PathCosTheta::get_block(const arma::uword index) const{
		auto it = blocks_.find(index);
		if(it==blocks_.end())rat_throw_line("could not find block");
		return (*it).second;
	}

	// add a block
	arma::uword PathCosTheta::add_block(const ShCosThetaBlockPr &block){
		// check input
		assert(block!=NULL); 

		// get counters
		const arma::uword index = blocks_.size()+1;

		// insert
		blocks_.insert({index, block});

		// return index
		return index;
	}

	bool PathCosTheta::delete_block(const arma::uword index){
		auto it = blocks_.find(index);
		if(it==blocks_.end())return false;
		(*it).second = NULL; return true;
	}

	// reindex
	void PathCosTheta::reindex(){
		Transformations::reindex();
		std::map<arma::uword, ShCosThetaBlockPr> new_blocks; arma::uword idx = 1;
		for(auto it=blocks_.begin();it!=blocks_.end();it++)
			if((*it).second!=NULL)new_blocks.insert({idx++, (*it).second});
		blocks_ = new_blocks;
	}

	// get number of blocks
	arma::uword PathCosTheta::get_num_blocks() const{
		return blocks_.size();
	}
	
	// calculate natural inclination angle
	fltp PathCosTheta::calc_natural_inclination(
		const arma::uword num_poles, const fltp radius, 
		const fltp phi, const fltp ba, const fltp n1, 
		const fltp n2, const fltp delta_radius){

		// create a local group (for now)
		const fltp t = arma::Datum<fltp>::pi/2;
		const fltp rr = radius; const fltp vr = 0.0; const fltp ar = 0.0; 
		const fltp xi = arma::Datum<fltp>::pi/(2*num_poles) - phi;

		// scale factor in z
		const fltp y0 = radius*std::cos(xi);
		const fltp yscale = (radius - y0 + delta_radius)/(radius - y0);

		// this math seems a bit dodgy as it relies on a division by near zero
		// const fltp ax = 0.0; //-(4*(xi*xi)*rr*std::pow(std::cos(t),4.0/n2-2)*std::sin(t)*std::sin(t)*std::sin(xi*std::pow(std::cos(t),2.0/n2)))/(n2*n2)+ar*std::sin(xi*std::pow(std::cos(t),2.0/n2))+(2*(2.0/n2-1)*xi*rr*std::pow(std::cos(t),2.0/n2-2)*std::sin(t)*std::sin(t)*std::cos(xi*std::pow(std::cos(t),2.0/n2)))/n2-(4*xi*vr*std::pow(std::cos(t),2.0/n2-1)*std::sin(t)*std::cos(xi*std::pow(std::cos(t),2.0/n2)))/n2-(2*xi*rr*std::pow(std::cos(t),2.0/n2)*std::cos(xi*std::pow(std::cos(t),2.0/n2)))/n2;
		// const fltp ay = (((2*n2-4)*xi*rr*std::pow(std::cos(t),2.0/n2)*std::sin(t)*std::sin(t)+4*n2*xi*vr*std::pow(std::cos(t),2.0/n2+1)*std::sin(t)+2*n2*xi*rr*std::pow(std::cos(t),2.0/n2+2))*std::sin(xi*std::pow(std::cos(t),2.0/n2))+((n2*n2)*ar*std::cos(t)*std::cos(t)-4*(xi*xi)*rr*std::pow(std::cos(t),4.0/n2)*std::sin(t)*std::sin(t))*std::cos(xi*std::pow(std::cos(t),2.0/n2)))/((n2*n2)*std::cos(t)*std::cos(t));
		const fltp ay = (((2*n2-4)*xi*rr*std::pow(std::cos(t),2.0/n2-2) + 4*n2*xi*vr*std::pow(std::cos(t),2.0/n2+1-2) + 2*n2*xi*rr*std::pow(std::cos(t),2.0/n2+2-2))*std::sin(xi*std::pow(std::cos(t),2.0/n2)) + ((n2*n2)*ar - 4*(xi*xi)*rr*std::pow(std::cos(t),4.0/n2-2))*std::cos(xi*std::pow(std::cos(t),2.0/n2)))/((n2*n2));
		const fltp az = -(2*ba)/n1;
		return std::atan(az/(yscale*ay));
	}


	// get frame
	ShFramePr PathCosTheta::create_frame(const MeshSettings &stngs) const{
		// check validity
		is_valid(true);

		// settings
		const fltp cone_angle = RAT_CONST(0.0);

		// allocate data tables
		const arma::uword num_blocks = blocks_.size();
		arma::field<arma::Row<fltp> > radius(1,num_blocks);
		arma::field<arma::Row<fltp> > phi(1,num_blocks);
		arma::field<arma::Row<fltp> > alpha(1,num_blocks);
		arma::field<arma::Row<fltp> > beta(1,num_blocks);
		arma::field<arma::Row<fltp> > zeta1(1,num_blocks);
		arma::field<arma::Row<fltp> > zeta2(1,num_blocks);
		arma::field<arma::Row<fltp> > ba(1,num_blocks);
		arma::field<arma::Row<fltp> > keystone_angle(1,num_blocks);
		arma::field<arma::Mat<fltp> > Rcoil(1,num_blocks);
		arma::field<arma::Mat<fltp> > Dcoil(1,num_blocks);
		arma::field<arma::Mat<fltp> > Rnose(1,num_blocks);
		arma::field<arma::Row<fltp> > ell_straight_half(1,num_blocks);
		arma::field<arma::Row<fltp> > n1(1,num_blocks);
		arma::field<arma::Row<fltp> > n2(1,num_blocks);
		arma::field<arma::Row<arma::uword> > block_index(1,num_blocks);
		arma::field<arma::Row<arma::uword> > num_poles(1,num_blocks);

		// walk over blocks and calculate cable positions/angles per turn
		arma::uword k=0;
		for(auto it=blocks_.rbegin();it!=blocks_.rend();it++,k++){
			
			// get block
			const ShCosThetaBlockPr& block = (*it).second;

			// do not place inter-turn wedge
			const bool no_inter_turn_wedges = block->get_no_inter_turn_wedges();

			// const perimeter end settings
			const arma::uword num_cables = block->get_num_cables();
			const fltp lambda = block->get_lambda();
			const fltp zend = block->get_zend();
			const fltp beta0 = block->get_beta();
			const fltp douter = block->get_insulated_douter();

			// keystone of cable
			keystone_angle(k) = arma::Row<fltp>(num_cables, arma::fill::value(block->get_keystone_angle()));

			// superellipse order
			n1(k) = arma::Row<fltp>(num_cables, arma::fill::value(block->get_n1()));
			n2(k) = arma::Row<fltp>(num_cables, arma::fill::value(block->get_n2()));

			// index of block
			block_index(k) = arma::Row<arma::uword>(num_cables, arma::fill::value(k));
			num_poles(k) = arma::Row<arma::uword>(num_cables, arma::fill::value(block->get_num_poles()));

			// create cables
			block->create_cable_frames(Rcoil(k),Dcoil(k));
			assert(Rcoil(k).n_cols==num_cables);
			assert(Dcoil(k).n_cols==num_cables);

			// calculate properties for all the turns
			radius(k) = arma::hypot(Rcoil(k).row(0),Rcoil(k).row(1));
			phi(k) = arma::atan(Rcoil(k).row(1)/Rcoil(k).row(0));
			alpha(k) = arma::atan(Dcoil(k).row(1)/Dcoil(k).row(0));
			zeta1(k) = alpha(k) - phi(k);

			// get properties for first turn
			const fltp phi0 = phi(k).front();
			const fltp radius0 = radius(k).front();

			// calculate straight section length based on the inner most turn of the block
			const fltp ba0 = lambda*radius0*(arma::Datum<fltp>::pi/(2*block->get_num_poles()) - phi0);
			ell_straight_half(k) = arma::Row<fltp>(num_cables, arma::fill::value(zend - ba0)); // half coil length

			// determine beta
			beta(k).set_size(num_cables);
			for(arma::uword i=0;i<num_cables;i++){
				if(no_inter_turn_wedges)beta(k)(i) = beta0 + i*2*keystone_angle(k)(i);
				else beta(k)(i) = calc_natural_inclination(num_poles(k)(i),radius(k)(i),phi(k)(i),ba0 + i*douter,n1(k)(i),n2(k)(i)) - keystone_angle(k)(i);
			}

			// create nose
			block->create_nose(radius(k), beta(k), Rnose(k));
			assert(Rnose(k).n_cols==num_cables);

			// calculate properties for all the turns
			ba(k) = Rnose(k).row(0) - ell_straight_half(k);
			assert(arma::all(Rnose(k).row(1)-radius(k)<1e-9));

			// variables
			zeta2(k).set_size(num_cables);
			for(arma::uword i=0;i<num_cables;i++)
				zeta2(k)(i) = calc_natural_inclination(num_poles(k)(i),radius(k)(i),phi(k)(i),ba(k)(i),n1(k)(i),n2(k)(i)) - (beta(k)(i) + keystone_angle(k)(i));
		}

		// main group
		const ShPathGroupPr main_path = PathGroup::create({Rcoil(0)(0,0),Rcoil(0)(1,0),-ell_straight_half(0)(0)}, {0,0,1}, {-Dcoil(0)(1,0),Dcoil(0)(0,0),0.0});
		main_path->set_align_frame(use_lead_end_);

		// walk over cables in this block
		for(arma::uword k=0;k<num_blocks;k++){

			// walk over cables in this block
			for(arma::uword i=0;i<radius(k).n_elem;i++){
				// create a group for a turn
				const ShPathGroupPr group = PathGroup::create({Rcoil(k)(0,i),Rcoil(k)(1,i),-ell_straight_half(k)(i)}, {0,0,1}, {-Dcoil(k)(1,i),Dcoil(k)(0,i),0.0});
				group->set_align_frame(true);

				// straight section
				if(k>0 && i==0 && use_lead_end_){
					group->add_path(PathStraight::create(ell_straight_half(k-1).back(), element_size_)); // after turn increment
				}else{
					group->add_path(PathStraight::create(ell_straight_half(k)(i), element_size_));
				}
				group->add_path(PathStraight::create(ell_straight_half(k)(i), element_size_));

				// create ellipse
				const rat::mdl::ShPathSuperEllipsePr ellipse1 = rat::mdl::PathSuperEllipse::create(
					radius(k)(i), ba(k)(i), cone_angle, phi(k)(i), arma::Datum<fltp>::pi/2, element_size_, num_poles(k)(i), n1(k)(i), n2(k)(i), zeta1(k)(i), zeta2(k)(i));
				ellipse1->add_flip();
				group->add_path(ellipse1);

				// create ellipse
				const rat::mdl::ShPathSuperEllipsePr ellipse2 = rat::mdl::PathSuperEllipse::create(
					radius(k)(i), ba(k)(i), cone_angle, phi(k)(i), arma::Datum<fltp>::pi/2, element_size_, num_poles(k)(i), n1(k)(i), n2(k)(i), zeta1(k)(i), zeta2(k)(i));
				ellipse2->set_reverse();
				ellipse2->add_flip();
				group->add_path(ellipse2);

				// straight section
				group->add_path(PathStraight::create(ell_straight_half(k)(i), element_size_));
				group->add_path(PathStraight::create(ell_straight_half(k)(i), element_size_));

				// symmetric end
				if(!use_lead_end_){
					// create ellipse
					const rat::mdl::ShPathSuperEllipsePr ellipse3 = rat::mdl::PathSuperEllipse::create(
						radius(k)(i), ba(k)(i), cone_angle, phi(k)(i), arma::Datum<fltp>::pi/2, element_size_, 
						num_poles(k)(i), n1(k)(i), n2(k)(i), zeta1(k)(i), zeta2(k)(i));
					ellipse3->add_flip();
					group->add_path(ellipse3);

					// create ellipse
					const rat::mdl::ShPathSuperEllipsePr ellipse4 = rat::mdl::PathSuperEllipse::create(
						radius(k)(i), ba(k)(i), cone_angle, phi(k)(i), arma::Datum<fltp>::pi/2, element_size_, 
						num_poles(k)(i), n1(k)(i), n2(k)(i), zeta1(k)(i), zeta2(k)(i));
					ellipse4->set_reverse();
					ellipse4->add_flip();
					group->add_path(ellipse4);
				}

				// unsymmetric end
				else{
					// turn end connect to next turn in same block
					if(i!=radius(k).n_elem-1){
						// create ellipse
						const rat::mdl::ShPathSuperEllipsePr ellipse3 = rat::mdl::PathSuperEllipse::create(
							radius(k)(i), ba(k)(i), cone_angle, phi(k)(i), arma::Datum<fltp>::pi/2, element_size_, 
							num_poles(k)(i), n1(k)(i), n2(k)(i), zeta1(k)(i), zeta2(k)(i));
						ellipse3->add_flip();
						group->add_path(ellipse3);

						// create ellipse
						// delta radius is causing not perfect coordinate system at end ...
						const fltp delta_radius = radius(k)(i) - radius(k)(i+1);
						const fltp zeta12 = calc_natural_inclination(
							num_poles(k)(i+1),radius(k)(i+1),phi(k)(i+1),ba(k)(i),n1(k)(i),n2(k)(i),delta_radius) - (beta(k)(i) + keystone_angle(k)(i));
						const rat::mdl::ShPathSuperEllipsePr ellipse4 = rat::mdl::PathSuperEllipse::create(
							radius(k)(i+1), ba(k)(i), cone_angle, phi(k)(i+1), arma::Datum<fltp>::pi/2, element_size_, 
							num_poles(k)(i+1), n1(k)(i), n2(k)(i), zeta1(k)(i+1), zeta12, delta_radius);
						ellipse4->set_reverse();
						ellipse4->add_flip();
						group->add_path(ellipse4);
					}

					// turn end + connect to next block
					else if(k!=num_blocks-1){
						// take average end position
						const fltp z12 = (ell_straight_half(k)(i) + ba(k)(i) + ell_straight_half(k+1)(0) + ba(k+1)(0))/2;
						const fltp ba1 = z12 - ell_straight_half(k)(i);
						const fltp ba2 = z12 - ell_straight_half(k)(i);
						// const fltp ba2 = z12 - ell_straight_half(k+1)(0);

						// radius increment
						const fltp radius12 = (radius(k)(i) + radius(k+1)(0))/2;
						const fltp delta_radius1 = radius12 - radius(k)(i);
						const fltp delta_radius2 = radius12 - radius(k+1)(0);

						// twist towards end
						const fltp beta12 = (beta(k)(i) + beta(k+1)(0))/2;
						const fltp zeta2_a = calc_natural_inclination(num_poles(k)(i),radius(k)(i),phi(k)(i),ba1,n1(k)(i),n2(k)(i),delta_radius1) - (beta12 + keystone_angle(k)(i));
						const fltp zeta2_b = calc_natural_inclination(num_poles(k+1)(0),radius(k+1)(0),phi(k+1)(0),ba2,n1(k+1)(0),n2(k+1)(0),delta_radius2) - (beta12 + keystone_angle(k+1)(0));

						// create ellipse
						const rat::mdl::ShPathSuperEllipsePr ellipse3 = rat::mdl::PathSuperEllipse::create(
							radius(k)(i), ba1, cone_angle, phi(k)(i), arma::Datum<fltp>::pi/2, element_size_, 
							num_poles(k)(i), n1(k)(i), n2(k)(i), zeta1(k)(i), zeta2_a, delta_radius1);
						ellipse3->add_flip();
						group->add_path(ellipse3);

						// create ellipse
						const rat::mdl::ShPathSuperEllipsePr ellipse4 = rat::mdl::PathSuperEllipse::create(
							radius(k+1)(0), ba2, cone_angle, phi(k+1)(0), arma::Datum<fltp>::pi/2, element_size_, 
							num_poles(k+1)(0), n1(k+1)(0), n2(k+1)(0), zeta1(k+1)(0), zeta2_b, delta_radius2);
						ellipse4->set_reverse();
						ellipse4->add_flip();
						group->add_path(ellipse4);
					}
				}

				// add group
				main_path->add_path(group);
			}
		}

		// create frame and return 
		const ShFramePr frame = main_path->create_frame(stngs);
	
		// apply transformations
		frame->apply_transformations(get_transformations(), stngs.time);

		// return the frame
		return frame;
	}

	// validity check
	bool PathCosTheta::is_valid(const bool enable_throws) const{
		if(blocks_.empty()){if(enable_throws){rat_throw_line("there are no blocks set");} return false;};
		for(auto it=blocks_.begin();it!=blocks_.end();it++)if(!(*it).second->is_valid(enable_throws))return false;
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be positive");} return false;};
		return true;
	}

	// serialization
	std::string PathCosTheta::get_type(){
		return "rat::mdl::pathcostheta";
	}

	// serialization
	void PathCosTheta::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		// parents
		Transformations::serialize(js,list);

		// set type
		js["type"] = get_type();

		// lead end
		js["use_lead_end"] = use_lead_end_;

		// store element size
		js["element_size"] = element_size_;

		// serialize block list
		for(auto it=blocks_.begin();it!=blocks_.end();it++)
			js["blocks"].append(cmn::Node::serialize_node((*it).second, list));
		
	}

	// deserialization
	void PathCosTheta::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parents
		Transformations::deserialize(js,list,factory_list,pth);

		// lead end
		if(js.isMember("use_lead_end"))use_lead_end_ = js["use_lead_end"].asBool();

		// store element size
		set_element_size(js["element_size"].ASFLTP());

		// deserialize block list
		for(auto it=js["blocks"].begin();it!=js["blocks"].end();it++)
			add_block(cmn::Node::deserialize_node<CosThetaBlock>((*it), list, factory_list, pth));
	}

}}