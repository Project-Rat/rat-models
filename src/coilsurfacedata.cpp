// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "coilsurfacedata.hh"

// rat-common headers
#include "rat/common/extra.hh"
#include "rat/common/gauss.hh"
#include "rat/common/elements.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	CoilSurfaceData::CoilSurfaceData(){
		set_output_type("coilsurface");
	}

	// factory
	ShCoilSurfaceDataPr CoilSurfaceData::create(){
		return std::make_shared<CoilSurfaceData>();
	}

	// set operating current
	void CoilSurfaceData::set_operating_current(const fltp operating_current){
		operating_current_ = operating_current;
	}

	// get operating current
	fltp CoilSurfaceData::get_operating_current() const{
		return operating_current_;
	}

	// set number of turns
	void CoilSurfaceData::set_number_turns(const fltp number_turns){
		number_turns_ = number_turns;
	}

	// get number of turns
	fltp CoilSurfaceData::get_number_turns() const{
		return number_turns_;
	}

	// set material
	void CoilSurfaceData::set_material(rat::mat::ShConductorPr material){
		// assert(material!=NULL);
		material_ = material;
	}

	// get material
	rat::mat::ShConductorPr CoilSurfaceData::get_material() const{
		// assert(material_!=NULL);
		return material_;
	}

	// // current density
	// fltp CoilSurfaceData::calc_current_density() const{
	// 	// calculate cross section areas
	// 	const fltp cross_area = area_->get_area();

	// 	// calculate current distribution in cross section
	// 	const fltp Jop = number_turns_*operating_current_/cross_area;

	// 	// return current density
	// 	return Jop;
	// }

	// VTK export
	ShVTKObjPr CoilSurfaceData::export_vtk() const{
		// create unstructured grid
		ShVTKUnstrPr vtk = std::dynamic_pointer_cast<VTKUnstr>(SurfaceData::export_vtk());
		assert(vtk!=NULL);

		// add magnetic field angle
		if(has('B') && has_field()){
			// get magnetic flux density at nodes
			const arma::Mat<fltp> Bn = get_field('B');

			// calculate components along L, N and D
			const arma::Row<fltp> Blong = cmn::Extra::dot(Bn,L_);
			const arma::Row<fltp> Bnorm = cmn::Extra::dot(Bn,N_);
			const arma::Row<fltp> Btrns = cmn::Extra::dot(Bn,D_);

			// calculate angle in radians
			const arma::Row<fltp> alpha_rad = arma::atan2(arma::sqrt(Blong%Blong + Btrns%Btrns),Bnorm);	
				
			// convert to degree
			const arma::Row<fltp> alpha = 360.0*alpha_rad/(arma::Datum<fltp>::pi*2);

			// add to vtk
			vtk->set_nodedata(alpha,"Mgn. Field Angle [deg]"); 
		}

		// return the unstructured mesh
		return vtk;
	}

	// // get curret density at nodes
	// arma::Mat<fltp> CoilSurfaceData::get_nodal_current_density() const{
	// 	return L_*calc_current_density();
	// }

	// // calculate field angle
	// arma::Row<fltp> CoilSurfaceData::calc_alpha(const arma::Mat<fltp> &B) const{
	// 	// check number of columns
	// 	assert(B.n_cols==get_num_nodes());

	// 	// calculate components along L, N and D
	// 	const arma::Row<fltp> Bl = cmn::Extra::dot(B,L_);
	// 	const arma::Row<fltp> Bn = cmn::Extra::dot(B,N_);
	// 	const arma::Row<fltp> Bd = cmn::Extra::dot(B,D_);

	// 	// calculate angle
	// 	const arma::Row<fltp> alpha_rad = arma::atan2(arma::sqrt(Bl%Bl + Bd%Bd),Bn);

	// 	// return angle
	// 	return alpha_rad;
	// }

	// // calculate engineering current density
	// arma::Row<fltp> CoilSurfaceData::calc_Je(
	// 	const arma::Mat<fltp> &B, 
	// 	const arma::Row<fltp> &T) const{
	// 	// calculate alpha
	// 	arma::Row<fltp> alpha = calc_alpha(B);

	// 	// calculate magnitude of field
	// 	arma::Row<fltp> Bm = cmn::Extra::vec_norm(B);

	// 	// calculate engineering current density and return
	// 	return material_->calc_critical_current_density_mat(T, Bm, alpha);
	// }

	// // calculate engineering current density
	// arma::Row<fltp> CoilSurfaceData::calc_Tc(
	// 	const arma::Mat<fltp> &J, 
	// 	const arma::Mat<fltp> &B) const{
	// 	// calculate alpha
	// 	arma::Row<fltp> alpha = calc_alpha(B);

	// 	// calculate magnitude of field
	// 	arma::Row<fltp> Bm = cmn::Extra::vec_norm(B);
	// 	arma::Row<fltp> Jm = cmn::Extra::vec_norm(J);

	// 	// calculate engineering current density and return
	// 	return material_->calc_cs_temperature_mat(Jm, Bm, alpha);
	// }

	// // calculate electric field
	// arma::Mat<fltp> CoilSurfaceData::calc_E(
	// 	const arma::Mat<fltp> &J, 
	// 	const arma::Mat<fltp> &B, 
	// 	const arma::Row<fltp> &T) const{
	// 		// calculate alpha
	// 	arma::Row<fltp> alpha = calc_alpha(B);

	// 	// calculate magnitude of field
	// 	arma::Row<fltp> Bm = cmn::Extra::vec_norm(B);
	// 	arma::Row<fltp> Jm = cmn::Extra::vec_norm(J);

	// 	// calculate electric field
	// 	return L_.each_row()%material_->calc_electric_field_mat(Jm, T, Bm, alpha);	
	// }


}}
