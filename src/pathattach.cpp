// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathattach.hh"

// headers for models
#include "pathgroup.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathAttach::PathAttach(){
		set_name("Attach");
	}

	PathAttach::PathAttach(const std::list<ShPathPr>&path_list, const arma::uword fixed_index) : PathAttach(){
		for(auto it=path_list.begin();it!=path_list.end();it++)add_path(*it);
		set_fixed_index(fixed_index);
	}

	// factory
	ShPathAttachPr PathAttach::create(){
		return std::make_shared<PathAttach>();
	}

	ShPathAttachPr PathAttach::create(const std::list<ShPathPr>&path_list, const arma::uword fixed_index){
		return std::make_shared<PathAttach>(path_list, fixed_index);
	}


	// retreive model at index
	ShPathPr PathAttach::get_path(const arma::uword index) const{
		auto it = input_paths_.find(index);
		if(it==input_paths_.end())rat_throw_line("Index does not exist.");
		return (*it).second;
	}

	// delete model at index
	bool PathAttach::delete_path(const arma::uword index){	
		auto it = input_paths_.find(index);
		if(it==input_paths_.end())return false;
		(*it).second = NULL; return true;
	}

	// re-index nodes after deleting
	void PathAttach::reindex(){
		Transformations::reindex();
		std::map<arma::uword, ShPathPr> new_paths; arma::uword idx = 1;
		for(auto it=input_paths_.begin();it!=input_paths_.end();it++)
			if((*it).second!=NULL)new_paths.insert({idx++, (*it).second});
		input_paths_ = new_paths;
	}

	// function for adding a path to the path list
	arma::uword PathAttach::add_path(const ShPathPr &path){
		// check input
		assert(path!=NULL); 

		// get counters
		const arma::uword index = input_paths_.size()+1;

		// insert
		input_paths_.insert({index, path});

		// return index
		return index;
	}

	// get number of paths
	arma::uword PathAttach::get_num_paths() const{
		return input_paths_.size();
	}
	
	void PathAttach::set_fixed_index(const arma::uword fixed_index){
		fixed_index_ = fixed_index;
	}

	arma::uword PathAttach::get_fixed_index()const{
		return fixed_index_;
	}

	// validity check
	bool PathAttach::is_valid(const bool enable_throws)const{
		for(auto it=input_paths_.begin();it!=input_paths_.end();it++)
			if(!it->second->is_valid())return false;
		if(!Path::is_valid(enable_throws))return false;
		if(!Transformations::is_valid(enable_throws))return false;
		if(input_paths_.find(fixed_index_)==input_paths_.end()){if(enable_throws){rat_throw_line("fixed index does not exist");} return false;};
		return true;
	}

	// update function
	ShFramePr PathAttach::create_frame(const MeshSettings &stngs) const{
		// check validty
		if(!is_valid())rat_throw_line("attachment path is not valid");

		// get base path
		auto it2 = input_paths_.find(fixed_index_);
		if(it2==input_paths_.end())rat_throw_line("could not find path with fixed input");
		const ShPathPr& base_path = (*it2).second;

		// get frame from base
		const ShFramePr base_frame = base_path->create_frame(stngs);

		// if base frame empty
		if(base_frame->get_num_sections()==0)return NULL;

		// attachment point for previous path
		const arma::Col<fltp>::fixed<3> R0 = base_frame->get_coords(0).col(0);
		const arma::Col<fltp>::fixed<3> L0 = base_frame->get_direction(0).col(0);
		const arma::Col<fltp>::fixed<3> N0 = base_frame->get_normal(0).col(0);
		const arma::Col<fltp>::fixed<3> D0 = base_frame->get_transverse(0).col(0);

		// create a group
		const ShPathGroupPr prev_path_group = PathGroup::create(R0,-L0,N0);
		for(arma::uword i=fixed_index_-1;i>0;i--){
			auto it = input_paths_.find(i);
			if(it==input_paths_.end())rat_throw_line("path does not exist");
			prev_path_group->add_path((*it).second);
		}

		// create frames
		ShFramePr prev_frame;
		if(prev_path_group->get_num_paths()!=0){
			prev_frame = prev_path_group->create_frame(stngs);
			prev_frame->reverse();
		}

		// attachment point for next path
		const arma::uword base_num_sections = base_frame->get_num_sections();
		const arma::Col<fltp>::fixed<3> R1 = base_frame->get_coords(base_num_sections-1).tail_cols(1);
		const arma::Col<fltp>::fixed<3> L1 = base_frame->get_direction(base_num_sections-1).tail_cols(1);
		const arma::Col<fltp>::fixed<3> N1 = base_frame->get_normal(base_num_sections-1).tail_cols(1);
		const arma::Col<fltp>::fixed<3> D1 = base_frame->get_transverse(base_num_sections-1).tail_cols(1);;

		// create a group
		const ShPathGroupPr next_path_group = PathGroup::create(R1,L1,N1);
		for(arma::uword i=fixed_index_+1;i<=input_paths_.size();i++){
			auto it = input_paths_.find(i);
			if(it==input_paths_.end())rat_throw_line("path does not exist");
			next_path_group->add_path((*it).second);
		}

		// create frames
		ShFramePr next_frame;
		if(next_path_group->get_num_paths()!=0)
			next_frame = next_path_group->create_frame(stngs);

		// allocate output frame
		ShFramePr combined_frame;

		// incomplete frames
		if(prev_frame==NULL && next_frame==NULL)combined_frame = base_frame;
		if(prev_frame==NULL && next_frame!=NULL)combined_frame = Frame::create({base_frame, next_frame});
		if(prev_frame!=NULL && next_frame==NULL)combined_frame = Frame::create({prev_frame, base_frame});
		if(prev_frame!=NULL && next_frame!=NULL)combined_frame = Frame::create({prev_frame, base_frame, next_frame});

		// apply transformations
		combined_frame->apply_transformations(get_transformations(), stngs.time);

		// output the frame
		return combined_frame;
	}

	// get type
	std::string PathAttach::get_type(){
		return "rat::mdl::pathattach";
	}

	// method for serialization into json
	void PathAttach::serialize(Json::Value &js, cmn::SList &list) const{

		// parent objects
		Path::serialize(js,list);
		Transformations::serialize(js,list);

		// type
		js["type"] = get_type();

		// fixed index
		js["fixed_index"] = static_cast<int>(fixed_index_);

		// serialize the models
		for(auto it = input_paths_.begin();it!=input_paths_.end();it++)
			js["input_paths"].append(cmn::Node::serialize_node((*it).second, list));
	}

	// method for deserialisation from json
	void PathAttach::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent objects
		Path::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);

		// fixed index
		set_fixed_index(js["fixed_index"].asUInt64());

		// deserialize the models
		for(auto it = js["input_paths"].begin();it!=js["input_paths"].end();it++)
			add_path(cmn::Node::deserialize_node<Path>(
				(*it), list, factory_list, pth));
	}

}}