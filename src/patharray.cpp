// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "patharray.hh"

// common headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathArray::PathArray(){
		set_name("Array");
	}

	// factory
	ShPathArrayPr PathArray::create(){
		return std::make_shared<PathArray>();
	}

	// set number of copies in x-direction
	void PathArray::set_num_x(const arma::uword num_x){
		num_x_ = num_x;
	}

	// set number of copies in y-direction
	void PathArray::set_num_y(const arma::uword num_y){
		num_y_ = num_y;
	}

	// set number of copies in z-direction
	void PathArray::set_num_z(const arma::uword num_z){
		num_z_ = num_z;
	}

	// set shift distance in x-direction
	void PathArray::set_dx(const fltp dx){
		dx_ = dx;
	}

	// set shift distance in y-direction
	void PathArray::set_dy(const fltp dy){
		dy_ = dy;
	}
	
	// set shift distance in z-direction
	void PathArray::set_dz(const fltp dz){
		dz_ = dz;
	}

	// set centering
	void PathArray::set_centered(const bool centered){
		centered_ = centered;
	}

	// get number of copies in x-direction
	arma::uword PathArray::get_num_x()const{
		return num_x_;
	}

	// get number of copies in y-direction
	arma::uword PathArray::get_num_y()const{
		return num_y_;
	}

	// get number of copies in z-direction
	arma::uword PathArray::get_num_z()const{
		return num_z_;
	}

	// get shift distance in x-direction
	fltp PathArray::get_dx()const{
		return dx_;
	}

	// get shift distance in y-direction
	fltp PathArray::get_dy()const{
		return dy_;
	}

	// get shift distance in z-direction
	fltp PathArray::get_dz()const{
		return dz_;
	}
	
	// get cent
	bool PathArray::get_centered()const{
		return centered_;
	}

	// get frame
	ShFramePr PathArray::create_frame(const MeshSettings &stngs) const{
		// check if base path was set
		is_valid(true);

		// create frame
		const ShFramePr frame = input_path_->create_frame(stngs);

		// copy coordinates
		const arma::field<arma::Mat<fltp> >& R = frame->get_coords();
		const arma::field<arma::Mat<fltp> >& L = frame->get_direction();
		const arma::field<arma::Mat<fltp> >& D = frame->get_transverse();
		const arma::field<arma::Mat<fltp> >& N = frame->get_normal();
		const arma::field<arma::Mat<fltp> >& B = frame->get_block();

		
		// create a list of frames
		std::list<ShFramePr> frame_list;

		// walk over copies
		for(arma::uword i=0;i<num_x_;i++){
			for(arma::uword j=0;j<num_y_;j++){
				for(arma::uword k=0;k<num_z_;k++){
					// copy R
					arma::field<arma::Mat<fltp> > Roff(1,R.n_elem);

					// walk over sections
					for(arma::uword m=0;m<R.n_elem;m++){
						// calculate offset
						arma::Col<fltp>::fixed<3> offset{i*dx_,j*dy_,k*dz_};

						// apply centering
						if(centered_)offset -= arma::Col<fltp>::fixed<3>{
							(num_x_-1)*dx_/2,(num_y_-1)*dy_/2,(num_z_-1)*dz_/2};

						// offset coordinates
						Roff(m) = R(m).each_col() + offset;
					}

					// create frame
					frame_list.push_back(Frame::create(Roff,L,N,D,B));
				}
			}
		}

		// no frames
		if(frame_list.empty())return Frame::create();

		// create a combined frame
		const ShFramePr dashed_frame = Frame::create(frame_list);

		// apply_transformations
		dashed_frame->apply_transformations(get_transformations(), stngs.time);

		// create new frame
		return dashed_frame;
	}

	// re-index nodes after deleting
	void PathArray::reindex(){
		InputPath::reindex(); Transformations::reindex();
	}

	// vallidity check
	bool PathArray::is_valid(const bool enable_throws) const{
		if(!InputPath::is_valid(enable_throws))return false;
		if(!Transformations::is_valid(enable_throws))return false;
		if(num_x_<=0){if(enable_throws){rat_throw_line("number of copies in x-direction must be larger than zero");} return false;};
		if(num_y_<=0){if(enable_throws){rat_throw_line("number of copies in y-direction must be larger than zero");} return false;};
		if(num_z_<=0){if(enable_throws){rat_throw_line("number of copies in z-direction must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string PathArray::get_type(){
		return "rat::mdl::patharray";
	}

	// method for serialization into json
	void PathArray::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents
		Path::serialize(js,list);
		InputPath::serialize(js,list);
		Transformations::serialize(js,list);
		
		// type
		js["type"] = get_type();

		// settings
		js["centered"] = centered_;

		// subnodes
		js["num_x"] = static_cast<int>(num_x_);
		js["num_y"] = static_cast<int>(num_y_);
		js["num_z"] = static_cast<int>(num_z_);

		// subnodes
		js["dx"] = dx_;
		js["dy"] = dy_;
		js["dz"] = dz_;
	}

	// method for deserialisation from json
	void PathArray::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parents
		Path::deserialize(js,list,factory_list,pth);
		InputPath::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);

		// settings
		set_centered(js["centered"].asBool());
		set_num_x(js["num_x"].asUInt64());
		set_num_y(js["num_y"].asUInt64());
		set_num_z(js["num_z"].asUInt64());
		set_dx(js["dx"].ASFLTP());
		set_dy(js["dy"].ASFLTP());
		set_dz(js["dz"].ASFLTP());
	}


}}