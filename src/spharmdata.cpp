// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "spharmdata.hh"

#include "rat/mlfmm/spharm.hh"
#include "rat/mlfmm/extra.hh"

#include "vtkunstr.hh"
#include "vtktable.hh"
#include "meshdata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	SpHarmData::SpHarmData(){
		set_output_type("sphereharm");
	}

	// constructor
	SpHarmData::SpHarmData(
		const fltp reference_radius, 
		const int nmax, const int mmax,
		const arma::Col<fltp>::fixed<3> &Rcen) : SpHarmData(){
		set_reference_radius(reference_radius);
		set_nmmax(nmax,mmax);
		set_coordinate(Rcen);
		setup();
	}

	// factory methods
	ShSpHarmDataPr SpHarmData::create(){
		return std::make_shared<SpHarmData>();
	}

	// factory
	ShSpHarmDataPr SpHarmData::create(
		const fltp reference_radius, 
		const int nmax, const int mmax,
		const arma::Col<fltp>::fixed<3> &Rcen){
		return std::make_shared<SpHarmData>(reference_radius, nmax, mmax, Rcen);
	}

	// setters
	void SpHarmData::set_reference_radius(const fltp reference_radius){
		reference_radius_ = reference_radius;
	}

	// set max number of expansions
	void SpHarmData::set_nmmax(
		const int nmax, const int mmax){
		nmax_ = nmax; mmax_ = mmax;
	}

	// set target coordinate
	void SpHarmData::set_coordinate(const arma::Col<fltp>::fixed<3> &Rcen){
		Rcen_ = Rcen;
	}


	// get number of planes
	arma::uword SpHarmData::get_num_planes()const{
		return nmax_+1;
	}

	// get number of points
	arma::uword SpHarmData::get_num_points()const{
		return 2*mmax_+1;
	}


	// calculation function
	void SpHarmData::setup(){
		// get number of points needed
		const arma::uword num_planes = get_num_planes(); // number in theta
		const arma::uword num_points = get_num_points(); // number in phi
		const arma::uword num_radial = 1;       // number in r

		// output field required
		set_field_type("BH",{3,3}); // analysis is performed on B, H is only used in VTK files

		// calculate points in theta
		arma::Row<fltp> x, wt;
		quadrature(x,wt,num_planes,0);
		theta_ = arma::fliplr(arma::acos(2*x-1));
		wtheta_ = arma::fliplr(2*wt);

		// calculate points in phi
		const fltp dphi = arma::Datum<fltp>::pi/num_points;
		phi_ = arma::linspace<arma::Row<fltp> >(dphi,2*arma::Datum<fltp>::pi-dphi,num_points);
		wphi_ = 2*arma::Datum<fltp>::pi*arma::Row<fltp>(num_points,arma::fill::ones)/num_points;

		// radial weight
		arma::Row<fltp> r, wr;
		quadrature(r,wr,num_radial,2);
		wr_ = arma::as_scalar(wr);

		// set number of targets
		num_targets_ = num_planes*num_points;

		// allocate
		Rt_.set_size(3,num_targets_);

		// walk over theta
		for(arma::uword i=0;i<theta_.n_elem;i++){
			// create coordinates
			const arma::uword idx1 = i*num_points;
			const arma::uword idx2 = (i+1)*num_points-1;
			Rt_.cols(idx1,idx2)= arma::join_vert(
				reference_radius_*std::sin(theta_(i))*arma::cos(phi_),
				reference_radius_*std::sin(theta_(i))*arma::sin(phi_),
				reference_radius_*std::cos(theta_(i))*arma::Row<fltp>(num_points,arma::fill::ones));
		}

		// move sphere
		Rt_.each_col()+=Rcen_;
	}

	// post processing
	void SpHarmData::post_process(){
		// get number of points needed
		const arma::uword num_planes = get_num_planes(); // number in theta
		const arma::uword num_points = get_num_points(); // number in phi
		// const arma::uword num_radial = 1;

		// max order and degree
		const arma::uword polesize = rat::fmm::Extra::polesize(nmax_);

		// convert to spherical coordinates (rho,theta,phi)
		const arma::Mat<fltp> dRsph = rat::fmm::Extra::cart2sph(Rt_.each_col() - Rcen_);

		// create a set of spherical harmonics
		const rat::fmm::Spharm spherical_harmonics(nmax_, dRsph);
		const arma::Mat<std::complex<fltp> >& Ynm = spherical_harmonics.get_all();

		// get field
		const arma::Row<fltp> Bz = get_field('B').row(2);

		// factor for compensating weights 3 for rho, 2*pi for phi, 1/2 for theta
		const fltp xfactor = RAT_CONST(3.0)/(2*2*arma::Datum<fltp>::pi);

		// allocate
		arma::Col<fltp> Anm(polesize,arma::fill::zeros);
		arma::Col<fltp> Bnm(polesize,arma::fill::zeros);

		// create factorial list
		const arma::Col<fltp> facs = fmm::Extra::factorial(2*nmax_);

		// fix for normalization
		arma::Col<fltp> factor(polesize,arma::fill::zeros);

		// walk over nm and make list
		for(int n=0;n<=nmax_;n++){
			for(int m=0;m<=std::min(n,mmax_);m++){ // here we clip off all higher order harmonics
				// calculate scaling
				factor(fmm::Extra::nm2fidx(n,m)) = 
					std::cos(arma::Datum<fltp>::pi*m)*
					(2*n+1)*std::sqrt(facs(n-m)/facs(n+m));

				// // for mmax divide by factor 2
				// // this apparently gives better 
				// // convergence when converting
				// // to field and back several times
				// if(use_nmax_factor_ && n!=m && m==mmax_)
				// 	factor(fmm::Extra::nm2fidx(n,m))/=2; // no longer needed as it was fixed by using one extra point

				// mirror other side
				factor(fmm::Extra::nm2fidx(n,-m)) = 
					factor(fmm::Extra::nm2fidx(n,m));
			}
		}

		// fmm::Extra::display_pole(nmax_,factor,cmn::Log::create());

		// walk over harmonics
		// fltp weight = 0;
		for(arma::uword i=0;i<num_planes;i++){
			// get indices of this plane
			const arma::uword idx1 = i*num_points;
			const arma::uword idx2 = (i+1)*num_points-1;
			
			// add plane contribution
			Anm += wr_*wtheta_(i)*xfactor*factor%arma::sum(
				arma::real(Ynm.cols(idx1,idx2)).eval()
				.each_row()%(Bz.cols(idx1,idx2)%wphi_),1);
			Bnm += wr_*wtheta_(i)*xfactor*factor%arma::sum(
				arma::imag(Ynm.cols(idx1,idx2)).eval()
				.each_row()%(Bz.cols(idx1,idx2)%wphi_),1);

			// count weight
			// weight += wr_*wtheta_(i)*arma::accu(wphi_);
		}

		// convert to complex
		Znm_.set_size(polesize);
		Znm_.set_real(Anm);
		Znm_.set_imag(Bnm);
	}

	// convert to field
	arma::Row<fltp> SpHarmData::spharm2field()const{
		return spharm2field(reference_radius_);
	}

	// convert to field
	arma::Row<fltp> SpHarmData::spharm2field(const fltp Rp)const{
		// get number of points needed
		// const arma::uword num_planes = nmax_+1; // number in theta
		// const arma::uword num_points = 2*mmax_; // number in phi

		// max order and degree
		const arma::uword polesize = rat::fmm::Extra::polesize(nmax_);

		// convert to spherical coordinates (rho,theta,phi)
		const arma::Mat<fltp> dRsph = rat::fmm::Extra::cart2sph(Rt_.each_col()-Rcen_);

		// create a set of spherical harmonics
		const rat::fmm::Spharm spherical_harmonics(nmax_, dRsph);
		const arma::Mat<std::complex<fltp> >& Ynm = spherical_harmonics.get_all();

		// create factorial list
		const arma::Col<fltp> facs = fmm::Extra::factorial(2*nmax_);

		// fix for normalization
		arma::Col<fltp> factor(polesize,arma::fill::zeros);

		// walk over nm and make list
		for(int n=0;n<=nmax_;n++){
			for(int m=0;m<=std::min(n,mmax_);m++){ // here we clip off all higher order harmonics
				// calculate scaling
				factor(fmm::Extra::nm2fidx(n,m)) = 
					std::cos(arma::Datum<fltp>::pi*m)*
					std::pow(reference_radius_/Rp,n)/
					std::sqrt(facs(n-m)/facs(n+m));

				// mirror other side
				factor(fmm::Extra::nm2fidx(n,-m)) = 
					factor(fmm::Extra::nm2fidx(n,m));
			}
		}

		// calculate Bz
		const arma::Row<fltp> Bz = arma::sum(
			arma::real(Ynm).eval().each_col()%(arma::real(Znm_)%factor) + 
			arma::imag(Ynm).eval().each_col()%(arma::imag(Znm_)%factor),0);

		// return calculated field
		return Bz;
	}


	// getting calculated harmonic tables (in L0 Normalisation)
	arma::Col<std::complex<fltp> > SpHarmData::get_harmonics_L0() const{
		const arma::Col<std::complex<fltp> > Znm_L0 = calc_L4_to_L0()%Znm_;
		return Znm_L0;
	}

	// getting calculated harmonic tables (in L4 Normalisation)
	const arma::Col<std::complex<fltp> >& SpHarmData::get_harmonics_L4() const{
		return Znm_;
	}

	// getting relative harmonic tables (in L0 Normalisation)
	arma::Col<std::complex<fltp> > SpHarmData::get_harmonics_L0_ppm() const{
		const arma::Col<std::complex<fltp> > Znm = get_harmonics_L0();
		const arma::Col<fltp> Znm_real = arma::real(Znm);
		const arma::Col<fltp> Znm_imag = arma::imag(Znm);
		const fltp Znmmax = std::max(arma::max(Znm_real),arma::max(Znm_imag));
		const arma::Col<std::complex<fltp> > Znm_ppm = RAT_CONST(1e6)*(Znm/Znmmax);
		return Znm_ppm;
	}

	// getting relative harmonic tables (in L4 Normalisation)
	arma::Col<std::complex<fltp> > SpHarmData::get_harmonics_L4_ppm() const{
		const arma::Col<std::complex<fltp> > Znm = get_harmonics_L4();
		const arma::Col<fltp> A = arma::abs(arma::real(Znm));
		const arma::Col<fltp> B = arma::abs(arma::imag(Znm));
		const rat::fltp Amax = arma::max(A);
		const rat::fltp Bmax = arma::max(B);
		const rat::fltp Znmmax = std::max(Amax,Bmax);
		return RAT_CONST(1e6)*Znm_/Znmmax;
	}

	// conversion factor from L4 to L0 normalization
	arma::Col<fltp> SpHarmData::calc_L4_to_L0()const{
		// create factorial list
		const arma::Col<fltp> facs = fmm::Extra::factorial(nmax_+mmax_);

		// size of the harmonics vector
		const arma::uword polesize = fmm::Extra::polesize(nmax_);

		// allocate scaling
		arma::Col<fltp> Wnm(polesize,arma::fill::zeros);
		
		// walk over order and degree
		for(int n=0;n<nmax_;n++){
			Wnm(fmm::Extra::nm2fidx(n,0)) = RAT_CONST(1.0);
			for(int m=1;m<=std::min(n,mmax_);m++){
				Wnm(fmm::Extra::nm2fidx(n,-m)) = std::sqrt(((2*n+1)*facs(n+m))/(2*facs(n-m)));
				Wnm(fmm::Extra::nm2fidx(n,m)) = std::sqrt(((2*n+1)*facs(n-m))/(2*facs(n+m)));
			}
		}

		// return conversion factor
		return Wnm;
	}

	// conversion factor from L4 to L0 normalization
	arma::Col<fltp> SpHarmData::calc_L0_to_L4()const{
		return RAT_CONST(1.0)/calc_L4_to_L0();
	}

	// display function
	void SpHarmData::display(const cmn::ShLogPr &lg) const{
		// display harmonic
		lg->msg(2,"%sHarmonic Content in [T]%s\n",KBLU,KNRM);
		lg->msg("using %sL4%s normalization\n",KYEL,KNRM);
		lg->msg("reference radius: %8.2e [mm]",1e3*reference_radius_);
		fmm::Extra::display_pole(nmax_,mmax_,get_harmonics_L4(),lg,3);
		lg->msg(-2);
	}

	// display function
	void SpHarmData::display_ppm(const cmn::ShLogPr &lg) const{
		// display harmonic
		lg->msg(2,"%sHarmonic Content in [ppm]%s\n",KBLU,KNRM);
		lg->msg("using %sL4%s normalization\n",KYEL,KNRM);
		lg->msg("reference radius: %8.2e [mm]",1e3*reference_radius_);
		fmm::Extra::display_pole(nmax_,mmax_,get_harmonics_L4_ppm(),lg,3);
		lg->msg(-2);
	}

	// quadrature function for spheres
	// https://se.mathworks.com/matlabcentral/fileexchange/10750-
	// quadrature-rules-for-spherical-volume-integrals
	void SpHarmData::quadrature(
		arma::Row<fltp> &abscissae, 
		arma::Row<fltp> &weights,
		const arma::uword num_points, 
		const arma::uword k){

		// get constants
		const arma::uword k1=k+1; 
		const arma::uword k2=k+2; 
		const fltp B0 = std::exp2(k1)/k1;
		const fltp B1 = (RAT_CONST(4.0)*k1)/(k2*k2*(k+3));
		const fltp A0 = fltp(k)/k2;

		// calculate A
		const arma::Row<arma::uword> n1 = arma::regspace<arma::Row<arma::uword> >(1,num_points);
		const arma::Row<arma::uword> nnk1 = 2*n1+k;
		const arma::Row<fltp> A = arma::join_horiz(
			arma::Row<fltp>{A0}, fltp(k*k)/arma::conv_to<arma::Row<fltp> >::from(nnk1%(nnk1+2)));

		// matrix 
		arma::Mat<fltp> M;
		if(num_points==1){
			M = arma::Col<fltp>{A0};
		}

		else{
			// calculate B
			const arma::Row<arma::uword> n2 = arma::regspace<arma::Row<arma::uword> >(2,num_points); 
			const arma::Row<arma::uword> nnk2 = nnk1.cols(n2-1);
			const arma::Row<arma::uword> nk=n2+k; 
			const arma::Row<fltp> nnksq = arma::conv_to<arma::Row<fltp> >::from(arma::square(nnk2));
			const arma::Row<fltp> B = RAT_CONST(4.0)*(n2%nk)%(n2%nk)/(nnksq%nnksq-nnksq);

			// combine A and B
			const arma::Mat<fltp> ab = arma::join_horiz(A.t(),arma::join_vert(arma::Col<fltp>{B0,B1},B.t())); 

			// create off diagonal
			const arma::Col<fltp> s = arma::sqrt(ab.col(1).rows(1,num_points-1));

			// combine diagonals
			M = arma::diagmat(ab.col(0).rows(0,num_points-1)) + arma::diagmat(s,-1) + arma::diagmat(s,1);
		}

		// calculate eigen values
		arma::Col<fltp> eigval; 
		arma::Mat<fltp> eigvec;
		arma::eig_sym(eigval,eigvec,M);

		// sort by values on diagonal 
		const arma::Col<arma::uword> sorting_indices = arma::sort_index(eigval);
		eigval = eigval(sorting_indices);
		eigvec = eigvec.cols(sorting_indices);

		// get abscissae and weights
		abscissae = (eigval.t()+1)/2; 
		weights = std::pow(RAT_CONST(0.5),k1)*B0*(eigvec.row(0)%eigvec.row(0));
	}



	// export points/lines to vtk file
	ShVTKObjPr SpHarmData::export_vtk() const{
		// check if setup
		assert(!Rt_.empty());

		// create point elements
		const arma::Row<arma::uword> n = arma::regspace<arma::Row<arma::uword> >(0,Rt_.n_cols-1);
			
		// mesh data
		const ShMeshDataPr mesh_data = MeshData::create(Rt_,n,n);
		mesh_data->set_field_type("HB",{3,3});
		mesh_data->setup_targets(); mesh_data->allocate();
		mesh_data->add_field('H',get_field('H'));
		mesh_data->add_field('B',get_field('B'));
		mesh_data->post_process();

		// return data object
		return mesh_data->export_vtk();
	}

	// // VTK exporting
	// ShVTKObjPr SpHarmData::export_vtk_table() const{
	// 	return NULL;
	// }

}}