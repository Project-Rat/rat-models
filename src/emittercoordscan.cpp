// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "emittercoordscan.hh"

// rat-common headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	EmitterCoordScan::EmitterCoordScan(){
		// set name
		set_name("Coordinates Scan");

		// protons are default
		set_proton(); beam_energy_ = rest_mass_ + 0.1;
	}

	// factory
	ShEmitterCoordScanPr EmitterCoordScan::create(){
		return std::make_shared<EmitterCoordScan>();
	}

	// set protons
	void EmitterCoordScan::set_proton(){
		rest_mass_ = 0.938; // [GeV/C^2] 
		charge_ = 1; // elementary charge
	}

	// set electrons
	void EmitterCoordScan::set_electron(){
		rest_mass_ = 0.51099895000e-3; // [GeV/C^2] 
		charge_ = -1; // elementary charge
	}


	// set rest mass
	void EmitterCoordScan::set_rest_mass(const fltp rest_mass){
		// if(rest_mass<=0)rat_throw_line("rest mass must be larger than zero");
		rest_mass_ = rest_mass;
	}

	// set charge
	void EmitterCoordScan::set_charge(const fltp charge){
		charge_ = charge;
	}

	// set beam energy
	void EmitterCoordScan::set_beam_energy(const fltp beam_energy){
		beam_energy_ = beam_energy;
	}

	// set particle lifetime
	void EmitterCoordScan::set_lifetime(const arma::uword lifetime){
		lifetime_ = lifetime; 
	}

	// set starting index
	void EmitterCoordScan::set_start_idx(const arma::uword start_idx){
		start_idx_ = start_idx;
	}

	// getting the rest mass of the particles
	fltp EmitterCoordScan::get_rest_mass() const{
		return rest_mass_;
	}

	// set orientation
	void EmitterCoordScan::set_spawn_coord(
		const arma::Col<fltp>::fixed<3> &R, 
		const arma::Col<fltp>::fixed<3> &L, 
		const arma::Col<fltp>::fixed<3> &N, 
		const arma::Col<fltp>::fixed<3> &D){

		R_ = R;
		L_ = L.each_row()/cmn::Extra::vec_norm(L); 
		N_ = N.each_row()/cmn::Extra::vec_norm(N);
		D_ = D.each_row()/cmn::Extra::vec_norm(D);
	} 

	void EmitterCoordScan::setup_particle(
		Particle &particle, const fltp x, const fltp xa, 
		const fltp y, const fltp ya, const fltp pt, const fltp time) const{

		// nominal beam momentum
		const fltp p0 = std::sqrt(beam_energy_*beam_energy_ - rest_mass_*rest_mass_);

		// Energy deviation from nominal
		const fltp DeltaE = pt*p0;
		const fltp E = beam_energy_ + DeltaE;  // Actual particle energy
		const fltp p = std::sqrt(E*E-rest_mass_*rest_mass_);  // Actual particle momentum

		// Momentum components in Frenet-Serret coordinate system
		const fltp px = p0*xa;
		const fltp py = p0*ya;
		const fltp ps = std::sqrt(p*p-px*px-py*py); // Forward momentum component

		const fltp gamma = E/rest_mass_;
		const fltp V = std::sqrt(1.0 - 1.0/(gamma*gamma));
		const fltp Vx = px/p*V;
		const fltp Vy = py/p*V;
		const fltp Vs = ps/p*V;

		arma::Col<fltp> R0(3), V0(3);

		for(arma::uword i=0; i<3; ++i){
			R0(i) = R_(i)     + N_(i)*x  + D_(i)*y;
			V0(i) = L_(i)*Vs  + N_(i)*Vx + D_(i)*Vy;
		}

		// apply transformations
		for(auto it=trans_.begin();it!=trans_.end();it++){
			(*it).second->apply_vectors(R0,V0,'L',time);
			(*it).second->apply_coords(R0,time);
		}

		// set maximum number of steps
		particle.set_num_steps(lifetime_);

		// set start coordinate and velocity
		particle.set_startcoord(R0,V0);
								
		// set start index
		particle.set_start_index(start_idx_);
								
		// set mass
		particle.set_rest_mass(rest_mass_);
		particle.set_charge(charge_);
								
		// setup internal storage
		particle.setup();

	}

	// particle creation
	arma::field<Particle> EmitterCoordScan::spawn_particles(const fltp time) const{
		is_valid(true);

		arma::field<Particle> particles(nx_*nxa_*ny_*nya_*npt_);

		arma::uword particle_index = 0;

		for(arma::uword ix=0; ix<nx_; ++ix)
		{
			const fltp x = -x_range_ + 2*x_range_/std::max(1llu,nx_-1)*ix;
			for(arma::uword ixa=0; ixa<nxa_; ++ixa)
			{
				const fltp xa = -xa_range_ + 2*xa_range_/std::max(1llu,nxa_-1)*ixa;
				for(arma::uword iy=0; iy<ny_; ++iy)
				{
					const fltp y = -y_range_ + 2*y_range_/std::max(1llu,ny_-1)*iy;
					for(arma::uword iya=0; iya<nya_; ++iya)
					{
						const fltp ya = -ya_range_ + 2*ya_range_/std::max(1llu,nya_-1)*iya;
						for(arma::uword ipt=0; ipt<npt_; ++ipt)
						{
							const fltp pt=-pt_range_ + 2*pt_range_/std::max(1llu,npt_-1)*ipt;  // MAD-X: PT = DeltaE/(p0*c), but now c=0

							setup_particle(particles(particle_index), x, xa, y, ya, pt, time);
							++particle_index;
						}
					}
				}
			}
		}

		// return list of particles
		return particles;
	}

	// create specific mesh
	std::list<ShMeshDataPr> EmitterCoordScan::create_meshes(
		const std::list<arma::uword> &trace, 
		const MeshSettings &stngs) const{
		
		// check if valid
		if(!trace.empty())rat_throw_line("max trace depth reached but trace not empty");
		if(!is_valid(stngs.enable_throws))return{};

		// draw ellipse
		const arma::uword num_elements = 30;
		const arma::uword num_nodes = num_elements+1;
		const arma::Row<fltp> theta = arma::linspace<arma::Row<fltp> >(0,2*arma::Datum<fltp>::pi,num_nodes);
		const arma::Row<fltp> u = x_range_*arma::cos(theta);
		const arma::Row<fltp> v = y_range_*arma::sin(theta);
		const arma::Row<fltp> w(num_nodes, arma::fill::zeros);

		// combine
		arma::Mat<fltp> Rc(3,num_nodes);
		for(arma::uword i=0;i<3;i++)
			Rc.row(i) = R_(i) + L_(i)*w + N_(i)*u + D_(i)*v;

		// add center line
		Rc = arma::join_horiz(Rc, -2*L_*std::max(x_range_,y_range_), 2*L_*std::max(x_range_,y_range_));


		// apply transformations
		for(auto it=trans_.begin();it!=trans_.end();it++)
			(*it).second->apply_coords(Rc,stngs.time);
		
		// create coords
		arma::Mat<arma::uword> n = arma::join_vert(
			arma::regspace<arma::Row<arma::uword> >(0,num_nodes-2),
			arma::regspace<arma::Row<arma::uword> >(1,num_nodes-1));

		// add center line
		n = arma::join_horiz(n, arma::Col<arma::uword>::fixed<2>{num_nodes, num_nodes+1});

		// create mesh
		ShMeshDataPr mesh_data = MeshData::create(Rc, n, n);

		// draw mesh
		// set temperature
		mesh_data->set_operating_temperature(0);

		// set time
		mesh_data->set_time(stngs.time);

		// set name (is appended by models later)
		mesh_data->append_name(myname_);

		// flag this mesh as calculation
		mesh_data->set_calc_mesh();
		
		// mesh data object
		return {mesh_data};
	}

	// validity check
	bool EmitterCoordScan::is_valid(const bool enable_throws) const{
		if(rest_mass_<=0){if(enable_throws){rat_throw_line("rest mass must be larger than zero");} return false;};
		if(beam_energy_<=rest_mass_){if(enable_throws){rat_throw_line("beam energy must exceed rest mass");} return false;};
		if(lifetime_<=0){if(enable_throws){rat_throw_line("lifetime must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string EmitterCoordScan::get_type(){
		return "rat::mdl::emittercoordscan";
	}

	// method for serialization into json
	void EmitterCoordScan::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Emitter::serialize(js,list);

		// properties
		js["type"] = get_type();

		// beam parameters
		js["beam_energy"] = beam_energy_;
		js["rest_mass"] = rest_mass_;
		js["charge"] = charge_;

		// position orientation
		js["position"] = Node::serialize_matrix(R_);
		js["longitudinal"] = Node::serialize_matrix(L_);
		js["normal"] = Node::serialize_matrix(N_);
		js["transverse"] = Node::serialize_matrix(D_);

		// beam
		js["x_range"] = x_range_;
				js["nx"] = static_cast<unsigned int>(nx_);
		js["xa_range"] = xa_range_;
				js["nxa"] = static_cast<unsigned int>(nxa_);
		js["y_range"] = y_range_;
				js["ny"] = static_cast<unsigned int>(ny_);
		js["ya_range"] = ya_range_;
				js["nya"] = static_cast<unsigned int>(nya_);
		js["pt_range"] = pt_range_;
				js["npt"] = static_cast<unsigned int>(npt_);

		// tracking
		js["lifetime"] = static_cast<unsigned int>(lifetime_);
		js["start_idx"] = static_cast<unsigned int>(start_idx_);
	}

	// method for deserialisation from json
	void EmitterCoordScan::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		
		// parent
		Emitter::deserialize(js,list,factory_list,pth);
		
		// beam parameters
		beam_energy_ = js["beam_energy"].ASFLTP();
		rest_mass_ = js["rest_mass"].ASFLTP();
		charge_ = js["charge"].ASFLTP();

		// position orientation
		R_ = Node::deserialize_matrix(js["position"]);
		L_ = Node::deserialize_matrix(js["longitudinal"]);
		N_ = Node::deserialize_matrix(js["normal"]);
		D_ = Node::deserialize_matrix(js["transverse"]);

				// coord scan parameters
		x_range_  = js["x_range"].ASFLTP();
				nx_       = js["nx"].asUInt64();
		xa_range_ = js["xa_range"].ASFLTP();
				nxa_      = js["nxa"].asUInt64();
		y_range_  = js["y_range"].ASFLTP();
				ny_       = js["ny"].asUInt64();
		ya_range_ = js["ya_range"].ASFLTP();
				nya_      = js["nya"].asUInt64();
		pt_range_ = js["pt_range"].ASFLTP();
				npt_      = js["npt"].asUInt64();


		// tracking
		lifetime_ = js["lifetime"].asUInt64();
		start_idx_ = js["start_idx"].asUInt64();
	}


}}
