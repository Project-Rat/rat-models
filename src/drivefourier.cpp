// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "drivefourier.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	DriveFourier::DriveFourier(){
		set_name("Fourier");
	}

	// constructor
	DriveFourier::DriveFourier(
		const arma::Col<fltp> &an, 
		const arma::Col<fltp> &bn) : DriveFourier(){
		set_an(an); set_bn(bn);
	}

	// factory
	ShDriveFourierPr DriveFourier::create(){
		return std::make_shared<DriveFourier>();
	}

	// factory
	ShDriveFourierPr DriveFourier::create(
		const arma::Col<fltp> &an, 
		const arma::Col<fltp> &bn){
		return std::make_shared<DriveFourier>(an,bn);
	}

	// set properties
	void DriveFourier::set_an(const arma::Col<fltp> &an){
		an_ = an;
	}

	void DriveFourier::set_bn(const arma::Col<fltp> &bn){
		bn_ = bn;
	}


	// get properties
	const arma::Col<fltp>& DriveFourier::get_an()const{
		return an_;
	}

	const arma::Col<fltp>& DriveFourier::get_bn()const{
		return bn_;
	}


	// get current
	fltp DriveFourier::get_scaling(
		const fltp position,
		const fltp /*time*/, // TODO add phase velocity
		const arma::uword derivative) const{

		// start at zero
		fltp scale = RAT_CONST(0.0);
		
		// add components of the fourier series
		for(arma::uword i=0;i<an_.n_elem;i++)
			scale += an_(i)*std::pow(i*arma::Datum<fltp>::tau,derivative)*(derivative%4==1 || derivative%4==2 ? -1.0 : 1.0)*
			(derivative%2==0 ? std::cos(i*arma::Datum<fltp>::tau*position) : std::sin(i*arma::Datum<fltp>::tau*position));
		for(arma::uword i=0;i<bn_.n_elem;i++)
			scale += bn_(i)*std::pow(i*arma::Datum<fltp>::tau,derivative)*(derivative%4==2 || derivative%4==3 ? -1.0 : 1.0)*
			(derivative%2==0 ? std::sin(i*arma::Datum<fltp>::tau*position) : std::cos(i*arma::Datum<fltp>::tau*position));

		// return scaling
		return scale;
	}

	// apply scaling for the input settings
	void DriveFourier::rescale(const fltp scale_factor){
		an_ *= scale_factor; bn_ *= scale_factor;
	}

	// get type
	std::string DriveFourier::get_type(){
		return "rat::mdl::drivefourier";
	}

	// method for serialization into json
	void DriveFourier::serialize(
		Json::Value &js, cmn::SList &list) const{
		Drive::serialize(js,list);
		js["type"] = get_type();
		for(arma::uword n=0;n<an_.n_elem;n++)
			js["an"].append(an_(n));
		for(arma::uword n=0;n<bn_.n_elem;n++)
			js["bn"].append(bn_(n));
	}

	// method for deserialisation from json
	void DriveFourier::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		Drive::deserialize(js,list,factory_list,pth);

		an_.set_size(js["an"].size()); arma::uword idx = 0;
		
		for(auto it = js["an"].begin();it!=js["an"].end();it++)
			an_(idx++) = (*it).ASFLTP();
		
		bn_.set_size(js["bn"].size()); idx = 0;
		for(auto it = js["bn"].begin();it!=js["bn"].end();it++)
			bn_(idx++) = (*it).ASFLTP();
	}

}}