// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modeledges.hh"

#include "rat/common/elements.hh"
#include "rat/common/error.hh"

#include "crossrectangle.hh"
#include "coildata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ModelEdges::ModelEdges(){
		set_name("Edges");
	}

	// constructor
	ModelEdges::ModelEdges(
		const ShPathPr& pth1, const ShPathPr& pth2, 
		const ShPathPr& pth3, const ShPathPr& pth4,
		const arma::uword num_n, const arma::uword num_t) : ModelEdges(){

		// set to self
		input_paths_[1] = pth1; input_paths_[2] = pth2; 
		input_paths_[3] = pth3; input_paths_[4] = pth4;
		num_n_ = num_n; num_t_ = num_t;
	}

	// factory
	ShModelEdgesPr ModelEdges::create(){
		//return ShModelEdgesPr(new ModelEdges);
		return std::make_shared<ModelEdges>();
	}

	// factory
	ShModelEdgesPr ModelEdges::create(
		const ShPathPr& pth1, const ShPathPr& pth2, 
		const ShPathPr& pth3, const ShPathPr& pth4,
		const arma::uword num_n, const arma::uword num_t){
		//return ShModelEdgesPr(new ModelEdges);
		return std::make_shared<ModelEdges>(pth1, pth2, pth3, pth4, num_n, num_t);
	}


	// set current sharing 
	void ModelEdges::set_enable_current_sharing(const bool enable_current_sharing){
		enable_current_sharing_ = enable_current_sharing;
	}
	
	// get current sharing 
	bool ModelEdges::get_enable_current_sharing()const{
		return enable_current_sharing_;
	}

	// set operating temperature 
	fltp ModelEdges::get_operating_temperature() const{
		return operating_temperature_;
	}

	// get number of elements in normal direction
	arma::uword ModelEdges::get_num_n()const{
		return num_n_;
	}

	// get number of elements in transverse direction
	arma::uword ModelEdges::get_num_t()const{
		return num_t_;
	}

	// get number of elements in normal direction
	void ModelEdges::set_num_n(const arma::uword num_n){
		num_n_ = num_n;
	}

	// get number of elements in transverse direction
	void ModelEdges::set_num_t(const arma::uword num_t){
		num_t_ = num_t;
	}

	// set circuit index
	void ModelEdges::set_circuit_index(const arma::uword circuit_index){
		circuit_index_ = circuit_index;
	}

	// set operating current 
	void ModelEdges::set_operating_current(const fltp operating_current){
		operating_current_ = operating_current;
	}
	
	// set number of turns
	void ModelEdges::set_num_turns(const fltp num_turns){
		num_turns_ = num_turns;
	}

	// set number of gauss points
	void ModelEdges::set_num_gauss(const arma::sword num_gauss){
		num_gauss_ = num_gauss;
	}

	void ModelEdges::set_softening(const fltp softening){
		softening_ = softening;
	}


	// get circuit index
	arma::uword ModelEdges::get_circuit_index()const{
		return circuit_index_;
	}

	// set operating temperature 
	void ModelEdges::set_operating_temperature(const fltp operating_temperature){
		operating_temperature_ = operating_temperature;
	}

	// getting of coil number of turns
	fltp ModelEdges::get_num_turns() const{
		return num_turns_;
	}
	
	// operating current
	fltp ModelEdges::get_operating_current() const{
		return operating_current_;
	}

	// set edge refinement option
	void ModelEdges::set_refine_edges(const bool refine_edges){
		refine_edges_ = refine_edges;
	}

	// set path vector option
	void ModelEdges::set_use_path_vectors(const bool use_path_vectors){
		use_path_vectors_ = use_path_vectors;
	}
	
	// get edge refinement option
	bool ModelEdges::get_refine_edges() const{
		return refine_edges_;
	}	

	// get path vector option
	bool ModelEdges::get_use_path_vectors() const{
		return use_path_vectors_;
	}

	arma::sword ModelEdges::get_num_gauss()const{
		return num_gauss_;
	}

	fltp ModelEdges::get_softening()const{
		return softening_;
	}

	// retreive path at index
	const ShPathPr& ModelEdges::get_path(const arma::uword index) const{
		auto it = input_paths_.find(index);
		if(it==input_paths_.end())rat_throw_line("index does not exist");
		return (*it).second;
	}

	// delete model at index
	bool ModelEdges::delete_path(const arma::uword index){	
		auto it = input_paths_.find(index);
		if(it==input_paths_.end())return false;
		(*it).second = NULL; return true;
	}

	// re-index nodes after deleting
	void ModelEdges::reindex(){
		Transformations::reindex();
		std::map<arma::uword, ShPathPr> new_paths; arma::uword idx = 1;
		for(auto it=input_paths_.begin();it!=input_paths_.end();it++)
			if((*it).second!=NULL)new_paths.insert({idx++, (*it).second});
		input_paths_ = new_paths;
	}

	// function for adding a path to the path list
	arma::uword ModelEdges::add_path(const ShPathPr &path){
		// check input
		assert(path!=NULL); 

		// get counters
		const arma::uword index = input_paths_.size()+1;

		// insert
		input_paths_.insert({index, path});

		// return index
		return index;
	}

	// get number of paths
	arma::uword ModelEdges::get_num_paths() const{
		return input_paths_.size();
	}



	// method for aligning edge coordinates to improve
	// element quality
	void ModelEdges::refine_edges(
		arma::field<arma::Mat<fltp> > &R1234,
		arma::field<arma::Mat<fltp> > &L1234,
		arma::field<arma::Mat<fltp> > &N1234,
		arma::field<arma::Mat<fltp> > &D1234)const{

		// get number of sections
		const arma::uword num_sections = R1234.n_elem;

		// calculate location
		arma::field<arma::Mat<fltp> > ell1234(1,R1234.n_elem);
		arma::Col<fltp>::fixed<4> ell_shift(arma::fill::zeros);
		for(arma::uword i=0;i<num_sections;i++){
			// get number of cross sections in this section
			const arma::uword num_crss = R1234(i).n_cols;
			ell1234(i).set_size(4,num_crss);
			for(arma::uword j=0;j<4;j++){
				ell1234(i).row(j) = arma::join_horiz(arma::Row<fltp>::fixed<1>{0.0}, arma::cumsum(cmn::Extra::vec_norm(arma::diff(R1234(i).rows(j*3,(j+1)*3-1),1,1)))) + ell_shift(j);
				ell_shift(j) = ell1234(i).row(j).back();
			}
		}

		// create interpolation matrices
		arma::field<arma::Mat<fltp> > R1234r = R1234;
		arma::field<arma::Mat<fltp> > L1234r = L1234;
		arma::field<arma::Mat<fltp> > N1234r = N1234;
		arma::field<arma::Mat<fltp> > D1234r = D1234;
		arma::field<arma::Mat<fltp> > ell1234r = ell1234;
		for(arma::uword i=0;i<num_sections-1;i++){
			R1234r(i).shed_col(R1234r(i).n_cols-1); L1234r(i).shed_col(L1234r(i).n_cols-1);
			N1234r(i).shed_col(N1234r(i).n_cols-1); D1234r(i).shed_col(D1234r(i).n_cols-1);
			ell1234r(i).shed_col(ell1234r(i).n_cols-1);
		}
		arma::Mat<fltp> R1234m = cmn::Extra::field2mat(R1234r);
		arma::Mat<fltp> L1234m = cmn::Extra::field2mat(L1234r);
		arma::Mat<fltp> N1234m = cmn::Extra::field2mat(N1234r);
		arma::Mat<fltp> D1234m = cmn::Extra::field2mat(D1234r);
		arma::Mat<fltp> ell1234m = cmn::Extra::field2mat(ell1234r);

		// walk over sections
		for(arma::uword i=0;i<num_sections;i++){
			// get number of cross sections in this section
			const arma::uword num_crss = R1234(i).n_cols;

			// walk over cross sections
			for(arma::uword j=0;j<num_crss;j++){
				// average coordinates
				const arma::Mat<fltp>::fixed<3,4> Re = arma::reshape(R1234(i).col(j),3,4);
				const arma::Mat<fltp>::fixed<3,4> Le = arma::reshape(L1234(i).col(j),3,4);
				const arma::Col<fltp>::fixed<3> Rm = arma::mean(Re,1);
				const arma::Col<fltp>::fixed<3> Lm = arma::mean(Le,1);

				// project coordinates to plane
				arma::Col<fltp>::fixed<4> dell = cmn::Extra::dot(Re.each_col() - Rm, arma::join_horiz(Lm,Lm,Lm,Lm)).t();

				// update edge coordinates
				ell1234(i).col(j)-=dell;
			}
		}

		// interpolate
		for(arma::uword k=0;k<12;k++){
			const arma::Col<fltp> ellm = ell1234m.row(k/3).t();
			const arma::Col<fltp> Rm = R1234m.row(k).t(); const arma::Col<fltp> Lm = L1234m.row(k).t();
			const arma::Col<fltp> Nm = N1234m.row(k).t(); const arma::Col<fltp> Dm = D1234m.row(k).t();
			for(arma::uword i=0;i<num_sections;i++){
				// interpolate
				arma::Col<fltp> vv;
				cmn::Extra::interp1(ellm, Rm, ell1234(i).row(k/3).t(), vv, "linear", true);
				R1234(i).row(k) = vv.t();
				
				cmn::Extra::interp1(ellm, Lm, ell1234(i).row(k/3).t(), vv, "linear", true);
				L1234(i).row(k) = vv.t();

				cmn::Extra::interp1(ellm, Nm, ell1234(i).row(k/3).t(), vv, "linear", true);
				N1234(i).row(k) = vv.t();

				cmn::Extra::interp1(ellm, Dm, ell1234(i).row(k/3).t(), vv, "linear", true);
				D1234(i).row(k) = vv.t();
			}
		}

		// normalize
		for(arma::uword i=0;i<num_sections;i++){
			for(arma::uword j=0;j<4;j++){
				L1234(i).rows(j*3,(j+1)*3-1).each_row()/=cmn::Extra::vec_norm(L1234(i).rows(j*3,(j+1)*3-1));
				N1234(i).rows(j*3,(j+1)*3-1).each_row()/=cmn::Extra::vec_norm(N1234(i).rows(j*3,(j+1)*3-1));
				D1234(i).rows(j*3,(j+1)*3-1).each_row()/=cmn::Extra::vec_norm(D1234(i).rows(j*3,(j+1)*3-1));
			}
		}

	}

	// get current mesh that represents this coil
	std::list<ShMeshDataPr> ModelEdges::create_meshes(
		const std::list<arma::uword> &trace, 
		const MeshSettings &stngs) const{

		// trace
		if(!trace.empty())rat_throw_line("trace must be empty for leaf");

		// check enabled
		if(!get_enable())return{};

		// check input
		if(!is_valid(stngs.enable_throws))return{};

		// settings
		const bool cap_start = true;
		const bool cap_end = true;
		const bool is_reversed = false;

		// count number of input paths
		const arma::uword num_edges = input_paths_.size();
		assert(num_edges==4);

		// make edges
		arma::field<ShFramePr> frames(1,num_edges);
		for(arma::uword i=0;i<num_edges;i++){
			// get frame from path
			frames(i) = input_paths_.at(i+1)->create_frame(stngs);

			// split
			if(stngs.num_sub!=0)frames(i)->split(stngs.num_sub);

			// combine
			if(stngs.combine_sections)frames(i)->combine();
		}

		// get coordinate matrices
		arma::field<arma::Mat<fltp> > R1 = frames(0)->get_coords();
		arma::field<arma::Mat<fltp> > R2 = frames(1)->get_coords();
		arma::field<arma::Mat<fltp> > R3 = frames(2)->get_coords();
		arma::field<arma::Mat<fltp> > R4 = frames(3)->get_coords();

		// closed state
		const bool is_loop1 = frames(0)->is_loop();
		const arma::Row<arma::uword> close1 = frames(0)->is_closed();
		const arma::Row<arma::uword> close2 = frames(1)->is_closed();
		const arma::Row<arma::uword> close3 = frames(2)->is_closed();
		const arma::Row<arma::uword> close4 = frames(3)->is_closed();
		if(arma::any(close1!=close2))rat_throw_line("close state on path 2 does not agree with path 1");
		if(arma::any(close1!=close3))rat_throw_line("close state on path 3 does not agree with path 1");
		if(arma::any(close1!=close4))rat_throw_line("close state on path 4 does not agree with path 1");

		// check number of sections
		const arma::uword num_sections = R1.n_elem;
		if(num_sections!=R2.n_elem)rat_throw_line("paths must have equal number of sections: " + std::to_string(R2.n_elem) + "/" + std::to_string(num_sections));
		if(num_sections!=R3.n_elem)rat_throw_line("paths must have equal number of sections: " + std::to_string(R3.n_elem) + "/" + std::to_string(num_sections));
		if(num_sections!=R4.n_elem)rat_throw_line("paths must have equal number of sections: " + std::to_string(R4.n_elem) + "/" + std::to_string(num_sections));
		for(arma::uword i=0;i<num_sections;i++){
			if(R1(i).n_cols!=R2(i).n_cols)rat_throw_line("number of coordinates in path 1 and path 2 must be the same");
			if(R1(i).n_cols!=R3(i).n_cols)rat_throw_line("number of coordinates in path 1 and path 3 must be the same");
			if(R1(i).n_cols!=R4(i).n_cols)rat_throw_line("number of coordinates in path 1 and path 4 must be the same");
		}


		// get orientation matrices
		arma::field<arma::Mat<fltp> > L1 = frames(0)->get_direction(); 
		arma::field<arma::Mat<fltp> > L2 = frames(1)->get_direction();
		arma::field<arma::Mat<fltp> > L3 = frames(2)->get_direction(); 
		arma::field<arma::Mat<fltp> > L4 = frames(3)->get_direction();

		// get orientation matrices
		arma::field<arma::Mat<fltp> > N1 = frames(0)->get_normal(); 
		arma::field<arma::Mat<fltp> > N2 = frames(1)->get_normal();
		arma::field<arma::Mat<fltp> > N3 = frames(2)->get_normal(); 
		arma::field<arma::Mat<fltp> > N4 = frames(3)->get_normal();

		// get orientation matrices
		arma::field<arma::Mat<fltp> > D1 = frames(0)->get_transverse(); 
		arma::field<arma::Mat<fltp> > D2 = frames(1)->get_transverse();
		arma::field<arma::Mat<fltp> > D3 = frames(2)->get_transverse(); 
		arma::field<arma::Mat<fltp> > D4 = frames(3)->get_transverse();

		// walk over sections
		arma::Row<arma::uword> is_clockwise(R1.n_elem);
		for(arma::uword i=0;i<R1.n_elem;i++){
			// check if hexahedrons formed from edges are clockwise
			is_clockwise(i) = arma::as_scalar(cmn::Hexahedron::is_clockwise(arma::join_horiz(
				arma::join_horiz(R1(i).col(0),R2(i).col(0),R3(i).col(0),R4(i).col(0)),
				arma::join_horiz(R1(i).col(1),R2(i).col(1),R3(i).col(1),R4(i).col(1))),
				arma::regspace<arma::Col<arma::uword> >(0,7)));
			
			// invert order of edges
			if(!is_clockwise(i)){
				std::swap(R1(i),R4(i)); std::swap(R2(i),R3(i)); 
				std::swap(L1(i),L4(i)); std::swap(L2(i),L3(i));
				std::swap(N1(i),N4(i)); std::swap(N2(i),N3(i));
				std::swap(D1(i),D4(i)); std::swap(D2(i),D3(i)); 
			}
		}

		// derive vectors from the relative position of the paths
		// e4 -- e3  O--N
		// |     |   |
		// e1 -- e2  D
		if(!use_path_vectors_){
			// walk over sections
			for(arma::uword i=0;i<R1.n_elem;i++){
				// derive normal vectors from coordinates
				N1(i) = cmn::Extra::normalize(R2(i) - R1(i),true); N2(i) = cmn::Extra::normalize(R2(i) - R1(i),true);
				N3(i) = cmn::Extra::normalize(R3(i) - R4(i),true); N4(i) = cmn::Extra::normalize(R3(i) - R4(i),true);

				// derive transverse vectors from coordinates
				D1(i) = cmn::Extra::normalize(R1(i) - R4(i),true); D2(i) = cmn::Extra::normalize(R2(i) - R3(i),true);
				D3(i) = cmn::Extra::normalize(R2(i) - R3(i),true); D4(i) = cmn::Extra::normalize(R1(i) - R4(i),true);

				// derive longitudinal vectors from transverse and normal vectors
				L1(i) = cmn::Extra::normalize(cmn::Extra::cross(D1(i), N1(i))); 
				L2(i) = cmn::Extra::normalize(cmn::Extra::cross(D2(i), N2(i)));
				L3(i) = cmn::Extra::normalize(cmn::Extra::cross(D3(i), N3(i))); 
				L4(i) = cmn::Extra::normalize(cmn::Extra::cross(D4(i), N4(i)));

				// check vector lengths
				if(arma::any(cmn::Extra::vec_norm(N1(i))<1e-14))
					rat_throw_line("coinciding points cause normal vector to be zero");
				if(arma::any(cmn::Extra::vec_norm(N2(i))<1e-14))
					rat_throw_line("coinciding points cause normal vector to be zero");
				if(arma::any(cmn::Extra::vec_norm(N3(i))<1e-14))
					rat_throw_line("coinciding points cause normal vector to be zero");
				if(arma::any(cmn::Extra::vec_norm(N4(i))<1e-14))
					rat_throw_line("coinciding points cause normal vector to be zero");

				// check vector lengths
				if(arma::any(cmn::Extra::vec_norm(D1(i))<1e-14))
					rat_throw_line("coinciding points cause transverse vector to be zero");
				if(arma::any(cmn::Extra::vec_norm(D2(i))<1e-14))
					rat_throw_line("coinciding points cause transverse vector to be zero");
				if(arma::any(cmn::Extra::vec_norm(D3(i))<1e-14))
					rat_throw_line("coinciding points cause transverse vector to be zero");
				if(arma::any(cmn::Extra::vec_norm(D4(i))<1e-14))
					rat_throw_line("coinciding points cause transverse vector to be zero");
			}
		}

		// fix edges
		arma::field<arma::Mat<fltp> > R1234(1,num_sections);
		arma::field<arma::Mat<fltp> > L1234(1,num_sections);
		arma::field<arma::Mat<fltp> > N1234(1,num_sections);
		arma::field<arma::Mat<fltp> > D1234(1,num_sections);
		for(arma::uword i=0;i<num_sections;i++){
			// check number of nodes in each section
			const arma::uword num_crss = R1(i).n_cols;
			if(num_crss!=R2(i).n_cols)rat_throw_line("sections must have equal number of nodes: " + std::to_string(R2(i).n_cols) + "/" + std::to_string(num_crss));
			if(num_crss!=R3(i).n_cols)rat_throw_line("sections must have equal number of nodes: " + std::to_string(R3(i).n_cols) + "/" + std::to_string(num_crss));
			if(num_crss!=R4(i).n_cols)rat_throw_line("sections must have equal number of nodes: " + std::to_string(R4(i).n_cols) + "/" + std::to_string(num_crss));

			// combine
			R1234(i) = arma::join_vert(R1(i),R2(i),R3(i),R4(i));
			L1234(i) = arma::join_vert(L1(i),L2(i),L3(i),L4(i));
			N1234(i) = arma::join_vert(N1(i),N2(i),N3(i),N4(i));
			D1234(i) = arma::join_vert(D1(i),D2(i),D3(i),D4(i));
		}

		// edge refinement
		if(refine_edges_)refine_edges(R1234,L1234,N1234,D1234);

		// // check if the mesh is clockwise (at least the first element) and invert it if its not
		// const bool is_clockwise = arma::as_scalar(cmn::Hexahedron::is_clockwise(arma::reshape(R1234(0).cols(0,1),3,8),arma::regspace<arma::Col<arma::uword> >(0,7)));
		// if(!is_clockwise){
		// 	for(arma::uword i=0;i<3;i++){
		// 		const arma::Col<arma::uword> idx{i,i+3,i+6,i+9};
		// 		for(arma::uword j=0;j<R1234.n_elem;j++){
		// 			R1234(j).rows(idx) = arma::flipud(R1234(j).rows(idx));
		// 			L1234(j).rows(idx) = arma::flipud(L1234(j).rows(idx));
		// 			N1234(j).rows(idx) = arma::flipud(N1234(j).rows(idx));
		// 			D1234(j).rows(idx) = arma::flipud(D1234(j).rows(idx));
		// 		}
		// 	}
		// }

		// create grid in quadrilateral coordinates 
		const ShAreaPr crss_area = CrossRectangle::create(
			-RAT_CONST(1.0), RAT_CONST(1.0), -RAT_CONST(1.0), 
			RAT_CONST(1.0), RAT_CONST(2.0)/num_n_, RAT_CONST(2.0)/num_t_)->create_area(MeshSettings());

		// edge matrix construction
		const arma::Mat<arma::uword>& s2d = crss_area->get_perimeter();
		// const arma::Col<arma::uword>& i2d = crss_area->get_internal();	

		// get 2D mesh elements and node coords
		const arma::Mat<fltp>& R2d = crss_area->get_nodes();
		const arma::Mat<arma::uword>& n2d = crss_area->get_elements();

		// get counters
		const arma::uword num_nodes_2d = R2d.n_cols;
		const arma::uword num_element_2d = n2d.n_cols;
		const arma::uword num_nodes_per_element_2d = n2d.n_rows;
		const arma::uword num_surface_element_2d = s2d.n_cols;
		const arma::uword num_nodes_per_surface_element_2d = s2d.n_rows;

		// allocate 3D mesh
		arma::field<arma::Mat<fltp> > R(1,num_sections), L(1,num_sections), N(1,num_sections), D(1,num_sections), Rc(1,num_sections);
		arma::field<arma::Mat<arma::uword> > n(1,num_sections), s(1,num_sections);

		// allocate meshes
		std::list<ShMeshDataPr> meshes;

		// walk over sections
		// can be done in parfor loop?
		for(arma::uword i=0;i<num_sections;i++){
			// check number of nodes in each section
			const arma::uword num_cis = R1234(i).n_cols - 1 + int(!close1(i));
			const arma::uword num_cs = num_cis - 1;

			// allocate nodes
			R(i).set_size(3,R2d.n_cols*num_cis);
			L(i).set_size(3,R2d.n_cols*num_cis);
			N(i).set_size(3,R2d.n_cols*num_cis);
			D(i).set_size(3,R2d.n_cols*num_cis);
			n(i).set_size(2*num_nodes_per_element_2d,n2d.n_cols*num_cs);
			s(i).set_size(2*num_nodes_per_surface_element_2d,s2d.n_cols*num_cs);
			Rc(i).set_size(3,num_cis);

			// create 3D nodes
			for(arma::uword j=0;j<num_cis;j++){
				// get edge points
				const arma::Mat<fltp>::fixed<3,4> Re = arma::reshape(R1234(i).col(j), 3, 4);
				const arma::Mat<fltp>::fixed<3,4> Le = arma::reshape(L1234(i).col(j), 3, 4);
				const arma::Mat<fltp>::fixed<3,4> Ne = arma::reshape(N1234(i).col(j), 3, 4);
				const arma::Mat<fltp>::fixed<3,4> De = arma::reshape(D1234(i).col(j), 3, 4);

				// get start and end index
				const arma::uword idx1 = j*R2d.n_cols;
				const arma::uword idx2 = (j+1)*R2d.n_cols - 1;

				// create quadrilateral coordinates of the mesh
				R(i).cols(idx1, idx2) = cmn::Quadrilateral::quad2cart(Re, R2d);
				L(i).cols(idx1, idx2) = cmn::Quadrilateral::quad2cart(Le, R2d);
				N(i).cols(idx1, idx2) = cmn::Quadrilateral::quad2cart(Ne, R2d);
				D(i).cols(idx1, idx2) = cmn::Quadrilateral::quad2cart(De, R2d);

				// normalization
				L(i).cols(idx1, idx2).each_row()/=cmn::Extra::vec_norm(L(i).cols(idx1, idx2));
				N(i).cols(idx1, idx2).each_row()/=cmn::Extra::vec_norm(N(i).cols(idx1, idx2));
				D(i).cols(idx1, idx2).each_row()/=cmn::Extra::vec_norm(D(i).cols(idx1, idx2));

				// centerline
				Rc(i).col(j) = arma::mean(Re,1);
			}

			// create 3D elements
			for(arma::uword j=0;j<num_cs;j++){
				n(i).submat(0,j*num_element_2d,num_nodes_per_element_2d-1,(j+1)*num_element_2d-1) = n2d + j*num_nodes_2d;
				n(i).submat(num_nodes_per_element_2d,j*num_element_2d,2*num_nodes_per_element_2d-1,(j+1)*num_element_2d-1) = n2d + (j+1)*num_nodes_2d;
			}

			// create 3D surface elements
			for(arma::uword j=0;j<num_cs;j++){
				s(i).submat(0,j*num_surface_element_2d,num_nodes_per_surface_element_2d-1,(j+1)*num_surface_element_2d-1) = s2d + j*num_nodes_2d;
				s(i).submat(num_nodes_per_surface_element_2d,j*num_surface_element_2d,2*num_nodes_per_surface_element_2d-1,(j+1)*num_surface_element_2d-1) = s2d + (j+1)*num_nodes_2d;
			}

			// close last to first
			if(close1(i)){
				// total_num_cs++; total_num_cis++; 
				n(i) = arma::join_horiz(n(i), arma::join_vert(n2d + num_cs*num_nodes_2d, n2d));
				s(i) = arma::join_horiz(s(i), arma::join_vert(s2d + num_cs*num_nodes_2d, s2d));
			}

			// cap start and end for quadrilateral surface of hexahedron mesh
			if(!close1(i) && n2d.n_rows==4){
				// check if the 3D surface mesh also has 4 rows
				assert(s(i).n_rows==4);

				// get start and end cap meshes
				arma::Mat<arma::uword> ncap1 = n2d.rows(arma::Col<arma::uword>::fixed<4>{0,1,3,2});
				arma::Mat<arma::uword> ncap2 = n2d.rows(arma::Col<arma::uword>::fixed<4>{0,3,1,2});

				if(!is_reversed)ncap1.swap(ncap2);

				// what to cap
				if(cap_start && cap_end){
					s(i) = arma::join_horiz(ncap1,s(i),ncap2 + (num_cis-1)*num_nodes_2d);
				}else if(cap_start){
					s(i) = arma::join_horiz(ncap1,s(i));
				}else if(cap_end){
					s(i) = arma::join_horiz(s(i),ncap2 + (num_cis-1)*num_nodes_2d);
				}
			}

			// swap rows
			if(s(i).n_rows==4)s(i).swap_rows(2,3);
		}

		// keep track of node shift with the sections
		arma::uword node_offset = 0;
		for(arma::uword j=0;j<num_sections;j++){
			// apply node offset
			n(j) += node_offset;
			s(j) += node_offset;

			// offset nodes
			node_offset += R(j).n_cols;
		}


		// convert to mat
		const arma::Mat<fltp> Rm = cmn::Extra::field2mat(R);
		const arma::Mat<fltp> Lm = cmn::Extra::field2mat(L);
		const arma::Mat<fltp> Nm = cmn::Extra::field2mat(N);
		const arma::Mat<fltp> Dm = cmn::Extra::field2mat(D);
		const arma::Mat<arma::uword> nm = cmn::Extra::field2mat(n);
		const arma::Mat<arma::uword> sm = cmn::Extra::field2mat(s);

		// check 
		if(arma::any(cmn::Hexahedron::is_clockwise(Rm,nm)))
			rat_throw_line("elements created from edges are inverted");

		// check inverted mesh
		// const arma::Col<arma::uword> idx_inverted = arma::find(cmn::Hexahedron::is_clockwise(Rm,nm));
		// nm.cols(idx_inverted) = cmn::Hexahedron::invert_elements(nm.cols(idx_inverted));

		// check mesh
		assert(nm.max()<Rm.n_cols);
		assert(sm.max()<Rm.n_cols);

		// check mesh before
		if(!Rm.is_finite())rat_throw_line("mesh coordinates are not finite");
		if(!Lm.is_finite())rat_throw_line("mesh direction vectors are not finite");
		if(!Nm.is_finite())rat_throw_line("mesh normal vectors are not finite");
		if(!Dm.is_finite())rat_throw_line("mesh transverse vectors are not finite");

		// create mesh
		const ShCoilDataPr mesh = CoilData::create();
		mesh->set_part_name(get_name());
		mesh->set_name(get_name());

		// set mesh
		mesh->setup(Rm, nm, sm);
		mesh->set_center_line(Rc);

		// set orientation vectors
		mesh->set_longitudinal(Lm); 
		mesh->set_normal(Nm); 
		mesh->set_transverse(Dm);

		// get base mesh
		mesh->set_base_mesh(crss_area->get_elements());
		mesh->set_perimeter(crss_area->get_perimeter());
		mesh->set_edge_nodes(crss_area->get_corner_nodes());
		mesh->set_base_nodes(crss_area->get_nodes());
		mesh->set_base_area(crss_area->get_area());
		mesh->set_num_base_elements(num_element_2d);
		mesh->set_num_base_nodes(num_nodes_2d);
		mesh->set_num_base_perimeter(num_surface_element_2d);

		// frame data
		mesh->set_is_loop(is_loop1);
		const arma::Row<arma::uword> num_cis_frame = frames(0)->get_num_nodes();
		mesh->set_num_cis_frame(num_cis_frame);
		mesh->set_ell_frame(frames(0)->calc_ell());
		mesh->set_section(frames(0)->get_section());
		mesh->set_turn(frames(0)->get_turn());
		mesh->set_num_cis(arma::accu(num_cis_frame-close1));
		mesh->set_num_cs(nm.n_cols/num_element_2d);

		// mesh data
		mesh->set_num_cis_elements(arma::accu(num_cis_frame-close1)*crss_area->get_num_elements());
		mesh->set_num_cs_elements(nm.n_cols);
		mesh->set_is_closed(close1);

		// material properties
		mesh->set_material(conductor_);

		// color
		mesh->set_use_custom_color(use_custom_color_);
		mesh->set_color(color_);

		// raccoon settings
		mesh->set_enable_current_sharing(enable_current_sharing_);

		// operating conditions
		mesh->set_operating_temperature(operating_temperature_); // design temperature
		mesh->set_temperature(operating_temperature_); // at nodes
		mesh->set_operating_current(operating_current_);
		mesh->set_number_turns(num_turns_);
		mesh->set_circuit_index(circuit_index_);
		mesh->set_num_gauss_volume(num_gauss_);
		mesh->set_softening(softening_);

		// apply transformations to mesh
		mesh->apply_transformations(get_transformations(),stngs.time);

		// store in mesh list
		meshes.push_back(mesh);

		// return list of meshes
		return meshes;
	}


	// check validity
	bool ModelEdges::is_valid(const bool enable_throws) const{
		// check if cross and base are valid
		if(!Model::is_valid(enable_throws))return false;
		if(conductor_!=NULL)if(!conductor_->is_valid(enable_throws))return false;

		// check for absolute zero temperature
		if(operating_temperature_<=0){if(enable_throws){rat_throw_line("operating temperature must be larger than zero");} return false;};

		// check if four paths provided
		if(input_paths_.size()!=4){if(enable_throws){rat_throw_line("exactly four input paths are required");} return false;};
		for(auto it=input_paths_.begin();it!=input_paths_.end();it++){
			if((*it).second==NULL){if(enable_throws){rat_throw_line("input path points to NULL");} return false;};
			if(!(*it).second->is_valid(enable_throws))return false;
		}
		// no problems found
		return true;
	}

	// get type
	std::string ModelEdges::get_type(){
		return "rat::mdl::modeledges";
	}

	// method for serialization into json
	void ModelEdges::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		Model::serialize(js,list);
	
		// type
		js["type"] = get_type();

		// temperature and drive
		js["enable_current_sharing"] = enable_current_sharing_;
		js["operating_temperature"] = operating_temperature_;
		js["num_turns"] = num_turns_;
		js["operating_current"] = operating_current_;

		// connectivity for Raccoon
		js["circuit_index"] = static_cast<int>(circuit_index_);

		// elements
		js["num_t"] = static_cast<int>(num_t_);
		js["num_n"] = static_cast<int>(num_n_);

		// serialize the models
		for(auto it = input_paths_.begin();it!=input_paths_.end();it++)
			js["input_paths"].append(cmn::Node::serialize_node((*it).second, list));

		// edge fix
		js["refine_edges"] = refine_edges_;

		js["use_path_vectors"] = use_path_vectors_;
		js["num_gauss"] = static_cast<int>(num_gauss_);

		// softening
		js["softening"] = softening_;
	}

	// method for deserialisation from json
	void ModelEdges::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent objects
		Model::deserialize(js,list,factory_list,pth);

		// temperature and drive
		set_enable_current_sharing(js["enable_current_sharing"].asBool());
		set_operating_temperature(js["operating_temperature"].ASFLTP());
		set_num_turns(js["num_turns"].ASFLTP());
		set_operating_current(js["operating_current"].ASFLTP());
		if(js.isMember("num_gauss"))set_num_gauss(js["num_gauss"].asInt64());

		// connectivity for Raccoon
		set_circuit_index(js["circuit_index"].asInt64());

		// elements
		set_num_t(js["num_t"].asUInt64());
		set_num_n(js["num_n"].asUInt64());

		// deserialize the models
		if(js.isMember("input_paths")){
			for(auto it = js["input_paths"].begin();it!=js["input_paths"].end();it++)
				add_path(cmn::Node::deserialize_node<Path>(
					(*it), list, factory_list, pth));
		}

		// backwards compatibility v2.014.0
		else{
			add_path(cmn::Node::deserialize_node<Path>(js["path1"], list, factory_list, pth));
			add_path(cmn::Node::deserialize_node<Path>(js["path2"], list, factory_list, pth));
			add_path(cmn::Node::deserialize_node<Path>(js["path3"], list, factory_list, pth));
			add_path(cmn::Node::deserialize_node<Path>(js["path4"], list, factory_list, pth));
		}

		// refine
		refine_edges_ = js["refine_edges"].asBool();

		// path vector option
		if(js.isMember("use_path_vectors"))
			set_use_path_vectors(js["use_path_vectors"].asBool());

		// softening factor
		if(js.isMember("softening"))
			set_softening(js["softening"].ASFLTP());
	}

}}