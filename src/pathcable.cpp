// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathcable.hh"

// common headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// model headers
#include "pathbezier.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathCable::PathCable(){
		set_name("Cable");
	}

	// constructor with input of base path
	PathCable::PathCable(
		const ShPathPr &input_path, 
		const bool reverse_base, 
		const arma::uword num_turns, 
		const arma::uword num_add, 
		const fltp turn_step, 
		const arma::uword idx_start, 
		const arma::uword idx_incr,
		const bool disable_increment,
		const arma::uword num_offset,
		const fltp offset,
		const bool layer_wound,
		const bool flip_direction,
		const bool reorient) : PathCable(){

		// set to self
		set_input_path(input_path);
		set_reverse_base(reverse_base);
		set_num_turns(num_turns);
		set_num_add(num_add);
		set_turn_step(turn_step);
		set_idx_start(idx_start);
		set_idx_incr(idx_incr);
		set_disable_increment(disable_increment);
		set_num_offset(num_offset);
		set_offset(offset);
		set_layer_wound(layer_wound);
		set_flip_direction(flip_direction);
		set_reorient(reorient);
	}

	// factory methods
	ShPathCablePr PathCable::create(){
		return std::make_shared<PathCable>();
	}

	// factory with input of base path
	ShPathCablePr PathCable::create(
		const ShPathPr &input_path, 
		const bool reverse_base, 
		const arma::uword num_turns, 
		const arma::uword num_add, 
		const fltp turn_step, 
		const arma::uword idx_start, 
		const arma::uword idx_incr,
		const bool disable_increment,
		const arma::uword num_offset,
		const fltp offset,
		const bool layer_wound,
		const bool flip_direction,
		const bool reorient){
		return std::make_shared<PathCable>(
			input_path,reverse_base,num_turns,num_add,
			turn_step,idx_start,idx_incr,disable_increment,
			num_offset,offset,layer_wound,flip_direction,reorient);
	}

	// flip winding direction
	void PathCable::set_flip_direction(const bool flip_direction){
		flip_direction_ = flip_direction;
	}

	// reverse winding direction
	void PathCable::set_reverse_base(const bool reverse_base){
		reverse_base_ = reverse_base;
	}

	// flip N and D
	void PathCable::set_layer_wound(const bool layer_wound){
		layer_wound_ = layer_wound;
	}

	// set number of turns in coil block
	void PathCable::set_num_turns(const arma::uword num_turns){
		// if(num_turns<=0)rat_throw_line("number of turns must be larger than zero");
		num_turns_ = num_turns;
	}

	// set the thickness of the cable
	void PathCable::set_turn_step(const fltp turn_step){
		// if(turn_step<=0)rat_throw_line("turn step must be larger than zero");
		turn_step_ = turn_step;
	}

	// set the thickness of the insulation layer
	void PathCable::set_idx_start(const arma::uword idx_start){
		idx_start_ = idx_start;
	}

	// set number of sections to add
	void PathCable::set_num_add(const arma::sword num_add){
		num_add_ = num_add;
	}

	// set order for the increment smooothing
	void PathCable::set_incr_order(const arma::uword incr_order){
		incr_order_ = incr_order;
	}


	// set increment position
	void PathCable::set_idx_incr(const arma::uword idx_incr){
		idx_incr_ = idx_incr;
	}

	// set cable offset
	void PathCable::set_num_offset(const arma::sword num_offset){
		num_offset_ = num_offset;
	}

	// set cable offset
	void PathCable::set_offset(const fltp offset){
		offset_ = offset;
	}

	// allow for disabling increment
	void PathCable::set_disable_increment(
		const bool disable_increment){
		disable_increment_ = disable_increment;
	}

	void PathCable::set_reorient(const bool reorient){
		reorient_ = reorient;
	}



	// reverse winding direction
	bool PathCable::get_reverse_base() const{
		return reverse_base_;
	}

	// get flip orientation
	bool PathCable::get_layer_wound() const{
		return layer_wound_;
	}

	// get number of turns in coil block
	arma::uword PathCable::get_num_turns() const{
		return num_turns_;
	}

	// get the thickness of the cable
	fltp PathCable::get_turn_step() const{
		return turn_step_;
	}

	// get the thickness of the insulation layer
	arma::uword PathCable::get_idx_start() const{
		return idx_start_;
	}

	// get number of sections to add
	arma::sword PathCable::get_num_add() const{
		return num_add_;
	}

	// get increment position
	arma::uword PathCable::get_idx_incr() const{
		return idx_incr_;
	}

	// get the order of the increment bspline smoothing
	arma::uword PathCable::get_incr_order() const{
		return incr_order_;
	}

	// get cable offset
	arma::sword PathCable::get_num_offset() const{
		return num_offset_;
	}

	// get cable offset
	fltp PathCable::get_offset() const{
		return offset_;
	}

	// allow for disabling increment
	bool PathCable::get_disable_increment() const{
		return disable_increment_;
	}

	bool PathCable::get_reorient()const{
		return reorient_;
	}

	bool PathCable::get_flip_direction()const{
		return flip_direction_;
	}


	// update function
	ShFramePr PathCable::create_frame(const MeshSettings &stngs) const{
		// check input
		is_valid(true);

		// create frame
		const ShFramePr frame = input_path_->create_frame(stngs);

		// perform flip
		if(layer_wound_){
			frame->flip_orientation();
		}

		if(flip_direction_){
			frame->flip();
		}

		// reverse frame if requested
		if(reverse_base_==true)frame->reverse();

		// number of sections in base
		const arma::uword num_base_sections = frame->get_num_sections();

		// check if this frame is a loop
		// const bool is_loop = frame->is_loop();

		// std::cout<<num_base_sections<<std::endl;
		// std::cout<<num_turns_<<std::endl;
		// std::cout<<num_add_<<std::endl;
		// std::cout<<(arma::sword(num_base_sections*num_turns_) + num_add_)<<std::endl;
		// check number of sections
		if((arma::sword(num_base_sections*num_turns_) + num_add_)<=0){
			rat_throw_line("can not remove all sections");
		}

		// calculate number of sections in cable
		const arma::uword num_cable_sections = num_base_sections*num_turns_ + num_add_;

		// check user input
		// if(idx_incr_>=num_base_sections)rat_throw_line("increment index exceeds number of base sections");
		
		// allocate cable coordinate system
		arma::field<arma::Mat<fltp> > R(1,num_cable_sections); 
		arma::field<arma::Mat<fltp> > L(1,num_cable_sections);
		arma::field<arma::Mat<fltp> > D(1,num_cable_sections); 
		arma::field<arma::Mat<fltp> > N(1,num_cable_sections);
		arma::field<arma::Mat<fltp> > B(1,num_cable_sections);

		// allocate section and turn indexes
		arma::Row<arma::uword> section_idx(num_cable_sections);
		arma::Row<arma::uword> turn_idx(num_cable_sections);

		// shift idx start
		const arma::uword idx_start = idx_start_%num_base_sections;

		// calculate increment position
		// unfortunately we must use sword here to allow negative
		arma::sword incr_real = static_cast<arma::sword>(idx_incr_) - static_cast<arma::sword>(idx_start);
		if(incr_real<0)incr_real = num_base_sections + incr_real;
		arma::uword idx_incr_real = static_cast<arma::uword>(incr_real);

		// walk over number of turns
		for(arma::uword i=0;i<(num_turns_ + std::ceil(static_cast<fltp>(num_add_)/num_base_sections));i++){
			// number of sections in this turn
			arma::uword num_sect;
			if((num_cable_sections-i*num_base_sections)<num_base_sections)
				num_sect = num_cable_sections-i*num_base_sections;
			else num_sect = num_base_sections;

			// walk over sections
			for(arma::uword j=0;j<num_sect;j++){
				// sector index
				const arma::uword isect = i*num_base_sections + j;

				// index into base
				const arma::uword ibase = (j+idx_start)%num_base_sections;

				// number of nodes in this section
				const arma::uword Nsn = frame->get_num_nodes(ibase);
				if(Nsn<=1)rat_throw_line("need at least two nodes in section");

				// num_nodes_(isect) = Nsn;

				// calculate position of non-incremented turn
				const fltp pn = (static_cast<arma::sword>(i)+num_offset_)*turn_step_ + offset_;

				// check for turn increment
				arma::Row<fltp> nshift(Nsn,arma::fill::value(pn));
				if(disable_increment_==false){
					if(j==idx_incr_real){
						// // linear interpolation
						// nshift = arma::linspace<arma::Row<fltp> >(pn, pn + turn_step_, Nsn);

						// cosine interpolation
						// const arma::Row<fltp> ell = arma::join_horiz(arma::Row<fltp>{RAT_CONST(0.0)}, arma::cumsum(cmn::Extra::vec_norm(arma::diff(frame->get_coords(j),1,1))));
						// nshift = pn + turn_step_*(RAT_CONST(0.5) - arma::cos(arma::Datum<fltp>::pi*ell/ell.back())/2);

						// bspline
						if(incr_order_>0){
							const arma::Row<fltp> ell = arma::join_horiz(arma::Row<fltp>{
								RAT_CONST(0.0)}, arma::cumsum(cmn::Extra::vec_norm(arma::diff(frame->get_coords(ibase),1,1))));
							const arma::field<arma::Mat<fltp> > C = PathBezier::create_bezier_tn(ell/ell.back(), 
								arma::join_horiz(arma::Row<fltp>(incr_order_,arma::fill::value(pn)),
								arma::Row<fltp>(incr_order_,arma::fill::value(pn+turn_step_))), 1);
							nshift = std::move(C(0));
						}

					}
					else if(j>idx_incr_real)nshift.fill(pn + turn_step_);
					else nshift.fill(pn);
				}

				// check
				assert(nshift.n_elem==Nsn);

				// generate coordinates by shifting in the normal direction
				//R(isect) = Rb(ibase) + Nb(ibase).each_row()%dN;
				R(isect) = frame->perform_normal_shift(ibase, nshift);

				// copy radial vector
				D(isect) = frame->get_transverse(ibase);

				// copy normal vector (do not use cross here
				// otherwise cables are not touching)
				N(isect) = frame->get_normal(ibase);

				// get block direction vector
				B(isect) = frame->get_block(ibase);

				// get longitudinal vector
				L(isect) = frame->get_direction(ibase);

				// set index
				section_idx(isect) = ibase;
				turn_idx(isect) = i;
			}


			// walk over sections
			for(arma::uword j=0;j<num_sect;j++){
				// sector index
				const arma::uword isect = i*num_base_sections + j;

				// // index into base
				// const arma::uword ibase = (j+idx_start)%num_base_sections;

				// number of nodes in this section
				// const arma::uword Nsn = frame->get_num_nodes(ibase);
				if(reorient_){
					// derivative
					L(isect) = cmn::Extra::normalize(PathBezier::diff(R(isect),2,2)(1));

					// // remove normal component at start and end point
					// L(isect).head_cols(1) -= N(isect).head_cols(1)*arma::as_scalar(cmn::Extra::dot(L(isect).head_cols(1),N(isect).head_cols(1)));
					// L(isect).tail_cols(1) -= N(isect).tail_cols(1)*arma::as_scalar(cmn::Extra::dot(L(isect).tail_cols(1),N(isect).tail_cols(1)));
					// L(isect) = cmn::Extra::normalize(L(isect));

					// update normal vector
					N(isect) = cmn::Extra::normalize(cmn::Extra::cross(L(isect),D(isect)));
				}

				// after off-setting the darboux ribbon is not even fixed width anymore
				// this cheat fixes the width at least
				D(isect) = cmn::Extra::normalize(D(isect)).each_row()/
					arma::cos(cmn::Extra::vec_angle(cmn::Extra::cross(D(isect),N(isect)),L(isect)));

				// check handedness
				// assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N(isect),L(isect)),D(isect))>RAT_CONST(0.0)));
			}
		}

		// reverse frame
		// if(reverse_base_==true){
		// 	Frame::reverse_field(R); Frame::reverse_field(L);
		// 	Frame::reverse_field(N); Frame::reverse_field(D);
		// 	for(arma::uword i=0;i<L.n_elem;i++)L(i) *= -1;
		// }

		// create frame
		const ShFramePr cable_frame = Frame::create(R,L,N,D,B);

		// set cable flag
		cable_frame->set_conductor_type(Frame::DbType::cable);

		// override location
		cable_frame->set_location(section_idx, turn_idx, num_base_sections);

		// flip direction back
		// if(flip_direction_)cable_frame->flip();

		// swap N and D
		if(layer_wound_)cable_frame->flip_orientation(true);

		// reverse frame
		if(reverse_base_)cable_frame->reverse();

		// flip direction of N and D
		if(reverse_base_ && layer_wound_)cable_frame->flip();

		// apply transformations
		cable_frame->apply_transformations(get_transformations(), stngs.time);

		// create frame and return
		return cable_frame;
	}

	// re-index nodes after deleting
	void PathCable::reindex(){
		InputPath::reindex(); Transformations::reindex();
	}

	// vallidity check
	bool PathCable::is_valid(const bool enable_throws) const{
		if(!InputPath::is_valid(enable_throws))return false;
		if(!Transformations::is_valid(enable_throws))return false;
		if(num_turns_<=0){if(enable_throws){rat_throw_line("number of turns must be larger than zero");} return false;};
		if(turn_step_<0){if(enable_throws){rat_throw_line("turn step must be larger than zero");} return false;};
		return true;
	}


	// get type
	std::string PathCable::get_type(){
		return "rat::mdl::pathcable";
	}

	// method for serialization into json
	void PathCable::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents first 
		// this allows the type to be overridden
		Path::serialize(js,list);
		InputPath::serialize(js,list);
		Transformations::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["layer_wound_new"] = layer_wound_;
		js["num_turns"] = static_cast<unsigned int>(num_turns_);
		js["turn_step"] = turn_step_;
		js["idx_start"] = static_cast<unsigned int>(idx_start_);
		js["idx_incr"] = static_cast<unsigned int>(idx_incr_);
		js["num_add"] = static_cast<int>(num_add_);
		js["incr_order"] = static_cast<int>(incr_order_);
		js["num_offset"] = static_cast<int>(num_offset_);
		js["offset"] = offset_;
		js["disable_increment"] = disable_increment_;
		js["reverse_base"] = reverse_base_;
		js["reorient"] = reorient_;
		js["flip_direction"] = flip_direction_;
	}

	// method for deserialisation from json
	void PathCable::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent objects
		Path::deserialize(js,list,factory_list,pth);
		InputPath::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);

		// properties
		// backwards compatibility version 2.017.0
		if(js.isMember("layer_wound_new")){
			set_layer_wound(js["layer_wound_new"].asBool()); 
		}else{
			set_layer_wound(js["layer_wound"].asBool());
			if(get_layer_wound())set_flip_direction(true);
		}
		set_num_turns(js["num_turns"].asUInt64());
		set_turn_step(js["turn_step"].ASFLTP());
		set_idx_start(js["idx_start"].asUInt64());
		set_idx_incr(js["idx_incr"].asUInt64());
		set_num_add(js["num_add"].asInt64());
		set_num_offset(js["num_offset"].asInt64());
		set_offset(js["offset"].ASFLTP());
		if(js.isMember("incr_order"))
			set_incr_order(js["incr_order"].asUInt64());
		set_disable_increment(js["disable_increment"].asBool());
		set_reverse_base(js["reverse_base"].asBool());
		set_reorient(js["reorient"].asBool());
		if(js.isMember("flip_direction"))
			set_flip_direction(js["flip_direction"].asBool());
	}

}}
