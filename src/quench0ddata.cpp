// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "quench0ddata.hh"

#include "vtktable.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	Quench0DData::Quench0DData(
		const arma::uword quenching_id,
		const arma::Row<arma::uword>& circuit_id,
		const arma::field<std::string>& coil_names,
		const arma::field<std::string>& circuit_names,
		const arma::Col<fltp>&time, 
		const arma::Row<fltp>&hotspot_position,
		const arma::Mat<fltp>&hotspot_temperature, 
		const arma::Mat<fltp> &coil_temperatures, 
		const arma::Mat<fltp> &circuit_currents,
		const arma::Mat<fltp> &circuit_voltage,
		const fltp treshold_time,
		const fltp detection_time,
		const arma::Row<fltp> &switching_times,
		const arma::Row<fltp> &heater_times,
		const fltp hotspot_flux_density,
		const fltp hotspot_alpha,
		const fltp hotspot_start_temperature,
		const fltp hotspot_start_current_density,
		const fltp normal_zone_propagation_velocity,
		const fltp minimal_propagation_zone_length,
		const fltp minimal_quench_energy,
		const arma::Row<arma::uword>& heatered,
		const arma::Row<arma::uword>& is_sc){

		// output type for vtk files
		set_output_type("quench");

		// system info
		set_name(coil_names(quenching_id));
		coil_names_ = coil_names;
		circuit_names_ = circuit_names;
		circuit_id_ = circuit_id;
		quenching_id_ = quenching_id;
		heatered_ = heatered;
		is_sc_ = is_sc;

		// data tables
		time_ = time;
		hotspot_position_ = hotspot_position;
		hotspot_temperature_ = hotspot_temperature;
		coil_temperatures_ = coil_temperatures;
		circuit_currents_ = circuit_currents;
		circuit_voltage_ = circuit_voltage;

		// event times
		treshold_time_ = treshold_time;
		detection_time_ = detection_time;
		switching_times_ = switching_times;
		heater_times_ = heater_times;

		// hotspot data
		hotspot_flux_density_ = hotspot_flux_density;
		hotspot_alpha_ = hotspot_alpha;
		hotspot_start_temperature_ = hotspot_start_temperature;
		hotspot_start_current_density_ = hotspot_start_current_density;
		normal_zone_propagation_velocity_ = normal_zone_propagation_velocity;
		minimal_propagation_zone_length_ = minimal_propagation_zone_length;
		minimal_quench_energy_ = minimal_quench_energy;
	}

	// factory
	ShQuench0DDataPr Quench0DData::create(
		const arma::uword quenching_id,
		const arma::Row<arma::uword>& circuit_id,
		const arma::field<std::string>& coil_names,
		const arma::field<std::string>& circuit_names,
		const arma::Col<fltp>&time, 
		const arma::Row<fltp>&hotspot_position,
		const arma::Mat<fltp>&hotspot_temperature, 
		const arma::Mat<fltp> &coil_temperatures, 
		const arma::Mat<fltp> &circuit_currents,
		const arma::Mat<fltp> &circuit_voltage,
		const fltp treshold_time,
		const fltp detection_time,
		const arma::Row<fltp> &switching_times,
		const arma::Row<fltp> &heater_times,
		const fltp hotspot_flux_density,
		const fltp hotspot_alpha,
		const fltp hotspot_start_temperature,
		const fltp hotspot_start_current_density,
		const fltp normal_zone_propagation_velocity,
		const fltp minimal_propagation_zone_length,
		const fltp minimal_quench_energy,
		const arma::Row<arma::uword>& heatered,
		const arma::Row<arma::uword>& is_sc){

		return std::make_shared<Quench0DData>(
			quenching_id,circuit_id,coil_names,circuit_names,
			time,hotspot_position,hotspot_temperature,coil_temperatures,circuit_currents,circuit_voltage,
			treshold_time,detection_time,switching_times,heater_times,hotspot_flux_density,
			hotspot_alpha,hotspot_start_temperature,hotspot_start_current_density,
			normal_zone_propagation_velocity,minimal_propagation_zone_length,
			minimal_quench_energy,heatered,is_sc);
	}

	// getters
	arma::uword Quench0DData::get_quenching_id()const{
		return quenching_id_;
	}

	const arma::Row<arma::uword>& Quench0DData::get_circuit_id()const{
		return circuit_id_;
	}

	const arma::field<std::string>& Quench0DData::get_coil_names()const{
		return coil_names_;
	}

	const arma::field<std::string>& Quench0DData::get_circuit_names()const{
		return circuit_names_;
	}

	const arma::Col<fltp>& Quench0DData::get_time()const{
		return time_;
	}

	const arma::Row<fltp>& Quench0DData::get_hotspot_position()const{
		return hotspot_position_;
	}

	const arma::Mat<fltp>& Quench0DData::get_hotspot_temperature()const{
		return hotspot_temperature_;
	}

	const arma::Mat<fltp>& Quench0DData::get_coil_temperatures()const{
		return coil_temperatures_;
	}

	const arma::Mat<fltp>& Quench0DData::get_circuit_currents()const{
		return circuit_currents_;
	}

	const arma::Mat<fltp>& Quench0DData::get_circuit_voltage()const{
		return circuit_voltage_;
	}


	fltp Quench0DData::get_treshold_time()const{
		return treshold_time_;
	}

	fltp Quench0DData::get_detection_time()const{
		return detection_time_;
	}

	const arma::Row<fltp>& Quench0DData::get_switching_times()const{
		return switching_times_;
	}

	const arma::Row<fltp>& Quench0DData::get_heater_times()const{
		return heater_times_;
	}

	fltp Quench0DData::get_hotspot_flux_density()const{
		return hotspot_flux_density_;
	}

	fltp Quench0DData::get_hotspot_alpha()const{
		return hotspot_alpha_;
	}

	fltp Quench0DData::get_normal_zone_propagation_velocity()const{
		return normal_zone_propagation_velocity_;
	}

	const arma::Row<arma::uword>& Quench0DData::get_heatered()const{
		return heatered_;
	}

	const arma::Row<arma::uword>& Quench0DData::get_is_sc()const{
		return is_sc_;
	}

	fltp Quench0DData::get_hotspot_start_temperature()const{
		return hotspot_start_temperature_;
	}

	fltp Quench0DData::get_hotspot_start_current_density()const{
		return hotspot_start_current_density_;
	}

	fltp Quench0DData::get_minimal_propagation_zone_length()const{
		return minimal_propagation_zone_length_;
	}

	fltp Quench0DData::get_minimal_quench_energy()const{
		return minimal_quench_energy_;
	}


	// calculate MIITS
	fltp Quench0DData::estimate_quench_integral()const{
		const arma::Col<fltp> current = circuit_currents_.col(circuit_id_(quenching_id_));
		return arma::as_scalar(arma::trapz(time_, current*current));
	}

	// export to vtk
	ShVTKObjPr Quench0DData::export_vtk() const{
		// create table
		const ShVTKTablePr table = VTKTable::create(time_.n_elem);

		// add datasets
		table->set_data(time_,"time");
		table->set_data(hotspot_temperature_,"hotspot_temperature");
		for(arma::uword i=0;i<coil_temperatures_.n_cols;i++)
			table->set_data(coil_temperatures_.col(i),coil_names_(i) + "_temperature");
		for(arma::uword i=0;i<circuit_currents_.n_cols;i++)
			table->set_data(circuit_currents_.col(i),circuit_names_(i) + "_current");
		for(arma::uword i=0;i<circuit_voltage_.n_cols;i++)
			table->set_data(circuit_voltage_.col(i),circuit_names_(i) + "_voltage");

		// return table object
		return table;
	}

	void Quench0DData::export_csv(const boost::filesystem::path& fpath) const{
		// create output stream
		std::ofstream fid(fpath.string(), std::ios::binary);

		// write header
		// set precision
		fid<<std::fixed;
		fid<<std::setprecision(16);

		// write header
		fid<<"time [s], ";
		for(arma::uword i=0;i<coil_temperatures_.n_cols;i++)
			fid<<coil_names_(i)<<"_temperature, ";
		for(arma::uword i=0;i<circuit_currents_.n_cols;i++)
			fid<<circuit_names_(i)<<"_current, ";
		for(arma::uword i=0;i<circuit_voltage_.n_cols;i++){
			fid<<circuit_names_(i)<<"_voltage, ";
		}
		fid<<"hotspot_temperature";
		fid<<"\n";

		// walk over time steps
		for(arma::uword i=0;i<time_.n_elem;i++){
			fid<<time_(i)<<", ";
			for(arma::uword j=0;j<coil_temperatures_.n_cols;j++)
				fid<<coil_temperatures_(i,j)<<", ";
			for(arma::uword j=0;j<circuit_currents_.n_cols;j++)
				fid<<circuit_currents_(i,j)<<", ";
			for(arma::uword j=0;j<circuit_voltage_.n_cols;j++){
				fid<<circuit_voltage_(i,j)<<", ";
			}
			fid<<hotspot_temperature_(i);
			fid<<"\n";
		}

		// close the file
		fid.close();
	}

	// export latex tikz figure
	void Quench0DData::export_tikz(const boost::filesystem::path& fpath) const{
		// settings
		const rat::fltp plot_width = 14.0; // [cm]
		const rat::fltp plot_height = 10.0; // [cm]
		const arma::uword max_separate_coil_temperatures = 4llu;

		// create output stream
		std::ofstream fid(fpath.string(), std::ios::binary);

		// set precision
		fid<<std::fixed;
		fid<<std::setprecision(7);

		arma::uword color_idx=0;
		const arma::field<std::string> colors{"lime","olive","orange","cyan","blue","purple","teal","violet","red"};

		fid<<"\\documentclass[tikz]{standalone}\n";

		fid<<"\\usepackage{tikz}\n";
		fid<<"\\usepackage{pgfplots}\n";
		fid<<"\\pgfplotsset{compat=newest}\n"; // Ensure compatibility
		// fid<<"\\usepackage[dvipsnames]{xcolor}\n";

		fid<<"\\begin{document}\n";

		// header
		fid<<"\\begin{tikzpicture}\n";

		// footnotesize
		fid<<"\\footnotesize\n";

		// create the first axis
		fid<<"\t% First Y-axis\n";
		fid<<"\t\\begin{axis}[\n";
		fid<<"\t\twidth="<<plot_width<<"cm,\n";
    	fid<<"\t\theight="<<plot_height<<"cm,\n";
		fid<<"\t\txmin="<<time_.front()<<",\n"; 
		fid<<"\t\txmax="<<time_.back()<<",\n";
		fid<<"\t\txlabel={Time [s]},\n";
		fid<<"\t\tgrid=major,\n";
		fid<<"\t\tylabel={Circuit Current [A]},\n";
		fid<<"\t\taxis y line*=left,\n";
		// fid<<"\t\taxis x line*=bottom\n";
		// fid<<"\t\taxis x line*=top\n";
		fid<<"\t]\n";
		
		// plot circuit current vs time
		for(arma::uword j=0;j<circuit_currents_.n_cols;j++){
			// plot current for this circuit
			fid<<"\t\\addplot["<<colors((color_idx++)%colors.n_elem)<<", line width=1] coordinates {\n";
			for(arma::uword i=0;i<time_.n_elem;i++)
				fid<<"\t\t("<<time_(i)<<","<<circuit_currents_(i,j)<<")\n";
			fid<<"\t};\n";

			// legend entry
			std::string circuit_name = circuit_names_(j);
			std::replace(circuit_name.begin(), circuit_name.end(), '_', '-');
			fid<<"\t\\label{plot::current::"<<circuit_name<<"}\n";
		}

		// axis done
		fid<<"\t\\end{axis}\n";
		   
		// create the second axis
		fid<<"\t% Second Y-axis\n";
		fid<<"\t\\begin{axis}[\n";
		fid<<"\t\twidth="<<plot_width<<"cm,\n";
    	fid<<"\t\theight="<<plot_height<<"cm,\n";
		fid<<"\t\tylabel={Coil Temperature [K]},\n";
		fid<<"\t\taxis y line*=right,\n";
		fid<<"\t\taxis x line=none,\n";
		fid<<"\t\txmin="<<time_.front()<<",\n"; 
		fid<<"\t\txmax="<<time_.back()<<",\n";
		fid<<"\t\tylabel near ticks,\n";
		fid<<"\t\tyticklabel pos=right\n";
		fid<<"\t]\n";

		// plot coil temperatures
		if(coil_temperatures_.n_cols>max_separate_coil_temperatures){
			const arma::Col<fltp> max_temperature = arma::max(coil_temperatures_,1);
			// plot current for this circuit
			fid<<"\t\\addplot["<<colors((color_idx++)%colors.n_elem)<<", line width=1] coordinates {\n";
			for(arma::uword i=0;i<time_.n_elem;i++)
				fid<<"\t\t("<<time_(i)<<","<<max_temperature(i)<<")\n";
			fid<<"\t};\n";

			// legend entry
			fid<<"\t\\label{plot::temp::coil_max}\n";
		}
		else{
			for(arma::uword j=0;j<coil_temperatures_.n_cols;j++){
				// plot current for this circuit
				fid<<"\t\\addplot["<<colors((color_idx++)%colors.n_elem)<<", line width=1] coordinates {\n";
				for(arma::uword i=0;i<time_.n_elem;i++)
					fid<<"\t\t("<<time_(i)<<","<<coil_temperatures_(i,j)<<")\n";
				fid<<"\t};\n";

				// legend entry
				std::string coil_name = coil_names_(j);
				std::replace(coil_name.begin(), coil_name.end(), '_', '-');
				fid<<"\t\\label{plot::temp::"<<coil_name<<"}\n";
			}
		}

		// hotspot temperature
		fid<<"\t\\addplot["<<colors((color_idx++)%colors.n_elem)<<", line width=1] coordinates {\n";
		for(arma::uword i=0;i<time_.n_elem;i++)
			fid<<"\t\t("<<time_(i)<<","<<hotspot_temperature_(i)<<")\n";
		fid<<"\t};\n";
		fid<<"\t\\label{plot::temp::hotspot}\n";

		// axis done
		fid<<"\t\\end{axis}\n";
		
		// create the third axis
		fid<<"\t% Third Y-axis\n";
		fid<<"\t\\begin{axis}[\n";
		fid<<"\t\twidth="<<plot_width<<"cm,\n";
    	fid<<"\t\theight="<<plot_height<<"cm,\n";
		fid<<"\t\tylabel={Circuit Voltage [V]},\n";
		fid<<"\t\ttick style={xshift=5em},\n";
		fid<<"\t\taxis y line*=right,\n";
		fid<<"\t\taxis x line=none,\n";
		fid<<"\t\tylabel near ticks,\n";
		fid<<"\t\tyticklabel pos=right,\n";
		fid<<"\t\txmin="<<time_.front()<<",\n"; 
		fid<<"\t\txmax="<<time_.back()<<",\n";
		fid<<"\t\tlegend cell align={left},\n";
		fid<<"\t\tyticklabel style={xshift=5em},\n";
		fid<<"\t\tafter end axis/.code={\\draw ([xshift=5em]rel axis cs:1,0) -- ([xshift=5em]rel axis cs:1,1);}\n";
		fid<<"\t]\n";

		// plot circuit current vs time
		for(arma::uword j=0;j<circuit_voltage_.n_cols;j++){
			// plot current for this circuit
			fid<<"\t\\addplot["<<colors((color_idx++)%colors.n_elem)<<", line width=1] coordinates {\n";
			for(arma::uword i=0;i<time_.n_elem;i++)
				fid<<"\t\t("<<time_(i)<<","<<circuit_voltage_(i,j)<<")\n";
			fid<<"\t};\n";

			// legend entry
			std::string circuit_name = circuit_names_(j);
			std::replace(circuit_name.begin(), circuit_name.end(), '_', '-');
			fid<<"\t\\addlegendentry{"<<circuit_name<<" Voltage}\n";
		}

		// legend entries
		for(arma::uword j=0;j<circuit_currents_.n_cols;j++){
			std::string circuit_name = circuit_names_(j);
			std::replace(circuit_name.begin(), circuit_name.end(), '_', '-');
			fid<<"\t\\addlegendimage{/pgfplots/refstyle=plot::current::"<<circuit_name<<"}\\addlegendentry{"<<circuit_name<<" Current}\n";
		}
		if(coil_temperatures_.n_cols>max_separate_coil_temperatures){
			fid<<"\t\\addlegendimage{/pgfplots/refstyle=plot::temp::coil_max}\\addlegendentry{Max Coil Temperature}\n";
		}else{
			for(arma::uword j=0;j<coil_temperatures_.n_cols;j++){
				std::string coil_name = coil_names_(j);
				std::replace(coil_name.begin(), coil_name.end(), '_', '-');
				fid<<"\t\\addlegendimage{/pgfplots/refstyle=plot::temp::"<<coil_name<<"}\\addlegendentry{"<<coil_name<<" Temperature}\n";
			}
		}
		fid<<"\t\\addlegendimage{/pgfplots/refstyle=plot::temp::hotspot}\\addlegendentry{Hotspot Temperature}\n";

		// axis done
		fid<<"\t\\end{axis}\n";

		fid<<"\\end{tikzpicture}\n";

		fid<<"\\end{document}\n";


		// close the file
		fid.close();
	}


}}


