// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "lengthdata.hh"
#include "vtktable.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	LengthData::LengthData(
		const arma::field<std::string>& coil_names,
		const arma::Row<fltp>& volume,
		const arma::Row<fltp>& num_turns,
		const arma::Row<fltp>& ell_gen,
		const arma::Row<fltp>& ell){
		coil_names_ = coil_names;
		volume_ = volume;
		num_turns_ = num_turns;
		ell_gen_ = ell_gen;
		ell_ = ell;
	}

	// factory
	ShLengthDataPr LengthData::create(
		const arma::field<std::string>& coil_names,
		const arma::Row<fltp>& volume,
		const arma::Row<fltp>& num_turns,
		const arma::Row<fltp>& ell_gen,
		const arma::Row<fltp>& ell){
		return std::make_shared<LengthData>(coil_names, volume, num_turns, ell_gen, ell);
	}

	// get names
	const arma::field<std::string>& LengthData::get_coil_names() const{
		return coil_names_;
	}

	// get volumes
	const arma::Row<fltp>& LengthData::get_volume() const{
		return volume_;
	}

	// get number of turns
	const arma::Row<fltp>& LengthData::get_num_turns() const{
		return num_turns_;
	}

	// get length of the frame
	const arma::Row<fltp>& LengthData::get_ell_gen() const{
		return ell_gen_;
	}

	// get calculated lenbgths
	const arma::Row<fltp>& LengthData::get_ell() const{
		return ell_;
	}

	// get number of coils
	arma::uword LengthData::get_num_coils() const{
		return coil_names_.n_elem;
	}


	// get names
	std::string LengthData::get_coil_name(const arma::uword index) const{
		return coil_names_(index);
	}

	// get volumes
	fltp LengthData::get_volume(const arma::uword index) const{
		return volume_(index);
	}

	// get number of turns
	fltp LengthData::get_num_turns(const arma::uword index) const{
		return num_turns_(index);
	}

	// get length of the frame
	fltp LengthData::get_ell_gen(const arma::uword index) const{
		return ell_gen_(index);
	}

	// get calculated lenbgths
	fltp LengthData::get_ell(const arma::uword index) const{
		return ell_(index);
	}


	// export vtk
	ShVTKObjPr LengthData::export_vtk() const{
		ShVTKTablePr vtk = VTKTable::create(coil_names_.n_elem);
		vtk->set_data(coil_names_,"name");
		vtk->set_data(num_turns_,"num_turns [#]");
		vtk->set_data(volume_,"volume [m^3]");
		vtk->set_data(ell_gen_,"lframe [m]");
		vtk->set_data(ell_,"lcable [m]");
		return vtk;
	}

	// write output files
	void LengthData::display(cmn::ShLogPr lg){
		
		// header
		lg->msg(2,"%s%sCALC LENGTH%s\n",KBLD,KGRN,KNRM);
		
		// walk over time steps
		lg->msg(2,"%sconductor length table%s\n",KBLU,KNRM);
		lg->msg("%s%8s %8s %8s %8s %8s%s\n",KBLD,"name","nt [#]","frm [m]","V [m^3]","ell [m]",KNRM);
		
		// walk over coils
		for(arma::uword i=0;i<coil_names_.n_elem;i++){
			std::string myname = coil_names_(i);
			if(myname.size()>8)myname = myname.substr(0,7);
			lg->msg("%8s %8.4f %8.4f %8.2e %8.4f\n", 
				myname.c_str(), num_turns_(i), ell_gen_(i), volume_(i), ell_(i));
		}

		// return terminal
		lg->msg(-2,"\n");
		lg->msg(-2);
	}

}}