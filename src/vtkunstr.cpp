// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "vtkunstr.hh"

// rat common headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// vtk includes
#include <vtkXMLUnstructuredGridWriter.h> 
#include <vtkDoubleArray.h>
#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkPointData.h>
#include <vtkDataSet.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkPolyData.h>
#include "vtkCellArray.h"
#include <vtkMergeCells.h>
#include <vtkAppendFilter.h>
#include <vtkLocator.h>

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	VTKUnstr::VTKUnstr(){
		
	}

	// constructor
	VTKUnstr::VTKUnstr(const std::list<ShVTKUnstrPr> &unstr_list, const bool merge_points, const fltp merge_tolerance){
		// count number of nodes
		arma::uword num_nodes = 0, num_elements = 0;
		for(auto it=unstr_list.begin();it!=unstr_list.end();it++){
			num_nodes+=(*it)->get_num_nodes();
			num_elements+=(*it)->get_num_elements(); // of any type
		}
			
		// allocate node coordinates
		Rn_.set_size(3,num_nodes);

		// gather data names
		for(auto it1=unstr_list.begin();it1!=unstr_list.end();it1++){
			// for node data
			const std::map<std::string, arma::Mat<fltp> > &node_data = (*it1)->get_node_data();
			for(auto it2=node_data.begin();it2!=node_data.end();it2++){
				node_data_[(*it2).first].set_size((*it2).second.n_rows,0);
			}

			// for element data
			const std::map<std::string, arma::Mat<fltp> > &element_data = (*it1)->get_element_data();
			for(auto it2=element_data.begin();it2!=element_data.end();it2++){
				element_data_[(*it2).first].set_size((*it2).second.n_rows,0);
			}
		}

		// allocate nodedata
		for(auto it1=node_data_.begin();it1!=node_data_.end();it1++){
			(*it1).second.zeros((*it1).second.n_rows,num_nodes);
		}

		// allocate element data
		for(auto it1=element_data_.begin();it1!=element_data_.end();it1++){
			(*it1).second.zeros((*it1).second.n_rows,num_elements);
		}

		// copy nodes and elements
		arma::uword node_idx_shift = 0, element_idx_shift = 0;
		std::list<arma::Mat<fltp> > R;
		for(auto it1=unstr_list.begin();it1!=unstr_list.end();it1++){
			// get nodes and elements from grid
			const std::map<arma::uword, std::list<arma::Mat<arma::uword> > > &n = (*it1)->get_elements();
			const std::map<arma::uword, std::list<arma::Mat<arma::uword> > > &d = (*it1)->get_data_index();
			const arma::Mat<fltp> &Rn = (*it1)->get_nodes();

			// number of nodes and elements inserted
			const arma::uword num_added_nodes = Rn.n_cols;
			const arma::uword num_added_elements = (*it1)->get_num_elements();

			// copy node coordinates
			Rn_.cols(node_idx_shift,node_idx_shift+Rn.n_cols-1) = Rn;

			// copy elements
			for(auto it2=n.begin();it2!=n.end();it2++){
				const arma::uword element_type = (*it2).first;
				const std::list<arma::Mat<arma::uword> >& nlist = (*it2).second;
				for(auto it3=nlist.begin();it3!=nlist.end();it3++){
					const arma::Mat<arma::uword>& nn = (*it3);
					n_[element_type].push_back(nn + node_idx_shift);
				}
			}

			// copy data indices
			for(auto it2=d.begin();it2!=d.end();it2++){
				const arma::uword element_type = (*it2).first;
				const std::list<arma::Mat<arma::uword> >& dlist = (*it2).second;
				for(auto it3=dlist.begin();it3!=dlist.end();it3++){
					const arma::Mat<arma::uword>& dd = (*it3);
					d_[element_type].push_back(dd + element_idx_shift);
				}
			}

			// copy node data
			const std::map<std::string, arma::Mat<fltp> > &node_data = (*it1)->get_node_data();
			for(auto it2=node_data.begin();it2!=node_data.end();it2++){
				node_data_.at((*it2).first).cols(node_idx_shift,node_idx_shift+num_added_nodes-1) = (*it2).second;
			}

			// copy element data
			const std::map<std::string, arma::Mat<fltp> > &element_data = (*it1)->get_element_data();
			for(auto it2=element_data.begin();it2!=element_data.end();it2++){
				element_data_.at((*it2).first).cols(element_idx_shift,element_idx_shift+num_added_elements-1) = (*it2).second;
			}

			// shift indices
			node_idx_shift+=num_added_nodes;
			element_idx_shift+=num_added_elements;
		}

		// merge elements
		merge_elements();

		// merge duplicate nodes
		if(merge_points){
			// merge nodes and indexing table
			const arma::Row<arma::uword> node_indices = cmn::Extra::combine_nodes(Rn_,merge_tolerance);

			// merge data (average between the connected nodes)
			for(auto it=node_data_.begin();it!=node_data_.end();it++){
				arma::Mat<fltp>& mydata = (*it).second;
				arma::Mat<fltp> merged_data(mydata.n_rows, Rn_.n_cols,arma::fill::zeros);
				arma::Row<fltp> cnt(Rn_.n_cols,arma::fill::zeros);
				for(arma::uword i=0;i<mydata.n_cols;i++){
					merged_data.col(node_indices(i)) += mydata.col(i); cnt(node_indices(i))+=1.0;
				}
				mydata = merged_data.each_row()/cnt;
			}

			// update element indices
			for(auto it=n_.begin();it!=n_.end();it++){
				std::list<arma::Mat<arma::uword> >& elements = (*it).second;
				for(auto it2=elements.begin();it2!=elements.end();it2++){
					arma::Mat<arma::uword>& n = (*it2);
					n = arma::reshape(node_indices(arma::vectorise(n)),n.n_rows,n.n_cols);
				}
			}

			// update data indices
			for(auto it=d_.begin();it!=d_.end();it++){
				std::list<arma::Mat<arma::uword> >& data = (*it).second;
				for(auto it2=data.begin();it2!=data.end();it2++){
					arma::Mat<arma::uword>& d = (*it2);
					d = arma::reshape(node_indices(arma::vectorise(d)),d.n_rows,d.n_cols);
				}
			}
		}

	}

	// factory
	ShVTKUnstrPr VTKUnstr::create(){
		return std::make_shared<VTKUnstr>();
	}

	// factory
	ShVTKUnstrPr VTKUnstr::create(const std::list<ShVTKUnstrPr> &unstr_list, const bool merge_points, const fltp merge_tolerance){
		return std::make_shared<VTKUnstr>(unstr_list, merge_points, merge_tolerance);
	}

	// compact data
	void VTKUnstr::merge_elements(){
		// walk over different element parts
		for(auto it=n_.begin();it!=n_.end();it++){
			// get elements
			// std::list<arma::Mat<arma::uword> >& elements = (*it).second;

			// // count
			// arma::uword num_elements = 0;
			// for(auto it2=elements.begin();it2!=elements.end();it2++){
			// 	const arma::Mat<arma::uword>& n = (*it2);
			// 	num_elements+=n.n_cols;
			// }

			// allocate
			(*it).second = std::list<arma::Mat<arma::uword> >{cmn::Extra::list2mat((*it).second,1)};
		}

		// walk over different element parts
		for(auto it=d_.begin();it!=d_.end();it++){
			// get elements
			// std::list<arma::Mat<arma::uword> >& data = (*it).second;

			// // count
			// arma::uword num_data = 0;
			// for(auto it2=data.begin();it2!=data.end();it2++){
			// 	const arma::Mat<arma::uword>& d = (*it2);
			// 	num_data+=d.n_cols;
			// }

			// allocate
			(*it).second = std::list<arma::Mat<arma::uword> >{cmn::Extra::list2mat((*it).second,1)};
		}
	}

	// merge meshes by index
	std::list<ShVTKUnstrPr> VTKUnstr::merge_by_index(
		const std::list<ShVTKUnstrPr> &unstr_list, 
		const bool merge_points, 
		const fltp merge_tolerance,
		const bool merge_parts){

		// no meshes to merge
		if(unstr_list.empty())return{};

		// create a map
		std::map<arma::uword, std::list<ShVTKUnstrPr> > unstr_map;

		// walk over meshes and insert them into a map
		for(auto it=unstr_list.begin();it!=unstr_list.end();it++)
			unstr_map[merge_parts ? (*it)->get_part_index() : (*it)->get_mesh_index()].push_back(*it);

		// allocate list of merged meshes
		std::list<ShVTKUnstrPr> unstr_combined;

		// walk over map
		for(auto it=unstr_map.begin();it!=unstr_map.end();it++){
			// combine mesh
			const ShVTKUnstrPr unstr = VTKUnstr::create((*it).second, merge_points, merge_tolerance);
			
			// merge parts
			if(merge_parts){
				// set part
				unstr->set_part_index((*it).first);
				
				// is no longer an individual mesh
				unstr->set_mesh_index(0);
			}

			// merge meshes
			else{
				// set mesh
				unstr->set_mesh_index((*it).first);

				// preserve part index
				unstr->set_part_index((*it).second.front()->get_part_index());
			}

			// add to list
			unstr_combined.push_back(unstr);
		}

		// return combined list
		return unstr_combined;
	}


	// get number of nodes
	arma::uword VTKUnstr::get_num_nodes() const{
		return Rn_.n_cols;
	}
	
	// get number of elements
	arma::uword VTKUnstr::get_num_elements() const{
		arma::uword num_elements = 0;
		for(auto it1=n_.begin();it1!=n_.end();it1++){
			const std::list<arma::Mat<arma::uword> >& elements = (*it1).second;
			for(auto it2=elements.begin();it2!=elements.end();it2++){
				const arma::Mat<arma::uword>& n = (*it2);
				num_elements+=n.n_cols;
			}
		}
		return num_elements;
	}

	// set the index
	void VTKUnstr::set_mesh_index(const arma::uword mesh_index){
		mesh_index_ = mesh_index;
	}
		
	// set the index
	void VTKUnstr::set_part_index(const arma::uword part_index){
		part_index_ = part_index;
	}

	// get the index
	arma::uword VTKUnstr::get_mesh_index()const{
		return mesh_index_;
	}

	// get the index
	arma::uword VTKUnstr::get_part_index()const{
		return part_index_;
	}

	// write mesh
	void VTKUnstr::set_mesh(const arma::Mat<fltp> &Rn, 
		const arma::Mat<arma::uword> &n,
		const arma::uword element_type){
		set_nodes(Rn); set_elements(n,element_type);
	}

	// write nodes
	void VTKUnstr::set_nodes(const arma::Mat<fltp> &Rn){
		Rn_ = Rn;
	}

	// write elements
	void VTKUnstr::set_elements(
		const arma::Mat<arma::uword> &n, 
		const arma::uword element_type){
		
		// create indexing array
		const arma::uword num_elements = get_num_elements();
		const arma::Row<arma::uword> d = arma::regspace<arma::Row<arma::uword> >(num_elements, num_elements + n.n_cols - 1);

		// store data (add additional row with data index)
		n_[element_type].push_back(n);
		d_[element_type].push_back(d);
	}

	// write elements with different types
	void VTKUnstr::set_elements(
		const arma::field<arma::Mat<arma::uword> > &n,
		const arma::Row<arma::uword> &element_types){
		// add elements
		for(arma::uword i=0;i<n.n_elem;i++)set_elements(n(i),element_types(i));

		// merge
		merge_elements();
	}

	// write data at nodes
	void VTKUnstr::set_nodedata(
		const arma::Mat<fltp> &v, 
		const std::string &data_name){
		node_data_[data_name] = v;
	}

	// write data at nodes
	void VTKUnstr::set_elementdata(
		const arma::Mat<fltp> &v, 
		const std::string &data_name){
		element_data_[data_name] = v;
	}

	// get elements
	const std::map<arma::uword, std::list<arma::Mat<arma::uword> > >& VTKUnstr::get_elements()const{
		return n_;
	}

	const std::map<arma::uword, std::list<arma::Mat<arma::uword> > >& VTKUnstr::get_data_index()const{
		return d_;
	}

	// get data at nodes
	const std::map<std::string, arma::Mat<fltp> >& VTKUnstr::get_node_data()const{
		return node_data_;
	}

	// get element data
	const std::map<std::string, arma::Mat<fltp> >& VTKUnstr::get_element_data()const{
		return element_data_;
	}

	// get nodes
	const arma::Mat<fltp>& VTKUnstr::get_nodes()const{
		return Rn_;
	}

	// get filename extension
	std::string VTKUnstr::get_filename_ext() const{
		return "vtu";
	}

	// write output file
	void VTKUnstr::write(const boost::filesystem::path fname, const cmn::ShLogPr& lg){
		// create extended filename
		const std::string ext = get_filename_ext();
		boost::filesystem::path fname_ext = fname;
		fname_ext.replace_extension(ext);

		// display file settings and type
		lg->msg(2,"%sVTK Unstr: %s%s\n",KBLU,get_name().c_str(),KNRM);
		lg->msg("filename: %s%s%s\n",KYEL,fname_ext.string().c_str(),KNRM);
		lg->msg("type: unstructed grid (%s)\n",ext.c_str());

		// message
		lg->msg("converting nodes to vtk\n");

		// create vtk grid dataset
		vtkSmartPointer<vtkUnstructuredGrid> vtk_ugrid = vtkSmartPointer<vtkUnstructuredGrid>::New();

		// create vtk node coordinates
		vtkSmartPointer<vtkPoints> nodes = vtkSmartPointer<vtkPoints>::New(); 
		nodes->SetDataTypeToDouble();
		
		// get number of nodes
		const arma::uword num_nodes = Rn_.n_cols;

		// copy node data
		nodes->Allocate(Rn_.n_elem);
		for(arma::uword i=0;i<Rn_.n_cols;i++)
			nodes->InsertNextPoint(Rn_(0,i),Rn_(1,i),Rn_(2,i));

		// set points to vtk grid
		vtk_ugrid->SetPoints(nodes);


		// message
		lg->msg("converting elements to vtk\n");

		// create vtk element data
		vtkSmartPointer<vtkCellArray> elements = vtkSmartPointer<vtkCellArray>::New();

		// count number of elements
		arma::uword num_elements = 0; 
		arma::uword num_point_id = 0;
		for(auto it1=n_.begin();it1!=n_.end();it1++){
			const std::list<arma::Mat<arma::uword> >& nlist = (*it1).second;
			for(auto it2=nlist.begin();it2!=nlist.end();it2++){
				const arma::Mat<arma::uword>& n = (*it2);
				num_elements += n.n_cols;
				num_point_id += n.n_elem;
			}
		}

		// allocate memory
		vtk_ugrid->Allocate(num_elements + num_point_id, 0);

		// walk over element types
		for(auto it1=n_.begin();it1!=n_.end();it1++){
			// get list of matrices
			const arma::uword element_type = (*it1).first;
			const std::list<arma::Mat<arma::uword> >& nlist = (*it1).second;

			// compact list
			arma::Mat<arma::uword> n = cmn::Extra::list2mat(nlist,1);

			// convert to signed integer
			const arma::Mat<arma::sword> ns = 
				arma::conv_to<arma::Mat<arma::sword> >::from(n);

			// insert cell elements
			for(arma::uword j=0;j<ns.n_cols;j++){
				vtk_ugrid->InsertNextCell(
					static_cast<int>(element_type), 
					static_cast<vtkIdType>(ns.n_rows), 
					reinterpret_cast<const vtkIdType*>(ns.colptr(j)));
			}
		}

		// message
		lg->msg("converting data to vtk\n");

		// create vtk node and element data
		std::list<vtkSmartPointer<vtkDoubleArray> > node_data;
		std::list<vtkSmartPointer<vtkDoubleArray> > element_data;

		// walk over element types
		for(auto it1=node_data_.begin();it1!=node_data_.end();it1++){
			// get references
			const std::string& data_name = (*it1).first;
			const arma::Mat<fltp>& v = (*it1).second;

			// check input
			if(v.n_cols!=num_nodes)rat_throw_line("data does not match number of nodes: " 
				+ std::to_string(v.n_cols) + "/" + std::to_string(num_nodes));

			// create vtk array
			vtkSmartPointer<vtkDoubleArray> vvtk = vtkSmartPointer<vtkDoubleArray>::New();
			node_data.push_back(vvtk);

			// add name to data
			vvtk->SetName(data_name.c_str());

			// allocate
			vvtk->SetNumberOfComponents(v.n_rows);
			vvtk->SetNumberOfValues(v.n_elem);
			
			// insert data elements
			for(arma::uword i=0;i<v.n_elem;i++)
				vvtk->SetValue(i,v(i));

			// set data to vtk grid
			vtk_ugrid->GetPointData()->AddArray(vvtk);
		}

		// get data re-indexing array
		arma::Row<arma::uword> dd(get_num_elements());
		arma::uword idx = 0;
		for(auto it1=d_.begin();it1!=d_.end();it1++){
			const arma::Row<arma::uword> di = cmn::Extra::list2mat((*it1).second,1);
			dd.cols(idx, idx+di.n_elem-1) = di;
			idx+=di.n_elem;
		}

		// walk over element types
		for(auto it1=element_data_.begin();it1!=element_data_.end();it1++){
			// get references
			const std::string& data_name = (*it1).first;

			// sort 
			arma::Mat<fltp> v = (*it1).second;
			v = v.cols(dd);

			// check input
			if(v.n_cols!=num_elements)rat_throw_line("data does not match number of elements: " 
				+ std::to_string(v.n_cols) + "/" + std::to_string(num_nodes));

			// create vtk array
			vtkSmartPointer<vtkDoubleArray> vvtk = vtkSmartPointer<vtkDoubleArray>::New();
			element_data.push_back(vvtk);

			// add name to data
			vvtk->SetName(data_name.c_str());

			// allocate
			vvtk->SetNumberOfComponents(v.n_rows);
			vvtk->SetNumberOfValues(v.n_elem);
			
			// insert data elements
			for(arma::uword i=0;i<v.n_elem;i++)
				vvtk->SetValue(i,v(i));

			// set data to vtk grid
			vtk_ugrid->GetCellData()->AddArray(vvtk);
		}

		// message
		lg->msg("writing output file\n");

		// create unstructured grid writer and set it up
		vtkSmartPointer<vtkXMLUnstructuredGridWriter> writer =  
			vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();
		writer->SetFileName(fname_ext.string().c_str());
		writer->SetInputData(vtk_ugrid);
		// writer->SetCompressorTypeToNone();
		writer->SetCompressorTypeToLZ4();
		writer->SetDataModeToBinary();
		
		// perform write
		writer->Write();

		// done writing VTK unstructured grid
		lg->msg(-2,"\n");
	}


	// element types
	int VTKUnstr::get_element_type(const arma::uword num_rows, const arma::uword num_dim){
		int type = 0;
		if(num_rows==1 && num_dim==0)type = 1; // points
		else if(num_rows==2 && num_dim==1)type = 3; // line
		else if(num_rows==3 && num_dim==2)type = 5; // triangle
		else if(num_rows==4 && num_dim==2)type = 9; // quadrilateral
		else if(num_rows==4 && num_dim==3)type = 10; // tetrahedron
		else if(num_rows==8 && num_dim==3)type = 12; // hexahedron
		else rat_throw_line("unknown element type: n_rows = " + std::to_string(num_rows) + ", n_dim = " + std::to_string(num_dim));
		return type;
	}


	// perform interpolation of specific field at given target coordinates
	arma::Mat<fltp> VTKUnstr::interpolate_field(const arma::Mat<fltp> &Rp, const std::string &field_name){
		// create vtk grid dataset
		vtkSmartPointer<vtkUnstructuredGrid> vtk_ugrid = vtkSmartPointer<vtkUnstructuredGrid>::New();

		// create vtk node coordinates
		vtkSmartPointer<vtkPoints> nodes = vtkSmartPointer<vtkPoints>::New(); 
		nodes->SetDataTypeToDouble();
		
		// get number of nodes
		const arma::uword num_nodes = Rn_.n_cols;

		// copy node data
		nodes->Allocate(Rn_.n_elem);
		for(arma::uword i=0;i<Rn_.n_cols;i++)
			nodes->InsertNextPoint(Rn_(0,i),Rn_(1,i),Rn_(2,i));

		// set points to vtk grid
		vtk_ugrid->SetPoints(nodes);

		// create vtk element data
		vtkSmartPointer<vtkCellArray> elements = vtkSmartPointer<vtkCellArray>::New();

		// count number of elements
		arma::uword num_elements = 0; 
		arma::uword num_point_id = 0;
		for(auto it1=n_.begin();it1!=n_.end();it1++){
			const std::list<arma::Mat<arma::uword> >& nlist = (*it1).second;
			for(auto it2=nlist.begin();it2!=nlist.end();it2++){
				const arma::Mat<arma::uword>& n = (*it2);
				num_elements += n.n_cols;
				num_point_id += n.n_elem;
			}
		}

		// allocate memory
		vtk_ugrid->Allocate(num_elements + num_point_id, 0);

		// create vtk node and element data
		std::list<vtkSmartPointer<vtkDoubleArray> > node_data;
		std::list<vtkSmartPointer<vtkDoubleArray> > element_data;

		// walk over element types
		for(auto it1=n_.begin();it1!=n_.end();it1++){
			// get list of matrices
			const arma::uword element_type = (*it1).first;
			const std::list<arma::Mat<arma::uword> >& nlist = (*it1).second;

			// compact list
			arma::Mat<arma::uword> n = cmn::Extra::list2mat(nlist,1);

			// convert to signed integer
			const arma::Mat<arma::sword> ns = 
				arma::conv_to<arma::Mat<arma::sword> >::from(n);

			// insert cell elements
			for(arma::uword j=0;j<ns.n_cols;j++){
				vtk_ugrid->InsertNextCell(
					static_cast<int>(element_type), 
					static_cast<vtkIdType>(ns.n_rows), 
					reinterpret_cast<const vtkIdType*>(ns.colptr(j)));
			}
		}

		// message
		auto it1 = node_data_.find(field_name);
		if(it1!=node_data_.end()){
			// get references
			const std::string& data_name = (*it1).first;
			const arma::Mat<fltp>& v = (*it1).second;

			// check input
			if(v.n_cols!=num_nodes)rat_throw_line("data does not match number of nodes: " 
				+ std::to_string(v.n_cols) + "/" + std::to_string(num_nodes));

			// create vtk array
			vtkSmartPointer<vtkDoubleArray> vvtk = vtkSmartPointer<vtkDoubleArray>::New();
			node_data.push_back(vvtk);

			// add name to data
			vvtk->SetName(data_name.c_str());

			// allocate
			vvtk->SetNumberOfComponents(v.n_rows);
			vvtk->SetNumberOfValues(v.n_elem);
			
			// insert data elements
			for(arma::uword i=0;i<v.n_elem;i++)
				vvtk->SetValue(i,v(i));

			// set data to vtk grid
			vtk_ugrid->GetPointData()->AddArray(vvtk);
		}

		auto it2 = element_data_.find(field_name);
		if(it2!=element_data_.end()){
			// get data re-indexing array
			arma::Row<arma::uword> dd(get_num_elements());
			arma::uword idx = 0;
			for(auto it1=d_.begin();it1!=d_.end();it1++){
				const arma::Row<arma::uword> di = cmn::Extra::list2mat((*it1).second,1);
				dd.cols(idx, idx+di.n_elem-1) = di;
				idx+=di.n_elem;
			}

			// get references
			const std::string& data_name = (*it2).first;

			// sort 
			arma::Mat<fltp> v = (*it2).second;
			v = v.cols(dd);

			// check input
			if(v.n_cols!=num_elements)rat_throw_line("data does not match number of elements: " 
				+ std::to_string(v.n_cols) + "/" + std::to_string(num_nodes));

			// create vtk array
			vtkSmartPointer<vtkDoubleArray> vvtk = vtkSmartPointer<vtkDoubleArray>::New();
			element_data.push_back(vvtk);

			// add name to data
			vvtk->SetName(data_name.c_str());

			// allocate
			vvtk->SetNumberOfComponents(v.n_rows);
			vvtk->SetNumberOfValues(v.n_elem);
			
			// insert data elements
			for(arma::uword i=0;i<v.n_elem;i++)
				vvtk->SetValue(i,v(i));

			// set data to vtk grid
			vtk_ugrid->GetCellData()->AddArray(vvtk);
		}

		// create target points
		vtkSmartPointer<vtkPoints> target_points = vtkSmartPointer<vtkPoints>::New();
		target_points->SetDataTypeToDouble();
		target_points->Allocate(Rp.n_elem);
		for(arma::uword i=0;i<Rp.n_cols;i++)
			target_points->InsertNextPoint(Rp(0,i), Rp(1,i), Rp(2,i));

		// wrap points into polydata
		vtkSmartPointer<vtkPolyData> probe_points = vtkSmartPointer<vtkPolyData>::New();
		probe_points->SetPoints(target_points);

		// create a probe filter to perform the interpolation
		auto probe_filter = vtkSmartPointer<vtkProbeFilter>::New();
		probe_filter->SetSourceData(vtk_ugrid);
		probe_filter->SetInputData(probe_points);
		probe_filter->Update();

		// access the interpolated data
		vtkDataSet* interpolated_data = probe_filter->GetOutput();
		vtkPointData* point_data = interpolated_data->GetPointData();
		vtkSmartPointer<vtkDataArray> interpolated_scalars = point_data->GetArray(0); 

		// allocate output
		arma::Mat<fltp> output_data(interpolated_scalars->GetNumberOfComponents(),interpolated_scalars->GetNumberOfTuples());

		// check 
		if(!interpolated_scalars)rat_throw_line("could not interpolate");
			
		// extract data 
		for(vtkIdType i=0;i<interpolated_scalars->GetNumberOfTuples();++i){
			interpolated_scalars->GetTuple(i, output_data.colptr(i));
		}

		// return the data
		return output_data;
	}

}}
