// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathconnect.hh"

#include "rat/common/extra.hh"

// #include "darboux.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathConnect::PathConnect(){
		set_name("Connection");
	}

	// constructor with dimension input
	PathConnect::PathConnect(
		const ShPathPr &prev_path, 
		const ShPathPr &next_path, 
		const fltp str1, const fltp str2, 
		const fltp str3, const fltp str4, 
		const fltp ell_trans1, 
		const fltp ell_trans2, 
		const fltp element_size) : PathConnect(){

		//check input
		assert(prev_path!=NULL);
		assert(next_path!=NULL);
		assert(ell_trans1>=0);
		assert(ell_trans2>=0);
		assert(element_size>0);

		// store to self
		add_path(prev_path);
		add_path(next_path);
		
		// set strengths
		str1_ = str1; str2_ = str2; 
		str3_ = str3; str4_ = str4;

		// set transition length
		ell_trans1_ = ell_trans1;
		ell_trans2_ = ell_trans2;

		// set element size
		element_size_ = element_size;
	}


	// factory
	ShPathConnectPr PathConnect::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<PathConnect>();
	}

	// factory with dimension input
	ShPathConnectPr PathConnect::create(
		const ShPathPr &prev_path, 
		const ShPathPr &next_path, 
		const fltp str1, const fltp str2, 
		const fltp str3, const fltp str4, 
		const fltp ell_trans1, 
		const fltp ell_trans2, 
		const fltp element_size){
		//return ShPathCirclePr(new PathCircle(radius,dl));
		return std::make_shared<PathConnect>(prev_path,next_path,str1,str2,str3,str4,ell_trans1,ell_trans2,element_size);
	}

	// retreive model at index
	ShPathPr PathConnect::get_path(const arma::uword index) const{
		auto it = input_paths_.find(index);
		if(it==input_paths_.end())rat_throw_line("index does not exist");
		return (*it).second;
	}

	// delete model at index
	bool PathConnect::delete_path(const arma::uword index){	
		auto it = input_paths_.find(index);
		if(it==input_paths_.end())return false;
		(*it).second = NULL; return true;
	}

	// re-index nodes after deleting
	void PathConnect::reindex(){
		Transformations::reindex();
		std::map<arma::uword, ShPathPr> new_paths; arma::uword idx = 1;
		for(auto it=input_paths_.begin();it!=input_paths_.end();it++)
			if((*it).second!=NULL)new_paths.insert({idx++, (*it).second});
		input_paths_ = new_paths;
	}

	// function for adding a path to the path list
	arma::uword PathConnect::add_path(const ShPathPr &path){
		// check input
		assert(path!=NULL); 

		// get counters
		const arma::uword index = input_paths_.size()+1;

		// insert
		input_paths_.insert({index, path});

		// return index
		return index;
	}

	// get number of paths
	arma::uword PathConnect::get_num_paths() const{
		return input_paths_.size();
	}
	// setters
	void PathConnect::set_strengths(
		const fltp str1, const fltp str2, const fltp str3, const fltp str4){
		set_str1(str1); set_str2(str2); set_str3(str3); set_str4(str4);
	}

	void PathConnect::set_str1(const fltp str1){
		str1_ = str1;
	}

	void PathConnect::set_str2(const fltp str2){
		str2_ = str2;
	}

	void PathConnect::set_str3(const fltp str3){
		str3_ = str3;
	}

	void PathConnect::set_str4(const fltp str4){
		str4_ = str4;
	}

	void PathConnect::set_ell_trans1(const fltp ell_trans1){
		ell_trans1_ = ell_trans1;
	}

	void PathConnect::set_ell_trans2(const fltp ell_trans2){
		ell_trans2_ = ell_trans2;
	}

	void PathConnect::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	void PathConnect::set_use_binormal(const bool use_binormal){
		use_binormal_ = use_binormal;
	}

	


	// getters
	fltp PathConnect::get_str1()const{
		return str1_;
	}

	fltp PathConnect::get_str2()const{
		return str2_;
	}

	fltp PathConnect::get_str3()const{
		return str3_;
	}

	fltp PathConnect::get_str4()const{
		return str4_;
	}

	fltp PathConnect::get_ell_trans1()const{
		return ell_trans1_;
	}

	fltp PathConnect::get_ell_trans2()const{
		return ell_trans2_;
	}

	fltp PathConnect::get_element_size()const{
		return element_size_;
	}

	bool PathConnect::get_use_binormal()const{
		return use_binormal_;
	}


	// validity check
	bool PathConnect::is_valid(const bool enable_throws) const{
		if(input_paths_.size()!=1 && input_paths_.size()!=2){if(enable_throws){rat_throw_line("need one or two input paths (no more)");} return false;};
		if(str1_<=0){if(enable_throws){rat_throw_line("first strength must be larger than zero");} return false;};
		if(str2_<=0){if(enable_throws){rat_throw_line("first strength must be larger than zero");} return false;};
		if(ell_trans1_<0){if(enable_throws){rat_throw_line("transition must be larger than or equal to zero");} return false;};
		if(ell_trans2_<0){if(enable_throws){rat_throw_line("transition must be larger than or equal to zero");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		for(auto it=input_paths_.begin();it!=input_paths_.end();it++)if(!(*it).second->is_valid(enable_throws))return false;
		return true;
	}

	// update function
	ShFramePr PathConnect::create_frame(const MeshSettings &stngs) const{
		// check validity
		is_valid(true);

		// get start and end path
		// ShPathPr prev_path = prev_path_, next_path;
		// if(next_path_==NULL && prev_path_!=NULL){
		// 	prev_path = prev_path_; next_path = prev_path_;
		// }else if(next_path_!=NULL && prev_path_==NULL){
		// 	prev_path = next_path_; next_path = next_path_;
		// }else if(next_path_!=NULL && prev_path_!=NULL){
		// 	prev_path = prev_path_; next_path = next_path_;
		// }else{
		// 	rat_throw_line("paths are not set");
		// }

		// get paths
		ShPathPr prev_path, next_path;
		if(input_paths_.size()==1){
			prev_path = input_paths_.at(1);
			next_path = input_paths_.at(1);
		}else{
			prev_path = input_paths_.at(1);
			next_path = input_paths_.at(2);
		}

		// sanity check
		assert(prev_path!=NULL);
		assert(next_path!=NULL);

		// create frame
		const ShFramePr prev_gen = prev_path->create_frame(stngs);
		const ShFramePr next_gen = next_path->create_frame(stngs);

		// get frame of first path
		const arma::field<arma::Mat<fltp> >& R0 = prev_gen->get_coords();
		const arma::field<arma::Mat<fltp> >& L0 = prev_gen->get_direction();
		const arma::field<arma::Mat<fltp> >& N0 = prev_gen->get_normal();
		const arma::field<arma::Mat<fltp> >& D0 = prev_gen->get_transverse();
		const arma::field<arma::Mat<fltp> >& B0 = prev_gen->get_block();

		// get frame of second path
		const arma::field<arma::Mat<fltp> >& R1 = next_gen->get_coords();
		const arma::field<arma::Mat<fltp> >& L1 = next_gen->get_direction();
		const arma::field<arma::Mat<fltp> >& N1 = next_gen->get_normal();
		const arma::field<arma::Mat<fltp> >& D1 = next_gen->get_transverse();
		const arma::field<arma::Mat<fltp> >& B1 = prev_gen->get_block();

		// start coordinate system
		const arma::uword num_first = R0.n_elem;
		const arma::Col<fltp>::fixed<3> r0 = R0(num_first-1).tail_cols(1);
		const arma::Col<fltp>::fixed<3> l0 = L0(num_first-1).tail_cols(1); 
		arma::Col<fltp>::fixed<3> n0 = N0(num_first-1).tail_cols(1); 
		arma::Col<fltp>::fixed<3> b0 = B0(num_first-1).tail_cols(1); 
		const arma::Col<fltp>::fixed<3> d0 = D0(num_first-1).tail_cols(1);

		// end coordinate system
		const arma::Col<fltp>::fixed<3> r1 = R1(0).head_cols(1);
		const arma::Col<fltp>::fixed<3> l1 = L1(0).head_cols(1);
		arma::Col<fltp>::fixed<3> n1 = N1(0).head_cols(1); 
		arma::Col<fltp>::fixed<3> b1 = B1(0).head_cols(1);
		const arma::Col<fltp>::fixed<3> d1 = D1(0).head_cols(1);

		// normalize normal direction
		const fltp n0norm = arma::as_scalar(cmn::Extra::vec_norm(n0)); n0/=n0norm;
		const fltp n1norm = arma::as_scalar(cmn::Extra::vec_norm(n1)); n1/=n1norm;
		const fltp b0norm = arma::as_scalar(cmn::Extra::vec_norm(b0)); b0/=b0norm;
		const fltp b1norm = arma::as_scalar(cmn::Extra::vec_norm(b1)); b1/=b1norm;

		// create generator pointer
		ShFramePr gen;

		// cubic spline with twist
		if(str3_==0 && str4_==0){
			// check connection
			const ShPathBSplinePr bezier = PathBSpline::create(
				r0, l0, n0, r1, l1, n1, str1_, str2_, element_size_);
			bezier->set_in_plane();
			
			// return frame for this path
			gen = bezier->create_frame(stngs);
		}

		// spline with constant perimeter
		else{
			// control points in UV plane
			const arma::Mat<fltp> Puv1 = arma::reshape(arma::Col<fltp>{
				0.0,0.0,0.0, 
				1*str1_,0,0.0, 
				2*str1_,0,0.0, 
				3*str1_,0,0.0, 
				4*str1_,1*str3_,0.0,
				5*str1_,2*str3_,0.0, 
				6*str1_,4*str3_,0.0, 
				6*str1_,10*str3_,0.0},3,8);
			const arma::Mat<fltp> Puv2 = arma::reshape(arma::Col<fltp>{
				0.0,0.0,0.0, 
				1*str2_,0,0.0, 
				2*str2_,0,0.0, 
				3*str2_,0,0.0, 
				4*str2_,1*str4_,0.0, 
				5*str2_,2*str4_,0.0, 
				6*str2_,4*str4_,0.0, 
				6*str2_,10*str4_,0.0},3,8);

			// check input
			assert(Puv1.is_finite());
			assert(Puv2.is_finite());

			// check connection
			const fltp path_offset = RAT_CONST(0.0);
			const ShPathBezierPr bezier = PathBezier::create(
				r0, l0, n0, d0, r1, l1, n1, d1, Puv1, Puv2, 
				ell_trans1_, ell_trans2_, element_size_, path_offset, use_binormal_);

			// return frame for this path
			gen = bezier->create_frame(stngs);
		}

		// scaling in the normal direction
		const arma::Row<fltp> ell = arma::cumsum(gen->calc_ell());
		const arma::Row<fltp> ell_frac = arma::join_horiz(arma::Row<fltp>{0.0}, ell)/ell.back();
		const arma::Row<fltp> nscaling = n1norm*ell_frac + (1.0 - ell_frac)*n0norm;
		const arma::Row<fltp> bscaling = b1norm*ell_frac + (1.0 - ell_frac)*b0norm;

		// scale normal based on start and end points
		for(arma::uword i=0;i<gen->get_num_sections();i++){
			const arma::Mat<fltp> Nscale = gen->get_normal(i).each_row()%arma::linspace<arma::Row<fltp> >(nscaling(i), nscaling(i+1), gen->get_num_nodes(i));
			const arma::Mat<fltp> Bscale = gen->get_block(i).each_row()%arma::linspace<arma::Row<fltp> >(bscaling(i), bscaling(i+1), gen->get_num_nodes(i));
			gen->set_normal(i, Nscale);
			gen->set_block(i, Bscale);
		}

		// transfer type
		gen->set_conductor_type(prev_gen->get_conductor_type());

		// create generator list
		std::list<ShFramePr> genlist{prev_gen,gen};
		if(prev_path!=next_path)genlist.push_back(next_gen);

		// combine frame and return
		const ShFramePr combined_frame = Frame::create(genlist);

		// apply transformations
		combined_frame->apply_transformations(get_transformations(), stngs.time);

		// return frame
		return combined_frame;
	}

	// get type
	std::string PathConnect::get_type(){
		return "rat::mdl::pathconnect";
	}

	// method for serialization into json
	void PathConnect::serialize(Json::Value &js, cmn::SList &list) const{

		// parent objects
		Path::serialize(js,list);
		Transformations::serialize(js,list);

		// type
		js["type"] = get_type();

		// serialize the models
		for(auto it = input_paths_.begin();it!=input_paths_.end();it++)
			js["input_paths"].append(cmn::Node::serialize_node((*it).second, list));

		// shape
		js["str1"] = str1_;
		js["str2"] = str2_;
		js["str3"] = str3_;
		js["str4"] = str4_;
		js["ell_trans1"] = ell_trans1_;
		js["ell_trans2"] = ell_trans2_;
		js["element_size"] = element_size_;
		js["use_binormal"] = use_binormal_;
	}

	// method for deserialisation from json
	void PathConnect::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent objects
		Path::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);

		// backwards compatibility with 2.015.1
		if(js.isMember("prev_path") && js.isMember("next_path")){
			add_path(cmn::Node::deserialize_node<Path>(js["prev_path"], list, factory_list, pth));
			add_path(cmn::Node::deserialize_node<Path>(js["next_path"], list, factory_list, pth));
		}

		// new implementation
		else{
			for(auto it = js["input_paths"].begin();it!=js["input_paths"].end();it++)
				add_path(cmn::Node::deserialize_node<Path>(
					(*it), list, factory_list, pth));
		}

		// shape
		set_str1(js["str1"].ASFLTP());
		set_str2(js["str2"].ASFLTP());
		set_str3(js["str3"].ASFLTP());
		set_str4(js["str4"].ASFLTP());

		// backwards compatibility with 2.015.1
		if(js.isMember("ell_trans")){
			set_ell_trans1(js["ell_trans"].ASFLTP());
			set_ell_trans2(js["ell_trans"].ASFLTP());
		}else{
			set_ell_trans1(js["ell_trans1"].ASFLTP());
			set_ell_trans2(js["ell_trans2"].ASFLTP());
		}
		set_element_size(js["element_size"].ASFLTP());
		set_use_binormal(js["use_binormal"].asBool());
	}

}}