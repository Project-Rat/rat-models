// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathfile2.hh"

// rat-common headers
#include "rat/common/extra.hh"

// rat-models headers
#include "pathxyz.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathFile2::PathFile2(){
		set_name("XYZ File");
	}

	// constructor
	PathFile2::PathFile2(const boost::filesystem::path &file_name, const arma::uword index) : PathFile2(){
		set_file_name(file_name); set_index(index);
	}

	// factory
	ShPathFile2Pr PathFile2::create(){
		return std::make_shared<PathFile2>();
	}

	// factory
	ShPathFile2Pr PathFile2::create(const boost::filesystem::path &file_name, const arma::uword index){
		return std::make_shared<PathFile2>(file_name,index);
	}

	// setters
	void PathFile2::set_file_name(const boost::filesystem::path &file_name){
		file_name_ = file_name;
	}

	void PathFile2::set_index(const arma::uword index){
		index_ = index;
	}

	void PathFile2::set_scale_factor(const fltp scale_factor){
		scale_factor_ = scale_factor;
	}

	void PathFile2::set_plane_vector(const arma::Col<fltp>::fixed<3>& plane_vector){
		Vn_ = plane_vector;
	}

	void PathFile2::set_is_cylinder(const bool is_cylinder){
		is_cylinder_ = is_cylinder;
	}

	void PathFile2::set_element_size_limit(const fltp element_size_limit){
		element_size_limit_ = element_size_limit;
	}

	void PathFile2::set_use_plane_vector(const bool use_plane_vector){
		use_plane_vector_ = use_plane_vector;
	}

	void PathFile2::set_normalize_orientation_vectors(const bool normalize_orientation_vectors){
		normalize_orientation_vectors_ = normalize_orientation_vectors;
	}



	// getters
	const boost::filesystem::path& PathFile2::get_file_name()const{
		return file_name_;
	}

	arma::uword PathFile2::get_index()const{
		return index_;
	}

	fltp PathFile2::get_scale_factor()const{
		return scale_factor_;
	}

	const arma::Col<fltp>::fixed<3>& PathFile2::get_plane_vector()const{
		return Vn_;
	}

	bool PathFile2::get_is_cylinder()const{
		return is_cylinder_;
	}

	fltp PathFile2::get_element_size_limit()const{
		return element_size_limit_;
	}

	bool PathFile2::get_use_plane_vector()const{
		return use_plane_vector_;
	}

	bool PathFile2::get_normalize_orientation_vectors()const{
		return normalize_orientation_vectors_;
	}


	// setup coordinates and orientation vectors
	ShFramePr PathFile2::create_frame(const MeshSettings &stngs) const{
		// check input
		is_valid(true);

		// load data from file
		arma::Mat<fltp> M; arma::field<std::string> header;
		M.load(arma::csv_name(file_name_.string(), header));
		if(!M.is_finite())rat_throw_line("file contains non finite number");

		// load coordinates
		const arma::uword stride = use_plane_vector_ ? 3llu : 12llu;

		// check supplied table
		if(M.n_cols<(index_+1)*stride)rat_throw_line("file does not contain enough columns");

		// extract coordinates
		const arma::Mat<fltp> R = M.cols(index_*stride, index_*stride + 2).t(); //position

		// test file script for octave
		// num_theta = 100; radius = 0.05; theta = linspace(0,2*pi,num_theta); R = [radius*sin(theta);radius*cos(theta);zeros(1,num_theta)]; L = diff(R,1,2); L = L./sqrt(sum(L.^2,1)); L = [L,zeros(3,1)] + [zeros(3,1),L]; L(:,2:end-1) = L(:,2:end-1)/2; N = cross(L,repmat([0;0;1],1,num_theta)); D=cross(N,L); csvwrite('data.csv',[R',L',N',D']);

		// read vectors
		arma::Mat<fltp> L,N,D;
		if(!use_plane_vector_){
			L = M.cols(index_*stride + 3, index_*stride + 5).t();
			N = M.cols(index_*stride + 6, index_*stride + 8).t();
			D = M.cols(index_*stride + 9, index_*stride + 11).t();
		}

		// create a path
		const ShPathXYZPr xyz = PathXYZ::create(R);
		xyz->set_longitudinal(L);
		xyz->set_normal(N);
		xyz->set_transverse(D);
		xyz->set_scale_factor(scale_factor_);
		xyz->set_is_cylinder(is_cylinder_);
		xyz->set_element_size_limit(element_size_limit_);
		xyz->set_use_plane_vector(use_plane_vector_);
		xyz->set_normalize_orientation_vectors(normalize_orientation_vectors_);
		xyz->set_normal_vector(Vn_);

		// create frame
		const ShFramePr frame = xyz->create_frame(stngs);

		// transformations
		frame->apply_transformations(get_transformations(), stngs.time);

		// create frame
		return frame;
	}

	// vallidity check
	bool PathFile2::is_valid(const bool enable_throws) const{
		if(scale_factor_<=0){if(enable_throws){rat_throw_line("scaling factor must be larger than zero");} return false;};
		if(file_name_.empty()){if(enable_throws){rat_throw_line("input file not set");} return false;};
		boost::system::error_code ec;
		if(!boost::filesystem::exists(file_name_,ec)){if(enable_throws){rat_throw_line("file does not exist");} return false;};
		if(arma::as_scalar(cmn::Extra::vec_norm(Vn_))<1e-14){if(enable_throws){rat_throw_line("normal vector has no length");} return false;};
		return true;
	}

	// get type
	std::string PathFile2::get_type(){
		return "rat::mdl::pathfile2";
	}

	// method for serialization into json
	void PathFile2::serialize(
		Json::Value &js, cmn::SList &list) const{
		// parent
		Path::serialize(js,list);
		Transformations::serialize(js,list);
		js["type"] = get_type();
		js["file_name"] = file_name_.string();
		js["index"] = static_cast<int>(index_);
		js["scale_factor"] = scale_factor_;
		js["Vn"]["x"] = Vn_(0);
		js["Vn"]["y"] = Vn_(1);
		js["Vn"]["z"] = Vn_(2);
		js["is_cylinder"] = is_cylinder_;
		js["element_size_limit"] = element_size_limit_;
		js["use_plane_vector"] = use_plane_vector_;
		js["normalize_orientation_vectors"] = normalize_orientation_vectors_;
	}

	// method for deserialisation from json
	void PathFile2::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent
		Path::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);

		// get path
		boost::filesystem::path filepath(
			Node::fix_filesep(js["file_name"].asString()));

		// check if path exists
		boost::system::error_code ec;
		if(!boost::filesystem::exists(filepath,ec)){
			if(boost::filesystem::exists(pth/filepath.filename(),ec))
				filepath = pth/filepath.filename();
		}

		// set filename
		set_file_name(filepath);

		// index and normal vector
		index_ = js["index"].asUInt64();
		Vn_(0) = js["Vn"]["x"].ASFLTP();
		Vn_(1) = js["Vn"]["y"].ASFLTP();
		Vn_(2) = js["Vn"]["z"].ASFLTP();
		if(js.isMember("scale_factor"))set_scale_factor(js["scale_factor"].ASFLTP());
		if(js.isMember("use_plane_vector"))set_use_plane_vector(js["use_plane_vector"].asBool());
		set_is_cylinder(js["is_cylinder"].asBool());
		set_element_size_limit(js["element_size_limit"].ASFLTP());
		set_normalize_orientation_vectors(js["normalize_orientation_vectors"].asBool());
	}

}}