// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "data.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// // get name
	// std::string Data::get_name() const{
	// 	return name_;
	// }

	// // set name normally
	// void Data::set_name(const std::string &name){
	// 	name_ = name;
	// }

	// // append name
	// void Data::append_name(const std::string &name, const bool append_back){
	// 	if(append_back==false)
	// 		name_ = name + "_" + name_;
	// 	else name_ = name_ + "_" + name;
	// }

	// output type
	void Data::set_output_type(const std::string &output_type){
		output_type_ = output_type;
	}

	// get output type
	std::string Data::get_output_type() const{
		return output_type_;
	}

	// set time 
	void Data::set_time(const fltp time){
		time_ = time;
	}

	// set time 
	fltp Data::get_time() const{
		return time_;
	}

	// interpolate
	arma::Mat<fltp> Data::interpolate(
		const arma::Mat<fltp> &/*values*/, 
		const arma::Mat<fltp> &/*R*/, 
		const bool /*use_parallel*/) const{
		rat_throw_line("interpolation not implemented for this data type");
	}

}}