// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelmirror.hh"

// common headers
#include "rat/common/error.hh"
#include "rat/common/parfor.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ModelMirror::ModelMirror(){
		set_name("Mirror");
	}

	// constructor
	ModelMirror::ModelMirror(const ShModelPr &model, const bool keep_original) : ModelMirror() {
		add_model(model); set_keep_original(keep_original);
	}

	// factory
	ShModelMirrorPr ModelMirror::create(){
		return std::make_shared<ModelMirror>();
	}

	// factory
	ShModelMirrorPr ModelMirror::create(const ShModelPr &model, const bool keep_original){
		return std::make_shared<ModelMirror>(model, keep_original);
	}

	// keep original
	void ModelMirror::set_keep_original(const bool keep_original){
		keep_original_ = keep_original;
	}

	bool ModelMirror::get_keep_original() const{
		return keep_original_;
	}

	// keep original
	void ModelMirror::set_anti_mirror(const bool anti_mirror){
		anti_mirror_ = anti_mirror;
	}

	bool ModelMirror::get_anti_mirror() const{
		return anti_mirror_;
	}

	// validity check
	bool ModelMirror::is_valid(const bool enable_throws) const{
		if(!ModelGroup::is_valid(enable_throws))return false;
		if(!TransReflect::is_valid(enable_throws))return false;
		return true;
	}

	// splitting the modelgroup
	std::list<ShModelPr> ModelMirror::split(const ShSerializerPr &slzr) const{
		// ensure validity
		is_valid(true);

		// create new model list
		std::list<ShModelPr> model_list;

		// walk over models
		if(keep_original_){
			for(auto it=models_.begin();it!=models_.end();it++){
				// create copy
				ShModelPr model_copy = slzr->copy((*it).second);

				// append name
				model_copy->set_name(model_copy->get_name() + " A");

				// set color from parent
				if(use_custom_color_)model_copy->set_color(color_, use_custom_color_);

				// add model to list
				model_list.push_back(model_copy);
			}
		}

		// walk over models
		for(auto it=models_.begin();it!=models_.end();it++){
			// create copy
			ShModelPr model_copy = slzr->copy((*it).second);

			// add reflection
			model_copy->add_transformation(TransReflect::create(origin_, plane_vector_));

			// anti-mirror
			if(anti_mirror_)model_copy->add_transformation(TransReverse::create());

			// append name
			model_copy->set_name(model_copy->get_name() + " B");

			// set color from parent
			if(use_custom_color_)model_copy->set_color(color_, use_custom_color_);

			// add model to list
			model_list.push_back(model_copy);
		}

		// walk over all split models
		for(auto it=model_list.begin();it!=model_list.end();it++){
			// add my transformations
			for(auto it2=trans_.begin();it2!=trans_.end();it2++)
				(*it)->add_transformation(slzr->copy((*it2).second));

			// create a copy of the conductor
			if(conductor_!=NULL)(*it)->set_input_conductor(slzr->copy(conductor_));

			// set tree closed
			(*it)->set_tree_open(false);
		}

		// return list of models
		return model_list;
	}

	// create calculation data objects
	std::list<ShMeshDataPr> ModelMirror::create_meshes_core(
		const std::list<arma::uword> &trace, 
		const MeshSettings &stngs)const{

		// create meshes to become mirror
		std::list<ShMeshDataPr> meshes = ModelGroup::create_meshes_core(trace, stngs);

		// walk over meshes
		for(auto it=meshes.begin();it!=meshes.end();it++){
			// apply mirror
			(*it)->apply_transformation(TransReflect::create(origin_, plane_vector_),stngs.time);

			// anti-mirror
			if(anti_mirror_)(*it)->apply_transformation(TransReverse::create(),stngs.time);

			// append name
			if(keep_original_)(*it)->append_name("B",true);
		}

		// also put original meshes
		if(keep_original_){
			// create meshes without reflection
			std::list<ShMeshDataPr> original_meshes = ModelGroup::create_meshes_core(trace, stngs);

			// append name
			for(auto it=original_meshes.begin();it!=original_meshes.end();it++)
				(*it)->append_name("A",true);

			// add to list
			meshes.splice(meshes.begin(), original_meshes);
		}

		// set conductor
		if(conductor_!=NULL)
			for(auto it=meshes.begin();it!=meshes.end();it++)
				(*it)->set_material(conductor_);

		// return meshes
		return meshes;
	}

	// serialization
	std::string ModelMirror::get_type(){
		return "rat::mdl::modelmirror";
	}

	// method for serialization into json
	void ModelMirror::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		ModelGroup::serialize(js,list);
		TransReflect::serialize(js,list);

		// type
		js["type"] = get_type();
		js["keep_original"] = keep_original_;
		js["anti_mirror"] = anti_mirror_;
	}

	// method for deserialisation from json
	void ModelMirror::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent objects
		ModelGroup::deserialize(js,list,factory_list,pth);
		TransReflect::deserialize(js,list,factory_list,pth);

		set_keep_original(js["keep_original"].asBool());
		set_anti_mirror(js["anti_mirror"].asBool());
	}

}}