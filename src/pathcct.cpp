// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathcct.hh"

// common headers
#include "rat/common/extra.hh"

// models headers
#include "darboux.hh"
#include "transbend.hh"
#include "pathbezier.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathCCT::PathCCT(){
		set_name("CCT");
	}

	// constructor with input
	PathCCT::PathCCT(const arma::uword num_poles, const fltp radius, const fltp a, 
		const fltp omega, const fltp num_turns, const arma::uword num_nodes_per_turn) : PathCCT(){

		// set to self
		set_num_poles(num_poles); set_radius(radius); set_amplitude(a);
		set_pitch(omega); set_num_turns(num_turns); set_num_nodes_per_turn(num_nodes_per_turn);
	}

	// constructor with input
	PathCCT::PathCCT(const arma::uword num_poles, const fltp radius, const fltp a, 
		const fltp omega, const fltp nt1, const fltp nt2, const arma::uword num_nodes_per_turn) : PathCCT(){

		// set to self
		set_num_poles(num_poles); set_radius(radius); set_amplitude(a);
		set_pitch(omega); set_nt1(nt1); set_nt2(nt2); set_num_nodes_per_turn(num_nodes_per_turn);
	}

	// factory
	ShPathCCTPr PathCCT::create(){
		return std::make_shared<PathCCT>();
	}

	// factory
	ShPathCCTPr PathCCT::create(const arma::uword num_poles, const fltp radius, const fltp a, 
		const fltp omega, const fltp num_turns, const arma::uword num_nodes_per_turn){
		return std::make_shared<PathCCT>(num_poles,radius,a,omega,num_turns,num_nodes_per_turn);
	}

	// factory
	ShPathCCTPr PathCCT::create(const arma::uword num_poles, const fltp radius, const fltp a, 
		const fltp omega, const fltp nt1, const fltp nt2, const arma::uword num_nodes_per_turn){
		return std::make_shared<PathCCT>(num_poles,radius,a,omega,nt1,nt2,num_nodes_per_turn);
	}

	// setters
	void PathCCT::set_is_reverse(const bool is_reverse){
		is_reverse_ = is_reverse;
	}

	void PathCCT::set_is_reverse_solenoid(const bool is_reverse_solenoid){
		is_reverse_solenoid_ = is_reverse_solenoid;
	}

	void PathCCT::set_is_solenoid(const bool is_solenoid){
		is_solenoid_ = is_solenoid;
	}

	void PathCCT::set_is_skew(const bool is_skew){
		is_skew_ = is_skew;
	}

	void PathCCT::set_bending_arc_length(const fltp bending_arc_length){
		bending_arc_length_ = bending_arc_length;
	}

	void PathCCT::set_twist(const fltp twist){
		twist_ = twist;
	}

	void PathCCT::set_use_skew_angle(const bool use_skew_angle){
		use_skew_angle_ = use_skew_angle;
	}

	void PathCCT::set_amplitude(const fltp a){
		a_ = a;
	}

	void PathCCT::set_alpha(const fltp alpha){
		alpha_ = alpha;
	}

	void PathCCT::set_normal_omega(const bool normal_omega){
		normal_omega_ = normal_omega;
	}

	void PathCCT::set_pitch(const fltp omega){
		omega_ = omega;
	}

	void PathCCT::set_num_turns(const fltp num_turns){
		nt1_ = -num_turns/2; nt2_ = num_turns/2;
	}

	void PathCCT::set_radius(const fltp radius){
		radius_ = radius;
	}

	void PathCCT::set_scale_x(const fltp scale_x){
		scale_x_ = scale_x;
	}

	void PathCCT::set_scale_y(const fltp scale_y){
		scale_y_ = scale_y;
	}

	void PathCCT::set_num_poles(const arma::uword num_poles){
		num_poles_ = num_poles;
	}

	void PathCCT::set_num_nodes_per_turn(const arma::uword num_nodes_per_turn){
		num_nodes_per_turn_ = num_nodes_per_turn;
	}

	void PathCCT::set_enable_lead1(const bool enable_lead1){
		enable_lead1_ = enable_lead1;
	}

	void PathCCT::set_enable_lead2(const bool enable_lead2){
		enable_lead2_ = enable_lead2;
	}

	void PathCCT::set_leadness1(const fltp leadness1){
		leadness1_ = leadness1;
	}

	void PathCCT::set_leadness2(const fltp leadness2){
		leadness2_ = leadness2;
	}

	void PathCCT::set_zlead1(const fltp zlead1){
		zlead1_ = zlead1;
	}

	void PathCCT::set_zlead2(const fltp zlead2){
		zlead2_ = zlead2;
	}

	void PathCCT::set_theta_lead1(const fltp theta_lead1){
		theta_lead1_ = theta_lead1;
	}

	void PathCCT::set_theta_lead2(const fltp theta_lead2){
		theta_lead2_ = theta_lead2;
	}

	void PathCCT::set_bending_origin(const bool bending_origin){
		bending_origin_ = bending_origin;
	}

	void PathCCT::set_use_radius(const bool use_radius){
		use_radius_ = use_radius;
	}

	void PathCCT::set_bending_radius(const fltp bending_radius){
		bending_radius_ = bending_radius;
	}

	void PathCCT::set_const_skew_angle(const bool const_skew_angle){
		const_skew_angle_ = const_skew_angle;
	}

	void PathCCT::set_is_rib(const bool is_rib){
		is_rib_ = is_rib;
	}

	void PathCCT::set_is_hardway(const bool is_hardway){
		is_hardway_ = is_hardway;
	}

	void PathCCT::set_nt1(const fltp nt1){
		nt1_ = nt1;
	}

	void PathCCT::set_nt2(const fltp nt2){
		nt2_ = nt2;
	}



	// getters
	bool PathCCT::get_is_reverse() const{
		return is_reverse_;
	}

	bool PathCCT::get_is_solenoid() const{
		return is_solenoid_;
	}

	bool PathCCT::get_is_reverse_solenoid()const{
		return is_reverse_solenoid_;
	}

	fltp PathCCT::get_bending_arc_length() const{
		return bending_arc_length_;
	}

	fltp PathCCT::get_twist()const{
		return twist_;
	}

	bool PathCCT::get_use_skew_angle()const{
		return use_skew_angle_;
	}

	fltp PathCCT::get_amplitude()const{
		return a_;
	}

	fltp PathCCT::get_alpha()const{
		return alpha_;
	}

	bool PathCCT::get_normal_omega()const{
		return normal_omega_;
	}


	fltp PathCCT::get_pitch()const{
		return omega_;
	}

	fltp PathCCT::get_radius()const{
		return radius_;
	}

	fltp PathCCT::get_scale_x()const{
		return scale_x_;
	}

	fltp PathCCT::get_scale_y()const{
		return scale_y_;
	}

	arma::uword PathCCT::get_num_poles()const{
		return num_poles_;
	}

	arma::uword PathCCT::get_num_nodes_per_turn() const{
		return num_nodes_per_turn_;
	}

	void PathCCT::set_num_layers(const arma::uword num_layers){
		num_layers_ = num_layers;
	}

	arma::uword PathCCT::get_num_layers() const{
		return num_layers_;
	}

	void PathCCT::set_rho_increment(const fltp rho_increment){
		rho_increment_ = rho_increment;
	}

	fltp PathCCT::get_rho_increment()const{
		return rho_increment_;
	}

	bool PathCCT::get_enable_lead1()const{
		return enable_lead1_;
	}
	
	bool PathCCT::get_enable_lead2()const{
		return enable_lead2_;
	}

	fltp PathCCT::get_leadness1()const{
		return leadness1_;
	}

	fltp PathCCT::get_leadness2()const{
		return leadness2_;
	}

	fltp PathCCT::get_zlead1()const{
		return zlead1_;
	}

	fltp PathCCT::get_zlead2()const{
		return zlead2_;
	}

	fltp PathCCT::get_theta_lead1()const{
		return theta_lead1_;
	}

	fltp PathCCT::get_theta_lead2()const{
		return theta_lead2_;
	}

	bool PathCCT::get_use_radius() const{
		return use_radius_;
	}

	bool PathCCT::get_bending_origin()const{
		return bending_origin_;
	}

	fltp PathCCT::get_bending_radius()const{
		return bending_radius_;
	}

	bool PathCCT::get_const_skew_angle()const{
		return const_skew_angle_;
	}

	bool PathCCT::get_is_skew()const{
		return is_skew_;
	}

	bool PathCCT::get_is_rib()const{
		return is_rib_;
	}

	bool PathCCT::get_is_hardway()const{
		return is_hardway_;
	}

	fltp PathCCT::get_nt1() const{
		return nt1_;
	}

	fltp PathCCT::get_nt2() const{
		return nt2_;
	}



	// compatibility with previous nturns definition
	fltp PathCCT::get_num_turns()const{
		return nt2_ - nt1_;
	}


	// get number of sectoins per turn
	arma::uword PathCCT::get_num_sect_per_turn()const{
		const arma::uword num_sect_per_pole = 4;
		const arma::uword num_sect_per_turn_min = 8; // Avoid FreeCAD 90 deg bending
		const arma::uword num_sect_per_turn = std::max(num_sect_per_turn_min, num_sect_per_pole*num_poles_);
		return num_sect_per_turn;
	}


	// setup coordinates and orientation vectors
	ShFramePr PathCCT::create_frame(const MeshSettings &stngs) const{
		// check validity
		is_valid(true);
		
		// check input
		if(radius_<=0)rat_throw_line("radius must be positive");
		if(num_poles_==0)rat_throw_line("number of poles must be positive");
		if(num_nodes_per_turn_==0)rat_throw_line("number of nodes per turn must be positive");

		// calculate number of sections per half
		const arma::uword num_sect_per_turn = get_num_sect_per_turn();
		const fltp sectsize = arma::Datum<fltp>::tau/num_sect_per_turn;

		//calculate radius
		const arma::Col<fltp> rho = arma::linspace<arma::Col<fltp> >(radius_, radius_ + (num_layers_-1)*rho_increment_, num_layers_);

		// get skew angle and amplitude based on input
		fltp a, alpha;
		if(use_skew_angle_){alpha = alpha_;	a = radius_*std::tan(alpha)/num_poles_;} else{a = a_; alpha = std::atan((num_poles_*a_)/radius_);}

		// calculate amplitude for each layer separately
		arma::Col<fltp> alayer(num_layers_);
		if(const_skew_angle_)alayer = (a/radius_)*rho; else alayer.fill(a);

		// calculate omega based on first layer
		const fltp omega = normal_omega_ ? omega_/std::cos(alpha) : omega_;

		// calculate maximum theta
		const fltp thetamin = nt1_*arma::Datum<fltp>::tau + (enable_lead1_ ? theta_lead1_ : RAT_CONST(0.0));
		const fltp thetamax = nt2_*arma::Datum<fltp>::tau - (enable_lead2_ ? theta_lead2_ : RAT_CONST(0.0));
		arma::sword num_int1 = arma::sword(std::floor(-thetamin/sectsize));
		arma::sword num_int2 = arma::sword(std::floor(thetamax/sectsize));
		if(std::abs(-thetamin-num_int1*sectsize)<RAT_CONST(1e-8))num_int1--;
		if(std::abs(thetamax-num_int2*sectsize)<RAT_CONST(1e-8))num_int2--;

		// calculate number of sections
		const arma::sword num_sections1 = num_int1+1;
		const arma::sword num_sections2 = num_int2+1;
		const arma::uword num_sections = num_sections1 + num_sections2;

		// allocate theta
		arma::Row<fltp> theta_start(num_sections);
		arma::Row<fltp> theta_end(num_sections);

		// set theta start positions
		theta_start(0) = thetamin;
		theta_start.cols(1,num_sections-1) = arma::linspace<arma::Row<fltp> >(-(num_int1*sectsize),num_int2*sectsize,num_sections-1);

		// set theta end positions
		theta_end.cols(0,num_sections-2) = arma::linspace<arma::Row<fltp> >(-(num_int1*sectsize),num_int2*sectsize,num_sections-1);
		theta_end(num_sections-1) = thetamax;

		// allocate list of frames
		std::list<ShFramePr> frame_list;

		// walk over layers
		for(arma::uword k=0;k<num_layers_;k++){
			// reverse
			// const bool rev = (int(is_reverse_) + int(!is_solenoid_)*k)%2==1;
			const bool rev = (int(is_reverse_) + k)%2==1;
			const fltp srev = rev ? -RAT_CONST(1.0) : RAT_CONST(1.0);

			// reverse winding
			const fltp dir = 
				(is_reverse_solenoid_ ? -RAT_CONST(1.0) : RAT_CONST(1.0))* // reverse the solenoidal winding direction
				(is_solenoid_ && k%2==1 ? -RAT_CONST(1.0) : RAT_CONST(1.0)); // reverse the winding direction for even layers when it is a solenoid

			// allocate
			arma::field<arma::Mat<fltp> > R(1,num_sections);
			arma::field<arma::Mat<fltp> > L(1,num_sections);
			arma::field<arma::Mat<fltp> > N(1,num_sections); 
			arma::field<arma::Mat<fltp> > D(1,num_sections);
			arma::field<arma::Row<fltp> > twist(1,num_sections);

			// allocate section and turn indexes
			arma::Row<arma::uword> section(num_sections);
			arma::Row<arma::uword> turn(num_sections); 

			// walk over sections
			cmn::parfor(0,num_sections,true,[&](arma::uword i, int /*cpu*/) {
			// for(arma::uword i=0;i<num_sections;i++){
				// calculate section
				const fltp theta1 = theta_start(i);
				const fltp theta2 = theta_end(i);
				assert(theta2>theta1);

				// calculate number of nodes
				arma::uword num_nodes = std::max(2llu,static_cast<arma::uword>(std::ceil(num_nodes_per_turn_*(theta2-theta1)/arma::Datum<fltp>::tau)));
				while((num_nodes-1)%stngs.element_divisor!=0)num_nodes++;

				// create theta (taking into account last section
				const arma::Row<fltp> theta = arma::linspace<arma::Row<fltp> >(theta1, theta2, num_nodes);

				// calculate expensive functions
				const arma::Row<fltp> cos_theta = arma::cos(theta);
				const arma::Row<fltp> sin_theta = arma::sin(theta);
				const arma::Row<fltp> cos_np_theta = arma::cos(fltp(num_poles_)*theta);
				const arma::Row<fltp> sin_np_theta = arma::sin(fltp(num_poles_)*theta);

				// create coordinates
				const arma::Row<fltp> x = scale_x_*rho(k)*cos_theta;
				const arma::Row<fltp> y = dir*scale_y_*rho(k)*sin_theta;
				//arma::Row<fltp> z = (radius_/(num_poles_*std::tan(calc_alpha())))*arma::sin(num_poles_*theta);
				arma::Row<fltp> z = alayer(k)*(is_skew_ ? cos_np_theta : sin_np_theta);

				// create direction vectors
				const arma::Row<fltp> vx = -scale_x_*rho(k)*sin_theta;
				const arma::Row<fltp> vy = dir*scale_y_*rho(k)*cos_theta;
				// arma::Row<fltp> vz = (num_poles_*(radius_/(num_poles_*std::tan(calc_alpha())))*arma::cos(num_poles_*theta) + 
				// 	num_poles_*omega/(2*arma::Datum<fltp>::pi))*(2*arma::Datum<fltp>::pi/num_nodes_per_turn_);
				arma::Row<fltp> vz = num_poles_*alayer(k)*(is_skew_ ? -sin_np_theta : cos_np_theta);

				// acceleration
				const arma::Row<fltp> ax = -scale_x_*rho(k)*cos_theta;
				const arma::Row<fltp> ay = -dir*scale_y_*rho(k)*sin_theta;
				arma::Row<fltp> az = alayer(k)*num_poles_*num_poles_*(is_skew_ ? -cos_np_theta : -sin_np_theta);

				// jerk
				const arma::Row<fltp> jx = scale_x_*rho(k)*sin_theta;
				const arma::Row<fltp> jy = -dir*scale_y_*rho(k)*cos_theta;
				arma::Row<fltp> jz = alayer(k)*num_poles_*num_poles_*num_poles_*(is_skew_ ? sin_np_theta : -cos_np_theta);

				// reverse axial direction if needed
				if(num_poles_%2==0){
					z *= -1; vz *= -1; az *= -1; jz *= -1;
				}

				// add turn increments
				z += srev*(omega/arma::Datum<fltp>::tau)*theta;
				vz += srev*omega/arma::Datum<fltp>::tau;


				// reverse axial direction if needed
				if(rev){
					z *= -1; vz *= -1; az *= -1; jz *= -1;
				}

				// create darboux frame
				// Darboux db(
				// 	arma::join_vert(vx,vy,vz),
				// 	arma::join_vert(ax,ay,az),
				// 	arma::join_vert(jx,jy,jz));
				// db.setup(true);

				// store position vectors
				// R(i).set_size(3,num_nodes);
				// R(i).row(0) = x; R(i).row(1) = y; R(i).row(2) = z;
				R(i) = arma::join_vert(x,y,z);

				// store longitudinal vectors
				L(i) = arma::join_vert(vx,vy,vz);

				// calculate radial vector
				arma::Mat<fltp> Vax(3,num_nodes,arma::fill::zeros);
				Vax.row(2).fill(RAT_CONST(1.0));
				D(i) = dir*cmn::Extra::cross(L(i),Vax);

				// create normal vectors
				N(i) = cmn::Extra::cross(L(i),D(i));
				
				// normalize
				L(i).each_row()/=cmn::Extra::vec_norm(L(i));
				D(i).each_row()/=cmn::Extra::vec_norm(D(i));
				N(i).each_row()/=cmn::Extra::vec_norm(N(i));

				// scale N
				if(is_rib_){
					const arma::Row<fltp> angle = arma::atan2(vz,arma::Row<fltp>(vz.n_elem,arma::fill::value(rho(k))));
					N(i).each_row()%=arma::cos(angle);
					N(i)-=(2.31/5.778)*(N(i).each_row()/cmn::Extra::vec_norm(N(i)));
				}

				// get darboux
				// L(i) = db.get_longitudinal();
				// D(i) = db.get_transverse();
				// N(i) = db.get_normal();

				
				// check size
				assert(R(i).n_rows==3);	assert(L(i).n_rows==3);
				assert(N(i).n_rows==3);	assert(D(i).n_rows==3);

				// set section and turn
				section(i) = i%num_sect_per_turn;
				turn(i) = i/num_sect_per_turn;

				// add aditional twist
				if(twist_!=0){
					twist(i) = arma::sin(fltp(num_poles_)*theta)*twist_;
					if(rev)twist(i)*=-1;
					const arma::Mat<fltp> Dtemp = D(i); const arma::Mat<fltp> Ntemp = N(i);
					const arma::Row<fltp> cos_twist = arma::cos(twist(i));
					const arma::Row<fltp> sin_twist = arma::sin(twist(i));
					D(i) = Dtemp.each_row()%cos_twist + Ntemp.each_row()%sin_twist;
					N(i) = Ntemp.each_row()%cos_twist - Dtemp.each_row()%sin_twist;
				}else{
					twist(i).zeros(num_nodes);
				}
			});

			// // reverse frame
			// if(is_reverse_){
			// 	cmn::Extra::reverse_field(R); cmn::Extra::reverse_field(L);
			// 	cmn::Extra::reverse_field(D); cmn::Extra::reverse_field(N);
			// 	for(arma::uword i=0;i<L.n_elem;i++){
			// 		L(i) *= -1; N(i) *= -1;
			// 	}
			// }

			// add start lead
			// calculate element size in the current leads
			// const fltp element_size = 2*2*arma::Datum<fltp>::pi*radius_/std::cos(calc_alpha())/num_nodes_per_turn_;

			// calculate element size in the current leads
			const fltp element_size = arma::as_scalar(cmn::Extra::vec_norm(R(0).col(1) - R(0).col(0)));

			// hardway version
			if(is_hardway_){std::swap(D,N); for(arma::uword i=0;i<num_sections;i++)D(i)*=-1;}

			// allocate list of frames
			std::list<ShFramePr> frame_sublist;

			// create layer jump to previous layer
			if(use_layer_jumps_ && k>0){
				const ShFramePr& prev_frame = frame_list.back();
				frame_sublist.push_back(create_layer_jump(
					prev_frame->get_coords(prev_frame->get_num_sections()-1).tail_cols(1), 
					prev_frame->get_direction(prev_frame->get_num_sections()-1).tail_cols(1),
					R(0).col(0), L(0).col(0), 0.03, 0.02, element_size));
			}

			// create current lead
			if(enable_lead1_ && (!use_layer_jumps_ || k==0))
				frame_sublist.push_back(create_current_lead(
					R(0).col(0), L(0).col(0), dir*theta_lead1_, 
					zlead1_, leadness1_, omega*nt1_/2, element_size, 
					false, arma::as_scalar(twist(0).col(0))));

			// check handedness
			#ifndef NDEBUG
			for(arma::uword i=0;i<N.n_elem;i++)
				assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N(i),L(i)),D(i))>RAT_CONST(0.0)));
			#endif
		
			// create the cct frame
			const ShFramePr cct_frame = Frame::create(R,L,N,D);

			// set section and turn indices
			cct_frame->set_location(section,turn,num_sections);

			// create regular frame
			frame_sublist.push_back(cct_frame);

			// create current lead
			if(enable_lead2_ && (!use_layer_jumps_ || k==num_layers_-1))
				frame_sublist.push_back(create_current_lead(
					R(R.n_elem-1).tail_cols(1), -L(L.n_elem-1).tail_cols(1), -dir*theta_lead2_, 
					-zlead2_, leadness2_, omega*nt2_/2, element_size, 
					true, arma::as_scalar(twist(twist.n_elem-1).tail_cols(1))));

			// combine coil and current leads
			const ShFramePr frame = Frame::create(frame_sublist);

			// bending with radius
			const fltp max_magnet_radius = radius_ + num_layers_*rho_increment_;
			if(use_radius_ && bending_radius_!=RAT_CONST(0.0)){
				const fltp sgn = cmn::Extra::sign(bending_radius_);
				frame->apply_transformation(TransBend::create({0,1,0},{1,0,0},bending_radius_-sgn*max_magnet_radius,-sgn*max_magnet_radius), stngs.time);
				if(bending_origin_)frame->apply_transformation(TransTranslate::create({bending_radius_,0,0}), stngs.time);
			}

			// bending with arc-length
			else if(!use_radius_ && bending_arc_length_!=RAT_CONST(0.0)){
				// check if the magnet has an actual length
				const fltp max_magnet_length = omega*(nt2_ - nt1_);
				if(max_magnet_length!=0){
					const fltp bend_radius = max_magnet_length/bending_arc_length_;
					const fltp sgn = cmn::Extra::sign(bend_radius);
					frame->apply_transformation(TransBend::create({0,1,0},{1,0,0},bend_radius,-sgn*max_magnet_radius), stngs.time);
					if(bending_origin_)frame->apply_transformation(TransTranslate::create({bend_radius+max_magnet_radius,0,0}), stngs.time);
				}
			}

			// reverse
			if(rev){frame->reverse(); frame->flip();}

			// add frame to list
			frame_list.push_back(frame);
		}

		// combine frames
		ShFramePr gen = Frame::create(frame_list);

		// transformations
		gen->apply_transformations(get_transformations(), stngs.time);

		// create frame
		return gen;
	}

	// function for creating current leads
	ShFramePr PathCCT::create_current_lead(
		const arma::Col<fltp>::fixed<3> &R0,
		const arma::Col<fltp>::fixed<3> &L0,
		const fltp theta_lead,
		const fltp zlead,
		const fltp leadness,
		const fltp z_end,
		const fltp element_size,
		const bool reverse,
		const fltp twist){

		// convert coordinate to cylindrical coordinates
		const fltp theta0 = std::atan2(R0(1),R0(0)); const fltp z0 = R0(2); 
		const fltp rho0 = std::sqrt(R0(0)*R0(0) + R0(1)*R0(1));

		// convert direction to cylindrical coordinates
		// const fltp vrho0 = L0(0)*std::cos(theta0) + L0(1)*std::sin(theta0);
		const fltp vtheta0 = -L0(0)*std::sin(theta0) + L0(1)*std::cos(theta0);
		const fltp vz0 = L0(2);

		// create points
		const fltp theta1 = theta0 - theta_lead; const fltp z1 = z_end - zlead; 
		const fltp theta2 = theta0 - theta_lead; const fltp z2 = z_end - RAT_CONST(0.6)*zlead;
		const fltp theta3 = theta0 - leadness*vtheta0/rho0; const fltp z3 = z0 - leadness*vz0;

		// combine points for bezier
		arma::Mat<fltp>::fixed<3,4> P{
			rho0,theta1*rho0,z1, rho0,theta2*rho0,z2,
			rho0,theta3*rho0,z3, rho0,theta0*rho0,z0};
		arma::Mat<fltp>::fixed<1,4> Pt{0.0,0.0,twist,twist};

		// reverse points
		if(reverse){P = arma::fliplr(P); Pt = arma::fliplr(Pt);}

		// get regular times in cylindrical coordinates
		const arma::Row<fltp> t = PathBezier::regular_times(P, element_size, 0.0, 500);

		// build bezier spline in cylindrical coordinates
		const arma::field<arma::Mat<fltp> > C = PathBezier::create_bezier_tn(t,arma::join_vert(P,Pt),2);
		const arma::Mat<fltp> Rleadcc = arma::join_vert(C(0).row(0), C(0).row(1)/rho0, C(0).row(2));
		const arma::Mat<fltp> Vleadcc = arma::join_vert(C(1).row(0), C(1).row(1)/rho0, C(1).row(2));
		const arma::Row<fltp> twisting = C(0).row(3);

		// convert back to carthesian coordinates
		arma::Mat<fltp> Rlead(3,t.n_elem);
		Rlead.row(0) = Rleadcc.row(0)%arma::cos(Rleadcc.row(1));
		Rlead.row(1) = Rleadcc.row(0)%arma::sin(Rleadcc.row(1));
		Rlead.row(2) = Rleadcc.row(2);

		arma::Mat<fltp> Vlead(3,t.n_elem);
		Vlead.row(0) = (-Vleadcc.row(1)%arma::sin(Rleadcc.row(1)) + Vleadcc.row(0)%arma::cos(Rleadcc.row(1)))*rho0;
		Vlead.row(1) = (Vleadcc.row(1)%arma::cos(Rleadcc.row(1)) + Vleadcc.row(0)%arma::sin(Rleadcc.row(1)))*rho0;
		Vlead.row(2) = Vleadcc.row(2);

		arma::Mat<fltp> Llead = Vlead.each_row()/cmn::Extra::vec_norm(Vlead);
		arma::Mat<fltp> Dlead = 
			arma::join_vert(Rlead.rows(0,1),arma::Row<fltp>(t.n_elem,arma::fill::zeros)).eval().each_row()/
			cmn::Extra::vec_norm(arma::join_vert(Rlead.rows(0,1),arma::Row<fltp>(t.n_elem,arma::fill::zeros)));
		arma::Mat<fltp> Nlead = cmn::Extra::cross(Llead,Dlead);

		// ensure attachment
		if(!reverse)Rlead.tail_cols(1) = R0; else Rlead.head_cols(1) = R0;
		if(!reverse)Llead.tail_cols(1) = L0; else Llead.head_cols(1) = -L0;

		// add twisting
		if(twist!=0){
			const arma::Mat<fltp> Dtemp = Dlead; const arma::Mat<fltp> Ntemp = Nlead;
			const arma::Row<fltp> cos_twist = arma::cos(twisting);
			const arma::Row<fltp> sin_twist = arma::sin(twisting);
			Dlead = Dtemp.each_row()%cos_twist + Ntemp.each_row()%sin_twist;
			Nlead = Ntemp.each_row()%cos_twist - Dtemp.each_row()%sin_twist;
		}

		// create frame
		return Frame::create(Rlead,Llead,Nlead,Dlead);
	}


	// function for creating current leads
	ShFramePr PathCCT::create_layer_jump(
		const arma::Col<fltp>::fixed<3> &R0,
		const arma::Col<fltp>::fixed<3> &L0,
		const arma::Col<fltp>::fixed<3> &R1,
		const arma::Col<fltp>::fixed<3> &L1,
		const fltp leadness0,
		const fltp leadness1,
		const fltp element_size){

		// convert coordinate to cylindrical coordinates
		const fltp theta0 = std::atan(R0(1)/R0(0)); const fltp z0 = R0(2); 
		const fltp rho0 = std::sqrt(R0(0)*R0(0) + R0(1)*R0(1));

		// convert direction to cylindrical coordinates
		// const fltp vrho0 = L0(0)*std::cos(theta0) + L0(1)*std::sin(theta0);
		const fltp vtheta0 = -L0(0)*std::sin(theta0) + L0(1)*std::cos(theta0);
		const fltp vz0 = L0(2);

		// convert coordinate to cylindrical coordinates
		const fltp theta1 = std::atan(R1(1)/R1(0)); const fltp z1 = R1(2); 
		const fltp rho1 = std::sqrt(R1(0)*R1(0) + R1(1)*R1(1));

		// convert direction to cylindrical coordinates
		// const fltp vrho1 = L1(0)*std::cos(theta1) + L1(1)*std::sin(theta1);
		const fltp vtheta1 = -L1(0)*std::sin(theta1) + L1(1)*std::cos(theta1);
		const fltp vz1 = L1(2);

		// create points
		const fltp theta2 = theta0 + leadness0*vtheta0/rho0; const fltp z2 = z0 + leadness0*vz0; const fltp rho2 = rho0;
		const fltp theta3 = theta1 - leadness1*vtheta1/rho1; const fltp z3 = z1 - leadness1*vz1; const fltp rho3 = rho1;

		// combine points for bezier
		arma::Mat<fltp>::fixed<3,4> P{
			rho0,theta0*rho0,z0, rho2,theta2*rho0,z2, 
			rho3,theta3*rho0,z3, rho1,theta1*rho0,z1}; 

		// get regular times in cylindrical coordinates
		const arma::Row<fltp> t = PathBezier::regular_times(P, element_size);

		// build bezier spline in cylindrical coordinates
		const arma::field<arma::Mat<fltp> > C = PathBezier::create_bezier_tn(t,P,2);
		const arma::Mat<fltp> Rleadcc = arma::join_vert(C(0).row(0), C(0).row(1)/rho0, C(0).row(2));
		const arma::Mat<fltp> Vleadcc = arma::join_vert(C(1).row(0), C(1).row(1)/rho0, C(1).row(2));

		// convert back to carthesian coordinates
		arma::Mat<fltp> Rlead(3,t.n_elem);
		Rlead.row(0) = Rleadcc.row(0)%arma::cos(Rleadcc.row(1));
		Rlead.row(1) = Rleadcc.row(0)%arma::sin(Rleadcc.row(1));
		Rlead.row(2) = Rleadcc.row(2);

		arma::Mat<fltp> Vlead(3,t.n_elem);
		Vlead.row(0) = (-Vleadcc.row(1)%arma::sin(Rleadcc.row(1)) + Vleadcc.row(0)%arma::cos(Rleadcc.row(1)))*rho0;
		Vlead.row(1) = (Vleadcc.row(1)%arma::cos(Rleadcc.row(1)) + Vleadcc.row(0)%arma::sin(Rleadcc.row(1)))*rho0;
		Vlead.row(2) = Vleadcc.row(2);

		const arma::Mat<fltp> Llead = Vlead.each_row()/cmn::Extra::vec_norm(Vlead);
		const arma::Mat<fltp> Dlead = 
			arma::join_vert(Rlead.rows(0,1),arma::Row<fltp>(t.n_elem,arma::fill::zeros)).eval().each_row()/
			cmn::Extra::vec_norm(arma::join_vert(Rlead.rows(0,1),arma::Row<fltp>(t.n_elem,arma::fill::zeros)));
		const arma::Mat<fltp> Nlead = cmn::Extra::cross(Llead,Dlead);

		// create frame
		return Frame::create(Rlead,Llead,Nlead,Dlead);
	}

	// check input
	bool PathCCT::is_valid(const bool enable_throws) const{
		// check input
		if(!Transformations::is_valid(enable_throws))return false;
		if(radius_<=0){if(enable_throws){rat_throw_line("radius must be larger than zero");} return false;};
		if(num_poles_<=0){if(enable_throws){rat_throw_line("number of poles must be larger than zero");} return false;};
		if(nt2_<=nt1_){if(enable_throws){rat_throw_line("number of turns must be larger than zero");} return false;};
		if(num_nodes_per_turn_==0){if(enable_throws){rat_throw_line("number of nodes per turn can not be zero");} return false;};
		if(scale_x_<=0){if(enable_throws){rat_throw_line("x scaling must be larger than zero");} return false;};
		if(scale_y_<=0){if(enable_throws){rat_throw_line("y scaling must be larger than zero");} return false;};
		if(num_layers_<=0){if(enable_throws){rat_throw_line("number of layers must be larger than zero");} return false;};
		if(enable_lead1_){
			if(leadness1_<=0){if(enable_throws){rat_throw_line("leadness for first lead must be larger than zero");} return false;};
		}
		if(enable_lead2_){
			if(leadness2_<=0){if(enable_throws){rat_throw_line("leadness for second lead must be larger than zero");} return false;};
		}
		const fltp thetamin = nt1_*arma::Datum<fltp>::pi + (enable_lead1_ ? theta_lead1_ : 0.0);
		const fltp thetamax = nt2_*arma::Datum<fltp>::pi - (enable_lead2_ ? theta_lead2_ : 0.0);
		if(thetamax<=thetamin){if(enable_throws){rat_throw_line("coil including leads will have negative number of turns");} return false;};
		return true;
	}

	// get type
	std::string PathCCT::get_type(){
		return "rat::mdl::pathcct";
	}

	// method for serialization into json
	void PathCCT::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents first 
		// this allows the type to be overridden
		Transformations::serialize(js,list);

		// settings
		js["type"] = get_type();
		js["is_skew"] = is_skew_;
		js["num_poles"] = static_cast<unsigned int>(num_poles_); 
		js["is_reverse"] = is_reverse_;
		js["is_solenoid"] = is_solenoid_;
		js["is_reverse_solenoid"] = is_reverse_solenoid_;
		js["radius"] = radius_;
		js["use_skew_angle"] = use_skew_angle_;
		js["amplitude"] = a_; 
		js["alpha"] = alpha_;
		js["normal_omega"] = normal_omega_; 
		js["omega"] = omega_; 
		js["nt1"] = nt1_; 
		js["nt2"] = nt2_; 
		js["num_nodes_per_turn"] = static_cast<unsigned int>(num_nodes_per_turn_); 
		js["twist"] = twist_; 
		js["bending_origin"] = bending_origin_;
		js["use_radius"] = use_radius_;
		js["bending_arc_length"] = bending_arc_length_;
		js["bending_radius"] = bending_radius_;
		js["scale_y"] = scale_y_;
		js["scale_x"] = scale_x_;
		js["num_layers"] = static_cast<int>(num_layers_);
		js["rho_increment"] = rho_increment_;
		js["const_skew_angle"] = const_skew_angle_;
		js["is_rib"] = is_rib_;
		js["is_hardway"] = is_hardway_;

		// lead settings
		js["enable_lead1"] = enable_lead1_;
		js["enable_lead2"] = enable_lead2_;
		js["leadness1"] = leadness1_;
		js["leadness2"] = leadness2_;
		js["zlead1"] = zlead1_;
		js["zlead2"] = zlead2_;
		js["theta_lead1"] = theta_lead1_;
		js["theta_lead2"] = theta_lead2_;
	}

	// method for deserialisation from json
	void PathCCT::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent objects
		Transformations::deserialize(js,list,factory_list,pth);

		// settings
		set_is_skew(js["is_skew"].asBool());
		set_num_poles(js["num_poles"].asUInt64()); 
		if(js.isMember("is_reverse_"))set_is_reverse(js["is_reverse_"].asBool()); // fix for typo V2.013.0
		else set_is_reverse(js["is_reverse"].asBool());
		set_is_reverse_solenoid(js["is_reverse_solenoid"].asBool());
		set_is_solenoid(js["is_solenoid"].asBool());
		set_radius(js["radius"].ASFLTP());
		set_use_skew_angle(js["use_skew_angle"].asBool());
		if(js.isMember("amplitude"))set_amplitude(js["amplitude"].ASFLTP()); 
		if(js.isMember("a"))set_amplitude(js["a"].ASFLTP()/num_poles_); // backward compatibility
		set_alpha(js["alpha"].ASFLTP());
		set_normal_omega(js["normal_omega"].asBool());
		set_pitch(js["omega"].ASFLTP());
		if(js.isMember("num_turns")){
			set_num_turns(js["num_turns"].ASFLTP()); 
		}else{
			set_nt1(js["nt1"].ASFLTP()); 
			set_nt2(js["nt2"].ASFLTP());
		}
		set_num_nodes_per_turn(js["num_nodes_per_turn"].asUInt64()); 
		set_twist(js["twist"].ASFLTP());
		set_bending_origin(js["bending_origin"].asBool());
		set_use_radius(js["use_radius"].asBool());
		set_bending_radius(js["bending_radius"].ASFLTP());
		set_bending_arc_length(js["bending_arc_length"].ASFLTP());
		set_scale_y(js["scale_y"].ASFLTP());
		set_scale_x(js["scale_x"].ASFLTP());
		set_num_layers(js["num_layers"].asUInt64());
		set_rho_increment(js["rho_increment"].ASFLTP());
		set_const_skew_angle(js["const_skew_angle"].asBool());
		set_is_rib(js["is_rib"].asBool());
		set_is_hardway(js["is_hardway"].asBool());

		// lead settings
		set_enable_lead1(js["enable_lead1"].asBool());
		set_enable_lead2(js["enable_lead2"].asBool());
		set_leadness1(js["leadness1"].ASFLTP());
		set_leadness2(js["leadness2"].ASFLTP());
		set_zlead1(js["zlead1"].ASFLTP());
		set_zlead2(js["zlead2"].ASFLTP());
		set_theta_lead1(js["theta_lead1"].ASFLTP());
		set_theta_lead2(js["theta_lead2"].ASFLTP());
	}

}}


