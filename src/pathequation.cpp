// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathequation.hh"

// rat-common headers
#include "rat/common/extra.hh"


// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathEquation::PathEquation(){

	}

	// constructor
	PathEquation::PathEquation(const fltp element_size, CoordFun fun){
		set_coord_fun(fun); set_element_size(element_size);
	}

	// factory
	ShPathEquationPr PathEquation::create(){
		return std::make_shared<PathEquation>();
	}

	// factory
	ShPathEquationPr PathEquation::create(const fltp element_size, CoordFun fun){
		return std::make_shared<PathEquation>(element_size, fun);
	}

	// set number of steps
	void PathEquation::set_num_steps_ini(const arma::uword num_steps_ini){
		num_steps_ini_ = num_steps_ini;
	}

	// set element size 
	void PathEquation::set_element_size(const fltp element_size){
		if(element_size<=0)rat_throw_line("element size must be larger than zero");
		element_size_ = element_size;
	}

	// set coordinate function
	void PathEquation::set_coord_fun(CoordFun fun){
		coord_fun_ = fun;
	}

	// set number of sections
	void PathEquation::set_num_sections(const arma::uword num_sections){
		if(num_sections<1)rat_throw_line("number of sections must be larger than one");
		num_sections_ = num_sections;
	}

	// set tolerance on length
	void PathEquation::set_ell_tol(const fltp ell_tol){
		ell_tol_ = ell_tol;
	}

	// setup coordinates and orientation vectors
	ShFramePr PathEquation::create_frame(const MeshSettings &stngs) const{
		if(element_size_==0)rat_throw_line("element size is not set");
		if(coord_fun_==NULL)rat_throw_line("coordinate function is not set");

		// calculate length
		const fltp ell = calc_length();

		// calculate number of times needed
		arma::uword num_times = static_cast<arma::uword>(std::ceil(static_cast<fltp>(std::max(2llu,static_cast<arma::uword>(std::ceil(ell/element_size_))))/num_sections_))+1;
		while((num_times-1)%stngs.element_divisor!=0)num_times++;
		num_times -= 1;
		num_times *= num_sections_;
		num_times += 1;

		// create times
		const arma::Row<fltp> t = arma::linspace<arma::Row<fltp> >(RAT_CONST(0.0),RAT_CONST(1.0),num_times);

		// allocate coordinates and direction
		arma::Mat<fltp> R, L, D;

		// calculate coordinates
		coord_fun_(R,L,D,t);

		// check output of user supplied coordinate function
		check_user_fun_output(R,L,D,t);

		// calculate normal vector
		const arma::Mat<fltp> N = cmn::Extra::cross(L,D);

		// check handedness
		assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N,L),D)>RAT_CONST(0.0)));
		
		// create frame
		const ShFramePr gen = Frame::create(R,L,N,D,num_sections_);

		// transformations
		gen->apply_transformations(get_transformations(), stngs.time);

		// create frame
		return gen;
	}

	// calculate length
	fltp PathEquation::calc_length() const{
		// start values
		fltp ell = 0;
		arma::uword num_steps = num_steps_ini_;

		// allocate coordinates and direction
		arma::Mat<fltp> R, L, D;

		// keep iterations untill length constant
		for(arma::uword i=0;i<max_iter_;i++){
			// create times
			const arma::Row<fltp> t = arma::linspace<arma::Row<fltp> >(RAT_CONST(0.0),RAT_CONST(1.0),num_steps);

			// calculate coordinates
			coord_fun_(R,L,D,t);

			// check output of user supplied coordinate function
			check_user_fun_output(R,L,D,t);

			// numerically calculate length
			fltp ell_new = arma::accu(
				cmn::Extra::vec_norm(R.tail_cols(num_steps-1) - 
				R.head_cols(num_steps-1)));

			// check stop criteion
			if(std::abs(ell-ell_new)<ell_tol_)break;

			// fltp number of steps and update ell
			num_steps*=2; ell = ell_new;
		}

		// return calculated length
		return ell;
	}

	// check output of user supplied coordinate function
	// no we do not thrust the user at all
	void PathEquation::check_user_fun_output(
		const arma::Mat<fltp> &R, const arma::Mat<fltp> &L, 
		const arma::Mat<fltp> &D, const arma::Row<fltp> &t) const{ 
		const arma::uword num_steps = t.n_elem;
		if(R.n_rows!=3)rat_throw_line("coordinate matrix must have three rows");
		if(L.n_rows!=3)rat_throw_line("longitudinal vector matrix must have three rows");
		if(D.n_rows!=3)rat_throw_line("transverse vector matrix must have three rows");
		if(R.n_cols!=num_steps)rat_throw_line("coordinate matrix must match number of steps");
		if(L.n_cols!=num_steps)rat_throw_line("longitudinal vector matrix must match number of steps");
		if(D.n_cols!=num_steps)rat_throw_line("transverse vector matrix must match number of steps");
		if(!R.is_finite())rat_throw_line("coordinate matrix must be finite");
		if(!L.is_finite())rat_throw_line("longitudinal vector matrix must be finite");
		if(!D.is_finite())rat_throw_line("transverse vector matrix must be finite");
	}

	// get type
	std::string PathEquation::get_type(){
		return "rat::mdl::pathequation";
	}

	// method for serialization into json
	void PathEquation::serialize(Json::Value &/*js*/, cmn::SList &/*list*/) const{
		rat_throw_line("due to its nature PathEquation in its current form can not be serialized");
	}

	// method for deserialisation from json
	void PathEquation::deserialize(const Json::Value &/*js*/, cmn::DSList &/*list*/, const cmn::NodeFactoryMap &/*factory_list*/, const boost::filesystem::path &/*pth*/){
		rat_throw_line("due to its nature PathEquation in its current form can not be serialized");
	}


}}