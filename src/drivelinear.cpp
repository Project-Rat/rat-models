// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "drivelinear.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	DriveLinear::DriveLinear(){
		set_name("Linear");
	}

	// constructor
	DriveLinear::DriveLinear(const fltp offset, const fltp slope) : DriveLinear(){
		set_offset(offset); set_slope(slope);
	}

	// factory
	ShDriveLinearPr DriveLinear::create(){
		return std::make_shared<DriveLinear>();
	}

	// factory
	ShDriveLinearPr DriveLinear::create(const fltp offset, const fltp slope){
		return std::make_shared<DriveLinear>(offset,slope);
	}


	// setters
	void DriveLinear::set_offset(const fltp offset){
		offset_ = offset;
	}

	void DriveLinear::set_slope(const fltp slope){
		slope_ = slope;
	}


	// getters
	fltp DriveLinear::get_offset() const{
		return offset_;
	}

	fltp DriveLinear::get_slope() const{
		return slope_;
	}

	// get current
	fltp DriveLinear::get_scaling(
		const fltp position,
		const fltp /*time*/,
		const arma::uword derivative) const{

		// value
		if(derivative==0)return slope_*position + offset_;

		// first derivative
		if(derivative==1)return slope_;

		// default
		return RAT_CONST(0.0);
	}

	// apply scaling for the input settings
	void DriveLinear::rescale(const fltp scale_factor){
		slope_ *= scale_factor;
		offset_ *= scale_factor;
	}

	// get type
	std::string DriveLinear::get_type(){
		return "rat::mdl::drivelinear";
	}

	// method for serialization into json
	void DriveLinear::serialize(Json::Value &js, cmn::SList &list) const{
		Drive::serialize(js,list);
		js["type"] = get_type();
		js["slope"] = slope_;
		js["offset"] = offset_;
	}

	// method for deserialisation from json
	void DriveLinear::deserialize(const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		Drive::deserialize(js,list,factory_list,pth);
		set_offset(js["offset"].ASFLTP());
		set_slope(js["slope"].ASFLTP());
	}

}}