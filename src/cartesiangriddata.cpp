// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "cartesiangriddata.hh"

// common headers
#include "rat/common/marchingcubes.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CartesianGridData::CartesianGridData(){
		// set for magnetic field calculation
		set_field_type('H',3); set_output_type("grid");
	}

	// constructor
	CartesianGridData::CartesianGridData(
		const fltp x1, const fltp x2, const arma::uword num_x, 
		const fltp y1, const fltp y2, const arma::uword num_y, 
		const fltp z1, const fltp z2, const arma::uword num_z){
		// set dimensions
		set_dim_x(x1,x2,num_x);	set_dim_y(y1,y2,num_y);	set_dim_z(z1,z2,num_z);
		
		// set for magnetic field calculation
		set_field_type('B',3); set_output_type("grid");
	}

	// constructor automatically enclosing meshes
	CartesianGridData::CartesianGridData(
		const std::list<ShMeshDataPr> &meshes, 
		const fltp scale_factor, 
		const fltp delem, 
		const arma::uword lim_num_targets){
		
		// output type
		set_output_type("grid");

		// set for magnetic field calculation
		set_field_type('B',3);
		
		// get bounds of meshes
		arma::Mat<fltp> Rl(3,meshes.size());
		arma::Mat<fltp> Ru(3,meshes.size());
		arma::uword idx =0;
		for(auto it=meshes.begin();it!=meshes.end();it++,idx++){
			Rl.col(idx) = (*it)->get_lower_bound();
			Ru.col(idx) = (*it)->get_upper_bound();
		}

		// get lowest lower bound and highest higher upper bound
		const arma::Col<fltp>::fixed<3> Rll = arma::min(Rl,1); 
		const arma::Col<fltp>::fixed<3> Ruu = arma::max(Ru,1);

		// find midpoint
		const arma::Col<fltp>::fixed<3> Rmid = (Rll + Ruu)/2;

		// scale from midpoint
		const arma::Col<fltp>::fixed<3> Rsll = Rmid + scale_factor*(Rll-Rmid);
		const arma::Col<fltp>::fixed<3> Rsuu = Rmid + scale_factor*(Ruu-Rmid);
		
		// calculate the number of nodes in each direction
		arma::Col<fltp> num_nodes = (Rsuu-Rsll)/delem + 1;

		// if the number of target nodes exceeds the limit
		// then adjust the grid
		fltp num_target_points = arma::prod(num_nodes);
		if(num_target_points>lim_num_targets)
 			num_nodes *= std::pow(lim_num_targets/num_target_points, 1.0/3);

		// take the ceil
		num_nodes = arma::ceil(num_nodes);

		// set dimensions
		set_dim_x(Rsll(0), Rsuu(0), num_nodes(0));
		set_dim_y(Rsll(1), Rsuu(1), num_nodes(1));
		set_dim_z(Rsll(2), Rsuu(2), num_nodes(2));
	}

	// factory
	ShCartesianGridDataPr CartesianGridData::create(){
		return std::make_shared<CartesianGridData>();
	}

	// factory
	ShCartesianGridDataPr CartesianGridData::create(
		const fltp x1, const fltp x2, const arma::uword num_x, 
		const fltp y1, const fltp y2, const arma::uword num_y, 
		const fltp z1, const fltp z2, const arma::uword num_z){
		return std::make_shared<CartesianGridData>(x1,x2,num_x,y1,y2,num_y,z1,z2,num_z);
	}

	// factory
	ShCartesianGridDataPr CartesianGridData::create(
		const std::list<ShMeshDataPr> &meshes, const fltp scale_factor, 
		const fltp delem, const arma::uword lim_num_elements){
		return std::make_shared<CartesianGridData>(meshes, scale_factor, delem, lim_num_elements);
	}

	// is integrated flag
	void CartesianGridData::set_is_integrated(const bool is_integrated){
		is_integrated_ = is_integrated;
	}

	// set using polar coordinates
	void CartesianGridData::set_is_polar(const bool is_polar){
		is_polar_ = is_polar;
	}

	// set dimensions for x
	void CartesianGridData::set_dim_x(const fltp x1, const fltp x2, const arma::uword num_x){
		if(x1>=x2 && num_x!=1)rat_throw_line("x1 must be smaller than x2");
		if(num_x<=0)rat_throw_line("number of elements in x must be larger than zero");
		x1_ = x1; x2_ = x2; num_x_ = num_x;
	}

	// set dimensions for y
	void CartesianGridData::set_dim_y(const fltp y1, const fltp y2, const arma::uword num_y){
		if(y1>=y2 && num_y!=1)rat_throw_line("y1 must be smaller than y2");
		if(num_y<=0)rat_throw_line("number of elements in y must be larger than zero");
		y1_ = y1; y2_ = y2; num_y_ = num_y;
	}

	// set dimensions for z
	void CartesianGridData::set_dim_z(const fltp z1, const fltp z2, const arma::uword num_z){
		if(z1>=z2 && num_z!=1)rat_throw_line("z1 must be smaller than z2");
		if(num_z<=0)rat_throw_line("number of elements in z must be larger than zero");
		z1_ = z1; z2_ = z2; num_z_ = num_z;
	}

	// setup function
	void CartesianGridData::setup_targets(){
		// check input
		if(num_x_==0)rat_throw_line("number of elements in x is not set");
		if(num_y_==0)rat_throw_line("number of elements in y is not set");
		if(num_z_==0)rat_throw_line("number of elements in z is not set");
		if(x1_>=x2_ && num_x_!=1)rat_throw_line("dimensions in x not set");
		if(y1_>=y2_ && num_y_!=1)rat_throw_line("dimensions in y not set");
		if(z1_>=z2_ && num_z_!=1)rat_throw_line("dimensions in z not set");

		// arrays defining coordinates
		const arma::Row<fltp> xa = arma::linspace<arma::Row<fltp> >(x1_,x2_,num_x_);
		const arma::Row<fltp> ya = arma::linspace<arma::Row<fltp> >(y1_,y2_,num_y_);
		const arma::Row<fltp> za = arma::linspace<arma::Row<fltp> >(z1_,z2_,num_z_);
		
		// allocate coordinates
		Rt_.set_size(3,num_x_*num_y_*num_z_);

		// walk over x
		for(arma::uword i=0;i<num_z_;i++){
			for(arma::uword j=0;j<num_y_;j++){
				// calculate indexes
				const arma::uword idx1 = i*num_x_*num_y_ + j*num_x_;
				const arma::uword idx2 = i*num_x_*num_y_ + (j+1)*num_x_ - 1;

				// set coordinates
				Rt_.submat(0,idx1,0,idx2) = xa;
				Rt_.submat(1,idx1,1,idx2).fill(ya(j));
				Rt_.submat(2,idx1,2,idx2).fill(za(i));
			}
		}

		// set number of targets
		num_targets_ = Rt_.n_cols;
	}

	// get is integrated flag
	bool CartesianGridData::get_is_integrated()const{
		return is_integrated_;
	}

	// using polar coordinates
	bool CartesianGridData::get_is_polar()const{
		return is_polar_;
	}

	// get grid data
	arma::uword CartesianGridData::get_num_x() const{
		return num_x_;
	}

	// get grid data
	arma::uword CartesianGridData::get_num_y() const{
		return num_y_;
	}
	
	// get grid data
	arma::uword CartesianGridData::get_num_z() const{
		return num_z_;
	}

	// get grid data
	fltp CartesianGridData::get_x1() const{
		return x1_;
	}

	// get grid data
	fltp CartesianGridData::get_x2() const{
		return x2_;
	}

	// get grid data
	fltp CartesianGridData::get_y1() const{
		return y1_;
	}
	
	// get grid data
	fltp CartesianGridData::get_y2() const{
		return y2_;
	}

	// get grid data
	fltp CartesianGridData::get_z1() const{
		return z1_;
	}
	
	// get grid data
	fltp CartesianGridData::get_z2() const{
		return z2_;
	}

	// interpolate
	arma::Mat<fltp> CartesianGridData::interpolate(
		const arma::Mat<fltp> &values,
		const arma::Mat<fltp> &R, 
		const bool use_parallel) const{

		// check field
		assert(!values.has_nan());
		assert(values.is_finite());

		// get grid spacing
		const fltp dx = (x2_ - x1_)/(num_x_-1);
		const fltp dy = (y2_ - y1_)/(num_y_-1);
		const fltp dz = (z2_ - z1_)/(num_z_-1);

		// interpolated values
		arma::Mat<fltp> interpolated_values(values.n_rows, R.n_cols);

		// walk over coordinates
		// walk over meshes
		cmn::parfor(0,R.n_cols,use_parallel,[&](int i, int /*cpu*/){
			// get coordinate and index of lower corner
			const fltp rx = R(0,i) - x1_, ry = R(1,i) - y1_, rz = R(2,i) - z1_;
			const arma::uword xi = std::min(num_x_-2,arma::uword(rx/dx));
			const arma::uword yi = std::min(num_y_-2,arma::uword(ry/dy)); 
			const arma::uword zi = std::min(num_z_-2,arma::uword(rz/dz));
			
			// calculate relative position in box
			const arma::Col<fltp>::fixed<3> Rq{
			 	2*(rx - xi*dx)/dx - RAT_CONST(1.0),
				2*(ry - yi*dy)/dy - RAT_CONST(1.0), 
				2*(rz - zi*dz)/dz - RAT_CONST(1.0)};

			// check indexes
			assert(xi<num_x_-1); assert(yi<num_y_-1); assert(zi<num_z_-1);

			// values at corners
			arma::Mat<fltp>::fixed<3,8> V;
			V.col(0) = values.col(zi*num_x_*num_y_ + yi*num_x_ + xi);
			V.col(1) = values.col(zi*num_x_*num_y_ + yi*num_x_ + xi+1);
			V.col(2) = values.col(zi*num_x_*num_y_ + (yi+1)*num_x_ + xi+1);
			V.col(3) = values.col(zi*num_x_*num_y_ + (yi+1)*num_x_ + xi);
			V.col(4) = values.col((zi+1)*num_x_*num_y_ + yi*num_x_ + xi);
			V.col(5) = values.col((zi+1)*num_x_*num_y_ + yi*num_x_ + xi+1);
			V.col(6) = values.col((zi+1)*num_x_*num_y_ + (yi+1)*num_x_ + xi+1);
			V.col(7) = values.col((zi+1)*num_x_*num_y_ + (yi+1)*num_x_ + xi);

			// interpolate
			interpolated_values.col(i) = cmn::Hexahedron::quad2cart(V,Rq);
		});

		// check field
		assert(!interpolated_values.has_nan());
		assert(interpolated_values.is_finite());
		assert(interpolated_values.n_cols==R.n_cols);
		assert(interpolated_values.n_rows==values.n_rows);

		// return interpolated values
		return interpolated_values;
	}

	// extract iso-surfaces
	ShMeshDataPr CartesianGridData::extract_isosurface(
		const arma::Row<fltp> &values,
		const fltp iso_value, 
		const bool use_parallel) const{

		// check values
		if(values.n_elem!=num_x_*num_y_*num_z_)
			rat_throw_line("number of values does not match number of nodes in grid");

		// // get values
		// arma::Row<fltp> values;
		// switch(component){
		// 	case 'x': values = get_field(field_type).row(0); break;
		// 	case 'y': values = get_field(field_type).row(1); break;
		// 	case 'z': values = get_field(field_type).row(2); break;
		// 	case 'm': values = cmn::Extra::vec_norm(get_field(field_type)); break;
		// 	default: rat_throw_line("component not recognized");
		// }

		// run marching cubes algorithm
		const cmn::ShMarchingCubesPr marching = cmn::MarchingCubes::create();
		marching->setup_hex2isotri();
		arma::Mat<rat::fltp> nodes = marching->polygonise(
			x1_,x2_,num_x_,y1_,y2_,num_y_,z1_,z2_,num_z_,values,iso_value);

		// create elements
		arma::Mat<arma::uword> elements = arma::reshape(
			arma::regspace<arma::Col<arma::uword> >(0,nodes.n_cols-1),3,nodes.n_cols/3);

		// combine nodes 
		const arma::Row<arma::uword> idx = cmn::Extra::combine_nodes(nodes);

		// re-index elements
		elements = arma::reshape(idx(arma::vectorise(elements)),elements.n_rows,elements.n_cols);

		// get rid of sliver elements
		elements = elements.cols(arma::find(cmn::Triangle::calc_area(nodes,elements)>0));

		// export to VTK
		const ShMeshDataPr md = MeshData::create(nodes, elements, elements);

		// clear the field type list of the surface
		md->clear_field_type();

		// transfer field
		if(has_field()){
			// transfer list of field types
			for(auto it = field_type_.begin();it!=field_type_.end();it++)
				md->add_field_type((*it).first, (*it).second);
		}

		// allocate surface to take the data
		md->setup_targets();
		md->allocate(); 

		// transfer the data
		for(auto it = field_type_.begin();it!=field_type_.end();it++){
			arma::Mat<fltp> fld = interpolate(get_field((*it).first), md->get_nodes(), use_parallel);
			md->add_field((*it).first, fld);
		}

		// output meshdata
		return md;
	}

	// create elements
	arma::Mat<arma::uword> CartesianGridData::create_elements()const{
		// create 2D element indices
		const arma::Mat<arma::uword> node_idx = arma::reshape(arma::regspace<arma::Col<arma::uword> >(0,num_x_*num_y_-1), num_x_, num_y_);
		
		// create 2D elements
		const arma::uword num_node_plane = num_x_*num_y_;
		const arma::uword num_element_plane = (num_x_-1)*(num_y_-1);
		arma::Mat<arma::uword> n2d(4,num_element_plane);
		for(arma::uword k=0;k<num_y_-1;k++)
			for(arma::uword j=0;j<num_x_-1;j++)
				n2d.col(k*(num_x_-1)+j) = arma::Col<arma::uword>::fixed<4>{
					node_idx(k,j), node_idx(k,j+1), node_idx(k+1,j+1), node_idx(k+1,j)};

		// create 3D elements
		const arma::uword num_volume = num_element_plane*(num_z_-1);
		arma::Mat<arma::uword> n(8,num_volume);
		for(arma::uword i=0;i<num_z_-1;i++)
			n.cols(i*num_element_plane, (i+1)*num_element_plane-1) = arma::join_vert(n2d+i*num_node_plane, n2d+(i+1)*num_node_plane);

		// return element connectivity matrix
		return n;
	}

	// write surface to VTK image file
	ShVTKObjPr CartesianGridData::export_vtk() const{
		// create VTK file
		const ShVTKImgPr vtk_data = VTKImg::create();

		// calculate spacing
		vtk_data->set_dims(x1_,x2_,num_x_, y1_,y2_,num_y_, z1_,z2_,num_z_);

		// set name
		vtk_data->set_name(get_name());

		// magnetic vector potential
		if(has('A') && has_field())vtk_data->set_nodedata(get_field('A'),is_integrated_ ? "Int. Vector Potential [Vs]" : "Vector Potential [Vs/m]");

		// magnetic flux density
		if(has('B') && has_field())vtk_data->set_nodedata(get_field('B'),is_integrated_ ? "Int. Mgn. Flux Density [Tm]" : "Mgn. Flux Density [T]");

		// magnetic field
		if(has('H') && has_field())vtk_data->set_nodedata(get_field('H'),is_integrated_ ? "Int. Magnetic Field [A]" : "Magnetic Field [A/m]");

		// magnetisation
		if(has('M') && has_field())vtk_data->set_nodedata(get_field('M'),is_integrated_ ? "Int. Magnetisation [A]" : "Magnetisation [A/m]");

		// return data object
		return vtk_data;
	}

	// display function
	void CartesianGridData::display(const cmn::ShLogPr &lg){
	// header
		lg->msg(2,"%s%sVOLUME GRID%s\n",KBLD,KGRN,KNRM);
		
		// display settings
		lg->msg(2,"%sSettings%s\n",KBLU,KNRM);
		lg->msg("grid size in x: %s%llu%s\n",KYEL,num_x_,KNRM);
		lg->msg("grid size in y: %s%llu%s\n",KYEL,num_y_,KNRM);
		lg->msg("grid size in z: %s%llu%s\n",KYEL,num_z_,KNRM);
		lg->msg("range in x: %s%2.2f - %2.2f%s\n",KYEL,x1_,x2_,KNRM);
		lg->msg("range in y: %s%2.2f - %2.2f%s\n",KYEL,y1_,y2_,KNRM);
		lg->msg("range in z: %s%2.2f - %2.2f%s\n",KYEL,z1_,z2_,KNRM);
		lg->msg("number of target coordinates: %s%llu%s\n",KYEL,num_x_*num_y_*num_z_,KNRM);
		lg->msg(-2,"\n");

		// done
		lg->msg(-2);
	}

}}