// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathstraight.hh"

// rat-common headers
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathStraight::PathStraight(){
		set_name("Straight");
	}

	// constructor with specification
	PathStraight::PathStraight(const fltp length, const fltp element_size) : PathStraight(){
		// set to self using respective set methods
		set_length(length); set_element_size(element_size);
	}

	// factory
	ShPathStraightPr PathStraight::create(){
		return std::make_shared<PathStraight>();
	}

	// constructor with dimensions and element size
	ShPathStraightPr PathStraight::create(const fltp length, const fltp element_size){
		return std::make_shared<PathStraight>(length,element_size);
	}

	// set path offset
	void PathStraight::set_offset(const fltp offset){
		offset_ = offset;
	}

	// set length
	void PathStraight::set_length(const fltp length){
		
		length_ = length;
	}

	// set element size
	void PathStraight::set_element_size(const fltp element_size){
		
		element_size_ = element_size;
	}

	// set length
	fltp PathStraight::get_length()const{
		return length_;
	}

	// set element size
	fltp PathStraight::get_element_size()const{
		return element_size_;
	}



	// darboux generator function
	ShFramePr PathStraight::create_frame(const MeshSettings &stngs) const{
		// check input
		is_valid(true);

		// calculate number of nodes for this section
		arma::uword num_nodes = std::max(2llu,static_cast<arma::uword>(std::ceil(length_/element_size_)+1));
		while((num_nodes-1)%stngs.element_divisor!=0)num_nodes++;

		// allocate coordinates
		arma::Mat<fltp> R(3,num_nodes,arma::fill::zeros);
		arma::Mat<fltp> L(3,num_nodes,arma::fill::zeros);
		arma::Mat<fltp> N(3,num_nodes,arma::fill::zeros);
		arma::Mat<fltp> D(3,num_nodes,arma::fill::zeros);

		// set vectors for line along x-axis
		R.row(1) = arma::linspace<arma::Row<fltp> >(0,length_,num_nodes);
		L.row(1).fill(RAT_CONST(1.0));	// along y
		N.row(0).fill(RAT_CONST(1.0));	// along x
		D.row(2).fill(RAT_CONST(1.0)); // along z

		// check handedness
		assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N,L),D)>RAT_CONST(0.0)));

		// create frame and return
		return Frame::create(R,L,N,D);
	}

	// validity check
	bool PathStraight::is_valid(const bool enable_throws) const{
		if(length_<=0){if(enable_throws){rat_throw_line("length must be larger than zero");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		if(length_/element_size_>1e5){if(enable_throws){rat_throw_line("element size too small");} return false;};
		return true;
	}

	// get type
	std::string PathStraight::get_type(){
		return "rat::mdl::pathstraight";
	}

	// method for serialization into json
	void PathStraight::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		Path::serialize(js,list);

		// type
		js["type"] = get_type();
		js["length"] = length_;
		js["element_size"] = element_size_;
		// js["element_divisor"] = (int)element_divisor_;
		js["offset"] = offset_;
	}

	// method for deserialisation from json
	void PathStraight::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent objects
		Path::deserialize(js,list,factory_list,pth);

		// deserialize the models
		length_ = js["length"].ASFLTP();
		element_size_ = js["element_size"].ASFLTP();
		// element_divisor_ = js["element_divisor"].asUInt64();
		offset_ = js["offset"].ASFLTP();
	}


}}