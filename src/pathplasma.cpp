// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathplasma.hh"

// rat-common headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// rat-models headers
#include "pathequation.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathPlasma::PathPlasma(){
		set_name("Plasma");
	}

	// constructor with shape input
	PathPlasma::PathPlasma(
		const arma::uword num_sections, const fltp a, const fltp r0,
		const fltp delta, const fltp kappa, const fltp element_size) : PathPlasma(){

		// set to self
		set_num_sections(num_sections);
		set_a(a); set_r0(r0); set_delta(delta); set_kappa(kappa);
		set_element_size(element_size);
	}

	// factory
	ShPathPlasmaPr PathPlasma::create(){
		return std::make_shared<PathPlasma>();
	}

	// factory
	ShPathPlasmaPr PathPlasma::create(
		const arma::uword num_sections, const fltp a, const fltp r0, 
		const fltp delta, const fltp kappa, const fltp element_size){
		return std::make_shared<PathPlasma>(num_sections,a,r0,delta,kappa,element_size);
	}


	// set plasma parameters
	void PathPlasma::set_r0(const fltp r0){
		r0_ = r0;
	}

	void PathPlasma::set_a(const fltp a){
		a_ = a;
	}

	void PathPlasma::set_delta(const fltp delta){
		delta_ = delta;
	}

	void PathPlasma::set_kappa(const fltp kappa){
		kappa_ = kappa;
	}

	// // set divisor
	// void PathPlasma::set_element_divisor(const arma::uword element_divisor){
	// 	element_divisor_ = element_divisor;
	// }

	// set element size
	void PathPlasma::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	// set range for theta
	void PathPlasma::set_theta(const fltp theta1, const fltp theta2){
		theta1_ = theta1; theta2_ = theta2;
	}

	void PathPlasma::set_theta1(const fltp theta1){
		theta1_ = theta1;
	}

	void PathPlasma::set_theta2(const fltp theta2){
		theta2_ = theta2;
	}

	// set number of sections
	void PathPlasma::set_num_sections(const arma::uword num_sections){
		num_sections_ = num_sections;
	}
	
	// set velocity
	void PathPlasma::set_velocity(const fltp velocity){
		velocity_ = velocity;
	}

	
	// set plasma parameters
	fltp PathPlasma::get_r0() const{
		return r0_;
	}

	fltp PathPlasma::get_a() const{
		return a_;
	}

	fltp PathPlasma::get_delta() const{
		return delta_;
	}

	fltp PathPlasma::get_kappa() const{
		return kappa_;
	}

	// set element size
	fltp PathPlasma::get_element_size() const{
		return element_size_;
	}

	// set number of sections
	arma::uword PathPlasma::get_num_sections() const{
		return num_sections_;
	}

	fltp PathPlasma::get_theta1() const{
		return theta1_;
	}

	fltp PathPlasma::get_theta2() const{
		return theta2_;
	}

	// set velocity
	fltp PathPlasma::get_velocity()const{
		return velocity_;
	}

	// calculate distance from origin
	fltp PathPlasma::calc_origin_distance() const{
		fltp theta = arma::Datum<fltp>::pi;
		return r0_ + (a_*std::cos(theta + delta_*std::sin(theta)));
	}

	// calculate inboard radius from axial coordinate
	arma::Row<rat::fltp> PathPlasma::calc_inboard_radius(const arma::Row<fltp>& z){
		const arma::Row<rat::fltp> theta = arma::Datum<rat::fltp>::pi + arma::asin(z/(kappa_*a_));
		const arma::Row<rat::fltp> radius = r0_ + a_*arma::cos(theta + delta_*arma::sin(theta));
		return radius;
	}

	// get frame
	mdl::ShFramePr PathPlasma::create_frame(const MeshSettings &stngs) const{
		// check input
		is_valid(true);

		// provided by TE for the inner shape of their coils
		const mdl::ShPathEquationPr path_dshape = mdl::PathEquation::create(element_size_, [=](
			arma::Mat<fltp> &R, arma::Mat<fltp> &L, 
			arma::Mat<fltp> &D, const arma::Row<fltp> &t){

			// calculate in cylindrical
			const arma::Row<fltp> theta = theta1_ + t*(theta2_-theta1_);

			// convert to carthesian coordinates
			R.set_size(3,t.n_elem);
			R.row(0) = r0_ + (a_*arma::cos(theta + delta_*arma::sin(theta)));
			R.row(1) = kappa_*a_*arma::sin(theta);
			R.row(2).fill(0);

			// calculate
			D.set_size(3,t.n_elem);
			D.row(0).fill(0); D.row(1).fill(0); D.row(2).fill(1);

			// rotation around axis
			if(velocity_!=0){
				fltp alpha = RAT_CONST(0.0);
				for(arma::uword i=0;i<theta.n_elem;i++){
					const fltp radius = R(0,i);
					const arma::Mat<fltp>::fixed<3,3> M = cmn::Extra::create_rotation_matrix({0,1,0},alpha);
					R.col(i) = M*R.col(i); D.col(i) = M*D.col(i);
					const fltp dalpha = ((theta2_-theta1_)/t.n_elem)*(velocity_/(arma::Datum<fltp>::tau*radius));
					alpha += dalpha;
				}
				L = arma::diff(R,1,1); L = (arma::join_horiz(L.head_cols(1),L) + arma::join_horiz(L,L.tail_cols(1)))/2; 
				L.each_row()/=cmn::Extra::vec_norm(L);
			}

			else{
				// calculate direction (using derivative)
				L.set_size(3,t.n_elem);
				L.row(0) = -a_*arma::sin(theta + delta_*arma::sin(theta))%(delta_*arma::cos(theta) + RAT_CONST(1.0));
				L.row(1) = kappa_*a_*arma::cos(theta);
				L.row(2).fill(0);
				L.each_row()/=cmn::Extra::vec_norm(L);
			}

		});

		path_dshape->set_ell_tol(1e-2);

		// set element divisor
		// path_dshape->set_element_divisor(element_divisor_);
		path_dshape->set_num_sections(num_sections_);

		// create frame
		mdl::ShFramePr frame = path_dshape->create_frame(stngs);

		// apply transformations
		frame->apply_transformations(get_transformations(), stngs.time);

		// return frame
		return frame;
	}

	// validity check
	bool PathPlasma::is_valid(const bool enable_throws) const{
		if(r0_<=0){if(enable_throws){rat_throw_line("major radius must be larger than zero");} return false;};
		if(a_<=0){if(enable_throws){rat_throw_line("minor radius must be larger than zero");} return false;};
		// if(delta_<=0){if(enable_throws){rat_throw_line("triangularity must be larger than zero");} return false;};
		if(kappa_<=0){if(enable_throws){rat_throw_line("kappa must be larger than zero");} return false;};
		// if(element_divisor_<=0){if(enable_throws){rat_throw_line("element divisor must be larger than zero");} return false;};
		if(element_size_<=1e-12){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		if(theta2_<=theta1_){if(enable_throws){rat_throw_line("theta1 must be smaller than theta2");} return false;};
		if(num_sections_==0){if(enable_throws){rat_throw_line("number of sections must be larger than zero");} return false;};
		return true;
	}


	// get type
	std::string PathPlasma::get_type(){
		return "rat::mdl::pathplasma";
	}

	// method for serialization into json
	void PathPlasma::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize
		Path::serialize(js,list);
		Transformations::serialize(js,list);

		// type
		js["type"] = get_type();
		js["ro"] = r0_; js["a"] = a_; js["delta"] = delta_; js["kappa"] = kappa_;
		js["element_size"] = element_size_;
		js["theta1"] = theta1_;
		js["theta2"] = theta2_;
		js["velocity"] = velocity_;
		// js["element_divisor"] = (int)element_divisor_;
		js["num_sections"] = static_cast<unsigned int>(num_sections_);
	}

	// method for deserialisation from json
	void PathPlasma::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent objects
		Path::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);
		
		r0_ = js["ro"].ASFLTP(); a_ = js["a"].ASFLTP();
		delta_ = js["delta"].ASFLTP(); kappa_ = js["kappa"].ASFLTP();
		element_size_ = js["element_size"].ASFLTP();
		theta1_ = js["theta1"].ASFLTP();
		theta2_ = js["theta2"].ASFLTP();
		velocity_ = js["velocity"].ASFLTP();
		element_size_ = js["element_size"].ASFLTP();
		// element_divisor_ = js["element_divisor"].asUInt64();
		num_sections_ = js["num_sections"].asUInt64();
	}

}}