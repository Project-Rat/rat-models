// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "vtktable.hh"

// rat common headers
#include "rat/common/error.hh"

// code specific to Rat
namespace rat{namespace mdl{

	VTKTable::VTKTable(const arma::uword num_rows){
		set_num_rows(num_rows);
	}

	ShVTKTablePr VTKTable::create(const arma::uword num_rows){
		return std::make_shared<VTKTable>(num_rows);
	}

	// set number of rows
	void VTKTable::set_num_rows(const arma::uword num_rows){
		num_rows_ = num_rows;
	}

	// write data at nodes
	void VTKTable::set_data(const arma::Col<fltp> &v, const std::string &name){
		if(v.n_elem!=num_rows_)rat_throw_line("length of dataset does not match table: " + name + " " + std::to_string(v.n_elem) + "/" + std::to_string(num_rows_));
		float_data_[name] = v;
	}
	
	// write data at nodes
	void VTKTable::set_data(const arma::field<std::string> &v, const std::string &name){
		if(v.n_elem!=num_rows_)rat_throw_line("number of rows of dataset does not match table: " + name + " " + std::to_string(v.n_elem) + "/" + std::to_string(num_rows_));
		string_data_[name] = v;
	}

	// get filename extension
	std::string VTKTable::get_filename_ext() const{
		return "vtt";
	}

	// write output file
	void VTKTable::write(const boost::filesystem::path fname, const cmn::ShLogPr& lg){
		// report
		lg->msg(2,"%sVTK Table: %s%s\n",KBLU,get_name().c_str(),KNRM);

		// create a VTK table object
		vtkSmartPointer<vtkTable> vtk_table = vtkSmartPointer<vtkTable>::New();

		// write mesh
		vtk_table->SetNumberOfRows(num_rows_);

		// temporary storage
		std::list<vtkSmartPointer<vtkDoubleArray> > vtk_numeric_data;
		std::list<vtkSmartPointer<vtkStringArray> > vtk_text_data;

		// walk over float data
		lg->msg("converting float data\n");
		for(auto it=float_data_.begin();it!=float_data_.end();it++){
			// get data references from iterator
			const std::string& name = (*it).first;
			const arma::Col<fltp>& v = (*it).second;

			// create VTK array
			vtkSmartPointer<vtkDoubleArray> vvtk = vtkDoubleArray::New();
			vtk_numeric_data.push_back(vvtk);

			// set data name	
			vvtk->SetName(name.c_str());
			
			// allocate
			vvtk->SetNumberOfComponents(1); 
			vvtk->SetNumberOfValues(v.n_elem);

			// copy data into array
			for(arma::uword i=0;i<v.n_elem;i++)
				vvtk->SetValue(i,v(i));

			// add to VTK
			vtk_table->AddColumn(vvtk);
		}

		// walk over float data
		lg->msg("converting text data\n");
		for(auto it=string_data_.begin();it!=string_data_.end();it++){
			// get data references from iterator
			const std::string& name = (*it).first;
			const arma::field<std::string>& v = (*it).second;

			// create VTK array
			vtkSmartPointer<vtkStringArray> vvtk = vtkStringArray::New();
			vtk_text_data.push_back(vvtk);

			// set data name
			vvtk->SetName(name.c_str());

			// allocate
			vvtk->SetNumberOfComponents(1); 
			vvtk->SetNumberOfValues(v.n_elem);

			// copy data into array
			for(arma::uword i=0;i<v.n_elem;i++)
				vvtk->SetValue(i,v(i));

			// add to VTK
			vtk_table->AddColumn(vvtk);
		}

		// report
		lg->msg("writing table\n");
		
		// display file settings and type
		std::string ext = get_filename_ext();
		boost::filesystem::path fname_ext = fname;
		fname_ext.replace_extension(ext);
		lg->msg("filename: %s%s%s\n",KYEL,fname_ext.string().c_str(),KNRM);
		lg->msg("type: table (%s)\n",ext.c_str());

		// create writer
		vtkSmartPointer<vtkXMLTableWriter> writer =  
			vtkSmartPointer<vtkXMLTableWriter>::New();
		writer->SetFileName(fname_ext.string().c_str());
		writer->SetInputData(vtk_table);
		
		// write data
		writer->Write();

		// done writing VTK table
		lg->msg(-2,"\n");
	}

}}