// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "transformations.hh"

// rat-common headers
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	Transformations::Transformations(){

	}

	// factory
	ShTransformationsPr Transformations::create(){
		return std::make_shared<Transformations>();
	}

	// add transformation
	arma::uword Transformations::add_transformation(const ShTransPr &trans){
		// check input
		assert(trans!=NULL); 

		// index
		const arma::uword index = trans_.size()+1;

		// add to list
		trans_.insert({index,trans}); 

		// return the index
		return index;
	}	

	// get transformation with specific index
	ShTransPr Transformations::get_transformation(const arma::uword index) const{
		auto it=trans_.find(index);
		if(it==trans_.end())rat_throw_line("index does not exist: " + std::to_string(index));
		return (*it).second;
	}

	// get transformation with specific index
	std::list<ShTransPr> Transformations::get_transformations() const{
		std::list<ShTransPr> trans_list;
		for(auto it=trans_.begin();it!=trans_.end();it++)
			trans_list.push_back((*it).second);
		return trans_list;
	}

	// delete a transformation
	bool Transformations::delete_transformation(const arma::uword index){
		auto it=trans_.find(index);
		if(it==trans_.end())return false;
		(*it).second = NULL; return true;
	}

	// get number fo transformations
	arma::uword Transformations::num_transformations() const{
		return trans_.size();
	}

	// combine function cleans up transformations
	void Transformations::clear_transformations(){
		trans_.clear();
	}

	// add list of transformations
	arma::Row<arma::uword> Transformations::add_transformations(const std::list<ShTransPr> &trans_list){
		arma::Row<arma::uword> indices(trans_list.size());
		arma::uword idx = 0;
		for(auto it=trans_list.begin();it!=trans_list.end();it++,idx++)
			indices(idx) = add_transformation(*it);
		return indices;
	}

	// rotation arround vector
	void Transformations::add_rotation(const arma::Col<fltp>::fixed<3> &V, const fltp alpha){
		add_transformation(TransRotate::create(V,alpha));
	}

	// rotation arround vector
	void Transformations::add_rotation(const arma::Col<fltp>::fixed<3> &O, const arma::Col<fltp>::fixed<3> &V, const fltp alpha, const arma::Col<fltp>::fixed<3> &offset){
		add_transformation(TransRotate::create(O,V,alpha,offset));
	}

	// add a translation to this object
	void Transformations::add_translation(const fltp dx, const fltp dy, const fltp dz){
		add_transformation(TransTranslate::create(dx,dy,dz));
	}

	// add a translation to this object
	void Transformations::add_translation(const arma::Col<fltp>::fixed<3> &dR){
		add_transformation(TransTranslate::create(dR));
	}

	// rotation around x, y and z axis respectively
	void Transformations::add_rotation(const fltp phi, const fltp theta, const fltp psi){
		add_transformation(TransRotate::create(1,0,0,phi));
		add_transformation(TransRotate::create(0,1,0,theta));
		add_transformation(TransRotate::create(0,0,1,psi));
	}

	// rotation arround vector
	void Transformations::add_rotation(const fltp ux, const fltp uy, const fltp uz, const fltp alpha){
		add_transformation(TransRotate::create(ux,uy,uz,alpha));
	}

	// rotation arround vector
	void Transformations::add_reflect_xy(){
		add_transformation(TransReflect::create({0,0,1}));
	}

	// rotation arround vector
	void Transformations::add_reflect_yz(){
		add_transformation(TransReflect::create({1,0,0}));
	}

	// rotation arround vector
	void Transformations::add_reflect_xz(){
		add_transformation(TransReflect::create({0,1,0}));
	}

	// add full reverse of the path
	void Transformations::add_reverse(){
		add_transformation(TransReverse::create());
	}

	// flip cable
	void Transformations::add_flip(){
		add_transformation(TransFlip::create());
	}

	// combine function cleans up transformations
	void Transformations::combine_transformations(){
		// keep track of matrix and vector
		arma::Mat<fltp>::fixed<3,3> M = arma::diagmat(arma::Col<fltp>::fixed<3>(arma::fill::ones));
		arma::Col<fltp>::fixed<3> V = cmn::Extra::null_vec(); 
		arma::uword cnt = 0;

		// create a list of transformations
		std::list<ShTransPr> trans_list;

		// walk over transformations
		for(auto it=trans_.begin();it!=trans_.end();it++){
			// try cast to translation and rotation
			mdl::ShTransTranslatePr translation = 
				std::dynamic_pointer_cast<TransTranslate>((*it).second);
			mdl::ShTransRotatePr rotation = 
				std::dynamic_pointer_cast<TransRotate>((*it).second);

			// translation add to vector
			if(translation!=NULL){
				if(arma::all(translation->get_velocity()==0)){
					V += translation->get_vector(); cnt++;
				}else{
					trans_list.push_back((*it).second);
				}
			}

			// rotation
			else if(rotation!=NULL){
				if(rotation->get_valpha()==0){
					const fltp time = RAT_CONST(0.0);
					const arma::Mat<fltp>::fixed<3,3> matrix = rotation->create_rotation_matrix(time);
					const arma::Col<fltp>::fixed<3> origin = rotation->get_origin();
					const arma::Col<fltp>::fixed<3> offset = rotation->get_offset();
					M = matrix*M; V = matrix*(V - origin) + origin + offset; cnt++;
				}else{
					trans_list.push_back((*it).second);
				}
			}

			// neither
			else if(cnt!=0){
				// create combined rotation
				arma::Col<fltp>::fixed<4> V4 = cmn::Extra::rotmatrix2angle(M);

				// add to list of transformations
				trans_list.push_back(TransRotate::create(cmn::Extra::null_vec(),V4.rows(0,2),V4(3),V));

				// add the transformation that 
				// doesnt match translation or rotation
				trans_list.push_back((*it).second);

				// reset counter
				cnt = 0;
			}else{
				trans_list.push_back((*it).second);
			}
		}

		if(cnt!=0){
			// create combined rotation
			arma::Col<fltp>::fixed<4> V4 = cmn::Extra::rotmatrix2angle(M);

			// add to list of transformations
			trans_list.push_back(TransRotate::create(cmn::Extra::null_vec(),V4.rows(0,2),V4(3),V));
		}

		// clear all transformations
		clear_transformations();

		// add the new combined list
		add_transformations(trans_list);
	}

	// check validity
	bool Transformations::is_valid(const bool enable_throws) const{
		for(auto it=trans_.begin();it!=trans_.end();it++){
			const ShTransPr &trans = (*it).second;
			if(trans==NULL)rat_throw_line("transformation list contains NULL");
			if(!trans->is_valid(enable_throws))return false;
		}
		return true;
	}

	// reindex
	void Transformations::reindex(){
		std::map<arma::uword, ShTransPr> new_trans; arma::uword idx = 1;
		for(auto it=trans_.begin();it!=trans_.end();it++)
			if((*it).second!=NULL)new_trans.insert({idx++, (*it).second});
		trans_ = new_trans;
	}

	// get type
	std::string Transformations::get_type(){
		return "rat::mdl::transformations";
	}

	// method for serialization into json
	void Transformations::serialize(Json::Value &js, cmn::SList &list) const{
		// set type
		Node::serialize(js,list);
		js["type"] = get_type();

		// transformations
		for(auto it=trans_.begin();it!=trans_.end();it++)
			js["trans"].append(cmn::Node::serialize_node((*it).second, list));
		
	}

	// method for deserialisation from json
	void Transformations::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		Node::deserialize(js,list,factory_list,pth);
		for(auto it=js["trans"].begin();it!=js["trans"].end();it++)
			add_transformation(cmn::Node::deserialize_node<Trans>(*it, list, factory_list, pth));
	}
}}