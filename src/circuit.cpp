// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "circuit.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	Circuit::Circuit(){
		set_name("Circuit");
		set_circuit_id(0);
		set_current_drive(DriveDC::create(0.0));
	}

	Circuit::Circuit(const arma::uword id, const ShDrivePr &current_drive) : Circuit(){
		set_circuit_id(id);
		set_current_drive(current_drive);
	}

	// factory
	ShCircuitPr Circuit::create(){
		return std::make_shared<Circuit>();
	}

	ShCircuitPr Circuit::create(const arma::uword id, const ShDrivePr &current_drive){
		return std::make_shared<Circuit>(id, current_drive);
	}


	// set current drive
	void Circuit::set_current_drive(const ShDrivePr &current_drive){
		if(current_drive==NULL)rat_throw_line("drive points to NULL");
		current_drive_ = current_drive;
	}

	// get current drive
	ShDrivePr Circuit::get_current_drive()const{
		return current_drive_;
	}

	// set circuit index
	void Circuit::set_circuit_id(const arma::uword circuit_id){
		circuit_id_ = circuit_id;
	}

	// get circuit index
	arma::uword Circuit::get_circuit_id()const{
		return circuit_id_;
	}

	// get current as function of time
	fltp Circuit::get_current(const fltp time)const{
		return current_drive_->get_scaling(time);
	}

	// get current change 
	fltp Circuit::get_dcurrent(const fltp time)const{
		return current_drive_->get_scaling(time,0.0,1);
	}

	// validity check
	bool Circuit::is_valid(const bool enable_throws) const{
		if(circuit_id_<=0){if(enable_throws){rat_throw_line("circuit id must be larger than zero");} return false;};
		return true;
	}

	// protection
	fltp Circuit::get_detection_voltage()const{
		return RAT_CONST(1e99);
	}

	fltp Circuit::get_detection_delay_time()const{
		return RAT_CONST(1e99);
	}

	fltp Circuit::get_heater_delay_time()const{
		return RAT_CONST(1e99);
	}

	fltp Circuit::get_switching_time()const{
		return RAT_CONST(1e99);
	}

	fltp Circuit::calc_protection_voltage(const fltp /*current*/)const{
		return RAT_CONST(0.0);
	}

	fltp Circuit::get_fraction_hit()const{
		return RAT_CONST(0.0);
	}


	fltp Circuit::get_transverse_propagation_fraction()const{
		return RAT_CONST(0.0);
	}
	
	bool Circuit::get_is_rapid()const{
		return false;
	}

	// get type
	std::string Circuit::get_type(){
		return "rat::mdl::circuit";
	}

	// method for serialization into json
	void Circuit::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Node::serialize(js,list);

		// my type
		js["type"] = get_type();

		// id
		js["circuit_id"] = static_cast<int>(circuit_id_);

		// current drive
		js["current_drive"] = cmn::Node::serialize_node(current_drive_, list);

		// // protection settings
		// // for future quench analysis
		// // dump resistor [Ohm]
		// fltp dump_resistance_;

		// // detection treshold voltage [V]
		// fltp detection_treshold_;

		// // delay time [s]
		// fltp delay_time_;
	}

	// method for deserialisation from json
	void Circuit::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		
		// parent
		Node::deserialize(js,list,factory_list,pth);

		// set id
		set_circuit_id(js["circuit_id"].asUInt64());

		// current drive
		set_current_drive(cmn::Node::deserialize_node<Drive>(js["current_drive"], list, factory_list, pth));
	}


}}