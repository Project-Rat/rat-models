// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "trans.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// mesh transformation
	void Trans::apply_coords(
		arma::Mat<fltp> &/*R*/, 
		const fltp /*time*/) const{

		// nothing happens
		return;
	}

	// vector transformation
	void Trans::apply_vectors(
		const arma::Mat<fltp> &/*R*/, 
		arma::Mat<fltp> &/*V*/, 
		const char /*str*/,
		const fltp /*time*/) const{

		// nothing happens
		return;
	}

	// mesh transformation
	void Trans::apply_mesh(
		arma::Mat<arma::uword> &/*n*/, 
		const arma::uword /*n_dim*/,
		const arma::uword /*num_nodes*/, 
		const fltp /*time*/) const{

		// nothing happens
		return;
	}

	// mesh transformation
	void Trans::apply_area_coords(
		arma::Mat<fltp> &/*R*/, 
		const fltp /*time*/) const{

		// nothing happens
		return;
	}

	// mesh transformation
	void Trans::apply_area_mesh(
		arma::Mat<arma::uword> &/*n*/, 
		const arma::uword /*num_nodes*/, 
		const fltp /*time*/) const{

		// nothing happens
		return;
	}

	// coordinate transformation for field array
	void Trans::apply_coords(
		arma::field<arma::Mat<fltp> > &R, 
		const fltp time) const{
		for(arma::uword i=0;i<R.n_elem;i++)
			apply_coords(R(i),time);
	}

	// vector transformation for field array
	void Trans::apply_vectors(
		const arma::field<arma::Mat<fltp> > &R, 
		arma::field<arma::Mat<fltp> > &V, 
		const char str, 
		const fltp time) const{
		assert(!R.is_empty()); 
		assert(!V.is_empty());
		assert(R.n_elem==V.n_elem);

		// walk over sections
		for(arma::uword i=0;i<R.n_elem;i++)
			apply_vectors(R(i),V(i),str,time);
	}


	// get type
	std::string Trans::get_type(){
		return "rat::mdl::trans";
	}

	// method for serialization into json
	void Trans::serialize(
		Json::Value &js, 
		cmn::SList &list) const{
		Node::serialize(js,list);
		js["type"] = get_type();
	}

	// method for deserialisation from json
	void Trans::deserialize(
		const Json::Value &js,
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		Node::deserialize(js,list,factory_list,pth);
	}

}}