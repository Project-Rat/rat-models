// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "inputhbcurve.hh"

// only available for non-linear solver
#ifdef ENABLE_NL_SOLVER

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	InputHBCurve::InputHBCurve(const ShHBCurvePr &hb_curve){
		set_hb_curve(hb_curve);
	}

	// setters
	void InputHBCurve::set_hb_curve(const ShHBCurvePr& hb_curve){
		hb_curve_ = hb_curve;
	}
	// getters
	const ShHBCurvePr& InputHBCurve::get_hb_curve()const{
		return hb_curve_;
	}

	// is valid
	bool InputHBCurve::is_valid(const bool enable_throws) const{		
		if(!hb_curve_->is_valid(enable_throws))return false;
		return true;
	}

	// get type
	std::string InputHBCurve::get_type(){
		return "rat::mdl::inputhbcurve";
	}

	// method for serialization into json
	void InputHBCurve::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		Node::serialize(js,list);

		// type
		js["type"] = get_type();
		
		// serialize path
		js["hb_curve"] = cmn::Node::serialize_node(hb_curve_, list);
	}

	// method for deserialisation from json
	void InputHBCurve::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent objects
		Node::deserialize(js,list,factory_list,pth);

		// deserialize path
		set_hb_curve(cmn::Node::deserialize_node<HBCurve>(js["hb_curve"], list, factory_list, pth));
	}


}}

#endif