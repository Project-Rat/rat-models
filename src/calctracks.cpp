// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calctracks.hh"

// nonlinear solver headers
#ifdef ENABLE_NL_SOLVER
#include "rat/nl/nonlinsolver.hh"
#endif

// mlfmm headers
#include "rat/mlfmm/ilist.hh"
#include "rat/mlfmm/trackinggrid.hh"
#include "rat/mlfmm/nodelevel.hh"
#include "rat/mlfmm/settings.hh"
#include "rat/mlfmm/multisources.hh"
#include "rat/mlfmm/multitargets2.hh"

// model headers
#include "nldata.hh"
#include "coildata.hh"
#include "bardata.hh"
#include "extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// track calculations
	CalcTracks::CalcTracks(){
		set_name("Particle Tracks");
		tracking_model_ = ModelGroup::create();
		tracking_model_->set_name("Tracking Geometry");

		// special refinement settings 
		// to ensure grid boxes exist
		stngs_->set_num_refine(50);
		stngs_->set_refine_stop_criterion(rat::fmm::RefineStopCriterion::EITHER);
		stngs_->set_memory_efficient_l2t(true);

		// default tracking settings
		track_type_ = TrackingType::RELATIVISTIC;
		enable_dynamic_ = false;
		max_step_size_ = RAT_CONST(0.05);
		rabstol_ = RAT_CONST(1e-6); // m
		pabstol_ = RAT_CONST(10.0); // eV/C
		rreltol_ = RAT_CONST(1e-4);
		preltol_ = RAT_CONST(1e-4);
	}

	CalcTracks::CalcTracks(
		const ShModelPr &model) : CalcTracks(){
		set_model(model);
	}	

	CalcTracks::CalcTracks(
		const ShModelPr &model, 
		const ShModelPr &tracking_model, 
		const std::list<ShEmitterPr> &emitters) : CalcTracks(model){
		set_tracking_model(tracking_model);
		add_emitters(emitters);
	}

	ShCalcTracksPr CalcTracks::create(){
		return std::make_shared<CalcTracks>();
	}

	ShCalcTracksPr CalcTracks::create(const ShModelPr &model){
		return std::make_shared<CalcTracks>(model);
	}

	ShCalcTracksPr CalcTracks::create(
		const ShModelPr &model, 
		const ShModelPr &tracking_model, 
		const std::list<ShEmitterPr> &emitters){
		return std::make_shared<CalcTracks>(model, tracking_model, emitters);
	}

	// get mesh enabled
	bool CalcTracks::get_visibility() const{
		return visibility_;
	}

	// set mesh enabled
	void CalcTracks::set_visibility(const bool visibility){
		visibility_ = visibility;
	}

	// set the absolute tracking tolerance
	void CalcTracks::set_rabstol(const fltp rabstol){
		rabstol_ = rabstol;
	}

	// set the relative tracking tolerance
	void CalcTracks::set_rreltol(const fltp rreltol){
		rreltol_ = rreltol;
	}

	// set the absolute tracking tolerance
	void CalcTracks::set_pabstol(const fltp pabstol){
		pabstol_ = pabstol;
	}

	// set the relative tracking tolerance
	void CalcTracks::set_preltol(const fltp preltol){
		preltol_ = preltol;
	}

	// set the maximum step size
	void CalcTracks::set_max_step_size(const fltp max_step_size){
		max_step_size_ = max_step_size;
	}

	// set the integration method (see trackdata)
	void CalcTracks::set_integration_method(
		const cmn::RungeKutta::IntegrationMethod integration_method){
		integration_method_ = integration_method;
	}

	// get the integration method
	cmn::RungeKutta::IntegrationMethod CalcTracks::get_integration_method()const{
		return integration_method_;
	}

	cmn::RungeKutta::IntegrationMethod* CalcTracks::access_integration_method(){
		return &integration_method_;
	}

	// set the maximum step size
	fltp CalcTracks::get_max_step_size()const{
		return max_step_size_;
	}


	// get the absolute tracking tolerance
	fltp CalcTracks::get_rabstol()const{
		return rabstol_;
	}

	// get the relative tracking tolerance
	fltp CalcTracks::get_rreltol()const{
		return rreltol_;
	}

	// get the absolute tracking tolerance
	fltp CalcTracks::get_pabstol()const{
		return pabstol_;
	}

	// get the relative tracking tolerance
	fltp CalcTracks::get_preltol()const{
		return preltol_;
	}

	// add emitter
	arma::uword CalcTracks::add_emitter(const ShEmitterPr &emitter){
		const arma::uword index = emitters_.size()+1;
		emitters_.insert({index,emitter}); return index;
	}

	// add multiple emitters
	arma::Row<arma::uword> CalcTracks::add_emitters(const std::list<ShEmitterPr> &emitters){
		arma::Row<arma::uword> idx(emitters.size()); arma::uword i=0;
		for(auto it = emitters.begin();it!=emitters.end();it++,i++){
			idx(i) = add_emitter(*it);
		}
		return idx;
	}

	// delete emitter at index
	bool CalcTracks::delete_emitter(const arma::uword index){	
		auto it = emitters_.find(index);
		if(it==emitters_.end())return false;
		(*it).second = NULL; return true;
	}

	// number of emitters
	arma::uword CalcTracks::num_emitters() const{
		return emitters_.size();
	}

	// get emitter
	ShEmitterPr CalcTracks::get_emitter(const arma::uword index)const{
		auto it = emitters_.find(index);
		if(it==emitters_.end())rat_throw_line("index does not exist");
		return (*it).second;
	}

	// get list of all emitters
	std::list<ShEmitterPr> CalcTracks::get_emitters() const{
		std::list<ShEmitterPr> emitter_list;
		for(auto it=emitters_.begin();it!=emitters_.end();it++)
			emitter_list.push_back((*it).second);
		return emitter_list;
	}

	// re-index nodes after deleting
	void CalcTracks::reindex(){
		CalcLeaf::reindex();
		std::map<arma::uword, ShEmitterPr> new_emitters; arma::uword idx = 1;
		for(auto it=emitters_.begin();it!=emitters_.end();it++)
			if((*it).second!=NULL)new_emitters.insert({idx++, (*it).second});
		emitters_ = new_emitters;
	}

	// set model to define the shape of the tracking region
	void CalcTracks::set_tracking_model(const ShModelPr &tracking_model){
		tracking_model_ = tracking_model;
	}

	// get model to define the shape of the tracking region
	const ShModelPr& CalcTracks::get_tracking_model()const{
		return tracking_model_;
	}

	// set enable dynamic setting
	void CalcTracks::set_enable_dynamic(const bool enable_dynamic){
		enable_dynamic_ = enable_dynamic;
	}

	void CalcTracks::set_track_type(const TrackingType track_type){
		track_type_ = track_type;
	}


	// get enable dynamic setting
	bool CalcTracks::get_enable_dynamic()const{
		return enable_dynamic_;
	}

	CalcTracks::TrackingType CalcTracks::get_track_type()const{
		return track_type_;
	}


	// calculate with inductance data output
	ShTrackDataPr CalcTracks::calculate_tracks(
		const fltp time, 
		const cmn::ShLogPr &lg,
		const ShSolverCachePr& cache)const{

		// when calculating the settings must be valid
		is_valid(true);

		// get date and time
		auto timdata = std::time(nullptr);
		auto localtime = *std::localtime(&timdata);
		std::ostringstream date_stream,time_stream;
		date_stream << std::put_time(&localtime, "%d-%m-%Y");
		time_stream << std::put_time(&localtime, "%H:%M:%S");
		const std::string date_str = date_stream.str();
		const std::string time_str = time_stream.str();

		// general info
		lg->msg("%s=== Starting Track Calculation ===%s\n",KGRN,KNRM);
		lg->msg(2,"%sGENERAL INFO%s\n",KGRN,KNRM);
		lg->msg("date: %s%s%s %s(dd-mm-yyyy)%s\n",KYEL,date_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("time: %s%s%s %s(hh:mm:ss)%s\n",KYEL,time_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("model name: %s%s%s\n",KYEL,model_->get_name().c_str(),KNRM);
		lg->msg("path name: %s%s%s\n",KYEL,get_name().c_str(),KNRM);
		lg->msg(-2,"\n");

		// header
		lg->msg(2,"%sSETTING UP MESHES%s\n",KGRN,KNRM);

		// check input model
		if(model_==NULL)rat_throw_line("model not set");
		model_->is_valid(true);

		// settings for mesh creation
		MeshSettings mesh_settings;
		mesh_settings.time = time;
		mesh_settings.low_poly = false;
		mesh_settings.combine_sections = true;

		// create a list of meshes
		std::list<ShMeshDataPr> meshes = model_->create_meshes({},mesh_settings);

		// remove calculation meshes
		for(auto it = meshes.begin();it!=meshes.end();)
			if((*it)->get_calc_mesh())it = meshes.erase(it); else it++;

		// set circuit currents
		apply_circuit_currents(time, meshes);

		// display function
		MeshData::display(lg,meshes);

		// end mesh setup
		lg->msg(-2,"\n");

		// create a list of meshes
		std::list<ShMeshDataPr> tracking_meshes = tracking_model_->create_meshes({},mesh_settings);

		// tracking start time
		const fltp start_time = time;

		lg->msg(2,"%sSETUP FIELDMAP%s\n",KGRN,KNRM);

		// setup field map
		const ShMaxwellFieldMapPr fieldmap = MaxwellFieldMap::create();
		fieldmap->set_solver_cache(cache);
		fieldmap->set_fmm_settings(stngs_);
		fieldmap->setup(enable_dynamic_, start_time, get_circuits(), meshes, tracking_meshes, bg_, lg);

		// end fieldmap setup
		lg->msg(-2,"\n");

		// check if cancelled
		if(lg->is_cancelled())return NULL;

		// track data
		const ShTrackDataPr track_data = TrackData::create();
		track_data->set_name(get_name() + "_data");
		track_data->set_integration_method(integration_method_);
		track_data->set_enable_dynamic(enable_dynamic_);
		track_data->set_circuits(get_circuits()); // for post processing
		track_data->set_time(start_time);

		// perform tracking
		switch(track_type_){
			case TrackingType::FLUX_LINES: track_data->create_flux_line_tracks(fieldmap, get_emitters(), max_step_size_, rabstol_, rreltol_, lg); break;
			case TrackingType::ELECTRIC_FIELD_LINES: track_data->create_electric_field_tracks(fieldmap, get_emitters(), max_step_size_, rabstol_, rreltol_, lg); break;
			case TrackingType::NEWTON: track_data->create_newton_tracks(fieldmap, get_emitters(), max_step_size_, rabstol_, rreltol_, pabstol_, preltol_, lg); break;
			case TrackingType::RELATIVISTIC: track_data->create_relativistic_tracks(fieldmap, get_emitters(), max_step_size_, rabstol_, rreltol_, pabstol_, preltol_, lg); break;
			default: rat_throw_line("track type not recognized");
		}

		// return tracks
		return track_data;
	}

	// generalized calculation
	std::list<ShDataPr> CalcTracks::calculate(
		const fltp time, 
		const cmn::ShLogPr &lg,
		const ShSolverCachePr& cache){
		return {calculate_tracks(time,lg,cache)};
	}

	// creation of calculation data objects
	std::list<ShMeshDataPr> CalcTracks::create_meshes(
		const std::list<arma::uword> &trace, 
		const MeshSettings &stngs)const{

		// do not make mesh if disabled
		if(!enable_)return{};
		
		// get trace index
		std::list<arma::uword> next_trace = trace;
		if(!trace.empty()){next_trace.pop_front();}

		// allocate list of output meshes
		std::list<ShMeshDataPr> meshes; arma::uword idx = 1;

		// create meshes from tracking model
		if(visibility_ && tracking_model_!=NULL){
			// check if this coil is in the trace
			bool in_trace = true; if(!trace.empty())if(trace.front()!=idx)in_trace = false;
				
			// when in trace create and add meshes
			if(in_trace){
				std::list<ShMeshDataPr> coil_meshes = tracking_model_->create_meshes(next_trace,stngs);
				for(auto it = coil_meshes.begin();it!=coil_meshes.end();it++){
					(*it)->append_trace_id(idx);
					meshes.push_back(*it);
				}
			}
		}

		// increment counter independent on whether the mesh is visible or not
		idx++;

		// create meshes from tracking model
		for(auto it=emitters_.begin();it!=emitters_.end();it++,idx++){
			// check if this coil is in the trace
			bool in_trace = true; if(!trace.empty())if(trace.front()!=idx)in_trace = false;
			
			// when in trace create and add meshes
			if(in_trace){
				std::list<ShMeshDataPr> emitter_meshes = (*it).second->create_meshes(next_trace,stngs);
				for(auto it2 = emitter_meshes.begin();it2!=emitter_meshes.end();it2++){
					(*it2)->append_trace_id(idx);
					meshes.push_back(*it2);
				}
			}
		}

		// return meshes
		return {meshes};
	}

	// validity check
	bool CalcTracks::is_valid(const bool enable_throws) const{
		// settings
		if(rreltol_<=0){if(enable_throws){rat_throw_line("rabstol must be larger than zero");} return false;};
		if(rabstol_<=0){if(enable_throws){rat_throw_line("rreltol must be larger than zero");} return false;};
		if(preltol_<=0){if(enable_throws){rat_throw_line("pabstol must be larger than zero");} return false;};
		if(pabstol_<=0){if(enable_throws){rat_throw_line("preltol must be larger than zero");} return false;};
		if(emitters_.empty()){if(enable_throws){rat_throw_line("there are no emitters");} return false;};
		// emitters
		for(auto it=emitters_.begin();it!=emitters_.end();it++){
			const ShEmitterPr& emitter = (*it).second;
			if(emitter==NULL)rat_throw_line("emitter list contains NULL");
			if(!emitter->is_valid(enable_throws))return false;
		}

		// tracking model
		if(tracking_model_==NULL){if(enable_throws){rat_throw_line("tracking model is NULL");} return false;};
		if(!tracking_model_->is_valid(enable_throws))return false;
		return true;
	}

	// serialization
	std::string CalcTracks::get_type(){
		return "rat::mdl::calctracks";
	}

	void CalcTracks::serialize(
		Json::Value &js, 
		cmn::SList &list) const{
		
		// parent
		CalcLeaf::serialize(js,list);

		// type
		js["type"] = get_type();

		// settings
		js["visibility"] = visibility_;
		js["integration_method"] = static_cast<int>(integration_method_);
		js["max_step_size"] = max_step_size_;
		js["rabstol"] = rabstol_;
		js["pabstol"] = pabstol_;
		js["rreltol"] = rreltol_;
		js["preltol"] = preltol_;

		// tracking options
		js["enable_dynamic"] = enable_dynamic_;
		js["tracking_type"] = static_cast<int>(track_type_);

		// serialize the emitters
		for(auto it = emitters_.begin();it!=emitters_.end();it++)
			js["emitters"].append(cmn::Node::serialize_node((*it).second, list));

		// tracking model
		js["tracking_model"] = cmn::Node::serialize_node(tracking_model_, list);
	}
	
	void CalcTracks::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		
		// parent
		CalcLeaf::deserialize(js,list,factory_list,pth);

		// settings
		set_visibility(js["visibility"].asBool());
		if(js.isMember("max_step_size"))set_max_step_size(js["max_step_size"].ASFLTP());
		if(js.isMember("rabstol"))set_rabstol(js["rabstol"].ASFLTP());
		if(js.isMember("pabstol"))set_pabstol(js["pabstol"].ASFLTP());
		if(js.isMember("rreltol"))set_rreltol(js["rreltol"].ASFLTP());
		if(js.isMember("preltol"))set_preltol(js["preltol"].ASFLTP());
		set_integration_method(static_cast<cmn::RungeKutta::IntegrationMethod>(js["integration_method"].asInt()));

		// dynamic
		set_enable_dynamic(js["enable_dynamic"].asBool());
		set_track_type(static_cast<TrackingType>(js["tracking_type"].asInt()));

		// deserialize the emitters
		for(auto it = js["emitters"].begin();it!=js["emitters"].end();it++)
			add_emitter(cmn::Node::deserialize_node<Emitter>(
				(*it), list, factory_list, pth));

		// tracking model
		set_tracking_model(cmn::Node::deserialize_node<Model>(
			js["tracking_model"], list, factory_list, pth));
	}


}}