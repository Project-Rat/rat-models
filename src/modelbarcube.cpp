// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelbarcube.hh"

#include "crossrectangle.hh"
#include "pathaxis.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	ModelBarCube::ModelBarCube(){
		set_name("Cube Permanent Magnet");
		set_magnetisation({RAT_CONST(2.0)/arma::Datum<fltp>::mu_0, RAT_CONST(0.0), RAT_CONST(0.0)});
	}

	// constructor with dimension input
	ModelBarCube::ModelBarCube(
		const fltp width, 
		const fltp thickness,
		const fltp height,
		const fltp element_size) : ModelBarCube(){

		// set settings to self
		set_width(width); set_thickness(thickness);
		set_height(height); set_element_size(element_size);
	}

	// factory
	ShModelBarCubePr ModelBarCube::create(){
		//return ShModelBarCubePr(new ModelBarCube);
		return std::make_shared<ModelBarCube>();
	}

	// factory with dimension input
	ShModelBarCubePr ModelBarCube::create(
		const fltp width, 
		const fltp thickness,
		const fltp height,
		const fltp element_size){
		return std::make_shared<ModelBarCube>(
			width, thickness, height, element_size);
	}

	// get base and cross
	ShPathPr ModelBarCube::get_input_path() const{
		// check input
		is_valid(true);

		// create axis
		const ShPathAxisPr axis = PathAxis::create('z','x',height_,cmn::Extra::null_vec(),element_size_);

		// return the racetrack
		return axis;
	}

	// get cross section
	ShCrossPr ModelBarCube::get_input_cross() const{
		// check input
		is_valid(true);

		// create rectangular cross
		ShCrossRectanglePr rectangle = CrossRectangle::create(
			-thickness_/2, thickness_/2, 
			-width_/2, width_/2, 
			element_size_, element_size_);

		// return the rectangle
		return rectangle;
	}


	// set width
	void ModelBarCube::set_width(const fltp width){
		width_ = width;
	}
	
	// set thickness
	void ModelBarCube::set_thickness(const fltp thickness){
		thickness_ = thickness;
	}
	
	// set height
	void ModelBarCube::set_height(const fltp height){
		height_ = height;
	}
	
		// set width
	void ModelBarCube::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}
	


	// get width
	fltp ModelBarCube::get_width() const{
		return width_;
	}

	// get thickness
	fltp ModelBarCube::get_thickness() const{
		return thickness_;
	}

	// get height
	fltp ModelBarCube::get_height() const{
		return height_;
	}

	// get element size
	fltp ModelBarCube::get_element_size() const{
		return element_size_;
	}


	
	// vallidity check
	bool ModelBarCube::is_valid(const bool enable_throws) const{
		if(!ModelBarWrapper::is_valid(enable_throws))return false;
		if(width_<=0){if(enable_throws){rat_throw_line("width must be larger than zero");} return false;};
		if(thickness_<=0){if(enable_throws){rat_throw_line("thickness must be larger than zero");} return false;};
		if(height_<=0){if(enable_throws){rat_throw_line("height must be larger than zero");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string ModelBarCube::get_type(){
		return "rat::mdl::modelbarcube";
	}

	// method for serialization into json
	void ModelBarCube::serialize(
		Json::Value &js, cmn::SList &list) const{
		// parent
		ModelBarWrapper::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["width"] = width_;
		js["thickness"] = thickness_;
		js["height"] = height_;
		js["element_size"] = element_size_;
	}

	// method for deserialisation from json
	void ModelBarCube::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent
		ModelBarWrapper::deserialize(js,list,factory_list,pth);

		// properties
		set_width(js["width"].ASFLTP());
		set_thickness(js["thickness"].ASFLTP());
		set_height(js["height"].ASFLTP());
		set_element_size(js["element_size"].ASFLTP());
	}

}}
