// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelline.hh"

#include "rat/common/error.hh"

#include "crosspoint.hh"
#include "pathstraight.hh"
#include "pathgroup.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	ModelLine::ModelLine(){
		set_name("Line");
	}

	// default constructor
	ModelLine::ModelLine(
		const arma::Col<fltp>::fixed<3> &R1, 
		const arma::Col<fltp>::fixed<3> &R2,
		const fltp element_size) : ModelLine(){

		// set to self
		set_start_point(R1); set_end_point(R2);
		set_element_size(element_size);
	}

	// factory
	ShModelLinePr ModelLine::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<ModelLine>();
	}

	// factory
	ShModelLinePr ModelLine::create(
		const arma::Col<fltp>::fixed<3> &R1, 
		const arma::Col<fltp>::fixed<3> &R2,
		const fltp element_size){
		return std::make_shared<ModelLine>(R1,R2,element_size);
	}

	// get base
	ShPathPr ModelLine::get_input_path() const{
		// check input
		is_valid(true);

		// get direction
		arma::Col<rat::fltp>::fixed<3> L = R2_-R1_; 
		const fltp ell = arma::as_scalar(cmn::Extra::vec_norm(L)); L/=ell;
		arma::Col<rat::fltp>::fixed<3> D;
		if(L.index_max()==0 || L.index_max()==1)D = cmn::Extra::cross(L, arma::Col<fltp>::fixed<3>{0,0,1});
		else D = cmn::Extra::cross(L, cmn::Extra::unit_vec('x'));

		// create a pathgroup
		ShPathGroupPr grp = PathGroup::create(R1_, L, D);
		grp->add_path(PathStraight::create(ell, element_size_));

		// return the circle
		return grp;
	}

	// get cross
	ShCrossPr ModelLine::get_input_cross() const{
		// check input
		is_valid(true);

		// create circular path
		ShCrossPointPr rectangle = CrossPoint::create(0,0,1e-6,1e-6);

		// return the rectangle
		return rectangle;
	}

	// set properties
	void ModelLine::set_start_point(const arma::Col<fltp>::fixed<3> &R1){
		R1_ = R1;
	}

	void ModelLine::set_end_point(const arma::Col<fltp>::fixed<3> &R2){
		R2_ = R2;
	}

	void ModelLine::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	// get properties
	arma::Col<fltp>::fixed<3> ModelLine::get_start_point() const{
		return R1_;
	}
	
	arma::Col<fltp>::fixed<3> ModelLine::get_end_point() const{
		return R2_;
	}
	
	fltp ModelLine::get_element_size() const{
		return element_size_;
	}

	// check input
	bool ModelLine::is_valid(const bool enable_throws) const{
		if(!ModelMeshWrapper::is_valid(enable_throws))return false;
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string ModelLine::get_type(){
		return "rat::mdl::modelline";
	}

	// method for serialization into json
	void ModelLine::serialize(
		Json::Value &js, cmn::SList &list) const{

		// parent
		ModelMeshWrapper::serialize(js,list);

		// set type
		js["type"] = get_type();

		// start point
		js["R1x"] = R1_(0); js["R1y"] = R1_(1); js["R1z"] = R1_(2);
			
		// start point
		js["R2x"] = R2_(0); js["R2y"] = R2_(1); js["R2z"] = R2_(2);

		// element size
		js["element_size"] = element_size_;
	}

	// method for deserialisation from json
	void ModelLine::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		ModelMeshWrapper::deserialize(js,list,factory_list,pth);

		// start point
		R1_(0) = js["R1x"].ASFLTP();
		R1_(1) = js["R1y"].ASFLTP();
		R1_(2) = js["R1z"].ASFLTP();
			
		// start point
		R2_(0) = js["R2x"].ASFLTP();
		R2_(1) = js["R2y"].ASFLTP();
		R2_(2) = js["R2z"].ASFLTP();

		// element size
		element_size_ = js["element_size"].ASFLTP();
	}

}}