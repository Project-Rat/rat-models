// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathspiral.hh"

// rat-common headers
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathSpiral::PathSpiral(){
		set_name("Spiral");
	}

	PathSpiral::PathSpiral(
		const fltp radius, 
		const fltp nt1, 
		const fltp nt2, 
		const fltp pitch,
		const fltp element_size,
		const bool transverse) : PathSpiral(){

		set_radius(radius);
		set_nt1(nt1);
		set_nt2(nt2);
		set_pitch(pitch);
		set_element_size(element_size);
		set_transverse(transverse);
	}

	// factory
	ShPathSpiralPr PathSpiral::create(){
		//return ShPathSpiralPr(new PathSpiral);
		return std::make_shared<PathSpiral>();
	}

	// factory with dimension input
	ShPathSpiralPr PathSpiral::create(
		const fltp radius, 
		const fltp nt1, 
		const fltp nt2, 
		const fltp pitch,
		const fltp element_size,
		const bool transverse){
		return std::make_shared<PathSpiral>(
			radius,nt1,nt2,pitch,element_size,transverse);
	}

	// set properties
	void PathSpiral::set_radius(const fltp radius){
		radius_ = radius;
	}

	void PathSpiral::set_nt1(const fltp nt1){
		nt1_ = nt1;
	}

	void PathSpiral::set_nt2(const fltp nt2){
		nt2_ = nt2;
	}
	
	void PathSpiral::set_pitch(const fltp pitch){
		pitch_ = pitch;
	}

	void PathSpiral::set_phase(const fltp phase){
		phase_ = phase;
	}
	
	void PathSpiral::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	void PathSpiral::set_transverse(const bool transverse){
		transverse_ = transverse;
	}


	// get properties
	fltp PathSpiral::get_radius() const{
		return radius_;
	}

	fltp PathSpiral::get_nt1() const{
		return nt1_;
	}

	fltp PathSpiral::get_nt2() const{
		return nt2_;
	}

	fltp PathSpiral::get_pitch() const{
		return pitch_;
	}

	fltp PathSpiral::get_phase() const{
		return phase_;
	}

	fltp PathSpiral::get_element_size() const{
		return element_size_;
	}

	bool PathSpiral::get_transverse()const{
		return transverse_;
	}



	// get frame
	ShFramePr PathSpiral::create_frame(const MeshSettings &stngs) const{
		// check input
		is_valid(true);

		// calculate number of nodes
		const fltp alpha = std::atan(2*radius_/(0.5*pitch_));
		arma::uword num_nodes = std::max(static_cast<arma::uword>(std::ceil((radius_*arma::Datum<fltp>::tau*(nt2_-nt1_)/std::sin(alpha))/element_size_)),2llu);
		while((num_nodes-1)%stngs.element_divisor!=0)num_nodes++;

		// create theta
		const arma::Row<fltp> theta = arma::linspace<arma::Row<fltp> >(
			nt1_*2*arma::Datum<fltp>::pi, nt2_*2*arma::Datum<fltp>::pi, num_nodes);

		// allocate
		const arma::Mat<fltp> R = arma::join_vert(
			radius_*arma::sin(theta + phase_), 
			radius_*arma::cos(theta + phase_),
			pitch_*theta/(arma::Datum<fltp>::tau));

		const arma::Mat<fltp> L = cmn::Extra::normalize(arma::join_vert(
			radius_*arma::cos(theta + phase_), 
			-radius_*arma::sin(theta + phase_),
			arma::Row<fltp>(num_nodes,arma::fill::ones)*
			pitch_/(arma::Datum<fltp>::tau)));

		const arma::Mat<fltp> N = cmn::Extra::normalize(arma::join_vert(
			radius_*arma::sin(theta + phase_), 
			radius_*arma::cos(theta + phase_),
			arma::Row<fltp>(num_nodes,arma::fill::zeros)));

		// calculate last vector
		arma::Mat<fltp> D = cmn::Extra::cross(L,N);

		// check handedness
		// assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N,L),D)>RAT_CONST(0.0)));
		
		// create frame
		const ShFramePr frame = Frame::create(R,L,transverse_ ? D : N, transverse_ ? N : -D);

		// apply transformations
		frame->apply_transformations(get_transformations(), stngs.time);
		
		// create frame and return
		return frame;
	}

	// validity check
	bool PathSpiral::is_valid(const bool enable_throws) const{
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		if(radius_<=0){if(enable_throws){rat_throw_line("radius must be larger than zero");} return false;};
		if(nt2_<=nt1_){if(enable_throws){rat_throw_line("nt1 must be smaller than nt2");} return false;};
		return true;
	}


	// get type
	std::string PathSpiral::get_type(){
		return "rat::mdl::pathspiral";
	}

	// method for serialization into json
	void PathSpiral::serialize(Json::Value &js, cmn::SList &list) const{
		// parent nodes
		Transformations::serialize(js,list);

		// settings
		js["type"] = get_type();
		js["transverse"] = transverse_;
		js["radius"] = radius_;
		js["nt1"] = nt1_; 
		js["nt2"] = nt2_; 
		js["pitch"] = pitch_;
		js["phase"] = phase_;
		js["element_size"] = element_size_;
	}

	// method for deserialisation from json
	void PathSpiral::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent nodes
		Transformations::deserialize(js,list,factory_list,pth);

		// settings
		set_transverse(js["transverse"].asBool());
		set_radius(js["radius"].ASFLTP());
		set_nt1(js["nt1"].ASFLTP()); 
		set_nt2(js["nt2"].ASFLTP()); 
		set_pitch(js["pitch"].ASFLTP());
		set_phase(js["phase"].ASFLTP());
		set_element_size(js["element_size"].ASFLTP());
	}

}}



