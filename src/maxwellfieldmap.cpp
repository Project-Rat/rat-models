// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "maxwellfieldmap.hh"

// rat-common headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// rat-fmm headers
#include "rat/mlfmm/multitargets2.hh"
#include "rat/mlfmm/multisources.hh"
#include "rat/mlfmm/mgntargets.hh"
#include "rat/mlfmm/nodelevel.hh"

// rat-models headers
#include "coildata.hh"
#include "bardata.hh"
#include "nldata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	MaxwellFieldMap::MaxwellFieldMap(){
		// call setup yourself
	}

	// factory
	ShMaxwellFieldMapPr MaxwellFieldMap::create(){
		return std::make_shared<MaxwellFieldMap>();
	}

	// set the solver cache
	void MaxwellFieldMap::set_solver_cache(const mdl::ShSolverCachePr& cache){
		cache_ = cache;
	}

	// set the settings for the MLFMM
	void MaxwellFieldMap::set_fmm_settings(const fmm::ShSettingsPr& stngs){
		stngs_ = stngs;
	}

	// setup function
	void MaxwellFieldMap::setup(
		const bool enable_dynamic,
		const fltp static_time,
		const std::list<ShCircuitPr>& circuits,
		const std::list<ShMeshDataPr>& source_meshes, 
		const std::list<ShMeshDataPr>& tracking_meshes,
		const mdl::ShBackgroundPr& bg,
		const cmn::ShLogPr& lg){

		// check input
		assert(lg!=NULL);

		// setup the fieldmap
		lg->msg(2,"%sSetup Fieldmap%s\n",KBLU,KNRM);

		// set background to self
		bg_ = bg;

		// if settings not set use default settings
		if(stngs_==NULL)stngs_ = fmm::Settings::create();

		// get direct calculation
		const bool use_direct_calculation = stngs_->get_direct()==fmm::DirectMode::ALWAYS;

		// setup some MLFMM related objects
		const fmm::ShIListPr ilist = fmm::IList::create(stngs_);

		// setup interaction lists
		ilist->setup(); 

		// create targets for the tracking mesh
		fmm::ShMultiTargets2Pr tar = fmm::MultiTargets2::create();
		for(auto it=tracking_meshes.begin();it!=tracking_meshes.end();it++)tar->add_targets(*it);
		tar->setup_targets();

		// create list of circuit indices
		std::map<arma::uword, ShCircuitPr> circuits_id;
		for(auto it=circuits.begin();it!=circuits.end();it++)
			circuits_id[(*it)->get_circuit_id()] = (*it);

		// sort coils by circuit ID
		std::map<arma::uword, std::list<ShCoilDataPr> > coil_meshes_id;
		for(auto it=source_meshes.begin();it!=source_meshes.end();it++){
			const ShCoilDataPr coil_data = std::dynamic_pointer_cast<CoilData>(*it);
			if(!coil_data)continue;

			// get circuit ID
			const arma::uword myid = (*it)->get_circuit_index();

			// check if ID exists if not add under ID 0
			if(myid!=0){
				if(circuits_id.find(myid)==circuits_id.end() || !enable_dynamic){
					coil_meshes_id[0].push_back(coil_data);
					continue;
				}
			}

			// add to list under its circuit ID
			coil_meshes_id[myid].push_back(coil_data);
		}

		// for each circuit setup the tracking grid
		if(enable_dynamic){
			for(auto it=circuits.begin();it!=circuits.end();it++){
				// get circuit
				const ShCircuitPr& circuit = (*it);
				const arma::uword myid = circuit->get_circuit_id();

				// setup the fieldmap
				lg->msg(2,"%sSetup Grid for: %s%s\n",KCYN,circuit->get_name().c_str(),KNRM);

				// get coil list for this circuit
				const auto it2 = coil_meshes_id.find(myid);
				if(it2==coil_meshes_id.end())rat_throw_line("circuit " + std::to_string(myid) + " has no coils");
				const std::list<ShCoilDataPr>& coil_list = (*it2).second;

				// coils
				lg->msg("Attached Coils:\n");
				for(auto it3 = coil_list.begin();it3!=coil_list.end();it3++)
					lg->msg("- %s\n",(*it3)->get_name().c_str());

				// set all currents to 1.0 so that the fieldmap can be scaled later
				for(auto it3 = coil_list.begin();it3!=coil_list.end();it3++)
					(*it3)->set_operating_current(base_current_);

				// dump coil list into a multisource
				const fmm::ShMultiSourcesPr src = fmm::MultiSources::create();
				for(const auto& coil_data : coil_list)
					src->add_sources(coil_data->create_currents());

				// call setup function on sources and targets
				src->setup_sources(); 

				// check for cancel
				if(lg->is_cancelled())return;

				// setup oct-tree grid
				const fmm::ShTrackingGridPr grid = setup_maxwellian_grid(use_direct_calculation, src, tar, stngs_, ilist, lg);

				// add grid to list
				maxwellian_grids_.push_back({circuit, grid});

				// done
				lg->msg(-2,"\n");

				// check for cancel
				if(lg->is_cancelled())return;
			}
		}


		// create static sources
		const fmm::ShMultiSourcesPr static_src = fmm::MultiSources::create();

		// treat leftover coils as if they are zero
		auto it = coil_meshes_id.find(0);
		if(it!=coil_meshes_id.end()){
			for(auto it2 = (*it).second.begin(); it2!=(*it).second.end();it2++){
				auto it3 = circuits_id.find((*it2)->get_circuit_index());
				if(it3!=circuits_id.end()){
					(*it2)->set_operating_current((*it3).second->get_current(static_time));
				}
				static_src->add_sources((*it2)->create_currents());
			}
		}

		// create DC sources grid including coils with ID 0
		for(auto it2=source_meshes.begin();it2!=source_meshes.end();it2++){
			const ShBarDataPr bar_data = std::dynamic_pointer_cast<BarData>(*it2);
			if(!bar_data)continue;

			// create bound currents for vector potential calcultion
			const fmm::ShSourcesPr bnd_surf_currents = bar_data->create_bnd_surf_currents();
			if(bnd_surf_currents!=NULL)static_src->add_sources(bnd_surf_currents);
			const fmm::ShSourcesPr bnd_vol_currents = bar_data->create_bnd_vol_currents();
			if(bnd_vol_currents!=NULL)static_src->add_sources(bnd_vol_currents);
		}

		// check for cancel
		if(lg->is_cancelled())return;


		// non-linear solver setup
		#ifdef ENABLE_NL_SOLVER
		// set a solver cache if not externally set
		if(!cache_)cache_ = mdl::SolverCache::create();

		// find non-linear meshes
		for(auto it2=source_meshes.begin();it2!=source_meshes.end();it2++){
			const ShNLDataPr nl_data = std::dynamic_pointer_cast<NLData>(*it2);
			if(!nl_data)continue;
			nl_meshes_.push_back(nl_data);
		}

		// check if any nl meshes
		if(!nl_meshes_.empty()){
			// combine meshes
			arma::uword idx = 0, nodeshift = 0;
			arma::field<arma::Mat<fltp> > Rnfld(1,nl_meshes_.size());
			arma::field<arma::Mat<arma::uword> > nfld(1,nl_meshes_.size());
			arma::field<arma::Row<arma::uword> > n2part(1,nl_meshes_.size());
			arma::field<nl::ShHBDataPr> hb(1,nl_meshes_.size());
			for(auto it=nl_meshes_.begin();it!=nl_meshes_.end();it++){
				Rnfld(idx) = (*it)->get_nodes();
				nfld(idx) = (*it)->get_elements() + nodeshift;
				hb(idx) = (*it)->get_hb_data();
				if(hb(idx)==NULL)rat_throw_line("HB Curve was not supplied for: " + (*it)->get_name());
				n2part(idx) = arma::Row<arma::uword>(nfld(idx).n_cols,arma::fill::value(idx));
				nodeshift += Rnfld(idx).n_cols; idx++; 
			}

			// convert to matrices
			arma::Mat<fltp> Rn = cmn::Extra::field2mat(Rnfld);
			arma::Mat<arma::uword> n = cmn::Extra::field2mat(nfld);

			// setup solver object 
			nl_solver_ = nl::NonLinSolver::create();
			nl_solver_->set_use_gpu(stngs_->get_fmm_enable_gpu());
			nl_solver_->set_gpu_devices(stngs_->get_gpu_devices());
			
			// disable logging for the entire NL solver
			nl_solver_->set_lg(cmn::NullLog::create());

			// set mesh and hb curves to solver
			nl_solver_->set_mesh(Rn, n);
			nl_solver_->set_n2part(cmn::Extra::field2mat(n2part)); 
			nl_solver_->set_hb_curve(hb);

			// setup mesh by calculating the face and edge connectivity matrices
			nl_solver_->setup_mesh();

			// calculate external field
			const rat::fmm::ShMgnTargetsPr nltar = nl_solver_->get_ext_targets();

			// for each circuit calculate the field on the non-linear targets
			for(auto it=circuits.begin();it!=circuits.end();it++){
				// allocate targets
				nltar->allocate();

				// get circuit index
				const ShCircuitPr& circuit = (*it);
				const arma::uword myid = circuit->get_circuit_id();

				// get coils attached to this circuti
				// get coil list for this circuit
				const auto it2 = coil_meshes_id.find(myid);
				if(it2==coil_meshes_id.end())rat_throw_line("circuit " + std::to_string(myid) + " has no coils");
				const std::list<ShCoilDataPr>& coil_list = (*it2).second;

				// create multisources
				const fmm::ShMultiSourcesPr src = fmm::MultiSources::create();
				for(const auto& coil_data : coil_list)
					src->add_sources(coil_data->create_currents());

				// multipole method using vector potentials
				const fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(src, nltar, stngs_);
				mlfmm->set_no_allocate();

				// setup mlfmm and calculate
				mlfmm->setup(); 
				
				// check for cancel
				if(lg->is_cancelled())return;

				// run calculation
				mlfmm->calculate();

				// check for cancel
				if(lg->is_cancelled())return;

				// get H field  on non-linear targets
				nl_dynamic_field_.push_back({circuit, nltar->get_field('H')});
			}

			// check if there are static sources as well as dynamic sources
			if(static_src->get_num_source_objects()>0 || bg_){
				// allocate targets again
				nltar->allocate();

				// calculate field from static sources onto the nl mesh
				if(static_src->get_num_source_objects()>0){
					// call setup function on sources and targets
					static_src->setup_sources(); 

					// multipole method using vector potentials
					const fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(static_src, nltar, stngs_);
					mlfmm->set_no_allocate();

					// setup mlfmm and calculate
					mlfmm->setup(); 
					
					// check for cancel
					if(lg->is_cancelled())return;

					// run calculation
					mlfmm->calculate();

					// check for cancel
					if(lg->is_cancelled())return;
				}

				// check if the background field is static
				bool bg_is_static = true;
				if(bg_)bg_is_static = bg_->is_static();

				// both static sources as well as dynamic sources
				// this means the nl solver needs to be updated
				// every time step
				if(enable_dynamic && (!maxwellian_grids_.empty() || !bg_is_static)){
					// get H field  on non-linear targets
					nl_static_field_ = nltar->get_field('H');
				}

				// if only static sources we can run the nlsolver once
				// and use the sources from there
				else{
					// add the static background field 
					// now as it contributes to the
					// external field on the nl mesh
					if(bg_)bg_->create_fmm_background(static_time)->calc_field(nltar);

					// set the external field
					nl_solver_->set_ext_field(nltar);

					// run nonlinear solver
					const arma::Row<rat::fltp> Ae = nl_solver_->solve(cache_).t();

					// check for cancel
					if(lg->is_cancelled())return;

					// calculate magnetization at elements
					const arma::Mat<fltp> Mg = nl_solver_->calc_elem_magnetization(Ae); 	
					const arma::Mat<fltp> Me = nl_solver_->element_average(Mg);

					// walk over non-linear meshes
					arma::uword elem_shift = 0;
					for(auto it=nl_meshes_.begin();it!=nl_meshes_.end();it++){
						// get number of elements in this mesh
						const arma::uword num_elements = (*it)->get_num_elements();

						// set field to respective non-linear meshes
						(*it)->set_magnetisation(Me.cols(elem_shift,elem_shift+num_elements-1),false);

						// shift position
						elem_shift += num_elements;
					}

					// create sources
					for(auto it=nl_meshes_.begin();it!=nl_meshes_.end();it++){
						// create the bound currents to represent the non-linear mesh
						const fmm::ShSourcesPr bnd_surf_currents = (*it)->create_bnd_surf_currents();
						if(bnd_surf_currents!=NULL)static_src->add_sources(bnd_surf_currents);

						const fmm::ShCurrentSourcesPr bnd_vol_currents = (*it)->create_bnd_vol_currents();
						if(bnd_vol_currents!=NULL)static_src->add_sources(bnd_vol_currents);
					}

					// clear nl-solver
					nl_solver_  = NULL; // this prevents running dynamic calculations
					nl_meshes_.clear();
				}
			}
		}
		#endif

		// setup static tracking grid
		if(static_src->get_num_source_objects()>0){
			// static source grid
			lg->msg(2,"%sSetup Grid for: Static Sources%s\n",KCYN,KNRM);

			// call setup function on sources and targets
			static_src->setup_sources(); 

			// create object and set sources and targets
			static_grid_ = setup_maxwellian_grid(use_direct_calculation, static_src, tar, stngs_, ilist, lg);

			// done
			lg->msg(-2,"\n");
		}

		// setup the fieldmap done
		lg->msg(-2,"\n");
	}

	// check if we are inside
	arma::Row<arma::uword> MaxwellFieldMap::is_inside(const arma::Mat<fltp>&Rt)const{
		arma::Row<arma::uword> inside(Rt.n_cols,arma::fill::ones);
		for(auto it=maxwellian_grids_.begin();it!=maxwellian_grids_.end();it++)
			inside = inside && (*it).second->is_inside(Rt);
		if(static_grid_)
			inside = inside && static_grid_->is_inside(Rt);
		return inside;
	}



	// setup tracking grid
	fmm::ShTrackingGridPr MaxwellFieldMap::setup_maxwellian_grid(
		const bool use_direct_calculation,
		const fmm::ShSourcesPr& src, 
		const fmm::ShTargetsPr& tar, 
		const fmm::ShSettingsPr& stngs,
		const fmm::ShIListPr& ilist,
		const cmn::ShLogPr& /*lg*/){

		// check if there are targets available
		if(tar->num_targets()==0)
			rat_throw_line("tracking model is empty");

		// create object and set sources and targets
		const fmm::ShTrackingGridPr grid = fmm::TrackingGrid::create();
		grid->set_settings(stngs);
		grid->set_sources(src);
		grid->set_targets(tar); // not sure if it is ok to use the targets twice here!

		// check whether to use MLFMM interpolation
		if(!use_direct_calculation){
			// set ilist to grid
			grid->set_ilist(ilist);

			// create root nodelevel object
			const fmm::ShNodeLevelPr root = fmm::NodeLevel::create(stngs,ilist,grid);

			// call setup functions
			grid->setup();
			grid->setup_matrices(); 

			// setup tree
			root->setup();

			// run multipole method recursively from root
			grid->sort_srctar();
			root->run_mlfmm();
			// leave sorces sorted

			// setup fmm (sorts sources and targets)
			grid->fmm_setup();
		}

		// setup grid only
		else{
			grid->setup_direct();
		}

		// return the trakcing grid
		return grid;
	}

	// check if the fieldmap is dynamic
	bool MaxwellFieldMap::is_dynamic()const{
		return !maxwellian_grids_.empty();
	}

	// calculation of field at target points
	std::pair<arma::Mat<double>,arma::Mat<double> > MaxwellFieldMap::calc_fields(
		const arma::Mat<double>& Rt, const fltp time, const cmn::ShLogPr& lg)const{

		// check input
		assert(Rt.n_rows==3); assert(!Rt.empty());

		// find nans
		arma::Row<arma::uword> is_finite(Rt.n_cols);
		for(arma::uword i=0;i<Rt.n_cols;i++)
			is_finite(i) = Rt.col(i).eval().is_finite();

		// check which target points are inside the grid
		const arma::Col<arma::uword> inside = arma::find(is_inside(Rt) && is_finite);

		if(inside.empty())return{
			arma::Mat<fltp>(3,Rt.n_cols,arma::fill::zeros), 
			arma::Mat<fltp>(3,Rt.n_cols,arma::fill::zeros)};

		// allocate field
		arma::Mat<fltp> B(3,Rt.n_cols,arma::fill::zeros);
		arma::Mat<fltp> E(3,Rt.n_cols,arma::fill::zeros);

		// calculate field on tracks
		const fmm::ShMgnTargetsPr tar = fmm::MgnTargets::create(Rt.cols(inside));

		// walk over grids
		for(auto it=maxwellian_grids_.begin();it!=maxwellian_grids_.end();it++){
			// get pointers
			const ShCircuitPr circuit = it->first;
			const fmm::ShTrackingGridPr grid = it->second;

			// get current and its change
			const fltp current = circuit->get_current(time);
			const fltp dcurrent = circuit->get_dcurrent(time);

			// update field type
			// NOTE: beware not all sources are setup for other field types
			tar->clear_field_type();
			if(current!=0){
				lg->msg("calc %sflux density%s due to dynamic grid\n",KYEL,KNRM);
				tar->add_field_type('B',3); 
			}
			if(dcurrent!=0){
				lg->msg("calc %selectric field%s due to dynamic grid\n",KYEL,KNRM);
				tar->add_field_type('A',3);
			}
			tar->allocate();

			// run field calculation
			grid->calc_field(tar);

			// get field, scale and add to flux density
			if(current!=0)B.cols(inside) += circuit->get_current(time)*tar->get_field('B')/base_current_;
			if(dcurrent!=0)E.cols(inside) += circuit->get_dcurrent(time)*tar->get_field('A')/base_current_;

			// check for cancel
			if(lg->is_cancelled())return {arma::Mat<fltp>(3,Rt.n_cols,arma::fill::zeros), arma::Mat<fltp>(3,Rt.n_cols,arma::fill::zeros)};
		}

		// field from static grid
		if(static_grid_){
			lg->msg("calc %sflux density%s due to static grid\n",KYEL,KNRM);
			tar->set_field_type('B',3); tar->allocate();
			static_grid_->calc_field(tar);
			B.cols(inside) += tar->get_field('B'); // only interested in steady state field, not dAdt
		}
		
		// check for cancel
		if(lg->is_cancelled())return {arma::Mat<fltp>(3,Rt.n_cols,arma::fill::zeros), arma::Mat<fltp>(3,Rt.n_cols,arma::fill::zeros)};

		// background field
		if(bg_){
			// magnetic field
			lg->msg("calc %sflux density%s due to background\n",KYEL,KNRM);
			tar->set_field_type('B',3); tar->allocate();
			bg_->create_fmm_background(time)->calc_field(tar);
			B.cols(inside) += tar->get_field('B');
			
			// electric field
			lg->msg("calc %selectric field%s due to background\n",KYEL,KNRM);
			tar->set_field_type('A',3); tar->allocate();
			bg_->create_fmm_dbackground(time)->calc_field(tar);
			E.cols(inside) += tar->get_field('A');
		}

		// check field
		assert(B.is_finite()); assert(E.is_finite());

		// check for cancel
		if(lg->is_cancelled())return {arma::Mat<fltp>(3,Rt.n_cols,arma::fill::zeros), arma::Mat<fltp>(3,Rt.n_cols,arma::fill::zeros)};

		// dynamic contribution due to non-linear mesh
		#ifdef ENABLE_NL_SOLVER
		// deal with non-linear solver
		if(nl_solver_!=NULL){
			// calculate 
			lg->msg("calc %sflux density%s due to dynamic non-linear meshes\n",KYEL,KNRM);

			// flux density
			// create target points for external field
			const rat::fmm::ShMgnTargetsPr nltar = nl_solver_->get_ext_targets();
			nltar->allocate();

			// background field
			if(bg_)bg_->create_fmm_background(time)->calc_field(nltar);

			// add static field
			if(!nl_static_field_.empty())nltar->add_field('H', nl_static_field_);

			// add fields together
			for(auto it = nl_dynamic_field_.begin();it!=nl_dynamic_field_.end();it++){
				// get ciercuit and field contribution
				const ShCircuitPr& circuit = (*it).first;
				const arma::Mat<fltp>& H = (*it).second;
				const fltp current = circuit->get_current(time);
				nltar->add_field('H', current*H/base_current_);
			}

			// check if any current
			// set the external field
			nl_solver_->set_ext_field(nltar);

			// run nonlinear solver
			const arma::Row<rat::fltp> Ae = nl_solver_->solve(cache_).t();

			// check for cancel
			if(lg->is_cancelled())return {arma::Mat<fltp>(3,Rt.n_cols,arma::fill::zeros), arma::Mat<fltp>(3,Rt.n_cols,arma::fill::zeros)};

			// calculate field on targets
			const fmm::ShMgnTargetsPr tar = fmm::MgnTargets::create(Rt);
			tar->set_field_type('B',3);
			tar->allocate();

			// run mlfmm calculation
			calc_nl_field(Ae, tar, lg);

			// check for cancel
			if(lg->is_cancelled())return {arma::Mat<fltp>(3,Rt.n_cols,arma::fill::zeros), arma::Mat<fltp>(3,Rt.n_cols,arma::fill::zeros)};

			// add flux density to B
			B.cols(inside) += tar->get_field('B');


			// electric field
			// report 
			lg->msg("calc %selectric field%s due to dynamic non-linear meshes\n",KYEL,KNRM);

			// reallocate nl targets
			nltar->allocate();

			// changing background field
			if(bg_)bg_->create_fmm_dbackground(time)->calc_field(tar);

			// add fields together
			for(auto it = nl_dynamic_field_.begin();it!=nl_dynamic_field_.end();it++){
				// get ciercuit and field contribution
				const ShCircuitPr& circuit = (*it).first;
				const arma::Mat<fltp>& H = (*it).second;
				const fltp dIdt = circuit->get_dcurrent(time);
				nltar->add_field('H', dIdt*H/base_current_);
			}

			// external delta field
			nl_solver_->set_ext_dfield(nltar);

			// now that the iron is updated use dsolve to calculate the electric field
			const arma::Row<fltp> dAe = nl_solver_->dsolve(Ae.t()).t();

			// resetup targets
			tar->set_field_type('A',3);
			tar->allocate();

			// run mlfmm calculation
			calc_nl_field(dAe, tar, lg);

			// check for cancel
			if(lg->is_cancelled())return {arma::Mat<fltp>(3,Rt.n_cols,arma::fill::zeros), arma::Mat<fltp>(3,Rt.n_cols,arma::fill::zeros)};

			// add flux density to B
			E.cols(inside) += tar->get_field('A'); // this is actually dAdt as we put in dIdt
		}
		#endif

		// check output
		assert(E.n_cols==Rt.n_cols);
		assert(B.n_cols==Rt.n_cols);

		// return the field
		return {E,B};
	}

	// calculate field from nl solve
	#ifdef ENABLE_NL_SOLVER
	void MaxwellFieldMap::calc_nl_field(
		const arma::Row<fltp>& Ae, 
		const fmm::ShMgnTargetsPr& tar,
		const cmn::ShLogPr& lg)const{

		// calculate magnetization at elements
		const arma::Mat<fltp> Mg = nl_solver_->calc_elem_magnetization(Ae);
		const arma::Mat<fltp> Me = nl_solver_->element_average(Mg);

		// walk over non-linear meshes
		arma::uword elem_shift = 0;
		for(auto it=nl_meshes_.begin();it!=nl_meshes_.end();it++){
			// get number of elements in this mesh
			const arma::uword num_elements = (*it)->get_num_elements();

			// set field to respective non-linear meshes
			(*it)->set_magnetisation(Me.cols(elem_shift,elem_shift+num_elements-1),false);

			// shift position
			elem_shift += num_elements;
		}

		// create sources
		const fmm::ShMultiSourcesPr src = fmm::MultiSources::create();
		for(auto it=nl_meshes_.begin();it!=nl_meshes_.end();it++){
			// create the bound currents to represent the non-linear mesh
			const fmm::ShSourcesPr bnd_surf_currents = (*it)->create_bnd_surf_currents();
			if(bnd_surf_currents!=NULL)src->add_sources(bnd_surf_currents);

			const fmm::ShCurrentSourcesPr bnd_vol_currents = (*it)->create_bnd_vol_currents();
			if(bnd_vol_currents!=NULL)src->add_sources(bnd_vol_currents);
		}

		// no sources?
		if(src->get_num_source_objects()==0)return;


		// setup the sources
		src->setup_sources();

		// multipole method using vector potentials
		const fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(src, tar, stngs_);
		mlfmm->set_no_allocate();

		// setup mlfmm and calculate
		mlfmm->setup(); 

		// check for cancel
		if(lg->is_cancelled())return;

		// run calculation
		mlfmm->calculate();

		// check for cancel
		if(lg->is_cancelled())return;

		// done
		return;
	}
	#endif

}}