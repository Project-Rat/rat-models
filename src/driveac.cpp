// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "driveac.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	DriveAC::DriveAC(){
		set_name("AC");
	}

	// constructor
	DriveAC::DriveAC(
		const fltp amplitude, 
		const fltp frequency, 
		const fltp phase, 
		const fltp offset) : DriveAC(){
		set_amplitude(amplitude); 
		set_frequency(frequency); 
		set_phase(phase); 
		set_offset(offset);
	}

	// factory
	ShDriveACPr DriveAC::create(){
		return std::make_shared<DriveAC>();
	}

	// factory
	ShDriveACPr DriveAC::create(
		const fltp amplitude, 
		const fltp frequency, 
		const fltp phase, 
		const fltp offset){
		return std::make_shared<DriveAC>(amplitude,frequency,phase,offset);
	}

	// boundaries for making a plot
	fltp DriveAC::get_v1() const{
		return -RAT_CONST(0.5)/frequency_;
	}
	
	fltp DriveAC::get_v2() const{
		return RAT_CONST(0.5)/frequency_;
	}


	// set frequency
	void DriveAC::set_frequency(const fltp frequency){
		frequency_ = frequency;
	}

	// set frequency
	void DriveAC::set_phase(const fltp phase){
		phase_ = phase;
	}

	// get frequency
	void DriveAC::set_phase_velocity(const fltp phase_velocity){
		phase_velocity_ = phase_velocity;
	}

	// set scaling
	void DriveAC::set_amplitude(const fltp amplitude){
		amplitude_ = amplitude;
	}

	// set offset
	void DriveAC::set_offset(const fltp offset){
		offset_ = offset;
	}



	// get frequency
	fltp DriveAC::get_frequency() const{
		return frequency_;
	}

	// get frequency
	fltp DriveAC::get_phase() const{
		return phase_;
	}

	// get frequency
	fltp DriveAC::get_phase_velocity() const{
		return phase_velocity_;
	}

	// get scaling
	fltp DriveAC::get_amplitude() const{
		return amplitude_;
	}

	// get offset
	fltp DriveAC::get_offset() const{
		return offset_;
	}

	// get current
	fltp DriveAC::get_scaling(
		const fltp position,
		const fltp time,
		const arma::uword derivative) const{

		// calculate and return value
		return (derivative==0 ? offset_ : RAT_CONST(0.0)) + 
			amplitude_*std::pow(arma::Datum<fltp>::tau*frequency_,derivative)*
			(derivative%4<2 ? RAT_CONST(1.0) : -RAT_CONST(1.0))*((derivative%2==0) ? 
			std::sin(arma::Datum<fltp>::tau*frequency_*position + phase_ + phase_velocity_*time) : 
			std::cos(arma::Datum<fltp>::tau*frequency_*position + phase_ + phase_velocity_*time));
	}

	// apply scaling for the input settings
	void DriveAC::rescale(const fltp scale_factor){
		amplitude_ *= scale_factor;
		offset_ *= scale_factor;
	}

	// get type
	std::string DriveAC::get_type(){
		return "rat::mdl::driveac";
	}

	// method for serialization into json
	void DriveAC::serialize(Json::Value &js, cmn::SList &list) const{
		Drive::serialize(js,list);
		
		js["type"] = get_type();
		js["amplitude"] = amplitude_;
		js["frequency"] = frequency_;
		js["phase"] = phase_;
		js["phase_velocity"] = phase_velocity_;
		js["offset"] = offset_;
	}

	// method for deserialisation from json
	void DriveAC::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		Drive::deserialize(js,list,factory_list,pth);
		set_amplitude(js["amplitude"].ASFLTP());
		set_frequency(js["frequency"].ASFLTP());
		set_phase(js["phase"].ASFLTP());
		set_phase_velocity(js["phase_velocity"].ASFLTP());
		set_offset(js["offset"].ASFLTP());
	}

}}