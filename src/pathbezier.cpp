// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathbezier.hh"

// rat-common headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// rat-mlfmm headers
#include "rat/mlfmm/extra.hh"

// rat-models headers
#include "darboux.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathBezier::PathBezier(){
		set_name("Bezier");
	}

	// constructor
	PathBezier::PathBezier(
		const arma::Col<fltp>::fixed<3>& pstart, 
		const arma::Col<fltp>::fixed<3>& lstart, 
		const arma::Col<fltp>::fixed<3>& nstart,
		const arma::Col<fltp>::fixed<3>& dstart,
		const arma::Col<fltp>::fixed<3>& pend, 
		const arma::Col<fltp>::fixed<3>& lend, 
		const arma::Col<fltp>::fixed<3>& nend,
		const arma::Col<fltp>::fixed<3>& dend,
		const arma::Mat<fltp> &Puv1,
		const arma::Mat<fltp> &Puv2,
		const fltp ell_trans1, const fltp ell_trans2, 
		const fltp element_size, 
		const fltp path_offset,
		const bool use_binormal) : PathBezier(){
		
		// start point
		pstart_ = pstart; lstart_ = lstart; nstart_ = nstart; dstart_ = dstart;

		// end point
		pend_ = pend; lend_ = lend; nend_ = nend; dend_ = dend;

		// set strengths
		Puv1_ = Puv1; 
		Puv2_ = Puv2;

		// transition length
		ell_trans1_ = ell_trans1;
		ell_trans2_ = ell_trans2;

		// element size
		element_size_ = element_size;
		path_offset_ = path_offset;
		use_binormal_ = use_binormal;
	}

	// factory
	ShPathBezierPr PathBezier::create(){
		return std::make_shared<PathBezier>();
	}

	// factory
	ShPathBezierPr PathBezier::create(
		const arma::Col<fltp>::fixed<3>& pstart, 
		const arma::Col<fltp>::fixed<3>& lstart, 
		const arma::Col<fltp>::fixed<3>& nstart,
		const arma::Col<fltp>::fixed<3>& pend, 
		const arma::Col<fltp>::fixed<3>& lend, 
		const arma::Col<fltp>::fixed<3>& nend,
		const arma::Mat<fltp> &Puv1,
		const arma::Mat<fltp> &Puv2,
		const fltp ell_trans1, const fltp ell_trans2, 
		const fltp element_size,
		const fltp path_offset,
		const bool use_binormal){
		return std::make_shared<PathBezier>(pstart,lstart,nstart,cmn::Extra::cross(nstart,lstart),
			pend,lend,nend,cmn::Extra::cross(nend,lend),Puv1,Puv2,ell_trans1,ell_trans2,element_size,path_offset,use_binormal);
	}

	// factory
	ShPathBezierPr PathBezier::create(
		const arma::Col<fltp>::fixed<3>& pstart, 
		const arma::Col<fltp>::fixed<3>& lstart, 
		const arma::Col<fltp>::fixed<3>& nstart,
		const arma::Col<fltp>::fixed<3>& dstart,
		const arma::Col<fltp>::fixed<3>& pend, 
		const arma::Col<fltp>::fixed<3>& lend, 
		const arma::Col<fltp>::fixed<3>& nend,
		const arma::Col<fltp>::fixed<3>& dend,
		const arma::Mat<fltp> &Puv1,
		const arma::Mat<fltp> &Puv2,
		const fltp ell_trans1, const fltp ell_trans2, 
		const fltp element_size,
		const fltp path_offset,
		const bool use_binormal){
		return std::make_shared<PathBezier>(pstart,lstart,nstart,dstart,
			pend,lend,nend,dend,Puv1,Puv2,ell_trans1,ell_trans2,element_size,path_offset,use_binormal);
	}

	// setters
	void PathBezier::set_planar_winding(const bool planar_winding){
		planar_winding_ = planar_winding;
	}

	void PathBezier::set_analytic_frame(const bool analytic_frame){
		analytic_frame_ = analytic_frame;
	}

	void PathBezier::set_num_sections(const arma::uword num_sections){
		if(num_sections<1)rat_throw_line("number of sections must be larger than one");
		num_sections_ = num_sections;
	}

	void PathBezier::set_ell_trans1(const fltp ell_trans1){
		ell_trans1_ = ell_trans1;
	}

	void PathBezier::set_ell_trans2(const fltp ell_trans2){
		ell_trans2_ = ell_trans2;
	}

	// getters
	fltp PathBezier::get_ell_trans1()const{
		return ell_trans1_;
	}

	fltp PathBezier::get_ell_trans2()const{
		return ell_trans2_;
	}

	arma::uword PathBezier::get_num_reg()const{
		return num_reg_;
	}

	fltp PathBezier::get_path_offset()const{
		return path_offset_;
	}

	fltp PathBezier::get_element_size()const{
		return element_size_;
	}

	// create control points by combining Puv1 and Puv2
	arma::Mat<fltp> PathBezier::create_control_points()const{
		// calculate start and end frame
		const arma::Col<fltp>::fixed<3> dstart = cmn::Extra::cross(nstart_,lstart_);
		const arma::Col<fltp>::fixed<3> dend = cmn::Extra::cross(nend_,lend_);

		// add points the u-direction is longitudinal and the v-direction is normal
		// for start points
		arma::Mat<fltp> P1(3,Puv1_.n_cols);
		for(arma::uword j=0;j<3;j++){
			P1.row(j) = pstart_(j) + lstart_(j)*(Puv1_.row(0)+ell_trans1_) + nstart_(j)*Puv1_.row(1);
			if(Puv1_.n_rows>2)P1.row(j) += dstart(j)*Puv1_.row(2);
		}

		// for end points
		arma::Mat<fltp> P2(3,Puv2_.n_cols);
		for(arma::uword j=0;j<3;j++){
			P2.row(j) = pend_(j) - lend_(j)*(Puv2_.row(0)+ell_trans2_) + nend_(j)*Puv2_.row(1);
			if(Puv2_.n_rows>2)P2.row(j) += dend(j)*Puv2_.row(2);
		}

		// join start and end-points
		arma::Mat<fltp> P = arma::join_horiz(P1,arma::fliplr(P2));

		// return control points
		return P;
	}

	// get frame
	ShFramePr PathBezier::create_frame(const MeshSettings &stngs) const{
		// create control points for quintic spline
		// arma::Mat<fltp> P(3,6);
		// P.col(0) = pstart_ + lstart_*ell_trans_;
		// P.col(1) = pstart_ + lstart_*ell_trans_ + lstart_*str1_;
		// P.col(2) = pstart_ + lstart_*ell_trans_ + lstart_*(str1_+str2_) - nstart_*str3_;
		// P.col(3) = pend_ - lend_*ell_trans_ - lend_*(str1_+str2_) - nend_*str4_;
		// P.col(4) = pend_ - lend_*ell_trans_ - lend_*str1_;
		// P.col(5) = pend_ - lend_*ell_trans_;

		// control points for septic spline
		// arma::Mat<fltp> P(3,8);
		// P.col(0) = pstart_ + lstart_*ell_trans_;
		// P.col(1) = pstart_ + lstart_*ell_trans_ + lstart_*str1_;
		// P.col(2) = pstart_ + lstart_*ell_trans_ + lstart_*(str1_+str2_) - nstart_*str3_;
		// P.col(3) = pstart_ + lstart_*ell_trans_ + lstart_*(str1_+2*str2_) - 2*nstart_*str3_;
		// P.col(4) = pend_ - lend_*ell_trans_ - lend_*(str1_+2*str2_) - 2*nend_*str4_;
		// P.col(5) = pend_ - lend_*ell_trans_ - lend_*(str1_+1*str2_) - nend_*str4_;
		// P.col(6) = pend_ - lend_*ell_trans_ - lend_*str1_;
		// P.col(7) = pend_ - lend_*ell_trans_;

		// control points for septic spline
		// arma::Mat<fltp> P(3,8);
		// P.col(0) = pstart_ + lstart_*ell_trans_;
		// P.col(1) = pstart_ + lstart_*ell_trans_ + lstart_*str1_;
		// P.col(2) = pstart_ + lstart_*ell_trans_ + lstart_*(str1_+str2_);
		// P.col(3) = pstart_ + lstart_*ell_trans_ + lstart_*(str1_+2*str2_) - nstart_*str3_;
		// P.col(4) = pend_ - lend_*ell_trans_ - lend_*(str1_+2*str2_) - nend_*str4_;
		// P.col(5) = pend_ - lend_*ell_trans_ - lend_*(str1_+1*str2_);
		// P.col(6) = pend_ - lend_*ell_trans_ - lend_*str1_;
		// P.col(7) = pend_ - lend_*ell_trans_;

		// control points for nonantic spline
		// arma::Mat<fltp> P(3,10);
		// P.col(0) = pstart_ + lstart_*ell_trans_;
		// P.col(1) = pstart_ + lstart_*ell_trans_ + lstart_*str1_;
		// P.col(2) = pstart_ + lstart_*ell_trans_ + lstart_*(str1_+str2_) - (1.0/5)*nstart_*str3_;
		// P.col(3) = pstart_ + lstart_*ell_trans_ + lstart_*(str1_+2*str2_) - (3.0/5)*nstart_*str3_;
		// P.col(4) = pstart_ + lstart_*ell_trans_ + lstart_*(str1_+3*str2_) - 1.0*nstart_*str3_;
		// P.col(5) = pend_ - lend_*ell_trans_ - lend_*(str1_+3*str2_) - 1.0*nend_*str4_;
		// P.col(6) = pend_ - lend_*ell_trans_ - lend_*(str1_+2*str2_) - (3.0/5)*nend_*str4_;
		// P.col(7) = pend_ - lend_*ell_trans_ - lend_*(str1_+str2_) - (1.0/5)*nend_*str4_;
		// P.col(8) = pend_ - lend_*ell_trans_ - lend_*str1_;
		// P.col(9) = pend_ - lend_*ell_trans_ ;

		// Thomas Nes Point definition 35e-3, 14e-3
		// arma::Mat<fltp> P(3,16);
		// P.col(0) = pstart_;
		// P.col(1) = pstart_ + (lstart_*1*str1_);
		// P.col(2) = pstart_ + (lstart_*2*str1_);
		// P.col(3) = pstart_ + (lstart_*3*str1_);
		// P.col(4) = pstart_ + (lstart_*(4*str1_) + 1*nstart_*str3_);
		// P.col(5) = pstart_ + (lstart_*(5*str1_) + 2*nstart_*str3_);
		// P.col(6) = pstart_ + (lstart_*(6*str1_) + 4*nstart_*str3_);
		// P.col(7) = pstart_ + (lstart_*(6*str1_) + 10*nstart_*str3_);
		// P.col(8) = pend_ - (lend_*(6*str2_) - 10*nend_*str4_);
		// P.col(9) = pend_ - (lend_*(6*str2_) - 4*nend_*str4_);
		// P.col(10) = pend_ - (lend_*(5*str2_) - 2*nend_*str4_);
		// P.col(11) = pend_ - (lend_*(4*str2_) - 1*nend_*str4_);
		// P.col(12) = pend_ - (lend_*3*str2_);
		// P.col(13) = pend_ - (lend_*2*str2_);
		// P.col(14) = pend_ - (lend_*1*str2_);
		// P.col(15) = pend_;

		// first set of points
		// const arma::Mat<fltp> Puv1_ = arma::reshape(arma::Col<fltp>{
		// 	0.0,0.0, 1*str1_,0, 2*str1_,0, 3*str1_,0, 4*str1_,1*str3_, 
		// 	5*str1_,2*str3_, 6*str1_,4*str3_, 6*str1_,10*str3_},2,8);
		// const arma::Mat<fltp> Puv2_ = arma::reshape(arma::Col<fltp>{
		// 	0.0,0.0, 1*str2_,0, 2*str2_,0, 3*str2_,0, 4*str2_,1*str4_, 
		// 	5*str2_,2*str4_, 6*str2_,4*str4_, 6*str2_,10*str4_},2,8);

		// calculate start and end frame
		const arma::Col<fltp>::fixed<3> dstart = cmn::Extra::cross(nstart_,lstart_);
		const arma::Col<fltp>::fixed<3> dend = cmn::Extra::cross(nend_,lend_);

		// create control points
		const arma::Mat<fltp> P = create_control_points();
		
		// create times
		const arma::Row<fltp> t = regular_times(P, element_size_, path_offset_, num_reg_);

		// number of times
		// const arma::uword num_times = t.n_elem;

		// create spline and its derivatives
		//arma::Mat<fltp> R1,V1,A1; create_bezier(R1,V1,A1,t,P);
		const arma::field<arma::Mat<fltp> > C = create_bezier_tn(t,P,4);
		
		// get coordinates
		arma::Mat<fltp> R = C(0); 

		// get derivatives
		const arma::Mat<fltp>& V = C(1); // velocity
		const arma::Mat<fltp>& A = C(2); // acceleration
		const arma::Mat<fltp>& J = C(3); // jerk
		


		// create darboux frame
		Darboux db(V,A,J);
		db.setup(analytic_frame_); 
		db.set_first_transverse(dstart);
		db.set_last_transverse(dend);

		// correct darboux generators for sign flips and take the sign at the end
		const int end_sgn = db.correct_sign(dstart);

		// get data from darboux
		arma::Mat<fltp> D = db.get_transverse(use_binormal_);
		arma::Mat<fltp> L = db.get_longitudinal();
		// arma::Mat<fltp> N = db.get_normal();

		// check output
		assert(L.is_finite()); assert(D.is_finite());

		// allocate number of transition elements
		arma::uword num_trans1 = 0llu, num_trans2 = 0llu;

		// check if transition region needed
		if(ell_trans1_>0.0){
			// number of transition nodes
			num_trans1 = std::max(2llu,static_cast<arma::uword>(std::ceil(RAT_CONST(1.5)*ell_trans1_/element_size_)));

			// allocate transition
			arma::Mat<fltp> R1trans(3,num_trans1);
			arma::Mat<fltp> D1trans(3,num_trans1);
			arma::Mat<fltp> L1trans(3,num_trans1);

			// times for transition
			const arma::Row<fltp> ttrans = arma::linspace<arma::Row<fltp> >(RAT_CONST(0.0),RAT_CONST(1.0),num_trans1);

			// calculate transition
			for(arma::uword i=0;i<num_trans1;i++){
				const fltp a = RAT_CONST(1.0)-ttrans(i), b = ttrans(i);
				R1trans.col(i) = a*pstart_ + b*R.col(0);
				D1trans.col(i) = a*dstart_ + b*D.col(0);
			}
			L1trans.each_col() = lstart_;

			// find vectors with zero length
			arma::Col<arma::uword> idx = arma::find(cmn::Extra::vec_norm(D1trans)<1e-9);
			D1trans.cols(idx) = arma::repmat(arma::Col<fltp>{0,0,1},1,idx.n_elem);

			// check vector length
			assert(arma::all(cmn::Extra::vec_norm(D1trans)>1e-14));

			// atach transition
			R = arma::join_horiz(R1trans.head_cols(num_trans1-1), R);
			D = arma::join_horiz(D1trans.head_cols(num_trans1-1), D);
			L = arma::join_horiz(L1trans.head_cols(num_trans1-1), L);
		}

		// check if transition region needed
		if(ell_trans2_>0.0){
			// number of transition nodes
			num_trans2 = std::max(2llu,static_cast<arma::uword>(std::ceil(RAT_CONST(1.5)*ell_trans2_/element_size_)));

			// allocate transition
			arma::Mat<fltp> R2trans(3,num_trans2);
			arma::Mat<fltp> D2trans(3,num_trans2);
			arma::Mat<fltp> L2trans(3,num_trans2);

			// times for transition
			const arma::Row<fltp> ttrans = arma::linspace<arma::Row<fltp> >(RAT_CONST(0.0),RAT_CONST(1.0),num_trans2);

			// calculate transition
			for(arma::uword i=0;i<num_trans2;i++){
				const fltp a = RAT_CONST(1.0)-ttrans(i), b = ttrans(i);
				R2trans.col(i) = a*R.tail_cols(1) + b*pend_;
				D2trans.col(i) = a*D.tail_cols(1) + b*end_sgn*dend_;
			}
			L2trans.each_col() = lend_;

			// find vectors with zero length
			arma::Col<arma::uword> idx = arma::find(cmn::Extra::vec_norm(D2trans)<1e-9);
			D2trans.cols(idx) = arma::repmat(arma::Col<fltp>{0,0,1},1,idx.n_elem);

			// check vector length
			assert(arma::all(cmn::Extra::vec_norm(D2trans)>1e-14));

			// atach transition
			R = arma::join_horiz(R, R2trans.tail_cols(num_trans2-1));
			D = arma::join_horiz(D, D2trans.tail_cols(num_trans2-1));
			L = arma::join_horiz(L, L2trans.tail_cols(num_trans2-1));
		}

		// Calculate normal vector
		assert(L.is_finite()); assert(D.is_finite());
		arma::Mat<fltp> N = cmn::Extra::normalize(cmn::Extra::cross(L,D));
		assert(N.is_finite());

		// fix start and end by overriding them
		L.col(0) = lstart_; N.col(0) = nstart_; D.col(0) = dstart_;
		L.tail_cols(1) = lend_; N.tail_cols(1) = end_sgn*nend_; D.tail_cols(1) = end_sgn*dend_;

		// allocate block direction vectors
		arma::Mat<fltp> B;

		// in case of planar windings
		if(planar_winding_){
			// get out of plane vector
			const arma::Col<fltp>::fixed<3> plane_vec = cmn::Extra::cross(lstart_,nstart_);
			assert(std::abs(arma::as_scalar(cmn::Extra::dot(plane_vec,lend_))-RAT_CONST(1.0))>1e-6);

			// calculate block vector
			B = cmn::Extra::normalize(cmn::Extra::cross(arma::repmat(plane_vec,1,L.n_cols),L));

			// B = (B+N)/2;
		}

		// non-planar winding
		else{
			B = N;
		}

		// check sizes
		assert(R.n_cols==L.n_cols); assert(R.n_cols==N.n_cols);
		assert(R.n_cols==D.n_cols); assert(R.n_cols==B.n_cols);

		// create field arrays
		// arma::field<arma::Mat<fltp> > 
		// 	Rfld(1,num_sections_), Lfld(1,num_sections_), 
		// 	Nfld(1,num_sections_), Dfld(1,num_sections_), 
		// 	Bfld(1,num_sections_);

		// // create indexes at which we want to split the sections
		// arma::Row<arma::uword> idx_divide = 
		// 	arma::regspace<arma::Row<arma::uword> >(0,num_sections_)*
		// 	std::floor(num_times/num_sections_);
		// idx_divide(num_sections_) = num_times-1;

		// // split sections
		// for(arma::uword i=0;i<num_sections_;i++){
		// 	const arma::uword idx1 = idx_divide(i);
		// 	const arma::uword idx2 = idx_divide(i+1);
		// 	Rfld(i) = R.cols(idx1,idx2); Lfld(i) = L.cols(idx1,idx2);
		// 	Nfld(i) = N.cols(idx1,idx2); Dfld(i) = D.cols(idx1,idx2);
		// 	Bfld(i) = B.cols(idx1,idx2);
		// }

		// subdivide
		//ShFramePr frame = Frame::create(Rfld,Lfld,Nfld,Dfld,Bfld);

		// check handedness
		assert(N.is_finite()); assert(L.is_finite()); assert(D.is_finite());
		assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N,L),D)>RAT_CONST(0.0)));

		// create frame and return 
		const ShFramePr frame1 = Frame::create(R,L,N,D,B,num_sections_);

		// apply transformations
		frame1->apply_transformations(get_transformations(), stngs.time);

		// // number of transition elements
		// // this is not possible anymore 
		// if(num_trans1!=0 && num_trans2!=0){
		// 	const ShFramePr frame2 = Frame::create({
		// 		frame1->get_sub(0,0,num_trans1-1),
		// 		frame1->get_sub(0,num_trans1-1,R.n_cols-num_trans2),
		// 		frame1->get_sub(0,R.n_cols-num_trans2,R.n_cols-1)});
		// 	return frame2;
		// }else if(num_trans1!=0){
		// 	const ShFramePr frame2 = Frame::create({
		// 		frame1->get_sub(0,0,num_trans1-1),
		// 		frame1->get_sub(0,num_trans1-1,R.n_cols-1)});
		// 	return frame2;
		// }else if(num_trans2!=0){
		// 	const ShFramePr frame2 = Frame::create({
		// 		frame1->get_sub(0,0,R.n_cols-num_trans2),
		// 		frame1->get_sub(0,R.n_cols-num_trans2,R.n_cols-1)});
		// 	return frame2;
		// }

		// return the frame
		return frame1;
	}

	// regular time function
	arma::Row<fltp> PathBezier::regular_times(
		const arma::Mat<fltp> &P,
		const fltp element_size,
		const fltp path_offset,
		const arma::uword num_reg){

		// check input
		assert(P.is_finite());

		// calculate line with regular spaced times
		const arma::Row<fltp> treg = arma::linspace<arma::Row<fltp> >(1e-9,1.0-1e-9,num_reg);

		// create spline
		//arma::Mat<fltp> R,V,A;
		//create_bezier(R,V,A,treg,P);
		const arma::field<arma::Mat<fltp> > C = create_bezier_tn(treg,P,3);
		// const arma::Mat<fltp> R = C(0);
		const arma::Mat<fltp>& V = C(1);
		const arma::Mat<fltp>& A = C(2);

		// calculate radius of curvature from velocity and acceleration
		const arma::Row<fltp> radius = arma::pow(cmn::Extra::vec_norm(V),3)/cmn::Extra::vec_norm(cmn::Extra::cross(V,A));

		// calculate scaling factor based on radius
		// the clamp here can be debated
		const arma::Row<fltp> factor = (radius + path_offset)/radius;

		// calculate spacing
		const arma::Row<fltp> Vnorm = factor%cmn::Extra::vec_norm(V);
		const arma::Row<fltp> s = arma::join_horiz(arma::Row<fltp>{0}, 
			arma::cumsum(arma::diff(treg,1,1)%(Vnorm.head_cols(num_reg-1)+Vnorm.tail_cols(num_reg-1)/2)));

		// divide into elements
		const arma::uword num_times = std::max(2llu,static_cast<arma::uword>(std::ceil(s.back()/element_size)));

		// find position along length
		assert(s.is_finite());
		const arma::Row<fltp> sreg = arma::linspace<arma::Row<fltp> >(0,s.back(),num_times);

		// check
		assert(sreg.is_finite());

		// get 
		arma::Col<fltp> tset;
 		cmn::Extra::interp1(s.t(),treg.t(),sreg.t(),tset,"linear",true);

		// // acceleration fix/cheat (zero acceleration gives weird results)
		// const arma::Row<arma::uword> idx = arma::find(cmn::Extra::vec_norm(A)<1e-9).t();
		// if(!idx.is_empty())A.cols(idx) = A.cols(idx-1);

		
		// // calculate length
		// const arma::Row<fltp> ell = factor%arma::sqrt(arma::sum(arma::pow(arma::diff(R,1,1),2),0));

		// // calculate number of times required
		// const arma::uword num_times = std::max(2,(int)(std::ceil(arma::sum(ell)/element_size)));

		// // calculate line with regular spaced times
		// const arma::Row<fltp> treq = arma::linspace<arma::Row<fltp> >(0,RAT_CONST(1.0),num_times);

		// // calculate accumalation of length
		// arma::Row<fltp> rp(num_reg_,arma::fill::zeros);
		// rp.tail_cols(num_reg_-1) = arma::cumsum(ell)/arma::sum(ell);

		// // allocate output
		// arma::Col<fltp> tset;

		// // use interpolation
		// arma::interp1(rp.t(),treg.t(),treq.t(),tset,"linear");

		// fix first and last point
		tset(0) = RAT_CONST(1e-9); tset.tail(1) = RAT_CONST(1.0) - RAT_CONST(1e-9);

		// check if sorted
		assert(tset.is_sorted("ascend"));

		// return time
		return tset.t();
	}


	// bezier function
	// general version
	void PathBezier::create_bezier(
		arma::Mat<fltp> &R,
		arma::Mat<fltp> &V,
		arma::Mat<fltp> &A,
		const arma::Row<fltp> &t, 
		const arma::Mat<fltp> &P){

		// get dimensions
		const arma::uword num_dim = P.n_rows;
		const arma::uword num_times = t.n_elem;
		const arma::uword nmax = P.n_cols-1; // the order of the spline
		const fltp spline_order = fltp(nmax);

		// allocate to zeros
		R.zeros(num_dim, num_times);
		V.zeros(num_dim, num_times);
		A.zeros(num_dim, num_times);

		// create factorial list
		const arma::Col<fltp> facs = fmm::Extra::factorial(int(nmax));

		// walk over source points
		for(arma::sword i=0;i<=arma::sword(nmax);i++){
			// calculate binomial coefficient
			fltp nk = facs(nmax)/(facs(i)*facs(nmax-i));

			// walk over dimensions
			for(arma::uword j=0;j<num_dim;j++){
				// calculate coordinates
				R.row(j) += nk*P(j,i)*arma::pow(RAT_CONST(1.0)-t,spline_order-i)%arma::pow(t,fltp(i));

				// calculate velocity (first derivative)
				V.row(j) += nk*P(j,i)*(spline_order*t-fltp(i))%arma::pow(RAT_CONST(1.0)-t,spline_order-fltp(i))%arma::pow(t,fltp(i)-1)/(t-RAT_CONST(1.0));

				// calculate acceleration (second derivative)
				A.row(j) += nk*P(j,i)*((-1+fltp(i))*fltp(i)*arma::pow(RAT_CONST(1.0)-t,-fltp(i)+spline_order)%arma::pow(t,-2+fltp(i)) - 
					2*i*(-fltp(i)+spline_order)*arma::pow(RAT_CONST(1.0)-t,-1-i+spline_order)%arma::pow(t,-1+fltp(i)) + 
					(-1-fltp(i)+spline_order)*(-fltp(i)+spline_order)*arma::pow(RAT_CONST(1.0)-t,-2-i+spline_order)%arma::pow(t,fltp(i)));
			}
		}

		// acceleration fix/cheat (zero acceleration gives weird results)
		const arma::Row<arma::uword> idx = arma::find(cmn::Extra::vec_norm(A)<RAT_CONST(1e-9)).t();
		if(!idx.is_empty())A.cols(idx) = A.cols(idx-1);
	}

	// bezier function with casteljau's algorithm
	// https://pages.mtu.edu/~shene/COURSES/cs3621/NOTES/spline/Bezier/bezier-der.html
	arma::field<arma::Mat<fltp> > PathBezier::create_bezier_tn(
		const arma::Row<fltp> &t, const arma::Mat<fltp> &P, const arma::uword depth){

		// check output
		assert(P.is_finite());

		// get dimensions
		const arma::uword num_dim = P.n_rows;
		const arma::uword num_times = t.n_elem;
		const arma::uword n = P.n_cols-1; // the order of the spline

		// create factorial list
		const arma::Col<fltp> facs = fmm::Extra::factorial(int(n));

		// allocate
		arma::field<arma::Mat<fltp> > C(depth);

		// store p for recursive calculation
		arma::Mat<fltp> D = P;

		// keep track of prefac
		fltp prefac = RAT_CONST(1.0);

		// calculate coordinates
		for(arma::uword k=0;k<depth;k++){
			// allocate spline
			C(k).zeros(num_dim,num_times);

			// walk over n
			for(arma::uword i=0;i<=n-k;i++){
				// calculate binomial coefficient
				const fltp nk = facs(n-k)/(facs(i)*facs(n-k-i));

				// calculate bezier function
				const arma::Row<fltp> B = nk*arma::pow(RAT_CONST(1.0)-t,n-k-fltp(i))%arma::pow(t,fltp(i));

				// walk over dimensions
				for(arma::uword j=0;j<num_dim;j++){
					// calculate coordinate or derivatives thereof
					C(k).row(j) += prefac*D(j,i)*B;
				}
			}

			// check output
			assert(C(k).is_finite());
			
			// casteljau's algorithm	
			D = arma::diff(D,1,1);

			// update prefactor
			prefac *= (n-k);
		}

		// return the spline and its derivatives
		return C;
	}

	// inverse calculation points from derrivatives
	// note that this only creates half of the points
	// the first column of the input is the coordinate
	// of the start point itself, the remaining columns
	// are the respective derivatives
	arma::Mat<fltp> PathBezier::create_control_points(const arma::Mat<fltp> &D){
		// number of points and max number of derivatives
		const arma::uword np = 2*(D.n_cols-1)+1; 
		const arma::uword max_depth = D.n_cols-1;

		// prefactor
		arma::Col<rat::fltp> prefac(max_depth+1);
		prefac(0) = 1.0;
		for(arma::uword k=0;k<max_depth;k++)
			prefac(k+1) = prefac(k)*(np-k);

		// calculate differentiation table recursively
		// + 1*P(0) 											= D(0)/prefac(0);    +1
		// - 1*P(0) + 1*P(1)  									= D(1)/prefac(1);    -1 +1
		// + 1*P(0) - 2*P(1) + 1*P(2)							= D(2)/prefac(2);    +1 -2 +1
		// - 1*P(0) + 3*P(1) - 3*P(2) + 1*P(3) 					= D(3)/prefac(3);    -1 +3 -3 +1
		// + 1*P(0) - 4*P(1) + 6*P(2) - 4*P(3) + 1*P(4)   		= D(4)/prefac(4);    +1 -4 +6 -4 +1
		const arma::uword psize = rat::fmm::Extra::hfnm2fidx(int(max_depth),int(max_depth))+1;
		arma::Col<rat::fltp> fk(psize);
		for(arma::uword n=0;n<=max_depth;n++){
			fk(rat::fmm::Extra::hfnm2fidx(int(n),int(n))) = 1;
		}
		for(arma::uword n=1;n<=max_depth;n++){
			for(arma::uword m=0;m<n;m++)fk(rat::fmm::Extra::hfnm2fidx(int(n),int(m))) += -fk(rat::fmm::Extra::hfnm2fidx(int(n-1),int(m)));
			for(arma::uword m=1;m<n;m++)fk(rat::fmm::Extra::hfnm2fidx(int(n),int(m))) += fk(rat::fmm::Extra::hfnm2fidx(int(n-1),int(m-1)));
		}

		// allocate P
		arma::Mat<rat::fltp> P(D.n_rows, max_depth+1, arma::fill::zeros);

		// create the points
		for(arma::uword n=0;n<=max_depth;n++){
			P.col(n) += D.col(n)/prefac(n);
			for(arma::uword m=0;m<n;m++){
				P.col(n) += -fk(rat::fmm::Extra::hfnm2fidx(int(n),int(n)))*fk(rat::fmm::Extra::hfnm2fidx(int(n),int(m)))*P.col(m);
			}
		}

		// return the point list
		return P;
	}

	// calculate smoothed derivative
	// the order is the order of the bezier spline fitted onto a moving window
	arma::field<arma::Mat<fltp> > PathBezier::diff(
		const arma::Mat<fltp>&R, 
		const arma::uword order, 
		const arma::uword depth){

		// allocate derivative
		arma::field<arma::Mat<fltp> > dR(depth);
		for(arma::uword i=0;i<depth;i++)
			dR(i).set_size(3,R.n_cols);

		// perform spline fitting
		for(arma::uword i=0;i<R.n_cols;i++){
			// define moving window
			const arma::uword idx1 = std::min(
				std::max(arma::sword(0),arma::sword(R.n_cols)-1-2*arma::sword(order)), 
				std::max(arma::sword(0),arma::sword(i)-arma::sword(order)));
			const arma::uword idx2 = std::min(idx1+2*order, R.n_cols-1);

			// create bspline
			const arma::Row<fltp> t{(fltp(i) - fltp(idx1))/(fltp(idx2) - fltp(idx1))};

			// calculate derivatives
			const arma::field<arma::Mat<fltp> > C = create_bezier_tn(t, R.cols(idx1,idx2), depth);
			assert(C.n_elem>1);

			// store derivative
			for(arma::uword j=0;j<depth;j++)dR(j).col(i) = C(j).col(0);
		}

		// return derivative
		return dR;
	}


	// get type
	std::string PathBezier::get_type(){
		return "rat::mdl::pathbezier";
	}

	// method for serialization into json
	void PathBezier::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents first 
		// this allows the type to be overridden
		Path::serialize(js,list);
		Transformations::serialize(js,list);

		// serialization type
		js["type"] = get_type();

		// start point
		js["pstartx"] = pstart_(0); js["pstarty"] = pstart_(1); js["pstartz"] = pstart_(2);
		js["nstartx"] = nstart_(0); js["nstarty"] = nstart_(1); js["nstartz"] = nstart_(2);
		js["lstartx"] = lstart_(0); js["lstarty"] = lstart_(1); js["lstartz"] = lstart_(2);
		js["dstartx"] = dstart_(0); js["dstarty"] = dstart_(1); js["dstartz"] = dstart_(2);

		// end point
		js["pendx"] = pend_(0); js["pendy"] = pend_(1); js["pendz"] = pend_(2);
		js["nendx"] = nend_(0); js["nendy"] = nend_(1); js["nendz"] = nend_(2);
		js["lendx"] = lend_(0); js["lendy"] = lend_(1); js["lendz"] = lend_(2);
		js["dendx"] = dend_(0); js["dendy"] = dend_(1); js["dendz"] = dend_(2);

		// other settings
		js["analytic_frame"] = analytic_frame_;
		js["num_reg"] = static_cast<unsigned int>(num_reg_);
		js["ell_trans1"] = ell_trans1_;
		js["ell_trans2"] = ell_trans2_;
		js["element_size"] = element_size_;
		js["planar_winding"] = planar_winding_;
		js["path_offset"] = path_offset_;
		js["num_sections"] = static_cast<unsigned int>(num_sections_);
		js["use_binormal"] = use_binormal_;

		// control points
		for(arma::uword i=0;i<Puv1_.n_cols;i++){
			js["Pu1"].append(Puv1_(0,i));
			js["Pv1"].append(Puv1_(1,i));
		}

		// control points
		for(arma::uword i=0;i<Puv2_.n_cols;i++){
			js["Pu2"].append(Puv2_(0,i));
			js["Pv2"].append(Puv2_(1,i));
		}

	}

	// method for deserialisation from json
	void PathBezier::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		
		// parent objects
		Path::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);

		// start point
		pstart_(0) = js["pstartx"].ASFLTP(); pstart_(1) = js["pstarty"].ASFLTP(); pstart_(2) = js["pstartz"].ASFLTP();
		nstart_(0) = js["nstartx"].ASFLTP(); nstart_(1) = js["nstarty"].ASFLTP(); nstart_(2) = js["nstartz"].ASFLTP();
		lstart_(0) = js["lstartx"].ASFLTP(); lstart_(1) = js["lstarty"].ASFLTP(); lstart_(2) = js["lstartz"].ASFLTP();
		dstart_(0) = js["dstartx"].ASFLTP(); dstart_(1) = js["dstarty"].ASFLTP(); dstart_(2) = js["dstartz"].ASFLTP();

		// end point
		pend_(0) = js["pendx"].ASFLTP(); pend_(1) = js["pendy"].ASFLTP(); pend_(2) = js["pendz"].ASFLTP();
		nend_(0) = js["nendx"].ASFLTP(); nend_(1) = js["nendy"].ASFLTP(); nend_(2) = js["nendz"].ASFLTP();
		lend_(0) = js["lendx"].ASFLTP(); lend_(1) = js["lendy"].ASFLTP(); lend_(2) = js["lendz"].ASFLTP();
		dend_(0) = js["dendx"].ASFLTP(); dend_(1) = js["dendy"].ASFLTP(); dend_(2) = js["dendz"].ASFLTP();

		// other settings
		analytic_frame_ = js["analytic_frame"].asBool();
		num_reg_ = js["num_reg"].asUInt64();
		// backwards compatibility with 2.015.1
		if(js.isMember("ell_trans")){
			set_ell_trans1(js["ell_trans"].ASFLTP());
			set_ell_trans2(js["ell_trans"].ASFLTP());
		}else{
			set_ell_trans1(js["ell_trans1"].ASFLTP());
			set_ell_trans2(js["ell_trans2"].ASFLTP());
		}
		element_size_ = js["element_size"].ASFLTP();
		planar_winding_ = js["planar_winding"].ASFLTP();
		path_offset_ = js["path_offset"].ASFLTP();
		num_sections_ = js["num_sections"].asUInt64();
		use_binormal_ = js["use_binormal"].asBool();

		// control points
		Puv1_.set_size(2,js["Pu1"].size());
		Puv2_.set_size(2,js["Pu2"].size());
		arma::uword idx = 0;
		for(auto it = js["Pu1"].begin();it!=js["Pu1"].end();it++)Puv1_(0,idx++) = (*it).ASFLTP();
		idx = 0;
		for(auto it = js["Pv1"].begin();it!=js["Pv1"].end();it++)Puv1_(1,idx++) = (*it).ASFLTP();
		idx = 0;
		for(auto it = js["Pu2"].begin();it!=js["Pu2"].end();it++)Puv2_(0,idx++) = (*it).ASFLTP();
		idx = 0;
		for(auto it = js["Pv2"].begin();it!=js["Pv2"].end();it++)Puv2_(1,idx++) = (*it).ASFLTP();
	}

}}