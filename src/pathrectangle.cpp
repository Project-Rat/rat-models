// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathrectangle.hh"

// rat-common headers
#include "rat/common/extra.hh"

// rat-model headers
#include "pathgroup.hh"
#include "patharc.hh"
#include "pathstraight.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathRectangle::PathRectangle(){
		// name
		set_name("Rectangle");
	}

	// constructor with dimension input
	PathRectangle::PathRectangle(
		const fltp width, const fltp height, 
		const fltp radius, const fltp element_size) : PathRectangle(){

		// set parameters
		set_width(width); set_height(height); 
		set_radius(radius); set_element_size(element_size); 
	}

	// factory
	ShPathRectanglePr PathRectangle::create(){
		return std::make_shared<PathRectangle>();
	}

	// factory with dimension input
	ShPathRectanglePr PathRectangle::create(
		const fltp width, const fltp height, 
		const fltp radius, const fltp element_size){
		return std::make_shared<PathRectangle>(width,height,radius,element_size);
	}

	// set coil width
	void PathRectangle::set_width(const fltp width){
		width_ = width;
	}

	// set coil height
	void PathRectangle::set_height(const fltp height){
		height_ = height;
	}

	// set radius
	void PathRectangle::set_radius(const fltp radius){
		radius_ = radius;
	}

	// set element size 
	void PathRectangle::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	// path offset
	void PathRectangle::set_offset(const fltp offset){
		offset_ = offset;
	}

	// set bending radius
	void PathRectangle::set_bending_radius(const fltp bending_radius){
		bending_radius_ = bending_radius;
	}

	// get properties
	fltp PathRectangle::get_radius() const{
		return radius_;
	}

	// // get number of sections
	// arma::uword get_num_sections() const{
	// 	return num_sections_;
	// }

	// get rectangle width
	fltp PathRectangle::get_width() const{
		return width_;
	}

	// get rectangle height
	fltp PathRectangle::get_height() const{
		return height_;
	}

	// get element size
	fltp PathRectangle::get_element_size() const{
		return element_size_;
	}

	// get offset
	fltp PathRectangle::get_offset() const{
		return offset_;
	}

	// get bending radius
	fltp PathRectangle::get_bending_radius() const{
		return bending_radius_;
	}

	// get frame
	ShFramePr PathRectangle::create_frame(const MeshSettings &stngs) const{
		// check input
		is_valid(true);

		// calculate lengths of straight sections
		const fltp ell1 = height_-2*radius_;
		const fltp ell2 = width_-2*radius_;

		// final shape check
		if(ell1<-1e-9)rat_throw_line("height must be larger than two radii");
		if(ell2<-1e-9)rat_throw_line("width must be larger than two radii");

		// create an arc
		const ShPathArcPr path_arc = PathArc::create(radius_,arma::Datum<fltp>::pi/2,element_size_,offset_);

		// create straight section
		ShPathPr path_straight1A, path_straight1B;
		if(bending_radius_==0){
			if(ell1>0){
				path_straight1A = PathStraight::create(ell1/2, element_size_);
				path_straight1B = path_straight1A;
			}
		}

		// when straight section is curved
		else{
			const fltp arc_length = ell1/(2*std::abs(bending_radius_));
			const fltp sgn = cmn::Extra::sign(bending_radius_);
			const fltp radA = std::abs(bending_radius_)-sgn*(ell2/2+radius_);
			const fltp radB = std::abs(bending_radius_)+sgn*(ell2/2+radius_);
			if(radA<=0)rat_throw_line("bending radius not feasible");
			if(radB<=0)rat_throw_line("bending radius not feasible");
			path_straight1A = PathArc::create(radA, -sgn*arc_length, element_size_);
			path_straight1B = PathArc::create(radB, sgn*arc_length, element_size_);
		}
		

		// second straight path
		ShPathStraightPr path_straight2;
		if(ell2>0)path_straight2 = PathStraight::create(ell2/2,element_size_);

		// create unified path
		ShPathGroupPr pathgroup = PathGroup::create();
		if(ell1>1e-9){
			pathgroup->add_path(path_straight1A);
		}
		pathgroup->add_path(path_arc);
		if(ell2>1e-9){
			pathgroup->add_path(path_straight2); 
			pathgroup->add_path(path_straight2);
		}
		pathgroup->add_path(path_arc);
		if(ell1>1e-9){
			pathgroup->add_path(path_straight1B);
			pathgroup->add_path(path_straight1B);
		}
		pathgroup->add_path(path_arc);
		if(ell2>1e-9){
			pathgroup->add_path(path_straight2);
			pathgroup->add_path(path_straight2);
		}
		pathgroup->add_path(path_arc);
		if(ell1>1e-9){
			pathgroup->add_path(path_straight1A);
		}
		pathgroup->add_translation(width_/2,0,0);

		// create frame
		ShFramePr frame = pathgroup->create_frame(stngs);

		// apply transformations
		frame->apply_transformations(get_transformations(), stngs.time);

		// return frame
		return frame;
	}

	// validity check
	// if(width<=0)rat_throw_line("coil width must be larger than zero");
	// if(height<=0)rat_throw_line("coil height must be larger than zero");
	// if(radius<=0)rat_throw_line("coil radius must be larger than zero");
	// if(element_size<=0)rat_throw_line("element size must be larger than zero");
	bool PathRectangle::is_valid(const bool enable_throws) const{
		if(width_<=0){if(enable_throws){rat_throw_line("width must be larger than zero");} return false;};
		if(height_<=0){if(enable_throws){rat_throw_line("height must be larger than zero");} return false;};
		if(radius_<=0){if(enable_throws){rat_throw_line("radius must be larger than zero");} return false;};
		if(width_<2*radius_){if(enable_throws){rat_throw_line("width must be smaller than two radii");} return false;};
		if(height_<2*radius_){if(enable_throws){rat_throw_line("height must be smaller than two radii");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		if(std::abs(bending_radius_)<=width_/2 && bending_radius_!=0){if(enable_throws){rat_throw_line("bending radius too small");} return false;};
		return true;
	}


	// get type
	std::string PathRectangle::get_type(){
		return "rat::mdl::pathrectangle";
	}

	// method for serialization into json
	void PathRectangle::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Path::serialize(js,list);
		Transformations::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["width"] = width_;
		js["height"] = height_;
		js["radius"] = radius_;
		js["element_size"] = element_size_;
		js["offset"] = offset_;
		js["bending_radius"] = bending_radius_;
	}

	// method for deserialisation from json
	void PathRectangle::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent
		Path::deserialize(js,list,factory_list, pth);
		Transformations::deserialize(js,list,factory_list, pth);
		
		// properties
		set_width(js["width"].ASFLTP());
		set_height(js["height"].ASFLTP());
		set_radius(js["radius"].ASFLTP());
		set_element_size(js["element_size"].ASFLTP());
		set_offset(js["offset"].ASFLTP());
		set_bending_radius(js["bending_radius"].ASFLTP());
	}

}}
