// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calcmesh.hh"

// nonlinear solver headers
#ifdef ENABLE_NL_SOLVER
#include "rat/nl/nonlinsolver.hh"
#include "rat/nl/solvercache.hh"
#endif

// models headers
#include "nldata.hh"
#include "coildata.hh"
#include "bardata.hh"
#include "extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcMesh::CalcMesh(){
		set_name("Mesh");
	}

	// constructor immediately setting base and cross section
	CalcMesh::CalcMesh(const ShModelPr &model) : CalcMesh(){
		set_model(model);
	}

	// factory with input
	ShCalcMeshPr CalcMesh::create(){
		return std::make_shared<CalcMesh>();
	}

	// factory with input
	ShCalcMeshPr CalcMesh::create(const ShModelPr &model){
		return std::make_shared<CalcMesh>(model);
	}

	// no time calculation t=0
	std::list<ShDataPr> CalcMesh::calculate(
		const fltp time, 
		const cmn::ShLogPr &lg, 
		const ShSolverCachePr& cache){

		// calculate
		std::list<ShMeshDataPr> mesh_data = calculate_mesh(time,lg,cache);
		
		// convert to data 
		std::list<ShDataPr> data;
		for(auto it = mesh_data.begin();it!=mesh_data.end();it++)
			data.push_back(*it);

		// return data
		return data;
	}

	// run calculation
	std::list<ShMeshDataPr> CalcMesh::calculate_mesh(
		const fltp time, 
		const cmn::ShLogPr &lg,
		#ifdef ENABLE_NL_SOLVER
		const ShSolverCachePr &cache)const{
		#else
		const ShSolverCachePr &/*cache*/)const{
		#endif
	
		// get date and time
		auto timdata = std::time(nullptr);
		auto localtime = *std::localtime(&timdata);
		std::ostringstream date_stream,time_stream;
		date_stream << std::put_time(&localtime, "%d-%m-%Y");
		time_stream << std::put_time(&localtime, "%H:%M:%S");
		const std::string date_str = date_stream.str();
		const std::string time_str = time_stream.str();
		
		// general info
		lg->msg("%s%s=== Starting Mesh Calculation ===%s\n",KBLD,KGRN,KNRM);
		lg->msg(2,"%s%sGENERAL INFO%s\n",KBLD,KGRN,KNRM);
		lg->msg("date: %s%s%s %s(dd-mm-yyyy)%s\n",KYEL,date_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("time: %s%s%s %s(hh:mm:ss)%s\n",KYEL,time_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("model name: %s%s%s\n",KYEL,model_->get_name().c_str(),KNRM);
		lg->msg("path name: %s%s%s\n",KYEL,get_name().c_str(),KNRM);
		lg->msg(-2,"\n");

		// header
		lg->msg(2,"%s%sSETTING UP MESHES%s\n",KBLD,KGRN,KNRM);

		// check input model
		if(model_==NULL)rat_throw_line("model not set");
		model_->is_valid(true);
		
		// settings for mesh creation
		MeshSettings source_mesh_settings;
		source_mesh_settings.time = time;
		source_mesh_settings.low_poly = false;
		source_mesh_settings.combine_sections = true;
		source_mesh_settings.is_calculation = true;

		// create a list of meshes
		std::list<ShMeshDataPr> meshes = model_->create_meshes({},source_mesh_settings);

		// check for cancel
		if(lg->is_cancelled())return{};
		
		// remove calculation meshes
		for(auto it = meshes.begin();it!=meshes.end();)
			if((*it)->get_calc_mesh())it = meshes.erase(it); else it++;

		// set circuit currents
		apply_circuit_currents(time, meshes);

		// display function
		MeshData::display(lg,meshes);

		// end mesh setup
		lg->msg(-2,"\n");


		// currentsources for calculating vector potential
		fmm::ShMultiSourcesPr src_a = fmm::MultiSources::create();

		// currentsources for calculating change in vector potential
		// fmm::ShMultiSourcesPr src_da = fmm::MultiSources::create();

		// charge sources for calculating scalar potential
		fmm::ShMultiSourcesPr src_v = fmm::MultiSources::create();

		// bound current sources
		fmm::ShMultiSourcesPr src_b = fmm::MultiSources::create();

		// general targets
		fmm::ShMultiTargets2Pr tar = fmm::MultiTargets2::create();

		// filter out nonlinear meshes and bar meshes for later magnetization calculation
		#ifdef ENABLE_NL_SOLVER
		std::list<ShNLDataPr> nl_meshes;
		#endif
		std::list<ShBarDataPr> bar_meshes;

		// keep track of magnetic charges per magnetized object
		// this to do force calculation later
		std::map<ShMgnDataPr, std::list<fmm::ShMgnTargetsPr> > mgn2charges;

		// walk over meshes
		for(auto it=meshes.begin();it!=meshes.end();it++){
			// calculation meshes
			if((*it)->get_calc_mesh())continue;

			// coil data
			const ShCoilDataPr coil_data = std::dynamic_pointer_cast<CoilData>(*it);
			if(coil_data!=NULL){
				// create current sources
				src_a->add_sources(coil_data->create_currents());

				// create current change sources
				// src_da->add_sources(coil_data->create_dcurrents());
			}

			// bar data
			const ShBarDataPr bar_data = std::dynamic_pointer_cast<BarData>(*it);
			if(bar_data!=NULL){
				// create fictitious magnetic charges
				const fmm::ShMgnChargeSurfacePr surf_charges = bar_data->create_surface_charges();
				// const fmm::ShMgnChargesPr surf_charges = bar_data->create_surface_point_charges();
				if(surf_charges!=NULL){
					src_v->add_sources(surf_charges); //tar->add_targets(surf_charges);
					mgn2charges[bar_data].push_back(surf_charges); 
				}
				const fmm::ShMgnChargesPr vol_charges = bar_data->create_vol_charges();
				if(vol_charges!=NULL){
					src_v->add_sources(vol_charges); //tar->add_targets(vol_charges);
					mgn2charges[bar_data].push_back(vol_charges);
				}

				// create bound currents for vector potential calculation
				// bound currents on surface
				const fmm::ShSourcesPr bnd_surf_currents = bar_data->create_bnd_surf_currents();
				if(bnd_surf_currents!=NULL)src_b->add_sources(bnd_surf_currents);

				// bound currents in volume
				const fmm::ShSourcesPr bnd_vol_currents = bar_data->create_bnd_vol_currents();
				if(bnd_vol_currents!=NULL)src_b->add_sources(bnd_vol_currents);

				// add to bar meshes list
				bar_meshes.push_back(bar_data);
			}

			// NL data
			#ifdef ENABLE_NL_SOLVER
			const ShNLDataPr nl_data = std::dynamic_pointer_cast<NLData>(*it);
			if(nl_data!=NULL){
				// add to nl meshes list
				nl_meshes.push_back(nl_data);
			}
			#endif

			// meshes
			const ShMeshDataPr mesh_data = std::dynamic_pointer_cast<MeshData>(*it);
			if(mesh_data!=NULL){
				// add any mesh to targets for field calculation
				tar->add_targets(*it);
			}

			// add to targets independent of what it is
			(*it)->set_field_type("ABHM",{3,3,3,3});
		}

		// check for cancel
		if(lg->is_cancelled())return{};

		// check if any nonlinear meshes present
		#ifdef ENABLE_NL_SOLVER
		if(!nl_meshes.empty() && (src_a->get_num_source_objects()!=0 || src_v->get_num_source_objects()!=0 || bg_!=NULL)){
			// create nonlinear solver
			const nl::ShNonLinSolverPr nl_solver = nl::NonLinSolver::create();
			nl_solver->set_use_gpu(stngs_->get_fmm_enable_gpu());
			nl_solver->set_gpu_devices(stngs_->get_gpu_devices());
			nl_solver->set_lg(lg); // maybe this should be a direct transver later

			// combine meshes
			arma::uword idx = 0, nodeshift = 0;
			arma::field<arma::Mat<fltp> > Rn(1,nl_meshes.size());
			arma::field<arma::Mat<arma::uword> > n(1,nl_meshes.size());
			arma::field<arma::Row<arma::uword> > n2part(1,nl_meshes.size());
			arma::field<nl::ShHBDataPr> hb(1,nl_meshes.size());
			for(auto it=nl_meshes.begin();it!=nl_meshes.end();it++){
				Rn(idx) = (*it)->get_nodes();
				n(idx) = (*it)->get_elements() + nodeshift;
				hb(idx) = (*it)->get_hb_data();
				if(hb(idx)==NULL)rat_throw_line("HB Curve was not supplied for: " + (*it)->get_name());
				n2part(idx) = arma::Row<arma::uword>(n(idx).n_cols,arma::fill::value(idx));
				nodeshift += Rn(idx).n_cols; idx++; 
			}

			// set mesh and hb curves to solver
			nl_solver->set_mesh(cmn::Extra::field2mat(Rn), cmn::Extra::field2mat(n));
			nl_solver->set_n2part(cmn::Extra::field2mat(n2part)); nl_solver->set_hb_curve(hb);

			// setup mesh by calculating the face and edge connectivity matrices
			nl_solver->setup_mesh();

			// calculate external field
			const rat::fmm::ShMgnTargetsPr nltar = nl_solver->get_ext_targets();

			// allocate targets
			nltar->allocate();

			// check if any vector potential sources
			if(src_a->get_num_source_objects()!=0){
				// general info
				lg->msg("%s%s=== CURRENTS TO NLMESH ===%s\n",KBLD,KGRN,KNRM);

				// multipole method using vector potentials
				const fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(src_a, nltar, stngs_);
				mlfmm->set_no_allocate();

				// setup mlfmm and calculate
				mlfmm->setup(lg); 
				
				// check for cancel
				if(lg->is_cancelled())return{};

				// run calculation
				mlfmm->calculate(lg);

				// check for cancel
				if(lg->is_cancelled())return{};
			}

			// check if any scalar potential sources
			if(src_v->get_num_source_objects()!=0){
				// general info
				lg->msg("%s%s=== CHARGES TO NLMESH ===%s\n",KBLD,KGRN,KNRM);

				// multipole method using scalar potentials
				const fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(src_v, nltar, stngs_);
				mlfmm->set_no_allocate();

				// setup mlfmm and calculate
				mlfmm->setup(lg); 

				// check for cancel
				if(lg->is_cancelled())return{};

				// run calculation
				mlfmm->calculate(lg);

				// check for cancel
				if(lg->is_cancelled())return{};
			}

			// calculate background field
			if(bg_!=NULL)bg_->create_fmm_background(time)->calc_field(nltar);

			// set external field
			nl_solver->set_ext_field(nltar);

			// general info
			lg->msg("%s%s=== NONLINEAR SOLVE ===%s\n",KBLD,KGRN,KNRM);

			// run nonlinear solver
			const arma::Row<rat::fltp> Ae = nl_solver->solve(cache).t();

			// check for cancel
			if(lg->is_cancelled())return{};

			// calculate magnetization at elements
			const arma::Mat<fltp> Mg = nl_solver->calc_elem_magnetization(Ae);
			const arma::Mat<fltp> Me = nl_solver->element_average(Mg);

			// walk over non-linear meshes
			arma::uword elem_shift = 0;
			for(auto it=nl_meshes.begin();it!=nl_meshes.end();it++){
				// get number of elements in this mesh
				const arma::uword num_elements = (*it)->get_num_elements();

				// set field to respective non-linear meshes
				(*it)->set_magnetisation(Me.cols(elem_shift,elem_shift+num_elements-1), false);

				// shift position
				elem_shift += num_elements;

				// // extrapolate values from gauss points for entire mesh
				// // this bit needs improvement!
				// (*it)->set_magnetisation((*it)->get_elements().n_rows==8 ? cmn::Hexahedron::gauss2nodes(
				// 	(*it)->get_nodes(), (*it)->get_elements(), 
				// 	Mg.cols(elem_shift,elem_shift+num_elements*nl_solver->get_num_gauss_element()-1), 
				// 	nl_solver->get_elem_gauss_coords(), 
				// 	nl_solver->get_elem_gauss_weights(), 
				// 	(*it)->get_num_nodes()) : 
				// 	cmn::Tetrahedron::gauss2nodes(
				// 	(*it)->get_nodes(), (*it)->get_elements(), 
				// 	Mg.cols(elem_shift,elem_shift+num_elements*nl_solver->get_num_gauss_element()-1), 
				// 	nl_solver->get_elem_gauss_coords(), 
				// 	nl_solver->get_elem_gauss_weights(), 
				// 	(*it)->get_num_nodes()),true);

				// // shift position
				// elem_shift += num_elements*nl_solver->get_num_gauss_element();
			}

			// create sources
			for(auto it=nl_meshes.begin();it!=nl_meshes.end();it++){
				// get pointer from iterator
				const ShNLDataPr& nl_mesh = (*it);

				// create fictitious magnetic charges to represent the non-linear mesh
				// charges on the surface
				const fmm::ShMgnChargeSurfacePr surf_charges = nl_mesh->create_surface_charges();
				if(surf_charges!=NULL){
					src_v->add_sources(surf_charges); //tar->add_targets(surf_charges);
					mgn2charges[nl_mesh].push_back(surf_charges);
				}

				// charges in the mesh volume
				const fmm::ShMgnChargesPr vol_charges = nl_mesh->create_vol_charges();
				if(vol_charges!=NULL){
					src_v->add_sources(vol_charges); //tar->add_targets(vol_charges);
					mgn2charges[nl_mesh].push_back(vol_charges);
				}

				// create the bound currents to represent the non-linear mesh
				// currents on the surface
				const fmm::ShSourcesPr bnd_surf_currents = nl_mesh->create_bnd_surf_currents();
				if(bnd_surf_currents!=NULL)src_b->add_sources(bnd_surf_currents);

				// currents in the mesh volume
				const fmm::ShCurrentSourcesPr bnd_vol_currents = nl_mesh->create_bnd_vol_currents();
				if(bnd_vol_currents!=NULL)src_b->add_sources(bnd_vol_currents);
			}

			// check for cancel
			if(lg->is_cancelled())return{};
		}
		#endif

		// allocate targets
		tar->setup_targets();
		tar->allocate();

		// calculate magnetization in the bar magnets
		for(auto it=bar_meshes.begin();it!=bar_meshes.end();it++)
			(*it)->set_self_magnetisation();

		// calculate magnetization in the non-linear meshes
		#ifdef ENABLE_NL_SOLVER
		for(auto it=nl_meshes.begin();it!=nl_meshes.end();it++)
			(*it)->set_self_magnetisation();
		#endif

		// check number of target objects
		if(tar->get_num_target_objects()!=0){
			// coil currents
			if(src_a->get_num_source_objects()!=0){
				// general info
				lg->msg("%s%s=== COIL CURRENTS TO TARGETS ===%s\n",KBLD,KGRN,KNRM);

				// multipole method
				const fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(src_a, tar, stngs_);
				mlfmm->set_no_allocate();

				// setup mlfmm and calculate
				mlfmm->setup(lg); 

				// check for cancel
				if(lg->is_cancelled())return{};

				// run calculation
				mlfmm->calculate(lg);

				// check for cancel
				if(lg->is_cancelled())return{};
			}

			// bound magnetic charges for calculating magnetic field 
			if(src_v->get_num_source_objects()!=0){
				// general info
				lg->msg("%s%s=== CHARGES TO TARGETS ===%s\n",KBLD,KGRN,KNRM);

				// create temporary target points
				const fmm::ShMgnTargetsPr tar_h = fmm::MgnTargets::create(tar->get_target_coords());
				tar_h->set_field_type('H',3);

				// multipole method
				const fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(src_v, tar_h, stngs_);
				
				// setup mlfmm and calculate
				mlfmm->setup(lg); 

				// check for cancel
				if(lg->is_cancelled())return{};

				// run calculation
				mlfmm->calculate(lg);

				// check for cancel
				if(lg->is_cancelled())return{};

				// get field and copy to real targets
				tar->add_field('H',tar_h->fmm::MgnTargets::get_field('H')); // copy field at gauss points
			}

			// bound surface currents for calculating vector potential
			if(src_b->get_num_source_objects()!=0){
				// general info
				lg->msg("%s%s=== BND CURRENTS TO TARGETS ===%s\n",KBLD,KGRN,KNRM);

				// create temporary target points
				const fmm::ShMgnTargetsPr tar_b = fmm::MgnTargets::create(tar->get_target_coords());
				tar_b->set_field_type("AB",{3,3});

				// multipole method
				const fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(src_b, tar_b, stngs_);

				// setup mlfmm and calculate
				mlfmm->setup(lg); 

				// check for cancel
				if(lg->is_cancelled())return{};

				// run calculation
				mlfmm->calculate(lg);

				// check for cancel
				if(lg->is_cancelled())return{};

				// get field and copy to real targets
				tar->add_field('A',tar_b->fmm::MgnTargets::get_field('A')); // copy field at gauss points
				tar->add_field('B',tar_b->fmm::MgnTargets::get_field('B')); // copy field at gauss points
			}

			// calculate background field
			if(bg_!=NULL)bg_->create_fmm_background(time)->calc_field(tar);

			// perform post processing
			tar->post_process();
		}

		// return the output data
		return meshes;
	}

	// get type
	std::string CalcMesh::get_type(){
		return "rat::mdl::calcmesh";
	}

	// method for serialization into json
	void CalcMesh::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		CalcLeaf::serialize(js,list);
		js["type"] = get_type();
	}

	// method for deserialisation from json
	void CalcMesh::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent
		CalcLeaf::deserialize(js,list,factory_list,pth);
	}


}}