// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelcct.hh"

#include "rat/common/error.hh"

#include "crossrectangle.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ModelCCTBase::ModelCCTBase(){
		set_number_turns(10);
		set_operating_current(400);
		set_name("Canted Cosine Theta");
	}

	// set the direction of the layer
	void ModelCCTBase::set_is_reverse(const bool is_reverse){
		is_reverse_ = is_reverse;
	}

	// set the direction of the layer
	void ModelCCTBase::set_bending_arc_length(const fltp bending_arc_length){
		bending_arc_length_ = bending_arc_length;
	}

	// set twisting
	void ModelCCTBase::set_twist(const fltp twist){
		twist_ = twist;
	}


	// set winding pitch
	void ModelCCTBase::set_drib(const fltp drib){
		drib_ = drib;
	}

	void ModelCCTBase::set_is_skew(const bool is_skew){
		is_skew_ = is_skew;
	}

	// set number of turns
	void ModelCCTBase::set_num_turns(const fltp num_turns){
		set_nt1(-num_turns/2); set_nt2(num_turns/2);
	}

	// set number of turns
	void ModelCCTBase::set_nt1(const fltp nt1){
		nt1_ = nt1;
	}

	// set number of turns
	void ModelCCTBase::set_nt2(const fltp nt2){
		nt2_ = nt2;
	}

	// set radius
	void ModelCCTBase::set_radius(const fltp radius){
		radius_ = radius;
	}

	// set number of poles
	void ModelCCTBase::set_num_poles(const arma::uword num_poles){
		num_poles_ = num_poles;
	}

	// set number of poles
	void ModelCCTBase::set_num_nodes_per_turn(const arma::uword num_nodes_per_turn){
		num_nodes_per_turn_ = num_nodes_per_turn;
	}

	// set the direction of the layer
	bool ModelCCTBase::get_is_reverse() const{
		return is_reverse_;
	}

	// set coil thickness
	void ModelCCTBase::set_cable_thickness(const fltp cable_thickness){
		cable_thickness_ = cable_thickness;
	}

	// set coil width
	void ModelCCTBase::set_cable_width(const fltp cable_width){
		cable_width_ = cable_width;
	}

	// set axial element size
	void ModelCCTBase::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	// get bending origin
	void ModelCCTBase::set_bending_origin(const bool bending_origin){
		bending_origin_ = bending_origin;
	}


	// set the direction of the layer
	fltp ModelCCTBase::get_bending_arc_length() const{
		return bending_arc_length_;
	}

	// set twisting
	fltp ModelCCTBase::get_twist()const{
		return twist_;
	}

	// set winding pitch
	fltp ModelCCTBase::get_drib()const{
		return drib_;
	}

	// get number of turns
	fltp ModelCCTBase::get_num_turns()const{
		return nt2_ - nt1_;
	}

	// get number of turns1
	fltp ModelCCTBase::get_nt1()const{
		return nt1_;
	}

	// get number of turns2
	fltp ModelCCTBase::get_nt2()const{
		return nt2_;
	}

	// set radius
	fltp ModelCCTBase::get_radius()const{
		return radius_;
	}

	// set number of poles
	arma::uword ModelCCTBase::get_num_poles()const{
		return num_poles_;
	}

	// set number of poles
	arma::uword ModelCCTBase::get_num_nodes_per_turn() const{
		return num_nodes_per_turn_;
	}

	// set number of layers
	void ModelCCTBase::set_num_layers(const arma::uword num_layers){
		num_layers_ = num_layers;
	}

	// get number of layers
	arma::uword ModelCCTBase::get_num_layers() const{
		return num_layers_;
	}

	bool ModelCCTBase::get_bending_origin()const{
		return bending_origin_;
	}

	// set radius increment between layers
	void ModelCCTBase::set_rho_increment(const fltp rho_increment){
		rho_increment_ = rho_increment;
	}

	void ModelCCTBase::set_enable_lead1(const bool enable_lead1){
		enable_lead1_ = enable_lead1;
	}

	void ModelCCTBase::set_enable_lead2(const bool enable_lead2){
		enable_lead2_ = enable_lead2;
	}

	void ModelCCTBase::set_leadness1(const fltp leadness1){
		leadness1_ = leadness1;
	}

	void ModelCCTBase::set_leadness2(const fltp leadness2){
		leadness2_ = leadness2;
	}

	void ModelCCTBase::set_zlead1(const fltp zlead1){
		zlead1_ = zlead1;
	}

	void ModelCCTBase::set_zlead2(const fltp zlead2){
		zlead2_ = zlead2;
	}

	void ModelCCTBase::set_theta_lead1(const fltp theta_lead1){
		theta_lead1_ = theta_lead1;
	}

	void ModelCCTBase::set_theta_lead2(const fltp theta_lead2){
		theta_lead2_ = theta_lead2;
	}

	void ModelCCTBase::set_use_skew_angle(const bool use_skew_angle){
		use_skew_angle_ = use_skew_angle;
	}

	void ModelCCTBase::set_amplitude(const fltp amplitude){
		amplitude_ = amplitude;
	}

	void ModelCCTBase::set_skew_angle(const fltp alpha){
		alpha_ = alpha;
	}

	void ModelCCTBase::set_const_skew_angle(const bool const_skew_angle){
		const_skew_angle_ = const_skew_angle;
	}

	void ModelCCTBase::set_use_radius(const bool use_radius){
		use_radius_ = use_radius;
	}

	void ModelCCTBase::set_bending_radius(const fltp bending_radius){
		bending_radius_ = bending_radius;
	}




	bool ModelCCTBase::get_is_skew()const{
		return is_skew_;
	}

	// get radius increment between layers
	fltp ModelCCTBase::get_rho_increment()const{
		return rho_increment_;
	}

	// get coil thickness
	fltp ModelCCTBase::get_cable_thickness()const{
		return cable_thickness_;
	}

	// get coil width
	fltp ModelCCTBase::get_cable_width()const{
		return cable_width_;
	}

	// get axial element size
	fltp ModelCCTBase::get_element_size()const {
		return element_size_;
	}

	bool ModelCCTBase::get_enable_lead1()const{
		return enable_lead1_;
	}
	
	bool ModelCCTBase::get_enable_lead2()const{
		return enable_lead2_;
	}

	fltp ModelCCTBase::get_leadness1()const{
		return leadness1_;
	}

	fltp ModelCCTBase::get_leadness2()const{
		return leadness2_;
	}

	fltp ModelCCTBase::get_zlead1()const{
		return zlead1_;
	}

	fltp ModelCCTBase::get_zlead2()const{
		return zlead2_;
	}

	fltp ModelCCTBase::get_theta_lead1()const{
		return theta_lead1_;
	}

	fltp ModelCCTBase::get_theta_lead2()const{
		return theta_lead2_;
	}

	bool ModelCCTBase::get_use_skew_angle()const{
		return use_skew_angle_;
	}

	fltp ModelCCTBase::get_amplitude()const{
		return amplitude_;
	}

	fltp ModelCCTBase::get_skew_angle()const{
		return alpha_;
	}

	bool ModelCCTBase::get_const_skew_angle()const{
		return const_skew_angle_;
	}

	bool ModelCCTBase::get_use_radius() const{
		return use_radius_;
	}
	
	fltp ModelCCTBase::get_bending_radius()const{
		return bending_radius_;
	}


	// get base
	ShPathPr ModelCCTBase::get_input_path() const{
		// check input
		is_valid(true);

		// calculate pitch
		const fltp omega = cable_thickness_ + drib_;

		// create path and transfer settings
		ShPathCCTPr cct_pth = PathCCT::create();
		cct_pth->set_normal_omega(true);
		cct_pth->set_is_skew(is_skew_);
		cct_pth->set_num_poles(num_poles_);
		cct_pth->set_radius(radius_);
		cct_pth->set_pitch(omega);
		cct_pth->set_nt1(nt1_);
		cct_pth->set_nt2(nt2_);
		cct_pth->set_num_nodes_per_turn(num_nodes_per_turn_);
		cct_pth->set_is_reverse(is_reverse_);
		cct_pth->set_num_layers(num_layers_);
		cct_pth->set_rho_increment(rho_increment_);
		cct_pth->set_bending_origin(bending_origin_);
		cct_pth->set_bending_arc_length(bending_arc_length_);
		cct_pth->set_twist(twist_);
		cct_pth->set_enable_lead1(enable_lead1_);
		cct_pth->set_enable_lead2(enable_lead2_);
		cct_pth->set_leadness1(leadness1_);
		cct_pth->set_leadness2(leadness2_);
		cct_pth->set_zlead1(zlead1_);
		cct_pth->set_zlead2(zlead2_);
		cct_pth->set_theta_lead1(theta_lead1_);
		cct_pth->set_theta_lead2(theta_lead2_);
		cct_pth->set_alpha(alpha_);
		cct_pth->set_amplitude(amplitude_);
		cct_pth->set_use_skew_angle(use_skew_angle_);
		cct_pth->set_const_skew_angle(const_skew_angle_);
		cct_pth->set_use_radius(use_radius_);
		cct_pth->set_bending_radius(bending_radius_);

		// return the circle
		return cct_pth;
	}

	// get cross
	ShCrossPr ModelCCTBase::get_input_cross() const{
		// check input
		is_valid(true);

		// create rectangular cross
		ShCrossRectanglePr rectangle = CrossRectangle::create(
			-cable_thickness_/2, cable_thickness_/2, 0, cable_width_, element_size_, element_size_);

		// return the rectangle
		return rectangle;
	}

	// check input
	bool ModelCCTBase::is_valid(const bool enable_throws) const{
		if(!Transformations::is_valid(enable_throws))return false;
		if(radius_<=0){if(enable_throws){rat_throw_line("radius must be larger than zero");} return false;};
		if(num_poles_<=0){if(enable_throws){rat_throw_line("number of poles must be larger than zero");} return false;};
		if(nt2_<=nt1_){if(enable_throws){rat_throw_line("number of turns must be larger than zero");} return false;};
		if(num_nodes_per_turn_==0){if(enable_throws){rat_throw_line("number of nodes per turn can not be zero");} return false;};
		if(num_layers_<=0){if(enable_throws){rat_throw_line("number of layers must be larger than zero");} return false;};
		if(cable_thickness_<=0){if(enable_throws){rat_throw_line("coil thickness must be larger than zero");} return false;};
		if(cable_width_<=0){if(enable_throws){rat_throw_line("coil width must be larger than zero");} return false;};
		if(enable_lead1_){
			if(leadness1_<=0){if(enable_throws){rat_throw_line("leadness for first lead must be larger than zero");} return false;};
		}
		if(enable_lead2_){
			if(leadness2_<=0){if(enable_throws){rat_throw_line("leadness for second lead must be larger than zero");} return false;};
		}
		return true;
	}

	// get type
	std::string ModelCCTBase::get_type(){
		return "rat::mdl::modelcctbase";
	}

	// method for serialization into json
	void ModelCCTBase::serialize(
		Json::Value &js, cmn::SList &list) const{

		// parent
		ModelCoilWrapper::serialize(js,list);

		// set type
		js["type"] = get_type();

		// settings
		js["type"] = get_type();
		js["is_skew"] = is_skew_;
		js["num_poles"] = static_cast<unsigned int>(num_poles_); 
		js["is_reverse_"] = is_reverse_;
		js["radius"] = radius_;
		js["drib"] = drib_; 
		js["nt1"] = nt1_; 
		js["nt2"] = nt2_; 
		js["num_nodes_per_turn"] = static_cast<unsigned int>(num_nodes_per_turn_); 
		js["twist"] = twist_; 
		js["bending_origin"] = bending_origin_;
		js["use_radius"] = use_radius_;
		js["bending_radius"] = bending_radius_;
		js["bending_arc_length"] = bending_arc_length_;
		js["num_layers"] = static_cast<int>(num_layers_);
		js["rho_increment"] = rho_increment_;
		js["cable_thickness"] = cable_thickness_;
		js["cable_width"] = cable_width_;
		js["element_size"] = element_size_;
		js["use_skew_angle"] = use_skew_angle_;
		js["amplitude"] = amplitude_; 
		js["alpha"] = alpha_;
		js["const_skew_angle"] = const_skew_angle_;

		// lead settings
		js["enable_lead1"] = enable_lead1_;
		js["enable_lead2"] = enable_lead2_;
		js["leadness1"] = leadness1_;
		js["leadness2"] = leadness2_;
		js["zlead1"] = zlead1_;
		js["zlead2"] = zlead2_;
		js["theta_lead1"] = theta_lead1_;
		js["theta_lead2"] = theta_lead2_;
	}

	// method for deserialisation from json
	void ModelCCTBase::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		ModelCoilWrapper::deserialize(js,list,factory_list,pth);

		// settings
		set_is_skew(js["is_skew"].asBool());
		set_num_poles(js["num_poles"].asUInt64()); 
		set_is_reverse(js["is_reverse_"].asBool());
		set_radius(js["radius"].ASFLTP());
		set_drib(js["drib"].ASFLTP()); 
		if(js.isMember("num_turns")){
			set_num_turns(js["num_turns"].ASFLTP()); // backwards compatibility V2.014.0
		}else{
			set_nt1(js["nt1"].ASFLTP()); 
			set_nt2(js["nt2"].ASFLTP());
		}
		set_num_nodes_per_turn(js["num_nodes_per_turn"].asUInt64()); 
		set_twist(js["twist"].ASFLTP());
		set_bending_arc_length(js["bending_arc_length"].ASFLTP());
		set_use_radius(js["use_radius"].asBool());
		set_bending_radius(js["bending_radius"].ASFLTP());
		set_bending_origin(js["bending_origin"].asBool());
		set_num_layers(js["num_layers"].asUInt64());
		set_rho_increment(js["rho_increment"].ASFLTP());
		set_cable_thickness(js["cable_thickness"].ASFLTP());
		set_cable_width(js["cable_width"].ASFLTP());
		set_element_size(js["element_size"].ASFLTP());
		set_use_skew_angle(js["use_skew_angle"].asBool());
		set_amplitude(js["amplitude"].ASFLTP()); 
		set_skew_angle(js["alpha"].ASFLTP());
		set_const_skew_angle(js["const_skew_angle"].asBool());

		// lead settings
		set_enable_lead1(js["enable_lead1"].asBool());
		set_enable_lead2(js["enable_lead2"].asBool());
		set_leadness1(js["leadness1"].ASFLTP());
		set_leadness2(js["leadness2"].ASFLTP());
		set_zlead1(js["zlead1"].ASFLTP());
		set_zlead2(js["zlead2"].ASFLTP());
		set_theta_lead1(js["theta_lead1"].ASFLTP());
		set_theta_lead2(js["theta_lead2"].ASFLTP());
	}



	// these two classes are here for backwards compatibility a
	// and will be removed in the future to be replaced by
	// the base class: ModelCCTBase.

	// default constructor
	ModelCCT::ModelCCT(){
		
	}

	// factory
	ShModelCCTPr ModelCCT::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<ModelCCT>();
	}

	// get type
	std::string ModelCCT::get_type(){
		return "rat::mdl::modelcct";
	}

	// method for deserialisation from json
	void ModelCCT::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		ModelCCTBase::deserialize(js,list,factory_list,pth);

		// properties
		set_use_skew_angle(true);
		set_skew_angle(arma::Datum<fltp>::pi/2 - js["skew_angle"].ASFLTP()); 
	}



	// CCT based on amplitude
	// default constructor
	ModelCCTAmpl::ModelCCTAmpl(){

	}

	// factory
	ShModelCCTAmplPr ModelCCTAmpl::create(){
		return std::make_shared<ModelCCTAmpl>();
	}

	// get type
	std::string ModelCCTAmpl::get_type(){
		return "rat::mdl::modelcctampl";
	}

	// method for deserialisation from json
	void ModelCCTAmpl::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		ModelCCTBase::deserialize(js,list,factory_list,pth);

		// properties
		set_use_skew_angle(false);
		set_amplitude(js["amplitude"].ASFLTP()/num_poles_); // multiply with number of poles as definition changed
	}

}}