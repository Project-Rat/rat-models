// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modeldshape.hh"

#include "rat/common/error.hh"

#include "crossrectangle.hh"
#include "pathgroup.hh"
#include "pathdshape.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	ModelDShape::ModelDShape::ModelDShape(){
		set_name("D-Shape");
	}

	// constructor with input
	ModelDShape::ModelDShape(
		const fltp ell1,
		const fltp ell2,
		const fltp coil_thickness,
		const fltp coil_width,
		const fltp element_size) : ModelDShape(){

		// set to self
		set_coil_thickness(coil_thickness);
		set_coil_width(coil_width);
		set_ell1(ell1);
		set_ell2(ell2);
		set_element_size(element_size);
	}

	// factory
	ShModelDShapePr ModelDShape::create(){
		return std::make_shared<ModelDShape>();
	}

	// factory with input
	ShModelDShapePr ModelDShape::create(
		const fltp ell1,
		const fltp ell2,
		const fltp coil_thickness,
		const fltp coil_width,
		const fltp element_size){
		return std::make_shared<ModelDShape>(ell1,ell2,coil_thickness,coil_width,element_size);
	}

	// get base and cross
	ShPathPr ModelDShape::get_input_path() const{
		// check input
		is_valid(true);

		// create circular path
		ShPathDShapePr dshape = PathDShape::create(
			ell1_, ell2_, element_size_, coil_thickness_);

		// return the racetrack
		return dshape;
	}

	// get cross section
	ShCrossPr ModelDShape::get_input_cross() const{
		// check input
		is_valid(true);

		// create rectangular cross
		ShCrossRectanglePr rectangle = CrossRectangle::create(
			0, coil_thickness_, -coil_width_/2, coil_width_/2, element_size_, element_size_);

		// return the rectangle
		return rectangle;
	}

	// setting
	void ModelDShape::set_coil_thickness(const fltp coil_thickness){
		coil_thickness_ = coil_thickness;
	}

	// set coil width
	void ModelDShape::set_coil_width(const fltp coil_width){
		coil_width_ = coil_width;
	}

	// set element size
	void ModelDShape::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	// set ell1 
	void ModelDShape::set_ell1(const fltp ell1){
		ell1_ = ell1;
	}

	// set ell2
	void ModelDShape::set_ell2(const fltp ell2){
		ell2_ = ell2;
	}
	
	// get coil thickness
	fltp ModelDShape::get_coil_thickness()const{
		return coil_thickness_;
	}

	// get coil width
	fltp ModelDShape::get_coil_width()const{
		return coil_width_;
	}
		
	// get element size
	fltp ModelDShape::get_element_size()const{
		return element_size_;
	}
	
	// get ell1
	fltp ModelDShape::get_ell1()const{
		return ell1_;
	}

	// get ell2
	fltp ModelDShape::get_ell2()const{
		return ell2_;
	}

	// check input
	bool ModelDShape::is_valid(const bool enable_throws) const{
		if(!ModelCoilWrapper::is_valid(enable_throws))return false;
		if(ell1_<=0){if(enable_throws){rat_throw_line("first length must be larger than zero");} return false;};
		if(ell2_<=0){if(enable_throws){rat_throw_line("second length must be larger than zero");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		if(coil_thickness_<=0){if(enable_throws){rat_throw_line("coil thickness must be larger than zero");} return false;};
		if(coil_width_<=0){if(enable_throws){rat_throw_line("coil width must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string ModelDShape::get_type(){
		return "rat::mdl::modeldhape";
	}

	// method for serialization into json
	void ModelDShape::serialize(
		Json::Value &js, cmn::SList &list) const{

		// parent
		ModelCoilWrapper::serialize(js,list);

		// set type
		js["type"] = get_type();

		// set properties
		js["coil_thickness"] = coil_thickness_;
		js["coil_width"] = coil_width_;
		js["ell1"] = ell1_;
		js["ell2"] = ell2_;
		js["element_size"] = element_size_;
	}

	// method for deserialisation from json
	void ModelDShape::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		ModelCoilWrapper::deserialize(js,list,factory_list,pth);

		// set properties
		set_coil_thickness(js["coil_thickness"].ASFLTP());
		set_coil_width(js["coil_width"].ASFLTP());
		set_ell1(js["ell1"].ASFLTP());
		set_ell2(js["ell2"].ASFLTP());
		set_element_size(js["element_size"].ASFLTP());
	}

}}