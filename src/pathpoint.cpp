// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathpoint.hh"

// rat-common headers
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathPoint::PathPoint(){
		set_name("Point");
	}

	// factory
	ShPathPointPr PathPoint::create(){
		return std::make_shared<PathPoint>();
	}

	// darboux generator function
	ShFramePr PathPoint::create_frame(const MeshSettings &/*stngs*/) const{
		// allocate coordinates
		arma::Col<fltp>::fixed<3> R(arma::fill::zeros);
		arma::Col<fltp>::fixed<3> L(arma::fill::zeros);
		arma::Col<fltp>::fixed<3> N(arma::fill::zeros);
		arma::Col<fltp>::fixed<3> D(arma::fill::zeros);

		// set vectors for line along x-axis
		L(1) = RAT_CONST(1.0); // along y
		N(0) = RAT_CONST(1.0); // along x
		D(2) = RAT_CONST(1.0); // along z

		// check handedness
		assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N,L),D)>RAT_CONST(0.0)));
		
		// create frame and return
		return Frame::create(R,L,N,D);
	}

	// get type
	std::string PathPoint::get_type(){
		return "rat::mdl::pathpoint";
	}

	// method for serialization into json
	void PathPoint::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		Path::serialize(js,list);

		// type
		js["type"] = get_type();
	}

	// method for deserialisation from json
	void PathPoint::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent objects
		Path::deserialize(js,list,factory_list,pth);
	}


}}