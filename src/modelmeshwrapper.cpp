// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelmeshwrapper.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// path can not be set externally
	bool ModelMeshWrapper::is_custom_path() const{
		return false;
	}

	// path can not be set externally
	bool ModelMeshWrapper::is_custom_cross() const{
		return false;
	}

	// splitting the modelgroup
	std::list<ShModelPr> ModelMeshWrapper::split(const ShSerializerPr &slzr) const{
		const ShPathPr pth = get_input_path();
		const ShCrossPr crss = get_input_cross();
		const mat::ShConductorPr con = slzr->copy(get_input_conductor());
		const ShModelMeshPr mesh = ModelMesh::create(pth, crss);
		mesh->set_input_conductor(con);
		mesh->add_transformations(get_transformations());
		mesh->set_name(get_name());
		mesh->set_tree_open(true);
		mesh->set_circuit_index(circuit_index_);
		mesh->set_operating_temperature(operating_temperature_);
		mesh->set_temperature_drive(temperature_drive_);
		if(use_custom_color_)mesh->set_color(color_, use_custom_color_);
		return {mesh};
	}

	// validity check
	bool ModelMeshWrapper::is_valid(const bool enable_throws)const{
		if(!ModelMesh::is_valid(enable_throws))return false;
		return true;
	}

	// get type
	std::string ModelMeshWrapper::get_type(){
		return "rat::mdl::modelmeshwrapper";
	}

	// method for serialization into json
	void ModelMeshWrapper::serialize(
		Json::Value &js, cmn::SList &list) const{

		// parent
		ModelMesh::serialize(js,list);

		// set type
		js["type"] = get_type();
	}

	// method for deserialisation from json
	void ModelMeshWrapper::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		ModelMesh::deserialize(js,list,factory_list,pth);
	}

}}