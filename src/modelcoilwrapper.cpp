// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelcoilwrapper.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// path can not be set externally
	bool ModelCoilWrapper::is_custom_path() const{
		return false;
	}

	// path can not be set externally
	bool ModelCoilWrapper::is_custom_cross() const{
		return false;
	}

	// splitting the modelgroup
	std::list<ShModelPr> ModelCoilWrapper::split(const ShSerializerPr &slzr) const{
		const ShPathPr pth = get_input_path();
		const ShCrossPr crss = get_input_cross();
		const mat::ShConductorPr con = slzr->copy(get_input_conductor());
		const ShModelCoilPr coil = ModelCoil::create(pth, crss, con);
		coil->add_transformations(get_transformations());
		coil->set_name(get_name());
		coil->set_number_turns(number_turns_);
		coil->set_enable_current_sharing(enable_current_sharing_);
		coil->set_softening(softening_);
		coil->set_use_current_density(use_current_density_);
		coil->set_operating_current(operating_current_);
		coil->set_operating_current_density(operating_current_density_);
		coil->set_current_drive(current_drive_);
		coil->set_tree_open(true);
		coil->set_circuit_index(circuit_index_);
		coil->set_operating_temperature(operating_temperature_);
		coil->set_temperature_drive(temperature_drive_);
		coil->set_color(color_, use_custom_color_);
		return {coil};
	}

	// validity check
	bool ModelCoilWrapper::is_valid(const bool enable_throws)const{
		if(!ModelCoil::is_valid(enable_throws))return false;
		return true;
	}

	// get type
	std::string ModelCoilWrapper::get_type(){
		return "rat::mdl::modelcoilwrapper";
	}

	// method for serialization into json
	void ModelCoilWrapper::serialize(
		Json::Value &js, cmn::SList &list) const{

		// parent
		ModelCoil::serialize(js,list);

		// set type
		js["type"] = get_type();
	}

	// method for deserialisation from json
	void ModelCoilWrapper::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		ModelCoil::deserialize(js,list,factory_list,pth);
	}

}}