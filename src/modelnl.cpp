// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// only available for non-linear solver
#ifdef ENABLE_NL_SOLVER

// include header
#include "modelnl.hh"

// rat models headers
#include "nldata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ModelNL::ModelNL(
		const ShHBCurvePr& hb_curve){
		set_name("Custom HB-Mesh");
		set_hb_curve(hb_curve);
		set_color({0.0,62.0/255,55.0/255},true);
	}

	// constructor
	ModelNL::ModelNL(
		const ShPathPr &input_path, 
		const ShCrossPr &input_cross, 
		const ShHBCurvePr& hb_curve) : ModelNL(hb_curve){

		// set to self
		set_input_path(input_path);
		set_input_cross(input_cross);
	}

	// factory immediately setting base and cross section
	ShModelNLPr ModelNL::create(
		const ShHBCurvePr& hb_curve){
		return std::make_shared<ModelNL>(hb_curve);
	}

	// factory immediately setting base and cross section
	ShModelNLPr ModelNL::create(
		const ShPathPr &input_path, 
		const ShCrossPr &input_cross, 
		const ShHBCurvePr& hb_curve){
		return std::make_shared<ModelNL>(input_path,input_cross,hb_curve);
	}

	// create data object
	ShMeshDataPr ModelNL::create_data()const{
		return NLData::create();
	}

	// setters
	void ModelNL::set_softening(const fltp softening){
		softening_ = softening;
	}

	void ModelNL::set_num_gauss_surface(const arma::sword num_gauss_surface){
		num_gauss_surface_ = num_gauss_surface;
	}

	void ModelNL::set_refine2tetrahedrons(const bool refine2tetrahedrons){
		refine2tetrahedrons_ = refine2tetrahedrons;
	}

	void ModelNL::set_num_gauss_volume(const arma::sword num_gauss_volume){
		num_gauss_volume_ = num_gauss_volume;
	}

	// getters
	bool ModelNL::get_refine2tetrahedrons()const{
		return refine2tetrahedrons_;
	}

	arma::sword ModelNL::get_num_gauss_surface()const{
		return num_gauss_surface_;
	}

	arma::sword ModelNL::get_num_gauss_volume()const{
		return num_gauss_volume_;
	}

	fltp ModelNL::get_softening()const{
		return softening_;
	}


	// factory for mesh objects
	std::list<ShMeshDataPr> ModelNL::create_meshes(
		const std::list<arma::uword> &trace, 
		const MeshSettings &stngs)const{

		// trace
		if(!trace.empty())rat_throw_line("trace must be empty for leaf");

		// check enabled
		if(!get_enable())return{};

		// check model
		if(!is_valid(stngs.enable_throws))return{};

		// create meshes
		std::list<ShMeshDataPr> meshes = ModelMesh::create_meshes(trace, stngs);

		// allocate list of bar meshes
		std::list<ShMeshDataPr> nl_meshes;
		for(auto it = meshes.begin();it!=meshes.end();it++){
			// reinterpret cast
			const ShNLDataPr nl_mesh = std::dynamic_pointer_cast<NLData>(*it);
			if(nl_mesh==NULL)rat_throw_line("pointer cast failed");

			// copy properties
			if(hb_curve_!=NULL)if(hb_curve_->is_valid(false))nl_mesh->set_hb_data(hb_curve_->create_hb_data());
			nl_mesh->set_num_gauss(num_gauss_volume_, num_gauss_surface_);
			nl_mesh->set_softening(softening_);

			// // convert
			if(refine2tetrahedrons_ && stngs.low_poly==false){
				// call refinement
				nl_mesh->refine_hexahedron_to_tetrahedron();

				// set temperature
				nl_mesh->set_temperature(operating_temperature_);
			}

			// add to list
			nl_meshes.push_back(nl_mesh);
		}

		// return mesh data
		return nl_meshes;
	}

	// check validity
	bool ModelNL::is_valid(const bool enable_throws) const{
		// check mesh
		if(!ModelMesh::is_valid(enable_throws))return false;
		return true;
	}

	// get type
	std::string ModelNL::get_type(){
		return "rat::mdl::modelnl";
	}

	// method for serialization into json
	void ModelNL::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents first 
		// this allows the type to be overridden
		ModelMesh::serialize(js,list);

		// properties
		js["type"] = get_type();

		// serialize hb curve
		js["hb_curve"] = cmn::Node::serialize_node(hb_curve_, list);

		// number of gauss points
		js["num_gauss_volume"] = static_cast<int>(num_gauss_volume_);
		js["num_gauss_surface"] = static_cast<int>(num_gauss_surface_);

		// softening factor
		js["softening"] = softening_;
		js["refine2tetrahedrons"] = refine2tetrahedrons_;
	}

	// method for deserialisation from json
	void ModelNL::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent objects
		ModelMesh::deserialize(js,list,factory_list,pth);

		// deserialize hb-curve
		hb_curve_ = cmn::Node::deserialize_node<HBCurve>(js["hb_curve"], list, factory_list, pth);

		// number of gauss points
		if(js.isMember("num_gauss_volume"))set_num_gauss_volume(js["num_gauss_volume"].asInt64());
		if(js.isMember("num_gauss_surface"))set_num_gauss_surface(js["num_gauss_surface"].asInt64());

		// softening factor
		if(js.isMember("softening"))set_softening(js["softening"].ASFLTP());
		if(js.isMember("refine2tetrahedrons"))set_refine2tetrahedrons(js["refine2tetrahedrons"].asBool());
	}

}}

#endif