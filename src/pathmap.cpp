// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathmap.hh"

// rat-common
#include "rat/common/extra.hh"

// boost headers
#include <boost/math/interpolators/barycentric_rational.hpp>



// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathMap::PathMap(){
		set_name("Map");
	}

	// constructor with dimension input
	PathMap::PathMap(const ShPathPr &base_path, const ShPathPr &source_path) : PathMap(){
		set_base_path(base_path);
		set_source_path(source_path);
	}


	// factory
	ShPathMapPr PathMap::create(){
		return std::make_shared<PathMap>();
	}

	// factory with dimension input
	ShPathMapPr PathMap::create(const ShPathPr &base_path, const ShPathPr &source_path){
		return std::make_shared<PathMap>(base_path, source_path);
	}

	// setters
	void PathMap::set_base_path(const ShPathPr &base_path){
		base_path_ = base_path;
	}

	void PathMap::set_source_path(const ShPathPr &source_path){
		source_path_ = source_path;
	}

	void PathMap::set_source2long(const char source2long){
		assert(cmn::Extra::is_xyz(source2long));
		source2long_ = source2long;
	}

	void PathMap::set_source2normal(const char source2normal){
		assert(cmn::Extra::is_xyz(source2normal));
		source2normal_ = source2normal;
	}

	void PathMap::set_normalize_length(const bool normalize_length){
		normalize_length_ = normalize_length;
	}

	void PathMap::set_loop_around(const bool loop_around){
		loop_around_ = loop_around;
	}

	void PathMap::set_use_bcr_interp(const bool use_bcr_interp){
		use_bcr_interp_ = use_bcr_interp;
	}

	void PathMap::set_bcr_order(const arma::uword bcr_order){
		bcr_order_ = bcr_order;
	}




	// getters
	char PathMap::get_source2long()const{
		return source2long_;
	}

	char PathMap::get_source2normal()const{
		return source2normal_;
	}

	bool PathMap::get_normalize_length()const{
		return normalize_length_;
	}

	bool PathMap::get_loop_around()const{
		return loop_around_;
	}

	bool PathMap::get_use_bcr_interp()const{
		return use_bcr_interp_;
	}

	arma::uword PathMap::get_bcr_order()const{
		return bcr_order_;
	}


	// update function
	ShFramePr PathMap::create_frame(const MeshSettings &stngs) const{
		// check validity
		is_valid(true);

		// create frame
		const ShFramePr base_frame = base_path_->create_frame(stngs);
		const ShFramePr source_frame = source_path_->create_frame(stngs);

		// combine sections
		base_frame->combine();

		// get base coordinates
		const bool base_is_loop = base_frame->is_loop();
		const arma::Mat<fltp> Rb = base_frame->get_coords(0);
		const arma::Mat<fltp> Lb = base_frame->get_direction(0);
		const arma::Mat<fltp> Nb = base_frame->get_normal(0);
		const arma::Mat<fltp> Db = base_frame->get_transverse(0);

		// check handedness
		assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(Nb,Lb),Db)>RAT_CONST(0.0)));

		// get source coordinates
		const arma::field<arma::Mat<fltp> > Rs = source_frame->get_coords();
		const arma::field<arma::Mat<fltp> > Ls = source_frame->get_direction();
		const arma::field<arma::Mat<fltp> > Ns = source_frame->get_normal();
		const arma::field<arma::Mat<fltp> > Ds = source_frame->get_transverse();
		
		// get source information
		const arma::Row<arma::uword> section = source_frame->get_section();
		const arma::Row<arma::uword> turn = source_frame->get_turn();
		const arma::uword num_section_base = source_frame->get_num_section_base();

		// get number of sections
		const arma::uword num_source_sections = Rs.n_elem;

		// numerically calculate position
		arma::Row<fltp> ell_base = arma::join_horiz(arma::Row<fltp>{0}, arma::cumsum(cmn::Extra::vec_norm(arma::diff(Rb,1,1))));

		// normalize length
		if(normalize_length_)ell_base /= ell_base.back();

		// allocate
		arma::field<arma::Mat<fltp> > Rm(1,num_source_sections), Lm(1,num_source_sections);
		arma::field<arma::Mat<fltp> > Nm(1,num_source_sections), Dm(1,num_source_sections);

		// find transverse direction
		assert(source2long_!=source2normal_);
		assert(cmn::Extra::is_xyz(source2long_));
		assert(cmn::Extra::is_xyz(source2normal_));
		arma::Row<arma::uword>::fixed<3> idx{1,1,1}; 
		idx(cmn::Extra::xyz2idx(source2long_)) = 0; 
		idx(cmn::Extra::xyz2idx(source2normal_)) = 0; 
		const char source2transverse = cmn::Extra::idx2xyz(arma::as_scalar(arma::find(idx)));
		// const int sgn = -1;

		// vectors
		const fltp sgn = arma::as_scalar(cmn::Extra::dot(cmn::Extra::cross(
			cmn::Extra::unit_vec(source2normal_),cmn::Extra::unit_vec(source2long_)),
			cmn::Extra::unit_vec(source2transverse)));
		assert(std::abs(std::abs(sgn)-1.0)<1e-14);

		// map x,y or z to position
		cmn::parfor(0,Rs.n_elem,true,[&](arma::uword i, arma::uword /*cpu*/){
		// for(arma::uword i=0;i<Rs.n_elem;i++){
			// check handedness
			assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(Ns(i),Ls(i)),Ds(i))>RAT_CONST(0.0)));

			// num modes
			const arma::uword num_source_nodes = Rs(i).n_cols;

			// get source coordinates
			arma::Row<fltp> ell_source = Rs(i).row(cmn::Extra::xyz2idx(source2long_));

			// loop around
			if(loop_around_)ell_source = rat::cmn::Extra::modulus(ell_source,ell_base.back());

			// base interpolation
			arma::Mat<fltp> Rbi(3,num_source_nodes), Lbi(3,num_source_nodes);
			arma::Mat<fltp> Nbi(3,num_source_nodes), Dbi(3,num_source_nodes);

			// interpolate into base frame
			if(use_bcr_interp_){
				// loop around
				const arma::Row<fltp> ell_base_ext = base_is_loop && loop_around_ ? arma::join_horiz(
					ell_base.head_cols(ell_base.n_cols-1)-ell_base.back(), 
					ell_base.head_cols(ell_base.n_cols-1), 
					ell_base+ell_base.back()) : ell_base;

				// check sorting
				assert(ell_base_ext.is_sorted("strictascend"));
				
				// extend coordinates and orientation vectors
				const arma::Mat<fltp> Rbext = base_is_loop && loop_around_ ? arma::join_horiz(
					Rb.head_cols(Rb.n_cols-1),Rb.head_cols(Rb.n_cols-1),Rb) : Rb;
				const arma::Mat<fltp> Dbext = base_is_loop && loop_around_ ? arma::join_horiz(
					Db.head_cols(Db.n_cols-1),Db.head_cols(Db.n_cols-1),Db) : Db;
				const arma::Mat<fltp> Lbext = base_is_loop && loop_around_ ? arma::join_horiz(
					Lb.head_cols(Db.n_cols-1),Lb.head_cols(Lb.n_cols-1),Lb) : Lb;

				// walk over XYZ
				for(arma::uword j=0;j<3;j++){
					// use boost interpolators
					const arma::Row<fltp> Rbxyz = Rbext.row(j);
					const arma::Row<fltp> Lbxyz = Lbext.row(j);
					const arma::Row<fltp> Dbxyz = Dbext.row(j);
					boost::math::interpolators::barycentric_rational<fltp> Rb_fun(ell_base_ext.memptr(), Rbxyz.memptr(), ell_base_ext.n_elem, bcr_order_);
					boost::math::interpolators::barycentric_rational<fltp> Lb_fun(ell_base_ext.memptr(), Lbxyz.memptr(), ell_base_ext.n_elem, bcr_order_);
					boost::math::interpolators::barycentric_rational<fltp> Db_fun(ell_base_ext.memptr(), Dbxyz.memptr(), ell_base_ext.n_elem, bcr_order_);
					for(arma::uword k=0;k<ell_source.n_elem;k++){
						Rbi(j,k) = Rb_fun(ell_source(k));
						Lbi(j,k) = Lb_fun(ell_source(k));
						Dbi(j,k) = Db_fun(ell_source(k));
					}
				}

				// calculate normal vector
				Nbi = cmn::Extra::cross(Lbi,Dbi);

				// normalize
				Lbi.each_row() /= cmn::Extra::vec_norm(Lbi);
				Dbi.each_row() /= cmn::Extra::vec_norm(Dbi);
				Nbi.each_row() /= cmn::Extra::vec_norm(Nbi);
			}

			// regular interpolation
			else{
				for(arma::uword j=0;j<3;j++){
					arma::Col<fltp> vi;
					cmn::Extra::interp1(ell_base.t(), Rb.row(j).t(), ell_source.t(), vi, "linear", true); Rbi.row(j) = vi.t();
					cmn::Extra::interp1(ell_base.t(), Lb.row(j).t(), ell_source.t(), vi, "linear", true); Lbi.row(j) = vi.t();
					cmn::Extra::interp1(ell_base.t(), Nb.row(j).t(), ell_source.t(), vi, "linear", true); Nbi.row(j) = vi.t();
					cmn::Extra::interp1(ell_base.t(), Db.row(j).t(), ell_source.t(), vi, "linear", true); Dbi.row(j) = vi.t();
				}
			}

			// map coordinates
			Rm(i) = Rbi + 
				Nbi.each_row()%Rs(i).row(cmn::Extra::xyz2idx(source2normal_)) + 
				Dbi.each_row()%Rs(i).row(cmn::Extra::xyz2idx(source2transverse));

			// apply rotation to new frame
			Lm(i) = Lbi.each_row()%Ls(i).row(cmn::Extra::xyz2idx(source2long_)) + 
				Nbi.each_row()%Ls(i).row(cmn::Extra::xyz2idx(source2normal_)) + 
				sgn*(Dbi.each_row()%Ls(i).row(cmn::Extra::xyz2idx(source2transverse)));
			Nm(i) = Lbi.each_row()%Ns(i).row(cmn::Extra::xyz2idx(source2long_)) + 
				Nbi.each_row()%Ns(i).row(cmn::Extra::xyz2idx(source2normal_)) + 
				sgn*(Dbi.each_row()%Ns(i).row(cmn::Extra::xyz2idx(source2transverse)));
			Dm(i) = Lbi.each_row()%Ds(i).row(cmn::Extra::xyz2idx(source2long_)) + 
				Nbi.each_row()%Ds(i).row(cmn::Extra::xyz2idx(source2normal_)) + 
				sgn*(Dbi.each_row()%Ds(i).row(cmn::Extra::xyz2idx(source2transverse)));

			// re-normalize vectors
			Lm(i) = cmn::Extra::normalize(Lm(i));
			Nm(i) = cmn::Extra::normalize(Nm(i));

			// check handedness
			assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(Nm(i),Lm(i)),Dm(i))>RAT_CONST(0.0)));
		});

		
		// create mapped frame
		const ShFramePr frame_mapped= Frame::create(Rm,Lm,Nm,Dm);

		// conserve location
		frame_mapped->set_location(section,turn,num_section_base);

		// apply_transformations
		frame_mapped->apply_transformations(get_transformations(), stngs.time);

		// create new frame
		return frame_mapped;
	}

	// validity check
	bool PathMap::is_valid(const bool enable_throws) const{
		if(base_path_ ==NULL)return false;
		if(source_path_ ==NULL)return false;
		if(!base_path_->is_valid(enable_throws))return false;
		if(!source_path_->is_valid(enable_throws))return false;
		if(source2long_==source2normal_){if(enable_throws){rat_throw_line("source2long can not be equal to source2normal");} return false;};
		if(use_bcr_interp_ && bcr_order_==0){if(enable_throws){rat_throw_line("order for bcr interpolation must be at least 1");} return false;};
		return true;
	}


	// get type
	std::string PathMap::get_type(){
		return "rat::mdl::pathmap";
	}

	// method for serialization into json
	void PathMap::serialize(Json::Value &js, cmn::SList &list) const{

		// parent objects
		Path::serialize(js,list);
		Transformations::serialize(js,list);

		// type
		js["type"] = get_type();

		// fixed index
		js["base_path"] = cmn::Node::serialize_node(base_path_, list);
		js["source_path"] = cmn::Node::serialize_node(source_path_, list);

		// orientation vectors
		js["source2long"] = static_cast<int>(cmn::Extra::xyz2idx(source2long_));
		js["source2normal"] = static_cast<int>(cmn::Extra::xyz2idx(source2normal_));

		// normalization
		js["normalize_length"] = normalize_length_;
		js["loop_around"] = loop_around_;

		js["use_bcr_interp"] = use_bcr_interp_;
		js["bcr_order"] = static_cast<int>(bcr_order_);

	}

	// method for deserialisation from json
	void PathMap::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent objects
		Path::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);

		// deserialize the models
		set_base_path(cmn::Node::deserialize_node<Path>(js["base_path"], list, factory_list, pth));
		set_source_path(cmn::Node::deserialize_node<Path>(js["source_path"], list, factory_list, pth));

		// orientation vectors
		if(js.isMember("source2long"))set_source2long(cmn::Extra::idx2xyz(js["source2long"].asUInt64()));
		if(js.isMember("source2normal"))set_source2normal(cmn::Extra::idx2xyz(js["source2normal"].asUInt64()));

		// normalization
		normalize_length_ = js["normalize_length"].asBool();
		loop_around_ = js["loop_around"].asBool();

		use_bcr_interp_ = js["use_bcr_interp"].asBool();
		bcr_order_ = js["bcr_order"].asUInt64();
	}

}}