// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// only avail
#ifdef ENABLE_NL_SOLVER

// include header file
#include "hbcurvetable.hh"

// code specific to Raccoon
namespace rat{namespace mdl{

	// constructor
	HBCurveTable::HBCurveTable(){
		// set name
		set_name("HB-Curve Table");
		
		// set default table
		// set_team13();
		set_armco_hot_rolled();
	}

	// factory
	ShHBCurveTablePr HBCurveTable::create(){
		return std::make_shared<HBCurveTable>();
	}

	// setting
	void HBCurveTable::set_filling_fraction(const fltp ff){
		ff_ = ff;
	}
	
	void HBCurveTable::set_reference(const std::string& reference){
		reference_ = reference;
	}

	// getting
	fltp HBCurveTable::get_filling_fraction()const{
		return ff_;
	}

	const std::string& HBCurveTable::get_reference()const{
		return reference_;
	}

	// load table from file
	void HBCurveTable::load_from_file(const boost::filesystem::path& fpath){
		arma::field<std::string> header;
		arma::Mat<fltp> M; M.load(arma::csv_name(fpath.string(), header));
		set_table(M);
	}

	// load table from file
	void HBCurveTable::save_to_file(const boost::filesystem::path& fpath)const{
		arma::field<std::string> header(2);
		header(0) = "H [A/m]";
		header(1) = "B [T]";
		const arma::Mat<fltp> M = get_table();
		M.save(arma::csv_name(fpath.string(),header));
	}

	// getters
	void HBCurveTable::set_table(const arma::Mat<fltp> &HB){
		Hinterp_ = HB.col(0);
		Binterp_ = HB.col(1);
	}

	// setters
	arma::Mat<fltp> HBCurveTable::get_table()const{
		return arma::join_horiz(Hinterp_,Binterp_);
	}

	// create hb data
	nl::ShHBDataPr HBCurveTable::create_hb_data()const{
		is_valid(true);
		return nl::HBData::create(Hinterp_,Binterp_,ff_);
	}
	
	// set BH curve for the team 13 problem (figure 3)
	// https://www.compumag.org/wp/wp-content/uploads/2018/06/problem13.pdf
	void HBCurveTable::set_team13(){
		Hinterp_ = {
			0,16.0,30.0,54.0,93.0,143.0,191.0,
			210.0,222.0,233.0,247.0,258.0,272.0,
			289.0,313.0,342.0,377.0,433.0,509.0,
			648.0,933.0,1228.0,1934.0,2913.0,
			4993.0,7189.0,9423.0,9500,10000,15000,
			20000,25000,30000,35000,40000,45000,
			55704.09982,87535.01401,111408.1996,
			190985.4851,270562.7706,270562.7706+0.1/arma::Datum<fltp>::mu_0
		};

		Binterp_ = {
			0.0,0.0025,0.0050,0.0125,0.025,0.05,0.10,
			0.20,0.30,0.40,0.50,0.60,0.70,0.80,0.90,
			1.00,1.1,1.2,1.3,1.4,1.5,1.55,1.6,1.65,1.7,1.75,1.80,
			1.81772453,1.8282464,1.9257046,2.0090528,2.078291,2.1334192,
			2.1744374,2.2013456,2.2141438,2.23,2.27,2.3,2.4,2.5,2.6
		};

		// check lengths
		assert(Hinterp_.n_elem==Binterp_.n_elem);

		// filling fraction
		ff_ = RAT_CONST(1.0);
	}

	// set BH curve for the team 13 problem (figure 3)
	// https://www.compumag.org/wp/wp-content/uploads/2018/06/problem13.pdf
	void HBCurveTable::set_team13_1(){
		Hinterp_ = {
			0,16.0,30.0,54.0,93.0,143.0,191.0,
			210.0,222.0,233.0,247.0,258.0,272.0,
			289.0,313.0,342.0,377.0,433.0,509.0,
			648.0,933.0,1228.0,1934.0,2913.0,
			4993.0,7189.0,9423.0
		};

		Binterp_ = {
			0.0,0.0025,0.0050,0.0125,0.025,0.05,0.10,
			0.20,0.30,0.40,0.50,0.60,0.70,0.80,0.90,
			1.00,1.1,1.2,1.3,1.4,1.5,1.55,1.6,1.65,1.7,1.75,1.80
		};

		// add equation 
		const fltp a = -2.381e-10, b = 2.327e-5, c = 1.590;
		const fltp Hend = 4.9e4;
		const arma::Col<fltp> Heq = arma::linspace(10000.0, Hend, 20);
		const arma::Col<fltp> Beq = a*Heq%Heq + (b+arma::Datum<fltp>::mu_0)*Heq + c;

		// extrapolation
		const arma::Col<fltp> Hext{Heq.back()+1.0/arma::Datum<fltp>::mu_0};
		const arma::Col<fltp> Bext{Beq.back()+1.0};

		// join
		Hinterp_ = arma::join_vert(Hinterp_,Heq,Hext);
		Binterp_ = arma::join_vert(Binterp_,Beq,Bext);
	      
		// check lengths
		assert(Hinterp_.n_elem==Binterp_.n_elem);

		// filling fraction
		ff_ = RAT_CONST(1.0);
	}

	// set BH curve for the team 13 problem (typewritten data)
	// https://www.compumag.org/wp/wp-content/uploads/2018/06/problem13.pdf
	void HBCurveTable::set_team13_2(){
		Hinterp_ = {
			0.0,45.0,75.0,120.0,173.0,201.0,222.0,240.0,250.0,265.0,
			280.0,300.0,330.0,365.0,415.0,500.0,640.0,890.0,
			1150.0,1940.0,3100.0,4370.0,6347.0,8655.0
		};

		Binterp_ = {
			0.0,0.025,0.05,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,
			1.0,1.1,1.2,1.3,1.4,1.5,1.55,1.60,1.65,1.70,1.75,1.80
		};

		// add equation 
		const fltp a = -2.822e-10, b = 2.529e-5, c = 1.591;
		const fltp Hend = 4.35e4;
		const arma::Col<fltp> Heq = arma::linspace(12000.0, Hend, 20);
		const arma::Col<fltp> Beq = a*Heq%Heq + (b+arma::Datum<fltp>::mu_0)*Heq + c;

		// extrapolation
		const arma::Col<fltp> Hext{Heq.back()+1.0/arma::Datum<fltp>::mu_0};
		const arma::Col<fltp> Bext{Beq.back()+1.0};

		// join
		Hinterp_ = arma::join_vert(Hinterp_,Heq,Hext);
		Binterp_ = arma::join_vert(Binterp_,Beq,Bext);
	      
		// check lengths
		assert(Hinterp_.n_elem==Binterp_.n_elem);

		// filling fraction
		ff_ = RAT_CONST(1.0);
	}



	// ARMCO Pure Iron Sheet Hot Rolled
	// data reproduced with written permission from aksteel 
	// https://www.aksteel.eu/
	void HBCurveTable::set_armco_hot_rolled(){
		// magnetic field table
		Hinterp_ = {0.0,4.3041e+01,
			8.3849e+01,1.1484e+02,1.1484e+02,1.5147e+02,
			1.8293e+02,2.1275e+02,2.5056e+02,2.9508e+02,
			3.5638e+02,4.3586e+02,5.8214e+02,8.3849e+02,
			1.5339e+03,3.3465e+03,5.2639e+03,8.1765e+03,
			1.0000e+04,1.0000e+04+1.0/arma::datum::mu_0};

		// flux density table
		Binterp_ = {0.0,5.5249e-03,
			2.2099e-02,4.4199e-02,4.4199e-02,9.3923e-02,
			1.9337e-01,3.3702e-01,5.2486e-01,7.0718e-01,
			8.8950e-01,1.0663e+00,1.2762e+00,1.4530e+00,
			1.5856e+00,1.6906e+00,1.7514e+00,1.8232e+00,
			1.8564e+00,1.8564e+00+1.0};

		// filling fraction
		ff_ = RAT_CONST(1.0);

		// reference
		reference_ = "ARMCO Pure Iron Sheet Hot Rolled, https://www.aksteel.eu";
	}

	// ARMCO Pure Iron Sheet Col Rolled
	// data reproduced with written permission from aksteel 
	// https://www.aksteel.eu/
	void HBCurveTable::set_armco_cold_rolled(){
		// magnetic field
		Hinterp_ = {0.0,2.0934e+01,3.6885e+01,
			7.1716e+01,9.6373e+01,1.1592e+02,1.4117e+02,
			1.7838e+02,2.1722e+02,2.7112e+02,3.1818e+02,
			3.9227e+02,4.7184e+02,6.5793e+02,8.8415e+02,
			1.1736e+03,1.5966e+03,2.4268e+03,3.5987e+03,
			5.0180e+03,6.8269e+03,9.8776e+03,9.8776e+03+1.0/arma::datum::mu_0};

		// flux density
		Binterp_ = {0.0,1.0899e-02,1.6349e-02,
			4.9046e-02,9.8093e-02,1.6894e-01,2.9428e-01,
			4.9046e-01,6.7030e-01,8.5014e-01,9.7003e-01,
			1.1117e+00,1.2153e+00,1.3624e+00,1.4768e+00,
			1.5586e+00,1.6240e+00,1.6785e+00,1.7384e+00,
			1.7766e+00,1.8256e+00,1.8910e+00,1.8910e+00+1.0};

		// filling fraction
		ff_ = RAT_CONST(1.0);

		// reference
		reference_ = "ARMCO Pure Iron Sheet Cold Rolled, https://www.aksteel.eu";
	}

	// vin le van fit
	void HBCurveTable::set_vinh_le_van_fit(const fltp mur, const fltp Js, const fltp ff){
		Hinterp_ = arma::linspace<arma::Col<fltp> >(0.0,60000.0,100);
		Binterp_ = arma::Datum<fltp>::mu_0*Hinterp_ + 
			(2*Js/arma::Datum<fltp>::pi)*arma::atan(
			arma::Datum<fltp>::pi*(mur - RAT_CONST(1.0))*
			arma::Datum<fltp>::mu_0*Hinterp_/(2*Js));
		ff_ = ff; 
	}

	// set constant 
	void HBCurveTable::set_constant_relative_permeability(const fltp mur){
		const fltp mu = mur*arma::Datum<fltp>::mu_0;
		const fltp maxfield = RAT_CONST(1000.0);
		Hinterp_ = {0.0,maxfield/mu};
		Binterp_ = {0.0,maxfield};
	}

	// set BH curve externally
	void HBCurveTable::set_extern(
		const arma::Row<fltp> &Hinterp, 
		const arma::Row<fltp> &Binterp,
		const fltp ff){

		// check lengths
		assert(Hinterp_.n_elem==Binterp_.n_elem);
		assert(ff>0); assert(ff<=1.0);

		// set interpolation arrays
		Hinterp_ = Hinterp.t(); Binterp_ = Binterp.t();	ff_ = ff;
	}

	// vallidity check
	bool HBCurveTable::is_valid(const bool enable_throws) const{
		if(Hinterp_.empty()){if(enable_throws){rat_throw_line("magnetic field table is empty");} return false;};
		if(Binterp_.empty()){if(enable_throws){rat_throw_line("magnetic flux table is empty");} return false;};
		if(Hinterp_.n_elem<=2){if(enable_throws){rat_throw_line("need at least two points in H");} return false;};
		if(Binterp_.n_elem<=2){if(enable_throws){rat_throw_line("need at least two points in B");} return false;};
		if(Hinterp_.n_elem!=Binterp_.n_elem){if(enable_throws){rat_throw_line("magnetic flux and magnetic field must have same number of elements");} return false;};
		if(ff_<=RAT_CONST(0.0)){if(enable_throws){rat_throw_line("filling fraction can not be less than zero");} return false;};
		return true;
	}

	// get type
	std::string HBCurveTable::get_type(){
		return "rat::mdl::hbcurvetable";
	}

	// method for serialization into json
	void HBCurveTable::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents first 
		// this allows the type to be overridden
		HBCurve::serialize(js,list);

		// properties
		js["type"] = get_type();

		// serialize hb curve
		js["ff"] = ff_;
		for(arma::uword i=0;i<Hinterp_.n_elem;i++)
			js["Hinterp"].append(Hinterp_(i));
		for(arma::uword i=0;i<Binterp_.n_elem;i++)
			js["Binterp"].append(Binterp_(i));
		js["reference"] = reference_;
	}

	// method for deserialisation from json
	void HBCurveTable::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		
		// parent objects
		HBCurve::deserialize(js,list,factory_list,pth);

		// serialize hb curve
		ff_ = js["ff"].ASFLTP();
		Hinterp_.set_size(js["Hinterp"].size());arma::uword idx = 0;
		for(auto it=js["Hinterp"].begin();it!=js["Hinterp"].end();it++)
			Hinterp_(idx++) = (*it).ASFLTP();
		Binterp_.set_size(js["Binterp"].size());idx = 0;
		for(auto it=js["Binterp"].begin();it!=js["Binterp"].end();it++)
			Binterp_(idx++) = (*it).ASFLTP();
		if(js.isMember("reference"))set_reference(js["reference"].asString());
	}


}}

#endif