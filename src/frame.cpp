// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "frame.hh"

// rat-common headers
#include "rat/common/extra.hh"
#include "rat/common/elements.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	Frame::Frame(){

	}

	// constructor with dimension input
	Frame::Frame(
		const arma::Mat<fltp> &R, 
		const arma::Mat<fltp> &L, 
		const arma::Mat<fltp> &N, 
		const arma::Mat<fltp> &D,
		const arma::Mat<fltp> &B){
		
		// check handedness (these checks are too strict)
		// assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N,L),D)>RAT_CONST(0.0)));
		// assert(arma::all(cmn::Extra::dot(B,N)>RAT_CONST(0.0)));

		// allocate
		R_.set_size(1,1); L_.set_size(1,1); 
		N_.set_size(1,1); D_.set_size(1,1);
		B_.set_size(1,1);

		// set to self
		R_(0) = R; L_(0) = L; N_(0) = N; D_(0) = D; B_(0) = B;

		// since it is single section we can set 0,0
		section_ = arma::Row<arma::uword>{0};
		turn_ = arma::Row<arma::uword>{0};

		// number of base sections
		set_num_section_base(1);
	}

	// constructor with dimension input and section division
	Frame::Frame(
		const arma::Mat<fltp> &R, 
		const arma::Mat<fltp> &L, 
		const arma::Mat<fltp> &N, 
		const arma::Mat<fltp> &D,
		const arma::Mat<fltp> &B,
		const arma::uword num_sections){
		
		// check handedness (these checks are too strict)
		// assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N,L),D)>RAT_CONST(0.0)));
		// assert(arma::all(cmn::Extra::dot(B,N)>RAT_CONST(0.0)));

		// number of elements
		const arma::uword num_elements = R.n_cols;

		// create field arrays
		R_.set_size(1,num_sections); L_.set_size(1,num_sections), 
		N_.set_size(1,num_sections); D_.set_size(1,num_sections), 
		B_.set_size(1,num_sections);

		// create indexes at which we want to split the sections
		arma::Row<arma::uword> idx_divide = 
			arma::conv_to<arma::Row<arma::uword> >::from(
			arma::floor(arma::regspace<arma::Row<fltp> >(0,fltp(num_sections))*
			fltp(num_elements)/fltp(num_sections)));
		idx_divide(num_sections) = num_elements-1;

		// split sections
		for(arma::uword i=0;i<num_sections;i++){
			const arma::uword idx1 = idx_divide(i);
			const arma::uword idx2 = idx_divide(i+1);
			R_(i) = R.cols(idx1,idx2); L_(i) = L.cols(idx1,idx2);
			N_(i) = N.cols(idx1,idx2); D_(i) = D.cols(idx1,idx2);
			B_(i) = B.cols(idx1,idx2);
		}

		// since it is single section we can set 0,0
		section_ = arma::regspace<arma::Row<arma::uword> >(0,R_.n_elem-1);
		turn_ = arma::Row<arma::uword>(R_.n_elem,arma::fill::zeros);

		// number of base sections
		set_num_section_base(num_sections);
	}


	// constructor with field arrays containing multiple sections
	Frame::Frame(
		const arma::field<arma::Mat<fltp> > &R, 
		const arma::field<arma::Mat<fltp> > &L, 
		const arma::field<arma::Mat<fltp> > &N, 
		const arma::field<arma::Mat<fltp> > &D,
		const arma::field<arma::Mat<fltp> > &B){
		
		// check handedness
		#ifndef NDEBUG
		for(arma::uword i=0;i<R.n_elem;i++){
			assert(R(i).n_cols==L(i).n_cols); assert(R(i).n_cols==N(i).n_cols);
			assert(R(i).n_cols==D(i).n_cols); assert(R(i).n_cols==B(i).n_cols);

			// handedness (these checks are too strict)
			// assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N(i),L(i)),D(i))>RAT_CONST(0.0))); 
			// assert(arma::all(cmn::Extra::dot(B(i),N(i))>RAT_CONST(0.0)));
		}
		#endif

		// check number of sections for vectors
		assert(L.n_elem==R.n_elem);
		assert(N.n_elem==R.n_elem);
		assert(D.n_elem==R.n_elem);
		assert(B.n_elem==R.n_elem);

		// set to self
		R_ = R;	L_ = L;	N_ = N;	D_ = D; B_ = B;

		// since it is single section we can set 0,0
		section_ = arma::regspace<arma::Row<arma::uword> >(0,R_.n_elem-1);
		turn_ = arma::Row<arma::uword>(R_.n_elem,arma::fill::zeros);

		// number of base sections
		set_num_section_base(section_.n_elem);
	}

	// copy constructor with dimension input
	Frame::Frame(const std::list<ShFramePr> &frames){
		// check if input empty
		assert(!frames.empty());

		// get type from first generator
		conductor_type_ = frames.front()->get_conductor_type();

		// number of frame provided
		const arma::uword num_frame = frames.size();

		// calculate number of sections
		arma::Row<arma::uword> num_sections_each(num_frame);

		// walk over generator list
		arma::uword idx = 0;
		for(auto it=frames.begin();it!=frames.end();it++,idx++)
			num_sections_each(idx) = (*it)->get_num_sections();

		// total number of sections
		const arma::uword num_sections = arma::accu(num_sections_each);

		// allocate space for the sections
		R_.set_size(1,num_sections); L_.set_size(1,num_sections);
		N_.set_size(1,num_sections); D_.set_size(1,num_sections);
		B_.set_size(1,num_sections);
		section_.set_size(num_sections);
		turn_.set_size(num_sections);

		// copy sections from the given frame
		arma::uword cnt = 0llu; idx = 0llu;
		for(auto it=frames.begin();it!=frames.end();it++,idx++){
			const ShFramePr& frame = *it;
			const arma::Row<arma::uword> section = frame->get_section();
			const arma::Row<arma::uword> turn = frame->get_turn();
			for(arma::uword j=0;j<num_sections_each(idx);j++){
				R_(cnt) = frame->get_coords(j);
				L_(cnt) = frame->get_direction(j);
				N_(cnt) = frame->get_normal(j);
				D_(cnt) = frame->get_transverse(j);
				B_(cnt) = frame->get_block(j);
				section_(cnt) = section(j);
				turn_(cnt) = turn(j);
				cnt++;
			}
		}

		// // since it is single section we can set 0,0
		// section_ = arma::regspace<arma::Row<arma::uword> >(0,num_sections-1);
		// turn_ = arma::Row<arma::uword>(num_sections,arma::fill::zeros);

		// sanity check
		assert(cnt==num_sections);

		// number of base sections
		set_num_section_base(num_sections);
	}

	// override automatic numbering of sections and turns
	// this is relevant when dealing with cables and coils etc.
	void Frame::set_location(
		const arma::Row<arma::uword> &section,
		const arma::Row<arma::uword> &turn,
		const arma::uword num_section_base){
		assert(section.n_elem==get_num_sections());
		assert(turn.n_elem==get_num_sections());
		section_ = section; turn_ = turn;
		num_section_base_ = num_section_base;
	}

	// get number of base sections
	arma::uword Frame::get_num_section_base() const{
		return num_section_base_;
	}

	// set number of base sections
	void Frame::set_num_section_base(const arma::uword num_section_base){
		assert(num_section_base>0);
		num_section_base_ = num_section_base;
	}

	// factory
	ShFramePr Frame::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<Frame>();
	}

	// factory with single section input
	ShFramePr Frame::create(
		const arma::Mat<fltp> &R, 
		const arma::Mat<fltp> &L, 
		const arma::Mat<fltp> &N, 
		const arma::Mat<fltp> &D){
		return std::make_shared<Frame>(R,L,N,D,N);
	}

	// factory with single section input
	ShFramePr Frame::create(
		const arma::Mat<fltp> &R, 
		const arma::Mat<fltp> &L, 
		const arma::Mat<fltp> &N, 
		const arma::Mat<fltp> &D,
		const arma::uword num_sections){
		return std::make_shared<Frame>(R,L,N,D,N,num_sections);
	}

	// factory with single section input
	ShFramePr Frame::create(
		const arma::field<arma::Mat<fltp> > &R, 
		const arma::field<arma::Mat<fltp> > &L, 
		const arma::field<arma::Mat<fltp> > &N, 
		const arma::field<arma::Mat<fltp> > &D){
		return std::make_shared<Frame>(R,L,N,D,N);
	}

	// factory with single section input
	ShFramePr Frame::create(
		const arma::Mat<fltp> &R, 
		const arma::Mat<fltp> &L, 
		const arma::Mat<fltp> &N, 
		const arma::Mat<fltp> &D,
		const arma::Mat<fltp> &B){
		return std::make_shared<Frame>(R,L,N,D,B);
	}

	// factory with section splitting
	ShFramePr Frame::create(
		const arma::Mat<fltp> &R, 
		const arma::Mat<fltp> &L, 
		const arma::Mat<fltp> &N, 
		const arma::Mat<fltp> &D,
		const arma::Mat<fltp> &B,
		const arma::uword num_sections){
		return std::make_shared<Frame>(R,L,N,D,B,num_sections);
	}

	// factory with single section input
	ShFramePr Frame::create(
		const arma::field<arma::Mat<fltp> > &R, 
		const arma::field<arma::Mat<fltp> > &L, 
		const arma::field<arma::Mat<fltp> > &N, 
		const arma::field<arma::Mat<fltp> > &D,
		const arma::field<arma::Mat<fltp> > &B){
		return std::make_shared<Frame>(R,L,N,D,B);
	}

	// factory creating frame from a set of other frame
	ShFramePr Frame::create(const std::list<ShFramePr> &frames){
		return std::make_shared<Frame>(frames);
	}

	// count number of sections
	arma::uword Frame::get_num_sections() const{
		return R_.n_elem;
	}

	// count number of sections
	arma::uword Frame::get_num_nodes(const arma::uword index) const{
		return R_(index).n_cols;
	}

	// get number of nodes in each section
	arma::Row<arma::uword> Frame::get_num_nodes() const{
		arma::Row<arma::uword> nn(get_num_sections());
		for(arma::uword i=0;i<get_num_sections();i++)
			nn(i) = get_num_nodes(i);
		return nn;
	}

	// get coordinates
	const arma::field<arma::Mat<fltp> >& Frame::get_coords() const{
		return R_;
	}

	// get coordinates
	const arma::field<arma::Mat<fltp> >& Frame::get_direction() const{
		return L_;
	}

	// get coordinates
	const arma::field<arma::Mat<fltp> >& Frame::get_normal() const{
		return N_;
	}

	// get coordinates
	const arma::field<arma::Mat<fltp> >& Frame::get_transverse() const{
		return D_;
	}

	// get coordinates
	const arma::field<arma::Mat<fltp> >& Frame::get_block() const{
		return B_;
	}

	// get coordinates for section
	const arma::Mat<fltp>& Frame::get_coords(const arma::uword section) const{
		assert(section<R_.n_elem);
		return R_(section);
	}

	// get coordinates for section
	const arma::Mat<fltp>& Frame::get_direction(const arma::uword section) const{
		assert(section<L_.n_elem);
		return L_(section);
	}

	// get coordinates for section
	const arma::Mat<fltp>& Frame::get_normal(const arma::uword section) const{
		assert(section<N_.n_elem);
		return N_(section);
	}

	// get coordinates for section
	const arma::Mat<fltp>& Frame::get_transverse(const arma::uword section) const{
		assert(section<D_.n_elem);
		return D_(section);
	}

	// get coordinates for section
	const arma::Mat<fltp>& Frame::get_block(const arma::uword section) const{
		assert(section<B_.n_elem);
		return B_(section);
	}

	// set normal
	void Frame::set_normal(const arma::uword section, const arma::Mat<fltp>&N){
		N_(section) = N;
	}

	// set normal
	void Frame::set_block(const arma::uword section, const arma::Mat<fltp>&B){
		B_(section) = B;
	}

	// get coordinates for section
	arma::Row<fltp> Frame::calc_ashear(const arma::uword section) const{
		assert(section<B_.n_elem);
		assert(B_(section).is_finite());
		assert(N_(section).is_finite());

		// find direction of the angle
		const arma::Row<fltp> dir = arma::sign(cmn::Extra::dot(L_(section),cmn::Extra::cross(D_(section),B_(section))));

		// calculate shear angle
		// const arma::Row<fltp> ashear = dir%arma::acos(arma::clamp(arma::abs(cmn::Extra::dot(B_(section),N_(section))/
		// 	(cmn::Extra::vec_norm(B_(section))%cmn::Extra::vec_norm(N_(section)))),RAT_CONST(0.0),RAT_CONST(1.0)));
		// const arma::Row<fltp> ashear = dir%cmn::Extra::vec_angle(B_(section),N_(section));
		const arma::Row<fltp> ashear = dir%cmn::Extra::vec_angle(B_(section),D_(section));

		// check output
		assert(ashear.is_finite());

		// calculate angle and return
		return ashear;
	}

	// calculate length
	arma::Row<fltp> Frame::calc_ell() const{
		// get counters
		arma::uword num_sections = get_num_sections();

		// allocate length per section
		arma::Row<fltp> ell(num_sections);

		// walk over sections
		for(arma::uword i=0;i<num_sections;i++)
			ell(i) = arma::accu(cmn::Extra::vec_norm(arma::diff(R_(i),1,1)));

		// return sum of lengths
		return ell;
	}

	// simplify
	void Frame::simplify(const fltp tolerance, const arma::Mat<fltp>::fixed<2,2> &bounds){
		// find object size
		const fltp dim = arma::max(calc_size());

		// walk over sections
		for(arma::uword i=0;i<std::min(get_num_sections(), num_section_base_);i++){
			// get number of nodes
			const arma::uword num_nodes = R_(i).n_cols;

			// nodes that we want to keep
			arma::Row<arma::uword> keep(num_nodes,arma::fill::ones);

			// check
			bool dropped_item=true;
			while(dropped_item){
				dropped_item = false;
				const arma::Col<arma::uword> idx = arma::find(keep);
				for(arma::uword j=1;j<idx.n_elem-1;j+=2){
					// checks
					bool keep_node = false, drop_node = false;

					// walk over turns
					for(arma::uword k=i;k<R_.n_elem && !keep_node;k+=num_section_base_){
						for(arma::uword m=0;m<2 && !keep_node;m++){
							for(arma::uword n=0;n<2 && !keep_node;n++){
								// get points
								const arma::Col<fltp>::fixed<3> R0 = 
									R_(k).col(idx(j)) + 
									N_(k).col(idx(j))*bounds(n,0) + 
									D_(k).col(idx(j))*bounds(m,1);
								const arma::Col<fltp>::fixed<3> R1 = 
									R_(k).col(idx(j-1)) + 
									N_(k).col(idx(j-1))*bounds(n,0) + 
									D_(k).col(idx(j-1))*bounds(m,1);
								const arma::Col<fltp>::fixed<3> R2 = 
									R_(k).col(idx(j+1)) + 
									N_(k).col(idx(j+1))*bounds(n,0) + 
									D_(k).col(idx(j+1))*bounds(m,1);

								// distance function
								const fltp offset = arma::as_scalar(cmn::Extra::vec_norm(
									cmn::Extra::cross(R2-R1, R1-R0))/cmn::Extra::vec_norm(R2-R1));

								// check criterion
								if(offset<tolerance*dim){
									drop_node = true;
								}else{
									keep_node = true;
								}
							}
						}
					}

					// decide whether to drop the node
					if(drop_node && !keep_node){
						keep(idx(j))=false;
						dropped_item=true;
					}
				}
			}

			// check dropped count
			// std::cout<<fltp(arma::find(keep).eval().n_elem)/keep.n_elem<<std::endl;

			// drop coordinates
			const arma::Col<arma::uword> idx_keep = arma::find(keep);
			for(arma::uword k=i;k<R_.n_elem;k+=num_section_base_){
				if(keep.n_elem!=R_(k).n_cols)
					rat_throw_line("base sections are not properly mapped");
				R_(k) = R_(k).cols(idx_keep);
				L_(k) = L_(k).cols(idx_keep);
				N_(k) = N_(k).cols(idx_keep);
				D_(k) = D_(k).cols(idx_keep);
				B_(k) = B_(k).cols(idx_keep);
			}
		}
	}

	// calculate length
	fltp Frame::calc_total_ell() const{
		return arma::accu(calc_ell());
	}

	// get section indexes
	const arma::Row<arma::uword>& Frame::get_section() const{
		return section_;
	}

	// get section indexes
	const arma::Row<arma::uword>& Frame::get_turn() const{
		return turn_;
	}

	// get section indexes
	void Frame::set_section(const arma::Row<arma::uword> &section){
		section_ = section;
	}


	// align sections using first section as a reference
	void Frame::align_sections(){
		// take first coordinate of first section as a reference
		align_sections(
			R_(0).col(0), L_(0).col(0),
			N_(0).col(0), D_(0).col(0));
	}

	// method for alligning all sections using start point as a reference
	void Frame::align_sections(
		const arma::Col<fltp>::fixed<3> &R0, 
		const arma::Col<fltp>::fixed<3> &L0, 
		const arma::Col<fltp>::fixed<3> &N0, 
		const arma::Col<fltp>::fixed<3> &D0){

		// get counters
		const arma::uword num_sections = get_num_sections();

		// check input
		assert(num_sections>0);
		assert(R_.n_elem==num_sections); assert(L_.n_elem==num_sections);
		assert(N_.n_elem==num_sections); assert(D_.n_elem==num_sections);

		// walk over sections
		for(arma::uword i=0;i<num_sections;i++){
			// allocate reference point
			arma::Col<fltp>::fixed<3> r1, l1, n1, d1;

			// check input
			assert(arma::all(arma::abs(cmn::Extra::vec_norm(L_(i))-1.0)<1e-9));
			assert(arma::all(arma::abs(cmn::Extra::vec_norm(N_(i))-1.0)<1e-9));

			// for first section reference point is provided
			if(i==0){
				l1 = L0; n1 = N0; d1 = D0;
			}

			// get last orientation vectors of this section
			else{
				// get longitudinal and transverse vector
				l1 = L_(i-1).tail_cols(1);
				n1 = N_(i-1).tail_cols(1);

				// calculate binormal vector
				d1 = cmn::Extra::cross(n1,l1);
			}

			// get last orientation vectors of this section
			const arma::Col<fltp>::fixed<3> l2 = L_(i).head_cols(1);
			const arma::Col<fltp>::fixed<3> n2 = N_(i).head_cols(1);
			//const arma::Col<fltp>::fixed<3> d2 = D_(i).head_cols(1);

			// calculate third component not using D as this one may be non-orthogonal
			const arma::Col<fltp>::fixed<3> d2 = cmn::Extra::cross(n2,l2);

			// std::cout<<arma::as_scalar(cmn::Extra::dot(d1,cmn::Extra::cross(l1,n1)))<<std::endl;
			// std::cout<<arma::as_scalar(cmn::Extra::dot(d2,cmn::Extra::cross(l2,n2)))<<std::endl;

			// check if L and N orthogonal
			if(std::abs(arma::as_scalar(cmn::Extra::dot(l1,n1)))>1e-6)rat_throw_line("L1 and N1 are not orthogonal");
			if(std::abs(arma::as_scalar(cmn::Extra::dot(l2,n2)))>1e-6)rat_throw_line("L2 and N2 are not orthogonal");

			// check if unit vectors
			if(std::abs(arma::as_scalar(cmn::Extra::vec_norm(l1))-RAT_CONST(1.0))>1e-6)rat_throw_line("L1 is not unit vector");
			if(std::abs(arma::as_scalar(cmn::Extra::vec_norm(n1))-RAT_CONST(1.0))>1e-6)rat_throw_line("N1 is not unit vector");
			if(std::abs(arma::as_scalar(cmn::Extra::vec_norm(d1))-RAT_CONST(1.0))>1e-6)rat_throw_line("D1 is not unit vector");
			if(std::abs(arma::as_scalar(cmn::Extra::vec_norm(l2))-RAT_CONST(1.0))>1e-6)rat_throw_line("L2 is not unit vector");
			if(std::abs(arma::as_scalar(cmn::Extra::vec_norm(n2))-RAT_CONST(1.0))>1e-6)rat_throw_line("N2 is not unit vector");
			if(std::abs(arma::as_scalar(cmn::Extra::vec_norm(d2))-RAT_CONST(1.0))>1e-6)rat_throw_line("D2 is not unit vector");

			// check that both coordinate systems have same handedness
			if(!(arma::as_scalar(cmn::Extra::dot(d1,cmn::Extra::cross(l1,n1))) 
				-arma::as_scalar(cmn::Extra::dot(d2,cmn::Extra::cross(l2,n2)))<1e-6))
				rat_throw_line("sections do not have same handedness");

			// create matrix
			arma::Mat<fltp>::fixed<3,3> M = arma::join_horiz(l1,d1,n1)*arma::join_vert(l2.t(),d2.t(),n2.t());
				
			// check buildup of errors
			const fltp deviation = arma::det(M)-RAT_CONST(1.0);
			if(!(std::abs(deviation)<1e-6))rat_throw_line("system is not orthogonal and/or not right handed");

			// clean up matrix to ensure orthogonal rotation 
			// this prevents buildup of tiny machine precision errors
			// after many rotations
			M = cmn::Extra::modified_gram_schmidt_orthogonalization(M);

			// check vectors
			if(!arma::all(arma::abs(M*l2 - l1)<1e-6))rat_throw_line("can not align sections in L");
			if(!arma::all(arma::abs(M*n2 - n1)<1e-6))rat_throw_line("can not align sections in N");
			if(!arma::all(arma::abs(M*d2 - d1)<1e-6))rat_throw_line("can not align sections in D");

			// apply matrix to second section in 
			// order to align it with the first one
			R_(i) = M*R_(i); L_(i) = M*L_(i);
			N_(i) = M*N_(i); D_(i) = M*D_(i);
			B_(i) = M*B_(i);

			// get position of the origins of the two coordinate systems
			if(i==0){
				r1 = R0;
			}

			// last point of previous section
			else{
				r1 = R_(i-1).tail_cols(1);
			}

			// first point of this section
			const arma::Col<fltp>::fixed<3> r2 = R_(i).head_cols(1);

			// shift to location
			R_(i).each_col() += r1 - r2;
		}
	}

	// typical size of frame
	arma::Col<fltp>::fixed<3> Frame::calc_size() const{
		// get counters
		const arma::uword num_sections = get_num_sections();

		// enclosing box
		arma::Col<fltp>::fixed<3> Rmin = R_(0).col(0);
		arma::Col<fltp>::fixed<3> Rmax = R_(0).col(0);

		// find minium and maximum coordinates
		for(arma::uword i=0;i<num_sections;i++){
			Rmin = arma::min(arma::min(R_(i),1),Rmin);
			Rmax = arma::max(arma::max(R_(i),1),Rmax);
		}

		// find maximum span
		return Rmax-Rmin;
	}

	// check if the sections are closed in on themselves
	arma::Row<arma::uword> Frame::is_closed() const{
		// get counters
		const arma::uword num_sections = get_num_sections();

		// typical size
		const fltp dim = arma::max(calc_size());
		
		// allocate
		arma::Row<arma::uword> closed(num_sections);

		// calculate vector between start and end
		for(arma::uword i=0;i<num_sections;i++){
			const arma::Col<fltp>::fixed<3> dR = 
				R_(i).tail_cols(1) - R_(i).head_cols(1);

			// distance between start and end
			const fltp dist = arma::as_scalar(cmn::Extra::vec_norm(dR));

			// check if distance within tolerance
			closed(i) = dist<(dim*tol_closed_);
		}

		// return closed
		return closed;
	}

	// check if the first section is connected to the last section
	bool Frame::is_loop() const{
		// get counters
		const arma::uword num_sections = get_num_sections();

		// enclosing box
		arma::Col<fltp>::fixed<3> Rmin = R_(0).col(0);
		arma::Col<fltp>::fixed<3> Rmax = R_(0).col(0);

		// find minium and maximum coordinates
		for(arma::uword i=0;i<num_sections;i++){
			Rmin = arma::min(arma::min(R_(i),1),Rmin);
			Rmax = arma::max(arma::max(R_(i),1),Rmax);
		}

		// find maximum span
		arma::Col<fltp>::fixed<3> Rdim = Rmax-Rmin;

		// typical size
		fltp dim = arma::max(Rdim);

		// get coordinates
		arma::Col<fltp>::fixed<3> R1 = R_(0).col(0);
		arma::Col<fltp>::fixed<3> R2 = R_(num_sections-1).tail_cols(1);
		arma::Col<fltp>::fixed<3> dR = R2-R1;

		// distance between start and end
		fltp dist = arma::as_scalar(cmn::Extra::vec_norm(dR));

		// check if distance is within tolerance
		return dist<(dim*tol_closed_);
	}

	// combine sections into single set of frame
	void Frame::combine(){
		// get counters
		const arma::uword num_sections = get_num_sections();

		// check if already combined
		if(num_sections==1)return;

		// typical size
		const fltp dim = arma::max(calc_size());

		// remove last node for all sections but last
		arma::uword idx = 0;
		for(arma::uword i=0;i<num_sections-1;i++){
			section_(i) = idx;
			const arma::Col<fltp>::fixed<3> R1 = R_(i).col(R_(i).n_cols-1);
			const arma::Col<fltp>::fixed<3> R2 = R_(i+1).col(0);

			// remove last node
			if(arma::as_scalar(cmn::Extra::vec_norm(R2 - R1)<dim*tol_closed_)){
				R_(i) = R_(i).head_cols(R_(i).n_cols-1); L_(i) = L_(i).head_cols(L_(i).n_cols-1);
				N_(i) = N_(i).head_cols(N_(i).n_cols-1); D_(i) = D_(i).head_cols(D_(i).n_cols-1);
				B_(i) = B_(i).head_cols(B_(i).n_cols-1);
			}

			// sections do not connect next index
			else idx+=1;
		}
		section_.back() = idx;

		// get unique sections
		const arma::Mat<arma::uword> ms = cmn::Extra::find_sections(section_);

		// collapse sections (no longer relevant)
		section_.set_size(ms.n_cols); turn_.set_size(ms.n_cols);

		// walk over sections
		for(arma::uword i=0;i<ms.n_cols;i++){
			// get indexe
			const arma::uword idx1 = ms(0,i);
			const arma::uword idx2 = ms(1,i);
			
			// get sub ranges
			const arma::field<arma::Mat<fltp> > Rt = R_.cols(idx1,idx2);
			const arma::field<arma::Mat<fltp> > Lt = L_.cols(idx1,idx2);
			const arma::field<arma::Mat<fltp> > Nt = N_.cols(idx1,idx2);
			const arma::field<arma::Mat<fltp> > Dt = D_.cols(idx1,idx2);
			const arma::field<arma::Mat<fltp> > Bt = B_.cols(idx1,idx2);

			// combine field arrays
			const arma::Mat<fltp> R = cmn::Extra::field2mat(Rt);
			const arma::Mat<fltp> L = cmn::Extra::field2mat(Lt);
			const arma::Mat<fltp> N = cmn::Extra::field2mat(Nt);
			const arma::Mat<fltp> D = cmn::Extra::field2mat(Dt);
			const arma::Mat<fltp> B = cmn::Extra::field2mat(Bt);

			// combine field arrays
			R_(i) = R; L_(i) = L; N_(i) = N; D_(i) = D; B_(i) = B;

			// set section and turn
			section_(i) = i; turn_(i) = i;
		}

		// set size
		R_ = R_.cols(0,ms.n_cols-1); L_ = L_.cols(0,ms.n_cols-1); N_ = N_.cols(0,ms.n_cols-1); 
		D_ = D_.cols(0,ms.n_cols-1); B_ = B_.cols(0,ms.n_cols-1);

		// set number of base sections
		num_section_base_ = 1;

		// done
		return;
	}

	// split sections into smaller sections with num_sub elements
	void Frame::split(const arma::uword num_sub_elements){
		// get counters
		const arma::uword num_sections = get_num_sections();

		// count number of sections
		const arma::Row<arma::uword> num_nodes = get_num_nodes();
		const arma::Row<arma::uword> num_elements = num_nodes-1;
		arma::Row<arma::uword> num_sub_sections = num_elements/num_sub_elements;

		// extend if last section incomplete
		const arma::Row<arma::uword> idx = arma::find(cmn::Extra::modulus(num_elements, num_sub_elements)!=0).t();
		if(!idx.is_empty())num_sub_sections(idx) = num_sub_sections(idx)+1;

		// get total number of sub sections
		const arma::uword total_num_sub_sections = arma::accu(num_sub_sections);

		// allocate matrices
		arma::field<arma::Mat<fltp> > R(1,total_num_sub_sections);
		arma::field<arma::Mat<fltp> > L(1,total_num_sub_sections);
		arma::field<arma::Mat<fltp> > N(1,total_num_sub_sections);
		arma::field<arma::Mat<fltp> > D(1,total_num_sub_sections);
		arma::field<arma::Mat<fltp> > B(1,total_num_sub_sections);

		// allocate section and turn arrays
		arma::Row<arma::uword> section(total_num_sub_sections);
		arma::Row<arma::uword> turn(total_num_sub_sections);

		// walk over sections
		arma::uword cnt = 0;
		for(arma::uword i=0;i<num_sections;i++){
			for(arma::uword j=0;j<num_sub_sections(i);j++){
				// get cross section indexes (within section)
				const arma::uword idx1 = j*num_sub_elements;
				arma::uword idx2 = (j+1)*num_sub_elements;

				// limit last index
				if(idx2>=R_(i).n_cols)idx2 = R_(i).n_cols-1;

				// copy
				R(cnt) = R_(i).cols(idx1,idx2);
				L(cnt) = L_(i).cols(idx1,idx2);
				N(cnt) = N_(i).cols(idx1,idx2);
				D(cnt) = D_(i).cols(idx1,idx2);
				B(cnt) = B_(i).cols(idx1,idx2);

				// section and turn
				section(cnt) = section_(i);
				turn(cnt) = turn_(i);

				// increment counter
				cnt++;
			}
		}

		// sanity check
		assert(cnt==total_num_sub_sections);

		// set to self
		R_ = R; L_ = L; N_ = N; D_ = D; B_ = B;
		section_ = section; turn_ = turn;

		// done
		return;
	}

	// separate disconnected frames
	std::list<ShFramePr> Frame::separate(const bool separate_all)const{
		// get counters
		const arma::uword num_sections = get_num_sections();

		// typical size
		const fltp dim = arma::max(calc_size());

		// allocate section indices
		arma::Row<arma::uword> sections(num_sections);

		// remove last node for all sections but last
		arma::uword idx = 0;
		for(arma::uword i=0;i<num_sections-1;i++){
			sections(i) = idx;
			const arma::Col<fltp>::fixed<3> R1 = R_(i).col(R_(i).n_cols-1);
			const arma::Col<fltp>::fixed<3> R2 = R_(i+1).col(0);

			// remove last node
			if(arma::as_scalar(cmn::Extra::vec_norm(R2 - R1))>dim*tol_closed_ || separate_all)idx+=1;	
			
		}
		sections.back() = idx;

		// get unique sections
		const arma::Mat<arma::uword> ms = cmn::Extra::find_sections(sections);

		// create frames
		std::list<ShFramePr> frames;

		// walk over separated parts
		for(arma::uword i=0;i<ms.n_cols;i++){
			// find start and end indices
			const arma::uword idx1 = ms(0,i);
			const arma::uword idx2 = ms(1,i);
			
			// copy coordinates and create a new frame
			const ShFramePr sframe = Frame::create(
				R_.cols(idx1,idx2), L_.cols(idx1,idx2),
				N_.cols(idx1,idx2), D_.cols(idx1,idx2),
				B_.cols(idx1,idx2));
			
			// copy location
			sframe->set_location(
				section_.cols(idx1,idx2),
				turn_.cols(idx1,idx2),
				num_section_base_);

			// transfer conductor type
			sframe->set_conductor_type(conductor_type_);

			// add to list
			frames.push_back(sframe);
		}

		// return list of separated frames
		return frames;
	}

	// apply multiple transformations
	void Frame::apply_transformations(const std::list<ShTransPr> &trans, const fltp time){
		for(auto it=trans.begin();it!=trans.end();it++)
			apply_transformation(*it,time);
	}

	// apply transformation
	void Frame::apply_transformation(const ShTransPr &trans, const fltp time){
		// apply to vectors	
		trans->apply_vectors(R_,L_,'L',time);
		trans->apply_vectors(R_,N_,'N',time);
		trans->apply_vectors(R_,D_,'D',time);
		trans->apply_vectors(R_,B_,'B',time);

		// apply to coordinates
		trans->apply_coords(R_,time);

		// check handedness
		#ifndef NDEBUG
		for(arma::uword i=0;i<L_.n_elem;i++)
			assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N_(i),L_(i)),D_(i))>RAT_CONST(0.0)));
		#endif
	}

	// reverse the path
	void Frame::reverse(){
		// reverse field arrays
		cmn::Extra::reverse_field(R_); cmn::Extra::reverse_field(L_);
		cmn::Extra::reverse_field(N_); cmn::Extra::reverse_field(D_);
		cmn::Extra::reverse_field(B_);

		// flip sections
		section_ = arma::fliplr(section_); 
		turn_ = arma::fliplr(turn_);

		// flip L and D (reverse)
		for(arma::uword i=0;i<L_.n_elem;i++){
			L_(i) *= -1; D_(i) *= -1;
			assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N_(i),L_(i)),D_(i))>RAT_CONST(0.0)));
		}
	}

	// reverse the path
	void Frame::flip(){
		// flip N and D
		for(arma::uword i=0;i<N_.n_elem;i++){
			N_(i) *= -1; D_(i) *= -1; B_(i) *= -1;
			assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N_(i),L_(i)),D_(i))>RAT_CONST(0.0)));
		}
	}

	// swap transverse and normal orientation vectors
	void Frame::flip_orientation(const bool flip_back){
		for(arma::uword i=0;i<N_.n_elem;i++){
			// check dimensions
			assert(N_(i).n_cols==D_(i).n_cols);
			
			if(flip_back)D_(i)*=-1;

			// swap vectors
			std::swap(B_(i),D_(i));

			// update block vector
			N_(i) = B_(i);

			// to ensure handedness is not changed we have to flip D
			if(!flip_back)D_(i)*=-1;

			// check handedness
			assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N_(i),L_(i)),D_(i))>RAT_CONST(0.0)));
		}
	}

	// set type for the frame
	void Frame::set_conductor_type(const DbType conductor_type){
		conductor_type_ = conductor_type;
	}

	// get type for the frame
	Frame::DbType Frame::get_conductor_type() const{
		return conductor_type_;
	}


	// loft core function
	void Frame::create_loft(arma::Mat<fltp> &R, 
		arma::Mat<fltp> &L, arma::Mat<fltp> &N, 
		arma::Mat<fltp> &D, arma::Mat<arma::uword> &n, 
		arma::Mat<arma::uword> &s, 
		const arma::Mat<fltp> &R2d, 
		const arma::Mat<fltp> &N2d,
		const arma::Mat<fltp> &D2d,
		const arma::Mat<arma::uword> &n2d,
		const arma::Mat<arma::uword> &s2d,
		const bool cap_start,
		const bool cap_end,
		const bool use_parallel) const{

		// get counters
		const arma::uword num_sections = get_num_sections();
		// check whether the path forms a closed loop
		const arma::Row<arma::uword> close = is_closed();

		// check if path is reversed
		fltp sign = arma::as_scalar(
			cmn::Extra::dot(cmn::Extra::cross(N_(0).col(0),L_(0).col(0)),D_(0).col(0)));
		bool is_reversed = sign>0;

		// get counters for 2D mesh
		const arma::uword Ne2d = n2d.n_cols; // number of elements
		const arma::uword Nn2d = R2d.n_cols; // number of nodes
		const arma::uword Nnpe = n2d.n_rows; // number of nodes per element
		const arma::uword Nse2d = s2d.n_cols; // number of surface elements
		const arma::uword Nnpse = s2d.n_rows; // number of nodes per surface element

		// allocate 3D mesh field arrays
		arma::field<arma::Mat<fltp> > R3d(1,num_sections);
		arma::field<arma::Mat<arma::uword> > n3d(1,num_sections);
		arma::field<arma::Mat<arma::uword> > s3d(1,num_sections);

		// allocate 3D orientation vectors
		arma::field<arma::Mat<fltp> > L3d(1,num_sections);
		arma::field<arma::Mat<fltp> > N3d(1,num_sections);
		arma::field<arma::Mat<fltp> > D3d(1,num_sections);

		// walk over sections
		// for(arma::uword j=0;j<num_sections;j++){
		cmn::parfor(0,num_sections,use_parallel,
			[&](const arma::uword j, int /*cpu*/){
			// number of nodes in the base path
			const arma::uword num_base = R_(j).n_cols;

			// allocate coordinate matrices
			arma::Mat<fltp> x(Nn2d,num_base), y(Nn2d,num_base), z(Nn2d,num_base);

			// transformation for block type using block vector
			if(conductor_type_==block){
				// calculate shear angle
				const arma::Row<fltp> sinashear = arma::sin(calc_ashear(j));

				// walk over 2D coordinates and lay out along path
				for(arma::uword i=0;i<Nn2d;i++){
					x.row(i) = R_(j).row(0) + B_(j).row(0)*R2d(0,i)/sinashear + D_(j).row(0)*R2d(1,i);
					y.row(i) = R_(j).row(1) + B_(j).row(1)*R2d(0,i)/sinashear + D_(j).row(1)*R2d(1,i);
					z.row(i) = R_(j).row(2) + B_(j).row(2)*R2d(0,i)/sinashear + D_(j).row(2)*R2d(1,i);
				}
			}

			// other types presently use normal vector
			else{
				// walk over 2D coordinates and lay out along path
				for(arma::uword i=0;i<Nn2d;i++){
					// walk over 2D coordinates and lay out along path
					x.row(i) = R_(j).row(0) + N_(j).row(0)*R2d(0,i) + D_(j).row(0)*R2d(1,i);
					y.row(i) = R_(j).row(1) + N_(j).row(1)*R2d(0,i) + D_(j).row(1)*R2d(1,i);
					z.row(i) = R_(j).row(2) + N_(j).row(2)*R2d(0,i) + D_(j).row(2)*R2d(1,i);
				}	
			}

			// allocate orientation matrices
			arma::Mat<fltp> lx(Nn2d,num_base), dx(Nn2d,num_base), nx(Nn2d,num_base);
			arma::Mat<fltp> ly(Nn2d,num_base), dy(Nn2d,num_base), ny(Nn2d,num_base);
			arma::Mat<fltp> lz(Nn2d,num_base), dz(Nn2d,num_base), nz(Nn2d,num_base);

			// setup node orientation vectors
			for(arma::uword i=0;i<Nn2d;i++){
				// lx.row(i) = L_(j).row(0); dx.row(i) = D_(j).row(0); nx.row(i) = N_(j).row(0);
				// ly.row(i) = L_(j).row(1); dy.row(i) = D_(j).row(1); ny.row(i) = N_(j).row(1);
				// lz.row(i) = L_(j).row(2); dz.row(i) = D_(j).row(2); nz.row(i) = N_(j).row(2);
				lx.row(i) = L_(j).row(0); 
				ly.row(i) = L_(j).row(1); 
				lz.row(i) = L_(j).row(2); 

				dx.row(i) = D2d(1,i)*D_(j).row(0) + D2d(0,i)*N_(j).row(0);
				dy.row(i) = D2d(1,i)*D_(j).row(1) + D2d(0,i)*N_(j).row(1);
				dz.row(i) = D2d(1,i)*D_(j).row(2) + D2d(0,i)*N_(j).row(2);

				nx.row(i) = N2d(1,i)*D_(j).row(0) + N2d(0,i)*N_(j).row(0);
				ny.row(i) = N2d(1,i)*D_(j).row(1) + N2d(0,i)*N_(j).row(1);
				nz.row(i) = N2d(1,i)*D_(j).row(2) + N2d(0,i)*N_(j).row(2);
			}

			// close last section to first
			arma::uword num_base_used = num_base;
			if(close(j))num_base_used--;

			// generate coordinate vector
			R3d(j).set_size(3,Nn2d*num_base_used);
			R3d(j).row(0) = arma::reshape(x,1,Nn2d*num_base_used);
			R3d(j).row(1) = arma::reshape(y,1,Nn2d*num_base_used);
			R3d(j).row(2) = arma::reshape(z,1,Nn2d*num_base_used);

			// a base of one means only cross section
			if(num_base==1 && num_sections==1){
				n3d(j) = n2d;
				s3d(j) = n2d;
			}

			// normal case
			else{
				// allocate mesh elements
				n3d(j).set_size(2*Nnpe,Ne2d*(num_base_used-1));
				s3d(j).set_size(2*Nnpse,Nse2d*(num_base_used-1));

				// generate mesh elements for volume
				if(Ne2d>0){
					for(arma::uword i=0;i<num_base_used-1;i++){
						n3d(j).submat(0,i*Ne2d,Nnpe-1,(i+1)*Ne2d-1) = n2d + i*Nn2d;
						n3d(j).submat(Nnpe,i*Ne2d,2*Nnpe-1,(i+1)*Ne2d-1) = n2d + (i+1)*Nn2d;
					}
				}

				// generate mesh elements for surface
				if(Nse2d>0){
					for(arma::uword i=0;i<num_base_used-1;i++){
						s3d(j).submat(0,i*Nse2d,Nnpse-1,(i+1)*Nse2d-1) = s2d + i*Nn2d;
						s3d(j).submat(Nnpse,i*Nse2d,2*Nnpse-1,(i+1)*Nse2d-1) = s2d + (i+1)*Nn2d;
					}
				}

				// close mesh for closed paths
				if(close(j)){
					n3d(j) = arma::join_horiz(n3d(j),
						arma::join_vert(n2d + (num_base_used-1)*Nn2d,n2d));
					s3d(j) = arma::join_horiz(s3d(j),
						arma::join_vert(s2d + (num_base_used-1)*Nn2d,s2d));
				}

				// invert mesh elements
				// in case path was defined in the "wrong" direction
				if(is_reversed){
					// flip face 1
					n3d(j).rows(0,n3d(j).n_rows/2-1) = 
						arma::flipud(n3d(j).rows(0,n3d(j).n_rows/2-1));

					// flip face 2
					n3d(j).rows(n3d(j).n_rows/2,n3d(j).n_rows-1) = 
						arma::flipud(n3d(j).rows(n3d(j).n_rows/2,n3d(j).n_rows-1));

					// flip face 1
					s3d(j).rows(0,s3d(j).n_rows/2-1) = 
						arma::flipud(s3d(j).rows(0,s3d(j).n_rows/2-1));

					// flip face 2
					s3d(j).rows(s3d(j).n_rows/2,s3d(j).n_rows-1) = 
						arma::flipud(s3d(j).rows(s3d(j).n_rows/2,s3d(j).n_rows-1));
				}

				// cap start and end for quadrilateral surface of hexahedron mesh
				if(!close(j) && n2d.n_rows==4){
					// check if the 3D surface mesh also has 4 rows
					assert(s3d(j).n_rows==4);

					// get start and end cap meshes
					arma::Mat<arma::uword> ncap1 = n2d.rows(arma::Col<arma::uword>::fixed<4>{0,1,3,2});
					arma::Mat<arma::uword> ncap2 = n2d.rows(arma::Col<arma::uword>::fixed<4>{0,3,1,2});

					if(!is_reversed)ncap1.swap(ncap2);

					// what to cap
					if(cap_start && cap_end){
						s3d(j) = arma::join_horiz(ncap1,s3d(j),ncap2 + (num_base_used-1)*Nn2d);
					}else if(cap_start){
						s3d(j) = arma::join_horiz(ncap1,s3d(j));
					}else if(cap_end){
						s3d(j) = arma::join_horiz(s3d(j),ncap2 + (num_base_used-1)*Nn2d);
					}
				}

				// fix quadrilateral elements 
				// by flipping second set of rows
				if(n3d(j).n_rows==4)n3d(j).swap_rows(2,3);
				if(s3d(j).n_rows==4)s3d(j).swap_rows(2,3);
			}

			// generate longitudinal orientation vector
			L3d(j).set_size(3,Nn2d*num_base_used);
			L3d(j).row(0) = arma::reshape(lx,1,Nn2d*num_base_used);
			L3d(j).row(1) = arma::reshape(ly,1,Nn2d*num_base_used);
			L3d(j).row(2) = arma::reshape(lz,1,Nn2d*num_base_used);

			// generate normal orientation vector
			N3d(j).set_size(3,Nn2d*num_base_used);
			N3d(j).row(0) = arma::reshape(nx,1,Nn2d*num_base_used);
			N3d(j).row(1) = arma::reshape(ny,1,Nn2d*num_base_used);
			N3d(j).row(2) = arma::reshape(nz,1,Nn2d*num_base_used);

			// generate transverse orientation vector
			D3d(j).set_size(3,Nn2d*num_base_used);
			D3d(j).row(0) = arma::reshape(dx,1,Nn2d*num_base_used);
			D3d(j).row(1) = arma::reshape(dy,1,Nn2d*num_base_used);
			D3d(j).row(2) = arma::reshape(dz,1,Nn2d*num_base_used);
		});

		// keep track of node shift with the sections
		arma::uword node_offset = 0;
		for(arma::uword j=0;j<num_sections;j++){
			// apply node offset
			n3d(j) += node_offset;
			s3d(j) += node_offset;

			// offset nodes
			node_offset += R3d(j).n_cols;
		}

		// combine arrays for output
		R = cmn::Extra::field2mat(R3d); L = cmn::Extra::field2mat(L3d);
		N = cmn::Extra::field2mat(N3d); D = cmn::Extra::field2mat(D3d);
		n = cmn::Extra::field2mat(n3d); s = cmn::Extra::field2mat(s3d);
	}

	// perform coordinate shift along normal direction
	arma::Mat<fltp> Frame::perform_normal_shift(
		const arma::uword section, const arma::Row<fltp> &n) const{
		// allocate output
		arma::Mat<fltp> Rs;

		// perform shift 
		if(conductor_type_==block){
			// calculate shear angle
			const arma::Row<fltp> ashear = calc_ashear(section);

			// perform shift with squeeze mapping
			//Rs = R_(section) + B_(section).each_row()%n;
			Rs = R_(section) + B_(section).each_row()%(n/arma::sin(ashear)); // ?
		}else{
			// perform normal shift
			Rs = R_(section) + N_(section).each_row()%n;
		}

		// return shifted vectors
		return Rs;
	}

	// perform coordinate shift along 
	// normal and transverse direction
	arma::Mat<fltp> Frame::perform_shift(
		const arma::uword section, const arma::Row<fltp> &n, const arma::Row<fltp> &d) const{
		// allocate output
		arma::Mat<fltp> Rs;

		// perform shift 
		if(conductor_type_==block){
			// calculate shear angle
			const arma::Row<fltp> ashear = calc_ashear(section);

			// perform shift with shear and squeeze mapping
			// Rs = R_(section) + 
			// 	B_(section).each_row()%(n/arma::sin(ashear) + arma::cos(ashear)%(d%arma::sin(ashear))) + 
			// 	D_(section).each_row()%(d%arma::sin(ashear));

			Rs = R_(section) + 
				B_(section).each_row()%(n/arma::sin(ashear)) + 
				D_(section).each_row()%d;
		}else{
			// perform normal shift
			Rs = R_(section) + N_(section).each_row()%n + D_(section).each_row()%d;
		}

		// return shifted vectors
		return Rs;
	}

	// perform coordinate shift along 
	// normal and transverse direction
	arma::Mat<fltp> Frame::perform_shift(
		const arma::uword section, const fltp n, const fltp d) const{
		// allocate output
		arma::Mat<fltp> Rs;

		// perform shift 
		if(conductor_type_==block){
			// calculate shear angle
			const arma::Row<fltp> ashear = calc_ashear(section);

			// perform shift with shear and squeeze mapping
			Rs = R_(section) + 
				B_(section).each_row()%(n/arma::sin(ashear)) + 
				D_(section)*d;
		}else{
			// perform normal shift
			Rs = R_(section) + N_(section)*n + D_(section)*d;
		}

		// return shifted vectors
		return Rs;
	}

	// get sub frame
	ShFramePr Frame::get_sub(const arma::uword section, const arma::uword idx1, const arma::uword idx2) const{
		// create sub frame
		ShFramePr frm = Frame::create();

		// copy coordinates and orientation vectors
		frm->R_.set_size(1); frm->R_(0) = R_(section).cols(idx1,idx2);
		frm->L_.set_size(1); frm->L_(0) = L_(section).cols(idx1,idx2);
		frm->N_.set_size(1); frm->N_(0) = N_(section).cols(idx1,idx2);
		frm->D_.set_size(1); frm->D_(0) = D_(section).cols(idx1,idx2);
		frm->B_.set_size(1); frm->B_(0) = B_(section).cols(idx1,idx2);

		// set location
		frm->section_.set_size(1); frm->section_(0) = section_(section);
		frm->turn_.set_size(1); frm->turn_(0) = turn_(section);

		// copy type
		frm->conductor_type_ = conductor_type_;

		// return subframe
		return frm;
	}

	// get sub frame
	ShFramePr Frame::get_sub(const arma::uword section) const{
		// create sub frame
		ShFramePr frm = Frame::create();

		// copy coordinates and orientation vectors
		frm->R_.set_size(1); frm->R_(0) = R_(section);
		frm->L_.set_size(1); frm->L_(0) = L_(section);
		frm->N_.set_size(1); frm->N_(0) = N_(section);
		frm->D_.set_size(1); frm->D_(0) = D_(section);
		frm->B_.set_size(1); frm->B_(0) = B_(section);

		// set location
		frm->section_.set_size(1); frm->section_(0) = section_(section);
		frm->turn_.set_size(1); frm->turn_(0) = turn_(section);

		// copy type
		frm->conductor_type_ = conductor_type_;

		// return subframe
		return frm;
	}

	// calculate curvature of the frame
	arma::field<arma::Mat<fltp> > Frame::calc_curvature() const{

		// allocate curvature
		arma::field<arma::Mat<fltp> > curvature(R_.n_elem);

		// walk over sections
		for(arma::uword i=0;i<R_.n_elem;i++)
			curvature(i) = cmn::Extra::calc_curvature(R_(i));

		// return values
		return curvature;
	}

}}