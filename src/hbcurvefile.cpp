// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// only avail
#ifdef ENABLE_NL_SOLVER

// include header file
#include "hbcurvefile.hh"

// code specific to Raccoon
namespace rat{namespace mdl{

	// constructor
	HBCurveFile::HBCurveFile(){
		set_name("HB-Curve File");
	}

	// factory
	ShHBCurveFilePr HBCurveFile::create(){
		return std::make_shared<HBCurveFile>();
	}

	// setters
	void HBCurveFile::set_hb_file(const boost::filesystem::path &hb_file){
		hb_file_ = hb_file;
	}

	void HBCurveFile::set_filling_fraction(const fltp ff){
		ff_ = ff;
	}
	
	// getters
	const boost::filesystem::path& HBCurveFile::get_hb_file()const{
		return hb_file_;
	}

	fltp HBCurveFile::get_filling_fraction()const{
		return ff_;
	}

	arma::Mat<fltp> HBCurveFile::get_table()const{
		// check if file exists
		boost::system::error_code ec;
		if(!boost::filesystem::exists(hb_file_,ec))
			rat_throw_line("HB file does not exist");

		// load data from file
		arma::Mat<fltp> M; arma::field<std::string> header;
		M.load(arma::csv_name(hb_file_.string(), header));

		// check format
		if(M.n_cols!=2)rat_throw_line("file must contain two columns with H and B respectively");

		// return table
		return M;
	}

	// create hb data
	nl::ShHBDataPr HBCurveFile::create_hb_data()const{
		// check validity
		is_valid(true);

		// get table
		const arma::Mat<fltp> M = get_table();

		// create data object
		return nl::HBData::create(M.col(0),M.col(1),ff_);
	}

	// vallidity check
	bool HBCurveFile::is_valid(const bool enable_throws) const{
		if(hb_file_.empty()){if(enable_throws){rat_throw_line("input file not set");} return false;};
		boost::system::error_code ec;
		if(!boost::filesystem::exists(hb_file_,ec)){if(enable_throws){rat_throw_line("input does not exist");} return false;};
		if(ff_<=RAT_CONST(0.0)){if(enable_throws){rat_throw_line("filling fraction can not be less than zero");} return false;};
		return true;
	}

	// get type
	std::string HBCurveFile::get_type(){
		return "rat::mdl::hbcurvefile";
	}

	// method for serialization into json
	void HBCurveFile::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents first 
		// this allows the type to be overridden
		HBCurve::serialize(js,list);

		// properties
		js["type"] = get_type();

		// filename
		js["hb_file"] = hb_file_.string();
	}

	// method for deserialisation from json
	void HBCurveFile::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		
		// parent objects
		HBCurve::deserialize(js,list,factory_list,pth);

		// get path
		boost::filesystem::path hb_file(
			Node::fix_filesep(js["hb_file"].asString()));

		// check if path exists
		boost::system::error_code ec;
		if(!boost::filesystem::exists(hb_file,ec)){
			if(boost::filesystem::exists(pth/hb_file.filename()))
				hb_file = pth/hb_file.filename();
		}

		// filename
		set_hb_file(hb_file);
	}


}}

#endif