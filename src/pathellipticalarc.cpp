// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathellipticalarc.hh"

// rat-common headers
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathEllipticalArc::PathEllipticalArc(){
		// set default name
		set_name("Elliptical Arc");

		// set parameters
		set_semi_major(0.1); set_element_size(2e-3);
		set_semi_minor(0.05); set_element_size(2e-3);
		set_theta1(0.0); set_theta2(arma::Datum<fltp>::pi/2);
		set_offset(0.0);
	}

	// constructor with dimension input
	PathEllipticalArc::PathEllipticalArc(
		const fltp semi_major, const fltp semi_minor,
		const fltp theta1, const fltp theta2,
		const fltp element_size, const fltp offset, const bool transverse) : PathEllipticalArc(){

		// set parameters
		set_semi_major(semi_major); set_semi_minor(semi_minor);
		set_element_size(element_size); set_theta1(theta1); set_theta2(theta2);
		set_offset(offset); set_transverse(transverse);
	}

	// factory
	ShPathEllipticalArcPr PathEllipticalArc::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<PathEllipticalArc>();
	}

	// factory with dimension input
	ShPathEllipticalArcPr PathEllipticalArc::create(
		const fltp semi_major, const fltp semi_minor,
		const fltp theta1, const fltp theta2,
		const fltp element_size, const fltp offset, const bool transverse){
		return std::make_shared<PathEllipticalArc>(semi_major,semi_minor,theta1,theta2,element_size,offset,transverse);
	}

	// set transverse
	void PathEllipticalArc::set_transverse(const bool transverse){
		transverse_ = transverse;
	}

	// set path offset
	void PathEllipticalArc::set_offset(const fltp offset){
		offset_ = offset;
	}

	// set element size
	void PathEllipticalArc::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	// set arc length theta1
	void PathEllipticalArc::set_theta1(const fltp theta1){
		theta1_ = theta1;
	}

	// set arc length theta2
	void PathEllipticalArc::set_theta2(const fltp theta2){
		theta2_ = theta2;
	}

	// set semi major
	void PathEllipticalArc::set_semi_major(const fltp semi_major){
		semi_major_ = semi_major;
	}

	// set semi minor
	void PathEllipticalArc::set_semi_minor(const fltp semi_minor){
		semi_minor_ = semi_minor;
	}

	void PathEllipticalArc::set_negative_bend(const bool negative_bend){
		negative_bend_ = negative_bend;
	}

	// get transverse
	bool PathEllipticalArc::get_transverse()const{
		return transverse_;
	}

	// get path offset
	fltp PathEllipticalArc::get_offset()const{
		return offset_;
	}

	// get element size
	fltp PathEllipticalArc::get_element_size()const{
		return element_size_;
	}

	// get arc length theta1
	fltp PathEllipticalArc::get_theta1()const{
		return theta1_;
	}

	// get arc length theta2
	fltp PathEllipticalArc::get_theta2()const{
		return theta2_;
	}

	// set semi major
	fltp PathEllipticalArc::get_semi_major()const{
		return semi_major_;
	}

	// set semi minor
	fltp PathEllipticalArc::get_semi_minor()const{
		return semi_minor_;
	}

	bool PathEllipticalArc::get_negative_bend()const{
		return negative_bend_;
	}

	// method for creating the frame
	ShFramePr PathEllipticalArc::create_frame(const MeshSettings &stngs) const{
		// check if is valid
		is_valid(true);

		// decide which part of the circle to use
		fltp uoff,rhos_a,rhos_b;
		if(negative_bend_){
			rhos_a = semi_major_;
			rhos_b = -semi_minor_;
			uoff = semi_minor_; 
		}else{
			rhos_a = semi_major_;
			rhos_b = semi_minor_;
			uoff = -semi_minor_; 
		}

		// calculate number of nodes
		const fltp semi_major_off = semi_major_ + offset_;
		const fltp semi_minor_off = semi_minor_ + offset_;
		const fltp perimeter = arma::Datum<fltp>::tau*std::max(semi_major_off,semi_minor_off)*(std::abs(theta2_ - theta1_)/arma::Datum<fltp>::tau);
		// const fltp perimeter = arma::Datum<fltp>::pi*(3*(semi_major_off+semi_minor_off) - std::sqrt((3*semi_major_off + semi_minor_off)*(semi_major_off + 3*semi_minor_off)));
		arma::uword num_nodes = std::max(2llu,static_cast<arma::uword>(std::ceil(perimeter/element_size_)+1));
		while((num_nodes-1)%stngs.element_divisor!=0)num_nodes++;

		// create cylindrical coordinates for circle
		// const arma::Row<fltp> rhovec = arma::linspace<arma::Row<fltp> >(rho,rho,num_nodes);
		const arma::Row<fltp> theta_linear = arma::linspace<arma::Row<fltp> >(theta1_,theta2_,num_nodes);
		const arma::Row<fltp> ell = arma::join_horiz(arma::Row<fltp>{RAT_CONST(0.0)},arma::cumsum(cmn::Extra::vec_norm(arma::diff(arma::join_vert(rhos_a*arma::sin(theta_linear), rhos_b*arma::cos(theta_linear)),1,1))));
		const arma::Row<fltp> ell_linear = arma::linspace<arma::Row<fltp> >(ell.front(),ell.back(),ell.n_elem);

		// find theta such that ell is linear
		arma::Col<fltp> theta_col; cmn::Extra::interp1(ell.t(),theta_linear.t(),ell_linear.t(),theta_col,"linear",true);
		const arma::Row<fltp> theta = theta_col.t();

		// allocate
		arma::Mat<fltp> R(3,num_nodes,arma::fill::zeros);
		arma::Mat<fltp> L(3,num_nodes,arma::fill::zeros);
		arma::Mat<fltp> D(3,num_nodes,arma::fill::zeros);
		arma::Mat<fltp> N(3,num_nodes,arma::fill::zeros);

		// bend in transverse direction
		if(transverse_){
			// coordinates
			R.row(1) = rhos_a*arma::sin(theta);			// semi-major axis scaling
			R.row(2) = rhos_b*arma::cos(theta) + uoff;	// semi-minor axis scaling

			// direction
			L.row(1) = rhos_a*arma::cos(theta);
			L.row(2) = -rhos_b*arma::sin(theta);

			L.each_row()/=cmn::Extra::vec_norm(L);

			// normal vector
			N.row(0).fill(RAT_CONST(1.0));

			// set remaining orientation vector
			D = cmn::Extra::cross(N,L);
		}

		// bend in normal direction
		else{
			// coordinates
			R.row(0) = rhos_b*arma::cos(theta)+uoff;
			R.row(1) = rhos_a*arma::sin(theta);

			// direction
			L.row(0) = -rhos_b*arma::sin(theta);
			L.row(1) = rhos_a*arma::cos(theta);

			L.each_row()/=cmn::Extra::vec_norm(L);

			// direction vector
			D.row(2).fill(RAT_CONST(1.0));

			// set remaining orientation vector
			N = cmn::Extra::cross(L,D);
		}

		// check handedness
		assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N,L),D)>RAT_CONST(0.0)));

		// create frame and return
		return Frame::create(R,L,N,D);
	}

	// validity check
	bool PathEllipticalArc::is_valid(const bool enable_throws) const{
		if(semi_major_<=0){if(enable_throws){rat_throw_line("semi major must be larger than zero");} return false;};
		if(semi_minor_<=0){if(enable_throws){rat_throw_line("semi minor must be larger than zero");} return false;};
		if(theta1_>=theta2_){if(enable_throws){rat_throw_line("theta1 must be smaller than theta2");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string PathEllipticalArc::get_type(){
		return "rat::mdl::pathellipticalarc";
	}

	// method for serialization into json
	void PathEllipticalArc::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Path::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["negative_bend"] = negative_bend_;
		js["transverse"] = transverse_;
		js["offset"] = offset_;
		js["element_size"] = element_size_;
		js["semi_major"] = semi_major_;
		js["semi_minor"] = semi_minor_;
		js["theta1"] = theta1_;
		js["theta2"] = theta2_;
	}

	// method for deserialisation from json
	void PathEllipticalArc::deserialize(const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent
		Path::deserialize(js,list,factory_list,pth);

		// properties
		set_negative_bend(js["negative_bend"].asBool());
		set_transverse(js["transverse"].asBool());
		set_offset(js["offset"].ASFLTP());
		set_element_size(js["element_size"].ASFLTP());
		set_semi_major(js["semi_major"].ASFLTP());
		set_semi_minor(js["semi_minor"].ASFLTP());
		set_theta1(js["theta1"].ASFLTP());
		set_theta2(js["theta2"].ASFLTP());
	}

}}

