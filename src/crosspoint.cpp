// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "crosspoint.hh"

#include "rat/common/error.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CrossPoint::CrossPoint(){
		set_name("Point");
	}

	// constructor with input
	CrossPoint::CrossPoint(
		const fltp nc, const fltp tc, 
		const fltp thickness, const fltp width) : CrossPoint(){
		set_coord(nc,tc);
		set_thickness(thickness);
		set_width(width);
	}

	// factory
	ShCrossPointPr CrossPoint::create(){
		return std::make_shared<CrossPoint>();
	}

	// factory with dimension input
	ShCrossPointPr CrossPoint::create(
		const fltp nc, const fltp tc, const fltp thickness, const fltp width){
		return std::make_shared<CrossPoint>(nc,tc,thickness,width);
	}

	// set dimensions in the normal direction
	void CrossPoint::set_coord(const fltp nc, const fltp tc){
		set_nc(nc); set_tc(tc);
	}

	// set hidden area
	void CrossPoint::set_thickness(const fltp thickness){
		// if(thickness_==RAT_CONST(0.0))rat_throw_line("must have finite thickness");
		thickness_ = thickness;
	}

	// set hidden area
	void CrossPoint::set_width(const fltp width){
		// if(width==RAT_CONST(0.0))rat_throw_line("must have finite width");
		width_ = width;
	}

	// set dimensions in the normal direction
	void CrossPoint::set_nc(const fltp nc){
		nc_ = nc;
	}

	// set dimensions in the normal direction
	void CrossPoint::set_tc(const fltp tc){
		tc_ = tc;
	}

	// get thickness
	fltp CrossPoint::get_thickness() const{
		return thickness_;
	}

	// get width
	fltp CrossPoint::get_width() const{
		return width_;
	}
	
	// get normal coordinate
	fltp CrossPoint::get_nc() const{
		return nc_;
	}

	// get transverse coordinate
	fltp CrossPoint::get_tc() const{
		return tc_;
	}

	// volume mesh
	ShAreaPr CrossPoint::create_area(const MeshSettings &/*stngs*/) const{
		// run validity check
		is_valid(true);

		// coordinate
		const arma::Col<fltp>::fixed<2> Rn{nc_,tc_};
		const arma::Row<arma::uword>::fixed<1> n = {0};
		const arma::Col<fltp> N{RAT_CONST(1.0),RAT_CONST(0.0)};
		const arma::Col<fltp> D{RAT_CONST(0.0),RAT_CONST(1.0)};

		// set area
		const ShAreaPr area = Area::create(Rn,N,D,n,thickness_,width_);

		// set center coord
		area->set_center_coord({nc_,tc_});

		// setup
		area->setup_perimeter();
		area->setup_corner_nodes();

		// return area
		return area;
	}

	// // surface mesh
	// ShPerimeterPr CrossPoint::create_perimeter() const{
	// 	// coordinate
	// 	arma::Col<fltp> Rn(2,0);
	// 	arma::Row<arma::uword> s(2,0);

	// 	// extract periphery and return
	// 	return Perimeter::create(Rn,s);
	// }

	// get bounding box
	arma::Mat<fltp>::fixed<2,2> CrossPoint::get_bounding() const{
		arma::Mat<fltp>::fixed<2,2> bnd;
		bnd.col(0) = arma::Col<fltp>::fixed<2>{nc_,nc_};
		bnd.col(1) = arma::Col<fltp>::fixed<2>{tc_,tc_};
		return bnd;
	}

	// validity check
	bool CrossPoint::is_valid(const bool enable_throws) const{
		if(width_<0){if(enable_throws){rat_throw_line("width must be larger than or equal tozero");} return false;};
		if(thickness_<0){if(enable_throws){rat_throw_line("thickness must be larger than or equal to zero");} return false;};
		return true;
	}


	// get type
	std::string CrossPoint::get_type(){
		return "rat::mdl::crosspoint";
	}

	// method for serialization into json
	void CrossPoint::serialize(Json::Value &js, cmn::SList &list) const{
		Cross::serialize(js,list);
		js["type"] = get_type();
		js["nc"] = nc_; js["tc"] = tc_;
		js["thickness"] = thickness_;
		js["width"] = width_;
	}

	// method for deserialisation from json
	void CrossPoint::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		Cross::deserialize(js,list,factory_list,pth);
		set_coord(js["nc"].ASFLTP(),js["tc"].ASFLTP());
		set_thickness(js["thickness"].ASFLTP());
		set_width(js["width"].ASFLTP());
	}

}}
