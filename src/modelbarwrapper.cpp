// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelbarwrapper.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// path can not be set externally
	bool ModelBarWrapper::is_custom_path() const{
		return false;
	}

	// path can not be set externally
	bool ModelBarWrapper::is_custom_cross() const{
		return false;
	}

	// splitting the modelgroup
	std::list<ShModelPr> ModelBarWrapper::split(const ShSerializerPr &slzr) const{
		const ShPathPr pth = get_input_path();
		const ShCrossPr crss = get_input_cross();
		const mat::ShConductorPr con = slzr->copy(get_input_conductor());
		const ShModelBarPr bar = ModelBar::create(pth, crss);
		bar->set_input_conductor(con);
		bar->add_transformations(get_transformations());
		bar->set_name(get_name());
		bar->set_softening(softening_);
		bar->set_magnetisation(Mf_);
		bar->set_tree_open(true);
		bar->set_circuit_index(circuit_index_);
		bar->set_operating_temperature(operating_temperature_);
		bar->set_temperature_drive(temperature_drive_);
		return {bar};
	}

	// validity check
	bool ModelBarWrapper::is_valid(const bool enable_throws)const{
		if(!ModelBar::is_valid(enable_throws))return false;
		return true;
	}

	// get type
	std::string ModelBarWrapper::get_type(){
		return "rat::mdl::modelbarwrapper";
	}

	// method for serialization into json
	void ModelBarWrapper::serialize(
		Json::Value &js, cmn::SList &list) const{

		// parent
		ModelBar::serialize(js,list);

		// set type
		js["type"] = get_type();
	}

	// method for deserialisation from json
	void ModelBarWrapper::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		ModelBar::deserialize(js,list,factory_list,pth);
	}

}}