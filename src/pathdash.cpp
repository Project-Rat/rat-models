// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathdash.hh"

// common headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathDash::PathDash(){
		set_name("Dash");
	}

	// factory
	ShPathDashPr PathDash::create(){
		return std::make_shared<PathDash>();
	}

	// setters
	void PathDash::set_num_segment(const arma::uword num_segment){
		num_segment_ = num_segment;
	}

	void PathDash::set_ell_segment(const fltp ell_segment){
		ell_segment_ = ell_segment;
	}

	void PathDash::set_combine_frame(const bool combine_frame){
		combine_frame_ = combine_frame;
	}

	void PathDash::set_ell_offset(const fltp ell_offset){
		ell_offset_ = ell_offset;
	}

	void PathDash::set_omega(const fltp omega){
		omega_ = omega;
	}

	void PathDash::set_alignment(const Align alignment){
		alignment_ = alignment;
	}

	// getters
	arma::uword PathDash::get_num_segment()const{
		return num_segment_;
	}
	
	fltp PathDash::get_ell_segment()const{
		return ell_segment_;
	}
	
	bool PathDash::get_combine_frame()const{
		return combine_frame_;
	}
	
	fltp PathDash::get_ell_offset()const{
		return ell_offset_;
	}

	fltp PathDash::get_omega()const{
		return omega_;
	}

	PathDash::Align PathDash::get_alignment()const{
		return alignment_;
	}

	// get frame
	ShFramePr PathDash::create_frame(const MeshSettings &stngs) const{
		// check if base path was set
		is_valid(true);

		// create frame
		const ShFramePr frame = input_path_->create_frame(stngs);

		// combine frame
		if(combine_frame_)frame->combine();

		// early out
		if(num_segment_==1llu && ell_segment_==RAT_CONST(0.0) && omega_==RAT_CONST(0.0) && ell_offset_==RAT_CONST(0.0))return frame;

		// copy coordinates
		const arma::field<arma::Mat<fltp> > R = frame->get_coords();
		const arma::field<arma::Mat<fltp> > L = frame->get_direction();
		const arma::field<arma::Mat<fltp> > D = frame->get_transverse();
		const arma::field<arma::Mat<fltp> > N = frame->get_normal();
		const arma::field<arma::Mat<fltp> > B = frame->get_block();

		
		// create a list of frames
		std::list<ShFramePr> frame_list;

		// walk over sections
		for(arma::uword i=0;i<R.n_elem;i++){
			// calculate length coordinate
			const arma::Row<fltp> ell = arma::join_horiz(
				arma::Row<fltp>{0},arma::cumsum(cmn::Extra::vec_norm(arma::diff(R(i),1,1))));

			// calculate number of dashes that fit onto this line
			// const arma::uword num_dash = ell.back()/pitch;
	
			// pitch for this line
			const fltp pitch = omega_==RAT_CONST(0.0) ? ell.back()/num_segment_ : omega_;

			// check number of dash 
			const arma::uword num_dash = num_segment_==0 ? arma::uword((ell.back() - ell_offset_)/pitch) + 1 : num_segment_;

			// calculate offset due to alignment
			const fltp align_offset = alignment_==Align::CENTER ? pitch/2 : (alignment_==Align::LEFT ? ell_segment_/2 : pitch - ell_segment_/2);

			// calculate segment length
			const fltp ell_segment = ell_segment_==RAT_CONST(0.0) ? ell.back()/num_dash : ell_segment_;

			// walk over dashes
			for(arma::uword j=0;j<num_dash;j++){
				// get position of dash
				const fltp ell_mid = j*pitch + align_offset + ell_offset_;
				const fltp ell1 = std::max(ell(0),ell_mid - ell_segment/2);
				const fltp ell2 = std::min(ell.back(),ell_mid + ell_segment/2);
				if(ell1>=ell2-1e-9)continue;

				// extend end
				arma::Col<fltp>::fixed<3> Rd1,Ld1,Dd1,Nd1,Bd1,Rd2,Ld2,Dd2,Nd2,Bd2;
				for(arma::uword k=0;k<3;k++){
					// first point
					Rd1(k) = cmn::Extra::interp1(ell,R(i).row(k),ell1); Ld1(k) = cmn::Extra::interp1(ell,L(i).row(k),ell1);
					Nd1(k) = cmn::Extra::interp1(ell,N(i).row(k),ell1); Dd1(k) = cmn::Extra::interp1(ell,D(i).row(k),ell1);
					Bd1(k) = cmn::Extra::interp1(ell,B(i).row(k),ell1);

					// second point
					Rd2(k) = cmn::Extra::interp1(ell,R(i).row(k),ell2); Ld2(k) = cmn::Extra::interp1(ell,L(i).row(k),ell2);
					Nd2(k) = cmn::Extra::interp1(ell,N(i).row(k),ell2); Dd2(k) = cmn::Extra::interp1(ell,D(i).row(k),ell2);
					Bd2(k) = cmn::Extra::interp1(ell,B(i).row(k),ell2);
				}

				// get coordinates
				const arma::Col<arma::uword> idx = arma::find(ell>ell1 && ell<ell2);
				const arma::Mat<fltp> Rd = arma::join_horiz(Rd1,R(i).cols(idx),Rd2);
				const arma::Mat<fltp> Ld = arma::join_horiz(Ld1,L(i).cols(idx),Ld2);
				const arma::Mat<fltp> Nd = arma::join_horiz(Nd1,N(i).cols(idx),Nd2);
				const arma::Mat<fltp> Dd = arma::join_horiz(Dd1,D(i).cols(idx),Dd2);
				const arma::Mat<fltp> Bd = arma::join_horiz(Bd1,B(i).cols(idx),Bd2);

				// check distance between points and drop

				// check handedness
				assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(Nd,Ld),Dd)>RAT_CONST(0.0)));

				// create frame
				frame_list.push_back(Frame::create(Rd,Ld,Nd,Dd,Bd));
			}
		}

		// no frames
		if(frame_list.empty())return Frame::create();

		// create a combined frame
		const ShFramePr dashed_frame = Frame::create(frame_list);

		// apply_transformations
		dashed_frame->apply_transformations(get_transformations(), stngs.time);

		// create new frame
		return dashed_frame;
	}

	// re-index nodes after deleting
	void PathDash::reindex(){
		InputPath::reindex(); Transformations::reindex();
	}

	// vallidity check
	bool PathDash::is_valid(const bool enable_throws) const{
		if(num_segment_==0 && omega_==0.0){if(enable_throws){rat_throw_line("omega and num segments can not both be zero");} return false;};
		if(!InputPath::is_valid(enable_throws))return false;
		if(!Transformations::is_valid(enable_throws))return false;
		return true;
	}

	// get type
	std::string PathDash::get_type(){
		return "rat::mdl::pathdash";
	}

	// method for serialization into json
	void PathDash::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents
		Path::serialize(js,list);
		InputPath::serialize(js,list);
		Transformations::serialize(js,list);
		
		// type
		js["type"] = get_type();

		// properties
		js["alignment"] = static_cast<int>(alignment_);
		js["num_segment"] = static_cast<int>(num_segment_);
		js["ell_segment"] = ell_segment_;
		js["combine_frame"] = combine_frame_;
		js["ell_offset"] = ell_offset_;
		js["omega"] = omega_;
	}

	// method for deserialisation from json
	void PathDash::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parents
		Path::deserialize(js,list,factory_list,pth);
		InputPath::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);

		// properties
		if(js.isMember("alignment"))set_alignment(Align(js["alignment"].asInt()));
		set_num_segment(js["num_segment"].asUInt64());
		set_ell_segment(js["ell_segment"].ASFLTP());
		set_combine_frame(js["combine_frame"].asBool());
		set_ell_offset(js["ell_offset"].ASFLTP());
		set_omega(js["omega"].ASFLTP());
	}


}}