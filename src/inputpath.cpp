// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "inputpath.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	InputPath::InputPath(const ShPathPr &input_path){
		set_input_path(input_path);
	}

	// set path
	void InputPath::set_input_path(const ShPathPr &input_path){
		input_path_ = input_path;
	}

	// set path
	ShPathPr InputPath::get_input_path() const{
		return input_path_;
	}

	// is custom coil
	bool InputPath::is_custom_path() const{
		return true;
	}

	

	// is valid
	bool InputPath::is_valid(const bool enable_throws) const{
		if(is_custom_path()){
			if(input_path_==NULL){if(enable_throws){rat_throw_line("input path points to NULL");} return false;};
			if(!input_path_->is_valid(enable_throws))return false;
		}
		return true;
	}

	// get type
	std::string InputPath::get_type(){
		return "rat::mdl::inputpath";
	}

	// method for serialization into json
	void InputPath::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		Node::serialize(js,list);

		// type
		js["type"] = get_type();
		
		// serialize path
		js["input_path"] = cmn::Node::serialize_node(input_path_, list);
	}

	// method for deserialisation from json
	void InputPath::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent objects
		Node::deserialize(js,list,factory_list,pth);

		// deserialize path
		set_input_path(cmn::Node::deserialize_node<Path>(js["input_path"], list, factory_list, pth));
	}


}}
