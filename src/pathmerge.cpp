// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathmerge.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathMerge::PathMerge(){
		set_name("Merge");
	}

	// factory
	ShPathMergePr PathMerge::create(){
		return std::make_shared<PathMerge>();
	}


	// setters
	void PathMerge::set_use_first(const bool use_first){
		use_first_ = use_first;
	}

	void PathMerge::set_use_second(const bool use_second){
		use_second_ = use_second;
	}

	// getters
	bool PathMerge::get_use_first()const{
		return use_first_;
	}

	bool PathMerge::get_use_second()const{
		return use_second_;
	}

	// retreive model at index
	ShPathPr PathMerge::get_path(const arma::uword index) const{
		auto it = input_paths_.find(index);
		if(it==input_paths_.end())rat_throw_line("Index does not exist.");
		return (*it).second;
	}

	// delete model at index
	bool PathMerge::delete_path(const arma::uword index){	
		auto it = input_paths_.find(index);
		if(it==input_paths_.end())return false;
		(*it).second = NULL; return true;
	}

	// re-index nodes after deleting
	void PathMerge::reindex(){
		Transformations::reindex();
		std::map<arma::uword, ShPathPr> new_paths; arma::uword idx = 1;
		for(auto it=input_paths_.begin();it!=input_paths_.end();it++)
			if((*it).second!=NULL)new_paths.insert({idx++, (*it).second});
		input_paths_ = new_paths;
	}

	// function for adding a path to the path list
	arma::uword PathMerge::add_path(const ShPathPr &path){
		// check input
		assert(path!=NULL); 

		// get counters
		const arma::uword index = input_paths_.size()+1;

		// insert
		input_paths_.insert({index, path});

		// return index
		return index;
	}

	// get number of paths
	arma::uword PathMerge::get_num_paths() const{
		return input_paths_.size();
	}

	// update function
	ShFramePr PathMerge::create_frame(const MeshSettings &stngs) const{
		// check validty
		if(!is_valid())rat_throw_line("merge path is not valid");

		// get coordinates
		const ShFramePr coord_frame = input_paths_.at(1)->create_frame(stngs);
		const ShFramePr orientation_frame = input_paths_.at(2)->create_frame(stngs);

		// allow using only first or only second
		if(use_first_ && !use_second_)return coord_frame;
		if(use_second_ && !use_first_)return orientation_frame;
		
		// get coordinates and orientation
		const arma::field<arma::Mat<fltp> >& R = coord_frame->get_coords();
		const arma::field<arma::Mat<fltp> >& L = orientation_frame->get_direction();
		const arma::field<arma::Mat<fltp> >& N = orientation_frame->get_normal();
		const arma::field<arma::Mat<fltp> >& D = orientation_frame->get_transverse();
		const arma::field<arma::Mat<fltp> >& B = orientation_frame->get_block();

		// get source information
		const arma::Row<arma::uword>& section = coord_frame->get_section();
		const arma::Row<arma::uword>& turn = coord_frame->get_turn();
		const arma::uword num_section_base = coord_frame->get_num_section_base();

		// check number of sections
		if(R.n_elem!=L.n_elem)rat_throw_line("number of sections do not match");

		// check lengths
		for(arma::uword i=0;i<R.n_elem;i++){
			if(R(i).n_cols!=L(i).n_cols)rat_throw_line("frames to be merged do not match");
			if(R(i).n_cols!=N(i).n_cols)rat_throw_line("frames to be merged do not match");
			if(R(i).n_cols!=D(i).n_cols)rat_throw_line("frames to be merged do not match");
		}

		// combine frame
		const ShFramePr frame = Frame::create(R,L,N,D,B);

		// conserve location
		frame->set_location(section,turn,num_section_base);

		// apply_transformations
		frame->apply_transformations(get_transformations(), stngs.time);

		// create new frame
		return frame;
	}

	// validity check
	bool PathMerge::is_valid(const bool enable_throws)const{
		if(input_paths_.size()!=2){if(enable_throws){rat_throw_line("need exactly two input paths");} return false;};
		if(use_first_ && !use_second_)if(!input_paths_.at(1)->is_valid())return false;
		if(use_second_ && !use_first_)if(!input_paths_.at(2)->is_valid())return false;
		if(use_second_ == use_first_){
			if(!input_paths_.at(1)->is_valid())return false;
			if(!input_paths_.at(2)->is_valid())return false;
		}
		if(!Transformations::is_valid(enable_throws))return false;
		return true;
	}

	// get type
	std::string PathMerge::get_type(){
		return "rat::mdl::pathmerge";
	}

	// method for serialization into json
	void PathMerge::serialize(Json::Value &js, cmn::SList &list) const{

		// parent objects
		Path::serialize(js,list);
		Transformations::serialize(js,list);

		// type
		js["type"] = get_type();

		// settings
		js["use_first"] = use_first_;
		js["use_second"] = use_second_;

		// serialize the models
		for(auto it = input_paths_.begin();it!=input_paths_.end();it++)
			js["input_paths"].append(cmn::Node::serialize_node((*it).second, list));
	}

	// method for deserialisation from json
	void PathMerge::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent objects
		Path::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);

		// settings
		use_first_ = js["use_first"].asBool();
		use_second_ = js["use_second"].asBool();

		// deserialize the models
		for(auto it = js["input_paths"].begin();it!=js["input_paths"].end();it++)
			add_path(cmn::Node::deserialize_node<Path>(
				(*it), list, factory_list, pth));
	}

}}