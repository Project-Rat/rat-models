// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "linedata.hh"

#include "vtkunstr.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	LineData::LineData(){
		set_output_type("line");
	}

	// constructor
	LineData::LineData(const ShFramePr &frame) : LineData(){
		set_frame(frame);
	}

	// factory
	ShLineDataPr LineData::create(){
		return std::make_shared<LineData>();
	}

	// factory
	ShLineDataPr LineData::create(const ShFramePr &frame){
		return std::make_shared<LineData>(frame);
	}


	// set path
	void LineData::set_frame(const ShFramePr &frame){
		if(frame==NULL)rat_throw_line("supplied frame points to NULL");
		frame_ = frame;
	}

	// calculate field on line
	void LineData::setup_targets(){
		// combine sections
		frame_->combine();

		// get line properties
		Rt_ = frame_->get_coords(0);
		L_ = frame_->get_direction(0);
		N_ = frame_->get_normal(0);
		D_ = frame_->get_transverse(0);

		// calculate length
		const arma::Row<fltp> dl = arma::sqrt(arma::sum(arma::pow(arma::diff(Rt_,1,1),2),0));
		ell_ = arma::join_horiz(arma::Row<fltp>{0},arma::cumsum(dl));

		// get number of target points
		num_targets_ = Rt_.n_cols;
		assert(num_targets_>0);
	}

	// get position
	arma::Row<fltp> LineData::get_ell()const{
		return ell_;
	}

	// get coordinates for section
	arma::Mat<fltp> LineData::get_direction() const{
		return L_;
	}

	// get coordinates for section
	arma::Mat<fltp> LineData::get_normal() const{
		return N_;
	}

	// get coordinates for section
	arma::Mat<fltp> LineData::get_transverse() const{
		return D_;
	}

	// export points/lines to vtk file
	ShVTKObjPr LineData::export_vtk() const{
		// check if calculation was done
		if(!has_field())rat_throw_line("field is not calculated");

		// counters
		const arma::uword num_ell = Rt_.n_cols;

		// create unstructured grid
		const ShVTKUnstrPr vtk_data = VTKUnstr::create();

		// write node coordinates
		vtk_data->set_nodes(Rt_);

		// create line elements
		arma::Mat<arma::uword> n(2,num_ell-1);
		for(arma::uword i=0;i<num_ell-1;i++)
			n.col(i) = arma::Col<arma::uword>::fixed<2>{i,i+1};
		const arma::uword line_type = 3;
		vtk_data->set_elements(n,line_type);

		// magnetic vector potential
		if(has('A') && has_field())vtk_data->set_nodedata(get_field('A'),"Vector Potential [Vs/m]");

		// magnetic flux density
		if(has('B') && has_field())vtk_data->set_nodedata(get_field('B'),"Mgn. Flux Density [T]");
	
		// magnetic field
		if(has('H') && has_field())vtk_data->set_nodedata(get_field('H'),"Magnetic Field [A/m]");

		// magnetisation
		if(has('M') && has_field())vtk_data->set_nodedata(get_field('M'),"Magnetisation [A/m]");

		// return data object
		return vtk_data;
	}

	// export points/lines to vtk file
	ShVTKTablePr LineData::export_vtk_table() const{
		// check if calculation was done
		if(!has_field())rat_throw_line("field is not calculated");

		// create unstructured grid
		const ShVTKTablePr vtk_data = VTKTable::create(Rt_.n_cols);

		// coordinates
		vtk_data->set_data(Rt_.row(0).t(),"x [m]");
		vtk_data->set_data(Rt_.row(1).t(),"y [m]");
		vtk_data->set_data(Rt_.row(2).t(),"z [m]");

		// magnetic vector potential
		if(has('A') && has_field()){
			const arma::Mat<fltp> A = get_field('A');
			vtk_data->set_data(A.row(0).t(),"Ax [Vs/m]");
			vtk_data->set_data(A.row(1).t(),"Ay [Vs/m]");
			vtk_data->set_data(A.row(2).t(),"Az [Vs/m]");
		}

		// magnetic field
		if(has('H') && has_field()){
			// magnetic field
			const arma::Mat<fltp> H = get_field('H');
			vtk_data->set_data(H.row(0).t(),"Hx [A/m]");
			vtk_data->set_data(H.row(1).t(),"Hy [A/m]");
			vtk_data->set_data(H.row(2).t(),"Hz [A/m]");
		}

		// magnetic flux density
		if(has('B') && has_field()){
			// magnetic flux density
			const arma::Mat<fltp> B = get_field('B');
			vtk_data->set_data(B.row(0).t(),"Bx [T]");
			vtk_data->set_data(B.row(1).t(),"By [T]");
			vtk_data->set_data(B.row(2).t(),"Bz [T]");
		}

		// magnetisation
		if(has('M') && has_field()){
			// magnetic flux density
			const arma::Mat<fltp> M = get_field('M');
			vtk_data->set_data(M.row(0).t(),"Mx [A/m]");
			vtk_data->set_data(M.row(1).t(),"My [A/m]");
			vtk_data->set_data(M.row(2).t(),"Mz [A/m]");
		}

		// return data object
		return vtk_data;
	}



	// // export a vtk with times
	// void LineData::write(cmn::ShLogPr lg){
	// 	// check if data directory set
	// 	if(output_dir_.empty())return;

	// 	// header
	// 	lg->msg(2,"%s%sWRITING OUTPUT FILES%s\n",KBLD,KGRN,KNRM);
		
	// 	// check output directory
	// 	if(output_times_.is_empty())rat_throw_line("output times are not set");

	// 	// create output directory
	// 	boost::filesystem::create_directory(output_dir_);

	// 	// output filename
	// 	std::string fname  = output_fname_;

	// 	// report
	// 	lg->msg(2,"%s%sVISUALISATION TOOLKIT%s\n",KBLD,KGRN,KNRM);

	// 	// settings report
	// 	display_settings(lg);

	// 	// report
	// 	lg->msg(2, "%swriting linedata%s\n",KBLU,KNRM);
	// 	lg->msg("%s%4s %8s %s%s\n",KBLD,"id","time","filename",KNRM);

	// 	// walk over timesteps
	// 	for(arma::uword i=0;i<output_times_.n_elem;i++){
	// 		// set time
	// 		set_time(output_times_(i));

	// 		// get data at this time
	// 		ShVTKUnstrPr vtk_line = export_vtk_coord();

	// 		// extend filename with index
	// 		if(output_times_.n_elem!=1)fname = indexed_output_fname(output_fname_,i);

	// 		// show in log
	// 		lg->msg("%04llu %8.2e %s\n",i,get_time(),fname.c_str());

	// 		// write data to file
	// 		vtk_line->write(output_dir_/(fname + "_ln"));
	// 	}

	// 	// return
	// 	lg->msg(-6,"\n"); 
	// }

	// // get type
	// std::string LineData::get_type(){
	// 	return "rat::mdl::calcline";
	// }

	// // method for serialization into json
	// void LineData::serialize(Json::Value &js, cmn::SList &list) const{
	// 	// serialize fieldmap
	// 	CalcFieldMap::serialize(js,list);

	// 	// properties
	// 	js["type"] = get_type();

	// 	// subnodes
	// 	js["base"] = cmn::Node::serialize_node(base_, list);
		
	// }

	// // method for deserialisation from json
	// void LineData::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
	// 	// serialize fieldmap
	// 	CalcFieldMap::deserialize(js,list,factory_list,pth);

	// 	// subnodes
	// 	base_ = cmn::Node::deserialize_node<Path>(js["base"], list, factory_list, pth);
	// }


}}
