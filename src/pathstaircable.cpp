// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathstaircable.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathStairCable::PathStairCable(){
		set_name("Staircase Cable");
	}

	// constructor with input of base path
	PathStairCable::PathStairCable(const rat::mdl::ShPathPr &base) : PathStairCable(){
		set_input_path(base);
	}

	// factory methods
	ShPathStairCablePr PathStairCable::create(){
		return std::make_shared<PathStairCable>();
	}

	// factory with input of base path
	ShPathStairCablePr PathStairCable::create(const rat::mdl::ShPathPr &base){
		return std::make_shared<PathStairCable>(base);
	}

	// setters
	void PathStairCable::set_pitch(const rat::fltp pitch){
		pitch_ = pitch;
	}

	void PathStairCable::set_spacing(const rat::fltp spacing){
		spacing_ = spacing;
	}

	void PathStairCable::set_num_strands(const arma::uword num_strands){
		num_strands_ = num_strands;
	}

	void PathStairCable::set_flatten_cable(const bool flatten_cable){
		flatten_cable_ = flatten_cable;
	}

	// getters
	rat::fltp PathStairCable::get_pitch()const{
		return pitch_;
	}

	rat::fltp PathStairCable::get_spacing()const{
		return spacing_;
	}

	arma::uword PathStairCable::get_num_strands()const{
		return num_strands_;
	}

	bool PathStairCable::get_flatten_cable()const{
		return flatten_cable_;
	}

	// update function
	rat::mdl::ShFramePr PathStairCable::create_frame(const rat::mdl::MeshSettings& stngs) const{
		// check validity
		is_valid(true);
		
		// get frame from input path
		const rat::mdl::ShFramePr input_frame = input_path_->create_frame(stngs);

		// get frame info
		const arma::field<arma::Mat<rat::fltp> >& R = input_frame->get_coords();
		const arma::field<arma::Mat<rat::fltp> >& L = input_frame->get_direction();
		const arma::field<arma::Mat<rat::fltp> >& N = input_frame->get_normal();
		const arma::field<arma::Mat<rat::fltp> >& D = input_frame->get_transverse();
		const arma::field<arma::Mat<rat::fltp> >& B = input_frame->get_block();

		// number of sections
		const arma::uword num_sections = input_frame->get_num_sections();

		// create list of strands
		std::list<rat::mdl::ShFramePr> frame_list;

		// calculate omega
		const rat::fltp alpha = std::atan(num_strands_*spacing_/pitch_);
		const rat::fltp ell_pitch = std::cos(alpha)*pitch_; // pitch along ell
		const rat::fltp thickness = std::sin(alpha)*pitch_;
		const rat::fltp slope = thickness/ell_pitch;

		// walk over sections
		rat::fltp section_shift = RAT_CONST(0.0);
		for(arma::uword i=0;i<num_sections;i++){

			// calculate longitudinal position of nodes
			const arma::Row<rat::fltp> ell = 
				arma::join_horiz(arma::Row<rat::fltp>{section_shift}, 
				section_shift + arma::cumsum(rat::cmn::Extra::vec_norm(arma::diff(R(i),1,1))));

			// walk over strands
			for(arma::uword j=0;j<num_strands_;j++){
				// offset for this strand
				const rat::fltp offset = j*thickness/num_strands_;

				// create normal at nodes
				arma::Row<rat::fltp> n = offset + slope*ell;
				if(flatten_cable_)n = spacing_*arma::floor(n/(thickness/num_strands_) + 1e-9);

				// take modulus with cable thickness
				const arma::Row<rat::fltp> noff = arma::floor(n/thickness);
				n = n - noff*thickness;

				// find sections this is where the tape with this index starts and ends
				// this can be found from discontinuity in normal coordinate
				// idx is determined at nodes
				arma::Col<arma::uword> idx = arma::join_vert(
					arma::Col<arma::uword>{0},arma::find(
					n.head_cols(n.n_cols-1)/thickness>
					n.tail_cols(n.n_cols-1)/thickness)+1,
					arma::Col<arma::uword>{n.n_cols});

				// get unique to avoid sections with zero length
				idx = arma::unique(idx);

				// walk over strand parts
				for(arma::uword k=0;k<idx.n_rows-1;k++){
					// get indices
					arma::uword idx1 = idx(k);
					arma::uword idx2 = idx(k+1)-1;
					
					// snap to nearest node
					if(stngs.num_sub!=0){
						idx1 = stngs.num_sub*arma::uword(std::ceil(rat::fltp(idx1)/stngs.num_sub));
						idx2 = stngs.num_sub*arma::uword(std::floor(rat::fltp(idx2)/stngs.num_sub));
					}

					// check
					if(idx1>=idx2)continue;

					// // snap to nearest node
					// std::cout<<idx1<<" "<<idx2<<" "<<ell.n_cols<<std::endl;

					// get section
					arma::Row<rat::fltp> ns = n.cols(idx1,idx2);
					arma::Row<rat::fltp> ls = ell.cols(idx1,idx2);

					// check simplification
					if(extend_ends_){
						// add extra node at start and end 
						// const rat::fltp ell1 = std::max(ell.front(), rat::cmn::Extra::interp1(ns,ls,RAT_CONST(0.0),"linear",true));
						// const rat::fltp ell2 = std::min(ell.back(), rat::cmn::Extra::interp1(ns,ls,thickness,"linear",true));

						const rat::fltp ell1 = std::max(ell.front(), (noff(idx1)*thickness - offset)/slope);
						const rat::fltp ell2 = std::min(ell.back(), (thickness + noff(idx2)*thickness - offset)/slope);

						// const rat::fltp n1 = rat::cmn::Extra::interp1(ls,ns,ell1,"linear",true);
						// const rat::fltp n2 = rat::cmn::Extra::interp1(ls,ns,ell2,"linear",true);

						const rat::fltp n1 = slope*ell1 + offset - noff(idx1)*thickness;
						const rat::fltp n2 = slope*ell2 + offset - noff(idx2)*thickness;

						// attach
						ls = arma::join_horiz(arma::Row<rat::fltp>{ell1}, ls, arma::Row<rat::fltp>{ell2});
						ns = arma::join_horiz(arma::Row<rat::fltp>{n1}, ns, arma::Row<rat::fltp>{n2});

						// remove duplicates
						const arma::Col<arma::uword> id = arma::find_unique(ls);
						ls = ls.cols(id); ns = ns.cols(id);
					}

					// interpolate other variables
					arma::Mat<rat::fltp> Rs(3,ls.n_elem), Ls(3,ls.n_elem), 
						Ns(3,ls.n_elem), Ds(3,ls.n_elem), Bs(3,ls.n_elem);

					// walk over xyz
					for(arma::uword m=0;m<3;m++){
						// interpolate
						if(ell.n_elem!=R(i).n_cols)rat_throw_line("interpolation arrays have different length");
						arma::Col<rat::fltp> Rsc, Lsc, Nsc, Dsc, Bsc;
						rat::cmn::Extra::interp1(ell.t(), R(i).row(m).t(), ls.t(), Rsc, "linear", true);
						rat::cmn::Extra::interp1(ell.t(), L(i).row(m).t(), ls.t(), Lsc, "linear", true);
						rat::cmn::Extra::interp1(ell.t(), N(i).row(m).t(), ls.t(), Nsc, "linear", true);
						rat::cmn::Extra::interp1(ell.t(), D(i).row(m).t(), ls.t(), Dsc, "linear", true);
						rat::cmn::Extra::interp1(ell.t(), B(i).row(m).t(), ls.t(), Bsc, "linear", true);

						// insert component
						Rs.row(m) = Rsc.t(); Ls.row(m) = Lsc.t(); Ns.row(m) = Nsc.t();
						Ds.row(m) = Dsc.t(); Bs.row(m) = Bsc.t();
					}

					// create offset
					const arma::Mat<rat::fltp> Rf = Rs + Ns.each_row()%ns;

					// // rotate vectors
					// for(arma::uword m=0;m<Rs.n_cols;m++){
					// 	// create rotation matrix around transverse vector
					// 	const arma::Mat<rat::fltp>::fixed<3,3> Mrot = 
					// 		rat::cmn::Extra::create_rotation_matrix(Ds.col(m), -alpha);

					// 	// perform rotation
					// 	Ls.col(m) = Mrot*Ls.col(m); Ns.col(m) = Mrot*Ns.col(m);
					// }

					// check handedness
					assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(Ns,Ls),Ds)>RAT_CONST(0.0)));
		
					// create frame
					frame_list.push_back(rat::mdl::Frame::create(Rf,Ls,Ns,Ds,Bs));

					// set section

				}
			}

			// update section shift
			section_shift = ell.back();
		}

		if(frame_list.empty())rat_throw_line("no frames found");

		// combine frames
		const rat::mdl::ShFramePr frame = 
			rat::mdl::Frame::create(frame_list);

		// apply transformations
		frame->apply_transformations(get_transformations(),stngs.time);

		// return the new frame
		return frame;
	}

	// validity check
	bool PathStairCable::is_valid(const bool enable_throws)const{
		if(!rat::mdl::InputPath::is_valid(enable_throws))return false;
		if(!rat::mdl::Transformations::is_valid(enable_throws))return false;
		if(num_strands_<=0){if(enable_throws){rat_throw_line("number of strands must be larger than zero");} return false;};
		if(pitch_<=0){if(enable_throws){rat_throw_line("pitch must be larger than zero");} return false;};
		if(spacing_<=0){if(enable_throws){rat_throw_line("spacing must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string PathStairCable::get_type(){
		return "rat::mdl::pathstaircable";
	}

	// method for serialization into json
	void PathStairCable::serialize(Json::Value &js, rat::cmn::SList &list) const{
		// serialize parents first 
		// this allows the type to be overridden
		rat::mdl::InputPath::serialize(js,list);
		rat::mdl::Transformations::serialize(js,list);

		js["type"] = get_type();

		js["pitch"] = pitch_;
		js["spacing"] = spacing_;
		js["num_strands"] = static_cast<int>(num_strands_);
		js["extend_ends"] = extend_ends_;
		js["flatten_cable"] = flatten_cable_;
	}

	// method for deserialisation from json
	void PathStairCable::deserialize(
		const Json::Value &js, rat::cmn::DSList &list, const 
			rat::cmn::NodeFactoryMap &factory_list, 
			const boost::filesystem::path &pth){
		
		// parent objects
		rat::mdl::InputPath::deserialize(js,list,factory_list,pth);
		rat::mdl::Transformations::deserialize(js,list,factory_list,pth);

		// subnodes
		pitch_ = js["pitch"].ASFLTP();
		spacing_ = js["spacing"].ASFLTP();
		num_strands_ = js["num_strands"].asUInt64();
		extend_ends_ = js["extend_ends"].asBool();
		flatten_cable_ = js["flatten_cable"].asBool();
	}

}}


