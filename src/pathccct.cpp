// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathccct.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// function that computes the phi values
	arma::Row<rat::fltp> PathCCCT::phi(const arma::Row<rat::fltp> &theta) const{
		arma::Col<rat::fltp> n_vector = arma::linspace<arma::Col<rat::fltp>>(1, Jn_.n_elem, Jn_.n_elem);
		arma::Mat<rat::fltp> n_theta_matrix = n_vector * theta;
		arma::Col<rat::fltp> sum = arma::sin(n_theta_matrix.t()) * (Jn_.t() / n_vector);

		arma::Row<rat::fltp> phases(theta.n_elem);
		if(phi0_) for(unsigned int i=0; i<phases.n_elem; ++i) phases(i) = phi0_.integral(0.0,theta(i));
		else phases = main_phi0_*theta;

		arma::Row<rat::fltp> phi_val = current_sign_ * (d2_ * R_in_ * main_phi0_ / (nc_ * I0_)) * sum.t() + hand_ * phases / (2 * arma::Datum<fltp>::pi);

		return phi_val;
	}

	// function that computes the phi derivative values
	arma::Row<rat::fltp> PathCCCT::dphi(const arma::Row<rat::fltp> &theta) const{
		arma::Col<rat::fltp> n_vector = arma::linspace<arma::Col<rat::fltp>>(1, Jn_.n_elem, Jn_.n_elem);
		arma::Mat<rat::fltp> n_theta_matrix = n_vector * theta;
		arma::Col<rat::fltp> sum = arma::cos(n_theta_matrix.t()) * Jn_.t();

		arma::Row<rat::fltp> phi0_vec(theta.n_elem);
		for(unsigned int i=0; i<theta.n_elem; ++i)
		{
			if(phi0_) phi0_vec(i) = phi0_(theta(i));
			else phi0_vec(i) = main_phi0_;
		}

		arma::Row<rat::fltp> dphi_val = current_sign_ * (d2_ * R_in_ * main_phi0_ / (nc_ * I0_)) * sum.t() + hand_ * phi0_vec / (2 * arma::Datum<fltp>::pi);
		
		return dphi_val;
	}

	// function that computes the length of the tangent to the path at given theta
	rat::fltp PathCCCT::tangent_length(rat::fltp theta) const{
		const arma::Row<rat::fltp> theta_vect{theta};
		const rat::fltp dphi_val = dphi(theta_vect)(0);
		return sqrt(pow(R_in_, 2) + pow(rho_ + R_in_ * cos(theta), 2) * pow(dphi_val, 2));
	}

	// setup coordinates and orientation vectors
	ShFramePr PathCCCT::create_frame(const MeshSettings &) const{
		std::vector<rat::fltp> t;
		t.reserve(1000);
		rat::fltp t_curr = 0.0;
			
		while (t_curr < 2 * arma::Datum<fltp>::pi){
			t.push_back(t_curr);
			t_curr += step_size_ / tangent_length(t_curr);
		}
			
		const arma::uword n_turns_int = (arma::uword)n_turns_;
		arma::Row<rat::fltp> theta((int)(n_turns_ * t.size()));
		theta.fill(0.0);
		for (arma::uword n = 0; n < n_turns_int; n++){
			for (arma::uword i = 0; i < t.size(); i++){
				theta(i + n * t.size()) = t[i] + n * 2 * arma::Datum<fltp>::pi;
			}
		}

		// in the for below I changed ++i to i++. E.g. N_turns=10.5, t.size()=100, the last
		// element filled by above nested fors is 999, so below for should fill element 1000 first
		// so i must be 0 at the start of the cycle but if I am not mistaken ++i would start by 1
		for(arma::uword i=0; i+n_turns_int*t.size()<theta.n_elem; i++){
			theta(i+n_turns_int*t.size()) = t[i] + n_turns_int*2*arma::Datum<fltp>::pi;
		}

		// define coil winding path
		arma::Mat<rat::fltp> R, L, D;

		const arma::Row<rat::fltp> phi_vector = phi(theta);
		const arma::Row<rat::fltp> dphi_vector = dphi(theta);

		// position vector
		arma::Row<rat::fltp> X = (rho_ + R_in_ * arma::cos(theta)) % arma::cos(phi_vector);
		arma::Row<rat::fltp> Y = (rho_ + R_in_ * arma::cos(theta)) % arma::sin(phi_vector);
		arma::Row<rat::fltp> Z = R_in_ * arma::sin(theta);
		R = arma::join_vert(X, Y, Z);

		// direction vector
		arma::Row<rat::fltp> dX = -R_in_ * arma::sin(theta) % arma::cos(phi_vector) -
			(rho_ + R_in_ * arma::cos(theta)) % arma::sin(phi_vector) %
			dphi_vector;
		arma::Row<rat::fltp> dY = -R_in_ * arma::sin(theta) % arma::sin(phi_vector) +
			(rho_ + R_in_ * arma::cos(theta)) % arma::cos(phi_vector) %
			dphi_vector;
		arma::Row<rat::fltp> dZ = R_in_ * arma::cos(theta);
		L = arma::join_vert(dX, dY, dZ);
		L.each_row() /= rat::cmn::Extra::vec_norm(L);

		// radial vector
		arma::Row<rat::fltp> DX = arma::cos(theta) % arma::cos(phi_vector);
		arma::Row<rat::fltp> DY = arma::cos(theta) % arma::sin(phi_vector);
		arma::Row<rat::fltp> DZ = arma::sin(theta);
		D = arma::join_vert(DX, DY, DZ);
		D.each_row() /= rat::cmn::Extra::vec_norm(D);

		// check orthogonality
		if (arma::any(rat::cmn::Extra::dot(D, L) > 1e-6))
			rat_throw_line("D and L vectors are not orthogonal");

		// calculate normal vector
		const arma::Mat<fltp> N = cmn::Extra::cross(L, D);

		// check handedness
		assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N,L),D)>RAT_CONST(0.0)));
		
		// create frame
		const ShFramePr gen = Frame::create(R, L, N, D, 1);

		// transformations
		gen->apply_transformations(get_transformations(), stngs.time);

		// create frame
		return gen;
	}

}}
