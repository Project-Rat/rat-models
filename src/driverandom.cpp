// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "driverandom.hh"

// common headers
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	DriveRandom::DriveRandom(){
		set_name("Random");
	}

	// constructor
	DriveRandom::DriveRandom(
		const fltp ell, 
		const fltp amplitude, 
		const fltp offset,
		const arma::uword seed) : DriveRandom(){
		set_ell(ell);
		set_amplitude(amplitude);
		set_offset(offset);
		set_seed(seed);
	}

	// factory
	ShDriveRandomPr DriveRandom::create(){
		return std::make_shared<DriveRandom>();
	}

	// factory
	ShDriveRandomPr DriveRandom::create(
		const fltp ell, 
		const fltp amplitude, 
		const fltp offset,
		const arma::uword seed){
		return std::make_shared<DriveRandom>(ell,amplitude,offset,seed);
	}


	// setters
	void DriveRandom::set_ell(const fltp ell){
		ell_ = ell;
	}

	void DriveRandom::set_amplitude(const fltp amplitude){
		amplitude_ = amplitude;
	}

	void DriveRandom::set_offset(const fltp offset){
		offset_ = offset;
	}

	void DriveRandom::set_seed(const arma::uword seed){
		seed_ = seed;
	}


	// getters
	fltp DriveRandom::get_ell()const{
		return ell_;
	}

	fltp DriveRandom::get_amplitude()const{
		return amplitude_;
	}

	fltp DriveRandom::get_offset()const{
		return offset_;
	}

	arma::uword DriveRandom::get_seed()const{
		return seed_;
	}

	// get current
	fltp DriveRandom::get_scaling(
		const fltp position,
		const fltp /*time*/,
		const arma::uword derivative) const{

		// find nearest discrete position
		const arma::sword discrete_position1 = static_cast<arma::sword>(std::floor(position/ell_));
		arma::sword discrete_position2 = static_cast<arma::sword>(std::ceil(position/ell_));
		
		// catch case where the position is an integer of ell
		if(discrete_position1==discrete_position2)discrete_position2++;

		// Hash the discretized position to get a "random" seed
		std::hash<arma::sword> hasher;
		const arma::uword hash1 = hasher(discrete_position1);
		const arma::uword hash2 = hasher(discrete_position2);

		// Use the hash value as a seed for the PRNG
		std::mt19937 generator1(static_cast<unsigned int>(seed_ + hash1));
		std::mt19937 generator2(static_cast<unsigned int>(seed_ + hash2));
		std::uniform_real_distribution<fltp> distribution(-1.0, 1.0);  // Adjust range as needed

		// create the two neighbouring values
		const fltp value1 = amplitude_*distribution(generator1);
		const fltp value2 = amplitude_*distribution(generator2);

		// interpolate
		const fltp slope = (value2 - value1)/((discrete_position2 - discrete_position1)*ell_);

		if(derivative==0)return offset_ + value1 + (position - discrete_position1*ell_)*slope;
		else if(derivative==1)return slope;
		else return RAT_CONST(0.0);
	}

	
	// apply scaling for the input settings
	void DriveRandom::rescale(const fltp scale_factor){
		amplitude_ *= scale_factor;
	}

	// validity check
	bool DriveRandom::is_valid(const bool enable_throws) const{
		if(ell_<=0){if(enable_throws){rat_throw_line("characteristic length must be positive");} return false;};
		return true;
	}


	// get type
	std::string DriveRandom::get_type(){
		return "rat::mdl::driverandom";
	}

	// method for serialization into json
	void DriveRandom::serialize(Json::Value &js, cmn::SList &list) const{
		Drive::serialize(js,list);
		js["type"] = get_type();
		js["ell"] = ell_;
		js["amplitude"] = amplitude_;
		js["offset"] = offset_;
		js["seed"] = static_cast<int>(seed_);
	}

	// method for deserialisation from json
	void DriveRandom::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		Drive::deserialize(js,list,factory_list,pth);
		set_ell(js["ell"].ASFLTP());
		set_amplitude(js["amplitude"].ASFLTP());
		set_offset(js["offset"].ASFLTP());
		set_seed(js["seed"].asUInt64());
	}

}}