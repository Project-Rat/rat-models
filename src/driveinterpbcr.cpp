// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "driveinterpbcr.hh"

// boost headers
#include <boost/math/interpolators/barycentric_rational.hpp>


// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	DriveInterpBCR::DriveInterpBCR(){
		set_name("Smooth Interpolation");
	}

	// constructor
	DriveInterpBCR::DriveInterpBCR(
		const arma::Col<fltp> &ti, 
		const arma::Col<fltp> &vi, 
		const arma::uword order) : DriveInterpBCR(){
		set_interpolation_table(ti,vi);
		set_order(order);
	}

	// factory
	ShDriveInterpBCRPr DriveInterpBCR::create(){
		return std::make_shared<DriveInterpBCR>();
	}

	// factory
	ShDriveInterpBCRPr DriveInterpBCR::create(
		const arma::Col<fltp> &ti, 
		const arma::Col<fltp> &vi, 
		const arma::uword order){
		return std::make_shared<DriveInterpBCR>(ti,vi,order);
	}


	// set and get current
	void DriveInterpBCR::set_interpolation_table(const arma::Col<fltp> &ti, const arma::Col<fltp> &vi){
		if(ti.n_elem!=vi.n_elem)rat_throw_line("number of elements must be the same");
		ti_ = ti; vi_ = vi;
	}

	// get times
	void DriveInterpBCR::set_interpolation_times(const arma::Col<fltp> &ti){
		ti_ = ti;
	}

	// get values
	void DriveInterpBCR::set_interpolation_values(const arma::Col<fltp> &vi){
		vi_ = vi;
	}

	// set order
	void DriveInterpBCR::set_order(const arma::uword order){
		order_ = order;
	}

	// get times
	const arma::Col<fltp>& DriveInterpBCR::get_interpolation_times()const{
		return ti_;
	}

	// get values
	const arma::Col<fltp>& DriveInterpBCR::get_interpolation_values()const{
		return vi_;
	}

	// get order
	arma::uword DriveInterpBCR::get_order()const{
		return order_;
	}

	// get number of times
	arma::uword DriveInterpBCR::get_num_times()const{
		return ti_.n_elem;
	}

	// boundaries for making a plot
	fltp DriveInterpBCR::get_v1() const{
		return ti_.front() - RAT_CONST(0.1)*(ti_.back() - ti_.front());
	}
	
	fltp DriveInterpBCR::get_v2() const{
		return ti_.back() + RAT_CONST(0.1)*(ti_.back() - ti_.front());
	}
	

	// get drive scaling
	fltp DriveInterpBCR::get_scaling(
		const fltp position,
		const fltp time,
		const arma::uword derivative) const{

		// check order
		if(ti_.n_elem<=order_)rat_throw_line("number of times must be larger than order: " + std::to_string(ti_.n_elem) + "/" + std::to_string(order_));

		// use boost interpolators
		boost::math::interpolators::barycentric_rational<fltp> fun(ti_.memptr(), vi_.memptr(), ti_.n_elem, order_);
		if(derivative==0)return fun(position);
		if(derivative==1)return fun.prime(position);
		
		// finite difference to get higher order derivatives
		const fltp delta_position = RAT_CONST(1e-4)*(ti_.max() - ti_.min());
		return (get_scaling(position+delta_position/2,time,derivative-1) - get_scaling(position-delta_position/2,time,derivative-1))/delta_position;
	}

	// get scaling for arrays
	// this way the barycentric interpolation object is only created once
	arma::Row<fltp> DriveInterpBCR::get_scaling_vec(
		const arma::Row<fltp> &positions, const fltp time, 
		const arma::uword derivative) const{

		// check order
		if(ti_.n_elem<=order_)rat_throw_line("number of times must be larger than order: " + std::to_string(ti_.n_elem) + "/" + std::to_string(order_));

		// use boost interpolators
		boost::math::interpolators::barycentric_rational<fltp> fun(ti_.memptr(), vi_.memptr(), ti_.n_elem, order_);
		arma::Row<fltp> vi(positions.n_elem);
		if(derivative==0)for(arma::uword i=0;i<positions.n_elem;i++)vi(i) = fun(positions(i));
		if(derivative==1)for(arma::uword i=0;i<positions.n_elem;i++)vi(i) = fun.prime(positions(i));
		
		// finite difference
		if(derivative>1){
			const fltp delta_position = RAT_CONST(1e-4)*(ti_.max() - ti_.min());
			vi = (get_scaling_vec(positions+delta_position/2,time,derivative-1) - get_scaling_vec(positions-delta_position/2,time,derivative-1))/delta_position;
		}

		return vi;
	}

	// validity check
	bool DriveInterpBCR::is_valid(const bool enable_throws)const{
		if(ti_.n_elem<=order_){if(enable_throws){rat_throw_line("number of times must be larger than 1");} return false;};
		if(!ti_.is_sorted("strictascend")){if(enable_throws){rat_throw_line("times must be sorted");} return false;};
		if(arma::any(arma::diff(ti_)<1e-9)){if(enable_throws){rat_throw_line("times must be incrementing");} return false;};
		return true;
	}

	// apply scaling for the input settings
	void DriveInterpBCR::rescale(const fltp scale_factor){
		vi_ *= scale_factor;
	}
	
	// get type
	std::string DriveInterpBCR::get_type(){
		return "rat::mdl::driveinterpbcr";
	}

	// method for serialization into json
	void DriveInterpBCR::serialize(Json::Value &js, cmn::SList &list) const{
		Drive::serialize(js,list);
		js["type"] = get_type();
		js["order"] = static_cast<int>(order_);

		// walk over timesteps and export interpolation table
		for(arma::uword i=0;i<ti_.n_elem;i++){
			js["ti"].append(ti_(i)); js["vi"].append(vi_(i));
		}
	}

	// method for deserialisation from json
	void DriveInterpBCR::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		Drive::deserialize(js,list,factory_list,pth);
		order_ = js["order"].asUInt64();
		ti_.set_size(js["ti"].size()); vi_.set_size(js["vi"].size()); arma::uword i;
		if(ti_.n_elem!=vi_.n_elem)rat_throw_line("interpolation arrays do not have same size");
		i = 0; for(auto it = js["ti"].begin(); it!=js["ti"].end();it++,i++)ti_(i) = (*it).ASFLTP();
		i = 0; for(auto it = js["vi"].begin(); it!=js["vi"].end();it++,i++)vi_(i) = (*it).ASFLTP();
	}

}}