// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathxyz.hh"

// rat-common headers
#include "rat/common/extra.hh"
#include "rat/common/error.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathXYZ::PathXYZ(){
		R_.set_size(3,0); L_.set_size(3,0); N_.set_size(3,0); D_.set_size(3,0);
		set_name("XYZ Table");
	}

	// constructor
	PathXYZ::PathXYZ(
		const arma::Mat<fltp> &R, 
		const arma::Col<fltp>::fixed<3>& Vn,
		const bool is_cylinder) : PathXYZ(){
		set_coords(R); set_normal_vector(Vn); set_is_cylinder(is_cylinder); 
		set_use_plane_vector(true);
	}

	// constructor
	PathXYZ::PathXYZ(
		const arma::Mat<fltp> &R, 
		const arma::Mat<fltp> &L, 
		const arma::Mat<fltp> &N, 
		const arma::Mat<fltp> &D) : PathXYZ(){
		set_coords(R); set_longitudinal(L); set_normal(N); set_transverse(D);
		set_use_plane_vector(false);
	}

	// factory
	ShPathXYZPr PathXYZ::create(){
		return std::make_shared<PathXYZ>();
	}

	// factory
	ShPathXYZPr PathXYZ::create(
		const arma::Mat<fltp> &R, 
		const arma::Col<fltp>::fixed<3>& Vn,
		const bool is_cylinder){
		return std::make_shared<PathXYZ>(R,Vn,is_cylinder);
	}

	// factory
	ShPathXYZPr PathXYZ::create(
		const arma::Mat<fltp> &R, 
		const arma::Mat<fltp> &L, 
		const arma::Mat<fltp> &N, 
		const arma::Mat<fltp> &D){
		return std::make_shared<PathXYZ>(R,L,N,D);
	}


	// setters
	void PathXYZ::set_coords(const arma::Mat<fltp> &R){
		R_ = R;
	}

	void PathXYZ::set_longitudinal(const arma::Mat<fltp> &L){
		L_ = L;
	}

	void PathXYZ::set_normal(const arma::Mat<fltp> &N){
		N_ = N;
	}

	void PathXYZ::set_transverse(const arma::Mat<fltp> &D){
		D_ = D;
	}

	void PathXYZ::set_normal_vector(const arma::Col<fltp>::fixed<3>& Vn){
		Vn_ = Vn;
	}

	void PathXYZ::set_scale_factor(const fltp scale_factor){
		scale_factor_ = scale_factor;
	}

	void PathXYZ::set_is_cylinder(const bool is_cylinder){
		is_cylinder_ = is_cylinder;
	}

	void PathXYZ::set_element_size_limit(const fltp element_size_limit){
		element_size_limit_ = element_size_limit;
	}

	void PathXYZ::set_use_plane_vector(const bool use_plane_vector){
		use_plane_vector_ = use_plane_vector;
	}

	void PathXYZ::set_normalize_orientation_vectors(const bool normalize_orientation_vectors){
		normalize_orientation_vectors_ = normalize_orientation_vectors;
	}


	// getters
	const arma::Mat<fltp>& PathXYZ::get_coords()const{
		return R_;
	}

	const arma::Mat<fltp>& PathXYZ::get_longitudinal()const{
		return L_;
	}

	const arma::Mat<fltp>& PathXYZ::get_normal()const{
		return N_;
	}

	const arma::Mat<fltp>& PathXYZ::get_transverse()const{
		return D_;
	}

	const arma::Col<fltp>::fixed<3>& PathXYZ::get_normal_vector()const{
		return Vn_;
	}

	fltp PathXYZ::get_scale_factor()const{
		return scale_factor_;
	}

	bool PathXYZ::get_is_cylinder()const{
		return is_cylinder_;
	}

	fltp PathXYZ::get_element_size_limit()const{
		return element_size_limit_;
	}

	bool PathXYZ::get_use_plane_vector()const{
		return use_plane_vector_;
	}

	bool PathXYZ::get_normalize_orientation_vectors()const{
		return normalize_orientation_vectors_;
	}


	// setup coordinates and orientation vectors
	ShFramePr PathXYZ::create_frame(const MeshSettings &stngs) const{
		// check input
		is_valid(true);

		// scale data
		arma::Mat<fltp> R = scale_factor_*R_;

		// limit element size
		if(element_size_limit_>RAT_CONST(0.0) && !R.empty()){
			// create a list of coordinates and add first coordinate
			std::list<arma::Col<fltp>::fixed<3> > Rlist{R.col(0)};
			
			// fill list
			for(arma::uword i=1;i<R.n_cols;i++){
				// calculate length
				const fltp ell = arma::as_scalar(cmn::Extra::vec_norm(R.col(i) - R.col(i-1)));

				// check against limit
				if(ell>element_size_limit_){
					// calculate number to add
					const arma::uword num_add = static_cast<arma::uword>(ell/element_size_limit_);
					assert(num_add>=1);

					// interpolation positions
					const fltp spacing = RAT_CONST(1.0)/(num_add+1);
					const arma::Row<fltp> tinterp = arma::linspace<arma::Row<fltp> >(spacing,RAT_CONST(1.0) - spacing,num_add);

					// walk over new coordinates and add to list
					for(arma::uword j=0;j<num_add;j++)
						Rlist.push_back((1.0 - tinterp(j))*R.col(i-1) + tinterp(j)*R.col(i));
				}

				// add regular coordinate
				Rlist.push_back(R.col(i));
			}

			// combine
			R.set_size(3,Rlist.size()); arma::uword cnt = 0llu;
			for(auto it=Rlist.begin();it!=Rlist.end();it++)R.col(cnt++) = *it;

			// check if algorithm works as expected
			assert(arma::all(cmn::Extra::vec_norm(arma::diff(R,1,1))<=element_size_limit_));
		}

		// check
		arma::Mat<fltp> V = arma::diff(R,1,1);
		if(arma::any(cmn::Extra::vec_norm(V)==0))rat_throw_line("distance between consecutive points must be non-zero: " + 
			std::to_string(arma::as_scalar(arma::find(cmn::Extra::vec_norm(V)==0,1,"first"))) + "-" + 
			std::to_string(arma::as_scalar(arma::find(cmn::Extra::vec_norm(V)==0,1,"first"))+1) );

		// extend V
		V = arma::join_horiz(V,cmn::Extra::null_vec()) + arma::join_horiz(cmn::Extra::null_vec(),V);
		if(V.n_cols>2)V.cols(1,V.n_cols-2)/=2;

		// darboux numerical
		const arma::Mat<fltp> Va = is_cylinder_ ? 
			cmn::Extra::normalize(R - arma::repmat(cmn::Extra::normalize(Vn_),1,R.n_cols).eval().each_row()%cmn::Extra::dot(arma::repmat(cmn::Extra::normalize(Vn_),1,R.n_cols),R)) : 
			arma::repmat(cmn::Extra::normalize(Vn_),1,R.n_cols);

		// allocate frame 
		ShFramePr frame;

		// assume the path lies in a plane (or cylinder)
		if(use_plane_vector_){
			// calculate orientation vectors assuming Va is axial vector
			const arma::Mat<fltp> L = cmn::Extra::normalize(V);
			const arma::Mat<fltp> N = cmn::Extra::normalize(cmn::Extra::cross(L,Va));
			const arma::Mat<fltp> D = cmn::Extra::cross(N,L);

			// create frame from calculated vectors
			frame = Frame::create(R, L, N, D);
		}

		// direct setting of orientation vectors
		else{
			// check if vectors have same size
			if(R_.n_cols!=L_.n_cols)rat_throw_line("L must be equal in size to R");
			if(R_.n_cols!=N_.n_cols)rat_throw_line("N must be equal in size to R"); 
			if(R_.n_cols!=D_.n_cols)rat_throw_line("D must be equal in size to R");
			
			// create frame directly from vectors
			const bool check_zeros = true;
			frame = normalize_orientation_vectors_ ? Frame::create(R_, cmn::Extra::normalize(L_,check_zeros), cmn::Extra::normalize(N_,check_zeros), cmn::Extra::normalize(D_,check_zeros)) : Frame::create(R_, L_, N_, D_);
		}

		// transformations
		frame->apply_transformations(get_transformations(), stngs.time);

		// create frame
		return frame;
	}


	// validity check
	bool PathXYZ::is_valid(const bool enable_throws) const{
		if(scale_factor_<=0){if(enable_throws){rat_throw_line("scaling factor must be larger than zero");} return false;};
		if(R_.n_cols<2){if(enable_throws){rat_throw_line("need at least two coordinates");} return false;};
		if(R_.n_rows!=3){if(enable_throws){rat_throw_line("coordinates must be three dimensional");} return false;};
		if(use_plane_vector_){
			if(arma::as_scalar(cmn::Extra::vec_norm(Vn_))<1e-14){if(enable_throws){rat_throw_line("plane normal vector has no length");} return false;};
		}else{
			if(L_.empty()){if(enable_throws){rat_throw_line("longitudinal vector is empty");} return false;};
			if(N_.empty()){if(enable_throws){rat_throw_line("longitudinal vector is empty");} return false;};
			if(D_.empty()){if(enable_throws){rat_throw_line("longitudinal vector is empty");} return false;};
			if(L_.n_cols!=R_.n_cols){if(enable_throws){rat_throw_line("number of longitudinal vectors must equal the number of coordinates");} return false;};
			if(N_.n_cols!=R_.n_cols){if(enable_throws){rat_throw_line("number of normal vectors must equal the number of coordinates");} return false;};
			if(D_.n_cols!=R_.n_cols){if(enable_throws){rat_throw_line("number of transverse vectors must equal the number of coordinates");} return false;};
		}
		return true;
	}


	// get type
	std::string PathXYZ::get_type(){
		return "rat::mdl::pathxyz";
	}

	// method for serialization into json
	void PathXYZ::serialize(
		Json::Value &js, cmn::SList &list) const{
		// parent
		Path::serialize(js,list);
		Transformations::serialize(js,list);
		js["type"] = get_type();
		
		// export coordinates
		for(arma::uword i=0;i<R_.n_elem;i++)
			js["R"].append(R_(i));
		for(arma::uword i=0;i<L_.n_elem;i++)
			js["L"].append(L_(i));
		for(arma::uword i=0;i<N_.n_elem;i++)
			js["N"].append(N_(i));
		for(arma::uword i=0;i<D_.n_elem;i++)
			js["D"].append(D_(i));

		// normal vector
		js["Vn"]["x"] = Vn_(0);
		js["Vn"]["y"] = Vn_(1);
		js["Vn"]["z"] = Vn_(2);

		// scaling factor
		js["scale_factor"] = scale_factor_;

		// cylinder
		js["is_cylinder"] = is_cylinder_;

		// limit element size
		js["element_size_limit"] = element_size_limit_;

		// plane vector switch
		js["use_plane_vector"] = use_plane_vector_;

		// normalize the orientation vectors
		js["normalize_orientation_vectors"] = normalize_orientation_vectors_;
	}

	// method for deserialisation from json
	void PathXYZ::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent
		Path::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);
			
		// counter
		arma::uword idx;

		// read coordinate matrix
		R_.set_size(3,js["R"].size()/3); idx = 0llu;
		for(auto it=js["R"].begin();it!=js["R"].end();it++)
			R_(idx++) = (*it).ASFLTP();

		L_.set_size(3,js["L"].size()/3); idx = 0llu;
		for(auto it=js["L"].begin();it!=js["L"].end();it++)
			L_(idx++) = (*it).ASFLTP();

		N_.set_size(3,js["N"].size()/3); idx = 0llu;
		for(auto it=js["N"].begin();it!=js["N"].end();it++)
			N_(idx++) = (*it).ASFLTP();

		D_.set_size(3,js["D"].size()/3); idx = 0llu;
		for(auto it=js["D"].begin();it!=js["D"].end();it++)
			D_(idx++) = (*it).ASFLTP();

		// normal vector
		Vn_(0) = js["Vn"]["x"].ASFLTP();
		Vn_(1) = js["Vn"]["y"].ASFLTP();
		Vn_(2) = js["Vn"]["z"].ASFLTP();

		// scaling factor
		if(js.isMember("scale_factor"))set_scale_factor(js["scale_factor"].ASFLTP());

		// plane vector
		if(js.isMember("use_plane_vector")){
			set_use_plane_vector(js["use_plane_vector"].asBool());
		}else{
			set_use_plane_vector(L_.empty() && N_.empty() && D_.empty());
		}

		// cylinder
		set_is_cylinder(js["is_cylinder"].asBool());

		// limit element size
		set_element_size_limit(js["element_size_limit"].ASFLTP());

		// normalize
		set_normalize_orientation_vectors(js["normalize_orientation_vectors"].asBool());
	}

}}