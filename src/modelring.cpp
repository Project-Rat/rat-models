// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelring.hh"

#include "crossrectangle.hh"
#include "pathcircle.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	ModelRing::ModelRing(){
		set_name("Ring Permanent Magnet");
		set_magnetisation({RAT_CONST(0.0), RAT_CONST(2.0)/arma::Datum<fltp>::mu_0, RAT_CONST(0.0)});
	}

	// constructor with dimension input
	ModelRing::ModelRing(
		const fltp inner_radius,
		const fltp thickness,
		const fltp height,
		const fltp element_size) : ModelRing(){

		// set settings to self
		set_inner_radius(inner_radius); set_thickness(thickness);
		set_height(height); set_element_size(element_size);
	}

	// factory
	ShModelRingPr ModelRing::create(){
		//return ShModelRingPr(new ModelRing);
		return std::make_shared<ModelRing>();
	}

	// factory with dimension input
	ShModelRingPr ModelRing::create(
		const fltp inner_radius,
		const fltp thickness,
		const fltp height,
		const fltp element_size){
		return std::make_shared<ModelRing>(
			inner_radius, thickness, height, element_size);
	}

	// get base and cross
	ShPathPr ModelRing::get_input_path() const{
		// check input
		is_valid(true);

		// create axis
		const ShPathCirclePr circle = PathCircle::create(inner_radius_,4,element_size_,thickness_);

		// return the racetrack
		return circle;
	}

	// get cross section
	ShCrossPr ModelRing::get_input_cross() const{
		// check input
		is_valid(true);

		// create rectangular cross
		ShCrossRectanglePr rectangle = CrossRectangle::create(
			0, thickness_, -height_/2, height_/2, element_size_, element_size_);

		// return the rectangle
		return rectangle;
	}


	// set inner_radius
	void ModelRing::set_inner_radius(const fltp inner_radius){
		inner_radius_ = inner_radius;
	}
	
	// set thickness
	void ModelRing::set_thickness(const fltp thickness){
		thickness_ = thickness;
	}
	
	// set height
	void ModelRing::set_height(const fltp height){
		height_ = height;
	}
	
		// set inner_radius
	void ModelRing::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}
	


	// get inner_radius
	fltp ModelRing::get_inner_radius() const{
		return inner_radius_;
	}

	// get thickness
	fltp ModelRing::get_thickness() const{
		return thickness_;
	}

	// get height
	fltp ModelRing::get_height() const{
		return height_;
	}

	// get element size
	fltp ModelRing::get_element_size() const{
		return element_size_;
	}


	
	// vallidity check
	bool ModelRing::is_valid(const bool enable_throws) const{
		if(!ModelBarWrapper::is_valid(enable_throws))return false;
		if(inner_radius_<=0){if(enable_throws){rat_throw_line("inner_radius must be larger than zero");} return false;};
		if(thickness_<=0){if(enable_throws){rat_throw_line("thickness must be larger than zero");} return false;};
		if(height_<=0){if(enable_throws){rat_throw_line("height must be larger than zero");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string ModelRing::get_type(){
		return "rat::mdl::modelring";
	}

	// method for serialization into json
	void ModelRing::serialize(
		Json::Value &js, cmn::SList &list) const{
		// parent
		ModelBarWrapper::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["inner_radius"] = inner_radius_;
		js["thickness"] = thickness_;
		js["height"] = height_;
		js["element_size"] = element_size_;
	}

	// method for deserialisation from json
	void ModelRing::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent
		ModelBarWrapper::deserialize(js,list,factory_list,pth);

		// properties
		set_inner_radius(js["inner_radius"].ASFLTP());
		set_thickness(js["thickness"].ASFLTP());
		set_height(js["height"].ASFLTP());
		set_element_size(js["element_size"].ASFLTP());
	}

}}
