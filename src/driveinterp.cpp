// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "driveinterp.hh"


// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	DriveInterp::DriveInterp(){
		set_name("Interpolation");
	}

	// constructor
	DriveInterp::DriveInterp(const arma::Col<fltp> &ti, const arma::Col<fltp> &vi, const fltp dt) : DriveInterp(){
		set_interpolation_table(ti,vi); set_delta_time(dt);
	}

	// factory
	ShDriveInterpPr DriveInterp::create(){
		return std::make_shared<DriveInterp>();
	}

	// factory
	ShDriveInterpPr DriveInterp::create(const arma::Col<fltp> &ti, const arma::Col<fltp> &vi, const fltp dt){
		return std::make_shared<DriveInterp>(ti,vi,dt);
	}

	// set delta time for smoothing
	void DriveInterp::set_delta_time(const fltp dt){
		dt_ = dt;
	}

	// get delta time for smoothing
	fltp DriveInterp::get_delta_time()const{
		return dt_;
	}

	// set and get current
	void DriveInterp::set_interpolation_table(const arma::Col<fltp> &ti, const arma::Col<fltp> &vi){
		if(ti.n_elem!=vi.n_elem)rat_throw_line("number of elements must be the same");
		ti_ = ti; vi_ = vi;
	}

	// setters
	void DriveInterp::set_interpolation_times(const arma::Col<fltp> &ti){
		ti_ = ti;
	}

	// get values
	void DriveInterp::set_interpolation_values(const arma::Col<fltp> &vi){
		vi_ = vi;
	}

	// get times
	const arma::Col<fltp>& DriveInterp::get_interpolation_times()const{
		return ti_;
	}

	// get values
	const arma::Col<fltp>& DriveInterp::get_interpolation_values()const{
		return vi_;
	}

	// getters
	arma::uword DriveInterp::get_num_times()const{
		return ti_.n_elem;
	}

	// boundaries for making a plot
	fltp DriveInterp::get_v1() const{
		return ti_.front() - RAT_CONST(0.1)*(ti_.back() - ti_.front());
	}
	
	fltp DriveInterp::get_v2() const{
		return ti_.back() + RAT_CONST(0.1)*(ti_.back() - ti_.front());
	}

	// get drive scaling
	fltp DriveInterp::get_scaling_core(
		const fltp position, const arma::uword derivative) const{

		// check if interpolation table exists
		assert(!ti_.is_empty()); assert(!vi_.is_empty());

		// function itself
		if(derivative==0){
			// extrapolation
			if(position<=ti_.front())return vi_.front();
			if(position>=ti_.back())return vi_.back();

			// find array
			const arma::uword idx = arma::as_scalar(arma::find(ti_.head_rows(ti_.n_elem-1)<position && ti_.tail_rows(ti_.n_elem-1)>=position));

			// find neighbouring points
			const fltp t1 = ti_(idx);
			const fltp t2 = ti_(idx+1);
			const fltp v1 = vi_(idx);
			const fltp v2 = vi_(idx+1);
			
			// relative position
			const fltp tt = position - ti_(idx);

			// calculate slope
			const fltp slope = (v2-v1)/(t2-t1);

			// return interpolated value
			return v1 + tt*slope;
		}
		
		// first derivative
		if(derivative==1){
			// extrapolation
			if(position<=ti_.front() || position>=ti_.back())return 0;
			
			// find array
			const arma::uword idx = arma::as_scalar(arma::find(ti_.head_rows(ti_.n_elem-1)<position && ti_.tail_rows(ti_.n_elem-1)>=position));

			// find neighbouring points
			const fltp t1 = ti_(idx);
			const fltp t2 = ti_(idx+1);
			const fltp v1 = vi_(idx);
			const fltp v2 = vi_(idx+1);
			
			// calculate slope
			const fltp slope = (v2-v1)/(t2-t1);

			// return slope
			return slope;
		}

		// higher order derivatives are all zero because of linear interpolation
		return RAT_CONST(0.0);
	}

	fltp DriveInterp::get_scaling(
		const fltp position,
		const fltp /*time*/,
		const arma::uword derivative) const{

		// no smoothing
		if(dt_==0)return get_scaling_core(position,derivative);

		// with smoothing
		return (get_scaling_core(position-dt_/2, derivative) + get_scaling_core(position+dt_/2, derivative))/2;
	}

	// get times at which an inflection occurs
	arma::Col<fltp> DriveInterp::get_inflection_points() const{
		// smoothing enabled
		if(dt_>0)return{};

		// check if there are inflection points
		if(ti_.n_elem>2){
			arma::Col<fltp> times =  ti_.rows(1,ti_.n_elem-2);
			times = times(arma::find(arma::join_vert(arma::Col<arma::uword>{1},
				(times.tail_rows(times.n_elem-1)-
				times.head_rows(times.n_elem-1))>1e-8)));
			return times;
		}
		else return arma::Col<fltp>{};
	}

	// validity check
	bool DriveInterp::is_valid(const bool enable_throws)const{
		if(ti_.n_elem<2){if(enable_throws){rat_throw_line("number of times must be larger than 1");} return false;};
		if(!ti_.is_sorted("ascend")){if(enable_throws){rat_throw_line("times must be sorted");} return false;};
		return true;
	}

	// apply scaling for the input settings
	void DriveInterp::rescale(const fltp scale_factor){
		vi_ *= scale_factor;
	}

	// get type
	std::string DriveInterp::get_type(){
		return "rat::mdl::driveinterp";
	}

	// method for serialization into json
	void DriveInterp::serialize(Json::Value &js, cmn::SList &list) const{
		Drive::serialize(js,list);
		js["type"] = get_type();

		// walk over timesteps and export interpolation table
		for(arma::uword i=0;i<ti_.n_elem;i++){
			js["ti"].append(ti_(i)); js["vi"].append(vi_(i));
		}
		js["dt"] = dt_;
	}

	// method for deserialisation from json
	void DriveInterp::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		Drive::deserialize(js,list,factory_list,pth);
		ti_.set_size(js["ti"].size()); vi_.set_size(js["vi"].size()); arma::uword i;
		if(ti_.n_elem!=vi_.n_elem)rat_throw_line("interpolation arrays do not have same size");
		i = 0; for(auto it = js["ti"].begin(); it!=js["ti"].end();it++,i++)ti_(i) = (*it).ASFLTP();
		i = 0; for(auto it = js["vi"].begin(); it!=js["vi"].end();it++,i++)vi_(i) = (*it).ASFLTP();
		set_delta_time(js["dt"].ASFLTP());
	}

}}