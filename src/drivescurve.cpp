// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "drivescurve.hh"

// common headers
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	

	// constructor
	DriveSCurve::DriveSCurve(){
		set_name("S-Curve");
	}

	// constructor
	DriveSCurve::DriveSCurve(
		const fltp offset, const fltp amplitude, const fltp slope, 
		const fltp radius, const fltp tmid, const fltp velocity) : DriveSCurve(){
		
		set_offset(offset); set_amplitude(amplitude); set_slope(slope);
		set_radius(radius); set_tmid(tmid); set_velocity(velocity);
	}

	// factory
	ShDriveSCurvePr DriveSCurve::create(){
		return std::make_shared<DriveSCurve>();
	}

	// factory
	ShDriveSCurvePr DriveSCurve::create(
		const fltp offset, const fltp amplitude, const fltp slope, 
		const fltp radius, const fltp tmid, const fltp velocity){
		return std::make_shared<DriveSCurve>(offset, amplitude, slope, radius, tmid, velocity);
	}

	// setters
	void DriveSCurve::set_tmid(const fltp tmid){
		tmid_ = tmid;
	}

	void DriveSCurve::set_radius(const fltp radius){
		radius_ = radius;
	}

	void DriveSCurve::set_slope(const fltp slope){
		slope_ = slope;
	}

	void DriveSCurve::set_amplitude(const fltp amplitude){
		amplitude_ = amplitude;
	}

	void DriveSCurve::set_offset(const fltp offset){
		offset_ = offset;
	}

	void DriveSCurve::set_velocity(const fltp velocity){
		velocity_ = velocity;
	}


	// getters
	fltp DriveSCurve::get_tmid()const{
		return tmid_;
	}

	fltp DriveSCurve::get_radius()const{
		return radius_;
	}

	fltp DriveSCurve::get_slope()const{
		return slope_;
	}

	fltp DriveSCurve::get_amplitude()const{
		return amplitude_;
	}

	fltp DriveSCurve::get_offset()const{
		return offset_;
	}

	fltp DriveSCurve::get_velocity()const{
		return velocity_;
	}


	// boundaries for making a plot
	fltp DriveSCurve::get_v1() const{
		return tmid_ - (std::abs(amplitude_) - 2*(radius_ - std::cos(std::atan(slope_))*radius_))/slope_/2 - std::sin(std::atan(slope_))*radius_;
	}
	
	fltp DriveSCurve::get_v2() const{
		return tmid_ + (std::abs(amplitude_) - 2*(radius_ - std::cos(std::atan(slope_))*radius_))/slope_/2 + std::sin(std::atan(slope_))*radius_;
	}

	// get scaling
	fltp DriveSCurve::get_scaling(
		const fltp position,
		const fltp time,
		const arma::uword derivative) const{

		// calculate angle
		const fltp alpha = std::atan(slope_);

		// calculate midpoint with velocity
		const fltp tmid = tmid_ + velocity_*time;

		// duration of each part of the curve
		const fltp sgn = cmn::Extra::sign(amplitude_);
		const fltp h1 = radius_ - std::cos(alpha)*radius_;
		const fltp h2 = std::abs(amplitude_) - 2*h1;
		const fltp ell1 = std::sin(alpha)*radius_;
		const fltp ell2 = h2/slope_;
		const fltp ell3 = std::sin(alpha)*radius_;
		const fltp t1 = tmid - ell2/2 - ell1;
		const fltp t3 = tmid + ell2/2 + ell3;

		// return value based on section
		if(derivative==0){
			if(position<t1)return offset_;	
			if(position<tmid - ell2/2)return offset_ + sgn*(radius_ - std::sqrt(radius_*radius_ - (position - t1)*(position - t1)));
			if(position<tmid + ell2/2)return offset_ + sgn*(h1 + slope_*(position-tmid+ell2/2));
			if(position<t3)return offset_ + sgn*(std::abs(amplitude_) - radius_ + std::sqrt(radius_*radius_ - (position - t3)*(position - t3)));
			return offset_ + amplitude_;
		}
		if(derivative==1){
			if(position<t1)return 0.0;	
			if(position<tmid - ell2/2)return (position-t1)/std::sqrt(radius_*radius_-(position-t1)*(position-t1));
			if(position<tmid + ell2/2)return slope_;
			if(position<t3)return -(position-t3)/std::sqrt(radius_*radius_-(position-t3)*(position-t3));
			return 0.0;
		}
		if(derivative==2){
			if(position<t1)return 0.0;	
			if(position<tmid - ell2/2)return radius_*radius_/std::pow(radius_*radius_-(position-t1)*(position-t1),3.0/2);
			if(position<tmid + ell2/2)return 0.0;
			if(position<t3)return -radius_*radius_/std::pow(radius_*radius_-(position-t3)*(position-t3),3.0/2);
			return 0.0;
		}
		if(derivative==3){
			if(position<t1)return 0.0;	
			if(position<tmid - ell2/2)return (3*radius_*radius_*(position-t1))/std::pow(radius_*radius_-(position-t1)*(position-t1),5.0/2);
			if(position<tmid + ell2/2)return 0.0;
			if(position<t3)return -(3*radius_*radius_*(position-t3))/std::pow(radius_*radius_-(position-t3)*(position-t3),5.0/2);
			return 0.0;
		}
		rat_throw_line("higher derivative not implemented");
		return 0.0;
	}

	// apply scaling for the input settings
	void DriveSCurve::rescale(const fltp scale_factor){
		amplitude_ *= scale_factor;
	}

	// validity check
	bool DriveSCurve::is_valid(const bool enable_throws)const{
		if(radius_<=0.0){if(enable_throws){rat_throw_line("radius must be positive");} return false;};
		return true;
	}

	// get type
	std::string DriveSCurve::get_type(){
		return "rat::mdl::drivescurve";
	}

	// method for serialization into json
	void DriveSCurve::serialize(
		Json::Value &js, cmn::SList &list) const{
		Drive::serialize(js,list);
		js["type"] = get_type();
		js["amplitude"] = amplitude_;
		js["tmid"] = tmid_;
		js["radius"] = radius_;
		js["slope"] = slope_;
		js["offset"] = offset_;
		js["velocity"] = velocity_;
	}

	// method for deserialisation from json
	void DriveSCurve::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		Drive::deserialize(js,list,factory_list,pth);
		set_amplitude(js["amplitude"].ASFLTP());
		set_tmid(js["tmid"].ASFLTP());
		set_radius(js["radius"].ASFLTP());
		set_slope(js["slope"].ASFLTP());
		set_offset(js["offset"].ASFLTP());
		set_velocity(js["velocity"].ASFLTP());
	}
}}