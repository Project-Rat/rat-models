// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "emitternewton.hh"

// rat-common headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// headers
#include "modelmesh.hh"
#include "pathcircle.hh"
#include "crosspoint.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	EmitterNewton::EmitterNewton(){
		set_name("Newton Emitter");
	}

	// constructor
	EmitterNewton::EmitterNewton(
		const fltp sigma_x, const fltp sigma_y,
		const fltp sigma_xa, const fltp sigma_ya,
		const fltp corr_xx, const fltp corr_yy) : EmitterNewton(){
		sigma_x_ = sigma_x; sigma_y_ = sigma_y;
		sigma_xa_ = sigma_xa; sigma_ya_ = sigma_ya;
		corr_xx_ = corr_xx; corr_yy_ = corr_yy;
	}

	// factory
	ShEmitterNewtonPr EmitterNewton::create(){
		return std::make_shared<EmitterNewton>();
	}

	// factory
	ShEmitterNewtonPr EmitterNewton::create(
		const fltp sigma_x, const fltp sigma_y,
		const fltp sigma_xa, const fltp sigma_ya,
		const fltp corr_xx, const fltp corr_yy){
		return std::make_shared<EmitterNewton>(
			sigma_x,sigma_y,sigma_xa,sigma_ya,corr_xx,corr_yy);
	}
				
	// create correlated data
	arma::Mat<fltp> EmitterNewton::correlated_normdist(
		const fltp correlation, const arma::uword num_cols){
		// full correlation
		if(correlation==1.0)return arma::repmat(arma::Row<fltp>(num_cols,arma::fill::randn),2,1);

		// create correlation matrix for normal direction
		arma::Mat<fltp>::fixed<2,2> Mc;

		// create correlation matrix
		arma::Mat<fltp>::fixed<2,2> M;
		M.row(0) = arma::Row<fltp>{RAT_CONST(1.0), correlation};
		M.row(1) = arma::Row<fltp>{correlation, RAT_CONST(1.0)};
		Mc = arma::chol(M).t();

		// multiply correlation matrix with
		// random distribution and return
		return Mc*arma::Mat<fltp>(2,num_cols,arma::fill::randn);
	}


	// set orientation
	void EmitterNewton::set_spawn_coord(
		const arma::Col<fltp>::fixed<3> &R, 
		const arma::Col<fltp>::fixed<3> &L, 
		const arma::Col<fltp>::fixed<3> &N, 
		const arma::Col<fltp>::fixed<3> &D){

		R_ = R;
		L_ = L.each_row()/cmn::Extra::vec_norm(L); 
		N_ = N.each_row()/cmn::Extra::vec_norm(N);
		D_ = D.each_row()/cmn::Extra::vec_norm(D);
	} 

	// set position xx
	void EmitterNewton::set_xx(const fltp corr_xx, const fltp sigma_x, const fltp sigma_xa){
		corr_xx_ = corr_xx; sigma_x_ = sigma_x; sigma_xa_ = sigma_xa;
	}

	// set position yy
	void EmitterNewton::set_yy(const fltp corr_yy, const fltp sigma_y, const fltp sigma_ya){
		corr_yy_ = corr_yy; sigma_y_ = sigma_y; sigma_ya_ = sigma_ya;
	}



	// set corelation in horizontal position
	void EmitterNewton::set_corelation_xx(const fltp corr_xx){
		corr_xx_ = corr_xx;
	}

	// set corelation in vertical position
	void EmitterNewton::set_corelation_yy(const fltp corr_yy){
		corr_yy_ = corr_yy;
	}

	// set standard deviation in horizontal position
	void EmitterNewton::set_sigma_x(const fltp sigma_x){
		sigma_x_ = sigma_x;
	}

	// set standard deviation in vertical position
	void EmitterNewton::set_sigma_y(const fltp sigma_y){
		sigma_y_ = sigma_y;
	}

	// set standard deviation in horizontal momentum
	void EmitterNewton::set_sigma_xa(const fltp sigma_xa){
		sigma_xa_ = sigma_xa;
	}

	// set standard deviation in vertical momentum
	void EmitterNewton::set_sigma_ya(const fltp sigma_ya){
		sigma_ya_ = sigma_ya;
	}
	

	void EmitterNewton::set_sigma_velocity(const fltp sigma_velocity){
		sigma_velocity_ = sigma_velocity;
	}


	// setters
	void EmitterNewton::set_velocity(const fltp velocity){
		// if(rest_mass<=0)rat_throw_line("rest mass must be larger than zero");
		velocity_ = velocity;
	}

	void EmitterNewton::set_charge(const fltp charge){
		charge_ = charge;
	}

	void EmitterNewton::set_mass(const fltp mass){
		mass_ = mass;
	}

	void EmitterNewton::set_lifetime(const arma::uword lifetime){
		// if(lifetime<=0)rat_throw_line("lifetime must be larger than zero");
		lifetime_ = lifetime; 
	}

	void EmitterNewton::set_start_idx(const arma::uword start_idx){
		start_idx_ = start_idx;
	}

	void EmitterNewton::set_num_particles(const arma::uword num_particles){
		num_particles_ = num_particles;
	}

	void EmitterNewton::set_coord(
		const arma::Col<fltp>::fixed<3> &R){
		R_ = R;
	}

	void EmitterNewton::set_longitudinal(
		const arma::Col<fltp>::fixed<3> &L){
		L_ = L;
	}

	void EmitterNewton::set_normal(
		const arma::Col<fltp>::fixed<3> &N){
		N_ = N;
	}

	void EmitterNewton::set_transverse(
		const arma::Col<fltp>::fixed<3> &D){
		D_ = D;
	}

	void EmitterNewton::set_seed(const unsigned int seed){
		seed_ = seed;
	}


	// getters
	fltp EmitterNewton::get_mass() const{
		return mass_;
	}
	
	arma::uword EmitterNewton::get_num_particles() const{
		return num_particles_;
	}

	fltp EmitterNewton::get_charge()const{
		return charge_;
	}

	fltp EmitterNewton::get_velocity()const{
		return velocity_;
	}

	arma::uword EmitterNewton::get_lifetime()const{
		return lifetime_;
	}

	const arma::Col<fltp>::fixed<3>& EmitterNewton::get_coord()const{
		return R_;
	}

	const arma::Col<fltp>::fixed<3>& EmitterNewton::get_longitudinal()const{
		return L_;
	}

	const arma::Col<fltp>::fixed<3>& EmitterNewton::get_normal()const{
		return N_;
	}

	const arma::Col<fltp>::fixed<3>& EmitterNewton::get_transverse()const{
		return D_;
	}

	// set corelation in horizontal position
	fltp EmitterNewton::get_corelation_xx()const{
		return corr_xx_;
	}

	// set corelation in vertical position
	fltp EmitterNewton::get_corelation_yy()const{
		return corr_yy_;
	}

	// set standard deviation in horizontal position
	fltp EmitterNewton::get_sigma_x()const{
		return sigma_x_;
	}

	// set standard deviation in vertical position
	fltp EmitterNewton::get_sigma_y()const{
		return sigma_y_;
	}

	// set standard deviation in horizontal momentum
	fltp EmitterNewton::get_sigma_xa()const{
		return sigma_xa_;
	}

	// set standard deviation in vertical momentum
	fltp EmitterNewton::get_sigma_ya()const{
		return sigma_ya_;
	}
	
	arma::uword EmitterNewton::get_start_idx()const{
		return start_idx_;
	}

	unsigned int EmitterNewton::get_seed()const{
		return seed_;
	}

	fltp EmitterNewton::get_sigma_velocity()const{
		return sigma_velocity_;
	}

	// particle creation
	arma::field<Particle> EmitterNewton::spawn_particles(const fltp time) const{
		// error check
		is_valid(true);

		// apply transformations
		arma::Col<fltp>::fixed<3> R = R_, L = L_, N = N_, D = D_;
		for(auto it=trans_.begin();it!=trans_.end();it++){
			(*it).second->apply_vectors(R, L, 'L', time);
			(*it).second->apply_vectors(R, N, 'N', time);
			(*it).second->apply_vectors(R, D, 'D', time);
			(*it).second->apply_coords(R, time);
		}

		// set seed for random number generator
		arma::arma_rng::set_seed(seed_);

		// velocity
		const arma::Row<fltp> velocity = (velocity_ + sigma_velocity_*
			arma::randn<arma::Row<fltp> >(num_particles_))/arma::Datum<fltp>::c_0;

		// generate data in x
		const arma::Mat<fltp> XX = correlated_normdist(corr_xx_, num_particles_);
		const arma::Mat<fltp> YY = correlated_normdist(corr_yy_, num_particles_);

		// calculate angles
		const arma::Row<fltp> alpha = sigma_xa_*XX.row(1);
		const arma::Row<fltp> beta = sigma_ya_*YY.row(1);

		// get position in local coordinates
		const arma::Row<fltp> u = sigma_x_*XX.row(0);
		const arma::Row<fltp> v = sigma_y_*YY.row(0);
		const arma::Row<fltp> w(num_particles_,arma::fill::zeros);
		assert(u.n_elem==num_particles_);
		assert(v.n_elem==num_particles_);

		// create start positions
		arma::Mat<fltp> R0(3,num_particles_);
		arma::Mat<fltp> V0(3,num_particles_);

		// convert to cartesian coordinates
		for(arma::uword i=0;i<num_particles_;i++){

			// apply rotation
			const arma::Mat<fltp>::fixed<3,3> Mrot = 
				cmn::Extra::create_rotation_matrix({0,1,0},-alpha(i))*
				cmn::Extra::create_rotation_matrix({1,0,0},beta(i));

			// get velocity in local coordinates
			const fltp du = velocity(i)*Mrot(0,2);
			const fltp dv = velocity(i)*Mrot(1,2);
			const fltp dw = velocity(i)*Mrot(2,2);

			// walk over dimensions
			for(arma::uword j=0;j<3;j++){ 
				R0(j,i) = R(j) + L(j)*w(i) + N(j)*u(i) + D(j)*v(i);
				V0(j,i) = L(j)*dw + N(j)*du + D(j)*dv;
			}
		}

		// check velocity
		if(!V0.is_finite())
			rat_throw_line("particle velocity is not finite");
		if(arma::any(arma::abs(cmn::Extra::vec_norm(V0) - arma::abs(velocity))>RAT_CONST(1e-9)))
			rat_throw_line("particle velocity does not correspond to setting");
		if(arma::any(cmn::Extra::vec_norm(V0)>RAT_CONST(1.0)))
			rat_throw_line("A particle is going faster than the speed of light");

		// allocate list of particles
		arma::field<Particle> particles(num_particles_);

		// create all particles
		for(arma::uword i=0;i<num_particles_;i++){
			// set maximum number of steps
			particles(i).set_num_steps(lifetime_);

			// set start coordinate and velocity
			particles(i).set_startcoord(R0.col(i),V0.col(i));

			// set start index
			particles(i).set_start_index(start_idx_);

			// set mass
			const fltp mass_in_kg = mass_*arma::Datum<fltp>::m_u;
			const fltp mass_in_J = mass_in_kg*arma::Datum<fltp>::c_0*arma::Datum<fltp>::c_0;
			const fltp mass_in_eV = mass_in_J/arma::Datum<fltp>::eV;
			const fltp mass_in_GeV = mass_in_eV/1e9;
			particles(i).set_rest_mass(mass_in_GeV); // atomic mass constant 
			particles(i).set_charge(charge_);

			// setup internal storage
			particles(i).setup();
			if(start_idx_==0)particles(i).terminate_start();
			if(start_idx_==lifetime_-1)particles(i).terminate_end();
		}

		// return list of particles
		return particles;
	}

	// create specific mesh
	std::list<ShMeshDataPr> EmitterNewton::create_meshes(
		const std::list<arma::uword> &trace, 
		const MeshSettings &stngs) const{

		// check if valid
		if(!trace.empty())rat_throw_line("max trace depth reached but trace not empty");
		if(!is_valid(stngs.enable_throws))return{};

		// draw ellipse
		const arma::uword num_elements = 30;
		const arma::uword num_nodes = num_elements+1;
		const arma::Row<fltp> theta = arma::linspace<arma::Row<fltp> >(0,2*arma::Datum<fltp>::pi,num_nodes);
		const arma::Row<fltp> u = sigma_x_*arma::cos(theta);
		const arma::Row<fltp> v = sigma_y_*arma::sin(theta);
		const arma::Row<fltp> w(num_nodes, arma::fill::zeros);

		// combine
		arma::Mat<fltp> Rc(3,num_nodes);
		for(arma::uword i=0;i<3;i++)
			Rc.row(i) = R_(i) + L_(i)*w + N_(i)*u + D_(i)*v;

		// add center line
		Rc = arma::join_horiz(Rc, R_-2*L_*std::max(sigma_x_, sigma_y_), R_+2*L_*std::max(sigma_x_, sigma_y_));

		// apply transformations
		for(auto it=trans_.begin();it!=trans_.end();it++)
			(*it).second->apply_coords(Rc,stngs.time);
		
		// create coords
		arma::Mat<arma::uword> n = arma::join_vert(
			arma::regspace<arma::Row<arma::uword> >(0,num_nodes-2),
			arma::regspace<arma::Row<arma::uword> >(1,num_nodes-1));

		// add center line
		n = arma::join_horiz(n, arma::Col<arma::uword>::fixed<2>{num_nodes, num_nodes+1});

		// create mesh
		ShMeshDataPr mesh_data = MeshData::create(Rc, n, n);

		// draw mesh
		// set temperature
		mesh_data->set_operating_temperature(0);

		// set time
		mesh_data->set_time(stngs.time);

		// set name (is appended by models later)
		mesh_data->append_name(myname_);

		// flag this mesh as calculation
		mesh_data->set_calc_mesh();
		
		// mesh data object
		return {mesh_data};
	}

	// validity check
	bool EmitterNewton::is_valid(const bool enable_throws) const{
		if(mass_<0){if(enable_throws){rat_throw_line("mass must be larger than zero");} return false;};
		if(lifetime_<=0){if(enable_throws){rat_throw_line("lifetime must be larger than zero");} return false;};
		if(start_idx_>=lifetime_){if(enable_throws){rat_throw_line("start index must be less than lifetime");} return false;};
		if(corr_xx_<0){if(enable_throws){rat_throw_line("correlation must be larger than zero");} return false;};
		if(corr_xx_>1){if(enable_throws){rat_throw_line("correlation must be smaller than one");} return false;};
		if(sigma_x_<0){if(enable_throws){rat_throw_line("beam size must be positive");} return false;};
		if(corr_yy_<0){if(enable_throws){rat_throw_line("correlation must be larger than zero");} return false;};
		if(corr_yy_>1){if(enable_throws){rat_throw_line("correlation must be smaller than one");} return false;};
		if(sigma_y_<0){if(enable_throws){rat_throw_line("beam size must be positive");} return false;};
		return true;
	}

	// get type
	std::string EmitterNewton::get_type(){
		return "rat::mdl::emitternewton";
	}

	// method for serialization into json
	void EmitterNewton::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Emitter::serialize(js,list);

		// properties
		js["type"] = get_type();

		// rng
		js["seed"] = static_cast<unsigned int>(seed_);

		// beam parameters
		js["num_particles"] = static_cast<unsigned int>(num_particles_);
		js["mass"] = mass_;
		js["velocity"] = velocity_;
		js["sigma_velocity"] = sigma_velocity_;
		js["charge"] = charge_;

		// position orientation
		js["position"] = Node::serialize_matrix(R_);
		js["longitudinal"] = Node::serialize_matrix(L_);
		js["normal"] = Node::serialize_matrix(N_);
		js["transverse"] = Node::serialize_matrix(D_);

		// beam
		// js["sigma"] = sigma_;
		js["corr_xx"] = corr_xx_;
		js["sigma_x"] = sigma_x_;
		js["sigma_xa"] = sigma_xa_;
		js["corr_yy"] = corr_yy_;
		js["sigma_y"] = sigma_y_;
		js["sigma_ya"] = sigma_ya_;

		// tracking
		js["lifetime"] = static_cast<unsigned int>(lifetime_);
		js["start_idx"] = static_cast<unsigned int>(start_idx_);
	}

	// method for deserialisation from json
	void EmitterNewton::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		
		// parent
		Emitter::deserialize(js,list,factory_list,pth);
		
		// rng
		seed_ = js["seed"].asUInt64();

		// beam parameters
		num_particles_ = js["num_particles"].asUInt64();
		mass_ = js["mass"].ASFLTP();
		velocity_ = js["velocity"].ASFLTP();
		sigma_velocity_ = js["sigma_velocity"].ASFLTP();
		charge_ = js["charge"].ASFLTP();

		// position orientation
		R_ = Node::deserialize_matrix(js["position"]);
		L_ = Node::deserialize_matrix(js["longitudinal"]);
		N_ = Node::deserialize_matrix(js["normal"]);
		D_ = Node::deserialize_matrix(js["transverse"]);

		// beam
		// sigma_ = js["sigma"].ASFLTP();
		corr_xx_ = js["corr_xx"].ASFLTP();
		sigma_x_ = js["sigma_x"].ASFLTP();
		sigma_xa_ = js["sigma_xa"].ASFLTP();
		corr_yy_ = js["corr_yy"].ASFLTP();
		sigma_y_ = js["sigma_y"].ASFLTP();
		sigma_ya_ = js["sigma_ya"].ASFLTP();

		// tracking
		lifetime_ = js["lifetime"].asUInt64();
		start_idx_ = js["start_idx"].asUInt64();
	}


}}
