// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelcube.hh"

#include "rat/common/error.hh"

#include "crossrectangle.hh"
#include "pathaxis.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	ModelCube::ModelCube(){
		set_name("Cube");
	}

	// default constructor
	ModelCube::ModelCube(
		const fltp width,
		const fltp thickness,
		const fltp height,
		const fltp element_size) : ModelCube(){

		// set to self
		set_width(width);
		set_thickness(thickness);
		set_height(height);
		set_element_size(element_size);
	}

	// factory
	ShModelCubePr ModelCube::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<ModelCube>();
	}

	// factory
	ShModelCubePr ModelCube::create(
		const fltp width,
		const fltp thickness,
		const fltp height,
		const fltp element_size){
		return std::make_shared<ModelCube>(
			width, thickness, height, element_size);
	}

	// get base
	ShPathPr ModelCube::get_input_path() const{
		// check input
		is_valid(true);

		// create axis
		const ShPathAxisPr axis = PathAxis::create('z','x',height_,cmn::Extra::null_vec(),element_size_);

		// return the racetrack
		return axis;
	}

	// get cross
	ShCrossPr ModelCube::get_input_cross() const{
		// check input
		is_valid(true);

		// create rectangular cross
		const ShCrossRectanglePr rectangle = CrossRectangle::create(
			-thickness_/2, thickness_/2, -width_/2, width_/2, element_size_);

		// return the rectangle
		return rectangle;
	}

	// set properties
	void ModelCube::set_width(const fltp width){
		width_ = width;
	}

	void ModelCube::set_thickness(const fltp thickness){
		thickness_ = thickness;
	}

	void ModelCube::set_height(const fltp height){
		height_ = height;
	}

	void ModelCube::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}


	// get properties
	fltp ModelCube::get_width()const{
		return width_;
	}

	fltp ModelCube::get_thickness()const{
		return thickness_;
	}

	fltp ModelCube::get_height()const{
		return height_;
	}

	fltp ModelCube::get_element_size()const{
		return element_size_;
	}


	// check input
	bool ModelCube::is_valid(const bool enable_throws) const{
		if(!ModelMeshWrapper::is_valid(enable_throws))return false;
		if(width_<=0){if(enable_throws){rat_throw_line("width must be larger than zero");} return false;};
		if(thickness_<=0){if(enable_throws){rat_throw_line("thickness must be larger than zero");} return false;};
		if(height_<=0){if(enable_throws){rat_throw_line("height must be larger than zero");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string ModelCube::get_type(){
		return "rat::mdl::modelcube";
	}

	// method for serialization into json
	void ModelCube::serialize(
		Json::Value &js, cmn::SList &list) const{

		// parent
		ModelMeshWrapper::serialize(js,list);

		// set type
		js["type"] = get_type();
		
		// set properties
		js["width"] = width_;
		js["height"] = height_;
		js["thickness"] = thickness_;
		js["element_size"] = element_size_;
	}

	// method for deserialisation from json
	void ModelCube::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		ModelMeshWrapper::deserialize(js,list,factory_list,pth);

		// get properties
		width_ = js["width"].ASFLTP();
		height_ = js["height"].ASFLTP();
		thickness_ = js["thickness"].ASFLTP();
		element_size_ = js["element_size"].ASFLTP();
	}

}}