// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathoffset.hh"

// rat-common headers
#include "rat/common/extra.hh"
#include "rat/common/error.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathOffset::PathOffset(){
		set_name("Offset");
	}

	// constructor with specification
	PathOffset::PathOffset(const ShPathPr &base, const fltp noff, const fltp doff, const fltp boff) : PathOffset(){
		set_input_path(base); set_noff(noff);
		set_doff(doff); set_boff(boff);
	}

	// factory
	ShPathOffsetPr PathOffset::create(){
		return std::make_shared<PathOffset>();
	}

	// constructor with dimensions and element size
	ShPathOffsetPr PathOffset::create(const ShPathPr &base, const fltp noff, const fltp doff, const fltp boff){
		return std::make_shared<PathOffset>(base,noff,doff,boff);
	}


	// set offset in normal direction
	void PathOffset::set_noff(const fltp noff){
		noff_ = noff;
	}

	// set offset in transverse direction
	void PathOffset::set_doff(const fltp doff){
		doff_ = doff;
	}

	// set offset in block direction
	void PathOffset::set_boff(const fltp boff){
		boff_ = boff;
	}

	// set offset in normal direction
	fltp PathOffset::get_noff()const{
		return noff_;
	}

	// set offset in transverse direction
	fltp PathOffset::get_doff()const{
		return doff_;
	}

	// set offset in block direction
	fltp PathOffset::get_boff()const{
		return boff_;
	}

	// get frame
	ShFramePr PathOffset::create_frame(const MeshSettings &stngs) const{
		// check if base path was set
		is_valid(true);

		// create frame
		const ShFramePr frame = input_path_->create_frame(stngs);

		// early out
		if(noff_==0 && boff_==0 && doff_==0)return frame;

		// copy coordinates
		arma::field<arma::Mat<fltp> > R = frame->get_coords();
		const arma::field<arma::Mat<fltp> >& L = frame->get_direction();
		arma::field<arma::Mat<fltp> > D = frame->get_transverse();
		const arma::field<arma::Mat<fltp> >& N = frame->get_normal();
		const arma::field<arma::Mat<fltp> >& B = frame->get_block();
		// const arma::Row<arma::uword> section = frame->get_section();
		// const arma::Row<arma::uword> turn = frame->get_turn();
		// const arma::uword num_section_base = frame->get_num_section_base();

		// move frame along N and D
		for(arma::uword i=0;i<R.n_cols;i++){
			// get shear angle
			const arma::Row<fltp> ashear = frame->calc_ashear(i);

			// apply offset
			if(frame->get_conductor_type()==Frame::DbType::block){
				R(i) += doff_*D(i) + B(i).each_row()%((boff_ + noff_)/arma::sin(ashear));
			}else{
				R(i) += noff_*N(i) + doff_*D(i) + B(i).each_row()%(boff_/arma::sin(ashear));
			}

			// after off-setting the darboux ribbon is not even fixed width anymore
			// this cheat fixes the width at least
			D(i) = cmn::Extra::normalize(D(i)).each_row()/
				arma::cos(cmn::Extra::vec_angle(cmn::Extra::cross(D(i),N(i)),L(i)));

			// check handedness
			assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N(i),L(i)),D(i))>RAT_CONST(0.0)));
		}

		
		// create offset frame
		const ShFramePr offset_frame = Frame::create(R,L,N,D,B);

		// conserve location
		offset_frame->set_location(
			frame->get_section(), 
			frame->get_turn(), 
			frame->get_num_section_base());

		// // conserve location
		// offset_frame->set_location(
		// 	section, turn, num_section_base);

		// apply_transformations
		offset_frame->apply_transformations(get_transformations(), stngs.time);

		// create new frame
		return offset_frame;
	}

	// re-index nodes after deleting
	void PathOffset::reindex(){
		InputPath::reindex(); Transformations::reindex();
	}

	// vallidity check
	bool PathOffset::is_valid(const bool enable_throws) const{
		if(!InputPath::is_valid(enable_throws))return false;
		if(!Transformations::is_valid(enable_throws))return false;
		return true;
	}

	// get type
	std::string PathOffset::get_type(){
		return "rat::mdl::pathoffset";
	}

	// method for serialization into json
	void PathOffset::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents
		Path::serialize(js,list);
		InputPath::serialize(js,list);
		Transformations::serialize(js,list);
		
		// type
		js["type"] = get_type();

		// properties
		js["noff"] = noff_;
		js["doff"] = doff_;
		js["boff"] = boff_;
	}

	// method for deserialisation from json
	void PathOffset::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		Path::deserialize(js,list,factory_list,pth);
		InputPath::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);
		set_noff(js["noff"].ASFLTP()); 
		set_doff(js["doff"].ASFLTP()); 
		set_boff(js["boff"].ASFLTP());
	}


}}