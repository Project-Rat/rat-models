// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "drivetrapz.hh"

// rat-common headers
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	DriveTrapz::DriveTrapz(){

	}

	// constructor
	DriveTrapz::DriveTrapz(const 
		fltp amplitude, 
		const fltp tstart, 
		const fltp ramprate, 
		const fltp tpulse, 
		const fltp offset, 
		const bool centered){
		
		set_amplitude(amplitude); 
		set_tstart(tstart); 
		set_ramprate(ramprate); 
		set_tpulse(tpulse); 
		set_offset(offset);	
		set_centered(centered);
		set_name("Trapezoid");
	}

	// factory
	ShDriveTrapzPr DriveTrapz::create(){
		return std::make_shared<DriveTrapz>();
	}

	// factory
	ShDriveTrapzPr DriveTrapz::create(
		const fltp amplitude, 
		const fltp tstart, 
		const fltp ramprate, 
		const fltp tpulse, 
		const fltp offset, 
		const bool centered){
		return std::make_shared<DriveTrapz>(amplitude,tstart,ramprate,tpulse,offset,centered);
	}

	// setters
	void DriveTrapz::set_centered(const bool centered){
		centered_ = centered;
	}

	void DriveTrapz::set_amplitude(const fltp amplitude){
		amplitude_ = amplitude;
	}

	void DriveTrapz::set_offset(const fltp offset){
		offset_ = offset;
	}

	void DriveTrapz::set_tstart(const fltp tstart){
		tstart_ = tstart;
	}

	void DriveTrapz::set_ramprate(const fltp ramprate){
		ramprate_ = ramprate;
	}

	void DriveTrapz::set_tpulse(const fltp tpulse){
		tpulse_ = tpulse;
	}

	

	// getters
	fltp DriveTrapz::get_offset() const{
		return offset_;
	}

	fltp DriveTrapz::get_amplitude() const{
		return amplitude_;
	}

	fltp DriveTrapz::get_tstart() const{
		return tstart_;
	}

	fltp DriveTrapz::get_ramprate() const{
		return ramprate_;
	}

	fltp DriveTrapz::get_tpulse() const{
		return tpulse_;
	}

	bool DriveTrapz::get_centered()const{
		return centered_;
	}

	// boundaries for making a plot
	fltp DriveTrapz::get_v1() const{
		return get_center_position() - RAT_CONST(1.1)*(tpulse_/2 + get_ramp_time());
	}
	
	fltp DriveTrapz::get_v2() const{
		return get_center_position() + RAT_CONST(1.1)*(tpulse_/2 + get_ramp_time());
	}

	// get ramping time
	fltp DriveTrapz::get_ramp_time()const{
		return std::abs(amplitude_)/std::abs(ramprate_);
	}

	// function that calculates the center position
	fltp DriveTrapz::get_center_position()const{
		return centered_ ? tstart_ : tstart_ + tpulse_/2 + (ramprate_!=0 ? get_ramp_time() : RAT_CONST(0.0));
	}

	// get edge position
	fltp DriveTrapz::get_edge_position()const{
		return centered_ ? tstart_ - tpulse_/2 - (ramprate_!=0 ? get_ramp_time() : RAT_CONST(0.0)) : tstart_;
	}

	// get scaling
	fltp DriveTrapz::get_scaling(
		const fltp position,
		const fltp /*time*/,
		const arma::uword derivative) const{
			
		// get position of center	
		const fltp position_offset = get_edge_position();
		const fltp tramp = get_ramp_time();

		// function value
		if(derivative==0){
			// block shaped pulse
			if(ramprate_==0){
				return (position-position_offset>0.0 && position-position_offset<tpulse_) ? amplitude_ + offset_ : offset_;
			}

			// trapzoidal shaped pulse
			else{
				const fltp value = offset_ + rat::cmn::Extra::sign(amplitude_)*
					std::max(RAT_CONST(0.0),std::min(std::abs(amplitude_),
					std::min(ramprate_*(position-position_offset-0.0),
					-ramprate_*(position-position_offset-0.0-tpulse_)+
					2*std::abs(amplitude_))));
				return value;
			}
		}

		// first derivative
		if(derivative==1){
			// for block
			if(ramprate_==0){
				return RAT_CONST(0.0);
			}

			// for trapzoidal
			else{
				fltp dsc = 0;
				if(position-position_offset>0.0 && position-position_offset<(tramp))
					dsc = rat::cmn::Extra::sign(amplitude_)*ramprate_;
				if(position-position_offset>tramp+tpulse_ && position-position_offset<2*tramp+tpulse_)
					dsc = -rat::cmn::Extra::sign(amplitude_)*ramprate_;
				return dsc;
			}
		}

		// higher derivatives are zero because only linear
		return 0.0;
	}

	// get positions at which an inflection occurs
	arma::Col<fltp> DriveTrapz::get_inflection_points() const{
		// for block
		if(ramprate_==0){
			return {tstart_,tstart_+tpulse_};
		}

		// for trapzoidal
		else{
			const fltp tramp = std::abs(amplitude_)/ramprate_;
			if(centered_)
				return {tstart_-tpulse_/2-tramp, tstart_-tpulse_/2, tstart_+tpulse_/2, tstart_+tpulse_/2+tramp};
			else
				return {tstart_, tstart_+tramp, tstart_+tramp+tpulse_, tstart_+2*tramp+tpulse_};
		}
	}

	// apply scaling for the input settings
	void DriveTrapz::rescale(const fltp scale_factor){
		amplitude_ *= scale_factor;
		ramprate_ *= scale_factor;
		offset_ *= scale_factor;
	}

	// get type
	std::string DriveTrapz::get_type(){
		return "rat::mdl::drivetrapz";
	}

	// method for serialization into json
	void DriveTrapz::serialize(Json::Value &js, cmn::SList &list) const{
		Drive::serialize(js,list);
		js["type"] = get_type();
		js["centered"] = centered_;
		js["amplitude"] = amplitude_;
		js["tstart"] = tstart_;
		js["ramprate"] = ramprate_;
		js["tpulse"] = tpulse_;
		js["offset"] = offset_;
	}

	// method for deserialisation from json
	void DriveTrapz::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		Drive::deserialize(js,list,factory_list,pth);
		set_centered(js["centered"].asBool());
		set_amplitude(js["amplitude"].ASFLTP());
		set_tstart(js["tstart"].ASFLTP());
		set_ramprate(js["ramprate"].ASFLTP());
		set_tpulse(js["tpulse"].ASFLTP());
		set_offset(js["offset"].ASFLTP());
	}

}}