// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "vtkpvdata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	VTKPvData::VTKPvData(){

	}

	// factory
	ShVTKPvDataPr VTKPvData::create(){
		return std::make_shared<VTKPvData>();
	}

	// data file
	void VTKPvData::add_file(
		const std::string &collection,
		const std::string &data_name, const fltp time, 
		const arma::uword part, boost::filesystem::path fpath){
		VTKFile file = {data_name,time,part,fpath};
		file_list_[collection].push_back(file);
	}

	// get filename extension
	std::string VTKPvData::get_filename_ext() const{
		return "pvd";
	}

	// check times
	void VTKPvData::write(const boost::filesystem::path output_dir, const cmn::ShLogPr& /*lg*/){
		// walk over collections
		for(auto it1 = file_list_.begin();it1!=file_list_.end();it1++){
			// collection name
			const std::string collection = (*it1).first;
			const std::list<VTKFile> file_list = (*it1).second;

			// // create pvd file for holding time information
			std::ofstream pvd_stream((output_dir/(collection + "." + get_filename_ext())).string());
			pvd_stream << "<VTKFile type=\"Collection\" version=\"0.1\" byte_order=\"LittleEndian\">\n";
			pvd_stream << "\t<Collection>\n";

			// walk over vtk objects
			for(auto it2 = file_list.begin();it2!=file_list.end();it2++){
				// get file
				VTKFile file = (*it2);

				// time
				pvd_stream << "\t\t<DataSet name=\"" << file.data_name;
				pvd_stream << "\" part=\"" << file.part;
				pvd_stream << "\" timestep=\"" << std::scientific << std::setprecision(12) << file.time; 
				pvd_stream << "\" file=\"" << file.fpath.string() << "\"/>\n";
			}

			// footer
			pvd_stream << "\t</Collection>\n";
			pvd_stream << "</VTKFile>";
			pvd_stream.close();
		}
	}

	// create filename
	boost::filesystem::path VTKPvData::create_fname(
		const std::string &prefix,
		const arma::uword time_idx, 
		const arma::uword part_idx){
		// create a name
		std::stringstream tname;
		tname << prefix;
		tname << "pt";
		tname << std::setfill('0');
		tname << std::setw(5) << part_idx;
		tname << "tm";
		tname << std::setw(5) << time_idx;
		return tname.str();
	}

}}