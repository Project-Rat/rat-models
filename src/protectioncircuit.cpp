// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "protectioncircuit.hh"

#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ProtectionCircuit::ProtectionCircuit() : Circuit(){
		set_name("Protection Circuit");
	}

	// factory
	ShProtectionCircuitPr ProtectionCircuit::create(){
		return std::make_shared<ProtectionCircuit>();
	}


	// setters
	void ProtectionCircuit::set_use_varistor(const bool use_varistor){
		use_varistor_ = use_varistor;
	}

	void ProtectionCircuit::set_protection_voltage(const fltp protection_voltage){
		protection_voltage_ = protection_voltage;
	}

	void ProtectionCircuit::set_protection_current(const fltp protection_current){
		protection_current_ = protection_current;
	}

	void ProtectionCircuit::set_detection_voltage(const fltp detection_voltage){
		detection_voltage_ = detection_voltage;
	}

	void ProtectionCircuit::set_detection_delay_time(const fltp detection_delay_time){
		detection_delay_time_ = detection_delay_time;
	}

	void ProtectionCircuit::set_switching_time(const fltp switching_time){
		switching_time_ = switching_time;
	}

	void ProtectionCircuit::set_metrosil_exponent(const fltp metrosil_exponent){
		metrosil_exponent_ = metrosil_exponent;
	}

	void ProtectionCircuit::set_heater_delay_time(const fltp heater_delay_time){
		heater_delay_time_ = heater_delay_time;
	}

	void ProtectionCircuit::set_fraction_hit(const fltp fraction_hit){
		fraction_hit_ = fraction_hit;
	}

	void ProtectionCircuit::set_is_rapid(const bool is_rapid){
		is_rapid_ = is_rapid;
	}

	void ProtectionCircuit::set_transverse_propagation_fraction(
		const fltp transverse_propagation_fraction){
		transverse_propagation_fraction_ = transverse_propagation_fraction;
	}



	// getters
	bool ProtectionCircuit::get_use_varistor()const{
		return use_varistor_;
	}

	fltp ProtectionCircuit::get_protection_voltage()const{
		return protection_voltage_;
	}

	fltp ProtectionCircuit::get_protection_current()const{
		return protection_current_;
	}

	fltp ProtectionCircuit::get_detection_voltage()const{
		return detection_voltage_;
	}

	fltp ProtectionCircuit::get_detection_delay_time()const{
		return detection_delay_time_;
	}

	fltp ProtectionCircuit::get_switching_time()const{
		return switching_time_;
	}

	fltp ProtectionCircuit::get_metrosil_exponent()const{
		return metrosil_exponent_;
	}

	fltp ProtectionCircuit::get_heater_delay_time()const{
		return heater_delay_time_;
	}

	fltp ProtectionCircuit::get_fraction_hit()const{
		return fraction_hit_;
	}

	bool ProtectionCircuit::get_is_rapid()const{
		return is_rapid_;
	}

	fltp ProtectionCircuit::get_transverse_propagation_fraction()const{
		return transverse_propagation_fraction_;
	}


	// calculate voltage
	fltp ProtectionCircuit::calc_protection_voltage(const fltp current)const{
		// varistor (such as metrosil)
		if(use_varistor_){
			// calculate the scaling factor for the metrosil unit
			const fltp metrosil_scaling = std::abs(protection_voltage_)/std::pow(std::abs(protection_current_), metrosil_exponent_);
			
			// calculate voltage
			return cmn::Extra::sign(current)*metrosil_scaling*std::pow(std::abs(current), metrosil_exponent_);
		}

		// regular resistor
		else{
			const fltp dump_resistance = std::abs(protection_voltage_)/std::abs(protection_current_);
			return dump_resistance*current;
		}
	}

	// validity check
	bool ProtectionCircuit::is_valid(const bool enable_throws) const{
		if(!Circuit::is_valid(enable_throws))return false;
		if(protection_voltage_<0){if(enable_throws){rat_throw_line("protection voltage must be positive");}; return false;};
		if(protection_current_<0){if(enable_throws){rat_throw_line("protection current must be positive");}; return false;};
		if(detection_delay_time_<0){if(enable_throws){rat_throw_line("detection delay time must be positive");}; return false;};
		if(switching_time_<0){if(enable_throws){rat_throw_line("switch delay time must be positive");}; return false;};
		if(use_varistor_)if(metrosil_exponent_<=0){if(enable_throws){rat_throw_line("metrosil exponent time must be positive");}; return false;};
		if(transverse_propagation_fraction_<0){if(enable_throws){rat_throw_line("transverse propagation can not be negative");}; return false;};
		return true;
	}

	// get type
	std::string ProtectionCircuit::get_type(){
		return "rat::mdl::protectioncircuit";
	}

	// method for serialization into json
	void ProtectionCircuit::serialize(Json::Value &js, cmn::SList &list) const{
		Circuit::serialize(js,list);
		js["type"] = get_type();
		js["use_varistor"] = use_varistor_;
		js["is_rapid"] = is_rapid_;
		js["detection_voltage"] = detection_voltage_;
		js["protection_voltage"] = protection_voltage_;
		js["protection_current"] = protection_current_;
		js["protection_voltage"] = protection_voltage_;
		js["detection_delay_time"] = detection_delay_time_;
		js["switching_time"] = switching_time_;
		js["metrosil_exponent"] = metrosil_exponent_;
		js["heater_delay_time"] = heater_delay_time_;
		js["fraction_hit"] = fraction_hit_;
		js["transverse_propagation_fraction"] = transverse_propagation_fraction_;
	}

	// method for deserialisation from json
	void ProtectionCircuit::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		Circuit::deserialize(js,list,factory_list,pth);
		set_use_varistor(js["use_varistor"].asBool());
		set_is_rapid(js["is_rapid"].asBool());
		set_protection_voltage(js["protection_voltage"].ASFLTP());
		set_protection_current(js["protection_current"].ASFLTP());
		set_protection_voltage(js["protection_voltage"].ASFLTP());
		set_detection_delay_time(js["detection_delay_time"].ASFLTP());
		set_switching_time(js["switching_time"].ASFLTP());
		set_metrosil_exponent(js["metrosil_exponent"].ASFLTP());
		set_heater_delay_time(js["heater_delay_time"].ASFLTP());
		set_fraction_hit(js["fraction_hit"].ASFLTP());
		set_detection_voltage(js["detection_voltage"].ASFLTP());
		if(js.isMember("transverse_propagation_fraction"))
			set_transverse_propagation_fraction(js["transverse_propagation_fraction"].ASFLTP());
	}


}}