// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelimport.hh"
#include "drivedc.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ModelImport::ModelImport(){
		temperature_drive_ = DriveDC::create(); set_name("Import Mesh");
	}

	// factory
	ShModelImportPr ModelImport::create(){
		//return ShModelImportPr(new ModelImport);
		return std::make_shared<ModelImport>();
	}

	// set operating temperature 
	fltp ModelImport::get_operating_temperature() const{
		return operating_temperature_;
	}

	// set operating temperature 
	void ModelImport::set_operating_temperature(const fltp operating_temperature){
		operating_temperature_ = operating_temperature;
	}

	// set drive
	void ModelImport::set_temperature_drive(const ShDrivePr &temperature_drive){
		if(temperature_drive==NULL)rat_throw_line("supplied drive points to NULL");
		temperature_drive_ = temperature_drive;
	}

	// set input path
	void ModelImport::set_fpath(const boost::filesystem::path &fpath){
		fpath_ = fpath;
	}

	// get input path
	boost::filesystem::path ModelImport::get_fpath() const{
		return fpath_;
	}

	// set scaling factor
	void ModelImport::set_scaling_factor(const rat::fltp scaling_factor){
		scaling_factor_ = scaling_factor;
	}

	// set position
	void ModelImport::set_position(const arma::Col<fltp>::fixed<3> &R0){
		R0_ = R0;
	}

	// get scaling factor
	fltp ModelImport::get_scaling_factor()const{
		return scaling_factor_;
	}

	// get position
	arma::Col<fltp>::fixed<3> ModelImport::get_position() const{
		return R0_;
	}


	// factory for mesh objects
	std::list<ShMeshDataPr> ModelImport::create_meshes(
		const std::list<arma::uword> &trace, 
		const MeshSettings &stngs)const{

		// trace
		if(!trace.empty())rat_throw_line("trace must be empty for leaf");

		// check enabled
		if(!get_enable())return{};

		// check input
		if(!is_valid(stngs.enable_throws))return{};

		// check if the stl file exists
		boost::system::error_code ec;
		if(!boost::filesystem::exists(fpath_,ec))
			rat_throw_line("file does not exist: " + fpath_.string());

		// input file
		std::ifstream myFile(fpath_.c_str(), std::ios::in | std::ios::binary);

		// header
		unsigned int num_triangles;

		//read 80 byte header
		if(myFile.good()){
			char header_info[80] = "";
			myFile.read(header_info, 80);
		}else{
			rat_throw_line("file header not ok");
		}

		//read 4-byte ulong
		if(myFile.good()){
			char nTri[4];
			myFile.read (nTri, 4);
			std::memcpy(&num_triangles, nTri, sizeof(int));
		}else{
			rat_throw_line("file length not ok");
		}

		// allocate three coordinates
		arma::Mat<float> M(12,num_triangles); // VTK uses float

		//now read in all the triangles
		for(unsigned int i=0;i<num_triangles;i++){
			// check if file is still ok
			if(myFile.good()) {
				//read one 50-byte triangle
				char facet[50];
				myFile.read(facet, 50);

				// copy memory
				std::memcpy(M.memptr() + i*12, facet, 12*sizeof(float));
			}
		}	

		// close file
		myFile.close();

		// number of targets
		const arma::uword num_targets = 3*num_triangles;

		// get coordinate vectors
		arma::Mat<fltp> Rt = scaling_factor_*arma::reshape(arma::conv_to<arma::Mat<fltp> >::from(M.rows(3,11)),3,num_targets);

		// get normals
		const arma::Mat<fltp> Nf = arma::conv_to<arma::Mat<fltp> >::from(M.rows(0,2));

		// move
		Rt.each_col() += R0_;

		// create mesh with these vectors
		arma::Mat<arma::uword> n = arma::reshape(arma::regspace<arma::Col<arma::uword> >(0,num_targets-1),3,num_triangles);

		// calculate face normals
		const arma::Col<arma::uword> idx_flip = arma::find(cmn::Extra::dot(cmn::Triangle::calc_face_normals(Rt,n), Nf)<RAT_CONST(0.0));
		n.cols(idx_flip) = arma::flipud(n.cols(idx_flip));

		// combine nodes in a mesh
		const arma::Row<arma::uword> idx = cmn::Extra::combine_nodes(Rt);
		n = arma::reshape(idx(arma::vectorise(n)),n.n_rows,n.n_cols);

		// mesh data
		const ShMeshDataPr mesh_data = MeshData::create(Rt,n,n);

		// set time
		mesh_data->set_time(stngs.time);

		// set orientation
		mesh_data->set_longitudinal(arma::Mat<fltp>(3,Rt.n_cols,arma::fill::zeros));
		mesh_data->set_normal(arma::Mat<fltp>(3,Rt.n_cols,arma::fill::zeros));
		mesh_data->set_transverse(arma::Mat<fltp>(3,Rt.n_cols,arma::fill::zeros));
		mesh_data->set_temperature(operating_temperature_);

		// set color
		mesh_data->set_color(color_);
		mesh_data->set_use_custom_color(use_custom_color_);

		// set temperature
		mesh_data->set_operating_temperature(operating_temperature_);

		// set name (is appended by models later)
		mesh_data->set_name(myname_);
		mesh_data->set_part_name(myname_);

		// apply transformations to mesh
		mesh_data->apply_transformations(get_transformations(),stngs.time);

		// mesh data object
		return {mesh_data};
	}

	// check validity
	bool ModelImport::is_valid(const bool enable_throws) const{
		if(!enable_)return true;
		if(operating_temperature_<=0){if(enable_throws){rat_throw_line("operating temperature must be larger than zero");} return false;};
		boost::system::error_code ec;
		if(!boost::filesystem::exists(fpath_,ec)){if(enable_throws){rat_throw_line("filepath does not exist");} return false;};
		return true;
	}

	// get type
	std::string ModelImport::get_type(){
		return "rat::mdl::modelimport";
	}

	// method for serialization into json
	void ModelImport::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		Model::serialize(js,list);

		// type
		js["type"] = get_type();

		// temperature and drive
		js["temperature_drive"] = cmn::Node::serialize_node(temperature_drive_, list);
		js["operating_temperature"] = operating_temperature_;
		
		// file location
		js["fpath"] = fpath_.string();

		// coordinate
		js["Rx"] = R0_(0); js["Ry"] = R0_(1); js["Rz"] = R0_(2);

		// scaling
		js["scaling_factor"] = scaling_factor_;

	}

	// method for deserialisation from json
	void ModelImport::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent objects
		Model::deserialize(js,list,factory_list,pth);

		// temperature and drive
		set_temperature_drive(cmn::Node::deserialize_node<Drive>(
			js["temperature_drive"], list, factory_list, pth));
		set_operating_temperature(js["operating_temperature"].ASFLTP());

		// get path
		boost::filesystem::path filepath(
			Node::fix_filesep(js["fpath"].asString()));

		// check if path exists
		boost::system::error_code ec;
		if(!boost::filesystem::exists(filepath,ec)){
			if(boost::filesystem::exists(pth/filepath.filename()))
				filepath = pth/filepath.filename();
		}

		// file location
		set_fpath(filepath);

		// coordinate
		R0_(0) = js["Rx"].ASFLTP(); 
		R0_(1) = js["Ry"].ASFLTP(); 
		R0_(2) = js["Rz"].ASFLTP(); 

		// scaling
		scaling_factor_ = js["scaling_factor"].ASFLTP();
	}

}}