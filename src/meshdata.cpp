// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "meshdata.hh"

#include "rat/common/elements.hh"
#include "rat/common/extra.hh"

#include "coildata.hh"
#include "bardata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	MeshData::MeshData(){
		set_name("data");
		set_output_type("mesh");
	}

	// constructor with input
	MeshData::MeshData(
		const ShFramePr &frame, 
		const ShAreaPr &area, 
		const bool use_parallel) : MeshData(){
		setup(frame, area, use_parallel);
	}

	// manual mesh construction
	MeshData::MeshData(
		const arma::Mat<fltp> &R, 
		const arma::Mat<arma::uword> &n, 
		const arma::Mat<arma::uword> &s) : MeshData(){
		setup(R,n,s);
	}

	// factory
	ShMeshDataPr MeshData::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<MeshData>();
	}

	// factory with input
	ShMeshDataPr MeshData::create(
		const ShFramePr &frame, 
		const ShAreaPr &area,
		const bool use_parallel){
		return std::make_shared<MeshData>(frame,area,use_parallel);
	}

	// factory with input
	ShMeshDataPr MeshData::create(
		const arma::Mat<fltp> &R, 
		const arma::Mat<arma::uword> &n, 
		const arma::Mat<arma::uword> &s){
		return std::make_shared<MeshData>(R,n,s);
	}

	// set part name
	void MeshData::set_part_name(const std::string& part_name){
		part_name_ = part_name;
	}

	// get part name
	const std::string& MeshData::get_part_name()const{
		return part_name_;
	}

	// set enable current sharing
	void MeshData::set_enable_current_sharing(const bool enable_current_sharing){
		enable_current_sharing_ = enable_current_sharing;
	}

	// get enable current sharing
	bool MeshData::get_enable_current_sharing()const{
		return enable_current_sharing_;
	}

	// set circuit index
	void MeshData::set_circuit_index(const arma::uword circuit_index){
		circuit_index_ = circuit_index;
	}

	// get circuit index
	arma::uword MeshData::get_circuit_index()const{
		return circuit_index_;
	}

	// set the color
	void MeshData::set_color(const arma::Col<float>::fixed<3> &color){
		color_ = color;
	}

	void MeshData::set_use_custom_color(const bool use_custom_color){
		use_custom_color_ = use_custom_color;
	}

	// append id
	void MeshData::append_trace_id(const arma::uword trace_id){
		trace_.push_front(trace_id);
	}

	// clear id
	void MeshData::clear_trace_id(){
		trace_.clear();
	}

	// set trace directly
	void MeshData::set_trace(const std::list<arma::uword>& trace){
		trace_ = trace;
	}

	// set number of gauss points
	void MeshData::set_num_gauss(
		const arma::sword num_gauss_volume, 
		const arma::sword num_gauss_surface){
		set_num_gauss_volume(num_gauss_volume);
		set_num_gauss_surface(num_gauss_surface);
	}

	// set softening factor
	void MeshData::set_softening(const fltp softening){
		softening_ = softening;
	}

	// set number of gauss points in the surface
	void MeshData::set_num_gauss_surface(const arma::sword num_gauss_surface){
		num_gauss_surface_ = num_gauss_surface;
	}

	// set number of gauss points in the volume
	void MeshData::set_num_gauss_volume(const arma::sword num_gauss_volume){
		num_gauss_volume_ = num_gauss_volume;
	}

	// get number of gauss points in surface
	arma::sword MeshData::get_num_gauss_surface()const{
		return num_gauss_surface_;
	}

	// get number of gauss points in volume
	arma::sword MeshData::get_num_gauss_volume()const{
		return num_gauss_volume_;
	}

	arma::uword MeshData::get_num_gauss_per_volume_element()const{
		return n_.n_rows==8 ? 
			cmn::Hexahedron::create_gauss_points(num_gauss_volume_).n_cols : 
			cmn::Tetrahedron::create_gauss_points(num_gauss_volume_).n_cols;
	}

	arma::uword MeshData::get_num_gauss_per_surface_element()const{
		return n_.n_rows==8 ? 
			cmn::Quadrilateral::create_gauss_points(num_gauss_surface_).n_cols : 
			cmn::Triangle::create_gauss_points(num_gauss_surface_).n_cols;
	}


	// get id
	const std::list<arma::uword>& MeshData::get_trace()const{
		return trace_;
	}

	// set calculation mesh flag
	void MeshData::set_calc_mesh(const bool calc_mesh){
		calc_mesh_ = calc_mesh;
	}
	
	// get calculation mesh flag
	bool MeshData::get_calc_mesh() const{
		return calc_mesh_;
	}

	// get the color
	const arma::Col<float>::fixed<3>& MeshData::get_color()const{
		return color_;
	}

	bool MeshData::get_use_custom_color()const{
		return use_custom_color_;
	}

	// set node by node temperature
	void MeshData::set_temperature(const fltp temperature){
		temperature_.set_size(R_.n_cols); 
		temperature_.fill(temperature);
	}

	// set nodal temperature
	void MeshData::set_temperature(const arma::Row<fltp> &temperature){
		assert(temperature.n_cols==R_.n_cols);
		temperature_ = temperature;
	}

	// get operating temperature
	const arma::Row<fltp>& MeshData::get_temperature() const{
		return temperature_;
	}

	
	// setup surface mesh
	void MeshData::setup_surface(const ShSurfaceDataPr &surf) const{
		// get counters
		const arma::uword num_nodes = get_num_nodes();

		// find indexes of unique nodes
		const arma::Row<arma::uword> idx_node_surface = 
			arma::unique<arma::Row<arma::uword> >(arma::reshape(s_,1,s_.n_elem));

		// figure out which nodes to drop as 
		// they are no longer contained in the mesh
		arma::Row<arma::uword> node_indexing(1,num_nodes,arma::fill::zeros);
		node_indexing.cols(idx_node_surface) = 
			arma::regspace<arma::Row<arma::uword> >(0,idx_node_surface.n_elem-1);

		// re-index connectivity
		const arma::Mat<arma::uword> s = arma::reshape(
			node_indexing(arma::reshape(s_,1,s_.n_elem)),s_.n_rows,s_.n_cols);

		// get node coordinates
		//const arma::Mat<fltp> R = R_.cols(idx_node_surface);

		// surface field calculation
		surf->set_mesh(R_.cols(idx_node_surface),s);
		surf->set_longitudinal(L_.cols(idx_node_surface));
		surf->set_normal(N_.cols(idx_node_surface));
		surf->set_transverse(D_.cols(idx_node_surface));

		// transfer name
		surf->set_name(get_name());

		// clear the field type list of the surface
		surf->clear_field_type();

		// transfer field
		if(has_field()){
			// transfer list of field types
			for(auto it = field_type_.begin();it!=field_type_.end();it++)
				surf->add_field_type((*it).first, (*it).second);
			
			// allocate surface to take the data
			surf->allocate();

			// transfer the data
			for(auto it = field_type_.begin();it!=field_type_.end();it++)
				surf->add_field((*it).first, get_field((*it).first).cols(idx_node_surface));
		}

		// set temperature
		surf->set_operating_temperature(operating_temperature_);
		if(!temperature_.empty())
			surf->set_temperature(temperature_.cols(idx_node_surface));

		// color
		surf->set_use_custom_color(use_custom_color_);
		surf->set_color(color_);
	}

	// extract surface mesh
	ShSurfaceDataPr MeshData::create_surface() const{
		// create coil surface
		ShSurfaceDataPr surf = SurfaceData::create();
		
		// regular setupp
		setup_surface(surf);

		// return surface
		return surf;
	}


	// create an interpolation mesh
	fmm::ShInterpPr MeshData::create_interp() const{
		fmm::ShInterpPr interp = fmm::Interp::create();
		interp->set_mesh(R_,n_);
		interp->set_interpolation_values('H',get_field('H'));
		return interp;
	}

	// VTK export
	ShVTKObjPr MeshData::export_vtk() const{

		// create unstructured mesh
		const ShVTKUnstrPr vtk = VTKUnstr::create();

		// get element type
		const arma::uword element_type = VTKUnstr::get_element_type(n_.n_rows, n_dim_);

		// set the mesh
		vtk->set_mesh(R_, n_, element_type);
		
		// tranfer name
		vtk->set_name(get_name());

		// add temperature to VTK
		if(!temperature_.empty())vtk->set_nodedata(temperature_,"Temperature [K]");
		else vtk->set_nodedata(operating_temperature_*
			arma::Row<fltp>(get_num_nodes(),arma::fill::ones),"Temperature [K]");

		// orientation vectors
		if(!L_.empty())vtk->set_nodedata(L_,"Longitudinal"); 
		if(!N_.empty())vtk->set_nodedata(N_,"Normal"); 
		if(!D_.empty())vtk->set_nodedata(D_,"Transverse"); 
		if(!L_.empty() && !N_.empty())vtk->set_nodedata(calc_binormal(),"Binormal"); 
		
		// add calculated vector potential
		if(has('A') && has_field())vtk->set_nodedata(get_field('A'),"Vector Potential [Vs/m]");

		// add calculated magnetic flux density
		if(has('B') && has_field())vtk->set_nodedata(get_field('B'),"Mgn. Flux Density [T]");

		// add calculated magnetisation
		if(has('M') && has_field())vtk->set_nodedata(get_field('M'),"Magnetisation [A/m]");

		// add calculated magnetic field
		if(has('H') && has_field())vtk->set_nodedata(get_field('H'),"Magnetic Field [A/m]");

		// return the unstructured mesh
		return vtk;
	}

	// set operating temperature
	void MeshData::set_operating_temperature(const fltp operating_temperature){
		operating_temperature_ = operating_temperature;
	}

	// get operating temperature
	fltp MeshData::get_operating_temperature() const{
		return operating_temperature_;
	}

	// get number of turns
	fltp MeshData::get_number_turns() const{
		return RAT_CONST(0.0);
	}

	// get number of turns
	fltp MeshData::get_num_turns() const{
		return get_number_turns();
	}

	// get operating current
	fltp MeshData::get_operating_current() const{
		return RAT_CONST(0.0);
	}

	// calculate enclosed flux
	fltp MeshData::calculate_flux() const{
		return RAT_CONST(0.0);
	}

	// calculate enclosed flux
	fltp MeshData::calculate_magnetic_energy() const{
		return RAT_CONST(0.0);
	}

	// // calculate self inductance of elements
	// fltp MeshData::calculate_element_self_inductance() const{
	// 	return RAT_CONST(0.0);
	// }

	// calculate MFOM in 2D based on cross section at ell/2
	fltp MeshData::calculate_mfom_2d(fltp bore_length) const{
		rat_throw_line("needs reimplement");
		
		// check area
		if(!is_extruded_)rat_throw_line("cross sectional area must be constant along mesh");

		// R_ are the nodes of my mesh
		// n_ are the elements of my mesh
		// But since I want the 2D version, I should ask for the elements and nodes from the area only
		// const arma::Mat<fltp>& d2_rt = get_base_elements();
		arma::Mat<arma::uword> d2_n = get_base_mesh();
		
		// allocate
		arma::Mat<fltp> d2_N; 
		arma::Mat<fltp> d2_D;

		// Now if have my 2D nodes and elements, now I need the field on the nodes
		// First get the field of all the nodes
		arma::Mat<fltp> b_field_total = get_field('B');
		if(!b_field_total.is_finite())rat_throw_line("field not finite");

		// Now get the field of just my 2D nodes:
		// First figure out the number of elements in my 2D area:
		arma::uword number_of_rt_2d = num_base_nodes_;
		arma::uword number_of_n_2d = num_base_elements_;
		// Now check how many "areas" fit in the 3D mesh (number of plane in the 3D object)
		arma::uword number_of_areas = R_.n_cols / number_of_rt_2d;
		// If the number of planes in the 3D object is even, we need to 
		// interpolate, otherwise we can just get the field of the one in the center
		
		// areas of intersections
		arma::Row<fltp> cis_areas = calc_cis_areas(); // this is actually all areas of all intersections, but this shouldn't matter as we are using the first cross section only

		// interpolate
		arma::Mat<fltp> b_field_2d ;
		arma::Row<fltp> d2_element_areas;
		const arma::uword area_index = std::floor(number_of_areas/2);
		if (number_of_areas%2 != 0)	{		// odd
			const arma::uword idx1 = number_of_rt_2d * area_index;
			const arma::uword idx2 = number_of_rt_2d * (area_index+1) - 1;
			b_field_2d = b_field_total.cols(idx1, idx2);
			d2_N = N_.cols(idx1, idx2);
			d2_D = D_.cols(idx1, idx2);
			d2_element_areas = cis_areas.cols(idx1,idx2);
		} else {							// even
			arma::uword idx1 = (area_index+0) * number_of_rt_2d;
			arma::uword idx2 = (area_index+1) * number_of_rt_2d - 1;
			arma::uword idx3 = (area_index+1) * number_of_rt_2d;
			arma::uword idx4 = (area_index+2) * number_of_rt_2d - 1;
			b_field_2d = (b_field_total.cols(idx1, idx2) + b_field_total.cols(idx3, idx4))/2;
			d2_N = (N_.cols(idx1, idx2) + N_.cols(idx3, idx4))/2;
			d2_D = (D_.cols(idx1, idx2) + D_.cols(idx1, idx2))/2;
			d2_element_areas = (cis_areas.cols(idx1,idx2) + cis_areas.cols(idx3,idx4))/2;
		}

		// Now we need to calculate the in plane component of the field for each node (B_inplane = sqrt((N dot B)^2 + (D dot B)^2))
		arma::Row<fltp> b_in_plane = arma::sqrt( 
			arma::square(rat::cmn::Extra::dot(d2_N, b_field_2d)) + 
			arma::square(rat::cmn::Extra::dot(d2_D, b_field_2d)) );
		if(!b_in_plane.is_finite())rat_throw_line("in plane field not finite");

		// Now I have the Bfield in 2D that is a matrix, I can calculate the average field for each node
		// So I loop over the elements, each column is an elements, and it contains the indexes of the nodes
		// By defenition there are 4 nodes per element
		fltp mfom2d = 0;
		// Get the areas that we need for the integration later:
		for (arma::uword i = 0; i<number_of_n_2d; i++) {			// index i is the index of the element
			fltp element_b_field = arma::as_scalar(arma::mean( b_in_plane.cols(d2_n.col(i)), 1));
			mfom2d += element_b_field * element_b_field * d2_element_areas(i);
		}

		// multiply with L squared
		if (bore_length==0){
			const fltp ell_frame = arma::accu(get_ell_frame());
			mfom2d *= ell_frame * ell_frame;
		}else{
			mfom2d *= bore_length * bore_length;
		}

		// return mfom
		return mfom2d;
	}

	// calculate MFOM in 3D
	fltp MeshData::calculate_mfom_3d() const{
		// check area
		if(!is_extruded_)rat_throw_line("cross sectional area must be constant along mesh");

		// R_ are the nodes of my mesh
		// n_ are the elements of my mesh
		// But since I want the 3D version, R_ and n_ are already the elements and the node that I need
		// So I can go right away to asking the field for every element and node
		arma::Mat<fltp> b_field_total = get_field('B');
		if(!b_field_total.is_finite())rat_throw_line("field not finite");

		// Regardless of which element we take, we always need the in plane component, so we calculate that first
		// Now we need to calculate the in plane component of the field for each node (B_inplane = sqrt((N dot B)^2 + (D dot B)^2))
		arma::Row<fltp> b_in_plane = arma::sqrt( 
			arma::square(rat::cmn::Extra::dot(N_, b_field_total)) + 
			arma::square(rat::cmn::Extra::dot(D_, b_field_total)) );
		if(!b_in_plane.is_finite())rat_throw_line("in plane field not finite");
		// b_in_plane is on each of the corners of the element still, so later we still need to average !!!

		// We need to get the number of elements in a plane, because that is the amount of line we need to integrate over
		// And then we need the number of planes, because that is the number of points in a line :)
		arma::uword number_of_lines = num_base_nodes_; // Number of lines that we need to do the area intergration over
		arma::uword number_of_points = R_.n_cols / number_of_lines; // Number of points in a path 

		// Now we need to get the B-field on each point in the line
		// The b_in_plane is a row vector, which we can reshape
		// such that each row is one line
		arma::Mat<fltp> b_lines_matrix = arma::reshape(b_in_plane.t(), number_of_lines, number_of_points);

		// Now we need to get the lenght of the line elements
		// We calculate this, because then the user can have an arched path
		// First we reorganise all the coordinates such that each row has the coordinates of a line
		arma::Mat<fltp> x_lines_matrix = arma::reshape(R_.row(0).t(), number_of_lines, number_of_points);
		arma::Mat<fltp> y_lines_matrix = arma::reshape(R_.row(1).t(), number_of_lines, number_of_points);
		arma::Mat<fltp> z_lines_matrix = arma::reshape(R_.row(2).t(), number_of_lines, number_of_points);

		// Then we calculate the differces between each point in a line, and take the root of the square sum for the length matrix
		arma::Mat<fltp> line_length_matrix = arma::sqrt( arma::square(arma::diff(x_lines_matrix, 1, 1)) + 
												arma::square(arma::diff(y_lines_matrix, 1, 1)) +
												arma::square(arma::diff(z_lines_matrix, 1, 1)) );

		// Now we need to interpolate the b-field on each node of each line to the b-field on the center of each line element
		arma::Mat<fltp> b_lines_interpolated_matrix = (b_lines_matrix.tail_cols(number_of_points-1) + b_lines_matrix.head_cols(number_of_points-1)) / 2 ;

		// Finally we can multiply the field of each element by the length of each line element, and integrate
		// This integral need to be squared
		arma::Row<fltp> line_integrals = arma::square(arma::sum(b_lines_interpolated_matrix % line_length_matrix, 1)).t();   // [T^2m^2]

		// Now that we have the line integrals, we can do the area integration
		// For this we need a for loop, and we loop over elements in the 2D area
		// For that we need to get the 2D elements so that we can select the correct columns that belong to an element
		// The we average again
		const arma::Mat<arma::uword>& d2_n = get_base_mesh();
		arma::uword number_of_elements = d2_n.n_cols;
		arma::Row<fltp> b_element(number_of_elements);
		for(arma::uword i = 0; i<number_of_elements; i++)
			b_element(i) = arma::as_scalar(arma::mean(line_integrals.cols(d2_n.col(i)), 1));

		// Then we only have to multiply by the area and do the last integration
		arma::Row<fltp> d2_element_areas = calc_cis_areas();
		fltp mfom3d = arma::accu(b_element%d2_element_areas); // [T^2m^4]

		return mfom3d;
	}


	// export to a csv file
	void MeshData::export_gmsh(const cmn::ShGmshFilePr &gmsh, const arma::uword idx) const{
		gmsh->write_nodes(R_);
		gmsh->write_elements(n_,arma::Row<arma::uword>(n_.n_cols, arma::fill::value(idx)));
	}

	// calculate Lorentz forces
	arma::Col<fltp>::fixed<3> MeshData::calc_lorentz_force() const{
		return cmn::Extra::null_vec();
	}

	// calculate force density
	arma::Mat<fltp> MeshData::calc_force_density() const{
		return arma::Mat<fltp>(3,R_.n_cols,arma::fill::zeros);
	}

	// export edges to freecad
	void MeshData::export_freecad(const cmn::ShFreeCADPr &freecad) const{
		// allocate
		arma::field<arma::Mat<fltp> > x,y,z; arma::uword num_edges; 
		const bool full_perimeter = q_.n_elem>4; // four edges will be sharp always
		create_xyz(x,y,z,num_edges,full_perimeter);

		// write to freecad
		if(use_custom_color_)freecad->write_edges(x,y,z,section_,turn_,get_name(),color_); 
		else freecad->write_edges(x,y,z,section_,turn_,get_name());
	}

	// export mesh to abaqus format
	void MeshData::export_abaqus(const cmn::ShAbaqusFilePr &abaqus) const{
		const std::string material_name = material_!=NULL ? material_->get_name() : "none";
		abaqus->write_part(get_name(), R_, n_, material_name);
	}

	// export edges to freecad
	void MeshData::export_opera(const cmn::ShOperaPr &opera) const{
		// can not convert non-extruded meshes to opera
		// because the cross section is not constant
		if(!is_extruded_)return;

		// allocate and get edge matrices
		arma::field<arma::Mat<fltp> > x,y,z; arma::uword num_edges; 
		create_xyz(x,y,z,num_edges);

		// calculate current density
		const fltp J = get_number_turns()*get_operating_current()/base_area_;

		// write edges with current density
		if(num_edges==4)opera->write_edges(x,y,z,J);
	}


	// display function
	void MeshData::display(const cmn::ShLogPr &lg, const std::list<ShMeshDataPr> &meshes){
		// header
		lg->msg(2,"%slist of coils%s\n",KBLU,KNRM);

		// count number of meshes
		const arma::uword num_coils = meshes.size();

		// allocate counters
		arma::Row<arma::uword> num_nodes(num_coils);
		arma::Row<arma::uword> num_elements(num_coils);
		arma::Row<arma::uword> num_surface(num_coils);

		// create table with stats for each coil
		lg->msg("%s%3s %12s %8s %8s %8s%s\n",KBLD,"idx","name","#node","#elem","#surf",KNRM);
		arma::uword idx = 0;
		for(auto it=meshes.begin();it!=meshes.end();it++,idx++){
			// get mesh
			ShMeshDataPr mesh = (*it);

			// get coil name
			std::string name = mesh->get_name();
			if(name.length()>12)name = name.substr(0,9) + "...";

			// get counters
			num_nodes(idx) = mesh->get_num_nodes();
			num_elements(idx) = mesh->get_num_elements();
			num_surface(idx) = mesh->get_num_surface();

			// print entr
			lg->msg("%03llu %12s %08llu %08llu %08llu\n",idx,name.c_str(),num_nodes(idx),num_elements(idx),num_surface(idx));
		}
		lg->msg("%03s %12s %08llu %08llu %08llu\n","+++","all",
			arma::sum(num_nodes),arma::sum(num_elements),arma::sum(num_surface));

		// footer
		lg->msg(-2);
	}

	// calculate incident angle of the magnetic field
	arma::Row<fltp> MeshData::calc_magnetic_field_angle(const FieldAngleType type)const{
		// get counters
		const arma::uword num_nodes = R_.n_cols;

		// check if field available
		if(!has('H') || !has_field())return arma::Row<fltp>(num_nodes, arma::fill::zeros);

		// get magnetic flux density at nodes
		const arma::Mat<fltp> magnetic_field = get_field('B');

		// calculate components along L, N and D
		const arma::Row<fltp> Blong = cmn::Extra::dot(magnetic_field,L_);
		const arma::Row<fltp> Bnorm = cmn::Extra::dot(magnetic_field,N_);
		const arma::Row<fltp> Bbinorm = cmn::Extra::dot(magnetic_field,calc_binormal());

		// calculate angle in radians and return
		arma::Row<fltp> magnetic_field_angle;
		if(type==FieldAngleType::CONE)magnetic_field_angle = arma::atan2(arma::hypot(Blong,Bbinorm),Bnorm);
		else if(type==FieldAngleType::BINORMAL)magnetic_field_angle = arma::atan2(arma::abs(Bbinorm), Bnorm);
		else if(type==FieldAngleType::LONGITUDINAL)magnetic_field_angle = arma::atan2(arma::abs(Blong), Bnorm);
		else if(type==FieldAngleType::PLANE)magnetic_field_angle = arma::atan2(arma::abs(Bbinorm), Blong);
		else rat_throw_line("field angle type not recognized");

		// field angle
		return magnetic_field_angle;
	}

	// calculate the critical current density
	arma::Row<fltp> MeshData::calc_critical_current_density(
		const bool /*use_current_sharing*/, 
		const FieldAngleType /*angle_type*/, 
		const bool /*use_parallel*/)const{
		return arma::Row<fltp>(get_num_nodes(), arma::fill::zeros);
	}
	
	// calculate the current density vector at the nodes
	arma::Mat<fltp> MeshData::calc_nodal_current_density()const{
		return arma::Mat<fltp>(3, get_num_nodes(), arma::fill::zeros);
	}

	// calculaet the critical temperature
	arma::Row<fltp> MeshData::calc_critical_temperature(
		const bool /*use_current_sharing*/, 
		const FieldAngleType /*angle_type*/, 
		const bool /*use_parallel*/) const{
		return arma::Row<fltp>(get_num_nodes(), arma::fill::zeros);
	}

	// calculate the temperature margin
	arma::Row<fltp> MeshData::calc_temperature_margin(
		const bool /*use_current_sharing*/, 
		const FieldAngleType /*angle_type*/, 
		const bool /*use_parallel*/) const{
		return arma::Row<fltp>(get_num_nodes(), arma::fill::zeros);
	}

	// calculate the electric field
	arma::Mat<fltp> MeshData::calc_electric_field(
		const bool /*use_current_sharing*/, 
		const FieldAngleType /*angle_type*/, 
		const bool /*use_parallel*/) const{
		return arma::Mat<fltp>(3, get_num_nodes(), arma::fill::zeros);
	}

	// calculate the voltage
	arma::Row<fltp> MeshData::calc_voltage(
		const bool /*use_current_sharing*/, 
		const FieldAngleType /*angle_type*/, 
		const bool /*use_parallel*/) const{
		return arma::Row<fltp>(get_num_nodes(), arma::fill::zeros);
	}

	// calculate the electrical power density 
	arma::Row<fltp> MeshData::calc_power_density(
		const bool /*use_current_sharing*/, 
		const FieldAngleType /*angle_type*/, 
		const bool /*use_parallel*/) const{
		return arma::Row<fltp>(get_num_nodes(), arma::fill::zeros);
	}

	// calculate the fraction on the loadline
	arma::Row<fltp> MeshData::calc_loadline_fraction(
		const bool /*use_current_sharing*/, 
		const FieldAngleType /*angle_type*/, 
		const bool /*use_parallel*/) const{
		return arma::Row<fltp>(get_num_nodes(), arma::fill::zeros);
	}

	// calculate the fraction of the critical current
	arma::Row<fltp> MeshData::calc_critical_current_fraction(
		const bool /*use_current_sharing*/, 
		const FieldAngleType /*angle_type*/, 
		const bool /*use_parallel*/) const{
		return arma::Row<fltp>(get_num_nodes(), arma::fill::zeros);
	}

	// calculate pressure vectors
	arma::Mat<fltp> MeshData::calc_pressure_vector()const{
		return N_.each_row()%calc_pressure(0) + D_.each_row()%calc_pressure(1);
	}
	
	// calculate pressure in normal direction
	arma::Row<fltp> MeshData::calc_normal_pressure()const{
		return calc_pressure(0);
	}

	// calculate pressure in normal direction
	arma::Row<fltp> MeshData::calc_transverse_pressure()const{
		return calc_pressure(1);
	}

	// calculate the pressure in the given direction
	arma::Row<fltp> MeshData::calc_pressure(
		const arma::uword /*int_dir*/, 
		const bool /*use_parallel*/)const{
		return arma::Row<fltp>(get_num_nodes(), arma::fill::zeros);
	}

	// calculate the von-mises pressure
	arma::Row<fltp> MeshData::calc_von_mises()const{
		const arma::Row<fltp> Pt = calc_transverse_pressure();
		const arma::Row<fltp> Pn = calc_normal_pressure();
		return arma::sqrt(arma::square(Pt) - Pt%Pn + arma::square(Pn));
	}

	// relative permeability
	arma::Row<fltp> MeshData::calc_relative_permeability()const{
		return arma::Row<fltp>(get_num_nodes(), arma::fill::ones);
	}

	arma::Row<fltp> MeshData::calc_relative_reluctivity()const{
		return RAT_CONST(1.0)/calc_relative_permeability();
	}

	// setup sources and targets
	void MeshData::setup_targets(){
		Rt_ = R_; num_targets_ = R_.n_cols;
	}

}}