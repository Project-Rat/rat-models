// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// only available for non-linear solver
#ifdef ENABLE_NL_SOLVER

// include header
#include "modelnlsphere.hh"

// rat models headers
#include "nldata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ModelNLSphere::ModelNLSphere(
		const ShHBCurvePr& hb_curve){
		set_name("Sphere HB-Mesh");
		set_hb_curve(hb_curve);
		set_color({0.0,62.0/255,55.0/255},true);
	}

	// factory immediately setting base and cross section
	ShModelNLSpherePr ModelNLSphere::create(
		const ShHBCurvePr& hb_curve){
		return std::make_shared<ModelNLSphere>(hb_curve);
	}

	// setters
	void ModelNLSphere::set_softening(const fltp softening){
		softening_ = softening;
	}	

	fltp ModelNLSphere::get_softening()const{
		return softening_;
	}

	// set number of gauss points
	void ModelNLSphere::set_num_gauss_volume(const arma::sword num_gauss_volume){
		num_gauss_volume_ = num_gauss_volume;
	}

	// set number of gauss points
	void ModelNLSphere::set_num_gauss_surface(const arma::sword num_gauss_surface){
		num_gauss_surface_ = num_gauss_surface;
	}

	// get number of gauss points in surface
	arma::sword ModelNLSphere::get_num_gauss_surface()const{
		return num_gauss_surface_;
	}

	// get number of gauss points in volume
	arma::sword ModelNLSphere::get_num_gauss_volume()const{
		return num_gauss_volume_;
	}

	// factory for mesh objects
	std::list<ShMeshDataPr> ModelNLSphere::create_meshes(
		const std::list<arma::uword> &trace, 
		const MeshSettings &stngs)const{

		// trace
		if(!trace.empty())rat_throw_line("trace must be empty for leaf");

		// check enabled
		if(!get_enable())return{};

		// check model
		if(!is_valid(stngs.enable_throws))return{};

		// create meshes
		ShNLDataPr nlmesh = NLData::create();
		
		// create spherical mesh
		setup_mesh(nlmesh, stngs);

		// copy properties
		if(hb_curve_!=NULL)if(hb_curve_->is_valid(false))nlmesh->set_hb_data(hb_curve_->create_hb_data());
		nlmesh->set_num_gauss(num_gauss_volume_, num_gauss_surface_);
		nlmesh->set_softening(softening_);
		
		// color
		if(use_custom_color_){
			nlmesh->set_use_custom_color(use_custom_color_);
			nlmesh->set_color(color_);
		}

		// return mesh data
		return {nlmesh};
	}

	// check validity
	bool ModelNLSphere::is_valid(const bool enable_throws) const{
		// check mesh
		if(!ModelSphere::is_valid(enable_throws))return false;
		return true;
	}

	// get type
	std::string ModelNLSphere::get_type(){
		return "rat::mdl::modelnlsphere";
	}

	// method for serialization into json
	void ModelNLSphere::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents first 
		// this allows the type to be overridden
		ModelSphere::serialize(js,list);

		// properties
		js["type"] = get_type();

		// serialize hb curve
		js["hb_curve"] = cmn::Node::serialize_node(hb_curve_, list);

		// number of gauss points
		js["num_gauss_volume"] = static_cast<int>(num_gauss_volume_);
		js["num_gauss_surface"] = static_cast<int>(num_gauss_surface_);

		// softening factor
		js["softening"] = softening_;
	}

	// method for deserialisation from json
	void ModelNLSphere::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent objects
		ModelSphere::deserialize(js,list,factory_list,pth);

		// deserialize hb-curve
		hb_curve_ = cmn::Node::deserialize_node<HBCurve>(js["hb_curve"], list, factory_list, pth);

		// number of gauss points
		if(js.isMember("num_gauss_volume"))set_num_gauss_volume(js["num_gauss_volume"].asInt64());
		if(js.isMember("num_gauss_surface"))set_num_gauss_surface(js["num_gauss_surface"].asInt64());

		// softening factor
		if(js.isMember("softening"))set_softening(js["softening"].ASFLTP());
	}

}}

#endif