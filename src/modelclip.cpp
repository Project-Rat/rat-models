// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelclip.hh"

// common headers
#include "rat/common/parfor.hh"
#include "rat/common/error.hh"
#include "rat/common/marchingcubes.hh"

// model headers
#include "coildata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	ModelClip::ModelClip(){
		set_name("Clip");
	}

	// constructor with setting of the base path
	ModelClip::ModelClip(const ShModelPr &base_model, const arma::Col<fltp>::fixed<3> &vector, const fltp offset) : ModelClip(){
		add_model(base_model); set_vector(vector); set_offset(offset);
	}

	// constructor with setting of the base path
	ModelClip::ModelClip(const std::list<ShModelPr> &models, const arma::Col<fltp>::fixed<3> &vector, const fltp offset) : ModelClip(){
		for(auto it=models.begin();it!=models.end();it++)add_model(*it);
		set_vector(vector); set_offset(offset);
	}

	// factory
	ShModelClipPr ModelClip::create(){
		return std::make_shared<ModelClip>();
	}

	// factory with setting of the base path
	ShModelClipPr ModelClip::create(const ShModelPr &base_model, const arma::Col<fltp>::fixed<3> &vector, const fltp offset){
		return std::make_shared<ModelClip>(base_model,vector,offset);
	}

	// factory with setting of the base path
	ShModelClipPr ModelClip::create(const std::list<ShModelPr> &models, const arma::Col<fltp>::fixed<3> &vector, const fltp offset){
		return std::make_shared<ModelClip>(models,vector,offset);
	}


	// setters
	void ModelClip::set_vector(const arma::Col<fltp>::fixed<3>& vector){
		vector_ = vector;
	}

	void ModelClip::set_offset(const fltp offset){
		offset_ = offset;
	}

	void ModelClip::set_velocity(const fltp velocity){
		velocity_ = velocity;
	}

	void ModelClip::set_invert(const bool invert){
		invert_ = invert;
	}


	// getters
	const arma::Col<fltp>::fixed<3>& ModelClip::get_vector()const{
		return vector_;
	}
	
	fltp ModelClip::get_offset()const{
		return offset_;
	}

	fltp ModelClip::get_velocity()const{
		return velocity_;
	}

	bool ModelClip::get_invert()const{
		return invert_;
	}


	// mesh generation
	std::list<ShMeshDataPr> ModelClip::create_meshes_core(
		const std::list<arma::uword> &trace, 
		const MeshSettings &stngs)const{

		// check enabled
		if(!get_enable())return{};

		// check validity
		if(!is_valid(stngs.enable_throws))return{};

		// create meshes
		const std::list<ShMeshDataPr> meshes = ModelGroup::create_meshes_core(trace, stngs);	

		// for calculation
		if(stngs.is_calculation)return meshes;

		// convert list to field
		arma::uword idx = 0;
		const arma::uword num_meshes = meshes.size();
		arma::field<mdl::ShMeshDataPr> meshes_fld(num_meshes);
		for(auto it=meshes.begin();it!=meshes.end();it++,idx++)meshes_fld(idx) = (*it);

		// allolcate output
		arma::field<mdl::ShMeshDataPr> meshes_clipped(num_meshes);

		// walk over meshes
		cmn::parfor(0,num_meshes,stngs.use_parallel_groups,[&](int k, int /*cpu*/){

			// try to cast to coilmesh
			const ShMeshDataPr& meshdata = meshes_fld(k);
			const ShCoilDataPr coildata = std::dynamic_pointer_cast<CoilData>(meshdata);

			// get mesh
			const arma::Mat<fltp>& Rt = meshdata->get_nodes();
			const arma::Mat<fltp>& L = meshdata->get_longitudinal();
			const arma::Mat<fltp>& N = meshdata->get_normal();
			const arma::Mat<fltp>& D = meshdata->get_transverse();
			const arma::Mat<fltp>& T = meshdata->get_temperature();
			const arma::Mat<arma::uword>& s = meshdata->get_surface_elements();
			const arma::Mat<arma::uword>& h = meshdata->get_elements();
			
			// check 
			assert(Rt.is_finite());
			assert(Rt.n_rows==3); assert(L.n_rows==3); assert(N.n_rows==3); assert(D.n_rows==3);
			assert(L.n_cols==Rt.n_cols); assert(N.n_cols==Rt.n_cols); assert(D.n_cols==Rt.n_cols); 
			assert(T.n_elem==Rt.n_cols);
			assert(!s.empty()); assert(!h.empty());


			// combine data
			arma::Mat<fltp> data(13,Rt.n_cols);
			data.rows(0,2) = Rt; data.rows(3,5) = L; data.rows(6,8) = N; data.rows(9,11) = D; data.row(12) = T;

			// dimensionality
			arma::uword n_dim = meshdata->get_n_dim();
			arma::uword s_dim = meshdata->get_s_dim();

			// settings
			const fltp offset = (invert_ ? -1 : 1)*(offset_ + stngs.time*velocity_);

			// create marching cube tables
			const cmn::ShMarchingCubesPr marching = cmn::MarchingCubes::create();

			// normalize vector
			const arma::Col<fltp>::fixed<3> vector = vector_/arma::as_scalar(cmn::Extra::vec_norm(vector_));

			// clip value
			const arma::Row<fltp> clip_value = 
				(invert_ ? -1 : 1)*(
				Rt.row(0)*vector(0) + 
				Rt.row(1)*vector(1) + 
				Rt.row(2)*vector(2));

			// allocate output
			arma::Mat<fltp> datac;
			arma::Mat<arma::uword> nc;

			// volume mesh
			if(n_dim==3 && s_dim==2){

				// quadrilaterals
				if(s.n_rows==4)marching->setup_quad2tri(); 

				// triangles
				else if(s.n_rows==3)marching->setup_tri2tri(); 

				// unknown element
				else rat_throw_line("element data has unexpected number of rows");

				// perform marching cubes on surface elements
				const arma::Mat<fltp> data1 = marching->polygonise(data,clip_value,s,offset);

				// hexahedrons
				if(h.n_rows==8)marching->setup_hex2isotri();

				// tetrahedrons
				else if(h.n_rows==4)marching->setup_tet2isotri();

				// unknown element
				else rat_throw_line("element data has unexpected number of rows");

				// perform marching cubes on volume elements
				const arma::Mat<fltp> data2 = marching->polygonise(data,clip_value,h,offset);

				// recombine
				datac = arma::join_horiz(data1,data2); 

				// create element matrix
				nc = arma::reshape(arma::regspace<arma::Col<arma::uword> >(0,datac.n_cols-1),3,datac.n_cols/3);
			}

			// surface clipping 
			else if(n_dim==2 && s_dim==2){
				// clip surface
				// quadrilaterals
				if(s.n_rows==4)marching->setup_quad2tri(); 

				// triangles
				else if(s.n_rows==3)marching->setup_tri2tri(); 

				// unknown element
				else rat_throw_line("element data has unexpected number of rows");

				// perform marching cubes on volume elements
				datac = marching->polygonise(data,clip_value,s,offset);

				// create element matrix
				nc = arma::reshape(arma::regspace<arma::Col<arma::uword> >(0,datac.n_cols-1),3,datac.n_cols/2);
			}

			// line2line clipping
			else if(n_dim==1 && s_dim==1){
				marching->setup_line2line();
				datac = marching->polygonise(data,clip_value,h,offset);
				nc = arma::reshape(arma::regspace<arma::Col<arma::uword> >(0,datac.n_cols-1),2,datac.n_cols/2);
			}

			// point to point clipping
			else if(n_dim==0){
				marching->setup_point2point();
				datac = marching->polygonise(Rt,clip_value,h,offset);
				nc = arma::regspace<arma::Row<arma::uword> >(0,data.n_cols-1);
			}

			// check if none left
			if(data.empty() || nc.empty())return; // this is actually "continue"

			// find triangles with no area and drop them from the list
			if(nc.n_rows==3){
				const arma::Mat<fltp> Rc = datac.rows(0,2);
				const arma::Mat<fltp> V0 = Rc.cols(nc.row(1)) - Rc.cols(nc.row(0));
				const arma::Mat<fltp> V1 = Rc.cols(nc.row(2)) - Rc.cols(nc.row(0));
				const arma::Mat<fltp> Nf = cmn::Extra::cross(V0,V1);
				nc.shed_cols(arma::find(cmn::Extra::vec_norm(Nf)<1e-14));
			}

			if(nc.empty())return; // this is actually "continue"

			// get coordinates
			assert(datac.n_rows>=13);
			arma::Mat<fltp> Rc = datac.rows(0,2);

			// combine nodes
			const arma::Row<arma::uword> idx = cmn::Extra::combine_nodes(Rc,datac);
			nc = arma::reshape(idx(arma::vectorise(nc)),nc.n_rows,nc.n_cols);
			assert(datac.n_cols==Rc.n_cols);

			// get orientation
			arma::Mat<fltp> Lc = datac.rows(3,5); 
			arma::Mat<fltp> Nc = datac.rows(6,8); 
			arma::Mat<fltp> Dc = datac.rows(9,11);

			// renormalize
			Lc.each_row()/=cmn::Extra::vec_norm(Lc);
			Nc.each_row()/=cmn::Extra::vec_norm(Nc);
			Dc.each_row()/=cmn::Extra::vec_norm(Dc);

			// get temperature
			const arma::Row<fltp> Tc = datac.row(12);

			// set clipped mesh
			meshes_clipped(k) = meshdata; // reuse old meshdata
			meshes_clipped(k)->set_mesh(Rc,nc,nc);

			// set orientation
			meshes_clipped(k)->set_longitudinal(Lc);
			meshes_clipped(k)->set_normal(Nc);
			meshes_clipped(k)->set_transverse(Dc);

			// set temperature
			meshes_clipped(k)->set_temperature(Tc);

			// transfer properties
			// meshes_clipped(k)->set_color(meshdata->get_color());
			// meshes_clipped(k)->set_use_custom_color(meshdata->get_use_custom_color());
			// meshes_clipped(k)->set_operating_temperature(meshdata->get_operating_temperature());
			// meshes_clipped(k)->set_trace(meshdata->get_trace());
			// meshes_clipped(k)->set_material(meshdata->get_material());
		});

		// convert to list and return
		std::list<ShMeshDataPr> meshes_list;
		for(arma::uword i=0;i<num_meshes;i++)
			if(meshes_clipped(i)!=NULL)
				meshes_list.push_back(meshes_clipped(i));

		// return meshes
		return meshes_list;
	}

	// check validity
	bool ModelClip::is_valid(const bool enable_throws) const{
		if(std::abs(arma::as_scalar(cmn::Extra::vec_norm(vector_)))<1e-14){if(enable_throws){rat_throw_line("vector must have nonzero length");} return false;};
		return true;
	}

	// get type
	std::string ModelClip::get_type(){
		return "rat::mdl::modelclip";
	}

	// method for serialization into json
	void ModelClip::serialize(Json::Value &js, cmn::SList &list) const{
		ModelGroup::serialize(js,list);
		js["type"] = get_type();
		js["vector"]["x"] = vector_(0);
		js["vector"]["y"] = vector_(1);
		js["vector"]["z"] = vector_(2);
		js["offset"] = offset_;
		js["invert"] = invert_;
		js["velocity"] = velocity_;
	}	

	// method for deserialisation from json
	void ModelClip::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent objects
		ModelGroup::deserialize(js,list,factory_list,pth);
		vector_(0) = js["vector"]["x"].ASFLTP();
		vector_(1) = js["vector"]["y"].ASFLTP();
		vector_(2) = js["vector"]["z"].ASFLTP();
		offset_ = js["offset"].ASFLTP();
		invert_ = js["invert"].ASFLTP();
		velocity_ = js["velocity"].ASFLTP();
	}

}}
