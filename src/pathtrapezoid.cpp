// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathtrapezoid.hh"

// rat-common headers
#include "rat/common/extra.hh"

// model headers
#include "patharc.hh"
#include "pathgroup.hh"
#include "pathstraight.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathTrapezoid::PathTrapezoid(){
		set_name("Trapezoid");
	}

	// constructor
	PathTrapezoid::PathTrapezoid(
		const fltp arc_radius, const fltp length_1,
		const fltp length_2, const fltp width,
		const fltp element_size, const fltp offset) : PathTrapezoid(){

		// set to self
		set_arc_radius(arc_radius);
		set_length_1(length_1);
		set_length_2(length_2);
		set_width(width);
		set_element_size(element_size);
		set_offset(offset);
	}

	// factory
	ShPathTrapezoidPr PathTrapezoid::create(){
		return std::make_shared<PathTrapezoid>();
	}

	// factory
	ShPathTrapezoidPr PathTrapezoid::create(
		const fltp arc_radius, const fltp length_1,
		const fltp length_2, const fltp width,
		const fltp element_size, const fltp offset){
		return std::make_shared<PathTrapezoid>(
			arc_radius,length_1,length_2,
			width,element_size,offset);
	}

	// set arc radius
	void PathTrapezoid::set_arc_radius(const fltp arc_radius){
		arc_radius_ = arc_radius;
	}

	// set length1
	void PathTrapezoid::set_length_1(const fltp length_1){
		length_1_ = length_1;
	}

	// set length2
	void PathTrapezoid::set_length_2(const fltp length_2){
		length_2_ = length_2;
	}

		// set width
	void PathTrapezoid::set_width(const fltp width){
		width_ = width;
	}

	// set axial element size
	void PathTrapezoid::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	// path offset
	void PathTrapezoid::set_offset(const fltp offset){
		offset_ = offset;
	}


	// get arc radius
	fltp PathTrapezoid::get_arc_radius()const{
		return arc_radius_;
	}

	// get length 1
	fltp PathTrapezoid::get_length_1()const{
		return length_1_;
	}

	// get length 2
	fltp PathTrapezoid::get_length_2()const{
		return length_2_;
	}

	// get width
	fltp PathTrapezoid::get_width()const{
		return width_;
	}

	// get axial element size
	fltp PathTrapezoid::get_element_size()const {
		return element_size_;
	}

	// get offset
	fltp PathTrapezoid::get_offset() const{
		return offset_;
	}

	// get frame
	ShFramePr PathTrapezoid::create_frame(const MeshSettings &stngs) const{
		// check input
		is_valid(true);

		// Length 1 is always bigger than length 2
		// So alpha is always the smallest angle
		const fltp alpha = std::atan(width_/(std::abs(length_1_-length_2_)/2));
		const fltp beta = arma::Datum<fltp>::pi-alpha;

		// Calculate the lengths that need to be subtracted
		// from the total to compensate for the rounded corners
		const fltp length_alpha = arc_radius_ / std::tan(alpha/2);
		const fltp length_beta = arc_radius_ * std::tan(RAT_CONST(0.5)*arma::Datum<fltp>::pi - RAT_CONST(0.5)*beta);

		// Now calculate the lengths
		const fltp length_3 = std::sqrt(((length_1_ - length_2_)/2)*((length_1_ - length_2_)/2) + width_*width_);

		// create geometry
		const ShPathGroupPr path_group = PathGroup::create();
		path_group->add_path(PathStraight::create(length_2_/2 - (length_1_<length_2_ ? length_alpha : length_beta), element_size_));
		path_group->add_path(PathArc::create(arc_radius_, 
			arma::Datum<fltp>::pi - (length_1_<length_2_ ? alpha : beta), 
			element_size_, offset_));
		path_group->add_path(PathStraight::create(length_3 - length_alpha - length_beta, element_size_));
		path_group->add_path(PathArc::create(arc_radius_, 
			arma::Datum<fltp>::pi - (length_1_>length_2_ ? alpha : beta), 
			element_size_, offset_));
		path_group->add_path(PathStraight::create(length_1_/2 - (length_1_>length_2_ ? length_alpha : length_beta), element_size_));
		
		// set one-fold symmetry
		path_group->set_sym(2);

		// move start point
		path_group->set_start_coord({width_/2, RAT_CONST(0.0), RAT_CONST(0.0)});

		// transfer transformations
		path_group->add_transformations(get_transformations());

		// return frame
		return path_group->create_frame(stngs);
	}

	// validity check
	// if(width<=0)rat_throw_line("coil width must be larger than zero");
	// if(height<=0)rat_throw_line("coil height must be larger than zero");
	// if(radius<=0)rat_throw_line("coil radius must be larger than zero");
	// if(element_size<=0)rat_throw_line("element size must be larger than zero");
	bool PathTrapezoid::is_valid(const bool enable_throws) const{
		if(!Path::is_valid(enable_throws))return false;
		if(arc_radius_<=0){if(enable_throws){rat_throw_line("arc radius must be larger than zero");} return false;};
		if(length_1_<2*arc_radius_){if(enable_throws){rat_throw_line("first length must be larger than zero");} return false;};
		if(length_2_<2*arc_radius_){if(enable_throws){rat_throw_line("second length must be larger than zero");} return false;};
		if(width_<2*arc_radius_){if(enable_throws){rat_throw_line("width must be larger than zero");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		return true;
	}


	// get type
	std::string PathTrapezoid::get_type(){
		return "rat::mdl::pathtrapezoid";
	}

	// method for serialization into json
	void PathTrapezoid::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Path::serialize(js,list);
		Transformations::serialize(js,list);

		// type
		js["type"] = get_type();

		// properties
		js["arc_radius"] = arc_radius_;
		js["length_1"] = length_1_;
		js["length_2"] = length_2_;
		js["width"] = width_;
		js["element_size"] = element_size_;
		js["offset"] = offset_;
	}

	// method for deserialisation from json
	void PathTrapezoid::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		Path::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list, pth);
		
		// set properties
		set_arc_radius(js["arc_radius"].ASFLTP());
		set_length_1(js["length_1"].ASFLTP());
		set_length_2(js["length_2"].ASFLTP());
		set_width(js["width"].ASFLTP());	
		set_element_size(js["element_size"].ASFLTP());
		set_offset(js["offset"].ASFLTP());
	}

}}
