// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "crossgroup.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CrossGroup::CrossGroup(){
		set_name("Cross Group");
	}


	CrossGroup::CrossGroup(const std::list<ShCrossPr>& cross) : CrossGroup(){
		add_cross(cross);
	}

	// factory
	ShCrossGroupPr CrossGroup::create(){
		return std::make_shared<CrossGroup>();
	}

	ShCrossGroupPr CrossGroup::create(const std::list<ShCrossPr>& cross){
		return std::make_shared<CrossGroup>(cross);
	}

	void CrossGroup::set_merge_nodes(const bool merge_nodes){
		merge_nodes_ = merge_nodes;
	}

	bool CrossGroup::get_merge_nodes()const{
		return merge_nodes_;
	}

	// modify cross section list
	arma::uword CrossGroup::add_cross(const ShCrossPr &cross){
		const arma::uword index = cross_.size()+1;
		cross_.insert({index,cross}); return index;
	}

	arma::Row<arma::uword> CrossGroup::add_cross(const std::list<ShCrossPr> &cross){
		arma::Row<arma::uword> indices(cross.size()); arma::uword idx = 0;
		for(auto it = cross.begin();it!=cross.end();it++,idx++)indices(idx) = add_cross(*it);
		return indices;
	}
	
	const ShCrossPr& CrossGroup::get_cross(const arma::uword index) const{
		auto it = cross_.find(index);
		if(it==cross_.end())rat_throw_line("index does not exist");
		return (*it).second;
	}

	std::list<ShCrossPr> CrossGroup::get_cross() const{
		std::list<ShCrossPr> cross;
		for(auto it=cross_.begin();it!=cross_.end();it++)
			cross.push_back((*it).second);
		return cross;
	}

	bool CrossGroup::delete_cross(const arma::uword index){
		auto it = cross_.find(index);
		if(it==cross_.end())return false;
		(*it).second = NULL; return true;
	}

	void CrossGroup::reindex(){
		std::map<arma::uword, ShCrossPr> new_cross; arma::uword idx = 1;
		for(auto it=cross_.begin();it!=cross_.end();it++)
			if((*it).second!=NULL)new_cross.insert({idx++, (*it).second});
		cross_ = new_cross;
	}

	arma::uword CrossGroup::num_cross() const{
		return cross_.size();
	}

	// volume mesh
	ShAreaPr CrossGroup::create_area(const MeshSettings &stngs) const{
		// allocate output
		std::list<ShAreaPr> areas;

		// walk over cross sections
		for(auto it=cross_.begin();it!=cross_.end();it++)
			areas.push_back((*it).second->create_area(stngs));
		
		// combine areas
		const ShAreaPr area = Area::create(areas, merge_nodes_);
		area->setup_perimeter();
		area->setup_corner_nodes();

		// return the area
		return area;
	}

	// get bounding box
	arma::Mat<fltp>::fixed<2,2> CrossGroup::get_bounding() const{
		// check
		if(cross_.empty())return{0.0,0.0,0.0,0.0};

		// allocate output
		arma::Mat<fltp>::fixed<2,2> overall_bounds{
			arma::Datum<fltp>::inf,arma::Datum<fltp>::inf,
			-arma::Datum<fltp>::inf,-arma::Datum<fltp>::inf};

		// walk over cross sections
		for(auto it=cross_.begin();it!=cross_.end();it++){
			// get bounds of this cross section
			const arma::Mat<fltp>::fixed<2,2> bnds = (*it).second->get_bounding();

			// extend overall bounds
			overall_bounds.row(0) = arma::min(overall_bounds.row(0), bnds.row(0));
			overall_bounds.row(1) = arma::max(overall_bounds.row(1), bnds.row(1));
		}

		// return the overall bounds
		return overall_bounds;
	}

	// check validity
	bool CrossGroup::is_valid(const bool enable_throws) const{
		// check input calculations
		if(cross_.empty()){if(enable_throws){rat_throw_line("there are no cross sections to group");} return false;};
		for(auto it = cross_.begin();it!=cross_.end();it++){
			const ShCrossPr &cross = (*it).second;
			if(cross==NULL)rat_throw_line("crossgroup list contains NULL");
			if(!cross->is_valid(enable_throws))return false;
		}

		// no problem found
		return true;
	}


	// serialization
	std::string CrossGroup::get_type(){
		return "rat::mdl::crossgroup";
	}


	void CrossGroup::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		// parent
		Cross::serialize(js,list);

		// type
		js["type"] = get_type();

		// serialize the models
		for(auto it = cross_.begin();it!=cross_.end();it++)
			js["cross"].append(cmn::Node::serialize_node((*it).second, list));

		// merge nodes
		js["merge_nodes"] = merge_nodes_;
	}

	void CrossGroup::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		Cross::deserialize(js,list,factory_list,pth);

		// deserialize the models
		for(auto it = js["cross"].begin();it!=js["cross"].end();it++)
			add_cross(cmn::Node::deserialize_node<Cross>((*it), list, factory_list, pth));

		if(js.isMember("merge_nodes"))set_merge_nodes(js["merge_nodes"].asBool());
	}

}}