// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "coildata.hh"

// rat-common headers
#include "rat/common/extra.hh"
#include "rat/common/gauss.hh"
#include "rat/common/elements.hh"

// rat-mlfmm headers
#include "rat/mlfmm/currentsources.hh"
#include "rat/mlfmm/currentsurface.hh"
#include "rat/mlfmm/sourcecoil.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	CoilData::CoilData(){
		set_output_type("coilmesh");
	}

	// constructor with input
	CoilData::CoilData(
		const ShFramePr &frame, 
		const ShAreaPr &area) : CoilData(){
		setup(frame, area); num_gauss_volume_ = std::exp2(3-n_dim_); 
	}

	// constructor with input
	CoilData::CoilData(
		const ShFramePr &frame, 
		const ShAreaPr &area, 
		const mat::ShConductorPr& conductor) : CoilData(frame,area){
		set_conductor(conductor);
	}

	// factory
	ShCoilDataPr CoilData::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<CoilData>();
	}

	// factory with input
	ShCoilDataPr CoilData::create(
		const ShFramePr &frame, 
		const ShAreaPr &area){
		return std::make_shared<CoilData>(frame, area);
	}

	// factory with input
	ShCoilDataPr CoilData::create(
		const ShFramePr &frame, 
		const ShAreaPr &area, 
		const mat::ShConductorPr& conductor){
		return std::make_shared<CoilData>(frame, area, conductor);
	}

	// set the mesh material
	void CoilData::set_conductor(
		const rat::mat::ShConductorPr &conductor){
		set_material(conductor);
	}

	// extract surface mesh
	ShSurfaceDataPr CoilData::create_surface() const{
		// create coil surface
		ShCoilSurfaceDataPr surf = CoilSurfaceData::create();
		
		// regular setupp
		setup_surface(surf);

		// set properties
		surf->set_material(get_material());
		surf->set_operating_current(operating_current_);
		surf->set_number_turns(number_turns_);

		// return surface
		return surf;
	}

	// calculate current density for each cable intersection
	// note that this is normal to the cis
	arma::Row<fltp> CoilData::calc_cis_current_density()const{
		return number_turns_*operating_current_/calc_cis_total_area();
	}

	// calculate current density for each cable section
	arma::Row<fltp> CoilData::calc_cs_current_density()const{
		return number_turns_*operating_current_/calc_cs_total_area();
	}

	// calculate current density at nodes
	arma::Row<fltp> CoilData::calc_current_density(const arma::Row<fltp> &cis_current_density)const{
		// check input
		assert(cis_current_density.n_cols==num_cis_);

		// allocate
		arma::Row<fltp> current_density(num_nodes_);

		// walk over cable intersections
		for(arma::uword i=0;i<num_cis_;i++){
			// first cable intersection start and end index
			const arma::uword idx1 = i*num_base_nodes_;
			const arma::uword idx2 = (i+1)*num_base_nodes_-1;

			current_density.cols(idx1,idx2).fill(cis_current_density(i));
		}

		// current density
		return current_density;
	}

	// calculate current density at nodes
	arma::Row<fltp> CoilData::calc_current_density()const{
		return calc_current_density(calc_cis_current_density());
	}

	// calc nodal current density
	arma::Mat<fltp> CoilData::calc_nodal_current_density()const{
		assert(!L_.empty());
		assert(arma::all(arma::abs(cmn::Extra::vec_norm(L_)-RAT_CONST(1.0))<1e-9));
		return L_.each_row()%calc_current_density();
	}

	// // calculate current in line elements
	// arma::Row<fltp> CoilData::calc_element_current(
	// 	const arma::Row<fltp>& cis_area, 
	// 	const arma::Row<fltp>& cis_total_area)const{
	// 	// allocate
	// 	arma::Row<fltp> element_current(num_cs_elements_);
			
	// 	// total current in cross section
	// 	const fltp total_current = number_turns_*operating_current_;

	// 	// walk over cable sections
	// 	for(arma::uword i=0;i<num_cs_;i++){
	// 		// first cable intersection start and end index
	// 		const arma::uword idx1 = (i*num_base_elements_)%num_cs_elements_;
	// 		const arma::uword idx2 = ((i+1)*num_base_elements_-1)%num_cs_elements_;

	// 		// second cable intersection stgart and end index
	// 		const arma::uword idx3 = ((i+1)*num_base_elements_)%num_cs_elements_;
	// 		const arma::uword idx4 = ((i+2)*num_base_elements_-1)%num_cs_elements_;

	// 		// calculate element current
	// 		element_current.cols(idx1,idx2) = total_current*
	// 			(cis_area.cols(idx1,idx2)+cis_area.cols(idx3,idx4))/
	// 			(cis_total_area(i)+cis_total_area((i+1)%num_cs_));
	// 	}

	// 	// return element current
	// 	return element_current;	
	// }

	// create point elements
	void CoilData::create_point_sources(
		arma::Mat<fltp>& Rs,
		arma::Mat<fltp>& dRs,
		arma::Row<fltp>& epss,
		arma::Row<fltp>& Is)const{

		// volume elements
		if(n_dim_==3){
			// current for each hexahedron
			// we divide the total current 
			// based on the areas in the base mesh
			const arma::Row<fltp> scale = hidden_width_*hidden_thickness_*
				arma::repmat(number_turns_*cmn::Quadrilateral::calc_area(Rb_,b_)/base_area_,1,num_cs_);

			// flip mesh TODO check why needed?
			const arma::Mat<arma::uword> n = arma::join_vert(arma::flipud(n_.rows(0,3)), arma::flipud(n_.rows(4,7)));

			// gauss points
			const arma::Mat<fltp> gp = cmn::Hexahedron::create_gauss_points(num_gauss_volume_);
			const arma::Mat<fltp> Rqg = gp.rows(0,2); 
			const arma::Row<fltp> wg = gp.row(3);

			// derivative of shape function at gauss points
			const arma::Mat<fltp> dN = cmn::Hexahedron::shape_function_derivative(Rqg);

			// vector shape function for nedelec/raviart thomas elements
			const arma::Mat<fltp> Vq = cmn::Hexahedron::nedelec_hdiv_shape_function(Rqg);

			// only get faces 5 and 6, current is in mu direction
			const arma::Mat<fltp> Vq56 = Vq.rows(12,17); // [12-15] and [16-17]

			// allocate
			Rs.set_size(3,n_.n_cols*Rqg.n_cols);
			dRs.set_size(3,n_.n_cols*Rqg.n_cols);
			epss.set_size(n_.n_cols*Rqg.n_cols);
			Is.set_size(n_.n_cols*Rqg.n_cols);

			// walk over source nodes
			cmn::parfor(0,n.n_cols,true,[&](arma::uword i, arma::uword /*cpu*/) {
			// for(arma::uword i=0;i<n.n_cols;i++){
				// get element nodes
				const arma::Mat<fltp>::fixed<3,8> Rn = R_.cols(n.col(i));

				// integration points in cartesian coordinates
				Rs.cols(i*Rqg.n_cols,(i+1)*Rqg.n_cols-1) = cmn::Hexahedron::quad2cart(Rn,Rqg);

				// jacobian matrices at integration points stored column wise
				const arma::Mat<fltp> J = cmn::Hexahedron::shape_function_jacobian(Rn,dN);
				if(!J.is_finite())rat_throw_line("jacobian matrix contains non-finite values, i.e. malformed mesh");

				// determinant of jacobian matrices
				const arma::Row<fltp> Jdet = cmn::Hexahedron::jacobian2determinant(J);
				if(arma::any(Jdet<0))rat_throw_line("jacobian determinant less than zero, i.e. inverted elements detected");

				// piola transform to get carthesian vectors
				// note that the faces in RAT are slightly different from defelement website
				const arma::Mat<fltp> Vc = cmn::Hexahedron::quad2cart_contravariant_piola(Vq56,J,Jdet);
				const arma::Mat<fltp> Vc0123 = Vc.rows(0,2); // face 5
				const arma::Mat<fltp> Vc4567 = Vc.rows(3,5); // face 6

				// calculate current at integration points
				dRs.cols(i*Rqg.n_cols,(i+1)*Rqg.n_cols-1) = (Vc0123 - Vc4567).eval().each_row()%(scale(i)*wg%Jdet);
				epss.cols(i*Rqg.n_cols,(i+1)*Rqg.n_cols-1) = softening_*std::sqrt(2.0)*arma::cbrt((wg%Jdet)*(RAT_CONST(3.0)/4)/arma::Datum<fltp>::pi); // calculate from volume of sphere
			});

			// set current
			Is.fill(operating_current_);
		}

		// surface elements
		else if(n_dim_==2){
			// current for each hexahedron
			// we divide the total current 
			// based on the areas in the base mesh
			const arma::Row<fltp> scale = hidden_width_*hidden_thickness_*
				arma::repmat(number_turns_*(cmn::Line::calc_length(Rb_,b_)/base_area_),1,num_cs_); // in [A]

			// gauss points
			const arma::Mat<fltp> gp = cmn::Quadrilateral::create_gauss_points(num_gauss_volume_);
			const arma::Mat<fltp> Rqg = gp.rows(0,1); 
			const arma::Row<fltp> wg = gp.row(2);

			// derivative of shape function at gauss points
			const arma::Mat<fltp> dN = cmn::Quadrilateral::shape_function_derivative(Rqg);

			// vector shape function for nedelec/raviart thomas elements
			const arma::Mat<fltp> Vq = cmn::Quadrilateral::nedelec_hdiv_shape_function(Rqg);

			// only get edges 0 and 2, current is in nu direction, positive for 0 and negative for 2...
			const arma::Mat<fltp> Vq02 = Vq.rows(arma::Col<arma::uword>::fixed<4>{0,1,4,5}); 

			// allocate
			Rs.set_size(3,n_.n_cols*Rqg.n_cols);
			dRs.set_size(3,n_.n_cols*Rqg.n_cols);
			epss.set_size(n_.n_cols*Rqg.n_cols);
			Is.set_size(n_.n_cols*Rqg.n_cols);

			// walk over source nodes
			cmn::parfor(0,n_.n_cols,true,[&](arma::uword i, arma::uword /*cpu*/) {
				// get element nodes
				const arma::Mat<fltp>::fixed<3,4> Rn = R_.cols(n_.col(i));

				// convert in-plane vectors to 3D coordinates
				const arma::Mat<fltp>::fixed<3,2> M = cmn::Quadrilateral::surface_to_volume_matrix(Rn);

				// integration points in cartesian coordinates
				Rs.cols(i*Rqg.n_cols,(i+1)*Rqg.n_cols-1) = cmn::Quadrilateral::quad2cart(Rn,Rqg);

				// jacobian matrices at integration points stored column wise
				const arma::Mat<fltp> J = cmn::Quadrilateral::shape_function_jacobian(Rn,dN);
				if(!J.is_finite())rat_throw_line("jacobian matrix contains non-finite values, i.e. malformed mesh");

				// determinant of jacobian matrices
				const arma::Row<fltp> Jdet = cmn::Quadrilateral::jacobian2determinant(J);
				if(arma::any(Jdet<0))rat_throw_line("jacobian determinant less than zero, i.e. inverted elements detected");

				// piola transform to get carthesian vectors
				// note that the faces in RAT are slightly different from defelement website
				const arma::Mat<fltp> Vc = cmn::Quadrilateral::quad2cart_contravariant_piola(Vq02,J,Jdet);
				const arma::Mat<fltp> Vc01 = M*Vc.rows(0,1); // edge 0
				const arma::Mat<fltp> Vc23 = M*Vc.rows(2,3); // edge 2
				assert(Vc.is_finite());

				// calculate current at integration points
				dRs.cols(i*Rqg.n_cols,(i+1)*Rqg.n_cols-1) = (Vc01 - Vc23).eval().each_row()%(scale(i)*wg%Jdet);
				epss.cols(i*Rqg.n_cols,(i+1)*Rqg.n_cols-1) = softening_*std::sqrt(2.0)*arma::sqrt((wg%Jdet)/arma::Datum<fltp>::pi); // calculate from area of circle
			});

			// set current
			Is.fill(operating_current_);
		}

		// line elements
		else if(n_dim_==1){
			// each line element carries all the current
			const fltp scale = number_turns_; // in [A]

			// gauss points
			const arma::Mat<fltp> gp = cmn::Line::create_gauss_points(num_gauss_volume_);
			const arma::Mat<fltp> Rqg = gp.row(0); 
			const arma::Row<fltp> wg = gp.row(1);

			// derivative of shape function at gauss points
			const arma::Mat<fltp> dN = cmn::Line::shape_function_derivative(Rqg);

			// vector shape function for nedelec/raviart thomas elements
			// current is in xi direction, negative for 0 and positive for 1
			const arma::Mat<fltp> Vq = cmn::Line::nedelec_hdiv_shape_function(Rqg);

			// allocate
			Rs.set_size(3,n_.n_cols*Rqg.n_cols);
			dRs.set_size(3,n_.n_cols*Rqg.n_cols);
			epss.set_size(n_.n_cols*Rqg.n_cols);
			Is.set_size(n_.n_cols*Rqg.n_cols);

			// walk over source nodes
			cmn::parfor(0,n_.n_cols,true,[&](arma::uword i, arma::uword /*cpu*/){
				// get element nodes
				const arma::Mat<fltp>::fixed<3,2> Rn = R_.cols(n_.col(i));

				// convert in-plane vectors to 3D coordinates
				const arma::Col<fltp>::fixed<3> M = cmn::Line::surface_to_volume_matrix(Rn);

				// integration points in cartesian coordinates
				Rs.cols(i*Rqg.n_cols,(i+1)*Rqg.n_cols-1) = cmn::Line::quad2cart(Rn,Rqg);

				// jacobian matrices at integration points stored column wise
				const arma::Mat<fltp> J = cmn::Line::shape_function_jacobian(Rn,dN);
				if(!J.is_finite())rat_throw_line("jacobian matrix contains non-finite values, i.e. malformed mesh");

				// determinant of jacobian matrices
				const arma::Row<fltp> Jdet = cmn::Line::jacobian2determinant(J);
				if(arma::any(Jdet<0))rat_throw_line("jacobian determinant less than zero, i.e. inverted elements detected");

				// piola transform to get carthesian vectors
				// note that the faces in RAT are slightly different from defelement website
				const arma::Mat<fltp> Vc = cmn::Line::quad2cart_contravariant_piola(Vq,J,Jdet);
				const arma::Mat<fltp> Vc0 = M*Vc.row(0); // node 0
				const arma::Mat<fltp> Vc1 = M*Vc.row(1); // node 1
				assert(Vc.is_finite());

				// calculate current at integration points
				dRs.cols(i*Rqg.n_cols,(i+1)*Rqg.n_cols-1) = (Vc0 - Vc1).eval().each_row()%(scale*wg%Jdet);
			});

			// current in each source
			epss = arma::Row<fltp>(n_.n_cols*Rqg.n_cols,arma::fill::value(
				softening_*std::sqrt(2.0)*std::sqrt(hidden_width_*hidden_thickness_/arma::Datum<fltp>::pi))); // calculate from area of circle
			Is.fill(operating_current_);
		}

		// unknown element
		else rat_throw_line("Element type not recognized");

		// check output
		if(!Rs.is_finite())rat_throw_line("coordinates contain non-finite value");
		if(!dRs.is_finite())rat_throw_line("direction contain non-finite value");
		if(!Is.is_finite())rat_throw_line("current contain non-finite value");
		if(!epss.is_finite())rat_throw_line("softness contain non-finite value");
	}


	// // create line elements
	// void CoilData::create_line_elements(
	// 	arma::Mat<fltp>& Rs,
	// 	arma::Mat<fltp>& dRs,
	// 	arma::Row<fltp>& epss,
	// 	arma::Row<fltp>& Acrss,
	// 	arma::Row<fltp>& Is,
	// 	const arma::sword num_gauss)const{

	// 	// check input
	// 	assert(!n_.is_empty()); 
	// 	assert(!R_.is_empty());
	// 	assert(num_gauss_volume_!=0);
	// 	assert(num_cs_elements_==n_.n_cols);
	// 	assert(num_nodes_==R_.n_cols);

	// 	// setup gauss points
	// 	arma::Mat<fltp> Rqg; arma::Row<fltp> wg;
	// 	if(n_dim_ == 1){
	// 		Rqg.set_size(0,1);
	// 		wg = arma::Row<fltp>{1.0};
	// 	}else if(n_dim_ == 2){
	// 		const arma::Mat<fltp> gp = cmn::Line::create_gauss_points(num_gauss);
	// 		Rqg = gp.row(0); wg = gp.row(1);
	// 	}else if(n_dim_ == 3){
	// 		const arma::Mat<fltp> gp = cmn::Quadrilateral::create_gauss_points(num_gauss);
	// 		Rqg = gp.rows(0,1); wg = gp.row(2);
	// 	}

	// 	// hidden dimensions
	// 	const fltp Ahidden = calc_hidden_dim();

	// 	// calculate angle of cross section at nodes
	// 	// this is necessary because sometimes the cross sections are 
	// 	// not perpendicular to the cable
	// 	const arma::Row<fltp> scaling_factor = arma::cos(cmn::Extra::vec_angle(L_,cmn::Extra::cross(D_,N_)));
	// 	assert(scaling_factor.is_finite());

	// 	// calculate element currents
	// 	const arma::Row<fltp> cis_area = calc_cis_areas();
	// 	const arma::Row<fltp> cis_delem = calc_cis_delem();
	// 	const arma::Row<fltp> cis_total_area = calc_cis_total_area(cis_area);
	// 	const arma::Row<fltp> cs_total_area = calc_cs_total_area(cis_total_area);
	// 	// const arma::Row<fltp> element_current = calc_element_current(cis_area, cis_total_area);
	// 	const arma::Row<fltp> element_size = cis2cs(cis_delem);
	// 	const arma::Row<fltp> cs_area = cis2cs(cis_area);
	// 	// const arma::Row<fltp> Jcs = calc_cs_current_density();


	// 	// calculate number of sources
	// 	const arma::uword num_element_gs = Rqg.n_cols;
	// 	const arma::uword num_sources = num_cs_elements_*num_element_gs; // property of source points

	// 	// allocate source line elements
	// 	Rs.set_size(3,num_sources);
	// 	dRs.set_size(3,num_sources);
	// 	Acrss.set_size(num_sources);
	// 	epss.set_size(num_sources);
	// 	Is.set_size(num_sources);

	// 	// walk over source elements
	// 	// for(arma::uword i=0;i<num_cs_elements_;i++){
	// 	cmn::parfor(0,num_cs_elements_,true,[&](arma::uword i, int /*cpu*/) {
	// 		// get neighbouring 2D elements
	// 		const arma::Col<arma::uword> n1 = n_.submat(0,i,n_.n_rows/2-1,i);
	// 		const arma::Col<arma::uword> n2 = n_.submat(n_.n_rows/2,i,n_.n_rows-1,i);

	// 		// get nodes of these elements
	// 		const arma::Mat<fltp> Re1 = R_.cols(n1);
	// 		const arma::Mat<fltp> Re2 = R_.cols(n2);

	// 		// cross section angle at gauss points
	// 		const arma::Row<fltp> sc1 = scaling_factor.cols(n1);
	// 		const arma::Row<fltp> sc2 = scaling_factor.cols(n2);

	// 		// allocate 
	// 		arma::Mat<fltp> R1, R2; arma::Row<fltp> Jdet1, Jdet2;

	// 		// calculate element start and endpoints at intersections
	// 		if(n_dim_==1){
	// 			assert(Re1.n_cols==1); assert(Re2.n_cols==1);
	// 			R1 = Re1; R2 = Re2; 
	// 			Jdet1 = sc1; 
	// 			Jdet2 = sc2;
	// 		}else if(n_dim_==2){
	// 			R1 = cmn::Line::quad2cart(Re1,Rqg); 
	// 			R2 = cmn::Line::quad2cart(Re2,Rqg);
	// 			Jdet1 = cmn::Line::quad2cart(sc1,Rqg)%
	// 				cmn::Line::jacobian2determinant(
	// 				cmn::Line::shape_function_jacobian(
	// 				Re1,cmn::Line::shape_function_derivative(Rqg)));
	// 			Jdet2 = cmn::Line::quad2cart(sc2,Rqg)%
	// 				cmn::Line::jacobian2determinant(
	// 				cmn::Line::shape_function_jacobian(
	// 				Re2,cmn::Line::shape_function_derivative(Rqg)));
	// 		}else if(n_dim_==3){
	// 			R1 = cmn::Quadrilateral::quad2cart(Re1,Rqg); 
	// 			R2 = cmn::Quadrilateral::quad2cart(Re2,Rqg);
	// 			Jdet1 = cmn::Quadrilateral::quad2cart(sc1,Rqg)%
	// 				cmn::Quadrilateral::jacobian2determinant(
	// 				cmn::Quadrilateral::shape_function_jacobian(
	// 				Re1,cmn::Quadrilateral::shape_function_derivative(Rqg)));
	// 			Jdet2 = cmn::Quadrilateral::quad2cart(sc2,Rqg)%
	// 				cmn::Quadrilateral::jacobian2determinant(
	// 				cmn::Quadrilateral::shape_function_jacobian(
	// 				Re2,cmn::Quadrilateral::shape_function_derivative(Rqg)));
	// 		}

	// 		// check for inverted elements
	// 		if(arma::any(Jdet1<0))rat_throw_line("one or multiple elements are inverted");
	// 		if(arma::any(Jdet2<0))rat_throw_line("one or multiple elements are inverted");

	// 		// coordinate and direction vector
	// 		Rs.cols(i*num_element_gs,(i+1)*num_element_gs-1) = (R1 + R2)/2;
	// 		dRs.cols(i*num_element_gs,(i+1)*num_element_gs-1) = R2 - R1;

	// 		// set softening based on element size
	// 		// van Lanen kernel assumed
	// 		epss.cols(i*num_element_gs,(i+1)*num_element_gs-1).fill(
	// 			softening_*std::sqrt(RAT_CONST(2.0))*
	// 			element_size(i)/std::abs(num_gauss));

	// 		// set element cross sectional area
	// 		Acrss.cols(i*num_element_gs,(i+1)*num_element_gs-1) = Ahidden*wg%(Jdet1 + Jdet2)/2;

	// 		// current density at cross sections
	// 		const fltp J1 = (number_turns_*operating_current_)/cis_total_area(i/num_base_elements_);
	// 		const fltp J2 = (number_turns_*operating_current_)/cis_total_area((i/num_base_elements_+1)%num_cis_);

	// 		// set element cross sectional area
	// 		Is.cols(i*num_element_gs,(i+1)*num_element_gs-1) = Ahidden*wg%(J1*Jdet1 + J2*Jdet2)/2;
	// 	});

	// 	// check that each cross section has the correct total current
	// 	const fltp tolerance = RAT_CONST(1e-4);
	// 	for(arma::uword i=0;i<num_cs_;i++){
	// 		// std::cout<<std::abs(number_turns_*operating_current_ - arma::accu(Is.cols(
	// 		// 	i*num_base_elements_*Rqg.n_cols,(i+1)*num_base_elements_*Rqg.n_cols-1)))/
	// 		// 	(std::abs(number_turns_*operating_current_) + tolerance)<<std::endl;
	// 		if(std::abs(number_turns_*operating_current_ - arma::accu(Is.cols(
	// 			i*num_base_elements_*Rqg.n_cols,(i+1)*num_base_elements_*Rqg.n_cols-1)))/
	// 			(std::abs(number_turns_*operating_current_) + tolerance)>tolerance)
	// 			rat_throw_line("current in cross section does not match given current, likely your model is malformed (locally)");
	// 	}
	// }

	// create sources
	fmm::ShSourcesPr CoilData::create_currents()const{
		// experimental volume elements
		const bool prefer_volume_elements = false;

		// allocate output sources
		fmm::ShSourcesPr sources;

		// line sources
		if(n_dim_==1){
			const arma::Mat<fltp> Rs = (R_.cols(n_.row(0)) + R_.cols(n_.row(1)))/2;
			const arma::Mat<fltp> dRs = R_.cols(n_.row(1)) - R_.cols(n_.row(0));
			const arma::Row<fltp> Is = arma::Row<fltp>(Rs.n_cols,arma::fill::value(number_turns_*operating_current_));
			const arma::Row<fltp> epss = arma::Row<fltp>(Rs.n_cols,arma::fill::value(softening_*std::sqrt(2.0)*std::sqrt(hidden_width_*hidden_thickness_/arma::Datum<fltp>::pi)));
			const fmm::ShCurrentSourcesPr current_sources = fmm::CurrentSources::create(Rs,dRs,Is,epss);
			current_sources->set_van_Lanen(true); // line sources
			sources = current_sources;
		}

		// use a ribbon source
		else if(n_dim_==2){
			const fltp hidden_dim = calc_hidden_dim();
			const arma::Mat<fltp> Jn = calc_nodal_current_density();
			arma::Mat<fltp> Je(3,n_.n_cols);
			for(arma::uword i=0;i<n_.n_cols;i++)
				Je.col(i) = hidden_dim*arma::mean(Jn.cols(n_.col(i)),1); // surface current density [A/m]
			sources = fmm::CurrentSurface::create(R_,n_,Je);
		}

		// experimental volume elements
		else if(n_dim_==3 && prefer_volume_elements){
			// volume mesh
			const arma::Row<fltp> In = arma::repmat(number_turns_*operating_current_*(cmn::Quadrilateral::calc_area(Rb_,b_)/base_area_),1,num_cs_);
			sources = fmm::SourceCoil::create(R_, arma::join_vert(arma::flipud(n_.rows(0,3)), arma::flipud(n_.rows(4,7))),In);
		}

		// create regular point sources fastest
		else{
			// create sources
			arma::Mat<fltp> Rs,dRs;
			arma::Row<fltp> epss,Is;
			create_point_sources(Rs,dRs,epss,Is);

			// create current source for FMM
			const fmm::ShCurrentSourcesPr current_sources = fmm::CurrentSources::create(Rs,dRs,Is,epss);
			current_sources->set_van_Lanen(false); // point sources

			// set it to sources
			sources = current_sources;
		}

		// return the sources
		return sources;
	}

	// set operating current
	// remember to update the current density as well
	void CoilData::set_operating_current(const fltp operating_current){
		operating_current_ = operating_current;
	}

	// get operating current
	fltp CoilData::get_operating_current() const{
		return operating_current_;
	}

	// set number of turns
	void CoilData::set_number_turns(const fltp number_turns){
		number_turns_ = number_turns;
	}

	// get number of turns
	fltp CoilData::get_number_turns() const{
		return number_turns_;
	}

	// get number of source points per element
	arma::uword CoilData::calc_num_sources_per_element()const{
		return std::pow(std::abs(num_gauss_volume_),n_dim_-1);
	}

	// calculate enclosed flux
	fltp CoilData::calculate_flux() const{
		// // check if field calculated
		// if(!has_field())rat_throw_line("field not calculated");
		// if(!has('A'))rat_throw_line("vector potential not calculated");

		// // check input
		// assert(num_cs_elements_==n_.n_cols);

		// // calculate element volume
		// const arma::Row<fltp> element_volume = calc_volume();
		// const arma::Row<fltp> cis_total_area = calc_cis_total_area();
		// assert(cis_total_area.n_cols==num_cis_);
		// assert(element_volume.n_cols==num_cs_elements_);

		// // calculate contribution from nodes
		// const arma::Row<fltp> phidn = rat::cmn::Extra::dot(get_field('A'), L_);

		// // calculate contribution from elements
		// arma::Row<fltp> phie(num_cs_elements_);
		// for(arma::uword l=0;l<num_cs_elements_;l++)
		// 	phie(l) = element_volume(l)*arma::as_scalar(arma::mean(phidn.cols(n_.col(l))/cis_total_area.cols(n_.col(l)/num_base_nodes_),1));

		// // convert to voltage contribution
		// const fltp phi = arma::accu(get_number_turns()*phie);

		// // accumulutae
		// return phi;

		// check if field calculated
		if(!has_field())rat_throw_line("field not calculated");
		if(!has('A'))rat_throw_line("vector potential not calculated");

		// get effective current
		arma::Mat<fltp> Rs,dRs; arma::Row<fltp> epss,Is;
		create_point_sources(Rs,dRs,epss,Is);

		// get vector potential
		const arma::Mat<fltp> Agp = get_field_gp('A');
		if(!Agp.is_finite())rat_throw_line("vector potential on gauss points is not finite");

		// integrate captured flux at the gauss points N*L/num_turns
		const rat::fltp phi_mut = arma::accu(rat::cmn::Extra::dot(Agp, dRs));

		// // self inductance of current elements
		// const arma::Row<fltp> length = cmn::Extra::vec_norm(dRs);
		// const arma::Row<fltp> diameter = 2*epss/(softening_*std::sqrt(2.0));
		// const rat::fltp phi_self = arma::accu(fmm::Extra::calc_wire_inductance(diameter, length));

		// scale for number of turns
		return phi_mut;
	}

	// calculate enclosed flux
	fltp CoilData::calculate_magnetic_energy()const{
		// check if field calculated
		if(!has_field())rat_throw_line("field not calculated");
		if(!has('A'))rat_throw_line("vector potential not calculated");

		// energy from flux
		const rat::fltp total_energy = calculate_flux()*operating_current_/2;

		// accumulutae
		return total_energy;
	}

	// // calculate self inductance of elements
	// fltp CoilData::calculate_element_self_inductance() const{
	// 	std::cout<<"TODO"<<std::endl;
	// 	return 0;
	// 	ribbon self inductance is taken into account
	// 	by using the charged polyhedron code
	// 	if(n_dim_==2)return 0.0;
			
	// 	// create line elements
	// 	arma::Mat<fltp> Rs,dRs; arma::Row<fltp> epss,Acrss,Is;
	// 	create_line_elements(Rs,dRs,epss,Acrss,Is,num_gauss_volume_);
	// 	const arma::uword num_sources = Rs.n_cols;
	// 	const arma::uword num_element_gs = calc_num_sources_per_element(); 

	// 	// check
	// 	assert(num_sources==num_element_gs*get_num_elements());
	// 	assert(num_sources==Rs.n_cols);
	// 	assert(num_sources==dRs.n_cols);
	// 	assert(num_sources==Acrss.n_elem);
	// 	assert(num_sources==Is.n_elem);
	// 	assert(num_sources==epss.n_elem);

	// 	// get area
	// 	const arma::Row<fltp> cs_total_area = calc_cs_total_area();

	// 	// check
	// 	assert(arma::is_finite(cs_total_area));

	// 	// calculate from elements
	// 	const arma::Row<fltp> length = cmn::Extra::vec_norm(dRs);
	// 	const arma::Row<fltp> diameter = 2*arma::sqrt(Acrss/arma::Datum<fltp>::pi);

	// 	// check
	// 	assert(arma::is_finite(length));
	// 	assert(arma::is_finite(diameter));

	// 	// calculate self inductance
	// 	const arma::Row<fltp> self_inductance = 
	// 		rat::fmm::Extra::calc_wire_inductance(diameter, length);

	// 	// check inductance
	// 	assert(arma::is_finite(self_inductance));

	// 	// allocate output
	// 	fltp accu_self_inductance = RAT_CONST(0.0);

	// 	// walk over elements
	// 	for(arma::uword i=0;i<num_sources;i++)
	// 		accu_self_inductance += 
	// 			self_inductance(i)*Acrss(i)*Acrss(i)/
	// 			cs_total_area(i/num_element_gs/num_base_elements_)/
	// 			cs_total_area(i/num_element_gs/num_base_elements_);
	

	// 	// scale
	// 	return number_turns_*number_turns_*accu_self_inductance;
	// }

	// VTK export
	ShVTKObjPr CoilData::export_vtk() const{
		// create unstructured grid
		ShVTKUnstrPr vtk = std::dynamic_pointer_cast<VTKUnstr>(MeshData::export_vtk());
		assert(vtk!=NULL);

		// check if field calculated
		if(has('B') && has_field()){
			// field angle
			const arma::Row<fltp> magnetic_field_angle = calc_magnetic_field_angle();

			// set to VTK
			vtk->set_nodedata(360.0*magnetic_field_angle/(arma::Datum<fltp>::pi*2),"Mgn. Field Angle [deg]"); 

			// current density vectors
			const arma::Mat<fltp> current_density = calc_nodal_current_density();

			// calculate force density vectors
			const arma::Mat<fltp> force_density = calc_force_density();

			// calculate pressure
			const arma::Mat<fltp> pressure = calc_pressure_vector();

			// calulate von mises from pressure
			const arma::Row<fltp> von_mises = calc_von_mises();

			// current density
			vtk->set_nodedata(1e-6*current_density,"Current Density [A/mm^2]");

			// mechanical properties
			// pressure vector
			vtk->set_nodedata(1e-6*pressure,"Pressure [MPa]");

			// von mises stress
			vtk->set_nodedata(1e-6*von_mises,"Von Mises [MPa]");

			// add force density to VTK
			vtk->set_nodedata(1e-6*force_density,"Force Density [N/cm^3]");

			// material related properties
			if(material_!=NULL){
				// engineering critical current
				const arma::Row<fltp> critical_current_density_local = calc_critical_current_density(false);
				const arma::Row<fltp> critical_current_density_shared = calc_critical_current_density(true);

				// divide current density by critical (engineering) current density
				const arma::Row<fltp> critical_current_fraction_local =  cmn::Extra::vec_norm(current_density)/critical_current_density_local;
				const arma::Row<fltp> critical_current_fraction_shared =  cmn::Extra::vec_norm(current_density)/critical_current_density_shared;

				// calculate critical temperature
				const arma::Row<fltp> critical_temperature_local = calc_critical_temperature(false);
				const arma::Row<fltp> critical_temperature_shared = calc_critical_temperature(true);

				// calculate electric field
				const arma::Mat<fltp> electric_field_local = calc_electric_field(false);
				const arma::Mat<fltp> electric_field_shared = calc_electric_field(true);

				// percentage on loadline
				const arma::Row<fltp> load_line_fraction_local = calc_loadline_fraction(false);
				const arma::Row<fltp> load_line_fraction_shared = calc_loadline_fraction(true);

				// add engineering current density to VTK
				vtk->set_nodedata(1e-6*critical_current_density_local,"Lc. Eng. Cur. Dens. [A/mm^2]"); 
				vtk->set_nodedata(1e-6*critical_current_density_shared,"Sh. Eng. Cur. Dens. [A/mm^2]"); 

				// set percentage oif critical current
				vtk->set_nodedata(100*critical_current_fraction_local,"Lc. Pct. Crit. Current [%]");
				vtk->set_nodedata(100*critical_current_fraction_shared,"Sh. Pct. Crit. Current [%]");

				// add critical temperature to VTK
				vtk->set_nodedata(critical_temperature_local,"Lc. Crit. Temperature [K]");
				vtk->set_nodedata(critical_temperature_shared,"Sh. Crit. Temperature [K]");

				// add temperature margin to VTK
				vtk->set_nodedata(critical_temperature_local - temperature_,"Lc. Temperature Margin [K]");
				vtk->set_nodedata(critical_temperature_shared - temperature_,"Sh. Temperature Margin [K]");

				// electric field
				vtk->set_nodedata(electric_field_local,"Lc. Electric Field [V/m]");
				vtk->set_nodedata(electric_field_shared,"Sh. Electric Field [V/m]");

				// power density
				vtk->set_nodedata(cmn::Extra::dot(electric_field_local, current_density),"Lc. Power Density [W/m^3]");
				vtk->set_nodedata(cmn::Extra::dot(electric_field_shared, current_density),"Sh. Power Density [W/m^3]");

				// load line calculation
				vtk->set_nodedata(100*load_line_fraction_local,"Lc. Pct. Loadline [%]");
				vtk->set_nodedata(100*load_line_fraction_shared,"Sh. Pct. Loadline [%]");
			}
		}

		// return the unstructured mesh
		return vtk;
	}

	// calculate average critical temperature
	arma::Row<fltp> CoilData::calc_critical_temperature(
		const bool use_current_sharing,
		const FieldAngleType angle_type,
		const bool use_parallel) const{
		// get counters
		const arma::uword num_nodes = R_.n_cols;

		// check if field calculated
		if(!has('B') || !has_field() || material_==NULL)
			return arma::Row<fltp>(num_nodes,arma::fill::zeros);

		// allocate
		arma::Row<fltp> critical_temperature(num_nodes);

		// current density at nodes
		const arma::Mat<fltp> current_density = calc_nodal_current_density();
		const arma::Row<fltp> current_density_norm = 
			rat::cmn::Extra::vec_norm(current_density);

		// get magnetic flux density at nodes
		const arma::Mat<fltp> magnetic_field = get_field('B');

		// calculate angle in radians and return
		const arma::Row<fltp> magnetic_field_angle = calc_magnetic_field_angle(angle_type);

		// calculate magnitude of field
		const arma::Row<fltp> magnetic_field_norm = cmn::Extra::vec_norm(magnetic_field);

		// scaling factor
		const arma::Row<fltp> scaling_factor(num_nodes, arma::fill::ones);

		// calculate critical temperature
		// without current sharing
		if(!use_current_sharing){
			// divide into chunks
			const arma::uword num_cross_per_chunk = 10;
			const arma::uword chunk_size = num_cross_per_chunk*num_base_nodes_;
			const arma::uword num_chunks = static_cast<arma::uword>(std::ceil(static_cast<fltp>(num_nodes)/chunk_size));

			// parallel calculation in chunks of 
			// integer number of cross-sections
			cmn::parfor(0,num_chunks,use_parallel,[&](int i, int){
				// get indexes
				const arma::uword idx1 = i*chunk_size;
				const arma::uword idx2 = std::min((i+1)*chunk_size-1, num_nodes-1);

				critical_temperature.cols(idx1,idx2) = 
					material_->calc_cs_temperature_mat(
						current_density_norm.cols(idx1,idx2), 
						magnetic_field_norm.cols(idx1,idx2), 
						magnetic_field_angle.cols(idx1,idx2),
						scaling_factor.cols(idx1,idx2));
			});
		}

		// with current sharing
		else{
			// get cross section mesh
			// const arma::Mat<arma::uword> ncrss = area_->get_elements();
			// const arma::Mat<fltp> Acrss = area_->get_areas();
			const arma::Row<fltp> cis_areas = calc_cis_areas();

			// get mesh
			const arma::Mat<arma::uword>& b = get_base_mesh();

			// parallel calculation in chunks of 
			// integer number of cross-sections
			cmn::parfor(0,num_cis_,use_parallel,[&](int i, int){
				// get indexes
				const arma::uword idx1 = i*num_base_nodes_;
				const arma::uword idx2 = (i+1)*num_base_nodes_-1;
				const arma::uword idx3 = i*num_base_elements_;
				const arma::uword idx4 = (i+1)*num_base_elements_-1;

				// calculate averaged critical temperature
				critical_temperature.cols(idx1,idx2) = 
					material_->calc_cs_temperature_av_mat(
						current_density_norm.cols(idx1,idx2), 
						magnetic_field_norm.cols(idx1,idx2), 
						magnetic_field_angle.cols(idx1,idx2),
						scaling_factor.cols(idx1,idx2), 
						b,num_base_nodes_,
						cis_areas.cols(idx3,idx4));
			});
		}

		// return critical temperature
		return critical_temperature;
	}

	// calculate average critical temperature
	arma::Row<fltp> CoilData::calc_temperature_margin(
		const bool use_current_sharing, 
		const FieldAngleType angle_type,
		const bool use_parallel) const{
		// get counters
		const arma::uword num_nodes = R_.n_cols;

		// check if field calculated
		if(!has('B') || !has_field() || material_==NULL)
			return arma::Row<fltp>(num_nodes,arma::fill::zeros);

		// for non-superconductors
		if(!material_->is_superconductor())
			return arma::Row<fltp>(num_nodes,arma::fill::zeros);

		// calc critical temperature and derive temperature margin
		return calc_critical_temperature(use_current_sharing, angle_type, use_parallel) - temperature_;
	}

	// critical current calculation
	arma::Row<fltp> CoilData::calc_critical_current_density(
		const bool use_current_sharing, 
		const FieldAngleType angle_type, 
		const bool use_parallel) const{

		// check
		assert(num_base_nodes_!=0);

		// get counters
		const arma::uword num_nodes = R_.n_cols;

		// check if field calculated
		if(!has('B') || !has_field() || material_==NULL)
			return arma::Row<fltp>(num_nodes,arma::fill::zeros);

		// get magnetic flux density at nodes
		const arma::Mat<fltp> magnetic_field = get_field('B');

		// calculate magnitude of field
		const arma::Row<fltp> magnetic_field_norm = cmn::Extra::vec_norm(magnetic_field);

		// calculate fiel dangle
		const arma::Row<fltp> magnetic_field_angle = calc_magnetic_field_angle(angle_type);

		// scaling factor
		const arma::Row<fltp> scaling_factor(num_nodes, arma::fill::ones);

		// allocate
		arma::Row<fltp> critical_current_density(num_nodes);

		// Jc calculation with current sharing over cross-section
		// without current sharing
		if(!use_current_sharing){
			// divide in chunks
			const arma::uword num_cross_per_chunk = 10;
			const arma::uword chunk_size = num_cross_per_chunk*num_base_nodes_;
			const arma::uword num_chunks = static_cast<arma::uword>(std::ceil(static_cast<fltp>(num_nodes)/chunk_size));

			// parallel calculation in chunks of 
			// integer number of cross-sections
			cmn::parfor(0,num_chunks,use_parallel,[&](int i, int){
				// get indexes
				const arma::uword idx1 = i*chunk_size;
				const arma::uword idx2 = std::min((i+1)*chunk_size-1, num_nodes-1);

				// calc engineering critical current density
				critical_current_density.cols(idx1,idx2) = 
					material_->calc_critical_current_density_mat(
						temperature_.cols(idx1,idx2), 
						magnetic_field_norm.cols(idx1,idx2), 
						magnetic_field_angle.cols(idx1,idx2),
						scaling_factor.cols(idx1,idx2));
			});
		}

		// with current sharing
		else{
			// get cross section mesh
			// const arma::Mat<arma::uword> ncrss = area_->get_elements();
			// const arma::Mat<fltp> Acrss = area_->get_areas();
			const arma::Row<fltp> cis_areas = calc_cis_areas();

			// get mesh
			const arma::Mat<arma::uword>& b = get_base_mesh();

			// parallel calculation in chunks of 
			// integer number of cross-sections
			cmn::parfor(0,num_cis_,use_parallel,[&](int i, int){
				// get indexes
				const arma::uword idx1 = i*num_base_nodes_;
				const arma::uword idx2 = (i+1)*num_base_nodes_-1;
				const arma::uword idx3 = i*num_base_elements_;
				const arma::uword idx4 = (i+1)*num_base_elements_-1;

				// calculate
				critical_current_density.cols(idx1,idx2) = 
					material_->calc_critical_current_density_av_mat(
						temperature_.cols(idx1,idx2), 
						magnetic_field_norm.cols(idx1,idx2), 
						magnetic_field_angle.cols(idx1,idx2), 
						scaling_factor.cols(idx1,idx2),
						b,num_base_nodes_,
						cis_areas.cols(idx3,idx4));
			});
		}

		// return critical current density
		return critical_current_density;
	}


	// critical current calculation
	arma::Row<fltp> CoilData::calc_loadline_fraction(
		const bool use_current_sharing, 
		const FieldAngleType angle_type, 
		const bool use_parallel) const{
		// get counters
		const arma::uword num_nodes = R_.n_cols;

		// check if field calculated
		if(!has('B') || !has_field() || material_==NULL)
			return arma::Row<fltp>(num_nodes,arma::fill::zeros);

		// get magnetic flux density at nodes
		const arma::Mat<fltp> magnetic_field = get_field('B');

		// calculate magnitude of field
		const arma::Row<fltp> magnetic_field_norm = cmn::Extra::vec_norm(magnetic_field);

		// calculate fiel dangle
		const arma::Row<fltp> magnetic_field_angle = calc_magnetic_field_angle(angle_type);

		// current density at nodes
		const arma::Mat<fltp> current_density = calc_nodal_current_density();
		const arma::Row<fltp> current_density_norm = rat::cmn::Extra::vec_norm(current_density);

		// scaling factor
		const arma::Row<fltp> scaling_factor(num_nodes, arma::fill::ones);

		// allocate
		arma::Row<fltp> percent_loadline(num_nodes);

		// electric field calculation
		if(!use_current_sharing){
			// divide in chunks
			const arma::uword num_cross_per_chunk = 10;
			const arma::uword chunk_size = num_cross_per_chunk*num_base_nodes_;
			const arma::uword num_chunks = static_cast<arma::uword>(std::ceil(static_cast<fltp>(num_nodes)/chunk_size));

			// parallel calculation in chunks of 
			// integer number of cross-sections
			cmn::parfor(0,num_chunks,use_parallel,[&](int i, int){
				// get indexes
				const arma::uword idx1 = i*chunk_size;
				const arma::uword idx2 = std::min((i+1)*chunk_size-1, num_nodes-1);

				// load line calculation
				percent_loadline.cols(idx1,idx2) = 
					material_->calc_percent_load_mat(
						current_density_norm.cols(idx1,idx2), 
						temperature_.cols(idx1,idx2), 
						magnetic_field_norm.cols(idx1,idx2), 
						magnetic_field_angle.cols(idx1,idx2),
						scaling_factor.cols(idx1,idx2));
			});
		}

		else{
			// get cross section mesh
			const arma::Row<fltp> cis_areas = calc_cis_areas();

			// get mesh
			const arma::Mat<arma::uword>& b = get_base_mesh();

			// parallel calculation in chunks of 
			// integer number of cross-sections
			cmn::parfor(0,num_cis_,use_parallel,[&](int i, int){
				// get indexes
				const arma::uword idx1 = i*num_base_nodes_;
				const arma::uword idx2 = (i+1)*num_base_nodes_-1;
				const arma::uword idx3 = i*num_base_elements_;
				const arma::uword idx4 = (i+1)*num_base_elements_-1;

				// load line calculation
				percent_loadline.cols(idx1,idx2) = 
					material_->calc_percent_load_mat_av(
						current_density_norm.cols(idx1,idx2), 
						temperature_.cols(idx1,idx2), 
						magnetic_field_norm.cols(idx1,idx2), 
						magnetic_field_angle.cols(idx1,idx2),
						scaling_factor.cols(idx1,idx2),
						b,num_base_nodes_,cis_areas.cols(idx3,idx4));
			});
		}

		// return critical current density
		return percent_loadline/100;
	}

	// electric field calculation
	arma::Mat<fltp> CoilData::calc_electric_field(
		const bool use_current_sharing, 
		const FieldAngleType angle_type, 
		const bool use_parallel) const{

		// get counters
		const arma::uword num_nodes = R_.n_cols;
		
		// check if field calculated
		if(!has('B') || !has_field() || material_==NULL)
			return arma::Mat<fltp>(3,num_nodes,arma::fill::zeros);

		// get magnetic flux density at nodes
		const arma::Mat<fltp> magnetic_field = get_field('B');

		// calculate magnitude of field
		const arma::Row<fltp> magnetic_field_norm = cmn::Extra::vec_norm(magnetic_field);

		// calculate fiel dangle
		const arma::Row<fltp> magnetic_field_angle = calc_magnetic_field_angle(angle_type);

		// current density at nodes
		const arma::Mat<fltp> current_density = calc_nodal_current_density();
		const arma::Row<fltp> current_density_norm = rat::cmn::Extra::vec_norm(current_density);

		// scaling factor
		const arma::Row<fltp> scaling_factor(num_nodes, arma::fill::ones);

		// allocate
		arma::Row<fltp> electric_field(num_nodes);

		// electric field calculation
		// without current sharing
		if(!use_current_sharing){
			// divide in chunks
			const arma::uword num_cross_per_chunk = 10;
			const arma::uword chunk_size = num_cross_per_chunk*num_base_nodes_;
			const arma::uword num_chunks = static_cast<arma::uword>(std::ceil(static_cast<fltp>(num_nodes)/chunk_size));

			// parallel calculation in chunks of 
			// integer number of cross-sections
			cmn::parfor(0,num_chunks,use_parallel,[&](int i, int){
				// get indexes
				const arma::uword idx1 = i*chunk_size;
				const arma::uword idx2 = std::min((i+1)*chunk_size-1, num_nodes-1);

				electric_field.cols(idx1,idx2) = 
					material_->calc_electric_field_mat(
						current_density_norm.cols(idx1,idx2), 
						temperature_.cols(idx1,idx2), 
						magnetic_field_norm.cols(idx1,idx2), 
						magnetic_field_angle.cols(idx1,idx2),
						scaling_factor.cols(idx1,idx2));
			});
		}

		// with current sharing
		else{

			// get cross section mesh
			// const arma::Mat<arma::uword> ncrss = area_->get_elements();
			// const arma::Mat<fltp> Acrss = area_->get_areas();
			const arma::Row<fltp> cis_areas = calc_cis_areas();

			// get mesh
			const arma::Mat<arma::uword>& b = get_base_mesh();

			// parallel calculation in chunks of 
			// integer number of cross-sections
			cmn::parfor(0,num_cis_,use_parallel,[&](int i, int){
				// get indexes
				const arma::uword idx1 = i*num_base_nodes_;
				const arma::uword idx2 = (i+1)*num_base_nodes_-1;
				const arma::uword idx3 = i*num_base_elements_;
				const arma::uword idx4 = (i+1)*num_base_elements_-1;

				// calculate
				electric_field.cols(idx1,idx2) = 
					material_->calc_electric_field_av_mat(
						current_density_norm.cols(idx1,idx2), 
						temperature_.cols(idx1,idx2), 
						magnetic_field_norm.cols(idx1,idx2), 
						magnetic_field_angle.cols(idx1,idx2),
						scaling_factor.cols(idx1,idx2), 
						b,num_base_nodes_,
						cis_areas.cols(idx3,idx4));
			});
		}

		// return critical current density
		return L_.each_row()%electric_field;
	}

	// calculate voltage
	arma::Row<fltp> CoilData::calc_voltage(
		const bool use_current_sharing,
		const FieldAngleType angle_type, 
		const bool use_parallel)const{

		// first calc electric field along length of conductor
		const arma::Row<fltp> E = cmn::Extra::dot(L_, calc_electric_field(use_current_sharing, angle_type, use_parallel));

		// calculate element volume
		const arma::Row<fltp> Ve = calc_volume();

		// calculate electric field in middle of elements
		arma::Row<fltp> Ee(n_.n_cols);
		for(arma::uword i=0;i<n_.n_cols;i++)
			Ee(i) = arma::as_scalar(arma::mean(E.cols(n_.col(i)),1));

		// allocate voltage
		arma::Row<fltp> dVcs(num_cs_);

		// walk over cable sections
		for(arma::uword i=0;i<num_cs_;i++){
			// get indices of this slice
			const arma::uword idx1 = i*num_base_elements_;
			const arma::uword idx2 = (i+1)*num_base_elements_-1;

			// calculate voltage drop
			dVcs(i) = arma::accu(Ve.cols(idx1,idx2)%Ee.cols(idx1,idx2))/base_area_;
		}

		// accumulate
		const arma::Row<fltp> Vcis = arma::join_horiz(arma::Row<fltp>{0.0}, arma::cumsum(dVcs));

		// allocate
		arma::Row<fltp> V(num_nodes_);

		// walk over cable intersections
		for(arma::uword i=0;i<num_cis_;i++){
			// get indices of this slice
			const arma::uword idx1 = i*num_base_nodes_;
			const arma::uword idx2 = (i+1)*num_base_nodes_-1;

			// set to nodes
			V.cols(idx1,idx2).fill(Vcis(i));
		}

		// return voltage
		return V;
	}



	// calculate power density at nodes
	arma::Row<fltp> CoilData::calc_power_density(
		const bool use_current_sharing, 
		const FieldAngleType angle_type, 
		const bool use_parallel) const{
		return calc_current_density()%cmn::Extra::vec_norm(calc_electric_field(use_current_sharing, angle_type, use_parallel));
	}

	// calculate power density at nodes
	arma::Row<fltp> CoilData::calc_critical_current_fraction(
		const bool use_current_sharing,
		const FieldAngleType angle_type,  
		const bool use_parallel) const{
		// get counters
		const arma::uword num_nodes = R_.n_cols;

		// check if field calculated
		if(!has('B') || !has_field() || material_==NULL)
			return arma::Row<fltp>(1,num_nodes,arma::fill::zeros);

		// non superconductor
		if(!material_->is_superconductor())
			return arma::Row<fltp>(num_nodes, arma::fill::zeros);

		// divide current density by critical (engineering) current density
		return cmn::Extra::vec_norm(calc_nodal_current_density())/calc_critical_current_density(use_current_sharing, angle_type, use_parallel);
	}


	// // export line elements
	// ShVTKTablePr CoilData::export_line_elements() const{
	// 	// check if sources were setup
	// 	if(Rs_.is_empty())rat_throw_line("sources are not setup");
	// 	if(dRs_.is_empty())rat_throw_line("sources are not setup");

	// 	// create table
	// 	const ShVTKTablePr table = VTKTable::create();
	// 	table->set_num_rows(Rs_.n_cols);
	// 	for(arma::uword i=0;i<3;i++)
	// 		table->set_data(Rs_.row(i).t(),"R" + std::string({cmn::Extra::idx2xyz(i)}));
	// 	for(arma::uword i=0;i<3;i++)
	// 		table->set_data(dRs_.row(i).t(),"dR" + std::string({cmn::Extra::idx2xyz(i)}));

	// 	// return table
	// 	return table;
	// }

	// calculate integrated foce
	arma::Col<fltp>::fixed<3> CoilData::calc_lorentz_force() const{
		// number of gauss points in each element
		const arma::uword num_gauss_per_element = std::pow(num_gauss_volume_,n_dim_);

		// get flux density at gauss points
		const arma::Mat<fltp> flux_density_gauss_points = get_field_gp('B');

		// get effective current
		arma::Mat<fltp> Rs,dRs; arma::Row<fltp> epss,Is;
		create_point_sources(Rs,dRs,epss,Is);

		// force density gauss points
		const arma::Mat<fltp> force_on_gauss_points = cmn::Extra::cross(dRs.each_row()%Is, flux_density_gauss_points); // F = J X B

		// accumulate forces
		arma::Col<fltp>::fixed<3> F(arma::fill::zeros);
		for(arma::uword i=0;i<n_.n_cols;i++)F += arma::sum(force_on_gauss_points.cols(i*num_gauss_per_element,(i+1)*num_gauss_per_element-1),1);

		// multiply by element volume and sum
		return F;
	}

	// calculate force density
	arma::Mat<fltp> CoilData::calc_force_density() const{
		return cmn::Extra::cross(calc_nodal_current_density(),arma::Datum<fltp>::mu_0*get_field('H'));
	}

	// calculate accumulated pressure direction given by int_dir
	// 0 Normal direction, 1. Transverse direction
	arma::Row<fltp> CoilData::calc_pressure(
		const arma::uword int_dir, 
		const bool use_parallel) const{

		// hard-coded settings
		const fltp tol = RAT_CONST(1e-12);

		// special case for line cross-section
		if(n_dim_<=2 && int_dir==0)return cmn::Extra::dot(calc_force_density(),N_)*hidden_thickness_;
		if(n_dim_<=2 && int_dir==1)return arma::Row<fltp>(R_.n_cols,arma::fill::zeros);

		// direction transverse to integration
		const arma::uword trans_dir = 1 - int_dir;

		// calculate Lorentz force density
		const arma::Mat<fltp> force_density = calc_force_density();
		assert(force_density.is_finite());

		// check N and D before using
		assert(N_.is_finite()); assert(D_.is_finite());

		// get in plane components only
		// Note. the method here assumes that the planes 
		// are planar and that N and D are in plane
		arma::Row<fltp> force_density_int;
		if(int_dir==0)force_density_int = cmn::Extra::dot(force_density, N_);
		else if(int_dir==1)force_density_int = cmn::Extra::dot(force_density, D_);
		else rat_throw_line("direction of integration not recognized");
		assert(force_density_int.is_finite());

		// get base mesh elements
		const arma::Mat<arma::uword>& ncrss = get_base_mesh();

		// store nodes
		arma::Row<fltp> pressure(num_nodes_,arma::fill::zeros);

		// walk over cross sections
		cmn::parfor(0,num_cis_,use_parallel,[&](int i, int){
		// for(arma::uword i=0;i<num_cis_;i++){
			// get indexing
			const arma::uword idx1 = i*num_base_nodes_;
			const arma::uword idx2 = (i+1)*num_base_nodes_ - 1;

			// get force densities for this cross section
			const arma::Row<fltp> force_density_int_crss = force_density_int.cols(idx1,idx2);

			// get cross sectional nodes
			const arma::Mat<fltp> Rcrss = arma::join_vert(
				cmn::Extra::dot(R_.cols(idx1,idx2),N_.cols(idx1,idx2)), 
				cmn::Extra::dot(R_.cols(idx1,idx2),D_.cols(idx1,idx2)));
			assert(Rcrss.n_cols==num_base_nodes_);

			// create trace line coordinates in the transverse direction
			const arma::Row<fltp> transverse = arma::unique(Rcrss.row(trans_dir));

			// find all unique edge elements
			const arma::Mat<arma::uword>::fixed<4,2> Me = cmn::Quadrilateral::get_edges();
			arma::Mat<arma::uword> ecrss(2,ncrss.n_cols*Me.n_rows);
			for(arma::uword i=0;i<Me.n_rows;i++)
				ecrss.cols(i*ncrss.n_cols,(i+1)*ncrss.n_cols-1) = ncrss.rows(Me.row(i));

			// convert to 2D coords
			arma::Mat<fltp> R1 = Rcrss.cols(ecrss.row(0));
			arma::Mat<fltp> R2 = Rcrss.cols(ecrss.row(1));

			// throw away edges that run along integration direction
			const arma::Col<arma::uword> idx_shed = arma::find(arma::abs(R1.row(trans_dir) - R2.row(trans_dir))<1e-9);
			ecrss.shed_cols(idx_shed); R1.shed_cols(idx_shed); R2.shed_cols(idx_shed);

			// const arma::Row<fltp> force_density_transverse_crss = force_density_transverse.cols(idx1,idx2);
			assert(force_density_int_crss.is_finite());

			// allocate pressure in nodes
			arma::Row<fltp> pressure_crss(num_base_nodes_, arma::fill::zeros);

			// ray casting
			for(arma::uword j=0;j<transverse.n_elem;j++){
				// get transverse corodinate
				const fltp t = transverse(j);

				// find indexes of edges that cross this line
				const arma::Col<arma::uword> idx = arma::find(
					(t>=(R1.row(trans_dir)-tol) && t<=(R2.row(trans_dir)+tol)) || 
					(t<=(R1.row(trans_dir)+tol) && t>=(R2.row(trans_dir)-tol)));
				if(idx.empty())rat_throw_line("no intersection points found");

				// force densities along line
				const arma::Row<fltp> Fdn1 = force_density_int_crss.cols(ecrss.cols(idx).eval().row(0));
				const arma::Row<fltp> Fdn2 = force_density_int_crss.cols(ecrss.cols(idx).eval().row(1));
				const arma::Mat<fltp> R1l = R1.cols(idx);
				const arma::Mat<fltp> R2l = R2.cols(idx);
				if(arma::any(arma::abs(R2l.row(trans_dir)-R1l.row(trans_dir))<1e-9))rat_throw_line("edge is in normal direction");

				// calculate weights
				const arma::Row<fltp> w1 = (R2l.row(trans_dir) - t)/(R2l.row(trans_dir) - R1l.row(trans_dir));
				const arma::Row<fltp> w2 = (t - R1l.row(trans_dir))/(R2l.row(trans_dir) - R1l.row(trans_dir));

				// find force and coordinate of intersection
				arma::Row<fltp> Fdni = w1%Fdn1 + w2%Fdn2;
				arma::Mat<fltp> Ri = w1%R1l.each_row() + w2%R2l.each_row();
				if(!Ri.is_finite())rat_throw_line("intersection coords are not finite");
				if(!Fdni.is_finite())rat_throw_line("Line force density is not finite");

				// three dimensional coordinates
				arma::Mat<fltp> R3d = w1%R_.cols(idx1 + ecrss.cols(idx).eval().row(0)).eval().each_row() + w2%R_.cols(idx1 + ecrss.cols(idx).eval().row(1)).eval().each_row();

				// sort nodes based on their position in the integration direction
				const arma::Col<arma::uword> idx_sort = arma::sort_index(Ri.row(int_dir));

				// perform sorting
				Fdni = Fdni.cols(idx_sort); Ri = Ri.cols(idx_sort); R3d = R3d.cols(idx_sort); 

				// find unique points
				// const arma::Col<arma::uword> idx_unique = arma::find_unique(Ri.row(int_dir));
				const arma::Col<arma::uword> idx_unique = cmn::Extra::find_unique(Ri.row(int_dir),true,tol);

				// perform sorting
				Fdni = Fdni.cols(idx_unique); Ri = Ri.cols(idx_unique);  R3d = R3d.cols(idx_unique);

				// determine sign
				const fltp sgn = cmn::Extra::sign(arma::as_scalar(cmn::Extra::dot(R3d.tail_cols(1) - R3d.col(0), int_dir==0 ?  N_.col(idx1) : D_.col(idx1))));
				Fdni*=sgn;

				// integrate force density
				if(Ri.n_cols!=Fdni.n_elem)rat_throw_line("num elements not ok");
				arma::Row<fltp> pressure_line = cmn::Extra::cumtrapz(Ri.row(int_dir), Fdni, 1);
				if(!pressure_line.is_finite())rat_throw_line("line pressure is not finite");
				if(pressure_line.n_elem!=Fdni.n_elem)rat_throw_line("num elements changed");

				// move minimum up
				pressure_line -= arma::min(pressure_line);

				// find elements
				const arma::Col<arma::uword> idx_nodes_along_ray = arma::find(Rcrss.row(trans_dir)>(t-tol) && Rcrss.row(trans_dir)<(t+tol));
				if(idx_nodes_along_ray.empty())rat_throw_line("no nodes associated to this ray");

				// normal direction
				const arma::Mat<fltp> Rn = Rcrss.cols(idx_nodes_along_ray);
				if(!Rn.is_finite())rat_throw_line("node coords is not finite");

				// interpolate
				arma::Col<fltp> pressure_nodes;
				if(pressure_line.n_elem>1){
					assert(Ri.n_cols==pressure_line.n_elem);
					cmn::Extra::interp1(Ri.row(int_dir).t(),pressure_line.t(),Rn.row(int_dir).t(),pressure_nodes,"linear",true);
				}else{
					pressure_nodes = pressure_line;
				}

				// save
				pressure_crss.cols(idx_nodes_along_ray) = pressure_nodes.t();
			}

			// check output
			if(!pressure_crss.is_finite())rat_throw_line("crss pressure not finite");

			// store pressure
			pressure.cols(idx1,idx2) = pressure_crss;
		});

		// return pressure
		if(!pressure.is_finite())rat_throw_line("pressure is not finite");
		return pressure;
	}

	// setup sources and targets
	void CoilData::setup_targets(){
		// for hexahedrons
		if(n_.n_rows==8){
			// create gauss points for hexahedron volume
			const arma::Mat<fltp> gp = cmn::Hexahedron::create_gauss_points(num_gauss_volume_);
			const arma::Mat<fltp> Rqg = gp.rows(0,2);

			// convert to carthesian coords
			arma::Mat<fltp> Rg(3,n_.n_cols*Rqg.n_cols);
			for(arma::uword i=0;i<n_.n_cols;i++)
				Rg.cols(i*Rqg.n_cols,(i+1)*Rqg.n_cols-1) = cmn::Hexahedron::quad2cart(R_.cols(n_.col(i)),Rqg);
			
			// join
			Rt_ = arma::join_horiz(R_, Rg);
			num_targets_ = Rt_.n_cols;
		}

		// for quadrilateral mesh
		else if(n_.n_rows==4){
			// create gauss points for quadrilateral surface
			const arma::Mat<fltp> gp = cmn::Quadrilateral::create_gauss_points(num_gauss_volume_);
			const arma::Mat<fltp> Rqg = gp.rows(0,1);

			// convert to carthesian coords
			arma::Mat<fltp> Rg(3,n_.n_cols*Rqg.n_cols);
			for(arma::uword i=0;i<n_.n_cols;i++)
				Rg.cols(i*Rqg.n_cols,(i+1)*Rqg.n_cols-1) = cmn::Quadrilateral::quad2cart(R_.cols(n_.col(i)),Rqg);

			// join
			Rt_ = arma::join_horiz(R_, Rg);
			num_targets_ = Rt_.n_cols;
		}

		else if(n_.n_rows==2){
			// create gauss points for quadrilateral surface
			const arma::Mat<fltp> gp = cmn::Line::create_gauss_points(num_gauss_volume_);
			const arma::Mat<fltp> Rqg = gp.row(0);

			// convert to carthesian coords
			arma::Mat<fltp> Rg(3,n_.n_cols*Rqg.n_cols);
			for(arma::uword i=0;i<n_.n_cols;i++)
				Rg.cols(i*Rqg.n_cols,(i+1)*Rqg.n_cols-1) = cmn::Line::quad2cart(R_.cols(n_.col(i)),Rqg);

			// join
			Rt_ = arma::join_horiz(R_, Rg);
			num_targets_ = Rt_.n_cols;
		}

		else{
			rat_throw_line("Element type not recognized");
		}
	}

	// override get field function to always return the field at the nodes
	arma::Mat<fltp> CoilData::get_field(const char field_type)const{
		return MgnTargets::get_field(field_type).cols(0,R_.n_cols-1);
	}


	// get field at elements
	arma::Mat<fltp> CoilData::get_field_gp(const char field_type)const{
		const arma::Mat<fltp> fld = MgnTargets::get_field(field_type);
		return fld.cols(R_.n_cols, fld.n_cols-1);
	}

}}
