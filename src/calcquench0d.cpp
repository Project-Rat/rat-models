// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calcquench0d.hh"

// rat-common headers
#include "rat/common/rungekutta.hh"

// rat-material headers
#include "rat/mat/conductor.hh"

// rat-model headers
#include "calcinductance.hh"
#include "calcmesh.hh"
#include "coildata.hh"
#include "protectioncircuit.hh"
#include "extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcQuench0D::CalcQuench0D(){
		set_name("Adiabatic Quench"); 
	}

	CalcQuench0D::CalcQuench0D(const ShModelPr &model) : CalcQuench0D(){
		set_model(model);
	}

	// factory methods
	ShCalcQuench0DPr CalcQuench0D::create(){
		return std::make_shared<CalcQuench0D>();
	}

	ShCalcQuench0DPr CalcQuench0D::create(const ShModelPr &model){
		return std::make_shared<CalcQuench0D>(model);
	}

	// setters
	void CalcQuench0D::set_stop_temperature(const fltp stop_temperature){
		stop_temperature_ = stop_temperature;
	}

	void CalcQuench0D::set_stop_time(const fltp stop_time){
		stop_time_ = stop_time;
	}

	void CalcQuench0D::set_minimum_current_fraction(const fltp minimum_current_fraction){
		minimum_current_fraction_ = minimum_current_fraction;
	}

	void CalcQuench0D::set_reltol(const fltp reltol){
		reltol_ = reltol;
	}

	void CalcQuench0D::set_abstol(const fltp abstol){
		abstol_ = abstol;
	}

	void CalcQuench0D::set_integration_method(
		const cmn::RungeKutta::IntegrationMethod integration_method){
		integration_method_ = integration_method;
	}

	void CalcQuench0D::set_scenario(const CalcQuench0D::Scenario scenario){
		scenario_ = scenario;
	}

	void CalcQuench0D::set_scenario_circuit(const arma::uword scenario_circuit){
		scenario_circuit_ = scenario_circuit;
	}

	void CalcQuench0D::set_scenario_coil(const arma::uword scenario_coil){
		scenario_coil_ = scenario_coil;
	}

	void CalcQuench0D::set_temperature_bias(const fltp temperature_bias){
		temperature_bias_  = temperature_bias;
	}

	void CalcQuench0D::set_max_iter(const arma::uword max_iter){
		max_iter_ = max_iter;
	}


	// getters
	fltp CalcQuench0D::get_stop_temperature()const{
		return stop_temperature_;
	}

	fltp CalcQuench0D::get_stop_time()const{
		return stop_time_;
	}

	fltp CalcQuench0D::get_minimum_current_fraction()const{
		return minimum_current_fraction_;
	}

	fltp CalcQuench0D::get_reltol()const{
		return reltol_;
	}

	fltp CalcQuench0D::get_abstol()const{
		return abstol_;
	}

	cmn::RungeKutta::IntegrationMethod CalcQuench0D::get_integration_method()const{
		return integration_method_;
	}

	void CalcQuench0D::set_nzp_element_size(const fltp nzp_element_size){
		nzp_element_size_ = nzp_element_size;
	}

	fltp CalcQuench0D::get_nzp_element_size()const{
		return nzp_element_size_;
	}

	void CalcQuench0D::set_use_nzp1d(const bool use_nzp1d){
		use_nzp1d_ = use_nzp1d;
	}

	bool CalcQuench0D::get_use_nzp1d()const{
		return use_nzp1d_;
	}

	void CalcQuench0D::set_nzp_length(const fltp nzp_length){
		nzp_length_ = nzp_length;
	}

	fltp CalcQuench0D::get_nzp_length()const{
		return nzp_length_;
	}

	CalcQuench0D::Scenario CalcQuench0D::get_scenario()const{
		return scenario_;
	}

	arma::uword CalcQuench0D::get_scenario_circuit()const{
		return scenario_circuit_;
	}

	arma::uword CalcQuench0D::get_scenario_coil()const{
		return scenario_coil_;
	}

	fltp CalcQuench0D::get_num_nz_parallel()const{
		return num_nz_parallel_;
	}

	void CalcQuench0D::set_num_nz_parallel(const fltp num_nz_parallel){
		num_nz_parallel_  = num_nz_parallel;
	}

	fltp CalcQuench0D::get_temperature_bias()const{
		return temperature_bias_;
	}
	
	arma::uword CalcQuench0D::get_max_iter()const{
		return max_iter_;
	}


	// calculate with quench data output
	// assumed is that the coil quenches at the given time
	std::list<ShQuench0DDataPr> CalcQuench0D::calculate_quench(
		const fltp time,
		const cmn::ShLogPr &lg,
		const ShSolverCachePr& cache){

		// LOGGER HEADER
		// get date and time
		auto timdata = std::time(nullptr);
		auto localtime = *std::localtime(&timdata);
		std::ostringstream date_stream,time_stream;
		date_stream << std::put_time(&localtime, "%d-%m-%Y");
		time_stream << std::put_time(&localtime, "%H:%M:%S");
		const std::string date_str = date_stream.str();
		const std::string time_str = time_stream.str();

		// general info
		lg->msg("%s=== Starting Quench Calculation ===%s\n",KGRN,KNRM);
		lg->msg(2,"%sGENERAL INFO%s\n",KGRN,KNRM);
		lg->msg("date: %s%s%s %s(dd-mm-yyyy)%s\n",KYEL,date_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("time: %s%s%s %s(hh:mm:ss)%s\n",KYEL,time_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("model name: %s%s%s\n",KYEL,model_->get_name().c_str(),KNRM);
		lg->msg("path name: %s%s%s\n",KYEL,get_name().c_str(),KNRM);
		lg->msg(-2,"\n");


		// MESH CALCULATION
		// we want to calculate the field from each 
		// coil onto each other coil here, this would 
		// allow to create a simplistic field map
		// report
		lg->msg(2,"%sBUILDING FIELD MAP%s\n",KGRN,KNRM);

		// check input model
		if(model_==NULL)rat_throw_line("model not set");
		model_->is_valid(true);

		// calculate field on mesh
		const ShCalcMeshPr calc_mesh = CalcMesh::create(model_);
		calc_mesh->set_settings(stngs_);
		calc_mesh->set_circuits(circuits_);

		// calculate field on mesh
		const std::list<ShMeshDataPr> mesh_data = 
			calc_mesh->calculate_mesh(time,cmn::NullLog::create(),cache);

		// break for cancellation
		if(lg->is_cancelled())return{};

		// extract coils
		const std::list<ShCoilDataPr> coil_data = mdl::Extra::filter<CoilData>(mesh_data);

		// check if all meshes have material
		for(auto it=coil_data.begin();it!=coil_data.end();it++)
			if((*it)->get_material()==NULL)
				rat_throw_line("material not set for: " + (*it)->get_name());

		// convert circuits and coils to field arrays
		const arma::field<ShCoilDataPr> coil_data_fld = mdl::Extra::list2field(coil_data);
		const arma::field<ShCircuitPr> circuits_fld = mdl::Extra::map2field(circuits_);

		// get number of circuits and number of coils
		const arma::uword num_coils = coil_data_fld.n_elem;
		const arma::uword num_circuits = circuits_fld.n_elem;

		// circuit map
		std::map<arma::uword, arma::uword> circuit_map;
		for(arma::uword i=0;i<num_circuits;i++)
			circuit_map[circuits_fld(i)->get_circuit_id()] = i;

		// get data from ciercuits
		arma::field<std::string> circuit_names(num_circuits);
		arma::Row<fltp> circuit_start_current(num_circuits); 
		arma::Row<fltp> heater_delay_times(num_circuits);
		arma::Row<fltp> switching_delay_times(num_circuits);
		arma::Row<fltp> detection_voltage(num_circuits);
		arma::Row<fltp> detection_delay_time(num_circuits);
		arma::Row<fltp> fraction_hit(num_circuits);
		arma::Row<arma::uword> is_rapid(num_circuits);
		for(arma::uword i=0;i<num_circuits;i++){
			circuit_names(i) = circuits_fld(i)->get_name();
			circuit_start_current(i) = circuits_fld(i)->get_current(time);
			heater_delay_times(i) = circuits_fld(i)->get_heater_delay_time();
			switching_delay_times(i) = circuits_fld(i)->get_switching_time();
			detection_delay_time(i) = circuits_fld(i)->get_detection_delay_time();
			detection_voltage(i) = circuits_fld(i)->get_detection_voltage();
			fraction_hit(i) = circuits_fld(i)->get_fraction_hit();
			is_rapid(i) = circuits_fld(i)->get_is_rapid();
		}

		// get coil data
		arma::field<std::string> coil_names(num_coils);
		arma::Row<fltp> coil_start_temperature(num_coils);
		arma::Row<fltp> turn_area(num_coils);
		arma::Row<arma::uword> circuit_index(num_coils);
		arma::Row<arma::uword> is_sc(num_coils);
		arma::Row<fltp> flux_density(num_coils);
		arma::Row<fltp> alpha(num_coils);
		arma::Row<fltp> cable_length(num_coils);
		arma::Row<fltp> coil_volume(num_coils);
		arma::Col<arma::uword> coil_count(num_circuits,arma::fill::zeros);
		for(arma::uword i=0;i<num_coils;i++){
			// find point in coil with least margin
			const arma::Row<fltp> gamma = coil_data_fld(i)->calc_critical_current_fraction(false,MeshData::FieldAngleType::CONE,true);
			const arma::uword idx_quench = gamma.index_min();
			flux_density(i) = arma::as_scalar(cmn::Extra::vec_norm(coil_data_fld(i)->get_field('B').col(idx_quench)));
			alpha(i) = arma::as_scalar(coil_data_fld(i)->calc_magnetic_field_angle().col(idx_quench));
			coil_names(i) = coil_data_fld(i)->get_name();
			coil_start_temperature(i) = coil_data_fld(i)->get_operating_temperature();
			turn_area(i) = arma::min(coil_data_fld(i)->calc_cis_total_area())/coil_data_fld(i)->get_number_turns();
			cable_length(i) = coil_data_fld(i)->calc_ell()*coil_data_fld(i)->get_number_turns();
			coil_volume(i) = coil_data_fld(i)->calc_total_volume();
			is_sc(i) = coil_data_fld(i)->get_material()->is_superconductor();

			// try to connect coil to circuit
			const auto it = circuit_map.find(coil_data_fld(i)->get_circuit_index());
			if(it==circuit_map.end())rat_throw_line("one or more coils have no circuit: " + 
				std::to_string(coil_data_fld(i)->get_circuit_index()));
			circuit_index(i) = (*it).second;
			coil_count((*it).second)++;
		}

		// count coils in circuits
		if(arma::any(coil_count==0))rat_throw_line("one or more circuits have no coils");

		lg->msg(-2,"\n");


		// INDUCTANCE MATRIX CALCULATION
		// report
		lg->msg(2,"%sCALCULATE INDUCTANCE%s\n",KGRN,KNRM);

		// perform inductance calculation
		const ShCalcInductancePr calc_inductance = CalcInductance::create(model_);
		calc_inductance->set_settings(stngs_);
		calc_inductance->set_group_circuits();
		calc_inductance->set_circuits(circuits_);
		calc_inductance->set_use_only_linear(); // TODO non-linear iron

		// calculate inductance (at start time)
		const ShInductanceDataPr inductance_data = 
			calc_inductance->calculate_inductance(time,cmn::NullLog::create(),cache);

		// break for cancellation
		if(lg->is_cancelled())return{};

		// get inductance matrix
		const arma::Mat<fltp>& full_inductance_matrix = inductance_data->get_matrix();
		assert(full_inductance_matrix.n_cols==num_circuits); 
		assert(full_inductance_matrix.n_rows==num_circuits);

		// get circuit id's used to calculate the inductance matrix
		const arma::Col<arma::uword>& inductance_matrix_circuit_id = inductance_data->get_circuit_indices();

		// allocate reduced inductance matrix
		arma::Mat<fltp> M(num_circuits,num_circuits);

		// match circuit id's to the circuits we have here
		for(arma::uword i=0;i<num_circuits;i++){
			for(arma::uword j=0;j<num_circuits;j++){
				// find columns and rows matching this submat
				const arma::Col<arma::uword> idx_match_i = arma::find(inductance_matrix_circuit_id==circuits_fld(i)->get_circuit_id());
				const arma::Col<arma::uword> idx_match_j = arma::find(inductance_matrix_circuit_id==circuits_fld(j)->get_circuit_id());

				// add together and store
				M(i,j) = arma::accu(full_inductance_matrix.submat(idx_match_i,idx_match_j));
			}
		}

		// factorise inductance matrix using LU decomposition
		arma::Mat<fltp> L,U,P;
		arma::lu(L,U,P,M);

		// break for cancellation
		if(lg->is_cancelled())return{};

		// done
		lg->msg(-2,"\n");

		// create a list of coils to quench
		arma::Row<arma::uword> nz_idx;
		switch(scenario_){
			case Scenario::FIRST:{
				nz_idx.set_size(1); nz_idx(0) = 0;
				break;
			}
			case Scenario::LAST:{
				nz_idx.set_size(1); nz_idx(0) = num_coils-1;
				break;
			}
			case Scenario::INDEX:{
				nz_idx.zeros(1);
				arma::uword cnt = 0;
				if(scenario_circuit_==0)rat_throw_line("circuit index not set");
				if(scenario_coil_==0)rat_throw_line("coil index not set");
				for(arma::uword i=0;i<num_coils;i++){
					if(coil_data_fld(i)->get_circuit_index()==scenario_circuit_){
						cnt++;
						if(cnt==scenario_coil_)nz_idx(0) = i;
					}
				}
				if(cnt<scenario_coil_)rat_throw_line("could not find coil index for this circuit");
				break;
			}
			case Scenario::WEAKEST:{
				nz_idx.set_size(1);
				fltp Jclow = arma::Datum<fltp>::inf;
				for(arma::uword i=0;i<num_coils;i++){
					if(!coil_data_fld(i)->get_material()->is_superconductor())continue;
					const fltp Jc = coil_data_fld(i)->get_material()->calc_critical_current_density(
						coil_start_temperature(i),flux_density(i),alpha(i));
					if(Jc<Jclow){
						nz_idx(0) = i;
						Jclow = Jc;
					}
				}
				break;
			}
			case Scenario::ALL:{
				nz_idx = arma::regspace<arma::Row<arma::uword> >(0,num_coils-1);
				break;
			}
			default:{
				rat_throw_line("scenario not recognized");
			}
		}

		// SOLVE STEP
		// create output
		std::list<ShQuench0DDataPr> quench_data;

		// walk over superconducting coils
		// a simulation is run for a quench 
		// in each superconducting coil
		for(arma::uword k=0;k<nz_idx.n_elem;k++){

			// get index of quenching coil
			const arma::uword i=nz_idx(k);

			// get coil reference
			const ShCoilDataPr& mycoil = coil_data_fld(i);

			// only run model for s.c. coils
			if(!mycoil->get_material()->is_superconductor())continue;

			// check if has any current
			if(circuit_start_current(circuit_index(i))==RAT_CONST(0.0))continue;

			// get info on the coil
			const std::string& name = mycoil->get_name();

			// report
			lg->msg(2,"%sQUENCH (%s)%s\n",KGRN,name.c_str(),KNRM);

			// find point in coil with least margin
			// const arma::uword idx_quench = mycoil->calc_critical_current_fraction(false,true).index_min();

			// magnetic field at hotspot position
			const fltp hotspot_flux_density = flux_density(i); //arma::as_scalar(cmn::Extra::vec_norm(mycoil->get_field('B').col(idx_quench)));

			// field angle at hotspot position
			const fltp hotspot_alpha = alpha(i); //arma::as_scalar(mycoil->calc_magnetic_field_angle().col(idx_quench));

			// get start temperature for the hotspot
			const fltp hotspot_start_temperature = coil_start_temperature(i);

			// current density in hotspot
			const fltp hotspot_start_current_density = circuit_start_current(circuit_index(i))/turn_area(i);

			// critical current scaling factor
			const fltp fscale = RAT_CONST(1.0);

			// normal zone velocity
			const fltp normal_zone_propagation_velocity = mycoil->get_material()->calc_theoretical_vnzp(
				hotspot_start_current_density,hotspot_start_temperature,hotspot_flux_density,hotspot_alpha,fscale);
			if(normal_zone_propagation_velocity<0)rat_throw_line("normal zone velocity is less than zero");

			// length of the minimal propagation zone
			const fltp minimal_propagation_zone_length = mycoil->get_material()->calc_theoretical_lmpz(
				hotspot_start_current_density,hotspot_start_temperature,hotspot_flux_density,hotspot_alpha,fscale);
			if(minimal_propagation_zone_length<0)rat_throw_line("minimal propagation zone length is less than zero");

			// minimal quench energy
			const fltp minimal_quench_energy = mycoil->get_material()->calc_theoretical_mqe(
				hotspot_start_current_density,hotspot_start_temperature,hotspot_flux_density,
				hotspot_alpha,fscale,mat::Direction::LONGITUDINAL,turn_area(i));
			if(minimal_propagation_zone_length<0)rat_throw_line("minimal propagation zone length is less than zero");

			// // current sharing temperature
			// const fltp current_sharing_temperature = mycoil->get_material()->calc_cs_temperature(
			// 	hotspot_start_current_density, hotspot_flux_density, hotspot_alpha);

			// transverse propagation fraction
			const fltp transverse_propagation_fraction = circuits_fld(circuit_index(i))->get_transverse_propagation_fraction();

			// this magnet is over critical temperature already
			if(normal_zone_propagation_velocity==0)continue;

			// estimate time to reach specified temperature
			const fltp estimated_end_time = mycoil->get_material()->calc_adiabatic_time(
				std::min(1000.0, stop_temperature_), hotspot_start_current_density, 
				hotspot_start_temperature, hotspot_flux_density, hotspot_alpha);
			if(estimated_end_time<0)rat_throw_line("estimated end time is less than zero");

			// setup normal zone
			const fltp length_safety_factor = RAT_CONST(1.4);
			const fltp nz_model_length = nzp_length_<=RAT_CONST(0.0) ? std::max(RAT_CONST(0.3),
				length_safety_factor*estimated_end_time*normal_zone_propagation_velocity) : nzp_length_;
			
			// calculate number of nodes
			const arma::uword num_nzp = (!use_nzp1d_ || nzp_element_size_==0.0) ? 1llu : std::max(static_cast<arma::uword>(std::ceil(nz_model_length/nzp_element_size_)),2llu);

			// create coordinates
			const arma::Row<fltp> xnz = arma::linspace<arma::Row<fltp> >(RAT_CONST(0.0), nz_model_length, num_nzp);
			const arma::Row<fltp> dxnz = arma::diff(xnz,1,1);

			// calculate the volume of the nodes
			arma::Row<fltp> node_volume(num_nzp,arma::fill::zeros); // [m^3]
			node_volume.head_cols(num_nzp-1) += turn_area(i)*dxnz/2; 
			node_volume.tail_cols(num_nzp-1) += turn_area(i)*dxnz/2;

			// scaling array with broken first elements to start the hotspot
			arma::Row<fltp> critical_current_scaling(num_nzp-1, arma::fill::ones);
			critical_current_scaling(arma::find(xnz<minimal_propagation_zone_length/2)).zeros();

			// create runge kutta 
			const cmn::ShRungeKuttaPr runge_kutta = cmn::RungeKutta::create(integration_method_);
			runge_kutta->set_reltol(reltol_); runge_kutta->set_abstol(abstol_);
			runge_kutta->set_max_iter(max_iter_);

			// print butcher's tableau
			runge_kutta->display_butchers_tableau(lg);

			// function for calculating normal zone voltage
			std::function<fltp(const fltp tt, const arma::Mat<fltp>&yy)> normal_zone_voltage_fun;

			// adiabatic hotspot 
			if(num_nzp==1)normal_zone_voltage_fun = [&](const fltp /*tt*/, const arma::Mat<fltp>&yy){
					// separate variables
					const fltp hotspot_temperature = yy(0);
					const arma::Row<fltp> average_temperature = yy.cols(num_nzp,num_nzp+num_coils-1);
					const arma::Row<fltp> operating_current = yy.cols(num_nzp+num_coils,num_nzp+num_coils+num_circuits-1);
					const fltp normal_zone_length = yy(num_nzp+num_coils+2*num_circuits+1);

					// voltage drop
					const fltp electrical_resistivity = mycoil->get_material()->calc_electrical_resistivity(
						((RAT_CONST(1.0)-temperature_bias_)*hotspot_start_temperature + 
						temperature_bias_*hotspot_temperature),hotspot_flux_density); // Ohm m

					// calculate normal zone resistance
					const fltp normal_zone_resistance = std::max(
						electrical_resistivity*normal_zone_length/turn_area(i),
						electrical_resistivity*std::min(coil_volume(i),
							((RAT_CONST(4.0)/3)*arma::Datum<fltp>::pi*cmn::Extra::square(
							(normal_zone_length/2)*transverse_propagation_fraction)*
							(normal_zone_length/2))/turn_area(i))/turn_area(i)); // Ohm

					// calculate voltage
					const fltp Vnz = num_nz_parallel_*normal_zone_resistance*operating_current(circuit_index(i));

					// return normal zone voltage
					return Vnz;
				};

			// one dimensional hotspot
			else if(num_nzp>1)normal_zone_voltage_fun = [&](const fltp /*tt*/, const arma::Mat<fltp>&yy){
					// separate variables
					const arma::Row<fltp> hotspot_node_temperature = yy.cols(0,num_nzp-1);
					const arma::Row<fltp> average_temperature = yy.cols(num_nzp,num_nzp+num_coils-1);
					const arma::Row<fltp> operating_current = yy.cols(num_nzp+num_coils,num_nzp+num_coils+num_circuits-1);

					// get current density
					const fltp hotspot_operating_current_density = 
						operating_current(circuit_index(i))/turn_area(i); // [A/m^2]

					// calculate element temperature
					const arma::Row<fltp> hotspot_element_temperature = 
						(hotspot_node_temperature.head_cols(num_nzp-1) + 
						hotspot_node_temperature.tail_cols(num_nzp-1))/2; // [K]

					// voltage drop
					const arma::Row<fltp> hotspot_electric_field = mycoil->get_material()->calc_electric_field_mat(
						arma::Row<fltp>(num_nzp-1, arma::fill::value(hotspot_operating_current_density)), 
						hotspot_element_temperature,
						arma::Row<fltp>(num_nzp-1, arma::fill::value(hotspot_flux_density)),
						arma::Row<fltp>(num_nzp-1, arma::fill::value(hotspot_alpha)),critical_current_scaling); // [V/m]

					// calculate voltage
					const fltp Vnz = 2*num_nz_parallel_*arma::accu(hotspot_electric_field%dxnz);

					// return normal zone voltage
					return Vnz;
				};

			// otherwise
			else rat_throw_line("normal zone must have at least one node");
			


			// start condition not yet switched and not yet heatered
			bool terminate_integration = false;
			arma::Row<arma::uword> switched(num_circuits,arma::fill::zeros);
			arma::Row<arma::uword> heatered(num_circuits,arma::fill::zeros);

			// check treshold voltage
			switched(arma::find(detection_voltage<=0)).fill(true);

			// set system function
			runge_kutta->set_system_fun([&](const fltp tt, const arma::Mat<fltp>&yy){
				// separate variables
				const arma::Row<fltp> hotspot_node_temperature = arma::clamp(yy.cols(0,num_nzp-1),0.0,arma::Datum<fltp>::inf);
				const arma::Row<fltp> average_temperature = yy.cols(num_nzp,num_nzp+num_coils-1);
				const arma::Row<fltp> operating_current = yy.cols(num_nzp+num_coils,num_nzp+num_coils+num_circuits-1);

				// get heat capacity
				const arma::Row<fltp> hotspot_volumetric_specific_heat = 
					mycoil->get_material()->calc_volumetric_specific_heat_mat(hotspot_node_temperature); // J/(m^3 K)
				const fltp hotspot_operating_current_density = operating_current(circuit_index(i))/turn_area(i);
				assert(arma::all(hotspot_volumetric_specific_heat>0));

				// allocate hotspot temperature change
				arma::Row<fltp> hotspot_temperature_change(num_nzp,arma::fill::zeros);

				// calculate change of temperature
				// 0D model: only Ohmic heating
				if(num_nzp==1){
					const fltp hotspot_electrical_resistivity = 
						mycoil->get_material()->calc_electrical_resistivity(
						hotspot_node_temperature(0),hotspot_flux_density); // Ohm m
					assert(hotspot_electrical_resistivity>0);
					hotspot_temperature_change(0) = 
						hotspot_electrical_resistivity*
						cmn::Extra::square(hotspot_operating_current_density)/
						hotspot_volumetric_specific_heat(0); // K/s
					assert(hotspot_temperature_change.is_finite());
				}

				// 1D model: Ohmic heating + temperature gradient
				else if(num_nzp>1){
					// temperature at elements averaged between nodes
					const arma::Row<fltp> hotspot_element_temperature = 
						(hotspot_node_temperature.head_cols(num_nzp-1) + 
						hotspot_node_temperature.tail_cols(num_nzp-1))/2; // [K]

					// thermal conductivity at elements
					const arma::Row<fltp> hotspot_thermal_conductivity = 
						mycoil->get_material()->calc_thermal_conductivity_mat(
						hotspot_element_temperature, arma::Row<fltp>(num_nzp-1, arma::fill::value(hotspot_flux_density))); // [W/(Km)]
					assert(hotspot_thermal_conductivity.is_finite());
					assert(arma::all(hotspot_thermal_conductivity>0));

					// difference in temperature between neighbouring nodes
					const arma::Row<fltp> dT = -arma::diff(hotspot_node_temperature,1,1); // [K]
					assert(dT.is_finite());

					// calculate power at the elements
					const arma::Row<fltp> hotspot_electric_field = mycoil->get_material()->calc_electric_field_mat(
						arma::Row<fltp>(num_nzp-1, arma::fill::value(hotspot_operating_current_density)), 
						hotspot_element_temperature,
						arma::Row<fltp>(num_nzp-1, arma::fill::value(hotspot_flux_density)),
						arma::Row<fltp>(num_nzp-1, arma::fill::value(hotspot_alpha)),
						critical_current_scaling); // [Ohm m]
					const arma::Row<fltp> element_power = operating_current(circuit_index(i))*hotspot_electric_field%dxnz; // [W]
					assert(element_power.is_finite());

					// divide element power over neighbouring nodes
					arma::Row<fltp> node_power(num_nzp,arma::fill::zeros); 
					node_power.head_cols(num_nzp-1) += element_power/2 - turn_area(i)*(dT%hotspot_thermal_conductivity/dxnz); // [W]
					node_power.tail_cols(num_nzp-1) += element_power/2 + turn_area(i)*(dT%hotspot_thermal_conductivity/dxnz); // [W]
					assert(node_power.is_finite());

					// calculate heat capacity of nodes
					const arma::Row<fltp> node_heat_capacity = hotspot_volumetric_specific_heat%node_volume; // [J/K]
					assert(node_heat_capacity.is_finite());
					assert(arma::all(node_heat_capacity>0));

					// temperature change
					hotspot_temperature_change = node_power/node_heat_capacity; // [K/s]
					assert(hotspot_temperature_change.is_finite());
				}

				// otherwise
				else rat_throw_line("normal zone must have at least one node");

				// change of average temperature
				arma::Row<fltp> temperature_change(num_coils,arma::fill::zeros);

				// voltage drop driving the current down
				arma::Col<fltp> voltage_drop(num_circuits,arma::fill::zeros);

				// walk over coils
				for(arma::uword j=0;j<num_coils;j++){
					// get material of this coil
					const mat::ShConductorPr& mat = coil_data_fld(j)->get_material();
					assert(mat!=NULL);

					// check if this circuit was heatered
					if(heatered(circuit_index(j)) || !is_sc(j)){
						// material properties
						const fltp coil_volumetric_specific_heat = 
							mat->calc_volumetric_specific_heat(average_temperature(j)); // J/(m^3 K)
						const fltp coil_electrical_resistivity = 
							mat->calc_electrical_resistivity(average_temperature(j),flux_density(j)/2); // Ohm m

						// add voltage drop contribution
						voltage_drop(circuit_index(j)) -= operating_current(circuit_index(j))*
							coil_electrical_resistivity*fraction_hit(circuit_index(j))*cable_length(j)/turn_area(j);

						// temperature rise
						const fltp coil_operating_current_density = operating_current(circuit_index(j))/turn_area(j);

						// calcultae change of temperature in the coil
						temperature_change(j) += coil_electrical_resistivity*
							cmn::Extra::square(coil_operating_current_density)/coil_volumetric_specific_heat;
					}

					// in case of rapid but not switched
					else if(is_rapid(circuit_index(j))){
						// temperature rise
						const fltp coil_operating_current_density = operating_current(circuit_index(j))/turn_area(j);

						// calculate electric field
						const fltp coil_electric_field = mat->calc_electric_field(
							coil_operating_current_density,
							average_temperature(j),flux_density(j)/2,alpha(j), // division by two to be debated (better average field maybe?)
							heatered(circuit_index(j)) ? 0.0 : 1.0);

						// add voltage drop contribution
						voltage_drop(circuit_index(j)) -= coil_electric_field*fraction_hit(circuit_index(j))*cable_length(j);

						// calculate specific heat
						const fltp coil_volumetric_specific_heat = mat->calc_volumetric_specific_heat(average_temperature(j)); // J/(m^3 K)

						// calculate power density
						const fltp power_density = coil_operating_current_density*coil_electric_field; // A/m^2 * V/m  = VA/m^3 = W/m^3

						// change of temperature
						temperature_change(j) += power_density/coil_volumetric_specific_heat; // W/m^3 / (J/(Km^3)) = 1/s / 1/(K) = K/s
					}

					// rapid protection system (dumping energy from resistor in coil)
					if(is_rapid(circuit_index(j)) && switched(circuit_index(j))){
						// calculate
						const fltp coil_volumetric_specific_heat = mat->calc_volumetric_specific_heat(average_temperature(j)); // J/(m^3 K)
						const fltp circuit_operating_current = operating_current(circuit_index(j));
						const fltp dump_voltage = circuits_fld(circuit_index(j))->calc_protection_voltage(circuit_operating_current);
						const fltp dump_power = circuit_operating_current*dump_voltage;

						// fraction of volume this coil contributes to the volume of all coils in this circuit
						const fltp volume_fraction = coil_volume(j)/arma::accu(coil_volume(arma::find(circuit_index==circuit_index(j))));
						
						// the tempertaure change due to radial current heating
						temperature_change(j) += volume_fraction*dump_power/(fraction_hit(circuit_index(j))*coil_volume(j)*coil_volumetric_specific_heat);

						// if coil equals quenching coil the peak temperature changes as well
						if(i==j)hotspot_temperature_change += volume_fraction*dump_power/(fraction_hit(circuit_index(j))*coil_volume(j)*hotspot_volumetric_specific_heat);
					}
				}

				// power to dump
				arma::Row<fltp> drive_power(num_circuits, arma::fill::zeros); // for voltage calculation
				fltp external_power = RAT_CONST(0.0); // this is power not converted into heat inside the coil pack

				// voltage drop due to protection system
				for(arma::uword j=0;j<num_circuits;j++){
					if(switched(j)){
						// calculate voltage due to external resistor or varistor
						const fltp v = circuits_fld(j)->calc_protection_voltage(operating_current(j));
						voltage_drop(j) -= v;

						// the energy is dumped in external resistor or varistor
						if(!is_rapid(j))external_power += v*operating_current(j);

						// power on dump side
						drive_power(j) += v*operating_current(j); // the power supply is adding this to the coil so positive
					}
				}

				// voltage drop due to hotspot propagation
				// note that if rapid is used this voltage may be counted double here
				if(!heatered(circuit_index(i))){
					// calculate normal zone voltage
					const fltp Vnz = normal_zone_voltage_fun(tt,yy);

					// in case of RAPID take strongest term
					if(is_rapid(circuit_index(i)) && switched(circuit_index(i))){
						if((operating_current(circuit_index(i))>0 && Vnz<voltage_drop(circuit_index(i))) ||
							(operating_current(circuit_index(i))<=0 && Vnz>voltage_drop(circuit_index(i)))){
							const fltp delta_v = voltage_drop(circuit_index(i)) + Vnz;
							voltage_drop(circuit_index(i)) -= delta_v;
							external_power += delta_v*operating_current(circuit_index(i));
						}
					}

					// the coil is ramping down by normal zone voltage
					else{
						voltage_drop(circuit_index(i)) -= Vnz;
						external_power += Vnz*operating_current(circuit_index(i));
					}
				}

				// allocate change of currents
				// arma::Row<fltp> current_change = arma::solve(U, arma::solve(L, P.t()*voltage_drop)).t();
				arma::Row<fltp> current_change = arma::solve(U, arma::solve(L, P * voltage_drop)).t();
				assert(current_change.is_finite());

				// still on power supply (not yet switched)
				const arma::Col<arma::uword> idx_on_psu = arma::find(switched==0);
				current_change(idx_on_psu).fill(0.0);
				drive_power(idx_on_psu) += voltage_drop(idx_on_psu)%operating_current(idx_on_psu);
				assert(current_change.is_finite());
				assert(drive_power.is_finite());
				assert(hotspot_temperature_change.is_finite());

				// normal zone velocity
				const fltp operating_vnzp = mycoil->get_material()->calc_theoretical_vnzp(
					hotspot_operating_current_density,hotspot_start_temperature,hotspot_flux_density,hotspot_alpha,RAT_CONST(1.0));
				if(normal_zone_propagation_velocity<0)rat_throw_line("normal zone velocity is less than zero");

				// calculate change of system variables
				const arma::Row<fltp> dy = arma::join_horiz(
					hotspot_temperature_change,
					arma::join_horiz(temperature_change,current_change,drive_power),
					arma::Row<fltp>{external_power,2*operating_vnzp});
				assert(dy.is_finite());

				// join and return
				return dy;
			});

			// terminal output
			runge_kutta->set_monitor_fun([&lg](
				const arma::uword iter, const rat::fltp tt, 
				const rat::fltp dt, const arma::sword event_detected){

				// draw header
				if(iter%100==0){
					lg->msg("%s%s%4s %9s %9s%s\n",KBLD,KCYN,"iter","time","dstep",KNRM);
					lg->hline(4+9+9+2,'=',KCYN,KNRM);
				}

				// draw table line
				if(iter%5==0 || event_detected>=0)
					lg->msg("%s%04llu %+9.2e %+9.2e%s\n",KCYN,iter,tt,dt,KNRM);

				// check for cancelling
				if(lg->is_cancelled())return -1;

				// done
				return 0;
			});


			// root finding for quench detection
			runge_kutta->set_event_fun([&](const fltp tt, const arma::Mat<fltp>&yy){
				// separate variables
				const fltp hotspot_temperature = arma::as_scalar(arma::max(yy.cols(0,num_nzp-1),1));
				const arma::Row<fltp> average_temperature = yy.cols(num_nzp,num_nzp+num_coils-1);
				const arma::Row<fltp> operating_current = yy.cols(num_nzp+num_coils,num_nzp+num_coils+num_circuits-1);

				// calculate voltage
				// normal_zone_resistance*operating_current(circuit_index(i));
				const fltp Vnz = std::abs(normal_zone_voltage_fun(tt,yy)); 

				// stop conditions
				const fltp cnd0 = Vnz - detection_voltage(circuit_index(i));
				const fltp cnd1 = arma::max(arma::abs(operating_current) - minimum_current_fraction_*arma::clamp(arma::abs(circuit_start_current),1.0,arma::Datum<fltp>::inf));
				const fltp cnd2 = hotspot_temperature - stop_temperature_;

				// return stop conditions
				return arma::Row<fltp>{cnd0,cnd1,cnd2};
			});

			// peak temperature and current start positions
			arma::Row<fltp> y0 = arma::join_horiz(
				arma::Row<fltp>(num_nzp,arma::fill::value(hotspot_start_temperature)),
				arma::join_horiz(coil_start_temperature,circuit_start_current,arma::Row<fltp>(num_circuits,arma::fill::zeros)),
				arma::Row<fltp>{RAT_CONST(0.0),minimal_propagation_zone_length});

			// report
			lg->msg(2,"%sIntegrate Until Trigger%s\n",KBLU,KNRM);

			// allocate runge-kutta data collection for storing solutions
			std::list<cmn::RKData> solution;

			// normal zone voltage at t0
			const fltp Vnz0 = std::abs(normal_zone_voltage_fun(RAT_CONST(0.0),y0)); 

			// add start solution
			if(Vnz0>=detection_voltage(circuit_index(i)))
				solution.push_back({{RAT_CONST(0.0)},y0,arma::Row<fltp>(y0.n_elem,arma::fill::zeros),0ll});

			// solve to protection trigger
			else solution.push_back(runge_kutta->integrate(0.0,stop_time_,y0));

			// break for cancellation
			if(lg->is_cancelled())return{};

			// check if event detected
			if(solution.back().event_detected==0){
				lg->msg("%s<<< Voltage Treshold Exceeded >>>%s\n",KYEL,KNRM);
			}else if(solution.back().event_detected==1){
				lg->msg("%s<<< Current Below Treshold >>>%s\n",KYEL,KNRM); 
				terminate_integration = true;
			}else if(solution.back().event_detected==2){
				lg->msg("%s<<< Reached Stop Temperature >>>%s\n",KYEL,KNRM); 
				terminate_integration = true;
			}else if(solution.back().tt.n_elem==runge_kutta->get_max_iter()+1){
				lg->msg("%s<<< Maximum number of Iterations reached >>>%s\n",KYEL,KNRM); 
				terminate_integration = true;
			}else{
				lg->msg("%s<<< End Time Reached >>>%s\n",KYEL,KNRM); 
				terminate_integration = true;
			}


			// store
			lg->msg(-2,"\n");

			// update event fun to not trigger on voltage
			runge_kutta->set_event_fun([&](const fltp /*tt*/, const arma::Mat<fltp>&yy){
				// separate variables
				const fltp hotspot_temperature = arma::as_scalar(arma::max(yy.cols(0,num_nzp-1),1));
				// const arma::Row<fltp> average_temperature = yy.cols(1,1+num_coils-1);
				const arma::Row<fltp> operating_current = yy.cols(num_nzp+num_coils,num_nzp+num_coils+num_circuits-1);
				const fltp cnd0 = arma::min(arma::abs(operating_current) - minimum_current_fraction_*arma::abs(circuit_start_current));
				const fltp cnd1 = hotspot_temperature - stop_temperature_;
				return arma::Row<fltp>{cnd0,cnd1};
			});

			// calculate switching time
			const fltp treshold_time = solution.back().tt.back();
			const fltp detection_time = treshold_time + detection_delay_time(circuit_index(i));
			
			// get times
			const arma::Row<fltp> switching_times = detection_time + switching_delay_times; 
			const arma::Row<fltp> heater_times = detection_time + heater_delay_times; 
				
			// sort switches and heaters
			const arma::Col<arma::uword> switch_order = arma::sort_index(switching_times);
			const arma::Col<arma::uword> heater_order = arma::sort_index(heater_times);

			// keep track of last switch and heater
			arma::uword switch_cnt = 0; arma::uword heater_cnt = 0;

			// break for cancellation
			if(lg->is_cancelled())return{};

			// // solve to switch time
			// lg->msg(2,"%sIntegrate Until Quench Detected%s\n",KBLU,KNRM);

			// // solve to detection time and then turn the power supply off
			// solution.push_back(runge_kutta->integrate(
			// 	solution.back().tt.back(),
			// 	detection_time,
			// 	solution.back().yy.tail_rows(1),
			// 	solution.back().yp.tail_rows(1)));
			// power_supplies_off = true;

			// // done
			// lg->msg(-2,"\n");

			// // break for cancellation
			// if(lg->is_cancelled())return{};

			// walk over heater times
			while(terminate_integration==false){
				// get next times
				const fltp next_switch_time = switch_cnt<switching_times.n_elem ? switching_times(switch_order(switch_cnt)) : 1e99;
				const fltp next_heater_time = heater_cnt<heater_times.n_elem ? heater_times(heater_order(heater_cnt)) : 1e99;

				// check if requested times are beyond the end of the solution
				if(next_switch_time>stop_time_ && next_heater_time>stop_time_)break;

				// next event is a switch
				if(next_switch_time<next_heater_time){
					// solve to switch time
					lg->msg(2,"%sIntegrate Until Switch%s\n",KBLU,KNRM);

					// run runge kutta
					solution.push_back(runge_kutta->integrate(
						solution.back().tt.back(),
						switching_times(switch_order(switch_cnt)),
						solution.back().yy.tail_rows(1),
						solution.back().yp.tail_rows(1)));

					// break for cancellation
					if(lg->is_cancelled())return{};

					// check if event detected
					if(solution.back().event_detected==-1){
						lg->msg("%s<<< Switching Protection (%llu) >>>%s\n",KYEL,switch_order(switch_cnt),KNRM);
						switched(switch_order(switch_cnt)) = true; 
					}else if(solution.back().event_detected==0){
						lg->msg("%s<<< Current Below Treshold >>>%s\n",KYEL,KNRM); 
						terminate_integration = true;
					}else if(solution.back().event_detected==1){
						lg->msg("%s<<< Reached Stop Temperature >>>%s\n",KYEL,KNRM); 
						terminate_integration = true;
					}else if(solution.back().tt.n_elem==runge_kutta->get_max_iter()){
						lg->msg("%s<<< Maximum number of Iterations reached >>>%s\n",KYEL,KNRM); 
						terminate_integration = true;
					}

					// done
					lg->msg(-2,"\n");

					// increment switch counter
					switch_cnt++;
				}

				// next event is a heater
				else{
					// solve to switch time
					lg->msg(2,"%sIntegrate Until Heater Hit%s\n",KBLU,KNRM);

					// run runge kutta
					solution.push_back(runge_kutta->integrate(
						solution.back().tt.back(),
						heater_times(heater_order(heater_cnt)),
						solution.back().yy.tail_rows(1),
						solution.back().yp.tail_rows(1)));

					// break for cancellation
					if(lg->is_cancelled())return{};

					// check if event detected
					if(solution.back().event_detected==-1){
						lg->msg("%s<<< Firing Heater (%llu) >>>%s\n",KYEL,heater_order(heater_cnt),KNRM);
						heatered(heater_order(heater_cnt)) = true; 
					}else if(solution.back().event_detected==0){
						lg->msg("%s<<< Current Below Treshold >>>%s\n",KYEL,KNRM); 
						terminate_integration = true;
					}else if(solution.back().event_detected==1){
						lg->msg("%s<<< Reached Stop Temperature >>>%s\n",KYEL,KNRM); 
						terminate_integration = true;
					}else if(solution.back().tt.n_elem==runge_kutta->get_max_iter()+1){
						lg->msg("%s<<< Maximum number of Iterations reached >>>%s\n",KYEL,KNRM); 
						terminate_integration = true;
					}

					// done
					lg->msg(-2,"\n");

					// increment heater counter
					heater_cnt++;
				}
				
				// break for cancellation
				if(lg->is_cancelled())return{};

				// check finished
				if(switch_cnt==switching_times.n_elem && heater_cnt==heater_times.n_elem)break;
			}

			// solve to end
			if(terminate_integration==false){
				// report
				lg->msg(2,"%sIntegrate Until End%s\n",KBLU,KNRM);

				// run runge kutta
				solution.push_back(runge_kutta->integrate(
					solution.back().tt.back(),
					stop_time_,
					solution.back().yy.tail_rows(1),
					solution.back().yp.tail_rows(1)));

				// break for cancellation
				if(lg->is_cancelled())return{};

				// check if event detected
				if(solution.back().event_detected==0){
					lg->msg("%s<<< Current Below Treshold >>>%s\n",KYEL,KNRM); 
					terminate_integration = true;
				}else if(solution.back().event_detected==1){
					lg->msg("%s<<< Reached Stop Temperature >>>%s\n",KYEL,KNRM); 
					terminate_integration = true;
				}else if(solution.back().tt.n_elem==runge_kutta->get_max_iter()+1){
					lg->msg("%s<<< Maximum number of Iterations reached >>>%s\n",KYEL,KNRM); 
					terminate_integration = true;
				}else{
					lg->msg("%s<<< End Time Reached >>>%s\n",KYEL,KNRM); 
					terminate_integration = true;
				}

				// done
				lg->msg(-2,"\n");
			}

			// break for cancellation
			if(lg->is_cancelled())return{};

			// joint solution
			arma::uword num_times=1;
			for(auto it=solution.begin();it!=solution.end();it++)
				num_times+=(*it).tt.n_elem-1;

			// allocate matrices
			arma::Col<fltp> tt(num_times);
			arma::Mat<fltp> peak_temperature(num_times,num_nzp);
			arma::Mat<fltp> coil_temperatures(num_times,num_coils);
			arma::Mat<fltp> circuit_currents(num_times,num_circuits);
			arma::Col<fltp> external_energy(num_times);
			arma::Mat<fltp> circuit_voltage(num_times,num_circuits);

			// concatenate solution
			arma::uword cnt = 0;
			for(auto it=solution.begin();it!=solution.end();it++){
				const cmn::RKData& data = (*it);
				const arma::uword num_times_sub = it==std::prev(solution.end()) ? data.tt.n_elem : data.tt.n_elem-1;
				tt.rows(cnt,cnt+num_times_sub-1) = data.tt.rows(0,num_times_sub-1);
				peak_temperature.rows(cnt,cnt+num_times_sub-1) = data.yy.submat(0,0,num_times_sub-1,num_nzp-1); // we're extracting the max value here
				coil_temperatures.rows(cnt,cnt+num_times_sub-1) = data.yy.submat(0,num_nzp,num_times_sub-1,num_nzp+num_coils-1);
				circuit_currents.rows(cnt,cnt+num_times_sub-1) = data.yy.submat(0,num_nzp+num_coils,num_times_sub-1,num_nzp+num_coils+num_circuits-1);
				external_energy.rows(cnt,cnt+num_times_sub-1) = data.yy.submat(0,num_nzp+num_coils+2*num_circuits,num_times_sub-1,num_nzp+num_coils+2*num_circuits);
				
				// voltage is calculated from external power
				{
					const arma::Mat<fltp> power = data.yp.submat(0,num_nzp+num_coils+num_circuits,num_times_sub-1,num_nzp+num_coils+2*num_circuits-1);
					const arma::Mat<fltp> current = circuit_currents.rows(cnt,cnt+num_times_sub-1);
					arma::Mat<fltp> voltage = -power/current;
					voltage(arma::find(arma::vectorise(current==0.0))).fill(0.0);
					circuit_voltage.rows(cnt,cnt+num_times_sub-1) = voltage;
				}
				cnt+=num_times_sub;
			}

			// sanity check
			assert(cnt==num_times);

			// energy check
			{
				// get final currents
				const arma::Row<fltp> circuit_end_current = circuit_currents.tail_rows(1);

				// calculate initial magnetic energy
				const fltp initial_magnetic_energy = arma::accu(M%arma::repmat(circuit_start_current,num_circuits,1)%arma::repmat(circuit_start_current.t(),1,num_circuits)/2);
				const fltp final_magnetic_energy = arma::accu(M%arma::repmat(circuit_end_current,num_circuits,1)%arma::repmat(circuit_end_current.t(),1,num_circuits)/2);
				
				// calculate final thermal energy
				fltp final_thermal_energy = RAT_CONST(0.0);
				fltp initial_thermal_energy = RAT_CONST(0.0);
				for(arma::uword j=0;j<num_coils;j++){
					initial_thermal_energy += fraction_hit(circuit_index(j))*coil_volume(j)*coil_data_fld(j)->get_material()->calc_thermal_energy_density(coil_temperatures.col(j).front()); 
					final_thermal_energy += fraction_hit(circuit_index(j))*coil_volume(j)*coil_data_fld(j)->get_material()->calc_thermal_energy_density(coil_temperatures.col(j).back()); 
				}

				// energy supplied by power supply minus the energy subtracted by external dumps
				const fltp final_external_energy = external_energy.back();

				// calculate
				const fltp magnetic_energy = initial_magnetic_energy - final_magnetic_energy;
				const fltp thermal_energy = final_thermal_energy - initial_thermal_energy;
				const fltp energy_leaked = magnetic_energy - thermal_energy - final_external_energy;
				const fltp fraction_leaked = std::abs(energy_leaked)/std::abs(initial_magnetic_energy);
				const bool energy_check_ok = fraction_leaked<energy_tolerance_;

				// report
				lg->msg(2,"%sChecking Energy%s\n",KBLU,KNRM);
				lg->msg("delta magnetic energy : %s%2.2e%s [kJ]\n",KYEL,magnetic_energy/1000,KNRM);
				lg->msg("delta thermal energy : %s%2.2e%s [kJ]\n",KYEL,thermal_energy/1000,KNRM);
				lg->msg("external energy : %s%2.2e%s [kJ]\n",KYEL,final_external_energy/1000,KNRM);
				lg->msg("leaked energy : %s%2.2e%s [kJ]\n",KYEL,energy_leaked/1000,KNRM);
				lg->msg("leaked fraction : %s%2.2f%s [%]\n",KYEL,100*fraction_leaked,KNRM);
				lg->msg("energy check %s%s%s\n",energy_check_ok ? KGRN : KRED,energy_check_ok ? "passed" : "failed",KNRM);
				lg->msg(-2,"\n");

				// error
				if(energy_check_hard_ && energy_check_ok)rat_throw_line("energy check failed");
			}

			// add to output data
			quench_data.push_back(Quench0DData::create(
				i,circuit_index,coil_names,circuit_names,
				tt,xnz,peak_temperature,coil_temperatures,circuit_currents,circuit_voltage,
				treshold_time,detection_time,switching_times,heater_times,
				hotspot_flux_density,hotspot_alpha,
				hotspot_start_temperature,
				hotspot_start_current_density,
				normal_zone_propagation_velocity,
				minimal_propagation_zone_length,
				minimal_quench_energy,
				heatered || is_rapid,is_sc));

			// done
			lg->msg(-2,"\n");

			// break for cancellation
			if(lg->is_cancelled())return{};
		}

		// check if any scenarios run
		if(quench_data.empty())rat_throw_line("no (valid) quench scenarios found");

		// create data object and return it
		return quench_data;
	}

	// generalized calculation
	std::list<ShDataPr> CalcQuench0D::calculate(
		const fltp time, 
		const cmn::ShLogPr &lg, 
		const ShSolverCachePr& cache){

		// run quench model
		const std::list<ShQuench0DDataPr> quench_data = calculate_quench(time,lg,cache);
		
		// convert to data type
		std::list<ShDataPr> data_list;
		for(auto it=quench_data.begin();it!=quench_data.end();it++)data_list.push_back(*it);

		// return data
		return data_list;
	}

	// is valid
	bool CalcQuench0D::is_valid(const bool enable_throws) const{
		if(max_iter_<=0){if(enable_throws){rat_throw_line("max iter must be positive");} return false;};
		return true;
	}


	// serialization type
	std::string CalcQuench0D::get_type(){
		return "rat::mdl::calcquench0";
	}

	// serialization
	void CalcQuench0D::serialize(Json::Value &js, cmn::SList &list) const{
		CalcLeaf::serialize(js,list);
		js["type"] = get_type();
		js["stop_temperature"] = stop_temperature_;
		js["stop_time"] = stop_time_;
		js["minimum_current_fraction"] = minimum_current_fraction_;
		js["reltol"] = reltol_;
		js["abstol"] = abstol_;
		js["energy_tolerance"] = energy_tolerance_;
		js["energy_check_hard"] = energy_check_hard_;
		js["integration_method"] = static_cast<int>(integration_method_);
		js["use_nzp1d"] = use_nzp1d_;
		js["nzp_element_size"] = nzp_element_size_;
		js["nzp_length"] = nzp_length_;
		js["scenario"] = static_cast<int>(scenario_);
		js["scenario_circuit"] = static_cast<int>(scenario_circuit_);
		js["scenario_coil"] = static_cast<int>(scenario_coil_);
		js["num_nz_parallel"] = num_nz_parallel_;
		js["temperature_bias"] = temperature_bias_;
		js["max_iter"] = static_cast<int>(max_iter_);
	}

	// deserialization
	void CalcQuench0D::deserialize(const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		CalcLeaf::deserialize(js,list,factory_list,pth);
		set_stop_temperature(js["stop_temperature"].ASFLTP());
		set_stop_time(js["stop_time"].ASFLTP());
		set_minimum_current_fraction(js["minimum_current_fraction"].ASFLTP());
		set_reltol(js["reltol"].ASFLTP());
		set_abstol(js["abstol"].ASFLTP());
		if(js.isMember("energy_tolerance"))energy_tolerance_ = js["energy_tolerance"].ASFLTP();
		if(js.isMember("energy_chack_hard"))energy_check_hard_ = js["energy_chack_hard"].asBool();
		if(js.isMember("integration_method"))integration_method_ = static_cast<cmn::RungeKutta::IntegrationMethod>(js["integration_method"].asInt());
		if(js.isMember("use_nzp1d"))use_nzp1d_ = js["use_nzp1d"].asBool();
		if(js.isMember("nzp_element_size"))nzp_element_size_ = js["nzp_element_size"].ASFLTP();
		if(js.isMember("nzp_length"))nzp_length_ = js["nzp_length"].ASFLTP();
		if(js.isMember("scenario"))scenario_ = static_cast<Scenario>(js["scenario"].asInt());
		if(js.isMember("scenario_circuit"))scenario_circuit_ = js["scenario_circuit"].asUInt64();
		if(js.isMember("scenario_coil"))scenario_coil_ = js["scenario_coil"].asUInt64();
		if(js.isMember("num_nz_parallel"))num_nz_parallel_ = js["num_nz_parallel"].ASFLTP();
		if(js.isMember("temperature_bias"))temperature_bias_ = js["temperature_bias"].ASFLTP(); else temperature_bias_ = RAT_CONST(0.5);
		if(js.isMember("max_iter"))max_iter_ = js["max_iter"].asUInt64();
	}

}}