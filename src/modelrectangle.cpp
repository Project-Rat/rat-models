// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelrectangle.hh"

#include "rat/common/error.hh"

#include "crossrectangle.hh"
#include "pathgroup.hh"
#include "pathrectangle.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	ModelRectangle::ModelRectangle(){
		set_name("Rectangle");
	}

	// factory
	ShModelRectanglePr ModelRectangle::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<ModelRectangle>();
	}

	// set arc radius
	void ModelRectangle::set_arc_radius(const fltp arc_radius){
		arc_radius_ = arc_radius;
	}

	// set length
	void ModelRectangle::set_length(const fltp length){
		length_ = length;
	}

		// set width
	void ModelRectangle::set_width(const fltp width){
		width_ = width;
	}

	// set coil thickness
	void ModelRectangle::set_coil_thickness(const fltp coil_thickness){
		coil_thickness_ = coil_thickness;
	}

	// set coil width
	void ModelRectangle::set_coil_width(const fltp coil_width){
		coil_width_ = coil_width;
	}

	// set axial element size
	void ModelRectangle::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	// get arc radius
	fltp ModelRectangle::get_arc_radius()const{
		return arc_radius_;
	}

	// get length
	fltp ModelRectangle::get_length()const{
		return length_;
	}

	// get length
	fltp ModelRectangle::get_width()const{
		return width_;
	}

	// get coil thickness
	fltp ModelRectangle::get_coil_thickness()const{
		return coil_thickness_;
	}

	// get coil width
	fltp ModelRectangle::get_coil_width()const{
		return coil_width_;
	}

	// get axial element size
	fltp ModelRectangle::get_element_size()const {
		return element_size_;
	}

	// get base
	ShPathPr ModelRectangle::get_input_path() const{
		// check input
		is_valid(true);

		// create circular path
		ShPathRectanglePr rectangle = PathRectangle::create(
			width_, length_, arc_radius_, element_size_);
		rectangle->set_offset(coil_thickness_);

		// return the rectangle
		return rectangle;
	}

	// get cross
	ShCrossPr ModelRectangle::get_input_cross() const{
		// check input
		is_valid(true);

		// create circular path
		ShCrossRectanglePr rectangle = CrossRectangle::create(
			0, coil_thickness_, -coil_width_/2, coil_width_/2, element_size_, element_size_);

		// return the rectangle
		return rectangle;
	}

	// check input
	bool ModelRectangle::is_valid(const bool enable_throws) const{
		if(!ModelCoilWrapper::is_valid(enable_throws))return false;
		if(arc_radius_<=0){if(enable_throws){rat_throw_line("arc radius must be larger than zero");} return false;};
		if(length_<=2*arc_radius_){if(enable_throws){rat_throw_line("length can not exceed twice the arc radius");} return false;};
		if(width_<=2*arc_radius_){if(enable_throws){rat_throw_line("width can not exceed twice the arc radius");} return false;};
		if(coil_thickness_<=0){if(enable_throws){rat_throw_line("coil thickness must be larger than zero");} return false;};
		if(coil_width_<=0){if(enable_throws){rat_throw_line("coil width must be larger than zero");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string ModelRectangle::get_type(){
		return "rat::mdl::modelrectangle";
	}

	// method for serialization into json
	void ModelRectangle::serialize(
		Json::Value &js, cmn::SList &list) const{

		// parent
		ModelCoilWrapper::serialize(js,list);

		// set type
		js["type"] = get_type();

		// set properties
		js["arc_radius"] = arc_radius_;
		js["length"] = length_;
		js["width"] = width_;
		js["coil_thickness"] = coil_thickness_;
		js["coil_width"] = coil_width_;
		js["element_size"] = element_size_;
	}

	// method for deserialisation from json
	void ModelRectangle::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		ModelCoilWrapper::deserialize(js,list,factory_list,pth);

		// set properties
		arc_radius_ = js["arc_radius"].ASFLTP();
		length_ = js["length"].ASFLTP();
		width_ = js["width"].ASFLTP();		
		coil_thickness_ = js["coil_thickness"].ASFLTP();
		coil_width_ = js["coil_width"].ASFLTP();
		element_size_ = js["element_size"].ASFLTP();
	}

}}