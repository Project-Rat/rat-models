// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathrail.hh"

// rat-common headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathRail::PathRail(){
		set_name("Rail");
	}

	// constructor with specification
	PathRail::PathRail(const ShPathPr &base, const fltp spacing, const fltp distance) : PathRail(){
		set_input_path(base);
		set_spacing(spacing);
		set_distance(distance);
	}

	// factory
	ShPathRailPr PathRail::create(){
		return std::make_shared<PathRail>();
	}

	// constructor with dimensions and element size
	ShPathRailPr PathRail::create(const ShPathPr &base, const fltp spacing, const fltp distance){
		return std::make_shared<PathRail>(base,spacing,distance);
	}


	// setters
	void PathRail::set_spacing(const fltp spacing){
		spacing_ = spacing;
	}

	void PathRail::set_distance(const fltp distance){
		distance_ = distance;
	}

	void PathRail::set_long_offset(const fltp long_offset){
		long_offset_ = long_offset;
	}


	// getters
	fltp PathRail::get_spacing()const{
		return spacing_;
	}

	fltp PathRail::get_distance()const{
		return distance_;
	}

	fltp PathRail::get_long_offset()const{
		return long_offset_;
	}


	// get frame
	ShFramePr PathRail::create_frame(const MeshSettings &stngs) const{
		// check if base path was set
		is_valid(true);

		// create frame
		const ShFramePr frame = input_path_->create_frame(stngs);

		// check if it is a loop
		if(!frame->is_loop())rat_throw_line("frame must be loop");

		// copy coordinates and orientation
		arma::field<arma::Mat<fltp> > Rf = frame->get_coords();
		arma::field<arma::Mat<fltp> > Df = frame->get_transverse();
		const arma::Row<arma::uword> num_nodes = frame->get_num_nodes();

		// remove last node for all sections but last
		for(arma::uword i=0;i<Rf.n_elem-1;i++){
			Rf(i) = Rf(i).head_cols(Rf(i).n_cols-1);
			Df(i) = Df(i).head_cols(Df(i).n_cols-1);
		}

		// combine arrays
		const arma::Mat<fltp> R = cmn::Extra::field2mat(Rf);
		const arma::Mat<fltp> D = cmn::Extra::field2mat(Df);

		// output 
		arma::Mat<fltp> Rrail(3,R.n_cols);
		arma::Mat<fltp> Lrail(3,R.n_cols);
		arma::Mat<fltp> Nrail(3,R.n_cols);

		// walk over positions
		for(arma::uword i=0;i<R.n_cols;i++){
			// take R1 from list
			arma::Col<fltp>::fixed<3> R1 = R.col(i); 

			// allocate guide points
			arma::Col<fltp>::fixed<3> R2 = cmn::Extra::null_vec();

			// find second coordinate by distance
			for(arma::uword j=0;j<R.n_cols;j++){
				// get points
				const arma::Col<fltp>::fixed<3>& R2_1 = R.col((i+j)%R.n_cols);
				const arma::Col<fltp>::fixed<3>& R2_2 = R.col((i+j+1)%R.n_cols);

				// distance to points
				const fltp d1 = arma::as_scalar(cmn::Extra::vec_norm(R2_1 - R1));
				const fltp d2 = arma::as_scalar(cmn::Extra::vec_norm(R2_2 - R1));

				// check if within spacing
				if(d1<distance_ && d2>=distance_){
					// interpolate the R2 point
					R2 = R2_1 + ((distance_ - d1)/(d2 - d1))*(R2_2 - R2_1);
					break;
				}

				// check
				else if(j==R.n_cols-1){
					rat_throw_line("could not find second point");
				}
			}

			// calculate position of R3
			Nrail.col(i) = cmn::Extra::cross((R2 - R1).eval().each_row()/cmn::Extra::vec_norm(R2 - R1), D.col(i));
			Rrail.col(i) = (R2 + R1)/2 + spacing_*Nrail.col(i) + long_offset_*Lrail.col(i);
			Lrail.col(i) = (R2 - R1)/arma::as_scalar(cmn::Extra::vec_norm(R2-R1));
		}

		// std::cout<<arma::join_horiz(R.t(), Rrail.t())<<std::endl;

		// recalculate the frame using finite difference
		// arma::Mat<fltp> Lrail = arma::diff(Rrail,1,1); Lrail = (arma::join_horiz(Lrail.head_cols(1), Lrail) + arma::join_horiz(Lrail, Lrail.tail_cols(1)))/2;
		// arma::Mat<fltp> Nrail = cmn::Extra::cross(Lrail,D); 

		// normalize
		Lrail.each_row()/=cmn::Extra::vec_norm(Lrail);
		Nrail.each_row()/=cmn::Extra::vec_norm(Nrail);

		// allocate
		arma::field<arma::Mat<fltp> > Lf(1,Rf.n_elem);
		arma::field<arma::Mat<fltp> > Nf(1,Rf.n_elem);

		// split sections
		arma::uword idx1 = 0, idx2 = 0;
		for(arma::uword i=0;i<Rf.n_elem;i++){
			// get sections
			idx2 = idx1 + num_nodes(i) - 1;
			
			// copy section
			Rf(i) = Rrail.cols(idx1,idx2%Nrail.n_cols); 
			Lf(i) = Lrail.cols(idx1,idx2%Nrail.n_cols);
			Nf(i) = Nrail.cols(idx1,idx2%Nrail.n_cols); 
			Df(i) = D.cols(idx1,idx2%Nrail.n_cols); 

			// check handedness
			assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(Nf(i),Lf(i)),Df(i))>RAT_CONST(0.0)));
		
			// shift
			idx1 = idx2;
		}

		// create offset frame
		ShFramePr offset_frame = Frame::create(Rf,Lf,Nf,Df);

		// conserve location
		offset_frame->set_location(
			frame->get_section(), 
			frame->get_turn(), 
			frame->get_num_section_base());

		// apply_transformations
		offset_frame->apply_transformations(get_transformations(), stngs.time);

		// create new frame
		return offset_frame;
	}

	// re-index nodes after deleting
	void PathRail::reindex(){
		InputPath::reindex(); Transformations::reindex();
	}

	// vallidity check
	bool PathRail::is_valid(const bool enable_throws) const{
		if(!InputPath::is_valid(enable_throws))return false;
		if(!Transformations::is_valid(enable_throws))return false;
		return true;
	}

	// get type
	std::string PathRail::get_type(){
		return "rat::mdl::pathrail";
	}

	// method for serialization into json
	void PathRail::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents
		Path::serialize(js,list);
		InputPath::serialize(js,list);
		Transformations::serialize(js,list);
		
		// type
		js["type"] = get_type();

		// properties
		js["spacing"] = spacing_;
		js["distance"] = distance_;
		js["long_offset"] = long_offset_;
	}

	// method for deserialisation from json
	void PathRail::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		Path::deserialize(js,list,factory_list,pth);
		InputPath::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);
		set_spacing(js["spacing"].ASFLTP()); 
		set_distance(js["distance"].ASFLTP()); 
		set_long_offset(js["long_offset"].ASFLTP());
	}


}}