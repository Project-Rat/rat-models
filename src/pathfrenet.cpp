// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathfrenet.hh"

// rat-common headers
#include "rat/common/extra.hh"

// rat-models headers
#include "darboux.hh"
#include "pathbezier.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathFrenet::PathFrenet(){
		set_name("Frenet-Serret");
	}

	// default constructor
	PathFrenet::PathFrenet(const ShPathPr &input_path) : PathFrenet(){
		set_input_path(input_path);
	}

	// factory
	ShPathFrenetPr PathFrenet::create(){
		//return ShPathFrenetPr(new PathFrenet);
		return std::make_shared<PathFrenet>();
	}

	// factory
	ShPathFrenetPr PathFrenet::create(const ShPathPr &input_path){
		//return ShPathFrenetPr(new PathFrenet);
		return std::make_shared<PathFrenet>(input_path);
	}


	// keep winding direction
	void PathFrenet::set_keep_block_vector(const bool keep_block_vector){
		keep_block_vector_ = keep_block_vector;
	}

	// correct for sign flips
	void PathFrenet::set_correct_sign_flips(const bool correct_sign_flips){
		correct_sign_flips_ = correct_sign_flips;
	}

	// get winding direction setting
	bool PathFrenet::get_keep_block_vector() const{
		return keep_block_vector_;
	}

	void PathFrenet::set_use_binormal(const bool use_binormal){
		use_binormal_ = use_binormal;
	}
	
	bool PathFrenet::get_use_binormal()const{
		return use_binormal_;
	}

	bool PathFrenet::get_correct_sign_flips()const{
		return correct_sign_flips_;
	}

	// get frame
	ShFramePr PathFrenet::create_frame(const MeshSettings &stngs) const{
		// check validity
		is_valid(true);

		// get frame from input path
		const ShFramePr input_frame = input_path_->create_frame(stngs);

		// separate the frames that are not connected
		const std::list<ShFramePr> input_frames = input_frame->separate();

		// list of frenet serret frames
		std::list<ShFramePr> output_frames;

		// walk over framelist
		for(auto it = input_frames.begin();it!=input_frames.end();it++){
			// get frame
			const ShFramePr& frame = (*it);

			// get coordinates
			const arma::field<arma::Mat<fltp> >& R = frame->get_coords();
			arma::field<arma::Mat<fltp> > L = frame->get_direction();
			const arma::uword num_sections = L.n_elem;

			// check if the path is closed
			const bool is_loop = frame->is_loop();

			// allocate darboux vectors
			arma::field<arma::Mat<fltp> > D(1,num_sections);
			arma::field<arma::Mat<fltp> > T(1,num_sections);
			arma::field<arma::Mat<fltp> > N(1,num_sections);

			// allocate alignment vector
			arma::Col<fltp>::fixed<3> Dalign;

			// average last to first
			for(arma::uword i=0;i<num_sections-1;i++){
				L(i).col(L(i).n_cols-1) += L(i+1).col(0);
				L(i).col(L(i).n_cols-1)/=2;
				L(i+1).col(0) = L(i).col(L(i).n_cols-1);
			}

			// average last to first for entire loop
			if(is_loop){
				L(num_sections-1).tail_cols(1) += L(0).head_cols(1);
				L(num_sections-1).tail_cols(1)/=2;
				L(0).head_cols(1) = L(num_sections-1).tail_cols(1);
			}


			// calculate velocity 
			arma::field<arma::Mat<fltp> > Rr;
			if(!is_loop){
				Rr = R;
			}else{
				Rr.set_size(1,num_sections+2);
				Rr(0) = R(num_sections-1);
				for(arma::uword i=0;i<num_sections;i++)
					Rr(i+1) = R(i);
				Rr(num_sections+1) = R(0);
			}
			for(arma::uword i=0;i<Rr.n_elem-1;i++)
				Rr(i) = Rr(i).head_cols(Rr(i).n_cols-1);

			// calculate derivative
			const arma::Mat<fltp> dR = PathBezier::diff(cmn::Extra::field2mat(Rr),2,2)(1); // check if this works with first and last points being duplicates?

			// go back to Rr
			arma::field<arma::Mat<fltp> > dRf(1,Rr.n_elem); arma::uword cnt = 0; 
			for(arma::uword i=0;i<Rr.n_elem;i++){
				const arma::uword idx1 = cnt;
				const arma::uword idx2 = i!=Rr.n_elem-1 ? cnt+Rr(i).n_cols : cnt+Rr(i).n_cols-1;
				dRf(i) = dR.cols(idx1,idx2);
				cnt+=Rr(i).n_cols;
			}

			// remove first and last for loop
			if(is_loop)dRf = dRf.cols(1,num_sections);


			// calculate darboux vectors
			for(arma::uword i=0;i<num_sections;i++){
				// calculate
				const bool use_analytic = false;
				// const arma::field<arma::Mat<fltp> > C = PathBezier::diff(R(i),2,2);
				Darboux db(dRf(i));
				db.setup(use_analytic);

				// get alignment vector for first section
				if(i==0)Dalign = db.get_transverse().col(0);

				// make sure that the radial vector is the transverse vector
				// db.correct_sign(Dalign);

				// get vectors
				D(i) = db.get_transverse(use_binormal_);
				N(i) = db.get_normal();
				T(i) = db.get_longitudinal();

				// store alignment of the last transverse vector
				// Dalign = D(i).tail_cols(1);
			}

			// average last to first
			for(arma::uword i=0;i<num_sections-1;i++){
				N(i).col(N(i).n_cols-1) += N(i+1).col(0);
				N(i).col(N(i).n_cols-1)/=2;
				N(i+1).col(0) = N(i).col(N(i).n_cols-1);

				D(i).col(D(i).n_cols-1) += D(i+1).col(0);
				D(i).col(D(i).n_cols-1)/=2;
				D(i+1).col(0) = D(i).col(D(i).n_cols-1);
			}

			// average last to first for entire loop
			if(is_loop){
				N(num_sections-1).tail_cols(1) += N(0).head_cols(1);
				N(num_sections-1).tail_cols(1)/=2;
				N(0).head_cols(1) = N(num_sections-1).tail_cols(1);

				D(num_sections-1).tail_cols(1) += D(0).head_cols(1);
				D(num_sections-1).tail_cols(1)/=2;
				D(0).head_cols(1) = D(num_sections-1).tail_cols(1);
			}

			// re-normalize N
			for(arma::uword i=0;i<num_sections;i++){
				// get vector norm of N
				const arma::Row<fltp> norm = cmn::Extra::vec_norm(N(i));
				
				// deal with infinites
				N(i).each_row()/=norm;

				// find nan values
				const arma::Col<arma::uword> idx = 
					arma::find_nonfinite(arma::sum(
					arma::join_vert(T(i),N(i),D(i))));

				// replace nans with pre-fs values
				if(!idx.empty()){
					// override non-finite values with original orientation matrix
					N(i).cols(idx) = frame->get_normal(i).cols(idx);
					T(i).cols(idx) = frame->get_direction(i).cols(idx);
					D(i).cols(idx) = frame->get_transverse(i).cols(idx);
				}
			}

			// fix sign flips
			if(correct_sign_flips_){
				arma::Col<fltp>::fixed<3> dstart = D(0).col(0);
				for(arma::uword i=0;i<num_sections;i++){
					// check sign flips
					const arma::Row<fltp> sf = 
						cmn::Extra::dot(arma::join_horiz(
						dstart,D(i).head_cols(D(i).n_cols-1)),D(i));

					// flip D when curvature changes from positive to negative
					int flipstate = 1;
					for(arma::uword j=0;j<D(i).n_cols;j++){
						// check whether flip is needed
						if(sf(j)<0)flipstate*=-1;

						// flip vectors (if needed)
						D(i).col(j)*=flipstate;
						N(i).col(j)*=flipstate;
					}

					// update starting vector
					dstart = D(i).tail_cols(1);
				}
			}

			// block vector
			arma::field<arma::Mat<fltp> > B;
			if(keep_block_vector_)B = frame->get_block(); else B = N;

			// check
			#ifndef NDEBUG
			for(arma::uword i=0;i<num_sections;i++){
				// check finite
				assert(T(i).is_finite()); assert(N(i).is_finite());

				// handedness
				// assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N(i),T(i)),D(i))>RAT_CONST(0.0)));
				
				// lontitudinal and normal vectors must be unit vectors
				assert(arma::all(arma::abs(cmn::Extra::vec_norm(T(i))-RAT_CONST(1.0))<1e-9));
				assert(arma::all(arma::abs(cmn::Extra::vec_norm(N(i))-RAT_CONST(1.0))<1e-9));
			}
			#endif

			// create new frame
			const ShFramePr output_frame = Frame::create(R, T, N, D, B);

			// transfer sections
			output_frame->set_location(frame->get_section(), frame->get_turn(), frame->get_num_section_base());

			// add to list
			output_frames.push_back(output_frame);
		}

		// combine frames again
		const ShFramePr output_frame = Frame::create(output_frames);

		// apply transformations
		output_frame->apply_transformations(get_transformations(), stngs.time);

		// return the new frame
		return output_frame;
	}


	// vallidity check
	bool PathFrenet::is_valid(const bool enable_throws) const{
		if(!InputPath::is_valid(enable_throws))return false;
		return true;
	}

	// get type
	std::string PathFrenet::get_type(){
		return "rat::mdl::pathfrenet";
	}

	// method for serialization into json
	void PathFrenet::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Path::serialize(js,list);
		InputPath::serialize(js,list);
		Transformations::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["keep_block_vector"] = keep_block_vector_;
		js["use_binormal"] = use_binormal_;
		js["correct_sign_flips"] = correct_sign_flips_;
	}

	// method for deserialisation from json
	void PathFrenet::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent
		Path::deserialize(js,list,factory_list,pth);
		InputPath::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);
		
		set_keep_block_vector(js["keep_block_vector"].asBool());
		set_use_binormal(js["use_binormal"].asBool());
		if(js.isMember("correct_sign_flips"))correct_sign_flips_ = js["correct_sign_flips"].asBool();
	}

}}
