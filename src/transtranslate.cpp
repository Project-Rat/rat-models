// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "transtranslate.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	TransTranslate::TransTranslate(){
		set_vector(RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0)); set_name("Translate");
	}

	TransTranslate::TransTranslate(const arma::Col<fltp>::fixed<3> &dR) : TransTranslate(){
		set_vector(dR);
	}

	TransTranslate::TransTranslate(const fltp dx, const fltp dy, const fltp dz) : TransTranslate(){
		set_vector(dx,dy,dz);
	}

	// factory methods
	ShTransTranslatePr TransTranslate::create(){
		//return ShTransTranslatePr(new TransTranslate);
		return std::make_shared<TransTranslate>();
	}

	ShTransTranslatePr TransTranslate::create(const arma::Col<fltp>::fixed<3> &dR){
		//return ShTransTranslatePr(new TransTranslate(dR));
		return std::make_shared<TransTranslate>(dR);
	}

	ShTransTranslatePr TransTranslate::create(const fltp dx, const fltp dy, const fltp dz){
		//return ShTransTranslatePr(new TransTranslate(dx,dy,dz));
		return std::make_shared<TransTranslate>(dx,dy,dz);
	}

	// set translation vector
	void TransTranslate::set_vector(const arma::Col<fltp>::fixed<3> &dR){
		dR_ = dR;
	}

	// set velocity
	void TransTranslate::set_velocity(const arma::Col<fltp>::fixed<3> &V){
		V_ = V;
	}

	// get the translation vector
	arma::Col<fltp>::fixed<3> TransTranslate::get_vector() const{
		return dR_;
	}

	// get the translation vector
	arma::Col<fltp>::fixed<3> TransTranslate::get_velocity() const{
		return V_;
	}

	// set translation vector from components
	void TransTranslate::set_vector(const fltp dx, const fltp dy, const fltp dz){
		// create vector
		const arma::Col<fltp>::fixed<3> dR = {dx,dy,dz};

		// call overloaded function
		set_vector(dR);
	}

	// apply to coordinates only
	void TransTranslate::apply_coords(arma::Mat<fltp> &R, const fltp time) const{
		// apply to coordinate
		R.each_col() += dR_ + time*V_;
	}

	// get type
	std::string TransTranslate::get_type(){
		return "rat::mdl::transtranslate";
	}

	// method for serialization into json
	void TransTranslate::serialize(Json::Value &js, cmn::SList &list) const{
		Trans::serialize(js,list);
		js["type"] = get_type();
		for(arma::uword i=0;i<3;i++)js["vector"].append(dR_(i));
		for(arma::uword i=0;i<3;i++)js["velocity"].append(V_(i));
	}

	// method for deserialisation from json
	void TransTranslate::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		Trans::deserialize(js,list,factory_list,pth);
		
		arma::uword idx1 = 0; for(auto it = js["vector"].begin();it!=js["vector"].end();it++)dR_(idx1++) = (*it).ASFLTP();
		arma::uword idx2 = 0; for(auto it = js["velocity"].begin();it!=js["velocity"].end();it++)V_(idx2++) = (*it).ASFLTP();
	}

}}