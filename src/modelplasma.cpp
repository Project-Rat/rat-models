// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelplasma.hh"

#include "rat/dmsh/dfones.hh"
#include "rat/dmsh/dfplasma.hh"

#include "pathcircle.hh"
#include "crossdmsh.hh"


// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	ModelPlasma::ModelPlasma(){
		set_name("Plasma");
	}

	// factory
	ShModelPlasmaPr ModelPlasma::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<ModelPlasma>();
	}

	// setters
	void ModelPlasma::set_r0(const fltp r0){
		r0_ = r0;
	}

	void ModelPlasma::set_a(const fltp a){
		a_ = a;
	}

	void ModelPlasma::set_delta(const fltp delta){
		delta_ = delta;
	}

	void ModelPlasma::set_kappa(const fltp kappa){
		kappa_ = kappa;
	}

	void ModelPlasma::set_num_sections(const arma::uword num_sections){
		num_sections_ = num_sections;
	}

	void ModelPlasma::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	// getters
	fltp ModelPlasma::get_r0() const{
		return r0_;
	}

	fltp ModelPlasma::get_a() const{
		return a_;
	}

	fltp ModelPlasma::get_delta() const{
		return delta_;
	}

	fltp ModelPlasma::get_kappa() const{
		return kappa_;
	}

	arma::uword ModelPlasma::get_num_sections()const{
		return num_sections_;
	}

	fltp ModelPlasma::get_element_size()const {
		return element_size_;
	}


	// get base
	ShPathPr ModelPlasma::get_input_path() const{
		// check input
		is_valid(true);

		// create circular path
		const ShPathCirclePr circle = PathCircle::create(r0_, num_sections_, element_size_, r0_ + a_/2);

		// return the circle
		return circle;
	}

	// get cross
	ShCrossPr ModelPlasma::get_input_cross() const{
		// check input
		is_valid(true);

		// create distance and scaling functions
		const bool center_r0 = true;
		const dm::ShDistFunPr fd = dm::DFPlasma::create(a_,r0_,delta_,kappa_,center_r0);
		const dm::ShDistFunPr fh = dm::DFOnes::create();

		// create circular path
		const ShCrossDMshPr crss = CrossDMsh::create(element_size_, fd, fh);

		// return the rectangle
		return crss;
	}

	// check input
	bool ModelPlasma::is_valid(const bool enable_throws) const{
		if(!ModelCoilWrapper::is_valid(enable_throws))return false;
		if(r0_<=0){if(enable_throws){rat_throw_line("major radius r0 must be larger than zero");} return false;};
		if(a_<=0){if(enable_throws){rat_throw_line("minor radius a must be larger than zero");} return false;};
		// if(delta_<=0){if(enable_throws){rat_throw_line("triangularity delta must be larger than zero");} return false;};
		if(kappa_<=0){if(enable_throws){rat_throw_line("elongation kappa must be larger than zero");} return false;};
		if(num_sections_<=0){if(enable_throws){rat_throw_line("number of sections must be larger than zero");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string ModelPlasma::get_type(){
		return "rat::mdl::modelplasma";
	}

	// method for serialization into json
	void ModelPlasma::serialize(
		Json::Value &js, cmn::SList &list) const{

		// parent
		ModelCoilWrapper::serialize(js,list);

		// set type
		js["type"] = get_type();

		// set properties
		js["ro"] = r0_; 
		js["a"] = a_; 
		js["delta"] = delta_; 
		js["kappa"] = kappa_;
		js["num_sections"] = static_cast<int>(num_sections_);
		js["element_size"] = element_size_;
	}

	// method for deserialisation from json
	void ModelPlasma::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		ModelCoilWrapper::deserialize(js,list,factory_list,pth);

		// set properties
		r0_ = js["ro"].ASFLTP(); 
		a_ = js["a"].ASFLTP();
		delta_ = js["delta"].ASFLTP(); 
		kappa_ = js["kappa"].ASFLTP();
		num_sections_ = js["num_sections"].asUInt64();
		element_size_ = js["element_size"].ASFLTP();
	}

}}