// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "particle.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// create particle
	Particle::Particle(){

	}

	// set mass
	void Particle::set_rest_mass(const fltp rest_mass){
		rest_mass_ = rest_mass;
	}

	// set charge
	void Particle::set_charge(const fltp charge){
		charge_ = charge;
	}

	// get mass
	fltp Particle::get_rest_mass() const{
		return rest_mass_;
	}

	// get charge
	fltp Particle::get_charge() const{
		return charge_;
	}

	// set number of steps
	void Particle::set_num_steps(const arma::uword num_steps){
		if(num_steps<=0)rat_throw_line("number of steps must be larger than zero");
		num_steps_ = num_steps;
	}

	// set start index
	void Particle::set_start_index(const arma::uword start_idx){
		// start index
		idx_max_ = arma::sword(start_idx)-1; // loop around at start_idx equals zero!
		idx_min_ = start_idx;
	}

	// set start coordinate
	void Particle::set_startcoord(
		const arma::Col<fltp>::fixed<3> &R0, 
		const arma::Col<fltp>::fixed<3> &V0){
		R0_ = R0; V0_ = V0;
	}

	// mark relativistic
	void Particle::set_is_relativistic(const bool is_relativistic){
		is_relativistic_ = is_relativistic;
	}

	// get start coordinate
	const arma::Col<fltp>::fixed<3>& Particle::get_initial_coord(){
		return R0_;
	}
	
	// get start velocity
	const arma::Col<fltp>::fixed<3>& Particle::get_initial_velocity(){
		return V0_;
	}

	// get whether the track is relativistic
	bool Particle::get_is_relativistic()const{
		return is_relativistic_;
	}

	// allocate
	void Particle::setup(){
		// check input
		if(idx_min_!=idx_max_+1)rat_throw_line("track must start and end at same index");
		if(idx_max_>arma::sword(num_steps_)-1)rat_throw_line("start index exceeds number of steps");
		if(num_steps_==0)rat_throw_line("must have at least one step");

		// allocate storage
		Rt_.set_size(3,num_steps_); Vt_.set_size(3,num_steps_); 
		Bt_.set_size(3,num_steps_); Et_.set_size(3,num_steps_); 
		t_.set_size(num_steps_);
		
		// set allive
		alive_start_ = true; alive_end_ = true;
		if(idx_min_==0)alive_start_ = false;
		if(idx_max_==arma::sword(num_steps_)-1)alive_end_ = false;

		// done
		return;
	}

	// truncate
	void Particle::truncate(){
		// check array lengths
		assert(Rt_.n_cols==Vt_.n_cols);
		assert(Rt_.n_cols==Bt_.n_cols);
		assert(Rt_.n_cols==Et_.n_cols);
		assert(Rt_.n_cols==t_.n_cols);
	
		// check for empty track
		if(is_empty()){Rt_.clear(); Vt_.clear(); Bt_.clear(); Et_.clear(); t_.clear(); return;}

		// perform truncation
		Rt_ = Rt_.cols(idx_min_,idx_max_);
		Vt_ = Vt_.cols(idx_min_,idx_max_);
		Bt_ = Bt_.cols(idx_min_,idx_max_);
		Et_ = Et_.cols(idx_min_,idx_max_);
		t_ = t_.cols(idx_min_,idx_max_);

		// update start-end indices
		idx_min_ = 0; idx_max_ = Rt_.n_cols-1;

		// done
		return;
	}

	// check if particle is empty
	bool Particle::is_empty()const{
		return idx_max_<idx_min_;
	}

	// check if the track start is alive
	bool Particle::is_alive_start() const{
		return alive_start_;
	}

	// check if the track end is alive
	bool Particle::is_alive_end() const{
		return alive_end_;
	}

	// get coordinate of track start
	arma::Col<fltp>::fixed<3> Particle::get_first_coord() const{
		return Rt_.col(idx_min_);
	}

	// get velocity of track start
	arma::Col<fltp>::fixed<3> Particle::get_first_velocity() const{
		return Vt_.col(idx_min_);
	}

	// get time
	fltp Particle::get_first_time() const{
		return t_(idx_min_);
	}

	// get coordinate of track end
	arma::Col<fltp>::fixed<3> Particle::get_last_coord() const{
		return Rt_.col(idx_max_);
	}

	// get velocity of track end 
	arma::Col<fltp>::fixed<3> Particle::get_last_velocity() const{
		return Vt_.col(idx_max_);
	}

	// get time
	fltp Particle::get_last_time() const{
		return t_(idx_max_);
	}

	// set coordinates
	void Particle::set_prev_coord(
		const fltp tp, 
		const arma::Col<fltp>::fixed<3> &Rp, 
		const arma::Col<fltp>::fixed<3> &Vp,
		const arma::Col<fltp>::fixed<3> &Bp,
		const arma::Col<fltp>::fixed<3> &Ep){
		// check if there is still space
		if(idx_min_==0)rat_throw_line("zero index reached (dead)");
		
		// update index
		idx_min_--;

		// store data at new index
		t_(idx_min_) = tp; 
		Rt_.col(idx_min_) = Rp; 
		Vt_.col(idx_min_) = Vp; 
		Bt_.col(idx_min_) = Bp;
		Et_.col(idx_min_) = Ep;

		// check alive
		if(idx_min_==0)alive_start_ = false;
	}

	// set coordinates
	void Particle::set_next_coord(
		const fltp tp, 
		const arma::Col<fltp>::fixed<3> &Rp, 
		const arma::Col<fltp>::fixed<3> &Vp,
		const arma::Col<fltp>::fixed<3> &Bp,
		const arma::Col<fltp>::fixed<3> &Ep){
		// check if there is space left
		if(idx_max_==arma::sword(num_steps_)-1)rat_throw_line("max index reached (dead)");

		// update index
		idx_max_++;

		// store data at new index
		t_(idx_max_) = tp; 
		Rt_.col(idx_max_) = Rp; 
		Vt_.col(idx_max_) = Vp;
		Bt_.col(idx_max_) = Bp;
		Et_.col(idx_max_) = Ep;

		// check alive
		if(idx_max_==arma::sword(num_steps_)-1)alive_end_ = false;
	}

	// terminate end
	void Particle::terminate_end(){
		alive_end_ = false;
	}

	// terminate start
	void Particle::terminate_start(){
		alive_start_ = false;
	}

	// get time
	arma::Row<fltp> Particle::get_time()const{
		if(is_empty())return{};
		return t_.cols(idx_min_,idx_max_);
	}

	// get all track coordinates
	arma::Mat<fltp> Particle::get_track_coords() const{
		if(is_empty())return{};
		return Rt_.cols(idx_min_,idx_max_);
	}

	// get track coordinates by reference
	const arma::Mat<fltp>& Particle::get_raw_track_coords()const{
		return Rt_;
	}

	// get all track velocities
	arma::Mat<fltp> Particle::get_track_velocities() const{
		if(is_empty())return{};
		return Vt_.cols(idx_min_,idx_max_);
	}

	// get track momentum in kg-m/s
	arma::Mat<fltp> Particle::get_track_momentum() const{
		const arma::Mat<fltp> velocity = get_track_velocities();
		const fltp rest_mass_kg = get_rest_mass_kg();
		if(is_relativistic_){
			const arma::Row<fltp> gamma = get_track_gamma();
			const arma::Mat<fltp> momentum = (gamma*rest_mass_kg)%velocity.each_row();
			return momentum;
		}else{
			const arma::Mat<fltp> momentum = rest_mass_kg*velocity;
			return momentum;
		}
	}

	// get lightspeed fraction
	arma::Mat<fltp> Particle::get_track_velocities_c()const{
		return get_track_velocities()/arma::Datum<fltp>::c_0;
	}

	// get Lorentz factor
	arma::Row<fltp> Particle::get_track_gamma()const{
		if(is_relativistic_){
			const arma::Mat<fltp> velocity = get_track_velocities();
			const arma::Row<fltp> gamma = RAT_CONST(1.0)/arma::sqrt(RAT_CONST(1.0) - arma::square(cmn::Extra::vec_norm(velocity)/arma::Datum<fltp>::c_0));
			return gamma;
		}else{
			return arma::Row<fltp>(get_num_coords(), arma::fill::ones);
		}
	}

	// get rest mass in kg
	fltp Particle::get_rest_mass_kg()const{
		return rest_mass_*RAT_CONST(1e9)*arma::Datum<fltp>::eV/(arma::Datum<fltp>::c_0*arma::Datum<fltp>::c_0);
	}

	// get track momentum in eV/C
	arma::Mat<fltp> Particle::get_track_momentum_ev()const{
		const arma::Mat<fltp> momentum_si = get_track_momentum(); // kg-m/s
		const arma::Mat<fltp> momentum_ev = momentum_si*arma::Datum<fltp>::c_0/arma::Datum<fltp>::eV;
		return momentum_ev;
	}

	// get curvature of track
	arma::Mat<fltp> Particle::get_track_curvature() const{
		if(is_empty())return{};
		return cmn::Extra::calc_curvature(Rt_.cols(idx_min_,idx_max_));
	}

	// get radius of curvature
	arma::Row<fltp> Particle::get_track_radius() const{
		return RAT_CONST(1.0)/cmn::Extra::vec_norm(get_track_curvature());
	}

	// calculate temperature of track
	arma::Row<fltp> Particle::get_track_temperature()const{
		if(is_relativistic_)return (RAT_CONST(2.0)/(3*arma::Datum<fltp>::k))*(get_track_gamma() - 1.0)*get_rest_mass_kg()*arma::Datum<fltp>::c_0*arma::Datum<fltp>::c_0;
		else return 2*(get_rest_mass_kg()*arma::square(cmn::Extra::vec_norm(get_track_velocities()))/2)/(3*arma::Datum<fltp>::k);
	}

	// get track angle with respect to t0
	arma::Row<fltp> Particle::get_deviated_angle(const bool to_end) const{
		if(is_empty())return{};
		arma::Mat<fltp> M(3,idx_max_-idx_min_+1); M.each_col() = Vt_.col(to_end ? idx_max_ : idx_min_);
		return arma::acos(cmn::Extra::dot(M,Vt_.cols(idx_min_,idx_max_))/
			(rat::cmn::Extra::vec_norm(M)%cmn::Extra::vec_norm(Vt_.cols(idx_min_,idx_max_))));
	}

	// get number of coordinates in track
	arma::uword Particle::get_num_coords()const{
		return idx_max_ - idx_min_ + 1;
	}

	// get magnetic field
	arma::Mat<fltp> Particle::get_flux_density()const{
		if(is_empty())return{};
		return Bt_.cols(idx_min_,idx_max_);
	}

	// get magnetic field
	arma::Mat<fltp> Particle::get_electric_field()const{
		if(is_empty())return{};
		return Et_.cols(idx_min_,idx_max_);
	}


}}
