// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelsolenoid.hh"

#include "rat/common/error.hh"

#include "crossrectangle.hh"
#include "pathcircle.hh"


// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	ModelSolenoid::ModelSolenoid(){
		set_name("Solenoid");
	}

	// default constructor
	ModelSolenoid::ModelSolenoid(
		const fltp inner_radius,
		const fltp dcoil,
		const fltp height,
		const fltp element_size,
		const arma::uword num_sections){

		// set to self
		set_inner_radius(inner_radius);
		set_dcoil(dcoil);
		set_height(height);
		set_element_size(element_size);
		set_num_sections(num_sections);
	}

	// factory
	ShModelSolenoidPr ModelSolenoid::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<ModelSolenoid>();
	}

	// factory
	ShModelSolenoidPr ModelSolenoid::create(const fltp inner_radius, const fltp dcoil, const fltp height, const fltp element_size, const arma::uword num_sections){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<ModelSolenoid>(inner_radius, dcoil, height, element_size, num_sections);
	}

	// set radius
	void ModelSolenoid::set_inner_radius(const fltp inner_radius){
		inner_radius_ = inner_radius;
	}

	// set radius
	void ModelSolenoid::set_dcoil(const fltp dcoil){
		dcoil_ = dcoil;
	}

	// set radius
	void ModelSolenoid::set_height(const fltp height){
		height_ = height;
	}

	// set number of sections
	void ModelSolenoid::set_num_sections(const arma::uword num_sections){
		num_sections_ = num_sections;
	}
	
	// set element size
	void ModelSolenoid::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	// get radius
	fltp ModelSolenoid::get_inner_radius()const{
		return inner_radius_;
	}

	// get thickness
	fltp ModelSolenoid::get_dcoil()const{
		return dcoil_;
	}

	// get height
	fltp ModelSolenoid::get_height()const{
		return height_;
	}

	// get number of sections
	arma::uword ModelSolenoid::get_num_sections()const{
		return num_sections_;
	}

	// get azimuthal element size
	fltp ModelSolenoid::get_element_size()const {
		return element_size_;
	}

	// get base
	ShPathPr ModelSolenoid::get_input_path() const{
		// check input
		is_valid(true);

		// create circular path
		ShPathCirclePr circle = PathCircle::create(
			inner_radius_, num_sections_, element_size_);
		circle->set_offset(dcoil_);

		// return the circle
		return circle;
	}

	// get cross
	ShCrossPr ModelSolenoid::get_input_cross() const{
		// check input
		is_valid(true);

		// create circular path
		ShCrossRectanglePr rectangle = CrossRectangle::create(
			0, dcoil_, -height_/2, height_/2, element_size_, element_size_);

		// return the rectangle
		return rectangle;
	}

	// check input
	bool ModelSolenoid::is_valid(const bool enable_throws) const{
		if(!ModelCoilWrapper::is_valid(enable_throws))return false;
		if(inner_radius_<=0){if(enable_throws){rat_throw_line("inner radius must be larger than zero");} return false;};
		if(dcoil_<=0){if(enable_throws){rat_throw_line("coil thickness must be larger than zero");} return false;};
		if(height_<=0){if(enable_throws){rat_throw_line("coil height must be larger than zero");} return false;};
		if(num_sections_<=0){if(enable_throws){rat_throw_line("number of sections must be larger than zero");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string ModelSolenoid::get_type(){
		return "rat::mdl::modelsolenoid";
	}

	// method for serialization into json
	void ModelSolenoid::serialize(
		Json::Value &js, cmn::SList &list) const{

		// parent
		ModelCoilWrapper::serialize(js,list);

		// set type
		js["type"] = get_type();

		// set properties
		js["inner_radius"] = inner_radius_;
		js["dcoil"] = dcoil_;
		js["height"] = height_;
		js["num_sections"] = static_cast<int>(num_sections_);
		js["element_size"] = element_size_;
	}

	// method for deserialisation from json
	void ModelSolenoid::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		ModelCoilWrapper::deserialize(js,list,factory_list,pth);

		// set properties
		dcoil_ = js["dcoil"].ASFLTP();
		inner_radius_ = js["inner_radius"].ASFLTP();
		height_ = js["height"].ASFLTP();
		num_sections_ = js["num_sections"].asUInt64();
		element_size_ = js["element_size"].ASFLTP();
	}

}}