// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "area.hh"

#include "rat/common/error.hh"
#include "rat/common/elements.hh"
#include "rat/common/node.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	Area::Area(){
		
	}


	// constructor with input
	Area::Area(
		const arma::Mat<fltp> &Rn, 
		const arma::Mat<fltp> &N, 
		const arma::Mat<fltp> &D, 
		const arma::Mat<arma::uword> &n,
		const fltp hidden_thickness,
		const fltp hidden_width){
		
		// make sure it is a quad mesh
		//assert(n.n_rows==4);
		assert(arma::max(arma::max(n))<Rn.n_cols);

		// set mesh
		set_mesh(Rn,N,D,n);

		// set hidden thickness and width
		set_hidden_thickness(hidden_thickness);
		set_hidden_width(hidden_width);
	}

	// constructor with multiple areas as input
	Area::Area(const std::list<ShAreaPr>& areas, const bool merge_nodes){
		// check input
		if(areas.empty())rat_throw_line("no areas to combine");

		// number of source areas
		const arma::uword num_areas = areas.size();

		// count number of nodes and elements
		arma::uword num_nodes = 0llu;
		arma::uword num_elements = 0llu;

		// things that must be identical
		arma::uword num_nodes_per_element = 0llu;
		hidden_thickness_ = RAT_CONST(0.0);
		hidden_width_ = RAT_CONST(0.0);

		for(auto it=areas.begin();it!=areas.end();it++){
			// get area
			const ShAreaPr& source_area = (*it); assert(source_area!=NULL);

			// count
			num_nodes+=source_area->Rn_.n_cols;
			num_elements+=source_area->n_.n_cols;

			// get number of nodes per element
			if(num_nodes_per_element!=0){
				if(num_nodes_per_element!=source_area->n_.n_rows)
					rat_throw_line("element type does not match");
				if(hidden_thickness_!=source_area->hidden_thickness_)
					rat_throw_line("hidden thickness does not match");
				if(hidden_width_!=source_area->hidden_width_)
					rat_throw_line("hidden width does not match");
			}else{
				num_nodes_per_element = source_area->n_.n_rows;
				hidden_thickness_ = source_area->hidden_thickness_;
				hidden_width_ = source_area->hidden_width_;
			}
		}

		// check
		assert(num_nodes_per_element!=0llu);
		assert(hidden_thickness_!=RAT_CONST(0.0));
		assert(hidden_width_!=RAT_CONST(0.0));

		// zero nodes or elements?
		if(num_nodes==0llu)rat_throw_line("there area no nodes to combine");
		if(num_elements==0llu)rat_throw_line("there area no elements to combine");

		// allocate
		Rn_.set_size(2llu,num_nodes);
		N_.set_size(2llu,num_nodes);
		D_.set_size(2llu,num_nodes);
		n_.set_size(num_nodes_per_element, num_elements);
		Rc_.zeros();

		// reset counters
		num_nodes = 0llu; num_elements = 0llu;

		// walk over areas again
		for(auto it=areas.begin();it!=areas.end();it++){

			// get area
			const ShAreaPr& source_area = (*it); assert(source_area!=NULL);
			const arma::uword num_source_nodes = source_area->Rn_.n_cols;
			const arma::uword num_source_elements = source_area->n_.n_cols;

			// insert nodes
			Rn_.cols(num_nodes,num_nodes+num_source_nodes-1) = source_area->Rn_;
			N_.cols(num_nodes,num_nodes+num_source_nodes-1) = source_area->N_;
			D_.cols(num_nodes,num_nodes+num_source_nodes-1) = source_area->D_;

			// insert elements and apply offset for the nodes
			n_.cols(num_elements,num_elements+num_source_elements-1) = source_area->n_ + num_nodes;

			// add source area center point contribution
			Rc_ += source_area->Rc_/static_cast<fltp>(num_areas);

			// update counters
			num_nodes += num_source_nodes;
			num_elements += num_source_elements;
		}

		// perform node merge
		if(merge_nodes){
			// merge the nodes
			arma::Mat<fltp> data = arma::join_vert(N_,D_);
			const arma::Col<arma::uword> node_indexing = cmn::Extra::combine_nodes(Rn_,data);
			n_ = arma::reshape(node_indexing(arma::vectorise(n_)),n_.n_rows,n_.n_cols);
			N_ = data.rows(0,1); D_ = data.rows(2,3);

			// let each element start with smallest index
			for(arma::uword i=0;i<n_.n_cols;i++)
				n_.col(i) = arma::shift(n_.col(i), -static_cast<arma::sword>(n_.col(i).index_min()));

			// merge duplicate elements
			n_ = cmn::Extra::unique_rows(n_.t()).t();
		}

		// check mesh
		assert(n_.max()<Rn_.n_cols);
	}


	// factory with dimension input
	ShAreaPr Area::create(
		const arma::Mat<fltp> &Rn, 
		const arma::Mat<fltp> &N, 
		const arma::Mat<fltp> &D, 
		const arma::Mat<arma::uword> &n,
		const fltp hidden_thickness,
		const fltp hidden_width){
		return std::make_shared<Area>(
			Rn,N,D,n,hidden_thickness,hidden_width);
	}

	// factory that combines other areas
	ShAreaPr Area::create(const std::list<ShAreaPr>& areas, const bool merge_nodes){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<Area>(areas,merge_nodes);
	}

	// method for setting the mesh
	void Area::set_mesh(
		const arma::Mat<fltp> &Rn,
		const arma::Mat<fltp> &N, 
		const arma::Mat<fltp> &D, 
		const arma::Mat<arma::uword> &n){

		// check mesh
		assert(n.max()<Rn.n_cols);

		// set to self
		Rn_ = Rn; N_ = N; D_ = D; n_ = n;
	}

	// set center point
	void Area::set_center_coord(const arma::Col<fltp>::fixed<2>& Rc){
		assert(Rc.is_finite());
		Rc_ = Rc;
	}

	// get center point
	const arma::Col<fltp>::fixed<2>& Area::get_center_coord()const{
		return Rc_;
	}

	// set hidden dimension
	void Area::set_hidden_thickness(const fltp hidden_thickness){
		// assert(hidden_thickness>0);
		hidden_thickness_ = hidden_thickness;
	}

	// set hidden dimension
	void Area::set_hidden_width(const fltp hidden_width){
		// assert(hidden_width>0);
		hidden_width_ = hidden_width;
	}

	// get hidden dimension
	fltp Area::get_hidden_thickness() const{
		return hidden_thickness_;
	}

	// get hidden dimension
	fltp Area::get_hidden_width() const{
		return hidden_width_;
	}

	// get hidden dimension
	fltp Area::get_hidden_dim() const{
		return hidden_thickness_*hidden_width_;
	}

	// get number of elements
	arma::uword Area::get_num_elements() const{
		return n_.n_cols;
	}

	// get number of nodes
	arma::uword Area::get_num_nodes() const{
		return Rn_.n_cols;
	}

	// get number of elements in perimeter
	arma::uword Area::get_num_perimeter() const{
		return p_.n_cols;
	}

	// get number of corner nodes
	arma::uword Area::get_num_corner() const{
		return q_.n_elem;
	}

	// get node coordinates
	const arma::Mat<fltp>& Area::get_nodes() const{
		return Rn_;
	}

	// get transverse direction
	const arma::Mat<fltp>& Area::get_transverse() const{
		return D_;
	}

	// get normal direction
	const arma::Mat<fltp>& Area::get_normal() const{
		return N_;
	}

	// get elements
	const arma::Mat<arma::uword>& Area::get_elements() const{
		return n_;
	}
	
	// set perimeter elements
	void Area::set_perimeter(const arma::Mat<arma::uword>& p){
		p_ = p;
	}

	// set internal nodes
	void Area::set_internal(const arma::Mat<arma::uword>& i){
		i_ = i;
	}

	// set corner nodes
	void Area::set_corner_nodes(const arma::Col<arma::uword>& q){
		q_ = q;
	}

	// get perimeter elements
	const arma::Mat<arma::uword>& Area::get_perimeter()const{
		return p_;
	}

	// get perimeter elements
	const arma::Col<arma::uword>& Area::get_corner_nodes()const{
		return q_;
	}

	// setup perimeter internally
	void Area::setup_perimeter(){
		create_edge_matrix(p_,i_);
	}

	// setup corner nodes
	void Area::setup_corner_nodes(){
		q_ = calculate_corner_nodes();
	}

	// smoothen mesh using simple node averaging algorithm
	void Area::smoothen(const arma::uword N, const fltp dmp){
		// check
		assert(!p_.empty()); assert(!i_.empty());

		// get counters
		const arma::uword num_nodes = get_num_nodes();

		// // create edges
		// arma::Mat<arma::uword> s,i; 
		// create_edge_matrix(s,i);

		// find indexes of unique nodes
		arma::Row<arma::uword> idx_node_surface = 
			arma::unique<arma::Row<arma::uword> >(arma::reshape(p_,1,p_.n_elem));

		// allocate internal node indexes
		arma::Row<arma::uword> idx_node_internal = 
			arma::regspace<arma::Row<arma::uword> >(0,num_nodes-1);

		// check which nodes are internal
		arma::Row<arma::uword> keep(num_nodes,arma::fill::ones);
		keep.cols(idx_node_surface).fill(0);

		// remove non-internal indexes from the list
		idx_node_internal = idx_node_internal.cols(arma::find(keep==1));

		// x and y coordinates of nodes
		arma::Row<fltp> x = Rn_.row(0); 
		arma::Row<fltp> y = Rn_.row(1); 

		// get indexes
		const arma::Row<arma::uword> a = i_.row(0);
		const arma::Row<arma::uword> b = i_.row(1);

		// iterations
		for(arma::uword j=0;j<N;j++){
			// get from store
			arma::Row<fltp> xnew = x; 
			arma::Row<fltp> ynew = y;

			// walk over internal nodes
			for(arma::uword k=0;k<idx_node_internal.n_cols;k++){
				// get this node's index
				arma::uword idx = idx_node_internal(k);

				// get neighbour indexes
				arma::Row<arma::uword> idxnb = arma::join_horiz(a.cols(arma::find(b==idx)),b.cols(arma::find(a==idx)));

				// calculate new position through
				// averaging with neighbours
				arma::Row<fltp> xn = x.cols(idxnb); arma::Row<fltp> yn = y.cols(idxnb);
				xnew(idx) = arma::sum(xn)/xn.n_elem; ynew(idx) = arma::sum(yn)/yn.n_elem;
			}

			// update
			x += dmp*(xnew-x); 
			y += dmp*(ynew-y);
		}

		// give p
		Rn_ = arma::join_vert(x,y);
	}

	// analyze function
	void Area::create_edge_matrix(
		arma::Mat<arma::uword> &s, 
		arma::Mat<arma::uword> &in) const{

		// point mesh (2D) will become linemesh in 3D
		if(n_.n_rows==1){
			s = n_; in.set_size(1,0); return;
		}

		// line mesh (2D) will become quadmesh in 3D
		else if(n_.n_rows==2){
			s = n_; in.set_size(1,0); return;
		}

		// must be quad mesh (2D) 
		else if(n_.n_rows!=4){
			rat_throw_line("area mesh not recognised");
		}

		// get counters
		const arma::uword num_elements = get_num_elements();

		// get list of faces for each element
		const arma::Mat<arma::uword>::fixed<4,2> M = cmn::Quadrilateral::get_edges();

		// create list of all faces present in mesh
		arma::Mat<arma::uword> S(2,num_elements*4);
		for(arma::uword i=0;i<4;i++){
			S.cols(i*num_elements,(i+1)*num_elements-1) = n_.rows(M.row(i));
		}

		// sort each column in S
		arma::Mat<arma::uword> Ss(2,num_elements*4);
		for(arma::uword i=0;i<num_elements*4;i++){
			Ss.col(i) = arma::sort(S.col(i));
		}

		// sort S by rows
		for(arma::uword i=0;i<2;i++){
			arma::Col<arma::uword> idx = arma::stable_sort_index(Ss.row(1-i));
			Ss = Ss.cols(idx); S = S.cols(idx);
		}	
		
		// find duplicates and mark
		arma::Row<arma::uword> duplicates = 
			arma::all(Ss.cols(0,num_elements*4-2)==Ss.cols(1,num_elements*4-1),0);

		// extend duplicate list to contain first and second
		arma::Row<arma::uword> extended(num_elements*4,arma::fill::zeros);
		extended.head(num_elements*4-1) += duplicates;
		extended.tail(num_elements*4-1) += duplicates;

		// remove marked indices
		s = S.cols(arma::find(extended==0));
		in = S.cols(arma::find(duplicates!=0));

		// sort edge matrix
		arma::Col<arma::uword> sidx = arma::sort_index(s.row(1));
		s = s.cols(sidx); 
		sidx = arma::stable_sort_index(s.row(0)); 
		s = s.cols(sidx); 

		// start of closed loop
		arma::uword idx0 = 0;

		// walk over s and put edges in the correct order
		for(arma::uword i=0;i<s.n_cols-1;i++){
			// get target node
			const arma::uword target_node = s(1,i);

			// find next section by searching first node list for the target node
			const arma::Row<arma::uword> idx = arma::find(s.submat(0,i+1,0,s.n_cols-1)==target_node,1,"first").t();

			// check if exists
			if(!idx.is_empty()){
				// move
				s.swap_cols(i+1+arma::as_scalar(idx),i+1);
			}

			// the target node was not found (probably because it was already used)
			else{
				// check for full circle (this check fails if for example mesh is not fully clockwise)
				if(s(0,idx0)!=s(1,i))
					rat_throw_line("cannot find node: " + std::to_string(s(1,i)));

				// move idx0 to the next index
				idx0 = i+1;
			}
		}

		// check
		assert(s.max()<Rn_.n_cols);
	}

	// get indexes of corner nodes
	// these wil become the edges of the mesh
	arma::Col<arma::uword> Area::calculate_corner_nodes() const{
		// create edges
		// arma::Mat<arma::uword> s,i; 
		// create_edge_matrix(s,i);
		assert(!p_.empty());
		if(p_.n_elem==1)return p_;

		// calculate angle between successive elements
		arma::Row<fltp> alpha(p_.n_cols);
		for(arma::uword k=0;k<p_.n_cols;k++){
			// prevent overlow using modulus
			const arma::uword idx2 = (k+1)%p_.n_cols;

			// calculate direction vectors
			const arma::Col<fltp>::fixed<2> dR1 = Rn_.col(p_(1,k)) - Rn_.col(p_(0,k));
			const arma::Col<fltp>::fixed<2> dR2 = Rn_.col(p_(1,idx2)) - Rn_.col(p_(0,idx2));

			// calculate angle
			alpha(k) = std::acos(arma::as_scalar(cmn::Extra::dot(dR1,dR2)/
				(cmn::Extra::vec_norm(dR1)%cmn::Extra::vec_norm(dR2))));
		}

		// find corner node indexes
		// const fltp corner_tolerance = arma::Datum<fltp>::pi/4;
		const fltp corner_tolerance = 1e-9;
		arma::Col<arma::uword> q = p_.submat(
			arma::Col<arma::uword>{1}, arma::find(
			arma::abs(alpha)>corner_tolerance)).t();

		// extend
		if(n_.n_rows==2){
			q = arma::join_vert(arma::Col<arma::uword>{p_.front()},
				q,arma::Col<arma::uword>{p_.back()});
		}

		// return corner node indexes
		return q;
	}

	// calculate areas by splitting elements into triangles
	arma::Row<fltp> Area::get_areas()const{
		// check if surface
		if(n_.is_empty())return{};

		// for quadrilaterals area 
		if(n_.n_rows==4){
			// get vectors spanning face
			const arma::Mat<fltp> V1 = Rn_.cols(n_.row(1)) - Rn_.cols(n_.row(0));
			const arma::Mat<fltp> V2 = Rn_.cols(n_.row(3)) - Rn_.cols(n_.row(0));
			const arma::Mat<fltp> V3 = Rn_.cols(n_.row(1)) - Rn_.cols(n_.row(2));
			const arma::Mat<fltp> V4 = Rn_.cols(n_.row(3)) - Rn_.cols(n_.row(2));

			// calculate cross products (only z component)
			const arma::Row<fltp> A1 = arma::abs(V1.row(0)%V2.row(1) - V1.row(1)%V2.row(0))/2;
			const arma::Row<fltp> A2 = arma::abs(V3.row(0)%V4.row(1) - V3.row(1)%V4.row(0))/2;
			
			// sum values and return
			return get_hidden_dim()*(A1 + A2);
		}

		// for line elements area
		else if(n_.n_rows==2){
			// get edge vector
			const arma::Mat<fltp> V1 = Rn_.cols(n_.row(1)) - Rn_.cols(n_.row(0));

			// sum values and return
			return get_hidden_dim()*arma::sqrt(arma::sum(V1%V1,0));
		}

		// for point sources area is just 1
		// this ensures that the current is 
		// set fully to this element
		else if(n_.n_rows==1){
			return arma::Row<fltp>{get_hidden_dim()};
		}

		// do not recognise this element type
		else rat_throw_line("unknown element type");
	}

	// apply multiple transformations
	void Area::apply_transformations(const std::list<ShTransPr> &trans, const fltp time){
		for(auto it=trans.begin();it!=trans.end();it++)
			apply_transformation(*it, time);
	}

	// apply transformation
	void Area::apply_transformation(const ShTransPr &trans, const fltp time){
		// // apply to vectors	
		// trans->apply_vectors(R_,L_,'L');
		// trans->apply_vectors(Rn_,N_,'N');
		// trans->apply_vectors(Rn_,D_,'D');
		// trans->apply_vectors(R_,B_,'B');

		// apply to coordinates
		trans->apply_area_coords(Rn_, time);

		// apply to mesh
		trans->apply_area_mesh(n_, get_num_nodes(), time);
	}

	arma::Row<fltp> Area::get_perimeter_lengths()const{
		// check input
		assert(!p_.empty());

		// calculate edge vectors
		const arma::Mat<fltp> dR = Rn_.cols(p_.row(1))-Rn_.cols(p_.row(0));

		// calculate length, sum and return
		return cmn::Extra::vec_norm(dR);
	}

	// get conductor area
	// calculated by splitting elements into triangles
	fltp Area::get_area() const{
		// sum values and return
		return arma::accu(get_areas());
	}

	// get length of the perimeter
	fltp Area::get_perimeter_length()const{
		return arma::accu(get_perimeter_lengths());
	}

	// get max distance from barycenter to nodes
	arma::Row<fltp> Area::get_element_size() const{
		// allocate output
		arma::Row<fltp> dbary(n_.n_cols);

		// walk over elements
		for(arma::uword i=0;i<n_.n_cols;i++){
			const arma::Mat<fltp> Rn = Rn_.cols(n_.col(i));
			const arma::Col<fltp> R0 = arma::mean(Rn,1);
			dbary(i) = arma::min(cmn::Extra::vec_norm(Rn.each_col() - R0));
		}

		// return element size
		return dbary;
	}

}}