// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathsuperellipse.hh"

// rat-common headers
#include "rat/common/extra.hh"

// rat-models headers
#include "darboux.hh"
#include "transreverse.hh"
#include "transflip.hh"
#include "pathbezier.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathSuperEllipse::PathSuperEllipse(){
		set_name("Super Ellipse");
	}

	// default constructor
	PathSuperEllipse::PathSuperEllipse(
		const fltp radius, 
		const fltp ba, const fltp beta,
		const fltp phi, 
		const fltp arclength,
		const fltp element_size, 
		const arma::uword num_poles, 
		const fltp n1, const fltp n2, 
		const fltp zeta1, const fltp zeta2,
		const fltp delta_radius) : PathSuperEllipse(){
		
		// set to self
		set_num_poles(num_poles);
		set_radius(radius); set_ba(ba); set_phi(phi); set_beta(beta);
		set_arclength(arclength); set_element_size(element_size); set_n1(n1); 
		set_n2(n2); set_zeta1(zeta1); set_zeta2(zeta2); set_delta_radius(delta_radius);
	}

	// factory
	ShPathSuperEllipsePr PathSuperEllipse::create(){
		//return ShPathSuperEllipsePr(new PathSuperEllipse);
		return std::make_shared<PathSuperEllipse>();
	}

	// factory
	ShPathSuperEllipsePr PathSuperEllipse::create(
		const fltp radius, const fltp ba,const fltp beta,
		const fltp phi, const fltp arclength,
		const fltp element_size, const arma::uword num_poles, 
		const fltp n1, const fltp n2, 
		const fltp zeta1, const fltp zeta2,
		const fltp delta_radius){
		//return ShPathSuperEllipsePr(new PathSuperEllipse);
		return std::make_shared<PathSuperEllipse>(radius, ba, beta, phi, 
			arclength, element_size, num_poles, n1, n2, zeta1, zeta2, delta_radius);
	}

	// setters
	void PathSuperEllipse::set_reverse(const bool reverse){
		reverse_ = reverse;
	}

	void PathSuperEllipse::set_num_poles(const arma::uword num_poles){
		num_poles_ = num_poles;
	}

	void PathSuperEllipse::set_n1(const fltp n1){
		n1_ = n1;
	}

	void PathSuperEllipse::set_n2(const fltp n2){
		n2_ = n2;
	}

	void PathSuperEllipse::set_radius(const fltp radius){
		radius_ = radius;
	}
	
	void PathSuperEllipse::set_phi(const fltp phi){
		phi_ = phi;
	}
	
	void PathSuperEllipse::set_arclength(const fltp arclength){
		arclength_ = arclength;
	}

	void PathSuperEllipse::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	void PathSuperEllipse::set_ba(const fltp ba){
		ba_ = ba;
	}

	void PathSuperEllipse::set_beta(const fltp beta){
		beta_ = beta;
	}

	void PathSuperEllipse::set_zeta1(const fltp zeta1){
		zeta1_ = zeta1;
	}

	void PathSuperEllipse::set_zeta2(const fltp zeta2){
		zeta2_ = zeta2;
	}

	void PathSuperEllipse::set_delta_radius(const fltp delta_radius){
		delta_radius_ = delta_radius;
	}

	// getters
	bool PathSuperEllipse::get_reverse() const{
		return reverse_;
	}

	fltp PathSuperEllipse::get_n1() const{
		return n1_;
	}

	fltp PathSuperEllipse::get_n2() const{
		return n2_;
	}

	fltp PathSuperEllipse::get_radius() const{
		return radius_;
	}
	
	fltp PathSuperEllipse::get_phi() const{
		return phi_;
	}
	
	fltp PathSuperEllipse::get_arclength() const{
		return arclength_;
	}

	fltp PathSuperEllipse::get_element_size() const{
		return element_size_;
	}

	fltp PathSuperEllipse::get_ba() const{
		return ba_;
	}

	fltp PathSuperEllipse::get_beta() const{
		return beta_;
	}

	fltp PathSuperEllipse::get_zeta1() const{
		return zeta1_;
	}

	fltp PathSuperEllipse::get_zeta2() const{
		return zeta2_;
	}

	fltp PathSuperEllipse::get_delta_radius() const{
		return delta_radius_;
	}

	arma::uword PathSuperEllipse::get_num_poles()const{
		return num_poles_;
	}

	// get frame
	ShFramePr PathSuperEllipse::create_frame(const MeshSettings &stngs) const{
		// check validity
		is_valid(true);

		// calculate slope of cone
		const fltp slope = std::tan(beta_);

		
		// calculate second angle
		const fltp xi = arma::Datum<fltp>::pi/(2*num_poles_) - phi_;
		const fltp phi = arma::Datum<fltp>::pi/2 - xi;

		// scale factor in z
		const fltp y0 = radius_*std::sin(phi);
		const fltp yscale = (radius_ - y0 + delta_radius_)/(radius_ - y0);


		// convert order to float
		const fltp n1 = n1_;
		const fltp n2 = n2_;

		// number of elements
		arma::uword num_elements = 101;

		// length of the curve
		arma::Row<fltp> t = arma::linspace<arma::Row<fltp> >(0.0,arclength_,num_elements);

		// calculate length
		const arma::Row<fltp> lz = ba_*arma::pow(arma::sin(t),RAT_CONST(2.0)/n1);
		const arma::Row<fltp> lr = radius_ - slope*lz;
		const arma::Row<fltp> lx = lr%arma::sin(xi*arma::pow(arma::cos(t),2.0/n2));
		const arma::Row<fltp> ly = lr%arma::cos(xi*arma::pow(arma::cos(t),2.0/n2));

		// calculate length
		const arma::Row<fltp> ell = arma::join_horiz(
			arma::Row<fltp>{0}, arma::cumsum(cmn::Extra::vec_norm(arma::diff(arma::join_vert(lx,ly,lz),1,1))));

		// update elements
		num_elements = std::max(2llu,arma::uword(ell.back()/element_size_));

		// regular time
		{
			arma::Col<fltp> tt;
			arma::interp1(ell.t(), t.t(), arma::linspace<arma::Col<fltp> >(0,ell.back(),num_elements), tt);
			t = tt.t();
		}

		// Field computation for accelerator magnets, page 627 (eq 19.50)
		const arma::Row<fltp> z = ba_*arma::pow(arma::sin(t),RAT_CONST(2.0)/n1);
		arma::Row<fltp> vz = (2*ba_*arma::cos(t)%arma::pow(arma::sin(t),RAT_CONST(2.0)/n1-1))/n1;
		arma::Row<fltp> az = -(2*ba_*arma::pow(arma::sin(t),RAT_CONST(2.0)/n1-2)%
			(n1*arma::square(arma::sin(t))+(n1-2)*arma::square(arma::cos(t))))/(n1*n1);
		arma::Row<fltp> jz = (4*ba_*arma::cos(t)%arma::pow(arma::sin(t),2.0/n1-3)%
			((n1*n1-3*n1)*arma::square(arma::sin(t))+(n1*n1-3*n1+2)*arma::square(arma::cos(t))))/(n1*n1*n1);

		// fix derivatives at starting point
		az(arma::find(t==0)).fill(0.0); 
		jz(arma::find(t==0)).fill(0.0);

		// calculate radius and derivatives
		const arma::Row<fltp> rr = radius_ - slope*z;
		const arma::Row<fltp> vr = -slope*vz; 
		const arma::Row<fltp> ar = -slope*az; 
		const arma::Row<fltp> jr = -slope*jz;

		// superellipse Russenschuck, Field computation for accelerator magnets, page 627 (eq 19.50)
		const arma::Row<fltp> x = rr%arma::sin(xi*arma::pow(arma::cos(t),2.0/n2));
		const arma::Row<fltp> y = rr%arma::cos(xi*arma::pow(arma::cos(t),2.0/n2));

		// analytical velocity (from here on the equations are not human readable)
		const arma::Row<fltp> vx = vr%arma::sin(xi*arma::pow(arma::cos(t),2.0/n2))-(2*xi*rr%arma::pow(arma::cos(t),2.0/n2-1)%arma::sin(t)%arma::cos(xi*arma::pow(arma::cos(t),2.0/n2)))/n2;
		const arma::Row<fltp> vy = (2*xi*rr%arma::pow(arma::cos(t),2.0/n2-1)%arma::sin(t)%arma::sin(xi*arma::pow(arma::cos(t),2.0/n2)))/n2+vr%arma::cos(xi*arma::pow(arma::cos(t),2.0/n2));

		// analytical acceleration
		arma::Row<fltp> ax = -(4*(xi*xi)*rr%arma::pow(arma::cos(t),4.0/n2-2)%arma::square(arma::sin(t))%arma::sin(xi*arma::pow(arma::cos(t),2.0/n2)))/(n2*n2)+ar%arma::sin(xi*arma::pow(arma::cos(t),2.0/n2))+(2*(2.0/n2-1)*xi*rr%arma::pow(arma::cos(t),2.0/n2-2)%arma::square(arma::sin(t))%arma::cos(xi*arma::pow(arma::cos(t),2.0/n2)))/n2-(4*xi*vr%arma::pow(arma::cos(t),2.0/n2-1)%arma::sin(t)%arma::cos(xi*arma::pow(arma::cos(t),2.0/n2)))/n2-(2*xi*rr%arma::pow(arma::cos(t),2.0/n2)%arma::cos(xi*arma::pow(arma::cos(t),2.0/n2)))/n2;
		arma::Row<fltp> ay = (((2*n2-4)*xi*rr%arma::pow(arma::cos(t),2.0/n2)%arma::square(arma::sin(t))+4*n2*xi*vr%arma::pow(arma::cos(t),2.0/n2+1)%arma::sin(t)+2*n2*xi*rr%arma::pow(arma::cos(t),2.0/n2+2))%arma::sin(xi*arma::pow(arma::cos(t),2.0/n2))+((n2*n2)*ar%arma::square(arma::cos(t))-4*(xi*xi)*rr%arma::pow(arma::cos(t),4.0/n2)%arma::square(arma::sin(t)))%arma::cos(xi*arma::pow(arma::cos(t),2.0/n2)))/((n2*n2)*arma::square(arma::cos(t)));

		// analytical jerk
		arma::Row<fltp> jx = -((-(16*(xi*xi)*rr%arma::pow(arma::cos(t),4.0/n2-1)%arma::pow(arma::sin(t),3))/n2+4*(xi*xi)*vr%arma::pow(arma::cos(t),4.0/n2)%arma::square(arma::sin(t))+8*(xi*xi)*rr%arma::pow(arma::cos(t),4.0/n2+1)%arma::sin(t)+2*(n2*n2)*ar%arma::cos(t)%arma::sin(t)-(n2*n2)*jr%arma::square(arma::cos(t)))%arma::sin(xi*arma::pow(arma::cos(t),2.0/n2))+(2*xi*arma::pow(arma::cos(t),2.0/n2-1)%arma::sin(t)%((2*n2-4)*xi*rr%arma::pow(arma::cos(t),2.0/n2)%arma::square(arma::sin(t))+4*n2*xi*vr%arma::pow(arma::cos(t),2.0/n2+1)%arma::sin(t)+2*n2*xi*rr%arma::pow(arma::cos(t),2.0/n2+2))%arma::sin(xi*arma::pow(arma::cos(t),2.0/n2)))/n2+(-(2*(2*n2-4)*xi*rr%arma::pow(arma::cos(t),2.0/n2-1)%arma::pow(arma::sin(t),3))/n2+(2*n2-4)*xi*vr%arma::pow(arma::cos(t),2.0/n2)%arma::square(arma::sin(t))-4*(2.0/n2+1)*n2*xi*vr%arma::pow(arma::cos(t),2.0/n2)%arma::square(arma::sin(t))+4*n2*xi*ar%arma::pow(arma::cos(t),2.0/n2+1)%arma::sin(t)+2*(2*n2-4)*xi*rr%arma::pow(arma::cos(t),2.0/n2+1)%arma::sin(t)-2*(2.0/n2+2)*n2*xi*rr%arma::pow(arma::cos(t),2.0/n2+1)%arma::sin(t)+6*n2*xi*vr%arma::pow(arma::cos(t),2.0/n2+2))%arma::cos(xi*arma::pow(arma::cos(t),2.0/n2))-(2*xi*arma::pow(arma::cos(t),2.0/n2-1)%arma::sin(t)%(4*(xi*xi)*rr%arma::pow(arma::cos(t),4.0/n2)%arma::square(arma::sin(t))-(n2*n2)*ar%arma::square(arma::cos(t)))%arma::cos(xi*arma::pow(arma::cos(t),2.0/n2)))/n2)/((n2*n2)*arma::square(arma::cos(t)))-(2*arma::sin(t)%((4*(xi*xi)*rr%arma::pow(arma::cos(t),4.0/n2)%arma::square(arma::sin(t))-(n2*n2)*ar%arma::square(arma::cos(t)))%arma::sin(xi*arma::pow(arma::cos(t),2.0/n2))+((2*n2-4)*xi*rr%arma::pow(arma::cos(t),2.0/n2)%arma::square(arma::sin(t))+4*n2*xi*vr%arma::pow(arma::cos(t),2.0/n2+1)%arma::sin(t)+2*n2*xi*rr%arma::pow(arma::cos(t),2.0/n2+2))%arma::cos(xi*arma::pow(arma::cos(t),2.0/n2))))/((n2*n2)*arma::pow(arma::cos(t),3));
		arma::Row<fltp> jy = ((-(2*(2*n2-4)*xi*rr%arma::pow(arma::cos(t),2.0/n2-1)%arma::pow(arma::sin(t),3))/n2+(2*n2-4)*xi*vr%arma::pow(arma::cos(t),2.0/n2)%arma::square(arma::sin(t))-4*(2.0/n2+1)*n2*xi*vr%arma::pow(arma::cos(t),2.0/n2)%arma::square(arma::sin(t))+4*n2*xi*ar%arma::pow(arma::cos(t),2.0/n2+1)%arma::sin(t)+2*(2*n2-4)*xi*rr%arma::pow(arma::cos(t),2.0/n2+1)%arma::sin(t)-2*(2.0/n2+2)*n2*xi*rr%arma::pow(arma::cos(t),2.0/n2+1)%arma::sin(t)+6*n2*xi*vr%arma::pow(arma::cos(t),2.0/n2+2))%arma::sin(xi*arma::pow(arma::cos(t),2.0/n2))+(2*xi*arma::pow(arma::cos(t),2.0/n2-1)%arma::sin(t)%((n2*n2)*ar%arma::square(arma::cos(t))-4*(xi*xi)*rr%arma::pow(arma::cos(t),4.0/n2)%arma::square(arma::sin(t)))%arma::sin(xi*arma::pow(arma::cos(t),2.0/n2)))/n2+((16*(xi*xi)*rr%arma::pow(arma::cos(t),4.0/n2-1)%arma::pow(arma::sin(t),3))/n2-4*(xi*xi)*vr%arma::pow(arma::cos(t),4.0/n2)%arma::square(arma::sin(t))-8*(xi*xi)*rr%arma::pow(arma::cos(t),4.0/n2+1)%arma::sin(t)-2*(n2*n2)*ar%arma::cos(t)%arma::sin(t)+(n2*n2)*jr%arma::square(arma::cos(t)))%arma::cos(xi*arma::pow(arma::cos(t),2.0/n2))-(2*xi*arma::pow(arma::cos(t),2.0/n2-1)%arma::sin(t)%((2*n2-4)*xi*rr%arma::pow(arma::cos(t),2.0/n2)%arma::square(arma::sin(t))+4*n2*xi*vr%arma::pow(arma::cos(t),2.0/n2+1)%arma::sin(t)+2*n2*xi*rr%arma::pow(arma::cos(t),2.0/n2+2))%arma::cos(xi*arma::pow(arma::cos(t),2.0/n2)))/n2)/((n2*n2)*arma::square(arma::cos(t)))+(2*arma::sin(t)%(((2*n2-4)*xi*rr%arma::pow(arma::cos(t),2.0/n2)%arma::square(arma::sin(t))+4*n2*xi*vr%arma::pow(arma::cos(t),2.0/n2+1)%arma::sin(t)+2*n2*xi*rr%arma::pow(arma::cos(t),2.0/n2+2))%arma::sin(xi*arma::pow(arma::cos(t),2.0/n2))+((n2*n2)*ar%arma::square(arma::cos(t))-4*(xi*xi)*rr%arma::pow(arma::cos(t),4.0/n2)%arma::square(arma::sin(t)))%arma::cos(xi*arma::pow(arma::cos(t),2.0/n2))))/((n2*n2)*arma::pow(arma::cos(t),3));

		// override nans (necessary?)
		ax(arma::find(t==arma::Datum<fltp>::pi/2)).fill(0.0);
		jx(arma::find(t==arma::Datum<fltp>::pi/2)).fill(0.0);
		jy(arma::find(t==arma::Datum<fltp>::pi/2)).fill(0.0);

		// combine vectors
		arma::Mat<fltp> R = arma::join_vert(x,y0+yscale*(y-y0),z);
		const arma::Mat<fltp> V = arma::join_vert(vx,yscale*vy,vz);
		const arma::Mat<fltp> A = arma::join_vert(ax,yscale*ay,az);
		const arma::Mat<fltp> J = arma::join_vert(jx,yscale*jy,jz);

		// setup darboux vectors
		Darboux db(V,A,J);

		// apply extra twist around L
		const arma::Row<fltp> P = {zeta1_,zeta1_,zeta2_,zeta2_};
		const arma::field<arma::Mat<fltp> > zeta_fld = PathBezier::create_bezier_tn(
			arma::linspace<arma::Row<fltp> >(RAT_CONST(0.0),RAT_CONST(1.0),num_elements),P,2llu);
		const arma::Row<fltp> zeta = zeta_fld(0).row(0);
		const arma::Row<fltp> dzetadt = zeta_fld(1).row(0);
		const arma::Row<fltp> dtds = RAT_CONST(1.0)/cmn::Extra::vec_norm(V);
		db.set_additional_twist(zeta, dzetadt%dtds);
		assert(dzetadt.front()==RAT_CONST(0.0));
		assert(dzetadt.back()==RAT_CONST(0.0));

		// create darboux
		db.setup(true);
		arma::Mat<fltp> L = db.get_longitudinal();
		arma::Mat<fltp> N = db.get_normal();
		arma::Mat<fltp> D = db.get_transverse();
		arma::Mat<fltp> B = db.get_binormal();

		// override first
		L.head_cols(1) = arma::Col<fltp>::fixed<3>{-std::cos(phi)*std::sin(beta_), -std::sin(phi)*std::sin(beta_), std::cos(beta_)};
		N.head_cols(1) = arma::Col<fltp>::fixed<3>{std::sin(phi + zeta1_), -std::cos(phi + zeta1_), 0.0};
		D.head_cols(1) = cmn::Extra::cross(N.head_cols(1), L.head_cols(1));

		// override last
		L.tail_cols(1) = arma::Col<fltp>::fixed<3>{-1,0,0};
		D.tail_cols(1) = B.tail_cols(1); D(0,D.n_cols-1) = 0;
		N.tail_cols(1) = cmn::Extra::cross(L.tail_cols(1), D.tail_cols(1));

		// check if all vectors are unit length
		assert(std::abs(arma::as_scalar(cmn::Extra::vec_norm(L.tail_cols(1))) - 1.0)<1e-14);
		assert(std::abs(arma::as_scalar(cmn::Extra::vec_norm(D.tail_cols(1))) - 1.0)<1e-14);
		assert(std::abs(arma::as_scalar(cmn::Extra::vec_norm(N.tail_cols(1))) - 1.0)<1e-14);
		assert(std::abs(arma::as_scalar(cmn::Extra::vec_norm(L.head_cols(1))) - 1.0)<1e-14);
		assert(std::abs(arma::as_scalar(cmn::Extra::vec_norm(D.head_cols(1))) - 1.0)<1e-14);
		assert(std::abs(arma::as_scalar(cmn::Extra::vec_norm(N.head_cols(1))) - 1.0)<1e-14);

		// check orthogonality of start and end points
		assert(arma::as_scalar(cmn::Extra::dot(L.tail_cols(1), D.tail_cols(1)))<1e-14);
		assert(arma::as_scalar(cmn::Extra::dot(L.tail_cols(1), N.tail_cols(1)))<1e-14);
		assert(arma::as_scalar(cmn::Extra::dot(D.tail_cols(1), N.tail_cols(1)))<1e-14);
		assert(arma::as_scalar(cmn::Extra::dot(L.head_cols(1), D.head_cols(1)))<1e-14);
		assert(arma::as_scalar(cmn::Extra::dot(L.head_cols(1), N.head_cols(1)))<1e-14);
		assert(arma::as_scalar(cmn::Extra::dot(D.head_cols(1), N.head_cols(1)))<1e-14);

		// check handedness
		assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N,L),D)>RAT_CONST(0.0)));
		
		// create frame
		const ShFramePr frame = Frame::create(R,L,N,D);

		// reverse
		if(reverse_)frame->apply_transformations({TransReflect::create(cmn::Extra::null_vec(),{1,0,0}), TransReverse::create()}, stngs.time);

		// apply transformations
		frame->apply_transformations(get_transformations(),stngs.time);

		// return frame
		return frame;
	}

	// validity check
	bool PathSuperEllipse::is_valid(const bool enable_throws) const{
		if(radius_<=0){if(enable_throws){rat_throw_line("radius must be larger than zero");} return false;};
		if(arclength_>arma::Datum<fltp>::pi/2){if(enable_throws){rat_throw_line("arclength can not exceed pi/2");} return false;};
		if(arclength_<=0){if(enable_throws){rat_throw_line("arclength must be larger than zero");} return false;};
		if(beta_<=-arma::Datum<fltp>::pi/2 || beta_>=arma::Datum<fltp>::pi/2){if(enable_throws){rat_throw_line("beta must be between the interval [-pi/2, pi/2].");} return false;};
		if(num_poles_<=0){if(enable_throws){rat_throw_line("number of poles must be positive");} return false;};
		return true;
	}

	// get type
	std::string PathSuperEllipse::get_type(){
		return "rat::mdl::pathsuperellipse";
	}

	// method for serialization into json
	void PathSuperEllipse::serialize(Json::Value &js, cmn::SList &list) const{
		// parent nodes
		Transformations::serialize(js,list);

		// settings
		js["type"] = get_type();

		// reverse
		js["reverse"] = reverse_;

		// order
		js["n1"] = n1_;
		js["n2"] = n2_;

		// circle radius
		js["radius"] = radius_;

		// start angle
		js["phi"] = phi_;

		// arclength
		js["arclength"] = arclength_;

		// element size
		js["element_size"] = element_size_;

		// design variable
		js["ba"] = ba_;

		// cone angle
		js["beta"] = beta_;

		// extra twist
		js["zeta1"] = zeta1_;
		js["zeta2"] = zeta2_;

		// radius delta
		js["delta_radius"] = delta_radius_;

		// number of poles
		js["num_poles"] = static_cast<unsigned int>(num_poles_);
	}

	// method for deserialisation from json
	void PathSuperEllipse::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent nodes
		Transformations::deserialize(js,list,factory_list,pth);

		// settings
		set_reverse(js["reverse"].asBool());
		set_n1(js["n1"].ASFLTP());
		set_n2(js["n2"].ASFLTP());
		set_radius(js["radius"].ASFLTP());
		set_phi(js["phi"].ASFLTP());
		set_arclength(js["arclength"].ASFLTP());
		set_element_size(js["element_size"].ASFLTP());
		set_ba(js["ba"].ASFLTP());
		set_beta(js["beta"].ASFLTP());
		set_zeta1(js["zeta1"].ASFLTP());
		set_zeta2(js["zeta2"].ASFLTP());
		set_delta_radius(js["delta_radius"].ASFLTP());
		set_num_poles(js["num_poles"].asUInt64());
	}

}}