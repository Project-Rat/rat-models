// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathaxis.hh"

// rat-common
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathAxis::PathAxis(){
		set_name("Axis");
	}

	// constructor
	PathAxis::PathAxis(
		const char axis, 
		const char transverse_direction, 
		const fltp ell, 
		const arma::Col<fltp>::fixed<3> &position, 
		const fltp element_size){

		// set to self
		set_name("Axis" + std::string(1,axis));
		axis_ = axis; transverse_direction_ = transverse_direction; ell_ = ell; position_ = position; element_size_ = element_size;
	}

	// factory
	ShPathAxisPr PathAxis::create(){
		return std::make_shared<PathAxis>();
	}

	// factory
	ShPathAxisPr PathAxis::create(
		const char axis, 
		const char transverse_direction, 
		const fltp ell, 
		const arma::Col<fltp>::fixed<3> &position, 
		const fltp element_size){
		return std::make_shared<PathAxis>(axis,transverse_direction,ell,position,element_size);
	}

	// factory
	ShPathAxisPr PathAxis::create(
		const char axis, 
		const char transverse_direction, 
		const fltp ell, 
		const fltp x, const fltp y, const fltp z, 
		const fltp element_size){
		return std::make_shared<PathAxis>(axis,transverse_direction,ell,arma::Col<fltp>::fixed<3>{x,y,z},element_size);
	}

	// set the axis
	void PathAxis::set_axis(const char axis){
		axis_ = axis;
	}

	// set the normal transverse_directionection
	void PathAxis::set_transverse_direction(const char transverse_direction){
		transverse_direction_ = transverse_direction;
	}

	// set length
	void PathAxis::set_ell(const fltp ell){
		ell_ = ell;
	}

	// set the center position
	void PathAxis::set_position(const arma::Col<fltp>::fixed<3> &position){
		position_ = position;
	}

	// set th element size
	void PathAxis::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	// get axis
	char PathAxis::get_axis()const{
		return axis_;
	}

	// get normal direction
	char PathAxis::get_transverse_direction()const{
		return transverse_direction_;
	}

	// get length
	fltp PathAxis::get_ell()const{
		return ell_;
	}

	// get center position
	arma::Col<fltp>::fixed<3> PathAxis::get_position()const{
		return position_;
	}

	// get element size
	fltp PathAxis::get_element_size()const{
		return element_size_;
	}

	// get frame
	ShFramePr PathAxis::create_frame(const MeshSettings &stngs) const{
		// check validity
		is_valid(true);

		// axis of magnet for field quality calculation
		ShPathGroupPr pth = PathGroup::create();
		ShPathStraightPr str = PathStraight::create(ell_,element_size_);
		pth->add_path(str);
		pth->add_translation(0,-ell_/2,0);

		// set line start location, transverse_directionection vector and orientation (N-vector)
		// depending on the axis and transverse_directionection chosen (note that D-is pointing in transverse_direction)
		if(axis_=='x' && transverse_direction_=='y'){
			pth->add_rotation(0,0,1,-arma::Datum<fltp>::pi/2);
			pth->add_rotation(1,0,0,-arma::Datum<fltp>::pi/2);
		}
		else if(axis_=='x' && transverse_direction_=='z'){
			pth->add_rotation(0,0,1,-arma::Datum<fltp>::pi/2);
			
		}
		else if(axis_=='y' && transverse_direction_=='x'){
			pth->add_rotation(0,1,0,-arma::Datum<fltp>::pi/2);
		}
		else if(axis_=='y' && transverse_direction_=='z'){
			// default orientation
		}
		else if(axis_=='z' && transverse_direction_=='x'){
			pth->add_rotation(1,0,0,arma::Datum<fltp>::pi/2);
			pth->add_rotation(0,0,1,arma::Datum<fltp>::pi/2);
		}
		else if(axis_=='z' && transverse_direction_=='y'){
			pth->add_rotation(1,0,0,arma::Datum<fltp>::pi/2);
			pth->add_rotation(0,0,1,arma::Datum<fltp>::pi);
		}
		else{
			rat_throw_line("axis and transverse_direction combination unrecognised");
		}
		
		// add translation
		pth->add_translation(position_);

		// create frame
		const ShFramePr cable_gen = pth->create_frame(stngs);

		// apply transformations
		cable_gen->apply_transformations(get_transformations(), stngs.time);

		// return frame from this path
		return cable_gen;
	}

	// vallidity check
	bool PathAxis::is_valid(const bool enable_throws) const{
		if(!Transformations::is_valid(enable_throws))return false;
		if(ell_<=0){if(enable_throws){rat_throw_line("length must be larger than zero");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		if(!rat::cmn::Extra::is_xyz(axis_)){if(enable_throws){rat_throw_line("axis must be x,y or z");} return false;};
		if(!rat::cmn::Extra::is_xyz(transverse_direction_)){if(enable_throws){rat_throw_line("transverse direction must be x,y or z");} return false;};
		if(axis_==transverse_direction_){if(enable_throws){rat_throw_line("axis and transverse direction must be orthogonal");} return false;};
		return true;
	}

	// get type
	std::string PathAxis::get_type(){
		return "rat::mdl::pathaxis";
	}

	// method for serialization into json
	void PathAxis::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents first 
		// this allows the type to be overridden
		Path::serialize(js,list);
		Transformations::serialize(js,list);

		// serialization type
		js["type"] = get_type();

		// orientation
		js["axis"] = axis_;
		js["transverse_direction"] = transverse_direction_;
		
		// position
		js["position_x"] = position_(0);
		js["position_y"] = position_(1);
		js["position_z"] = position_(2);

		// length
		js["ell"] = ell_;

		// discretization
		js["dl"] = element_size_;
	}

	// method for deserialisation from json
	void PathAxis::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent objects
		Path::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);

		// orientation
		axis_ = js["axis"].asInt();
		transverse_direction_ = js["transverse_direction"].asInt();

		// position
		position_(0) = js["position_x"].ASFLTP();
		position_(1) = js["position_y"].ASFLTP();
		position_(2) = js["position_z"].ASFLTP();

		// length
		ell_ = js["ell"].ASFLTP();

		// discretization
		element_size_ = js["dl"].ASFLTP();
	}

}}

