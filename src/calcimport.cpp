// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calcimport.hh"

// mlfmm headers
#include "rat/mlfmm/mlfmm.hh"
#include "rat/mlfmm/multisources.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcImport::CalcImport(){

	}

	// constructor with filepath
	CalcImport::CalcImport(const boost::filesystem::path &fpath, const arma::Col<fltp>::fixed<3> &R0, const rat::fltp scaling_factor){
		set_fpath(fpath); set_position(R0); set_scaling_factor(scaling_factor);
	}

	// factory methods
	ShCalcImportPr CalcImport::create(){
		return std::make_shared<CalcImport>();
	}

	// create with filepath input
	ShCalcImportPr CalcImport::create(const boost::filesystem::path &fpath, const arma::Col<fltp>::fixed<3> &R0, const rat::fltp scaling_factor){
		return std::make_shared<CalcImport>(fpath,R0,scaling_factor);
	}

	// set input path
	void CalcImport::set_fpath(const boost::filesystem::path &fpath){
		fpath_ = fpath;
	}

	// get input path
	boost::filesystem::path CalcImport::get_fpath() const{
		return fpath_;
	}

	// set scaling factor
	void CalcImport::set_scaling_factor(const rat::fltp scaling_factor){
		scaling_factor_ = scaling_factor;
	}

	// set position
	void CalcImport::set_position(const arma::Col<fltp>::fixed<3> &R0){
		R0_ = R0;
	}

	// get scaling factor
	fltp CalcImport::get_scaling_factor()const{
		return scaling_factor_;
	}

	// get position
	arma::Col<fltp>::fixed<3> CalcImport::get_position() const{
		return R0_;
	}

	// calculate with data output
	ShImportDataPr CalcImport::calculate_import(
		const fltp time, 
		const cmn::ShLogPr &lg, 
		const ShSolverCachePr& cache){

		// when calculating the settings must be valid
		is_valid(true);

		// get date and time
		auto timdata = std::time(nullptr);
		auto localtime = *std::localtime(&timdata);
		std::ostringstream date_stream,time_stream;
		date_stream << std::put_time(&localtime, "%d-%m-%Y");
		time_stream << std::put_time(&localtime, "%H:%M:%S");
		const std::string date_str = date_stream.str();
		const std::string time_str = time_stream.str();

		// general info
		lg->msg("%s=== Starting Import Calculation ===%s\n",KGRN,KNRM);
		lg->msg(2,"%sGENERAL INFO%s\n",KGRN,KNRM);
		lg->msg("date: %s%s%s %s(dd-mm-yyyy)%s\n",KYEL,date_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("time: %s%s%s %s(hh:mm:ss)%s\n",KYEL,time_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("model name: %s%s%s\n",KYEL,model_->get_name().c_str(),KNRM);
		lg->msg("path name: %s%s%s\n",KYEL,get_name().c_str(),KNRM);
		lg->msg(-2,"\n");

		// header
		lg->msg(2,"%sSETTING UP MESHES%s\n",KGRN,KNRM);

		// create targets for calculation
		const ShImportDataPr import_data = ImportData::create(fpath_);
		import_data->set_translation(R0_);
		import_data->set_scaling_factor(scaling_factor_);

		// check input model
		if(model_==NULL)rat_throw_line("model not set");
		model_->is_valid(true);

		// settings for mesh creation
		MeshSettings mesh_settings;
		mesh_settings.time = time;
		mesh_settings.low_poly = false;
		mesh_settings.combine_sections = true;

		// create a list of meshes
		std::list<ShMeshDataPr> meshes = model_->create_meshes({},mesh_settings);

		// set circuit currents
		apply_circuit_currents(time, meshes);
		
		// remove calculation meshes
		for(auto it = meshes.begin();it!=meshes.end();it++)
			if((*it)->get_calc_mesh())it = meshes.erase(it);

		// set field type
		for(auto it=meshes.begin();it!=meshes.end();it++)(*it)->set_field_type("AHM",{3,3,3});

		// display function
		MeshData::display(lg,meshes);

		// end mesh setup
		lg->msg(-2,"\n");

		// run field calculation
		calculate_field(meshes,import_data,time,lg,cache);

		// // combine sources and targets
		// fmm::ShMultiSourcesPr src = fmm::MultiSources::create();

		// // set meshes as sources for MLFMM
		// for(auto it=meshes.begin();it!=meshes.end();it++)src->add_sources((*it)->create_sources());

		// // multipole method
		// fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(src, import_data, stngs_);

		// // set background field
		// if(bg_!=NULL)mlfmm->set_background(bg_->create_fmm_background(time));
		
		// // setup MLFMM and calculate required fields
		// mlfmm->setup(lg); mlfmm->calculate(lg);

		// return imported data object
		return {};
	}

	// generalized calculation
	std::list<ShDataPr> CalcImport::calculate(
		const fltp time, 
		const cmn::ShLogPr &lg, 
		const ShSolverCachePr& cache){
		return {calculate_import(time,lg,cache)};
	}

	// input validity
	bool CalcImport::is_valid(const bool enable_throws) const{
		if(!CalcLeaf::is_valid(enable_throws))return false;
		boost::system::error_code ec;
		if(!boost::filesystem::exists(fpath_,ec)){if(enable_throws){rat_throw_line("filepath does not exist");} return false;};
		return true;
	}

	// serialization
	std::string CalcImport::get_type(){
		return "rat::mdl::calcimport";
	}

	// method for serialization into json
	void CalcImport::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		CalcLeaf::serialize(js,list);

		// type
		js["type"] = get_type();
		js["fpath"] = fpath_.string();
		js["Rx"] = R0_(0); js["Ry"] = R0_(1); js["Rz"] = R0_(2);
		js["scaling_factor"] = scaling_factor_;
	}

	// method for deserialisation from json
	void CalcImport::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent objects
		CalcLeaf::deserialize(js,list,factory_list,pth);
		fpath_ = js["fpath"].asString();
		R0_(0) = js["Rx"].ASFLTP(); 
		R0_(1) = js["Ry"].ASFLTP(); 
		R0_(2) = js["Rz"].ASFLTP(); 
		scaling_factor_ = js["scaling_factor"].ASFLTP();
	}

}}