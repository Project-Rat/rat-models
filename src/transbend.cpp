// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "transbend.hh"

// rat-common headers
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	TransBend::TransBend(){
		set_name("Bend");
	}

	// constructor with vector input
	TransBend::TransBend(
		const arma::Col<fltp>::fixed<3> &axis, 
		const arma::Col<fltp>::fixed<3> &direction, 
		const fltp radius, const fltp offset) : TransBend(){
		set_bending(axis,direction,radius,offset);
	}

	// factory
	ShTransBendPr TransBend::create(){
		//return ShTransBendPr(new TransBend);
		return std::make_shared<TransBend>();
	}

	// factory with vector input
	ShTransBendPr TransBend::create(
		const arma::Col<fltp>::fixed<3> &axis, 
		const arma::Col<fltp>::fixed<3> &direction, 
		const fltp radius, const fltp offset){
		//return ShTransBendPr(new TransBend(V1,V2,r));
		return std::make_shared<TransBend>(axis,direction,radius,offset);
	}

	// set bending vectors and radius
	void TransBend::set_bending(
		const arma::Col<fltp>::fixed<3> &axis, 
		const arma::Col<fltp>::fixed<3> &direction, 
		const fltp radius, const fltp offset){

		// set vectors and radius
		set_axis(axis);
		set_direction(direction);
		set_radius(radius);
		set_offset(offset);

		// normalize vectors
		axis_.each_row() /= cmn::Extra::vec_norm(axis_);
		direction_.each_row() /= cmn::Extra::vec_norm(direction_);

		// check orthogonality of vectors
		assert(arma::as_scalar(cmn::Extra::dot(axis_,direction_))<1e-6);
	}

	// set axis and direction
	void TransBend::set_axis(const arma::Col<fltp>::fixed<3> &axis){
		axis_ = axis;
	}

	// set bending direction
	void TransBend::set_direction(const arma::Col<fltp>::fixed<3> &direction){
		direction_ = direction;
	}

	// set bending radius
	void TransBend::set_radius(const fltp radius){
		radius_ = radius;
	}

	// set bending radius
	void TransBend::set_offset(const fltp offset){
		offset_ = offset;
	}

	// get bending axis
	arma::Col<fltp>::fixed<3> TransBend::get_axis() const{
		return axis_;
	}

	// get bending direction
	arma::Col<fltp>::fixed<3> TransBend::get_direction() const{
		return direction_;
	}

	// get bending radius
	fltp TransBend::get_radius() const{
		return radius_;
	}

	// get bending radius
	fltp TransBend::get_offset() const{
		return offset_;
	}


	// apply bending to coordinates only
	void TransBend::apply_coords(arma::Mat<fltp> &R, const fltp /*time*/) const{
		// skip if radius is set to zero
		if(radius_==0)return;

		// normalize
		const arma::Col<fltp>::fixed<3> axis_norm = axis_.each_row()/cmn::Extra::vec_norm(axis_);
		const arma::Col<fltp>::fixed<3> direction_norm = direction_.each_row()/cmn::Extra::vec_norm(direction_);

		// calcultate third vector
		const arma::Col<fltp>::fixed<3> normal = -cmn::Extra::cross(axis_norm,direction_norm);

		// offset R
		R.each_col() -= offset_*direction_norm;

		// calculate distance along vectors
		arma::Mat<fltp> axis_ext(3,R.n_cols); axis_ext.each_col() = axis_norm; 
		arma::Row<fltp> u = cmn::Extra::dot(R,axis_ext);

		arma::Mat<fltp> direction_ext(3,R.n_cols); direction_ext.each_col() = direction_norm; 
		arma::Row<fltp> v = cmn::Extra::dot(R,direction_ext);

		arma::Mat<fltp> normal_ext(3,R.n_cols); normal_ext.each_col() = normal;
		arma::Row<fltp> w = cmn::Extra::dot(R,normal_ext);

		// calculate angle
		arma::Row<fltp> angle = arma::Datum<fltp>::pi/2 - w/radius_;

		// perform bending
		w = (radius_ + v)%arma::cos(angle);
		v = (radius_ + v)%arma::sin(angle) - radius_;

		// transform back to coordinates
		R = axis_ext.each_row()%u + direction_ext.each_row()%v + normal_ext.each_row()%w;

		// apply offset
		R.each_col() += offset_*direction_norm;
	}

	// apply transformation to coordinates and vectors
	void TransBend::apply_vectors(
		const arma::Mat<fltp> &R, 
		arma::Mat<fltp> &V, 
		const char /*type*/, 
		const fltp /*time*/) const{
		// skip if radius is set to zero
		if(radius_==0)return;

		// normalize
		const arma::Col<fltp>::fixed<3> axis_norm = axis_.each_row()/cmn::Extra::vec_norm(axis_);
		const arma::Col<fltp>::fixed<3> direction_norm = direction_.each_row()/cmn::Extra::vec_norm(direction_);

		// calcultate third vector
		const arma::Col<fltp>::fixed<3> normal = -cmn::Extra::cross(axis_norm,direction_norm);

		// offset R
		const arma::Mat<fltp> Roff = R.each_col() - offset_*direction_norm;

		// calculate distance along vectors
		arma::Mat<fltp> axis_ext(3,Roff.n_cols); axis_ext.each_col() = axis_norm;
		// arma::Row<fltp> u = cmn::Extra::dot(R,V1ext);

		arma::Mat<fltp> direction_ext(3,Roff.n_cols); direction_ext.each_col() = direction_norm; 
		const arma::Row<fltp> v = cmn::Extra::dot(Roff,direction_ext);

		arma::Mat<fltp> normal_ext(3,Roff.n_cols); normal_ext.each_col() = normal;
		const arma::Row<fltp> w = cmn::Extra::dot(Roff,normal_ext);

		// calculate angle
		const arma::Row<fltp> angle = arma::Datum<fltp>::pi/2 - w/radius_;

		// rotate angles
		for(arma::uword i=0;i<V.n_cols;i++){
			// get local angle
			const fltp alpha = angle(i) - arma::Datum<fltp>::pi/2;

			// setup local rotation matrix
			const arma::Mat<fltp>::fixed<3,3> M = 
				cmn::Extra::create_rotation_matrix(axis_norm,alpha);

			// rotate
			V.col(i) = M*V.col(i);
		}
	}

	// validity check
	bool TransBend::is_valid(const bool enable_throws) const{
		if(arma::as_scalar(cmn::Extra::vec_norm(axis_))<1e-9){if(enable_throws){rat_throw_line("axis vector has no length");} return false;};
		if(arma::as_scalar(cmn::Extra::vec_norm(direction_))<1e-9){if(enable_throws){rat_throw_line("direction vector has no length");} return false;};
		if(std::abs(arma::as_scalar(cmn::Extra::dot(axis_,direction_)))>1e-9){if(enable_throws){rat_throw_line("axis and direction must be orthogonal");} return false;};
		return true;
	}

	// get type
	std::string TransBend::get_type(){
		return "rat::mdl::transbend";
	}

	// method for serialization into json
	void TransBend::serialize(Json::Value &js, cmn::SList &list) const{
		Trans::serialize(js,list);
		js["type"] = get_type();
		for(arma::uword i=0;i<3;i++)js["vector1"].append(axis_(i));
		for(arma::uword i=0;i<3;i++)js["vector2"].append(direction_(i));
		js["radius"] = radius_;
		js["offset"] = offset_;
	}

	// method for deserialisation from json
	void TransBend::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		Trans::deserialize(js,list,factory_list,pth);
		arma::uword idx = 0;
		for(auto it=js["vector1"].begin();it!=js["vector1"].end();it++,idx++)
			axis_(idx) = (*it).ASFLTP();
		idx = 0;
		for(auto it=js["vector2"].begin();it!=js["vector2"].end();it++,idx++)
			direction_(idx) = (*it).ASFLTP();
		radius_ = js["radius"].ASFLTP();
		offset_ = js["offset"].ASFLTP();
	}

}}