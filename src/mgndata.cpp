// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "mgndata.hh"

// rat-common headers
#include "rat/common/extra.hh"
#include "rat/common/elements.hh"
#include "rat/common/gauss.hh"

// rat-mlfmm headers
#include "rat/mlfmm/currentmesh.hh"
#include "rat/mlfmm/currentsurface.hh"
#include "rat/mlfmm/interp.hh"
#include "rat/mlfmm/chargedpolyhedron.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// create fictitious(!) magnetic charges to represent the H field
	// magnetic charge is delta M over the face boundaries in the face normal direction
	// we know that M is constant inside the volume so we only need surface charges
	fmm::ShMgnChargeSurfacePr MgnData::create_surface_charges()const{
		// check if this is hex mesh
		if(!(n_.n_rows==8 || (n_.n_rows==4 && s_.n_rows==3)))
			rat_throw_line("must be tetrahedron or hexahedral mesh");

		// calculate nodal magnetisation
		const arma::Mat<fltp> Mn = calc_nodal_magnetization();

		// calculate face normals of each face element
		const arma::Mat<fltp> Ns = n_.n_rows==8 ? 
			-cmn::Quadrilateral::calc_face_normals(R_,s_) : 
			cmn::Triangle::calc_face_normals(R_,s_);

		// calculate magnetisation at elements
		arma::Mat<fltp> Me(3,s_.n_cols);

		// gauss points
		const arma::Col<fltp> cp = n_.n_rows==8 ? 
			cmn::Quadrilateral::create_gauss_points(1) : 
			cmn::Triangle::create_gauss_points(1);
		const arma::Col<fltp> Rq = cp.rows(0,1);

		// calculate
		for(arma::uword i=0;i<s_.n_cols;i++){
			Me.col(i) = n_.n_rows==8 ? 
				cmn::Quadrilateral::quad2cart(Mn.cols(s_.col(i)),Rq) : 
				cmn::Triangle::quad2cart(Mn.cols(s_.col(i)),Rq);
		}

		// dot product
		const arma::Row<fltp> sigma = cmn::Extra::dot(Me,Ns);

		const fmm::ShMgnChargeSurfacePr src = fmm::MgnChargeSurface::create();
		src->set_mesh(R_,s_,sigma);

		// return charges
		return src;
	}

	// create fictitious(!) magnetic charges to represent the H field
	// magnetic charge is delta M over the face boundaries in the face normal direction
	// we know that M is constant inside the volume so we only need surface charges
	fmm::ShMgnChargesPr MgnData::create_surface_point_charges()const{
		// check if this is hex mesh
		if(!(n_.n_rows==8 || (n_.n_rows==4 && s_.n_rows==3)))
			rat_throw_line("must be tetrahedron or hexahedral mesh");

		// calculate nodal magnetisation
		const arma::Mat<fltp> Mn = calc_nodal_magnetization();

		// calculate face normals of each face element
		const arma::Mat<fltp> Ns = n_.n_rows==8 ? 
			-cmn::Quadrilateral::calc_face_normals(R_,s_) : 
			cmn::Triangle::calc_face_normals(R_,s_);

		// gauss points
		const arma::Mat<fltp> cp = n_.n_rows==8 ? 
			cmn::Quadrilateral::create_gauss_points(num_gauss_surface_) : 
			cmn::Triangle::create_gauss_points(num_gauss_surface_);
		const arma::Mat<fltp> Rq = cp.rows(0,1);
		const arma::Row<fltp> wg = cp.row(2);

		// calculate derivative of shape function
		const arma::Mat<fltp> dN = n_.n_rows==8 ? 
			cmn::Quadrilateral::shape_function_derivative(Rq) : 
			cmn::Triangle::shape_function_derivative(Rq);

		// calculate magnetisation at elements
		arma::Mat<fltp> Rg(3,s_.n_cols*Rq.n_cols);
		arma::Row<fltp> sigma(s_.n_cols*Rq.n_cols);
		arma::Row<fltp> Ag(s_.n_cols*Rq.n_cols);

		// walk over surface elements
		for(arma::uword i=0;i<s_.n_cols;i++){
			// get surface element
			const arma::Mat<fltp> Rn = R_.cols(s_.col(i));

			// calculate determinant of jabian at gauss points
			const arma::Mat<fltp> J = n_.n_rows==8 ? 
				cmn::Quadrilateral::shape_function_jacobian(Rn,dN) : 
				cmn::Triangle::shape_function_jacobian(Rn,dN);
			const arma::Row<fltp> Jdet = n_.n_rows==8 ? 
				cmn::Quadrilateral::jacobian2determinant(J) : 
				cmn::Triangle::jacobian2determinant(J);
			assert(arma::all(Jdet>0));

			// magnetisation on gauss points
			const arma::Mat<fltp> Mg = n_.n_rows==8 ? 
				cmn::Quadrilateral::quad2cart(Mn.cols(s_.col(i)),Rq) : 
				cmn::Triangle::quad2cart(Mn.cols(s_.col(i)),Rq);

			// get coordinates and charge
			Rg.cols(i*Rq.n_cols,(i+1)*Rq.n_cols-1) = n_.n_rows==8 ? 
				cmn::Quadrilateral::quad2cart(R_.cols(s_.col(i)),Rq) : 
				cmn::Triangle::quad2cart(R_.cols(s_.col(i)),Rq);
			sigma.cols(i*Rq.n_cols,(i+1)*Rq.n_cols-1) = Ns.col(i).t()*Mg;
			Ag.cols(i*Rq.n_cols,(i+1)*Rq.n_cols-1) = wg%Jdet;
		}

		// softening factor
		const arma::Row<fltp> epss = softening_*std::sqrt(RAT_CONST(2.0))*arma::sqrt(Ag/arma::Datum<fltp>::pi);

		// create magnetic charges
		const fmm::ShMgnChargesPr src = fmm::MgnCharges::create(Rg,sigma%Ag,epss);

		// return charges
		return src;
	}

	// create volume charges
	fmm::ShMgnChargesPr MgnData::create_vol_charges(const bool drop_zero_charge)const{
		// check if this is hex mesh
		if(!(n_.n_rows==8 || n_.n_rows==4))
			rat_throw_line("must be tetrahedron or hexahedral mesh");

		// calculate nodal magnetisation
		const arma::Mat<fltp> Mn = calc_nodal_magnetization();

		// create gauss points
		const arma::Mat<fltp> gp = n_.n_rows==8 ? 
			cmn::Hexahedron::create_gauss_points(num_gauss_volume_) : 
			cmn::Tetrahedron::create_gauss_points(num_gauss_volume_);
		const arma::Mat<fltp> Rgh = gp.rows(0,2);
		const arma::Mat<fltp> wgh = gp.row(3);

		// calculate derivative of shape function
		const arma::Mat<fltp> dN = n_.n_rows==8 ? 
			cmn::Hexahedron::shape_function_derivative(Rgh) : 
			cmn::Tetrahedron::shape_function_derivative(Rgh);

		// make sources for multiple gauss points
		arma::Row<fltp> Vg(n_.n_cols*Rgh.n_cols);
		arma::Row<fltp> dMg(n_.n_cols*Rgh.n_cols);
		arma::Mat<fltp> Rg(3,n_.n_cols*Rgh.n_cols);
		// arma::Row<fltp> epss(n_.n_cols*Rgh.n_cols);

		// walk over elements
		for(arma::uword i=0;i<n_.n_cols;i++){
			// element nodes
			const arma::Mat<fltp> Rn = R_.cols(n_.col(i));

			// create gauss point coordinates
			Rg.cols(i*Rgh.n_cols,(i+1)*Rgh.n_cols-1) = n_.n_rows==8 ? 
				cmn::Hexahedron::quad2cart(Rn,Rgh) : 
				cmn::Tetrahedron::quad2cart(Rn,Rgh);

			// calculate determinant of jabian at gauss points
			const arma::Mat<fltp> J = n_.n_rows==8 ? 
				cmn::Hexahedron::shape_function_jacobian(Rn,dN) : 
				cmn::Tetrahedron::shape_function_jacobian(Rn,dN);
			const arma::Row<fltp> Jdet = n_.n_rows==8 ? 
				-cmn::Hexahedron::jacobian2determinant(J) : 
				cmn::Tetrahedron::jacobian2determinant(J);
			assert(arma::all(Jdet>0));

			// scale volume
			Vg.cols(i*Rgh.n_cols,(i+1)*Rgh.n_cols-1) = Jdet%wgh;

			// derivatives of magnetization
			const arma::Mat<fltp> dM = n_.n_rows==8 ? 
				cmn::Hexahedron::quad2cart_derivative(
					Rn,Rgh,Mn.cols(n_.col(i))) : 
				cmn::Tetrahedron::quad2cart_derivative(
					R_.cols(n_.col(i)),Rgh,Mn.cols(n_.col(i)));

			// calculate charge density (see Russenschuck page 164)
			dMg.cols(i*Rgh.n_cols,(i+1)*Rgh.n_cols-1) = 
				-(dM.row(0) + dM.row(4) + dM.row(8)); // -div M = -(dMxdx + dMydy + dMzdz)

			// softening factor
			// epss.cols(i*Rgh.n_cols,(i+1)*Rgh.n_cols-1).fill(
			// 	softening_*std::sqrt(RAT_CONST(2.0))*arma::max(
			// 	cmn::Extra::vec_norm(Rn.each_col() - 
			// 	arma::mean(Rn,1)))/num_gauss_volume_);
		}

		// calculate eps
		const arma::Row<fltp> epss = softening_*std::sqrt(2.0)*arma::cbrt(3*Vg/(4*arma::Datum<fltp>::pi));

		// check for sources with zero charge
		const arma::Col<arma::uword> id = drop_zero_charge ? 
			arma::find(arma::abs(dMg)>1e-12) : 
			arma::regspace<arma::Col<arma::uword> >(0,dMg.n_cols-1);

		// no sources
		if(id.is_empty())return NULL;

		// create magnetic charges
		const fmm::ShMgnChargesPr src = fmm::MgnCharges::create(
			Rg.cols(id),dMg.cols(id)%Vg.cols(id),epss.cols(id));

		// return sources
		return src;
	}

	// create fictitious(!) magnetic charges to represent the H field
	// magnetic charge is delta M over the face boundaries in the face normal direction
	// we know that M is constant inside the volume so we only need surface charges
	fmm::ShCurrentSourcesPr MgnData::create_bnd_point_surf_currents()const{
		// check if this is hex mesh
		if(!(n_.n_rows==8 || (n_.n_rows==4 && s_.n_rows==3)))
			rat_throw_line("must be tetrahedron or hexahedral mesh");

		// calculate nodal magnetisation
		const arma::Mat<fltp> Mn = calc_nodal_magnetization();

		// calculate face normals of each face element
		const arma::Mat<fltp> Ns = n_.n_rows==8 ? 
			-cmn::Quadrilateral::calc_face_normals(R_,s_) : 
			cmn::Triangle::calc_face_normals(R_,s_);

		// gauss points
		const arma::Mat<fltp> cp = n_.n_rows==8 ? 
			cmn::Quadrilateral::create_gauss_points(num_gauss_surface_) : 
			cmn::Triangle::create_gauss_points(num_gauss_surface_);
		const arma::Mat<fltp> Rq = cp.rows(0,1);
		const arma::Row<fltp> wg = cp.row(2);

		// calculate derivative of shape function
		const arma::Mat<fltp> dN = n_.n_rows==8 ? 
			cmn::Quadrilateral::shape_function_derivative(Rq) : 
			cmn::Triangle::shape_function_derivative(Rq);

		// calculate magnetisation at elements
		arma::Mat<fltp> Rg(3,s_.n_cols*Rq.n_cols);
		arma::Mat<fltp> Je(3,s_.n_cols*Rq.n_cols);
		arma::Row<fltp> Ag(s_.n_cols*Rq.n_cols);

		// walk over surface elements
		for(arma::uword i=0;i<s_.n_cols;i++){
			// get surface element
			const arma::Mat<fltp> Rn = R_.cols(s_.col(i));

			// calculate determinant of jabian at gauss points
			const arma::Mat<fltp> J = n_.n_rows==8 ? 
				cmn::Quadrilateral::shape_function_jacobian(Rn,dN) : 
				cmn::Triangle::shape_function_jacobian(Rn,dN);
			const arma::Row<fltp> Jdet = n_.n_rows==8 ? 
				cmn::Quadrilateral::jacobian2determinant(J) : 
				cmn::Triangle::jacobian2determinant(J);
			assert(arma::all(Jdet>0));

			// magnetization density on gauss points
			const arma::Mat<fltp> Mg = n_.n_rows==8 ? 
				cmn::Quadrilateral::quad2cart(Mn.cols(s_.col(i)),Rq) : 
				cmn::Triangle::quad2cart(Mn.cols(s_.col(i)),Rq);

			// get coordinates and charge
			Rg.cols(i*Rq.n_cols,(i+1)*Rq.n_cols-1) = n_.n_rows==8 ? 
				cmn::Quadrilateral::quad2cart(R_.cols(s_.col(i)),Rq) : 
				cmn::Triangle::quad2cart(R_.cols(s_.col(i)),Rq);
			Je.cols(i*Rq.n_cols,(i+1)*Rq.n_cols-1) = cmn::Extra::cross(Mg,arma::repmat(Ns.col(i),1,Mg.n_cols));
			Ag.cols(i*Rq.n_cols,(i+1)*Rq.n_cols-1) = wg%Jdet;
		}

		// softening factor
		const arma::Row<fltp> epss = softening_*std::sqrt(RAT_CONST(2.0))*arma::sqrt(Ag/arma::Datum<fltp>::pi);

		// create magnetic charges
		const fmm::ShCurrentSourcesPr src = fmm::CurrentSources::create(Rg,Je.each_row()%Ag,arma::Row<fltp>(Je.n_cols,arma::fill::ones),epss);

		// return charges
		return src;
	}
	

	// create surface currents
	fmm::ShSourcesPr MgnData::create_bnd_surf_currents()const{
		// check if this is hex mesh
		if(!(n_.n_rows==8 || n_.n_rows==4))
			rat_throw_line("must be tetrahedron or hexahedral mesh");

		// calculate nodal magnetisation
		const arma::Mat<fltp> Mn = calc_nodal_magnetization();

		// calculate magnetisation at elements
		arma::Mat<fltp> Me(3,s_.n_cols);
		// arma::Mat<fltp> Re(3,s_.n_cols);

		// calculate
		for(arma::uword i=0;i<s_.n_cols;i++){
			const arma::Col<fltp> cp = n_.n_rows==8 ? 
				cmn::Quadrilateral::create_gauss_points(1) : 
				cmn::Triangle::create_gauss_points(1);
			const arma::Col<fltp> Rq = cp.rows(0,1);
			Me.col(i) = n_.n_rows==8 ? 
				cmn::Quadrilateral::quad2cart(Mn.cols(s_.col(i)),Rq) : 
				cmn::Triangle::quad2cart(Mn.cols(s_.col(i)),Rq);
		}

		// create normal 
		const arma::Mat<fltp> Ne = n_.n_rows==8 ? 
			-cmn::Quadrilateral::calc_face_normals(R_,s_) : 
			cmn::Triangle::calc_face_normals(R_,s_);

		// calculate current
		const arma::Mat<fltp> Je = cmn::Extra::cross(Me,Ne);

		// check for sources with zero current
		const arma::Col<arma::uword> id = arma::find(cmn::Extra::vec_norm(Je)>1e-12);

		// no sources
		if(id.is_empty())return NULL;

		// create source object
		const fmm::ShCurrentSurfacePr src = fmm::CurrentSurface::create();
		src->set_mesh(R_,s_,Je);
		src->set_num_gauss(num_gauss_surface_);

		// return sources
		return src;
	}



	// create volume currents
	fmm::ShCurrentSourcesPr MgnData::create_bnd_vol_currents(const bool drop_zero_current)const{
		// check if this is hex mesh
		if(!(n_.n_rows==8 || n_.n_rows==4))
			rat_throw_line("must be tetrahedron or hexahedral mesh");

		// calculate nodal magnetisation
		const arma::Mat<fltp> Mn = calc_nodal_magnetization();

		// create gauss points
		const arma::Mat<fltp> gp = n_.n_rows==8 ? 
			cmn::Hexahedron::create_gauss_points(num_gauss_volume_) : 
			cmn::Tetrahedron::create_gauss_points(num_gauss_volume_);
		const arma::Mat<fltp> Rgh = gp.rows(0,2);
		const arma::Mat<fltp> wgh = gp.row(3);

		// calculate derivative of shape function
		const arma::Mat<fltp> dN = n_.n_rows==8 ? 
			cmn::Hexahedron::shape_function_derivative(Rgh) : 
			cmn::Tetrahedron::shape_function_derivative(Rgh);

		// make sources for multiple gauss points
		arma::Row<fltp> Vg(n_.n_cols*Rgh.n_cols);
		arma::Mat<fltp> Jg(3,n_.n_cols*Rgh.n_cols);
		arma::Mat<fltp> Rg(3,n_.n_cols*Rgh.n_cols);
		arma::Row<fltp> epss(n_.n_cols*Rgh.n_cols);;
		for(arma::uword i=0;i<n_.n_cols;i++){
			// get element nodes
			const arma::Mat<fltp> Rn = R_.cols(n_.col(i));

			// create gauss point coordinates
			Rg.cols(i*Rgh.n_cols,(i+1)*Rgh.n_cols-1) = n_.n_rows==8 ? 
				cmn::Hexahedron::quad2cart(Rn,Rgh) : 
				cmn::Tetrahedron::quad2cart(Rn,Rgh);

			// calculate determinant of jabian at gauss points
			const arma::Mat<fltp> J = n_.n_rows==8 ? 
				cmn::Hexahedron::shape_function_jacobian(R_.cols(n_.col(i)),dN) : 
				cmn::Tetrahedron::shape_function_jacobian(Rn,dN);
			const arma::Row<fltp> Jdet = n_.n_rows==8 ? 
				-cmn::Hexahedron::jacobian2determinant(J) : 
				cmn::Tetrahedron::jacobian2determinant(J); 
			assert(arma::all(Jdet>0));

			// scale area
			Vg.cols(i*Rgh.n_cols,(i+1)*Rgh.n_cols-1) = Jdet%wgh;

			// calculate current density
			Jg.cols(i*Rgh.n_cols,(i+1)*Rgh.n_cols-1) = n_.n_rows==8 ? 
				cmn::Hexahedron::quad2cart_curl(Rn,Rgh,Mn.cols(n_.col(i))) : 
				cmn::Tetrahedron::quad2cart_curl(Rn,Rgh,Mn.cols(n_.col(i)));

			// softening factor
			epss.cols(i*Rgh.n_cols,(i+1)*Rgh.n_cols-1).fill(
				softening_*std::sqrt(RAT_CONST(2.0))*
				arma::max(cmn::Extra::vec_norm(Rn.each_col() - 
					arma::mean(Rn,1)))/num_gauss_volume_);
		}

		// check for sources with zero current
		const arma::Col<arma::uword> id = drop_zero_current ? 
			arma::find(cmn::Extra::vec_norm(Jg)>1e-9) : 
			arma::regspace<arma::Col<arma::uword> >(0,Jg.n_cols-1);

		// no sources
		if(id.is_empty())return NULL;

		// check
		assert(Vg.is_finite());
		assert(epss.is_finite());

		// create currents
		const fmm::ShCurrentSourcesPr src = fmm::CurrentSources::create(
			Rg.cols(id),Jg.cols(id).eval().each_row()%Vg.cols(id),
			arma::Row<fltp>(id.n_elem,arma::fill::ones),epss.cols(id));
		src->set_van_Lanen(false);

		// return sources
		return src;
	}

	// create interpolation source for magnetization
	fmm::ShInterpPr MgnData::create_magnetization_interp()const{
		// calculate nodal magnetisation
		const arma::Mat<fltp> Mn = calc_nodal_magnetization();

		// interpolation of magnetization
		const fmm::ShInterpPr interp = fmm::Interp::create();
		interp->set_mesh(R_,n_);
		interp->set_interpolation_values('M',Mn);
		return interp;
	}

	// set magnetization to self
	void MgnData::set_self_magnetisation(){
		// check if this mesh has magnetization
		if(!has('M'))rat_throw_line("mesh has no magnetization");
		if(Rt_.empty())rat_throw_line("target points not set");

		// magnetization at nodes
		const arma::Mat<fltp> Mn = calc_nodal_magnetization();

		// create gauss points for volume
		const arma::Mat<fltp> gp = n_.n_rows==8 ? 
			cmn::Hexahedron::create_gauss_points(num_gauss_volume_) : 
			cmn::Tetrahedron::create_gauss_points(num_gauss_volume_);
		const arma::Mat<fltp> Rqg = gp.rows(0,2);

		// magnetisation at gauss points
		arma::Mat<fltp> Me(3,n_.n_cols*Rqg.n_cols);
		for(arma::uword i=0;i<n_.n_cols;i++)
			Me.cols(i*Rqg.n_cols,(i+1)*Rqg.n_cols-1) = n_.n_rows==8 ? 
			cmn::Hexahedron::quad2cart(Mn.cols(n_.col(i)),Rqg) : 
			cmn::Tetrahedron::quad2cart(Mn.cols(n_.col(i)),Rqg);

		// create gauss points for surface
		const arma::Mat<fltp> gps = s_.n_rows==4 ? 
			cmn::Quadrilateral::create_gauss_points(num_gauss_surface_) : 
			cmn::Triangle::create_gauss_points(num_gauss_surface_);
		const arma::Mat<fltp> Rqgs = gps.rows(0,1);

		arma::Mat<fltp> Ms(3,s_.n_cols*Rqgs.n_cols);
		for(arma::uword i=0;i<s_.n_cols;i++)
			Ms.cols(i*Rqgs.n_cols,(i+1)*Rqgs.n_cols-1) = s_.n_rows==4 ? 
			cmn::Quadrilateral::quad2cart(Mn.cols(s_.col(i)),Rqgs) : 
			cmn::Triangle::quad2cart(Mn.cols(s_.col(i)),Rqgs);

		// set to self
		add_field('M',arma::join_horiz(Me,Ms));
	}

	// calculate integrated Lorentz forces
	arma::Col<fltp>::fixed<3> MgnData::calc_lorentz_force() const{
		// create charges on both surface and volume
		const fmm::ShMgnChargesPr volume_charges = create_vol_charges(false);
		const fmm::ShMgnChargesPr surface_charges = create_surface_point_charges();

		// calculate forces on volume charges and surface charges
		arma::Col<fltp>::fixed<3> Fv(arma::fill::zeros), Fs(arma::fill::zeros);
		if(volume_charges!=NULL)Fv= arma::Datum<fltp>::mu_0*arma::sum(get_field_gp('H').each_row()%volume_charges->get_charge(),1);
		if(surface_charges!=NULL)Fs = arma::Datum<fltp>::mu_0*arma::sum(get_field_gps('H').each_row()%surface_charges->get_charge(),1);

		// // create charges on both surface and volume
		// const fmm::ShCurrentSourcesPr volume_currents = create_bnd_vol_currents(false);
		// const fmm::ShCurrentSourcesPr surface_currents = create_bnd_point_surf_currents();

		// // calculate forces on volume charges and surface currents
		// arma::Col<fltp>::fixed<3> Fv(arma::fill::zeros), Fs(arma::fill::zeros);
		// if(volume_currents!=NULL)Fv = arma::sum(cmn::Extra::cross(volume_currents->get_source_direction().each_row()%volume_currents->get_source_currents(), get_field_gp('B')),1);
		// if(surface_currents!=NULL)Fs = arma::sum(cmn::Extra::cross(surface_currents->get_source_direction().each_row()%surface_currents->get_source_currents(), get_field_gps('B')),1);

		// combine and return
		return Fv + Fs;
	}

	// setup sources and targets
	void MgnData::setup_targets(){
		if(!(n_.n_rows==8 || (n_.n_rows==4 && s_.n_rows==3)))
			rat_throw_line("must be tetrahedron or hexahedral mesh");

		// create gauss points for volume
		const arma::Mat<fltp> gp = n_.n_rows==8 ? 
			cmn::Hexahedron::create_gauss_points(num_gauss_volume_) : 
			cmn::Tetrahedron::create_gauss_points(num_gauss_volume_);
		const arma::Mat<fltp> Rqg = gp.rows(0,2);

		// convert to carthesian coords
		arma::Mat<fltp> Rg(3,n_.n_cols*Rqg.n_cols);
		for(arma::uword i=0;i<n_.n_cols;i++)
			Rg.cols(i*Rqg.n_cols,(i+1)*Rqg.n_cols-1) = n_.n_rows==8 ? 
				cmn::Hexahedron::quad2cart(R_.cols(n_.col(i)),Rqg) : 
				cmn::Tetrahedron::quad2cart(R_.cols(n_.col(i)),Rqg);
		
		// create gauss points for surface
		const arma::Mat<fltp> gps = s_.n_rows==4 ? 
			cmn::Quadrilateral::create_gauss_points(num_gauss_surface_) : 
			cmn::Triangle::create_gauss_points(num_gauss_surface_);
		const arma::Mat<fltp> Rqgs = gps.rows(0,1);

		// gauss points at surface of mesh
		arma::Mat<fltp> Rgs(3,s_.n_cols*Rqgs.n_cols);
		for(arma::uword i=0;i<s_.n_cols;i++)
			Rgs.cols(i*Rqgs.n_cols,(i+1)*Rqgs.n_cols-1) = s_.n_rows==4 ? 
				cmn::Quadrilateral::quad2cart(R_.cols(s_.col(i)),Rqgs) : 
				cmn::Triangle::quad2cart(R_.cols(s_.col(i)),Rqgs);

		// join
		Rt_ = arma::join_horiz(Rg, Rgs);
		num_targets_ = Rt_.n_cols;
	}

	// get field calculated at elements
	arma::Mat<fltp> MgnData::get_field_gp(const char field_type)const{
		const arma::uword ngpve = get_num_gauss_per_volume_element();
		const arma::Mat<fltp> fld = MeshData::get_field(field_type);
		return fld.cols(0,n_.n_cols*ngpve-1);
	}

	// get field on surface
	arma::Mat<fltp> MgnData::get_field_gps(const char field_type)const{
		const arma::Mat<fltp> fld = MeshData::get_field(field_type);
		const arma::uword ngpve = get_num_gauss_per_volume_element();
		const arma::uword ngpse = get_num_gauss_per_surface_element();
		const arma::Mat<fltp> flds = fld.cols(
			n_.n_cols*ngpve, n_.n_cols*ngpve+s_.n_cols*ngpse-1);
		assert(flds.n_cols==s_.n_cols*ngpse);
		return flds;
	}

	// override get field function to always return the field at the nodes
	arma::Mat<fltp> MgnData::get_field(const char field_type)const{
		// get field from targets
		const arma::Mat<fltp> fld = get_field_gp(field_type);

		// create gauss points for volume
		const arma::Mat<fltp> gp = n_.n_rows==8 ? 
			cmn::Hexahedron::create_gauss_points(num_gauss_volume_) : 
			cmn::Tetrahedron::create_gauss_points(num_gauss_volume_);
		const arma::Mat<fltp> Rqg = gp.rows(0,2);
		const arma::Row<fltp> wg = gp.row(3);

		// extrapolate values from gauss points for entire mesh
		const arma::Mat<fltp> fldn = n_.n_rows==8 ? 
			cmn::Hexahedron::gauss2nodes(R_,n_,fld,Rqg,wg,R_.n_cols) :
			cmn::Tetrahedron::gauss2nodes(R_,n_,fld,Rqg,wg,R_.n_cols);

		// return the field at the nodes
		return fldn;
	}

}}