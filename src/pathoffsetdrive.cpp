// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathoffsetdrive.hh"

// common headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// model headers
#include "pathbezier.hh"


// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathOffsetDrive::PathOffsetDrive(){
		set_name("Local Offset");
	}

	// constructor with specification
	PathOffsetDrive::PathOffsetDrive(
		const ShPathPr &base,
		const ShDrivePr& noff,
		const ShDrivePr& doff,
		const ShDrivePr& boff) : PathOffsetDrive(){

		// set to self
		set_input_path(base); set_noff(noff);
		set_doff(doff); set_boff(boff);
	}

	// factory
	ShPathOffsetDrivePr PathOffsetDrive::create(){
		return std::make_shared<PathOffsetDrive>();
	}

	// constructor with dimensions and element size
	ShPathOffsetDrivePr PathOffsetDrive::create(
		const ShPathPr &base, 
		const ShDrivePr& noff, 
		const ShDrivePr& doff, 
		const ShDrivePr& boff){
		return std::make_shared<PathOffsetDrive>(base,noff,doff,boff);
	}


	// settters
	void PathOffsetDrive::set_reorient(const bool reorient){
		reorient_ = reorient;
	}

	void PathOffsetDrive::set_noff(const ShDrivePr& noff){
		noff_ = noff;
	}

	void PathOffsetDrive::set_doff(const ShDrivePr& doff){
		doff_ = doff;
	}

	void PathOffsetDrive::set_boff(const ShDrivePr& boff){
		boff_ = boff;
	}

	void PathOffsetDrive::set_rotate(const ShDrivePr& rotate){
		rotate_ = rotate;
	}

	void PathOffsetDrive::set_nscale(const ShDrivePr& nscale){
		nscale_ = nscale;
	}

	void PathOffsetDrive::set_dscale(const ShDrivePr& dscale){
		dscale_ = dscale;
	}

	void PathOffsetDrive::set_normalize_length(const bool normalize_length){
		normalize_length_ = normalize_length;
	}


	// getters
	bool PathOffsetDrive::get_reorient()const{
		return reorient_;
	}
	
	const ShDrivePr& PathOffsetDrive::get_noff()const{
		return noff_;
	}

	const ShDrivePr& PathOffsetDrive::get_doff()const{
		return doff_;
	}

	const ShDrivePr& PathOffsetDrive::get_boff()const{
		return boff_;
	}

	const ShDrivePr& PathOffsetDrive::get_nscale()const{
		return nscale_;
	}

	const ShDrivePr& PathOffsetDrive::get_dscale()const{
		return dscale_;
	}

	bool PathOffsetDrive::get_normalize_length()const{
		return normalize_length_;
	}

	const ShDrivePr& PathOffsetDrive::get_rotate()const{
		return rotate_;
	}

	// get frame
	ShFramePr PathOffsetDrive::create_frame(const MeshSettings &stngs) const{
		// check if base path was set
		is_valid(true);

		// create frame
		const ShFramePr frame = input_path_->create_frame(stngs);

		// copy coordinates
		arma::field<arma::Mat<fltp> > R = frame->get_coords();
		arma::field<arma::Mat<fltp> > L = frame->get_direction();
		arma::field<arma::Mat<fltp> > D = frame->get_transverse();
		arma::field<arma::Mat<fltp> > N = frame->get_normal();
		arma::field<arma::Mat<fltp> > B = frame->get_block();
		arma::Row<arma::uword> section = frame->get_section();
		arma::Row<arma::uword> turn = frame->get_turn();
		arma::uword num_section_base = frame->get_num_section_base();

		// calculate axial coordinate
		arma::field<arma::Row<fltp> > ell(R.n_elem);

		// walk over sections
		fltp ell_shift = RAT_CONST(0.0);
		for(arma::uword i=0;i<R.n_cols;i++){
			// calculate ell for this section and shift it
			ell(i) = arma::join_horiz(arma::Row<fltp>{RAT_CONST(0.0)}, 
				arma::cumsum(cmn::Extra::vec_norm(arma::diff(R(i),1,1)))) + ell_shift;

			// update shifting
			ell_shift = ell(i).back();
		}

		// normalize length
		if(normalize_length_){
			for(arma::uword i=0;i<R.n_cols;i++)
				ell(i) = ell(i)/ell_shift - RAT_CONST(0.5);
		}


		// walk over sections
		for(arma::uword i=0;i<R.n_cols;i++){
			// get shear angle
			const arma::Row<fltp> ashear = frame->calc_ashear(i);

			// apply offset
			if(frame->get_conductor_type()==Frame::DbType::block){
				R(i) += D(i).each_row()%doff_->get_scaling_vec(ell(i),stngs.time) + 
					B(i).each_row()%((boff_->get_scaling_vec(ell(i),stngs.time) + 
						noff_->get_scaling_vec(ell(i),stngs.time))/arma::sin(ashear));
			}else{
				R(i) += N(i).each_row()%noff_->get_scaling_vec(ell(i),stngs.time) + 
					D(i).each_row()%doff_->get_scaling_vec(ell(i),stngs.time) + 
					B(i).each_row()%(boff_->get_scaling_vec(ell(i),stngs.time)/arma::sin(ashear));
			}

			// rotate around N to accomodate changes in transverse
			const arma::Row<fltp> alpha1 = arma::atan(doff_->get_scaling_vec(ell(i),stngs.time,1));
			for(arma::uword j=0;j<R(i).n_cols;j++){
				const arma::Mat<fltp>::fixed<3,3> M = cmn::Extra::create_rotation_matrix(N(i).col(j),alpha1(j));
				D(i).col(j) = M*D(i).col(j); L(i).col(j) = M*L(i).col(j);
			}

			// rotate around ell (i.e. apply twist)
			const arma::Row<fltp> alpha2 = rotate_->get_scaling_vec(ell(i),stngs.time);
			for(arma::uword j=0;j<R(i).n_cols;j++){
				const arma::Mat<fltp>::fixed<3,3> M = cmn::Extra::create_rotation_matrix(L(i).col(j),alpha2(j));
				D(i).col(j) = M*D(i).col(j); N(i).col(j) = M*N(i).col(j); B(i).col(j) = M*B(i).col(j);
			}

			// scale vectors
			N(i).each_row()%=nscale_->get_scaling_vec(ell(i),stngs.time);
			D(i).each_row()%=dscale_->get_scaling_vec(ell(i),stngs.time);
			B(i).each_row()%=nscale_->get_scaling_vec(ell(i),stngs.time);

			// number of nodes in this section
			// const arma::uword Nsn = frame->get_num_nodes(ibase);
			if(reorient_){
				// derivative
				L(i) = cmn::Extra::normalize(PathBezier::diff(R(i),2,2)(1));

				// update normal vector
				N(i) = cmn::Extra::normalize(cmn::Extra::cross(L(i),D(i)));
			}

			// after off-setting the darboux ribbon is not even fixed width anymore
			// this cheat fixes the width at least
			D(i) = cmn::Extra::normalize(D(i)).each_row()/
				arma::cos(cmn::Extra::vec_angle(cmn::Extra::cross(D(i),N(i)),L(i)));

			// check handedness
			assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N(i),L(i)),D(i))>RAT_CONST(0.0)));
		}

		// create offset frame
		const ShFramePr offset_frame = Frame::create(R,L,N,D,B);

		// conserve location
		offset_frame->set_location(
			frame->get_section(), 
			frame->get_turn(), 
			frame->get_num_section_base());

		// conserve location
		offset_frame->set_location(
			section, turn, num_section_base);

		// apply_transformations
		offset_frame->apply_transformations(get_transformations(), stngs.time);

		// create new frame
		return offset_frame;
	}

	// re-index nodes after deleting
	void PathOffsetDrive::reindex(){
		InputPath::reindex(); Transformations::reindex();
	}

	// vallidity check
	bool PathOffsetDrive::is_valid(const bool enable_throws) const{
		if(!InputPath::is_valid(enable_throws))return false;
		if(!Transformations::is_valid(enable_throws))return false;
		if(!noff_->is_valid(enable_throws))return false;
		if(!doff_->is_valid(enable_throws))return false;
		if(!boff_->is_valid(enable_throws))return false;
		if(!nscale_->is_valid(enable_throws))return false;
		if(!dscale_->is_valid(enable_throws))return false;
		return true;
	}

	// get type
	std::string PathOffsetDrive::get_type(){
		return "rat::mdl::pathoffsetdrive";
	}

	// method for serialization into json
	void PathOffsetDrive::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents
		Path::serialize(js,list);
		InputPath::serialize(js,list);
		Transformations::serialize(js,list);
		
		// type
		js["type"] = get_type();

		// properties
		js["normalize_length"] = normalize_length_;
		js["reorient"] = reorient_;
		js["noff"]  = cmn::Node::serialize_node(noff_, list);
		js["doff"]  = cmn::Node::serialize_node(doff_, list);
		js["boff"]  = cmn::Node::serialize_node(boff_, list);
		js["nscale"] = cmn::Node::serialize_node(nscale_, list);
		js["dscale"] = cmn::Node::serialize_node(dscale_, list);
		js["rotate"] = cmn::Node::serialize_node(rotate_, list);
	}

	// method for deserialisation from json
	void PathOffsetDrive::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// deserialize parents
		Path::deserialize(js,list,factory_list,pth);
		InputPath::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);

		// properties
		set_reorient(js["reorient"].asBool());
		set_normalize_length(js["normalize_length"].asBool());
		set_noff(cmn::Node::deserialize_node<Drive>(js["noff"], list, factory_list, pth)); 
		set_doff(cmn::Node::deserialize_node<Drive>(js["doff"], list, factory_list, pth)); 
		set_boff(cmn::Node::deserialize_node<Drive>(js["boff"], list, factory_list, pth));
		if(js.isMember("nscale"))set_nscale(cmn::Node::deserialize_node<Drive>(js["nscale"], list, factory_list, pth));
		if(js.isMember("dscale"))set_dscale(cmn::Node::deserialize_node<Drive>(js["dscale"], list, factory_list, pth));
		if(js.isMember("rotate"))set_rotate(cmn::Node::deserialize_node<Drive>(js["rotate"], list, factory_list, pth));
	}


}}