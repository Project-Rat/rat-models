// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelpoint.hh"

#include "rat/common/error.hh"

#include "crosspoint.hh"
#include "pathpoint.hh"
#include "pathgroup.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	ModelPoint::ModelPoint(){
		set_name("Point");
	}

	// default constructor
	ModelPoint::ModelPoint(
		const arma::Col<fltp>::fixed<3> &R) : ModelPoint(){
		set_coordinate(R);
	}

	// factory
	ShModelPointPr ModelPoint::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<ModelPoint>();
	}

	// factory
	ShModelPointPr ModelPoint::create(
		const arma::Col<fltp>::fixed<3> &R){
		return std::make_shared<ModelPoint>(R);
	}

	// get base
	ShPathPr ModelPoint::get_input_path() const{
		// check input
		is_valid(true);

		// create a pathgroup
		ShPathGroupPr grp = PathGroup::create(R_, L_, D_);
		grp->add_path(PathPoint::create());

		// return the circle
		return grp;
	}

	// get cross
	ShCrossPr ModelPoint::get_input_cross() const{
		// check input
		is_valid(true);

		// create circular path
		ShCrossPointPr crss = CrossPoint::create(0,0,1e-6,1e-6);

		// return the rectangle
		return crss;
	}

	// set properties
	void ModelPoint::set_coordinate(const arma::Col<fltp>::fixed<3> &R){
		R_ = R;
	}


	// get properties
	arma::Col<fltp>::fixed<3> ModelPoint::get_coordinate() const{
		return R_;
	}

	// check input
	bool ModelPoint::is_valid(const bool enable_throws) const{
		if(!ModelMeshWrapper::is_valid(enable_throws))return false;
		return true;
	}

	// get type
	std::string ModelPoint::get_type(){
		return "rat::mdl::modelpoint";
	}

	// method for serialization into json
	void ModelPoint::serialize(
		Json::Value &js, cmn::SList &list) const{

		// parent
		ModelMeshWrapper::serialize(js,list);

		// set type
		js["type"] = get_type();

		// start point
		js["Rx"] = R_(0); js["Ry"] = R_(1); js["Rz"] = R_(2);
		js["Lx"] = L_(0); js["Ly"] = L_(1); js["Lz"] = L_(2);
		js["Dx"] = D_(0); js["Dy"] = D_(1); js["Dz"] = D_(2);
	}

	// method for deserialisation from json
	void ModelPoint::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		ModelMeshWrapper::deserialize(js,list,factory_list,pth);

		// coordinate and orientation
		R_(0) = js["Rx"].ASFLTP(); R_(1) = js["Ry"].ASFLTP(); R_(2) = js["Rz"].ASFLTP();
		L_(0) = js["Lx"].ASFLTP(); L_(1) = js["Ly"].ASFLTP(); L_(2) = js["Lz"].ASFLTP();
		D_(0) = js["Dx"].ASFLTP(); D_(1) = js["Dy"].ASFLTP(); D_(2) = js["Dz"].ASFLTP();
	}

}}