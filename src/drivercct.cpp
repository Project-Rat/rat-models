// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "drivercct.hh"

#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	DriveRCCT::DriveRCCT(){
		set_name("RCCT");
	}

	// constructor
	DriveRCCT::DriveRCCT(
		const arma::uword num_poles,
		const fltp radius,
		const fltp alpha,
		const bool is_skew,
		const fltp b) : DriveRCCT(){
		set_num_poles(num_poles); set_radius(radius);
		set_alpha(alpha); set_is_skew(is_skew); set_b(b);
	}

	// factory
	ShDriveRCCTPr DriveRCCT::create(){
		return std::make_shared<DriveRCCT>();
	}

	// factory
	ShDriveRCCTPr DriveRCCT::create(
		const arma::uword num_poles,
		const fltp radius,
		const fltp alpha,
		const bool is_skew,
		const fltp b){
		return std::make_shared<DriveRCCT>(num_poles, radius, alpha, is_skew, b);
	}


	// setters
	void DriveRCCT::set_radius(const fltp radius){
		radius_ = radius;
	}

	void DriveRCCT::set_alpha(const fltp alpha){
		alpha_ = alpha;
	}

	void DriveRCCT::set_num_poles(const arma::uword num_poles){
		num_poles_ = num_poles;
	}

	void DriveRCCT::set_is_skew(const bool is_skew){
		is_skew_ = is_skew;
	}
	
	void DriveRCCT::set_fstr(const fltp fstr){
		fstr_ = fstr;
	}
	

	// getters
	fltp DriveRCCT::get_radius()const{
		return radius_;
	}

	fltp DriveRCCT::get_alpha()const{
		return alpha_;
	}

	bool DriveRCCT::get_is_skew()const{
		return is_skew_;
	}

	arma::uword DriveRCCT::get_num_poles()const{
		return num_poles_;
	}

	void DriveRCCT::set_b(const fltp b){
		b_ = b;
	}
	
	fltp DriveRCCT::get_b()const{
		return b_;
	}

	fltp DriveRCCT::get_fstr()const{
		return fstr_;
	}


	// get scaling array fun
	arma::field<arma::Row<fltp> > DriveRCCT::calc_radius_cyl(
		const ZFunType &zfun, 
		const arma::Row<fltp>& theta_mod, 
		const fltp alpha, 
		const fltp radius){

		// number of points
		const arma::uword num_points = theta_mod.n_elem;

		// get arrays
		const arma::field<arma::Row<fltp> > C = zfun(theta_mod);
		const arma::Row<fltp>& zz = C(0); const arma::Row<fltp>& vz = C(1); 
		const arma::Row<fltp>& az = C(2); const arma::Row<fltp>& jz = C(3); 

		// the lines are given as Rp + t*dRp
		const arma::Mat<fltp> Rp = arma::join_vert(
			arma::Row<fltp>(num_points,arma::fill::zeros),
			arma::Row<fltp>(num_points,arma::fill::zeros),zz);
		
		const arma::Mat<fltp> vRp = arma::join_vert(
			arma::Row<fltp>(num_points,arma::fill::zeros),
			arma::Row<fltp>(num_points,arma::fill::zeros),vz);
		const arma::Mat<fltp> aRp = arma::join_vert(
			arma::Row<fltp>(num_points,arma::fill::zeros),
			arma::Row<fltp>(num_points,arma::fill::zeros),az);
		const arma::Mat<fltp> jRp = arma::join_vert(
			arma::Row<fltp>(num_points,arma::fill::zeros),
			arma::Row<fltp>(num_points,arma::fill::zeros),jz);

		// direction just pointing radially in xy-plane
		const arma::Mat<fltp> dRp = arma::join_vert(
			arma::cos(theta_mod), arma::sin(theta_mod), 
			arma::Row<fltp>(num_points,arma::fill::zeros));
		const arma::Mat<fltp> vdRp = arma::join_vert(
			-arma::sin(theta_mod), arma::cos(theta_mod),
			arma::Row<fltp>(num_points,arma::fill::zeros));
		const arma::Mat<fltp> adRp = arma::join_vert(
			-arma::cos(theta_mod), -arma::sin(theta_mod),
			arma::Row<fltp>(num_points,arma::fill::zeros));
		const arma::Mat<fltp> jdRp = arma::join_vert(
			arma::sin(theta_mod), -arma::cos(theta_mod),
			arma::Row<fltp>(num_points,arma::fill::zeros));

		// CYLINDER FITTING
		// cylinder angle is alpha create rotation matrix for this coordinate system
		const arma::Col<fltp>::fixed<3> V1{1.0, 0.0, 0.0};
		const arma::Col<fltp>::fixed<3> V2{0.0, std::sin(alpha), -std::cos(alpha)};
		const arma::Col<fltp>::fixed<3> V3{0.0, 0.0, 0.0};
		const arma::Mat<fltp>::fixed<3,3> M = arma::join_horiz(V1,V2,V3).t();

		// convert to circle coordinates
		const arma::Mat<fltp> Rpc = M*Rp;
		const arma::Mat<fltp> dRpc = M*dRp;
		
		const arma::Mat<fltp> vRpc = M*vRp;
		const arma::Mat<fltp> vdRpc = M*vdRp;
		
		const arma::Mat<fltp> aRpc = M*aRp;
		const arma::Mat<fltp> adRpc = M*adRp;
		
		const arma::Mat<fltp> jRpc = M*jRp;
		const arma::Mat<fltp> jdRpc = M*jdRp;

		// x^2 + y^2 = radius^2
		// (Rpc(0) + dRpc(0)*t)^2 + (Rpc(1) + dRpc(1)*t)^2 = radius_^2;
		// Rpc(0)^2 + dRpc(0)^2*t^2 + 2*Rpc(0)*dRpc(0)*t+ Rpc(1)^2 + dRpc(1)^2*t^2 + 2*Rpc(1)*dRpc(1)*t - radius_^2 = 0
		// Rpc(0)^2 + Rpc(1)^2 + (dRpc(0)^2 + dRpc(1)^2)*t^2 + 2*(Rpc(0)*dRpc(0) + Rpc(1)*dRpc(1))*t - radius_^2 = 0
		// dot(Rpc,Rpc) + dot(dRpc,dRpc)*t^2 + 2*dot(Rpc,dRpc)*t - radius_^2 = 0

		// abc formula using c2*t^2 + 2*c1*t + c0 = 0
		const arma::Row<fltp> c0 = 
			cmn::Extra::dot(Rpc,Rpc) - radius*radius;
		const arma::Row<fltp> c1 = 
			cmn::Extra::dot(Rpc,dRpc);
		const arma::Row<fltp> c2 = 
			cmn::Extra::dot(dRpc,dRpc);
		
		// and its derrivatives
		const arma::Row<fltp> vc0 = 
			2*cmn::Extra::dot(vRpc,Rpc);
		const arma::Row<fltp> vc1 = 
			cmn::Extra::dot(vRpc,dRpc) + 
			cmn::Extra::dot(Rpc,vdRpc);
		const arma::Row<fltp> vc2 = 
			2*cmn::Extra::dot(vdRpc,dRpc);
		
		const arma::Row<fltp> ac0 = 
			2*cmn::Extra::dot(aRpc,Rpc) + 
			2*cmn::Extra::dot(vRpc,vRpc);
		const arma::Row<fltp> ac1 = 
			cmn::Extra::dot(aRpc,dRpc) + 
			2*cmn::Extra::dot(vRpc,vdRpc) + 
			cmn::Extra::dot(Rpc,adRpc);
		const arma::Row<fltp> ac2 = 
			2*cmn::Extra::dot(adRpc,dRpc) + 
			2*cmn::Extra::dot(vdRpc,vdRpc);
		
		const arma::Row<fltp> jc0 = 
			2*cmn::Extra::dot(jRpc,Rpc) + 
			2*cmn::Extra::dot(aRpc,vRpc) +
			4*cmn::Extra::dot(aRpc,vRpc);
		const arma::Row<fltp> jc1 = 
			cmn::Extra::dot(jRpc,dRpc) + 
			cmn::Extra::dot(aRpc,vdRpc) +
			2*cmn::Extra::dot(aRpc,vdRpc) + 
			2*cmn::Extra::dot(vRpc,adRpc) +
			cmn::Extra::dot(vRpc,adRpc) +
			cmn::Extra::dot(Rpc,jdRpc);
		const arma::Row<fltp> jc2 = 
			2*cmn::Extra::dot(jdRpc,dRpc) +
			2*cmn::Extra::dot(adRpc,vdRpc) + 
			2*cmn::Extra::dot(adRpc,vdRpc) +
			2*cmn::Extra::dot(vdRpc,adRpc);
			
		// find roots using ABC formula there is two possible solutions
		const arma::Row<fltp> delta = c1%c1 - c0%c2;
		const arma::Row<fltp> vdelta = 2*c1%vc1-c0%vc2-vc0%c2;
		const arma::Row<fltp> adelta = 2*vc1%vc1+2*c1%ac1-vc0%vc2-c0%ac2-ac0%c2-vc0%vc2;
		const arma::Row<fltp> jdelta = 2*ac1%vc1+2*vc1%ac1 + 2*vc1%ac1+2*c1%jc1 - ac0%vc2-vc0%ac2 - vc0%ac2-c0%jc2 - jc0%c2-ac0%vc2 - ac0%vc2-vc0%ac2;

		assert(arma::all(delta>=0));

		const arma::Row<fltp> t1 = (-c1 - arma::sqrt(delta))/c2;
		const arma::Row<fltp> vt1 = (-vdelta/(2*arma::sqrt(delta))-vc1)/c2-(vc2%(-arma::sqrt(delta)-c1))/arma::square(c2);
		const arma::Row<fltp> at1 = (-adelta/(2*arma::sqrt(delta))+arma::square(vdelta)/(4*arma::pow(delta,3.0/2))-ac1)/c2-(2*vc2%(-vdelta/(2*arma::sqrt(delta))-vc1))/arma::square(c2)-(ac2%(-sqrt(delta)-c1))/arma::square(c2)+(2*arma::square(vc2)%(-sqrt(delta)-c1))/arma::pow(c2,3);
		const arma::Row<fltp> jt1 = (-jdelta/(2*arma::sqrt(delta))+(3*vdelta%adelta)/(4*arma::pow(delta,3.0/2))-(3*arma::pow(vdelta,3))/(8*arma::pow(delta,5.0/2))-jc1)/c2-(3*vc2%(-adelta/(2*arma::sqrt(delta))+arma::square(vdelta)/(4*arma::pow(delta,3.0/2))-ac1))/arma::square(c2)-(3*ac2%(-vdelta/(2*arma::sqrt(delta))-vc1))/arma::square(c2)+(6*arma::square(vc2)%(-vdelta/(2*arma::sqrt(delta))-vc1))/arma::pow(c2,3)-(jc2%(-sqrt(delta)-c1))/arma::square(c2)+(6*vc2%ac2%(-sqrt(delta)-c1))/arma::pow(c2,3)-(6*arma::pow(vc2,3)%(-sqrt(delta)-c1))/arma::pow(c2,4);

		const arma::Row<fltp> t2 = (-c1 + arma::sqrt(delta))/c2;
		const arma::Row<fltp> vt2 = (vdelta/(2*arma::sqrt(delta))-vc1)/c2-(vc2%(sqrt(delta)-c1))/arma::square(c2);
		const arma::Row<fltp> at2 = (adelta/(2*arma::sqrt(delta))-arma::square(vdelta)/(4*arma::pow(delta,3.0/2))-ac1)/c2-(2*vc2%(vdelta/(2*arma::sqrt(delta))-vc1))/arma::square(c2)-(ac2%(sqrt(delta)-c1))/arma::square(c2)+(2*arma::square(vc2)%(sqrt(delta)-c1))/arma::pow(c2,3);
		const arma::Row<fltp> jt2 = (jdelta/(2*arma::sqrt(delta))-(3*vdelta%adelta)/(4*arma::pow(delta,3.0/2))+(3*arma::pow(vdelta,3))/(8*arma::pow(delta,5.0/2))-jc1)/c2-(3*vc2%(adelta/(2*arma::sqrt(delta))-arma::square(vdelta)/(4*arma::pow(delta,3.0/2))-ac1))/arma::square(c2)-(3*ac2%(vdelta/(2*arma::sqrt(delta))-vc1))/arma::square(c2)+(6*arma::square(vc2)%(vdelta/(2*arma::sqrt(delta))-vc1))/arma::pow(c2,3)-(jc2%(sqrt(delta)-c1))/arma::square(c2)+(6*vc2%ac2%(sqrt(delta)-c1))/arma::pow(c2,3)-(6*arma::pow(vc2,3)%(sqrt(delta)-c1))/arma::pow(c2,4);

		// get cone intersection points
		const arma::Mat<fltp> Rci1 = Rp + dRp.each_row()%t1;
		const arma::Mat<fltp> Rci2 = Rp + dRp.each_row()%t2;

		// allocate output
		arma::Row<fltp> rr(num_points,arma::fill::zeros), vrr(num_points,arma::fill::zeros);
		arma::Row<fltp> arr(num_points,arma::fill::zeros), jrr(num_points,arma::fill::zeros);

		{// limit scope
			// select largest radius while ensuring we 
			// are not intersecting with the negative half of the cone
			const arma::Col<arma::uword> idxp1 = arma::find(t1>t2);
			const arma::Col<arma::uword> idxp2 = arma::find(t2>=t1);

			// assign points to output list
			rr(idxp1) = t1(idxp1); vrr(idxp1) = vt1(idxp1); arr(idxp1) = at1(idxp1); jrr(idxp1) = jt1(idxp1); 
			rr(idxp2) = t2(idxp2); vrr(idxp2) = vt2(idxp2); arr(idxp2) = at2(idxp2); jrr(idxp2) = jt2(idxp2); 
		}

		// check result
		assert(rr.is_finite());
		assert(vrr.is_finite());
		assert(arr.is_finite());
		assert(jrr.is_finite());
		assert(arma::all(rr>0));

		// calculate derivatives
		return {rr,vrr,arr,jrr};

	}

	// get scaling array fun
	arma::field<arma::Row<fltp> > DriveRCCT::calc_radius_cone(
		const ZFunType &zfun, 
		const arma::Row<fltp>& theta_mod, 
		const fltp radius, 
		const arma::uword num_poles){

		// number of points
		const arma::uword num_points = theta_mod.n_elem;

		// get arrays
		const arma::field<arma::Row<fltp> > C = zfun(theta_mod);
		const arma::Row<fltp>& zz = C(0); const arma::Row<fltp>& vz = C(1); 
		const arma::Row<fltp>& az = C(2); const arma::Row<fltp>& jz = C(3); 

		// the lines are given as Rp + t*dRp
		const arma::Mat<fltp> Rp = arma::join_vert(
			arma::Row<fltp>(num_points,arma::fill::zeros),
			arma::Row<fltp>(num_points,arma::fill::zeros),zz);
		
		const arma::Mat<fltp> vRp = arma::join_vert(
			arma::Row<fltp>(num_points,arma::fill::zeros),
			arma::Row<fltp>(num_points,arma::fill::zeros),vz);
		const arma::Mat<fltp> aRp = arma::join_vert(
			arma::Row<fltp>(num_points,arma::fill::zeros),
			arma::Row<fltp>(num_points,arma::fill::zeros),az);
		const arma::Mat<fltp> jRp = arma::join_vert(
			arma::Row<fltp>(num_points,arma::fill::zeros),
			arma::Row<fltp>(num_points,arma::fill::zeros),jz);

		// direction just pointing radially in xy-plane
		const arma::Mat<fltp> dRp = arma::join_vert(
			arma::cos(theta_mod), arma::sin(theta_mod), 
			arma::Row<fltp>(num_points,arma::fill::zeros));
		const arma::Mat<fltp> vdRp = arma::join_vert(
			-arma::sin(theta_mod), arma::cos(theta_mod),
			arma::Row<fltp>(num_points,arma::fill::zeros));
		const arma::Mat<fltp> adRp = arma::join_vert(
			-arma::cos(theta_mod), -arma::sin(theta_mod),
			arma::Row<fltp>(num_points,arma::fill::zeros));
		const arma::Mat<fltp> jdRp = arma::join_vert(
			arma::sin(theta_mod), -arma::cos(theta_mod),
			arma::Row<fltp>(num_points,arma::fill::zeros));

		// // currently this algorithm only works for omega zero
		// const fltp omega = RAT_CONST(0.0);

		// number of points
		const arma::uword num_sections = 2*num_poles;
		const arma::uword num_anchor_points = num_sections+1;

		// theta position for reference points
		const arma::Row<fltp> theta_ref = 
			arma::linspace<arma::Row<fltp> >(
			0,arma::Datum<fltp>::tau,num_anchor_points);

		// create coordinates at z=0
		const arma::field<arma::Row<fltp> > C0 = zfun(theta_ref);
		const arma::Mat<rat::fltp> R = arma::join_vert(
			radius*arma::cos(theta_ref),
			radius*arma::sin(theta_ref),C0(0));
		const arma::Mat<rat::fltp> V = arma::join_vert(
			-radius*arma::sin(theta_ref),
			radius*arma::cos(theta_ref),C0(1));

		// normalize velocity to get longitudinal vector
		const arma::Mat<rat::fltp> L = V.each_row()/rat::cmn::Extra::vec_norm(V);

		// transverse vector
		arma::Mat<rat::fltp> Vax(3,L.n_cols,arma::fill::zeros); Vax.row(2).fill(RAT_CONST(1.0));
		arma::Mat<rat::fltp> D = rat::cmn::Extra::cross(L,Vax);		
		D.each_row()/=rat::cmn::Extra::vec_norm(D); // normalize

		// normal vector
		const arma::Mat<rat::fltp> N = rat::cmn::Extra::cross(L,D);

		// get indices of connected control points
		const arma::uword idx1 = 0;
		const arma::uword idx2 = 1;

		// get frame at start point
		const arma::Col<rat::fltp>::fixed<3> R1 = R.col(idx1);
		const arma::Col<rat::fltp>::fixed<3> L1 = L.col(idx1);
		const arma::Col<rat::fltp>::fixed<3> N1 = N.col(idx1);
		const arma::Col<rat::fltp>::fixed<3> D1 = D.col(idx1);

		// get frame at end point
		const arma::Col<rat::fltp>::fixed<3> R2 = R.col(idx2);
		const arma::Col<rat::fltp>::fixed<3> L2 = -L.col(idx2);
		const arma::Col<rat::fltp>::fixed<3> N2 = N.col(idx2);
		const arma::Col<rat::fltp>::fixed<3> D2 = D.col(idx2);

		// vector from R1 to R2
		const arma::Col<rat::fltp>::fixed<3> V12 = R2 - R1;

		// calculate angle between curves the curves should intersect at the tip
		const rat::fltp gamma = std::acos(arma::as_scalar(rat::cmn::Extra::dot(L1,L2)));

		// distance between points
		const rat::fltp l12 = arma::as_scalar(rat::cmn::Extra::vec_norm(V12));

		// distance to tip
		const rat::fltp ht = l12/(2*std::tan(gamma/2));

		// triangle edge length is also edge of cone from tip to base
		const rat::fltp ledge = std::sqrt(std::pow(ht,2) + std::pow(l12/2,2));

		// intersection point
		const arma::Col<rat::fltp>::fixed<3> Ri1 = R1 + ledge*L1;
		const arma::Col<rat::fltp>::fixed<3> Ri2 = R2 + ledge*L2;

		// cone vector is at intersection of LxD planes
		// even if Rbase does not lie along D1 nor D2
		arma::Col<rat::fltp>::fixed<3> cone_axis = -rat::cmn::Extra::cross(N1, N2);

		// normalize
		cone_axis.each_row()/=rat::cmn::Extra::vec_norm(cone_axis);

		// cone angle
		const rat::fltp beta1 = std::acos(arma::as_scalar(rat::cmn::Extra::dot(cone_axis,L1)));
		// const rat::fltp beta2 = std::acos(arma::as_scalar(rat::cmn::Extra::dot(cone_axis,L2)));

		// cone radius and height
		// const rat::fltp rcone = std::sin(beta1)*ledge;
		const rat::fltp hcone = std::cos(beta1)*ledge;

		// find cone base
		const arma::Col<rat::fltp>::fixed<3> Rbase = Ri1 - hcone*cone_axis;

		// // vectors from base to R1 and R2
		// const arma::Col<rat::fltp>::fixed<3> V1 = R1 - Rbase;
		// const arma::Col<rat::fltp>::fixed<3> V2 = R2 - Rbase;

		// // lengths of these vectors
		// const rat::fltp lv1 = arma::as_scalar(rat::cmn::Extra::vec_norm(V1));
		// const rat::fltp lv2 = arma::as_scalar(rat::cmn::Extra::vec_norm(V2));

		// // angle between R1 and R2 with respect to Rbase
		// const rat::fltp phi = arma::Datum<rat::fltp>::pi/2 - 
		// 	RAT_CONST(0.5)*std::acos(arma::as_scalar(rat::cmn::Extra::dot(V1,V2))/(lv1*lv2));

		// cone vector
		const arma::Col<fltp>::fixed<3> Dc = -cone_axis;

		// relative position to cone tip
		const arma::Mat<fltp> Delta = Rp.each_col() - Ri1;
		const arma::Mat<fltp> vDelta = vRp;
		const arma::Mat<fltp> aDelta = aRp;
		const arma::Mat<fltp> jDelta = jRp;

		// find intersection with cone (https://www.geometrictools.com/Documentation/IntersectionLineCone.pdf)
		const fltp fg = std::cos(beta1)*std::cos(beta1);
		const arma::Row<fltp> c2 = 
			arma::square(Dc.t()*dRp) -
			fg*(cmn::Extra::dot(dRp,dRp));
		const arma::Row<fltp> c1 = 
			(Dc.t()*dRp)%(Dc.t()*Delta) -
			fg*(cmn::Extra::dot(dRp,Delta));
		const arma::Row<fltp> c0 = 
			arma::square(Dc.t()*Delta) -
			fg*(cmn::Extra::dot(Delta,Delta));

		// derivatives of coefficients
		// first derrivative
		const arma::Row<fltp> vc2 = 
			2*(Dc.t()*dRp)%(Dc.t()*vdRp) - 
			fg*2*cmn::Extra::dot(dRp,vdRp);
		const arma::Row<fltp> vc1 = 
			(Dc.t()*vdRp)%(Dc.t()*Delta) + 
			(Dc.t()*dRp)%(Dc.t()*vDelta) - 
			fg*(cmn::Extra::dot(vdRp,Delta)) - 
			fg*(cmn::Extra::dot(dRp,vDelta));
		const arma::Row<fltp> vc0 = 
			2*(Dc.t()*Delta)%(Dc.t()*vDelta) - 
			fg*2*(cmn::Extra::dot(Delta,vDelta));
		
		// second derrivative
		const arma::Row<fltp> ac2 = 
			2*(Dc.t()*vdRp)%(Dc.t()*vdRp) + 
			2*(Dc.t()*dRp)%(Dc.t()*adRp) -
			fg*2*cmn::Extra::dot(vdRp,vdRp)-
			fg*2*cmn::Extra::dot(dRp,adRp);
		const arma::Row<fltp> ac1 = 
			(Dc.t()*adRp)%(Dc.t()*Delta) + 
			2*(Dc.t()*vdRp)%(Dc.t()*vDelta) +
			(Dc.t()*dRp)%(Dc.t()*aDelta) - 
			fg*(cmn::Extra::dot(adRp,Delta)) -
			2*fg*(cmn::Extra::dot(vdRp,vDelta)) - 
			fg*(cmn::Extra::dot(dRp,aDelta));
		const arma::Row<fltp> ac0 =
			2*(Dc.t()*vDelta)%(Dc.t()*vDelta) +
			2*(Dc.t()*Delta)%(Dc.t()*aDelta) - 
			fg*2*(cmn::Extra::dot(vDelta,vDelta))- 
			fg*2*(cmn::Extra::dot(Delta,aDelta));
		
		// third derrivative
		const arma::Row<fltp> jc2 = 
			2*(Dc.t()*adRp)%(Dc.t()*vdRp) + 
			4*(Dc.t()*vdRp)%(Dc.t()*adRp) + 
			2*(Dc.t()*dRp)%(Dc.t()*jdRp) -
			fg*2*cmn::Extra::dot(adRp,vdRp)-
			2*fg*2*cmn::Extra::dot(vdRp,adRp)-
			fg*2*cmn::Extra::dot(dRp,jdRp);
		const arma::Row<fltp> jc1 = 
			(Dc.t()*jdRp)%(Dc.t()*Delta) + 
			(Dc.t()*adRp)%(Dc.t()*vDelta) + 
			2*(Dc.t()*adRp)%(Dc.t()*vDelta) +
			2*(Dc.t()*vdRp)%(Dc.t()*aDelta) +
			(Dc.t()*vdRp)%(Dc.t()*aDelta) +
			(Dc.t()*dRp)%(Dc.t()*jDelta) - 
			fg*(cmn::Extra::dot(jdRp,Delta)) -
			fg*(cmn::Extra::dot(adRp,vDelta)) -
			2*fg*(cmn::Extra::dot(adRp,vDelta)) -
			2*fg*(cmn::Extra::dot(vdRp,aDelta)) - 
			fg*(cmn::Extra::dot(vdRp,aDelta)) -
			fg*(cmn::Extra::dot(dRp,jDelta));
		const arma::Row<fltp> jc0 =
			2*(Dc.t()*aDelta)%(Dc.t()*vDelta) +
			4*(Dc.t()*vDelta)%(Dc.t()*aDelta) +
			2*(Dc.t()*Delta)%(Dc.t()*jDelta) - 
			fg*2*(cmn::Extra::dot(aDelta,vDelta))-
			fg*4*(cmn::Extra::dot(vDelta,aDelta))- 
			fg*2*(cmn::Extra::dot(Delta,jDelta));

		// find roots using ABC formula there is two possible solutions
		const arma::Row<fltp> delta = c1%c1 - c0%c2;
		const arma::Row<fltp> vdelta = 2*c1%vc1-c0%vc2-vc0%c2;
		const arma::Row<fltp> adelta = 2*vc1%vc1+2*c1%ac1-vc0%vc2-c0%ac2-ac0%c2-vc0%vc2;
		const arma::Row<fltp> jdelta = 2*ac1%vc1+2*vc1%ac1 + 2*vc1%ac1+2*c1%jc1 - ac0%vc2-vc0%ac2 - vc0%ac2-c0%jc2 - jc0%c2-ac0%vc2 - ac0%vc2-vc0%ac2;

		const arma::Row<fltp> t1 = (-c1 - arma::sqrt(delta))/c2;
		const arma::Row<fltp> vt1 = (-vdelta/(2*arma::sqrt(delta))-vc1)/c2-(vc2%(-arma::sqrt(delta)-c1))/arma::square(c2);
		const arma::Row<fltp> at1 = (-adelta/(2*arma::sqrt(delta))+arma::square(vdelta)/(4*arma::pow(delta,3.0/2))-ac1)/c2-(2*vc2%(-vdelta/(2*arma::sqrt(delta))-vc1))/arma::square(c2)-(ac2%(-sqrt(delta)-c1))/arma::square(c2)+(2*arma::square(vc2)%(-sqrt(delta)-c1))/arma::pow(c2,3);
		const arma::Row<fltp> jt1 = (-jdelta/(2*arma::sqrt(delta))+(3*vdelta%adelta)/(4*arma::pow(delta,3.0/2))-(3*arma::pow(vdelta,3))/(8*arma::pow(delta,5.0/2))-jc1)/c2-(3*vc2%(-adelta/(2*arma::sqrt(delta))+arma::square(vdelta)/(4*arma::pow(delta,3.0/2))-ac1))/arma::square(c2)-(3*ac2%(-vdelta/(2*arma::sqrt(delta))-vc1))/arma::square(c2)+(6*arma::square(vc2)%(-vdelta/(2*arma::sqrt(delta))-vc1))/arma::pow(c2,3)-(jc2%(-sqrt(delta)-c1))/arma::square(c2)+(6*vc2%ac2%(-sqrt(delta)-c1))/arma::pow(c2,3)-(6*arma::pow(vc2,3)%(-sqrt(delta)-c1))/arma::pow(c2,4);

		const arma::Row<fltp> t2 = (-c1 + arma::sqrt(delta))/c2;
		const arma::Row<fltp> vt2 = (vdelta/(2*arma::sqrt(delta))-vc1)/c2-(vc2%(sqrt(delta)-c1))/arma::square(c2);
		const arma::Row<fltp> at2 = (adelta/(2*arma::sqrt(delta))-arma::square(vdelta)/(4*arma::pow(delta,3.0/2))-ac1)/c2-(2*vc2%(vdelta/(2*arma::sqrt(delta))-vc1))/arma::square(c2)-(ac2%(sqrt(delta)-c1))/arma::square(c2)+(2*arma::square(vc2)%(sqrt(delta)-c1))/arma::pow(c2,3);
		const arma::Row<fltp> jt2 = (jdelta/(2*arma::sqrt(delta))-(3*vdelta%adelta)/(4*arma::pow(delta,3.0/2))+(3*arma::pow(vdelta,3))/(8*arma::pow(delta,5.0/2))-jc1)/c2-(3*vc2%(adelta/(2*arma::sqrt(delta))-arma::square(vdelta)/(4*arma::pow(delta,3.0/2))-ac1))/arma::square(c2)-(3*ac2%(vdelta/(2*arma::sqrt(delta))-vc1))/arma::square(c2)+(6*arma::square(vc2)%(vdelta/(2*arma::sqrt(delta))-vc1))/arma::pow(c2,3)-(jc2%(sqrt(delta)-c1))/arma::square(c2)+(6*vc2%ac2%(sqrt(delta)-c1))/arma::pow(c2,3)-(6*arma::pow(vc2,3)%(sqrt(delta)-c1))/arma::pow(c2,4);

		// case where c2 = 0
		const arma::Row<fltp> t3 = -c0/(2*c1);
		const arma::Row<fltp> vt3 = -(c0%vc1-vc0%c1)/(2*arma::square(c1));
		const arma::Row<fltp> at3 = (vc1%(c0%vc1-vc0%c1))/arma::pow(c1,3) - (c0%ac1-ac0%c1)/(2*arma::square(c1));
		const arma::Row<fltp> jt3 = -(c0%arma::square(c1)%jc1+(3*vc0%arma::square(c1)-6*c0%c1%vc1)%ac1+6*c0%arma::pow(vc1,3)-6*vc0%c1%arma::square(vc1)+3*ac0%arma::square(c1)%vc1-jc0%arma::pow(c1,3))/(2*arma::pow(c1,4));

		// get cone intersection points
		const arma::Mat<fltp> Rci1 = Rp + dRp.each_row()%t1;
		const arma::Mat<fltp> Rci2 = Rp + dRp.each_row()%t2;
		const arma::Mat<fltp> Rci3 = Rp + dRp.each_row()%t3;

		// find relative axial position to the cone tip
		const arma::Row<fltp> zp1 = Dc.t()*(Rci1.each_col() - Ri1);
		const arma::Row<fltp> zp2 = Dc.t()*(Rci2.each_col() - Ri1);
		const arma::Row<fltp> zp3 = Dc.t()*(Rci3.each_col() - Ri1);

		// allocate output
		arma::Row<fltp> rr(num_points,arma::fill::zeros), vrr(num_points,arma::fill::zeros);
		arma::Row<fltp> arr(num_points,arma::fill::zeros), jrr(num_points,arma::fill::zeros);

		{// limit scope
			// select largest radius while ensuring we 
			// are not intersecting with the negative half of the cone
			const arma::Col<arma::uword> idxp1 = arma::find((zp2<0 && zp1>0) || (zp1>0 && zp2>0 && t1>t2));
			const arma::Col<arma::uword> idxp2 = arma::find((zp1<0 && zp2>0) || (zp1>0 && zp2>0 && t2>t1));
			const arma::Col<arma::uword> idxp3 = arma::find(arma::abs(c2)<1e-9 && zp3>0);

			// assign points to output list
			rr(idxp1) = t1(idxp1); vrr(idxp1) = vt1(idxp1); arr(idxp1) = at1(idxp1); jrr(idxp1) = jt1(idxp1); 
			rr(idxp2) = t2(idxp2); vrr(idxp2) = vt2(idxp2); arr(idxp2) = at2(idxp2); jrr(idxp2) = jt2(idxp2); 
			rr(idxp3) = t3(idxp3); vrr(idxp3) = vt3(idxp3); arr(idxp3) = at3(idxp3); jrr(idxp3) = jt3(idxp3); 
		}

		// check if R is larger than zero
		// if(arma::any(rr<=0))rat_throw_line("radius is smaller than zero");

		// check result
		assert(rr.is_finite());
		assert(vrr.is_finite());
		assert(arr.is_finite());
		assert(jrr.is_finite());
		

		return {rr,vrr,arr,jrr};
	}


	// get scaling array fun
	arma::Row<fltp> DriveRCCT::get_scaling_vec(
		const arma::Row<fltp> &positions, 
		const fltp /*time*/, 
		const arma::uword derivative) const{

		// INPUT
		// check input
		is_valid(true);

		// get running variable
		const arma::Row<fltp>& vv = positions;

		// number of points requested
		const arma::uword num_points = vv.n_elem;

		// number of poles
		const rat::fltp np = rat::fltp(num_poles_);

		// for skew angle just shift theta
		const rat::fltp phase = is_skew_ ? arma::Datum<fltp>::pi/(2*np) : 0.0;

		// convert to angle each turn lies on the vv interval [-0.5,0.5]
		const arma::Row<fltp> theta = (vv+RAT_CONST(0.5))*arma::Datum<fltp>::tau + phase;

		// only take theta into account for half a pole
		const fltp theta_max = arma::Datum<fltp>::pi/np;
		const arma::Row<fltp> theta_mod = theta - arma::floor(theta/theta_max)*theta_max;

		// early out when alpha is 90 deg
		if(alpha_>(arma::Datum<rat::fltp>::pi/2 - RAT_CONST(1e-9)))
			return arma::Row<fltp>(num_points, arma::fill::value(radius_));

		// tangens of angle
		const fltp tan_alpha = std::tan(arma::Datum<fltp>::pi/2 - alpha_);

		// calculate amplitude to match the winding angle
		const fltp amplitude = radius_/(tan_alpha*np);

		// currently the algorithm only works with omega zero
		// const fltp omega = RAT_CONST(0.0);

		// // create functionf for calculating z,vz,az,jz as function of theta
		// const ZFunType zfun = [this,np,amplitude,omega](const arma::Row<fltp>&theta){
		// 	// precalculate
		// 	const arma::Row<fltp> sin_np_theta = arma::sin(np*theta);
		// 	const arma::Row<fltp> cos_np_theta = arma::cos(np*theta);

		// 	// flattening factors
		// 	const arma::Row<fltp> ff = RAT_CONST(1.0)/arma::sqrt(b_*arma::square(sin_np_theta)+1);
		// 	const arma::Row<fltp> vf = -(b_*np*cos_np_theta%sin_np_theta)/arma::pow(b_*arma::square(sin_np_theta)+1,3.0/2);
		// 	const arma::Row<fltp> af = -(b_*(np*np)*(b_*arma::pow(cos_np_theta,4)+2*arma::square(cos_np_theta)-b_-1))/arma::pow(b_*arma::square(sin_np_theta)+1,5.0/2);
		// 	const arma::Row<fltp> jf = (b_*std::pow(np,3)*cos_np_theta%(b_*(arma::square(cos_np_theta)%(b_*(arma::square(cos_np_theta)+4)+10)-5*b_-1)+4)%sin_np_theta)/arma::pow(b_*arma::square(sin_np_theta)+1,7.0/2);

		// 	// regular sine and cosine 
		// 	const arma::Row<fltp> gg = amplitude*sin_np_theta + omega*theta/arma::Datum<rat::fltp>::tau;
		// 	const arma::Row<fltp> vg = np*amplitude*cos_np_theta + omega/arma::Datum<rat::fltp>::tau;
		// 	const arma::Row<fltp> ag = -np*np*amplitude*sin_np_theta;
		// 	const arma::Row<fltp> jg = -np*np*np*amplitude*cos_np_theta;

		// 	// combine and calculate derivatives
		// 	const arma::Row<fltp> zz = ff%gg;
		// 	const arma::Row<fltp> vz = vf%gg + ff%vg;
		// 	const arma::Row<fltp> az = af%gg + 2*vf%vg + ff%ag;
		// 	const arma::Row<fltp> jz = jf%gg + af%vg + 2*af%vg + 2*vf%ag + vf%ag + ff%jg;

		// 	// combine and return
		// 	return arma::field<arma::Row<fltp> >{zz,vz,az,jz};
		// };

		// create function for calculating z,vz,az,jz as function of theta
		// f = 0.2; b = -0.2; n = 3; true_period = 2*pi/n; A = 1.5; true_amplitude = A/sqrt(b+1); theta = linspace(0,2*pi,100); z = A*sin(n*theta)./sqrt(b*sin(n*theta).^2 + 1); z_str = f*true_amplitude; dzdtheta0 = n*A; theta_str = z_str/dzdtheta0; theta_curved = linspace(theta_str,true_period/2-theta_str,100); n_curved = n*(true_period/(true_period - 4*theta_str)); A_curved = A*(n/n_curved); z_curved = z_str + A_curved*sin(n_curved*(theta_curved-theta_str))./sqrt(b*sin(n_curved*(theta_curved-theta_str)).^2+1); figure; plot(theta,z,[0,theta_str],[0,z_str],theta_curved,z_curved);
		const ZFunType zfun = [this,np,amplitude](const arma::Row<fltp>&theta){
			// calculate
			// const fltp fstr = 0.2;
			const fltp overall_period = 2*arma::Datum<fltp>::pi/np;
			const fltp overall_amplitude = amplitude/std::sqrt(b_+1.0);
			
			// straight section end point
			const fltp slope = np*amplitude; // slope at theta = 0
			const fltp z_straight = fstr_*overall_amplitude;
			const fltp theta_str = z_straight/slope; 

			// curved section parameters
			const fltp np_curved = np*(overall_period/(overall_period - 4*theta_str)); 
			const fltp amplitude_curved = amplitude*(np/np_curved);

			// sections
			// z_straight = slope*theta;
			// z_curved = z_str + A_curved*sin(n_curved*(theta_curved-theta_str))./sqrt(b*sin(n_curved*(theta_curved-theta_str))+1); 

			// allocate output vectors
			arma::Row<fltp> zz(theta.n_elem); arma::Row<fltp> vz(theta.n_elem);
			arma::Row<fltp> az(theta.n_elem); arma::Row<fltp> jz(theta.n_elem);

			// walk over coordinates
			for(arma::uword i=0;i<theta.n_elem;i++){
				// modulus
				const fltp mytheta = std::fmod(theta(i), arma::Datum<fltp>::tau);

				// first straight
				if(mytheta<theta_str){
					zz(i) = slope*mytheta; vz(i) = slope; az(i) = 0.0; jz(i) = 0.0;
				}

				// curved sections
				else if(
					(mytheta>=theta_str && mytheta<=overall_period/2 - theta_str) || 
					(mytheta>=overall_period/2 + theta_str && mytheta<=overall_period-theta_str)){
					
					// precalculate
					const fltp sin_np_theta = std::sin(np_curved*(mytheta-theta_str));
					const fltp cos_np_theta = std::cos(np_curved*(mytheta-theta_str));
					
					const fltp ff = RAT_CONST(1.0)/std::sqrt(b_*sin_np_theta*sin_np_theta+RAT_CONST(1.0));
					const fltp vf = -(b_*np_curved*cos_np_theta*sin_np_theta)/std::pow(b_*sin_np_theta*sin_np_theta+RAT_CONST(1.0),RAT_CONST(3.0)/2);
					const fltp af = -(b_*(np_curved*np_curved)*(b_*std::pow(cos_np_theta,4)+2*cos_np_theta*cos_np_theta-b_-1))/std::pow(b_*sin_np_theta*sin_np_theta+RAT_CONST(1.0),RAT_CONST(5.0)/2);
					const fltp jf = (b_*std::pow(np,3)*cos_np_theta*(b_*(cos_np_theta*cos_np_theta*(b_*(cos_np_theta*cos_np_theta+4)+10)-5*b_-1)+4)*sin_np_theta)/std::pow(b_*sin_np_theta*sin_np_theta+1,RAT_CONST(7.0)/2);

					const fltp gg = amplitude_curved*sin_np_theta;
					const fltp vg = np_curved*amplitude_curved*cos_np_theta;
					const fltp ag = -np_curved*np_curved*amplitude_curved*sin_np_theta;
					const fltp jg = -np_curved*np_curved*np_curved*amplitude_curved*cos_np_theta;

					zz(i) = ff*gg + z_straight;
					vz(i) = vf*gg + ff*vg;
					az(i) = af*gg + 2*vf*vg + ff*ag;
					jz(i) = jf*gg + af*vg + 2*af*vg + 2*vf*ag + vf*ag + ff*jg;
				}

				// second straight
				else if(mytheta>overall_period/2 - theta_str && mytheta<overall_period/2 + theta_str){
					zz(i) = -slope*(mytheta-overall_period/2); vz(i) = -slope; az(i) = 0.0; jz(i) = 0.0;
				}

				// third straight
				else{
					zz(i) = slope*(mytheta-overall_period); vz(i) = slope; az(i) = 0.0; jz(i) = 0.0;
				}
			}

			// combine and return
			return arma::field<arma::Row<fltp> >{zz,vz,az,jz};
		};

		

		// // create functionf for calculating z,vz,az,jz as function of theta
		// const ZFunType zfun2 = [this,zfun,np,amplitude,omega](const arma::Row<fltp> theta2, const arma::uword nn){
		// 	// precalculate
		// 	const arma::Row<fltp> sin_np_theta = arma::sin(np*theta2);
		// 	const arma::Row<fltp> cos_np_theta = arma::cos(np*theta2);

		// 	// interpolate radius at theta
		// 	const arma::field<arma::Row<fltp> > rho = calc_radius_cone(zfun,theta2);
		// 	const arma::Row<fltp>& rr = rho(0);
		// 	const arma::Row<fltp>& vr = rho(1);
		// 	const arma::Row<fltp>& ar = rho(2);
		// 	const arma::Row<fltp>& jr = rho(3);

		// 	// flattening factors
		// 	const arma::Row<fltp> ff = arma::pow(rr/radius_,np/2.0);
		// 	const arma::Row<fltp> vf = (np*arma::pow(rr/radius_,np/2)%vr)/(2*rr);
		// 	const arma::Row<fltp> af = (np*arma::pow(rr/radius_,np/2)%(2*rr%ar+(np-2)*arma::square(vr)))/(4*arma::square(rr));
		// 	const arma::Row<fltp> jf = (np*arma::pow(rr/radius_,np/2)%(4*arma::square(rr)%jr+(6*np-12)*rr%vr%ar+((np*np)-6*np+8)*arma::pow(vr,3)))/(8*arma::pow(rr,3));

		// 	// regular sine and cosine 
		// 	const arma::Row<fltp> gg = amplitude*sin_np_theta + omega*theta2/arma::Datum<rat::fltp>::tau;
		// 	const arma::Row<fltp> vg = np*amplitude*cos_np_theta + omega/arma::Datum<rat::fltp>::tau;
		// 	const arma::Row<fltp> ag = -np*np*amplitude*sin_np_theta;
		// 	const arma::Row<fltp> jg = -np*np*np*amplitude*cos_np_theta;

		// 	// combine and calculate derivatives
		// 	const arma::Row<fltp> zz = ff%gg;
		// 	const arma::Row<fltp> vz = vf%gg + ff%vg;
		// 	const arma::Row<fltp> az = af%gg + 2*vf%vg + ff%ag;
		// 	const arma::Row<fltp> jz = jf%gg + af%vg + 2*af%vg + 2*vf%ag + vf%ag + ff%jg;

		// 	// combine
		// 	if(nn==0)return zz; else if(nn==1)return vz; else if(nn==2)return az; else if(nn==3)return jz;
		// 	else rat_throw_line("higher derivative not available");
		// };

		// calculate
		arma::field<arma::Row<fltp> > rho;
		if(num_poles_==1)rho = calc_radius_cyl(zfun,theta_mod,alpha_,radius_);
		else if(num_poles_>1)rho = calc_radius_cone(zfun,theta_mod,radius_,num_poles_);
		else rat_throw_line("number of poles must be positive");

		// calculate derivatives
		if(derivative==0)return rho(0);
		if(derivative==1)return rho(1)*arma::Datum<fltp>::tau;
		if(derivative==2)return rho(2)*std::pow(arma::Datum<fltp>::tau,2);
		if(derivative==3)return rho(3)*std::pow(arma::Datum<fltp>::tau,3);
		rat_throw_line("fourth order derivative not available");
	}

	// get current
	fltp DriveRCCT::get_scaling(const fltp position, const fltp time, const arma::uword derivative) const{
		return arma::as_scalar(get_scaling_vec(arma::Row<fltp>{position}, time, derivative));
	}

	// apply scaling for the input settings
	void DriveRCCT::rescale(const fltp scale_factor){
		radius_ *= scale_factor;
	}

	// validity check
	bool DriveRCCT::is_valid(const bool enable_throws)const{
		if(b_<=-1.0){if(enable_throws){rat_throw_line("b must be larger than minus one");} return false;};
		if(num_poles_<=0){if(enable_throws){rat_throw_line("number of poles must be larger than zero");} return false;};
		if(radius_<=0){if(enable_throws){rat_throw_line("radius must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string DriveRCCT::get_type(){
		return "rat::mdl::drivercct";
	}

	// method for serialization into json
	void DriveRCCT::serialize(Json::Value &js, cmn::SList &list) const{
		Drive::serialize(js,list);
		js["type"] = get_type();
		js["num_poles"] = static_cast<int>(num_poles_);
		js["is_skew"] = is_skew_;
		js["radius"] = radius_;
		js["alpha"] = alpha_;
		js["b2"] = b_;
		js["fstr"] = fstr_;
		
	}

	// method for deserialisation from json
	void DriveRCCT::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		Drive::deserialize(js,list,factory_list,pth);
		num_poles_ = js["num_poles"].asUInt64();
		is_skew_ = js["is_skew"].asBool();
		radius_ = js["radius"].ASFLTP();
		alpha_ = js["alpha"].ASFLTP();
		if(js.isMember("b2"))set_b(js["b2"].ASFLTP());
		if(js.isMember("b"))set_b(js["b"].ASFLTP()*js["b"].ASFLTP()); // backwards compatibility v2.013.0
		fstr_ = js["fstr"].ASFLTP();
	}

}}