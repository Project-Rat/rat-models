// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "emitter.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// create specific mesh
	std::list<ShMeshDataPr> Emitter::create_meshes(
		const std::list<arma::uword> &/*trace*/, 
		const MeshSettings &/*stngs*/) const{
		return {};
	}

	// get type
	std::string Emitter::get_type(){
		return "rat::mdl::emitter";
	}

	// method for serialization into json
	void Emitter::serialize(
		Json::Value &js, cmn::SList &list) const{
		// parent
		Transformations::serialize(js,list);
	}

	// method for deserialisation from json
	void Emitter::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent
		Transformations::deserialize(js,list,factory_list,pth);
	}



}}