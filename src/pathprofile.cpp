// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathprofile.hh"

// rat-common headers
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathProfile::PathProfile(){
		set_name("Profile");
	}

	// constructor with input
	PathProfile::PathProfile(const ShPathPr &top, const ShPathPr &side) : PathProfile(){
		set_top(top); set_side(side);
	}

	// factory methods
	ShPathProfilePr PathProfile::create(){
		return std::make_shared<PathProfile>();
	}

	// factory with input
	ShPathProfilePr PathProfile::create(const ShPathPr &top, const ShPathPr &side){
		return std::make_shared<PathProfile>(top,side);
	}

	// set shearing
	void PathProfile::set_shearing(
		const fltp alpha, const fltp pshear, const fltp ellshear){
		if(pshear<=0)rat_throw_line("shearing parameter must be positive");
		if(ellshear<=0)rat_throw_line("shearing length must be positive");
		alpha_ = alpha; pshear_ = pshear; ellshear_ = ellshear;
	}

	// set top profile
	void PathProfile::set_top(const ShPathPr& top){
		top_ = top;
	}

	// set side profile
	void PathProfile::set_side(const ShPathPr& side){
		side_ = side;
	}


	// get frame
	ShFramePr PathProfile::create_frame(const MeshSettings &stngs) const{
		// check input
		is_valid(true);

		// get frame
		const ShFramePr top_gen = top_->create_frame(stngs);
		const ShFramePr side_gen = side_->create_frame(stngs);

		// combine
		side_gen->combine();

		// get side profile vectors
		const arma::Mat<fltp> Rside = side_gen->get_coords(0);
		const arma::Mat<fltp> Dside = side_gen->get_transverse(0);
		assert(Rside.n_cols>=2); assert(Dside.n_cols>=2);

		// get top profile vectors
		arma::field<arma::Mat<fltp> > R = top_gen->get_coords();
		arma::field<arma::Mat<fltp> > L = top_gen->get_direction();
		arma::field<arma::Mat<fltp> > N = top_gen->get_normal();
		arma::field<arma::Mat<fltp> > D = top_gen->get_transverse();
		arma::field<arma::Mat<fltp> > B = top_gen->get_block();

		// interpolate z-direction
		for(arma::uword i=0;i<R.n_elem;i++){
			// check vectors
			assert(arma::all(arma::abs(cmn::Extra::vec_norm(L(i))-1.0)<1e-9));
			assert(arma::all(arma::abs(cmn::Extra::vec_norm(N(i))-1.0)<1e-9));
			assert(arma::all(arma::abs(cmn::Extra::vec_norm(D(i))-1.0)<1e-9));

			// temporary array for storing interpolation results
			arma::Col<fltp> rval;

			// check range
			assert(arma::max(Rside.row(1))>arma::max(R(i).row(1)));
			assert(arma::min(Rside.row(1))<arma::min(R(i).row(1)));

			// interpolate z-coordinate
			arma::interp1(Rside.row(1).t(),Rside.row(2).t(),R(i).row(1).t(),rval,"linear");
			
			// check range
			assert(rval.is_finite());

			// set z-coordinate
			R(i).row(2) = rval.t();

			// set direction vector
			for(arma::uword k=0;k<3;k++){
				arma::interp1(Rside.row(1).t(),Dside.row(k).t(),R(i).row(1).t(),rval,"linear");
				assert(rval.is_finite());
				D(i).row(k) = rval.t();
			}

			// renormalize after interpolation
			D(i).each_row()/=cmn::Extra::vec_norm(D(i));

			// calculate rotation
			const arma::Row<fltp> alpha = -arma::atan(D(i).row(1)/D(i).row(2));
			assert(alpha.is_finite());

			// walk over angles
			for(arma::uword j=0;j<alpha.n_elem;j++){
				// calculate rotation matrix
				const arma::Mat<fltp>::fixed<3,3> M = 
					cmn::Extra::create_rotation_matrix(
					cmn::Extra::unit_vec('x'),alpha(j));
				
				// rotate
				L(i).col(j) = M*L(i).col(j);
				N(i).col(j) = M*N(i).col(j);
				B(i).col(j) = M*B(i).col(j);
			}

			// // rotate longitudinal vector over x-axis
			// L(i).row(1) = arma::cos(alpha)%L(i).row(1) - arma::sin(alpha)%L(i).row(2);
			// L(i).row(2) = arma::sin(alpha)%L(i).row(1) + arma::cos(alpha)%L(i).row(2);

			// // rotate normal vector over x-axis
			// N(i).row(1) = arma::cos(alpha)%N(i).row(1) - arma::sin(alpha)%N(i).row(2);
			// N(i).row(2) = arma::sin(alpha)%N(i).row(1) + arma::cos(alpha)%N(i).row(2);

			// // rotate block vector over x-axis
			// B(i).row(1) = arma::cos(alpha)%B(i).row(1) - arma::sin(alpha)%B(i).row(2);
			// B(i).row(2) = arma::sin(alpha)%B(i).row(1) + arma::cos(alpha)%B(i).row(2);

			// check if shearing was set
			// this is a feather-M2 feature
			// if it proves useful feel free 
			// to use for your projects
			if(alpha_!=0){
				// add twisting
				const arma::Row<fltp> as = alpha_*arma::pow(
					arma::clamp((ellshear_ - arma::abs(R(i).row(1)))/ellshear_,RAT_CONST(0.0),RAT_CONST(1.0)),pshear_); 

				// rotate vectors
				for(arma::uword j=0;j<R(i).n_cols;j++){
					// create rotation matrix
					const arma::Mat<fltp>::fixed<3,3> M = cmn::Extra::create_rotation_matrix(L(i).col(j),as(j));

					// rotate normal vector but not block vector
					N(i).col(j) = M*N(i).col(j);
					D(i).col(j) = M*D(i).col(j);
				}
			}

			// check vectors
			assert(arma::all(arma::abs(cmn::Extra::vec_norm(L(i))-1.0)<1e-9));
			assert(arma::all(arma::abs(cmn::Extra::vec_norm(N(i))-1.0)<1e-9));
			assert(arma::all(arma::abs(cmn::Extra::vec_norm(D(i))-1.0)<1e-9));

			// check handedness
			assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N(i),L(i)),D(i))>RAT_CONST(0.0)));
		}

		// create frame
		const ShFramePr gen = Frame::create(R,L,N,D,B);

		// apply transformations
		gen->apply_transformations(get_transformations(), stngs.time);

		// create combined profile
		return gen;
	}


	// validity check
	bool PathProfile::is_valid(const bool enable_throws) const{
		if(top_==NULL){if(enable_throws){rat_throw_line("top profile not set");} return false;};
		if(side_==NULL){if(enable_throws){rat_throw_line("side profile not set");} return false;};
		if(!top_->is_valid(enable_throws))return false;
		if(!side_->is_valid(enable_throws))return false;
		return true;
	}


	// get type
	std::string PathProfile::get_type(){
		return "rat::mdl::pathprofile";
	}

	// method for serialization into json
	void PathProfile::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Path::serialize(js,list);
		Transformations::serialize(js,list);

		js["type"] = get_type();

		// children
		js["top"] = cmn::Node::serialize_node(top_, list);
		js["side"] = cmn::Node::serialize_node(side_, list);

		js["alpha"] = alpha_;
		js["pshear"] = pshear_;
		js["ellshear"] = ellshear_;
	}

	// method for deserialisation from json
	void PathProfile::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent
		Path::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);
		
		set_top(cmn::Node::deserialize_node<Path>(js["top"], list, factory_list, pth));
		set_side(cmn::Node::deserialize_node<Path>(js["side"], list, factory_list, pth));

		// properties
		alpha_ = js["alpha"].ASFLTP();
		pshear_ = js["pshear"].ASFLTP();
		ellshear_ = js["ellshear"].ASFLTP();
	}

	

}}