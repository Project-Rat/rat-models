// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "crossline2.hh"

#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CrossLine2::CrossLine2(){
		set_name("Line");
	}

	// construct line with 2 points
	CrossLine2::CrossLine2 (
		const fltp u1, const fltp v1,
		const fltp u2, const fltp v2,
		const fltp thickness, const fltp dl) : CrossLine2() {
		set_coord(u1, v1, u2, v2, dl); set_thickness(thickness);
	}

	// factory
	ShCrossLine2Pr CrossLine2::create(){
		return std::make_shared<CrossLine2>();
	}

	// factory with dimension input
	ShCrossLine2Pr CrossLine2::create(
		const fltp u1, const fltp v1,
		const fltp u2, const fltp v2,
		const fltp thickness, const fltp dl){
		return std::make_shared<CrossLine2>(u1, v1, u2, v2, thickness, dl);
	}

	// get bounding box
	arma::Mat<fltp>::fixed<2,2> CrossLine2::get_bounding() const{
		arma::Mat<fltp>::fixed<2,2> bnd;

		bnd.col(0) = arma::Col<fltp>::fixed<2>{u1_, u2_};
		bnd.col(1) = arma::Col<fltp>::fixed<2>{v1_, v2_};
		return bnd;
	}

	// set coordinates
	void CrossLine2::set_coord(
		const fltp u1, const fltp v1,
		const fltp u2, const fltp v2,
		const fltp dl){
		u1_ = u1; u2_ = u2;
		v1_ = v1; v2_ = v2;
		dl_ = dl;
	}

	// setters
	void CrossLine2::set_u1(const fltp u1){
		u1_ = u1;
	}

	void CrossLine2::set_u2(const fltp u2){
		u2_ = u2;
	}

	void CrossLine2::set_v1(const fltp v1){
		v1_ = v1;
	}

	void CrossLine2::set_v2(const fltp v2){
		v2_ = v2;
	}

	void CrossLine2::set_thickness(const fltp thickness){
		thickness_ = thickness;
	}

	void CrossLine2::set_element_size(const fltp dl){
		dl_ = dl;
	}

	// getters
	fltp CrossLine2::get_u1() const {
		return u1_;
	}

	// getters
	fltp CrossLine2::get_u2() const {
		return u2_;
	}

	// getters
	fltp CrossLine2::get_v1() const {
		return v1_;
	}

	// getters
	fltp CrossLine2::get_v2() const {
		return v2_;
	}

	fltp CrossLine2::get_thickness() const {
		return thickness_;
	}

	fltp CrossLine2::get_element_size() const {
		return dl_;
	}

	// volume mesh
	ShAreaPr CrossLine2::create_area(const MeshSettings &/*stngs*/) const{
		// check settings
		is_valid(true);

		// number of coordinates in line
		const arma::uword num_coord = arma::uword(std::ceil(std::hypot(u2_ - u1_, v2_ - v1_) / dl_)) + 1;
		const arma::Row<fltp> u = arma::linspace<arma::Row<fltp> >(u1_, u2_, num_coord);
		const arma::Row<fltp> v = arma::linspace<arma::Row<fltp> >(v1_, v2_, num_coord);

		// coordinate matrix
		arma::Mat<fltp> Rl = arma::join_vert(u, v);

		// elements
		const arma::Mat<arma::uword> n = arma::join_vert(
			arma::regspace<arma::Row<arma::uword> >(0, num_coord-2),
			arma::regspace<arma::Row<arma::uword> >(1, num_coord-1));

		// find
		const arma::Mat<fltp> dRl = arma::diff(Rl, 1, 1);
		arma::Mat<fltp> D = arma::join_horiz(dRl, arma::Col<fltp>{0, 0}) + arma::join_horiz(arma::Col<fltp>{0, 0}, dRl);
		D.each_row()/=cmn::Extra::vec_norm(D);
		const arma::Mat<fltp> N = arma::flipud(D);

		// create area
		ShAreaPr area = Area::create(Rl, N, D, n, thickness_);

		// setup
		area->setup_perimeter();
		area->setup_corner_nodes();
		
		// return area
		return area;
	}

	// vallidity check
	bool CrossLine2::is_valid(const bool enable_throws) const{
		if(std::abs(u1_ - u2_) < 1e-14 && std::abs(v1_ - v2_) < 1e-14){if(enable_throws){rat_throw_line("start point can not be the same as end point");} return false;};
		if(thickness_<0){if(enable_throws){rat_throw_line("thickness must be positive");} return false;};
		if(dl_<0){if(enable_throws){rat_throw_line("element size must be positive");} return false;};
		return true;
	}

	// get type
	std::string CrossLine2::get_type(){
		return "rat::mdl::crossline2";
	}

	// method for serialization into json
	void CrossLine2::serialize(Json::Value &js, cmn::SList &list) const{
		Cross::serialize(js,list);
		js["type"] = get_type();
		js["u1"] = u1_;
		js["u2"] = u2_;
		js["v1"] = v1_;
		js["v2"] = v2_;
		js["thickness"] = thickness_;
		js["dl"] = dl_;
	}

	// method for deserialisation from json
	void CrossLine2::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list,
		const boost::filesystem::path &pth){
		Cross::deserialize(js,list, factory_list, pth);
		set_coord(js["u1"].ASFLTP(), js["v1"].ASFLTP(),
			js["u2"].ASFLTP(), js["v2"].ASFLTP(), js["dl"].ASFLTP());
		set_thickness(js["thickness"].ASFLTP());
	}

}}
