// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelsphere.hh"
#include "drivedc.hh"
#include "transrotate.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ModelSphere::ModelSphere(){
		set_name("Sphere");
	}

	// constructor
	ModelSphere::ModelSphere(const fltp radius, const fltp element_size) : ModelSphere(){
		set_radius(radius); set_element_size(element_size);
	}

	// factory
	ShModelSpherePr ModelSphere::create(){
		//return ShModelSpherePr(new ModelSphere);
		return std::make_shared<ModelSphere>();
	}

	// factory
	ShModelSpherePr ModelSphere::create(const fltp radius, const fltp element_size){
		//return ShModelSpherePr(new ModelSphere);
		return std::make_shared<ModelSphere>(radius, element_size);
	}

	// set operating temperature 
	fltp ModelSphere::get_operating_temperature() const{
		return operating_temperature_;
	}

	// set operating temperature 
	void ModelSphere::set_operating_temperature(const fltp operating_temperature){
		operating_temperature_ = operating_temperature;
	}

	// set drive
	void ModelSphere::set_temperature_drive(const ShDrivePr &temperature_drive){
		if(temperature_drive==NULL)rat_throw_line("supplied drive points to NULL");
		temperature_drive_ = temperature_drive;
	}

	// setting
	void ModelSphere::set_radius(const fltp radius){
		radius_ = radius;
	}

	void ModelSphere::set_use_tetrahedrons(const bool use_tetrahedrons){
		use_tetrahedrons_ = use_tetrahedrons;
	}

	void ModelSphere::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}
	

	// getting
	fltp ModelSphere::get_element_size()const{
		return element_size_;
	}

	fltp ModelSphere::get_radius()const{
		return radius_;
	}

	bool ModelSphere::get_use_tetrahedrons()const{
		return use_tetrahedrons_;
	}

	// set polar
	void ModelSphere::set_polar_orientation(const bool polar_orientation){
		polar_orientation_ = polar_orientation;
	}

	// create a sphere mesh
	void ModelSphere::setup_mesh(
		const ShMeshDataPr &mesh_data, 
		const MeshSettings &stngs) const{

		// allocate
		arma::Mat<fltp> Rn; arma::Mat<arma::uword> n,s;

		// create the sphere mesh for tetrahedrons
		if(use_tetrahedrons_)cmn::Tetrahedron::create_sphere(Rn,n,s,radius_,element_size_);
		
		// create the sphere mesh for tetrahedrons
		else cmn::Hexahedron::create_sphere(Rn,n,s,radius_,element_size_,core_size_);
		
		// offset the mesh
		Rn.each_col()+=R0_;

		// set to self
		mesh_data->setup(Rn,n,s);

		// cartesian
		if(!polar_orientation_){
			arma::Mat<fltp> L(Rn.n_rows,Rn.n_cols,arma::fill::zeros); L.row(0).fill(RAT_CONST(1.0));
			arma::Mat<fltp> N(Rn.n_rows,Rn.n_cols,arma::fill::zeros); N.row(1).fill(RAT_CONST(1.0));
			arma::Mat<fltp> D(Rn.n_rows,Rn.n_cols,arma::fill::zeros); D.row(2).fill(RAT_CONST(1.0));

			// set orientation
			mesh_data->set_longitudinal(L); mesh_data->set_normal(N); mesh_data->set_transverse(D); 
		}

		// polar
		else{
			// set orientation
			mesh_data->set_normal(Rn.each_row()/cmn::Extra::vec_norm(Rn));

			// do not use L, N and D
			mesh_data->set_transverse(arma::Mat<fltp>(Rn.n_rows,Rn.n_cols,arma::fill::zeros)); 
			mesh_data->set_longitudinal(arma::Mat<fltp>(Rn.n_rows,Rn.n_cols,arma::fill::zeros)); 
		}

		// determine dimensions for surface and volume mesh
		mesh_data->determine_dimensions();

		// set time
		mesh_data->set_time(stngs.time);

		// set temperature
		mesh_data->set_operating_temperature(operating_temperature_);
		mesh_data->set_temperature(operating_temperature_);

		// set name (is appended by models later)
		mesh_data->set_name(myname_);
		mesh_data->set_part_name(myname_);

		// color
		if(use_custom_color_){
			mesh_data->set_use_custom_color(use_custom_color_);
			mesh_data->set_color(color_);
		}
		
		// apply transformations to mesh
		mesh_data->apply_transformations(get_transformations(),stngs.time);

		// set material
		mesh_data->set_material(conductor_);
	}

	// factory for mesh objects
	std::list<ShMeshDataPr> ModelSphere::create_meshes(
		const std::list<arma::uword> &trace, 
		const MeshSettings &stngs)const{

		// trace
		if(!trace.empty())rat_throw_line("trace must be empty for leaf");

		// check enabled
		if(!get_enable())return{};

		// check input
		if(!is_valid(stngs.enable_throws))return{};

		// mesh data
		const ShMeshDataPr mesh_data = MeshData::create();

		// create spherical mesh
		setup_mesh(mesh_data, stngs);

		// mesh data object
		return {mesh_data};
	}

	// check validity
	bool ModelSphere::is_valid(const bool enable_throws) const{
		if(operating_temperature_<=0){if(enable_throws){rat_throw_line("operating temperature must be larger than zero");} return false;};
		if(radius_<=0){if(enable_throws){rat_throw_line("radius must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string ModelSphere::get_type(){
		return "rat::mdl::modelsphere";
	}

	// method for serialization into json
	void ModelSphere::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		Model::serialize(js,list);

		// type
		js["type"] = get_type();

		// temperature and drive
		js["temperature_drive"] = cmn::Node::serialize_node(temperature_drive_, list);
		js["operating_temperature"] = operating_temperature_;

		// geometry
		js["radius"] = radius_;
		js["element_size"] = element_size_;	
		js["core_size"] = core_size_;
		js["use_tetrahedrons"] = use_tetrahedrons_;
		js["polar_orientation"] = polar_orientation_;

		// coordinate
		js["Rx"] = R0_(0); js["Ry"] = R0_(1); js["Rz"] = R0_(2);

	}

	// method for deserialisation from json
	void ModelSphere::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent objects
		Model::deserialize(js,list,factory_list,pth);

		// temperature and drive
		set_temperature_drive(cmn::Node::deserialize_node<Drive>(
			js["temperature_drive"], list, factory_list, pth));
		set_operating_temperature(js["operating_temperature"].ASFLTP());
		
		// geometry
		radius_ = js["radius"].ASFLTP();
		element_size_ = js["element_size"].ASFLTP();
		core_size_ = js["core_size"].ASFLTP();
		use_tetrahedrons_  = js["use_tetrahedrons"].asBool(); 
		polar_orientation_ = js["polar_orientation"].asBool(); 

		// coordinate
		R0_(0) = js["Rx"].ASFLTP(); 
		R0_(1) = js["Ry"].ASFLTP(); 
		R0_(2) = js["Rz"].ASFLTP();
	}

}}

