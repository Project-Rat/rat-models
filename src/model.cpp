// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "model.hh"

#include "splitable.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// setters
	bool Model::get_use_custom_color()const{
		return use_custom_color_;
	}

	void Model::set_color(const arma::Col<float>::fixed<3> &color){
		color_ = color;
	}

	void Model::set_color(const arma::Col<float>::fixed<3> &color, const bool use_custom_color){
		set_color(color); set_use_custom_color(use_custom_color);
	}


	// getters
	void Model::set_use_custom_color(const bool use_custom_color){
		use_custom_color_ = use_custom_color;
	}

	const arma::Col<float>::fixed<3>& Model::get_color()const{
		return color_;
	}


	// export edges to freecad file
	void Model::export_freecad(const cmn::ShFreeCADPr &freecad)const{
		// check opera
		if(freecad==NULL)rat_throw_line("freecad points to NULL");

		// settings
		MeshSettings stngs;
		stngs.time = RAT_CONST(0.0);
		stngs.low_poly = false;
		stngs.combine_sections = false;
		stngs.visual_tolerance = RAT_CONST(0.0);
		stngs.visual_radius = RAT_CONST(0.0);
		stngs.is_calculation = true;

		// create edges
		// ShEdgesPrList edges = create_edge();
		const std::list<ShMeshDataPr> meshes = create_meshes({},stngs);

		// export to freecad
		for(auto it = meshes.begin();it!=meshes.end();it++)
			(*it)->export_freecad(freecad);
	}

	// export edges to opera file
	void Model::export_opera(const cmn::ShOperaPr &opera) const{
		// check opera
		if(opera==NULL)rat_throw_line("opera points to NULL");

		// settings
		MeshSettings stngs;
		stngs.time = RAT_CONST(0.0);
		stngs.low_poly = false;
		stngs.combine_sections = false;

		// create edges
		// ShEdgesPrList edges = create_edge();
		const std::list<ShMeshDataPr> meshes = create_meshes({},stngs);

		// export to opera
		for(auto it = meshes.begin();it!=meshes.end();it++)
			(*it)->export_opera(opera);
	}

	// export edges to opera file
	void Model::export_gmsh(const cmn::ShGmshFilePr &gmsh) const{
		// check opera
		if(gmsh==NULL)rat_throw_line("gmsh points to NULL");

		// settings
		MeshSettings stngs;
		stngs.time = RAT_CONST(0.0);
		stngs.low_poly = false;
		stngs.combine_sections = true;

		// create edges
		// ShEdgesPrList edges = create_edge();
		std::list<ShMeshDataPr> meshes = create_meshes({},stngs);

		// allocate
		arma::field<arma::Mat<fltp> > Rtfld(1,meshes.size());
		arma::field<arma::Mat<arma::uword> > nfld(1,meshes.size());
		arma::field<arma::Mat<arma::uword> > sfld(1,meshes.size());
		arma::field<arma::Mat<arma::uword> > nidfld(1,meshes.size());
		arma::field<arma::Mat<arma::uword> > sidfld(1,meshes.size());

		// export to opera
		arma::uword idx = 0, offset = 0;
		for(auto it = meshes.begin();it!=meshes.end();it++,idx++){
			Rtfld(idx) = (*it)->get_nodes();
			nfld(idx) = (*it)->get_elements() + offset;
			sfld(idx) = (*it)->get_surface_elements() + offset;
			nidfld(idx) = arma::Row<arma::uword>(nfld(idx).n_cols, arma::fill::value(idx+1));
			sidfld(idx) = arma::Row<arma::uword>(sfld(idx).n_cols, arma::fill::value(idx+1+meshes.size()));
			offset += Rtfld(idx).n_cols;
		}

		// write to gmsh
		gmsh->write_nodes(cmn::Extra::field2mat(Rtfld));
		gmsh->write_elements(
			cmn::Extra::field2mat(nfld),cmn::Extra::field2mat(nidfld),
			cmn::Extra::field2mat(sfld),cmn::Extra::field2mat(sidfld));
	}

	// export edges to opera file
	void Model::export_abaqus(const cmn::ShAbaqusFilePr &abaqus) const{
		// check opera
		if(abaqus==NULL)rat_throw_line("abaqus points to NULL");

		// settings
		MeshSettings stngs;
		stngs.time = RAT_CONST(0.0);
		stngs.low_poly = false;
		stngs.combine_sections = true;

		// create edges
		// ShEdgesPrList edges = create_edge();
		const std::list<ShMeshDataPr> meshes = create_meshes({},stngs);

		// export to opera
		for(auto it = meshes.begin();it!=meshes.end();it++)
			(*it)->export_abaqus(abaqus);
	}

	// export to stl file
	void Model::export_stl(const cmn::ShSTLFilePr &stlfile)const{
		// settings
		MeshSettings stngs;
		stngs.time = RAT_CONST(0.0);
		stngs.low_poly = true;
		stngs.is_calculation = false;
		stngs.combine_sections = true;
		stngs.visual_tolerance = RAT_CONST(0.0);
		stngs.visual_radius = RAT_CONST(0.0);

		// create edges
		const std::list<ShMeshDataPr> meshes = create_meshes({},stngs);

		// walk over meshes and write stl file
		for(auto it = meshes.begin();it!=meshes.end();it++)
			(*it)->create_surface()->export_stl(stlfile);
	}

	// function for calculating mesh volume
	fltp Model::calc_total_volume(const fltp time) const{
		// settings
		MeshSettings stngs;
		stngs.time = time;

		// create mesh list
		std::list<ShMeshDataPr> meshes = create_meshes({},stngs);

		// calculate volume
		fltp V = 0;
		for(auto it = meshes.begin();it!=meshes.end();it++)
			V += (*it)->calc_total_volume();

		// return volume
		return V;
	}

	// function for calculating mesh volume
	fltp Model::calc_total_surface_area(const fltp time) const{
		// settings
		MeshSettings stngs;
		stngs.time = time;

		// create mesh list
		std::list<ShMeshDataPr> meshes = create_meshes({},stngs);

		// calculate volume
		fltp A = 0;
		for(auto it = meshes.begin();it!=meshes.end();it++)
			A += (*it)->calc_total_surface_area();

		// return volume
		return A;
	}

	// create specific mesh
	std::list<ShMeshDataPr> Model::create_meshes(
		const std::list<arma::uword> &/*trace*/, 
		const MeshSettings &/*stngs*/) const{
		return {};
	}

	// validity check
	bool Model::is_valid(const bool enable_throws) const{
		if(!mat::InputConductor::is_valid(enable_throws))return false;
		if(!Transformations::is_valid(enable_throws))return false;
		return true;
	}

	// static function for taking into account directory structure
	// flattens the model tree structure recursively by using splitable
	void Model::export_freecad(
		const ShModelPr& model, 
		const cmn::ShFreeCADPr &freecad, 
		const ShSerializerPr& slzr, 
		const arma::uword depth, 
		const arma::uword max_depth){

		// check if enabled
		if(!model->get_enable())return;

		// check depth
		if(depth>max_depth)rat_throw_line("max depth limit reached");

		// cast to splitable
		const rat::mdl::ShSplitablePr base = std::dynamic_pointer_cast<rat::mdl::Splitable>(model);

		// check if splitable model
		if(base!=NULL){
			// split to sub-models
			const std::list<rat::mdl::ShModelPr> models = base->split(slzr);

			// no models
			if(models.empty())return;

			// single model
			if(models.size()==1){
				export_freecad(*models.begin(),freecad,slzr,depth+1,max_depth);
			}

			// multiple models
			else{
				// create group
				if(depth>0)freecad->append_group(model->get_name());

				// recursive call on all sub-models
				for(auto it=models.begin();it!=models.end();it++)export_freecad(*it,freecad,slzr,depth+1,max_depth);

				// pop group
				if(depth>0)freecad->pop_group();
			}
		}

		// model not splitable
		// convert leaf model into freecad file
		else model->export_freecad(freecad);
	}

	// type
	std::string Model::get_type(){
		return "rat::mdl::model";
	}

	// method for serialization into json
	void Model::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		Node::serialize(js,list);
		mat::InputConductor::serialize(js,list);
		Transformations::serialize(js,list);

		// properties
		js["use_custom_color"] = use_custom_color_;
		js["color"]["R"] = color_(0);
		js["color"]["G"] = color_(1);
		js["color"]["B"] = color_(2);
	}

	// method for deserialisation from json
	void Model::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		
		// parent objects
		Node::deserialize(js,list,factory_list,pth);
		mat::InputConductor::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);

		// properties
		set_use_custom_color(js["use_custom_color"].asBool());
		if(js.isMember("color")){
			color_(0) = js["color"]["R"].asFloat();
			color_(1) = js["color"]["G"].asFloat();
			color_(2) = js["color"]["B"].asFloat();
		}
	}

}}