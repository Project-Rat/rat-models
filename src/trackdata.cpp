// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "trackdata.hh"

// rat-common headers
#include "rat/common/error.hh"
#include "rat/common/rungekutta.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	TrackData::TrackData(){
		set_output_type("tracks");
	}

	// factory
	ShTrackDataPr TrackData::create(){
		return std::make_shared<TrackData>();
	}

	// set the integration method (see trackdata)
	void TrackData::set_integration_method(
		const cmn::RungeKutta::IntegrationMethod integration_method){
		integration_method_ = integration_method;
	}

	// set the circuits to this track data
	void TrackData::set_circuits(const std::list<ShCircuitPr>& circuits){
		circuits_ = circuits;
	}

	void TrackData::set_enable_dynamic(const bool enable_dynamic){
		enable_dynamic_ = enable_dynamic;
	}

	// getters
	const std::list<ShCircuitPr>& TrackData::get_circuits()const{
		return circuits_;
	}

	bool TrackData::get_use_start_time()const{
		return use_start_time_;
	}

	bool TrackData::get_enable_dynamic()const{
		return enable_dynamic_;
	}

	

	// calculate field
	std::pair<arma::Mat<fltp>,arma::Mat<fltp> > TrackData::calc_field(
		const fltp time,
		const arma::Mat<fltp> &Rt,
		const ShFieldMapPr &fieldmap,
		const cmn::ShLogPr& /*lg*/){

		// calculate field
		const std::pair<arma::Mat<fltp>,arma::Mat<fltp> > field_data = fieldmap->calc_fields(Rt,time,cmn::NullLog::create());

		// check
		if(!field_data.first.is_finite())rat_throw_line("electric field is not finite");
		if(!field_data.second.is_finite())rat_throw_line("flux density is not finite");

		// return field
		return field_data;
	}

	// run tracking code
	void TrackData::create_flux_line_tracks(
		const ShFieldMapPr &fieldmap,
		const std::list<ShEmitterPr> &emitters,
		const fltp max_step_size,
		const fltp rabstol, const fltp rreltol,
		const rat::cmn::ShLogPr &lg){

		// settings
		const fltp termination_flux_density = RAT_CONST(1e-3); // [T] when tracking field lines terminate the lines at this flux density (this is typically the accuracy of the MLFMM)

		// check settings
		if(rabstol<=0)rat_throw_line("rabstol can not be less than zero");
		if(rreltol<=0)rat_throw_line("rreltol can not be less than zero");

		// check if tracking mesh is setup
		if(fieldmap==NULL)rat_throw_line("tracking grid not set");

		// header
		lg->msg(2,"%s%sPARTICLE TRACING%s\n",KBLD,KGRN,KNRM);

		// get particles from emitter
		lg->msg(2,"%sspawn particles%s\n",KBLU,KNRM);
		arma::field<arma::field<Particle> > particles(emitters.size()); 
		arma::uword cnt = 0; arma::uword num_particles = 0;
		for(auto it=emitters.begin();it!=emitters.end();it++,cnt++){
			if((*it)==NULL)rat_throw_line("emitter is NULL");
			particles(cnt) = (*it)->spawn_particles(RAT_CONST(0.0));
			num_particles += particles(cnt).n_elem;
		}
		particles_.set_size(num_particles); cnt=0;
		for(arma::uword i=0;i<particles.n_elem;i++)
			for(arma::uword j=0;j<particles(i).n_elem;j++)
				particles_(cnt++) = particles(i)(j);

		lg->msg("number of particles: %05llu\n",num_particles);

		// setup runge-kutta method
		const cmn::ShRungeKuttaPr runge_kutta = cmn::RungeKutta::create(integration_method_);

		// get particle properties and store them in vectors
		arma::Mat<fltp> initial_coord(3,num_particles);
		for(arma::uword i=0;i<num_particles;i++)
			initial_coord.col(i) = particles_(i).get_initial_coord();
		const arma::Mat<fltp> initial_velocity(3,num_particles,arma::fill::zeros);

		// report
		lg->msg("track type: %sFlux Lines%s\n",KYEL,KNRM);

		// initial step size
		const fltp initial_step_size = RAT_CONST(1e-2)*max_step_size;
		use_start_time_ = true;
		lg->msg("initial time step: %s%8.2e%s [s]\n",KYEL,initial_step_size,KNRM);

		// set the system function
		runge_kutta->set_system_fun([&](const fltp /*tt*/, const arma::Mat<fltp>&yy){
			// get coordinates and velocities
			const arma::Mat<fltp> coord = yy.rows(0,2);

			// calculate field on particle positions
			const std::pair<arma::Mat<fltp>, arma::Mat<fltp> > field_data = calc_field(time_,coord,fieldmap,lg);
			const arma::Mat<fltp>& flux_density = field_data.second;
			assert(flux_density.n_cols==num_particles);

			// track field lines
			const arma::Mat<fltp> velocity = cmn::Extra::normalize(flux_density, true);

			// allocate output
			const arma::Mat<fltp> yp = velocity;

			// return the change
			return yp;
		});

		// setup done
		lg->msg(-2,"\n");

		// check cancel
		if(lg->is_cancelled())return;

		// display integration method
		runge_kutta->display_butchers_tableau(lg);

		// tracking direction
		for(arma::sword dir = -1;dir<=1;dir+=2){
			// time and initial time step
			arma::uword num_alive = num_particles;
			arma::uword num_fails = 0llu;
			fltp tt = RAT_CONST(0.0); arma::uword iter = 0llu;
			fltp dt = dir*initial_step_size;

			// set initial y
			arma::Mat<fltp> yy = initial_coord;

			// title
			if(dir==1)lg->msg(2,"%strace particles forward%s\n",KBLU,KNRM);
			else lg->msg(2,"%strace particles backward%s\n",KBLU,KNRM);
			
			// header
			lg->msg("%s%4s %9s %9s %5s %5s%s\n",KBLD,"iter","time","dstep","nfail","alive",KNRM);

			// start time stepping
			for(;;){
				// allocate output
				arma::Mat<fltp> yp1, yp2;

				// fehlberg method
				if(!runge_kutta->is_embedded()){
					// calculate one step
					yp1 = runge_kutta->perform_step(tt,dt,yy).front();

					// perform two half steps to find a more accurate result
					const arma::Mat<fltp> yp2a = runge_kutta->perform_step(tt,dt/2,yy).front();
					const arma::Mat<fltp> yp2b = runge_kutta->perform_step(tt+dt/2,dt/2,yy+yp2a/2).front();

					// add two separate step
					yp2 = yp2a + yp2b;
				}

				// embedded method
				else{
					// runge kutta
					const arma::field<arma::Mat<fltp> > ypfld = runge_kutta->perform_step(tt,dt,yy);
					assert(ypfld.n_elem==2);
					yp1 = ypfld(1); yp2 = ypfld(0);
				}

				// error in position
				const arma::uword order = runge_kutta->get_order();
				const arma::Mat<fltp> r1 = yy + yp1;
				const arma::Mat<fltp> r2 = yy + yp2;
				const fltp rerr = arma::max(rat::cmn::Extra::vec_norm(r2 - r1)/(rat::cmn::Extra::vec_norm(r2) + rabstol));
				const fltp s1 = std::pow(rreltol/rerr,RAT_CONST(1.0)/(order+1));

				// limit step size
				const fltp current_step = cmn::Extra::vec_norm(yp2.rows(0,2)).max();

				// step size scaling factor
				const fltp safety_factor = RAT_CONST(0.8);
				const fltp smin = RAT_CONST(0.1);
				const fltp smax = RAT_CONST(10.0);

				// update scaling factor
				const fltp s = std::min(safety_factor*std::max(smin,std::min(s1,smax)), max_step_size/current_step);


				// if error exceeds tolerance try again
				if(rerr>rreltol){num_fails++; dt *= s; continue;}

				// display time
				if(iter%5==0)lg->msg("%04llu %+9.2e %+9.2e %05llu %05llu\n",iter,tt,dt,num_fails,num_alive);

				// make timestep
				yy += yp2; tt += dt;

				// get coordinates and velocities
				const arma::Mat<fltp> coord = yy;
				const arma::Mat<fltp> velocity = yp2/dt;
				const arma::Row<fltp> step_size = cmn::Extra::vec_norm(yp2.rows(0,2));

				// calculate field at particle coordinates
				const std::pair<arma::Mat<fltp>, arma::Mat<fltp> > field_data = calc_field(use_start_time_ ? time_ : time_ + tt,coord,fieldmap,lg);
				const arma::Mat<fltp>& electric_field = field_data.first; 
				const arma::Mat<fltp>& flux_density = field_data.second;
				assert(electric_field.n_cols==num_particles); assert(flux_density.n_cols==num_particles);

				// check cancel
				if(lg->is_cancelled())break;

				// check which coordinates are still within the fieldmap
				arma::Row<arma::uword> is_inside = fieldmap->is_inside(coord);

				// terminate tracks based on field criterion
				is_inside = is_inside && cmn::Extra::vec_norm(flux_density)>termination_flux_density;

				// // mark velocities of terminated particles to zero so that they
				// // no longer move around
				// for(arma::uword i=0;i<num_particles;i++)
				// 	if(!is_inside(i))yy.submat(3,i,5,i).fill(0.0);

				// adapt stepsize for next step
				dt *= s;

				// store to particles
				num_alive = 0llu;
				for(arma::uword i=0;i<num_particles;i++){
					// check alive
					const bool alive = dir==1 ? particles_(i).is_alive_end() : particles_(i).is_alive_start();
					num_alive += arma::uword(alive);
					if(!alive)continue;

					// check if particle is inside tracking grid
					if(!is_inside(i) || step_size(i)==RAT_CONST(0.0))dir==1 ? particles_(i).terminate_end() : particles_(i).terminate_start();
					else dir==1 ? 
						particles_(i).set_next_coord(tt,coord.col(i),velocity.col(i),flux_density.col(i),electric_field.col(i)) : 
						particles_(i).set_prev_coord(tt,coord.col(i),velocity.col(i),flux_density.col(i),electric_field.col(i));
				}

				// stop conditions
				if(num_alive==0)break;

				// check cancel
				if(lg->is_cancelled())break;

				// increment iter
				iter++;
			}

			// check cancel
			if(lg->is_cancelled())break;

			// done
			lg->msg("%s= tracing complete%s\n",KCYN,KNRM);
			lg->msg(-2,"\n");
		}

		// reduce memory footprint
		for(arma::uword i=0;i<particles_.n_elem;i++)
			particles_(i).truncate();

		// end
		lg->msg(-2);
	}

	// run tracking code
	void TrackData::create_electric_field_tracks(
		const ShFieldMapPr &fieldmap,
		const std::list<ShEmitterPr> &emitters,
		const fltp max_step_size,
		const fltp rabstol, const fltp rreltol,
		const rat::cmn::ShLogPr &lg){

		// settings
		const fltp termination_electric_field = RAT_CONST(1e-9); // V/m

		// check settings
		if(rabstol<=0)rat_throw_line("rabstol can not be less than zero");
		if(rreltol<=0)rat_throw_line("rreltol can not be less than zero");
		
		// check if tracking mesh is setup
		if(fieldmap==NULL)rat_throw_line("tracking grid not set");

		// header
		lg->msg(2,"%s%sPARTICLE TRACING%s\n",KBLD,KGRN,KNRM);

		// get particles from emitter
		lg->msg(2,"%sspawn particles%s\n",KBLU,KNRM);
		arma::field<arma::field<Particle> > particles(emitters.size()); 
		arma::uword cnt = 0; arma::uword num_particles = 0;
		for(auto it=emitters.begin();it!=emitters.end();it++,cnt++){
			if((*it)==NULL)rat_throw_line("emitter is NULL");
			particles(cnt) = (*it)->spawn_particles(RAT_CONST(0.0));
			num_particles += particles(cnt).n_elem;
		}
		particles_.set_size(num_particles); cnt=0;
		for(arma::uword i=0;i<particles.n_elem;i++)
			for(arma::uword j=0;j<particles(i).n_elem;j++)
				particles_(cnt++) = particles(i)(j);

		lg->msg("number of particles: %05llu\n",num_particles);

		// setup runge-kutta method
		const cmn::ShRungeKuttaPr runge_kutta = cmn::RungeKutta::create(integration_method_);

		// get particle properties and store them in vectors
		arma::Mat<fltp> initial_coord(3,num_particles);
		for(arma::uword i=0;i<num_particles;i++)
			initial_coord.col(i) = particles_(i).get_initial_coord();
		arma::Mat<fltp> initial_velocity(3,num_particles,arma::fill::zeros);

		// report
		lg->msg("track type: %sElectric Field Lines%s\n",KYEL,KNRM);

		// initial step size
		const fltp initial_step_size = RAT_CONST(1e-2)*max_step_size;
		use_start_time_ = true;
		lg->msg("initial time step: %s%8.2e%s [s]\n",KYEL,initial_step_size,KNRM);
		initial_velocity.zeros();

		// set the system function
		runge_kutta->set_system_fun([&](const fltp /*tt*/, const arma::Mat<fltp>&yy){
			// get coordinates and velocities
			const arma::Mat<fltp> coord = yy.rows(0,2);

			// calculate field on particle positions
			const std::pair<arma::Mat<fltp>, arma::Mat<fltp> > field_data = calc_field(time_,coord,fieldmap,lg);
			const arma::Mat<fltp>& electric_field = field_data.first;

			// track field lines
			const arma::Mat<fltp> velocity = cmn::Extra::normalize(electric_field, true);

			// allocate output
			const arma::Mat<fltp> yp = velocity;

			// return the change
			return yp;
		});

		// setup done
		lg->msg(-2,"\n");

		// check cancel
		if(lg->is_cancelled())return;

		// display integration method
		runge_kutta->display_butchers_tableau(lg);

		// tracking direction
		for(arma::sword dir = -1;dir<=1;dir+=2){
			// time and initial time step
			arma::uword num_alive = num_particles;
			arma::uword num_fails = 0llu;
			fltp tt = RAT_CONST(0.0); arma::uword iter = 0llu;
			fltp dt = dir*initial_step_size;

			// set initial y
			arma::Mat<fltp> yy = initial_coord;

			// title
			if(dir==1)lg->msg(2,"%strace particles forward%s\n",KBLU,KNRM);
			else lg->msg(2,"%strace particles backward%s\n",KBLU,KNRM);
			
			// header
			lg->msg("%s%4s %9s %9s %5s %5s%s\n",KBLD,"iter","time","dstep","nfail","alive",KNRM);

			// start time stepping
			for(;;){
				// allocate output
				arma::Mat<fltp> yp1, yp2;

				// fehlberg method
				if(!runge_kutta->is_embedded()){
					// calculate one step
					yp1 = runge_kutta->perform_step(tt,dt,yy).front();

					// perform two half steps to find a more accurate result
					const arma::Mat<fltp> yp2a = runge_kutta->perform_step(tt,dt/2,yy).front();
					const arma::Mat<fltp> yp2b = runge_kutta->perform_step(tt+dt/2,dt/2,yy+yp2a/2).front();

					// add two separate step
					yp2 = yp2a + yp2b;
				}

				// embedded method
				else{
					// runge kutta
					const arma::field<arma::Mat<fltp> > ypfld = runge_kutta->perform_step(tt,dt,yy);
					assert(ypfld.n_elem==2);
					yp1 = ypfld(1); yp2 = ypfld(0);
				}

				// error in position
				const arma::uword order = runge_kutta->get_order();
				const arma::Mat<fltp> r1 = yy + yp1;
				const arma::Mat<fltp> r2 = yy + yp2;
				const fltp rerr = arma::max(rat::cmn::Extra::vec_norm(r2 - r1)/(rat::cmn::Extra::vec_norm(r2) + rabstol));
				const fltp s1 = std::pow(rreltol/rerr,RAT_CONST(1.0)/(order+1));

				// limit step size
				const fltp current_step = cmn::Extra::vec_norm(yp2.rows(0,2)).max();

				// step size scaling factor
				const fltp safety_factor = RAT_CONST(0.8);
				const fltp smin = RAT_CONST(0.1);
				const fltp smax = RAT_CONST(10.0);

				// update scaling factor
				const fltp s = std::min(safety_factor*std::max(smin,std::min(s1,smax)), max_step_size/current_step);

				// if error exceeds tolerance try again
				if(rerr>rreltol){num_fails++; dt *= s; continue;}

				// display time
				if(iter%5==0)lg->msg("%04llu %+9.2e %+9.2e %05llu %05llu\n",iter,tt,dt,num_fails,num_alive);

				// make timestep
				yy += yp2; tt += dt;

				// get coordinates and velocities
				const arma::Mat<fltp> coord = yy.rows(0,2);
				const arma::Mat<fltp> velocity = yp2.rows(0,2)/dt;
				const arma::Row<fltp> step_size = cmn::Extra::vec_norm(yp2.rows(0,2));

				// calculate field at particle coordinates
				const std::pair<arma::Mat<fltp>, arma::Mat<fltp> > field_data = calc_field(use_start_time_ ? time_ : time_ + tt,coord,fieldmap,lg);
				const arma::Mat<fltp>& electric_field = field_data.first; 
				const arma::Mat<fltp>& flux_density = field_data.second;
				assert(electric_field.n_cols==num_particles); assert(flux_density.n_cols==num_particles);

				// check cancel
				if(lg->is_cancelled())break;

				// check which coordinates are still within the fieldmap
				arma::Row<arma::uword> is_inside = fieldmap->is_inside(coord);
				
				// terminate tracks based on field criterion
				is_inside = is_inside && cmn::Extra::vec_norm(electric_field)>termination_electric_field;

				// adapt stepsize for next step
				dt *= s;

				// store to particles
				num_alive = 0llu;
				for(arma::uword i=0;i<num_particles;i++){
					// check alive
					const bool alive = dir==1 ? particles_(i).is_alive_end() : particles_(i).is_alive_start();
					num_alive += arma::uword(alive);
					if(!alive)continue;

					// check if particle is inside tracking grid
					if(!is_inside(i) || step_size(i)==RAT_CONST(0.0))dir==1 ? particles_(i).terminate_end() : particles_(i).terminate_start();
					else dir==1 ? 
						particles_(i).set_next_coord(tt,coord.col(i),velocity.col(i),flux_density.col(i),electric_field.col(i)) : 
						particles_(i).set_prev_coord(tt,coord.col(i),velocity.col(i),flux_density.col(i),electric_field.col(i));
				}

				// stop conditions
				if(num_alive==0)break;

				// check cancel
				if(lg->is_cancelled())break;

				// increment iter
				iter++;
			}

			// check cancel
			if(lg->is_cancelled())break;

			// done
			lg->msg("%s= tracing complete%s\n",KCYN,KNRM);
			lg->msg(-2,"\n");
		}

		// reduce memory footprint
		for(arma::uword i=0;i<particles_.n_elem;i++)
			particles_(i).truncate();

		// end
		lg->msg(-2);
	}

	// run tracking code
	void TrackData::create_newton_tracks(
		const ShFieldMapPr &fieldmap,
		const std::list<ShEmitterPr> &emitters,
		const fltp max_step_size,
		const fltp rabstol, const fltp rreltol,
		const fltp pabstol, const fltp preltol,
		const rat::cmn::ShLogPr &lg){

		// check settings
		if(rabstol<=0)rat_throw_line("rabstol can not be less than zero");
		if(rreltol<=0)rat_throw_line("rreltol can not be less than zero");
		if(pabstol<=0)rat_throw_line("pabstol can not be less than zero");
		if(preltol<=0)rat_throw_line("preltol can not be less than zero");

		// check if tracking mesh is setup
		if(fieldmap==NULL)rat_throw_line("tracking grid not set");

		// header
		lg->msg(2,"%s%sPARTICLE TRACING%s\n",KBLD,KGRN,KNRM);

		// get particles from emitter
		lg->msg(2,"%sspawn particles%s\n",KBLU,KNRM);
		arma::field<arma::field<Particle> > particles(emitters.size()); 
		arma::uword cnt = 0; arma::uword num_particles = 0;
		for(auto it=emitters.begin();it!=emitters.end();it++,cnt++){
			if((*it)==NULL)rat_throw_line("emitter is NULL");
			particles(cnt) = (*it)->spawn_particles(RAT_CONST(0.0));
			num_particles += particles(cnt).n_elem;
		}
		particles_.set_size(num_particles); cnt=0;
		for(arma::uword i=0;i<particles.n_elem;i++)
			for(arma::uword j=0;j<particles(i).n_elem;j++)
				particles_(cnt++) = particles(i)(j);

		lg->msg("number of particles: %05llu\n",num_particles);

		// setup runge-kutta method
		const cmn::ShRungeKuttaPr runge_kutta = cmn::RungeKutta::create(integration_method_);

		// get particle properties and store them in vectors
		arma::Row<fltp> elementary_charge(num_particles);
		arma::Row<fltp> rest_mass_gev(num_particles);
		arma::Mat<fltp> initial_coord(3,num_particles);
		arma::Mat<fltp> initial_velocity(3,num_particles);
		for(arma::uword i=0;i<num_particles;i++){
			elementary_charge(i) = particles_(i).get_charge();
			rest_mass_gev(i) = particles_(i).get_rest_mass();
			initial_coord.col(i) = particles_(i).get_initial_coord();
			initial_velocity.col(i) = particles_(i).get_initial_velocity()*arma::Datum<fltp>::c_0; // converted to [m/s]
		}

		// calculate rest mass [kg]
		const arma::Row<fltp> rest_mass = rest_mass_gev*RAT_CONST(1e9)*arma::Datum<fltp>::eV/(arma::Datum<fltp>::c_0*arma::Datum<fltp>::c_0);

		// calculate particle charge in [C]
		const arma::Row<fltp> charge_coulomb = elementary_charge*arma::Datum<fltp>::ec;

		// calculate initial impulse
		const arma::Mat<fltp> initial_momentum = rest_mass%initial_velocity.each_row();

		// report
		lg->msg("track type: %sNewtonian Particles%s\n",KYEL,KNRM);

		// initial step size
		const fltp initial_step_size = RAT_CONST(1e-2)*max_step_size/cmn::Extra::vec_norm(initial_velocity).max();
		lg->msg("initial time step: %s%8.2e%s [s]\n",KYEL,initial_step_size,KNRM);

		// time settings
		use_start_time_ = false;

		// set the system function
		runge_kutta->set_system_fun([&](const fltp tt, const arma::Mat<fltp>&yy){
			// get coordinates and velocities
			const arma::Mat<fltp> coord = yy.rows(0,2);
			const arma::Mat<fltp> momentum = yy.rows(3,5);
			const arma::Mat<fltp> velocity = momentum.each_row()/rest_mass;

			// calculate field on particle positions
			const std::pair<arma::Mat<fltp>, arma::Mat<fltp> > field_data = calc_field(time_ + tt,coord,fieldmap,lg);
			const arma::Mat<fltp>& electric_field = field_data.first; 
			const arma::Mat<fltp>& flux_density = field_data.second;
			assert(electric_field.n_cols==num_particles); assert(flux_density.n_cols==num_particles);

			// calculate force 
			const arma::Mat<fltp> lorentz_force = (charge_coulomb%cmn::Extra::cross(velocity, flux_density).each_row()) + (charge_coulomb%electric_field.each_row());

			// allocate output
			const arma::Mat<fltp> yp = arma::join_vert(velocity,lorentz_force);

			// return the change
			return yp;
		});
		

		// setup done
		lg->msg(-2,"\n");

		// check cancel
		if(lg->is_cancelled())return;

		// display integration method
		runge_kutta->display_butchers_tableau(lg);

		// check if step size set
		assert(initial_step_size!=RAT_CONST(0.0));

		// tracking direction
		for(arma::sword dir = -1;dir<=1;dir+=2){
			// time and initial time step
			arma::uword num_alive = num_particles;
			arma::uword num_fails = 0llu;
			fltp tt = RAT_CONST(0.0); arma::uword iter = 0llu;
			fltp dt = dir*initial_step_size;

			// set initial y
			arma::Mat<fltp> yy = arma::join_vert(initial_coord, initial_momentum);

			// title
			if(dir==1)lg->msg(2,"%strace particles forward%s\n",KBLU,KNRM);
			else lg->msg(2,"%strace particles backward%s\n",KBLU,KNRM);
			
			// header
			lg->msg("%s%4s %9s %9s %5s %5s%s\n",KBLD,"iter","time","dstep","nfail","alive",KNRM);

			// start time stepping
			for(;;){
				// allocate output
				arma::Mat<fltp> yp1, yp2;

				// fehlberg method
				if(!runge_kutta->is_embedded()){
					// calculate one step
					yp1 = runge_kutta->perform_step(tt,dt,yy).front();

					// perform two half steps to find a more accurate result
					const arma::Mat<fltp> yp2a = runge_kutta->perform_step(tt,dt/2,yy).front();
					const arma::Mat<fltp> yp2b = runge_kutta->perform_step(tt+dt/2,dt/2,yy+yp2a/2).front();

					// add two separate step
					yp2 = yp2a + yp2b;
				}

				// embedded method
				else{
					// runge kutta
					const arma::field<arma::Mat<fltp> > ypfld = runge_kutta->perform_step(tt,dt,yy);
					assert(ypfld.n_elem==2);
					yp1 = ypfld(1); yp2 = ypfld(0);
				}

				// error in position
				const arma::uword order = runge_kutta->get_order();
				const arma::Mat<fltp> r1 = yy.rows(0,2) + yp1.rows(0,2);
				const arma::Mat<fltp> r2 = yy.rows(0,2) + yp2.rows(0,2);
				const fltp rerr = arma::max(rat::cmn::Extra::vec_norm(r2 - r1)/(rat::cmn::Extra::vec_norm(r2) + rabstol));
				const fltp s1 = std::pow(rreltol/rerr,RAT_CONST(1.0)/(order+1));

				// error in momentum
				const arma::Mat<fltp> p1 = yy.rows(3,5) + yp1.rows(3,5);
				const arma::Mat<fltp> p2 = yy.rows(3,5) + yp2.rows(3,5);
				const fltp pabstol_si = pabstol*arma::Datum<fltp>::eV/arma::Datum<fltp>::c_0;
				const fltp perr = arma::max(cmn::Extra::vec_norm(p2 - p1)/(cmn::Extra::vec_norm(p1) + pabstol_si));
				const fltp s2 = std::pow(preltol/perr,RAT_CONST(1.0)/(order+1));

				// limit step size
				const fltp current_step = cmn::Extra::vec_norm(yp2.rows(0,2)).max();

				// step size scaling factor
				const fltp safety_factor = RAT_CONST(0.8);
				const fltp smin = RAT_CONST(0.1);
				const fltp smax = RAT_CONST(10.0);

				// update scaling factor
				const fltp s = std::min(safety_factor*std::max(smin,std::min(std::min(s1,s2),smax)), max_step_size/current_step);

				// if error exceeds tolerance try again
				if(rerr>rreltol || perr>preltol){num_fails++; dt *= s; continue;}

				// display time
				if(iter%5==0)lg->msg("%04llu %+9.2e %+9.2e %05llu %05llu\n",iter,tt,dt,num_fails,num_alive);

				// make timestep
				yy += yp2; tt += dt;

				// get coordinates and velocities
				const arma::Mat<fltp> coord = yy.rows(0,2);
				const arma::Mat<fltp> momentum = yy.rows(3,5);
				const arma::Mat<fltp> velocity = momentum.each_row()/rest_mass;

				// calculate field at particle coordinates
				const std::pair<arma::Mat<fltp>, arma::Mat<fltp> > field_data = calc_field(use_start_time_ ? time_ : time_ + tt,coord,fieldmap,lg);
				const arma::Mat<fltp>& electric_field = field_data.first; 
				const arma::Mat<fltp>& flux_density = field_data.second;
				assert(electric_field.n_cols==num_particles); assert(flux_density.n_cols==num_particles);

				// check cancel
				if(lg->is_cancelled())break;

				// check which coordinates are still within the fieldmap
				arma::Row<arma::uword> is_inside = fieldmap->is_inside(coord);
				
				// terminate tracks based on field criterion
				is_inside = is_inside && (cmn::Extra::vec_norm(flux_density)!=RAT_CONST(0.0) || cmn::Extra::vec_norm(electric_field)!=RAT_CONST(0.0));

				// mark velocities of terminated particles to zero so that they
				// no longer move around
				for(arma::uword i=0;i<num_particles;i++)
					if(!is_inside(i))yy.submat(3,i,5,i).fill(0.0);

				// adapt stepsize for next step
				dt *= s;

				// store to particles
				num_alive = 0llu;
				for(arma::uword i=0;i<num_particles;i++){
					// check alive
					const bool alive = dir==1 ? particles_(i).is_alive_end() : particles_(i).is_alive_start();
					num_alive += arma::uword(alive);
					if(!alive)continue;

					// check if particle is inside tracking grid
					if(!is_inside(i))
						dir==1 ? particles_(i).terminate_end() : particles_(i).terminate_start();
					else dir==1 ? 
						particles_(i).set_next_coord(tt,coord.col(i),velocity.col(i),flux_density.col(i),electric_field.col(i)) : 
						particles_(i).set_prev_coord(tt,coord.col(i),velocity.col(i),flux_density.col(i),electric_field.col(i));
				}

				// stop conditions
				if(num_alive==0)break;

				// check cancel
				if(lg->is_cancelled())break;

				// increment iter
				iter++;
			}

			// check cancel
			if(lg->is_cancelled())break;

			// done
			lg->msg("%s= tracing complete%s\n",KCYN,KNRM);
			lg->msg(-2,"\n");
		}

		// reduce memory footprint
		for(arma::uword i=0;i<particles_.n_elem;i++)
			particles_(i).truncate();

		// end
		lg->msg(-2);
	}


	// run tracking code
	void TrackData::create_relativistic_tracks(
		const ShFieldMapPr &fieldmap,
		const std::list<ShEmitterPr> &emitters,
		const fltp max_step_size,
		const fltp rabstol, const fltp rreltol,
		const fltp pabstol, const fltp preltol,
		const rat::cmn::ShLogPr &lg){

		// check settings
		if(rabstol<=0)rat_throw_line("rabstol can not be less than zero");
		if(rreltol<=0)rat_throw_line("rreltol can not be less than zero");
		if(pabstol<=0)rat_throw_line("pabstol can not be less than zero");
		if(preltol<=0)rat_throw_line("preltol can not be less than zero");

		// check if tracking mesh is setup
		if(fieldmap==NULL)rat_throw_line("tracking grid not set");

		// header
		lg->msg(2,"%s%sPARTICLE TRACING%s\n",KBLD,KGRN,KNRM);

		// get particles from emitter
		lg->msg(2,"%sspawn particles%s\n",KBLU,KNRM);
		arma::field<arma::field<Particle> > particles(emitters.size()); 
		arma::uword cnt = 0; arma::uword num_particles = 0;
		for(auto it=emitters.begin();it!=emitters.end();it++,cnt++){
			if((*it)==NULL)rat_throw_line("emitter is NULL");
			particles(cnt) = (*it)->spawn_particles(RAT_CONST(0.0));
			num_particles += particles(cnt).n_elem;
		}
		particles_.set_size(num_particles); cnt=0;
		for(arma::uword i=0;i<particles.n_elem;i++)
			for(arma::uword j=0;j<particles(i).n_elem;j++)
				particles_(cnt++) = particles(i)(j);

		lg->msg("number of particles: %05llu\n",num_particles);

		// setup runge-kutta method
		const cmn::ShRungeKuttaPr runge_kutta = cmn::RungeKutta::create(integration_method_);

		// get particle properties and store them in vectors
		arma::Row<fltp> elementary_charge(num_particles);
		arma::Row<fltp> rest_mass_gev(num_particles);
		arma::Mat<fltp> initial_coord(3,num_particles);
		arma::Mat<fltp> initial_velocity(3,num_particles);
		for(arma::uword i=0;i<num_particles;i++){
			elementary_charge(i) = particles_(i).get_charge();
			rest_mass_gev(i) = particles_(i).get_rest_mass();
			initial_coord.col(i) = particles_(i).get_initial_coord();
			initial_velocity.col(i) = particles_(i).get_initial_velocity()*arma::Datum<fltp>::c_0; // converted to [m/s]
		}

		// calculate rest mass [kg]
		const arma::Row<fltp> rest_mass_kg = rest_mass_gev*RAT_CONST(1e9)*arma::Datum<fltp>::eV/(arma::Datum<fltp>::c_0*arma::Datum<fltp>::c_0);

		// calculate particle charge in [C]
		const arma::Row<fltp> charge_coulomb = elementary_charge*arma::Datum<fltp>::ec;

		// initial momentum [kg m/s]
		const arma::Row<fltp> initial_gamma = RAT_CONST(1.0)/arma::sqrt(RAT_CONST(1.0) - arma::square(cmn::Extra::vec_norm(initial_velocity)/arma::Datum<fltp>::c_0));
		const arma::Mat<fltp> initial_momentum = (initial_gamma%rest_mass_kg)%initial_velocity.each_row();

		// report
		lg->msg("track type: %sRelativistic Particles%s\n",KYEL,KNRM);

		// mark all particles relativistic
		for(arma::uword i=0;i<num_particles;i++)particles_(i).set_is_relativistic();

		// initial step size
		const fltp initial_step_size = RAT_CONST(1e-2)*max_step_size/cmn::Extra::vec_norm(initial_velocity).max();
		lg->msg("initial time step: %s%8.2e%s [s]\n",KYEL,initial_step_size,KNRM);

		// time settings
		use_start_time_ = false;

		// set the system function
		runge_kutta->set_system_fun([&](const fltp tt, const arma::Mat<fltp>&yy){
			// get coordinates and momentum
			const arma::Mat<fltp> coord = yy.rows(0,2);
			const arma::Mat<fltp> momentum = yy.rows(3,5);

			// calculate Lorentz contraction factor
			const arma::Row<fltp> gamma = arma::sqrt(RAT_CONST(1.0) + arma::square(cmn::Extra::vec_norm(momentum)/(rest_mass_kg*arma::Datum<fltp>::c_0)));

			// calculate relativistic mass [kg]
			const arma::Row<fltp> relativistic_mass = gamma%rest_mass_kg;

			// calculate velocity [m/s]
			const arma::Mat<fltp> velocity = momentum.each_row()/relativistic_mass;

			// // check input
			// assert(coord.is_finite()); assert(velocity.is_finite());

			// // clamp velocity to prevent particles faster than lightspeed
			// const arma::Col<arma::uword> idx = arma::find(cmn::Extra::vec_norm(velocity)>0);
			// velocity.cols(idx) = arma::clamp(cmn::Extra::vec_norm(velocity.cols(idx)),
			// 	RAT_CONST(0.0),RAT_CONST(0.99999999999999)*arma::Datum<fltp>::c_0)%
			// 	cmn::Extra::normalize(velocity.cols(idx)).each_row();

			// calculate velocity magnitude
			const arma::Row<fltp> vmag = cmn::Extra::vec_norm(velocity);

			// check velocity
			if(arma::any(vmag>arma::Datum<fltp>::c_0))rat_throw_line("particle is going faster than the speed of light");

			// calculate field on particle positions
			const std::pair<arma::Mat<fltp>, arma::Mat<fltp> > field_data = calc_field(time_ + tt,coord,fieldmap,lg);
			const arma::Mat<fltp>& electric_field = field_data.first; 
			const arma::Mat<fltp>& flux_density = field_data.second;
			assert(electric_field.n_cols==num_particles); assert(flux_density.n_cols==num_particles);

			// calculate force 
			const arma::Mat<fltp> lorentz_force = (charge_coulomb%cmn::Extra::cross(velocity, flux_density).each_row()) + (charge_coulomb%electric_field.each_row());

			// allocate output
			const arma::Mat<fltp> yp = arma::join_vert(velocity,lorentz_force);

			// check output
			assert(yp.is_finite());

			// return the change
			return yp;
		});
	
		// setup done
		lg->msg(-2,"\n");

		// check cancel
		if(lg->is_cancelled())return;

		// display integration method
		runge_kutta->display_butchers_tableau(lg);

		// check if step size set
		assert(initial_step_size!=RAT_CONST(0.0));

		// tracking direction
		for(arma::sword dir = -1;dir<=1;dir+=2){
			// time and initial time step
			arma::uword num_alive = num_particles;
			arma::uword num_fails = 0llu;
			fltp tt = RAT_CONST(0.0); arma::uword iter = 0llu;
			fltp dt = dir*initial_step_size;

			// set initial y
			arma::Mat<fltp> yy = arma::join_vert(initial_coord, initial_momentum);

			// title
			if(dir==1)lg->msg(2,"%strace particles forward%s\n",KBLU,KNRM);
			else lg->msg(2,"%strace particles backward%s\n",KBLU,KNRM);
			
			// header
			lg->msg("%s%4s %9s %9s %5s %5s%s\n",KBLD,"iter","time","dstep","nfail","alive",KNRM);

			// start time stepping
			for(;;){
				// allocate output
				arma::Mat<fltp> yp1, yp2;

				// fehlberg method
				if(!runge_kutta->is_embedded()){
					// calculate one step
					yp1 = runge_kutta->perform_step(tt,dt,yy).front();

					// perform two half steps to find a more accurate result
					const arma::Mat<fltp> yp2a = runge_kutta->perform_step(tt,dt/2,yy).front();
					const arma::Mat<fltp> yp2b = runge_kutta->perform_step(tt+dt/2,dt/2,yy+yp2a/2).front();

					// add two separate step
					yp2 = yp2a + yp2b;
				}

				// embedded method
				else{
					// runge kutta
					const arma::field<arma::Mat<fltp> > ypfld = runge_kutta->perform_step(tt,dt,yy);
					assert(ypfld.n_elem==2);
					yp1 = ypfld(1); yp2 = ypfld(0);
				}

				// error in position
				const arma::uword order = runge_kutta->get_order();
				const arma::Mat<fltp> r1 = yy.rows(0,2) + yp1.rows(0,2);
				const arma::Mat<fltp> r2 = yy.rows(0,2) + yp2.rows(0,2);
				const fltp rerr = arma::max(rat::cmn::Extra::vec_norm(r2 - r1)/(rat::cmn::Extra::vec_norm(r2) + rabstol));
				const fltp s1 = std::pow(rreltol/rerr,RAT_CONST(1.0)/(order+1));

				// error in momentum
				const arma::Mat<fltp> p1 = yy.rows(3,5) + yp1.rows(3,5);
				const arma::Mat<fltp> p2 = yy.rows(3,5) + yp2.rows(3,5);
				const fltp pabstol_si = pabstol*arma::Datum<fltp>::eV/arma::Datum<fltp>::c_0; // kg m/s
				const fltp perr = arma::max(cmn::Extra::vec_norm(p2 - p1)/(cmn::Extra::vec_norm(p1) + pabstol_si));
				const fltp s2 = std::pow(preltol/perr,RAT_CONST(1.0)/(order+1));

				// limit step size
				const fltp current_step = cmn::Extra::vec_norm(yp2.rows(0,2)).max();

				// step size scaling factor
				const fltp safety_factor = RAT_CONST(0.8);
				const fltp smin = RAT_CONST(0.1);
				const fltp smax = RAT_CONST(10.0);

				// update scaling factor
				const fltp s = std::min(safety_factor*std::max(smin,std::min(std::min(s1,s2),smax)), max_step_size/current_step);

				// if error exceeds tolerance try again
				if(rerr>rreltol || perr>preltol){num_fails++; dt *= s; continue;}

				// display time
				if(iter%5==0)lg->msg("%04llu %+9.2e %+9.2e %05llu %05llu\n",iter,tt,dt,num_fails,num_alive);

				// make timestep
				yy += yp2; tt += dt;

				// get coordinates and velocities
				const arma::Mat<fltp> coord = yy.rows(0,2);
				const arma::Mat<fltp> momentum = yy.rows(3,5);

				// calculate Lorentz contraction factor
				const arma::Row<fltp> gamma = arma::sqrt(RAT_CONST(1.0) + arma::square(cmn::Extra::vec_norm(momentum)/(rest_mass_kg*arma::Datum<fltp>::c_0)));

				// calculate relativistic mass [kg]
				const arma::Row<fltp> relativistic_mass = gamma%rest_mass_kg;

				// calculate velocity
				const arma::Mat<fltp> velocity = momentum.each_row()/relativistic_mass;

				// check
				if(arma::any(cmn::Extra::vec_norm(velocity)>arma::Datum<fltp>::c_0))rat_throw_line("particle is going faster than the speed of light");

				// calculate field at particle coordinates
				const std::pair<arma::Mat<fltp>, arma::Mat<fltp> > field_data = calc_field(use_start_time_ ? time_ : time_ + tt,coord,fieldmap,lg);
				const arma::Mat<fltp>& electric_field = field_data.first; 
				const arma::Mat<fltp>& flux_density = field_data.second;
				assert(electric_field.n_cols==num_particles); assert(flux_density.n_cols==num_particles);

				// check cancel
				if(lg->is_cancelled())break;

				// check which coordinates are still within the fieldmap
				arma::Row<arma::uword> is_inside = fieldmap->is_inside(coord);
				
				// terminate tracks based on field criterion
				is_inside = is_inside && (
					cmn::Extra::vec_norm(flux_density)!=RAT_CONST(0.0) || 
					cmn::Extra::vec_norm(electric_field)!=RAT_CONST(0.0));

				// mark velocities of terminated particles to zero so that they
				// no longer move around
				for(arma::uword i=0;i<num_particles;i++)
					if(!is_inside(i))yy.submat(3,i,5,i).fill(0.0);

				// adapt stepsize for next step
				dt *= s;

				// store to particles
				num_alive = 0llu;
				for(arma::uword i=0;i<num_particles;i++){
					// check alive
					const bool alive = dir==1 ? particles_(i).is_alive_end() : particles_(i).is_alive_start();
					num_alive += arma::uword(alive);
					if(!alive)continue;

					// check if particle is inside tracking grid
					if(!is_inside(i))dir==1 ? particles_(i).terminate_end() : particles_(i).terminate_start();
					else dir==1 ? 
						particles_(i).set_next_coord(tt,coord.col(i),velocity.col(i),flux_density.col(i),electric_field.col(i)) : 
						particles_(i).set_prev_coord(tt,coord.col(i),velocity.col(i),flux_density.col(i),electric_field.col(i));
				}

				// stop conditions
				if(num_alive==0)break;

				// check cancel
				if(lg->is_cancelled())break;

				// increment iter
				iter++;
			}

			// check cancel
			if(lg->is_cancelled())break;

			// done
			lg->msg("%s= tracing complete%s\n",KCYN,KNRM);
			lg->msg(-2,"\n");
		}

		// reduce memory footprint
		for(arma::uword i=0;i<particles_.n_elem;i++)
			particles_(i).truncate();

		// end
		lg->msg(-2);
	}

	// get a particle from the list
	const Particle& TrackData::get_particle(const arma::uword idx) const{
		return particles_(idx);
	}

	// get particle list
	const arma::field<Particle>& TrackData::get_particles() const{
		return particles_;
	}

	// get number of stored particles
	arma::uword TrackData::get_num_particles()const{
		return particles_.n_elem;
	}

	// write particle tracks to VTK file
	ShVTKObjPr TrackData::export_vtk() const{
		// create VTK data object
		ShVTKUnstrPr vtk_data = VTKUnstr::create();

		// get number of particles
		const arma::uword num_particles = particles_.n_elem;

		// count number of values for each particle
		arma::Row<arma::uword> track_length(num_particles);
		for(arma::uword i=0;i<num_particles;i++)
			track_length(i) = particles_(i).get_num_coords();

		// calculate indices
		const arma::Row<arma::uword> track_idx = arma::join_horiz(
			arma::Row<arma::uword>{0}, arma::cumsum(track_length));
		const arma::uword num_coord = track_idx.back();

		// allocate
		arma::Mat<fltp> coordinate(3,num_coord);
		arma::Mat<fltp> velocity(3,num_coord);
		arma::Mat<fltp> flux_density(3,num_coord);
		arma::Mat<fltp> electric_field(3,num_coord);
		arma::Mat<fltp> momentum_ev(3,num_coord);
		arma::Mat<fltp> curvature(3,num_coord);
		arma::Row<fltp> angle_start(num_coord);
		arma::Row<fltp> angle_end(num_coord);
		arma::Row<fltp> temperature(num_coord);
		

		// get data
		for(arma::uword i=0;i<num_particles;i++){
			const arma::uword idx1 = track_idx(i);
			const arma::uword idx2 = track_idx(i+1)-1;
			coordinate.cols(idx1,idx2) = particles_(i).get_track_coords();
			velocity.cols(idx1,idx2) = particles_(i).get_track_velocities();
			flux_density.cols(idx1,idx2) = particles_(i).get_flux_density();
			electric_field.cols(idx1,idx2) = particles_(i).get_electric_field();
			momentum_ev.cols(idx1,idx2) = particles_(i).get_track_momentum_ev();
			curvature.cols(idx1,idx2) = particles_(i).get_track_curvature();
			angle_start.cols(idx1,idx2) = particles_(i).get_deviated_angle(false);
			angle_end.cols(idx1,idx2) = particles_(i).get_deviated_angle(true);
			temperature.cols(idx1,idx2) = particles_(i).get_track_temperature();
		}

		// construct mesh
		// allocate field arrays
		arma::field<arma::Mat<arma::uword> > nfld(1,num_particles);
		for(arma::uword i=0;i<num_particles;i++){
			// ignore tracks with only single coordinate
			if(track_length(i)<=1){
				nfld(i) = arma::Mat<arma::uword>(2,0);
				continue;
			}
			
			// get indices
			const arma::uword idx1 = track_idx(i);
			const arma::uword idx2 = track_idx(i+1)-1;

			// create mesh
			nfld(i) = arma::join_vert(
				arma::regspace<arma::Row<arma::uword> >(idx1,idx2-1),
				arma::regspace<arma::Row<arma::uword> >(idx1+1,idx2));
		}

		// combine
		const arma::Mat<arma::uword> n = cmn::Extra::field2mat(nfld);

		// export points as well
		vtk_data->set_nodes(coordinate);

		// set data
		vtk_data->set_nodedata(flux_density, "Mgn. Flux Density [T]");
		vtk_data->set_nodedata(electric_field, "Electric Field [V/m]");
		vtk_data->set_nodedata(velocity, "Velocity [m/s]");
		vtk_data->set_nodedata(velocity/arma::Datum<fltp>::c_0, "Lightspeed Fraction");
		vtk_data->set_nodedata(RAT_CONST(1e-9)*momentum_ev, "Momentum [GeV]");
		vtk_data->set_nodedata(curvature, "Curvature [1/m]");
		vtk_data->set_nodedata(360*angle_end/arma::Datum<fltp>::tau, "Angle Start [deg]");
		vtk_data->set_nodedata(360*angle_start/arma::Datum<fltp>::tau, "Angle End [deg]");
		vtk_data->set_nodedata(temperature, "Temperature [K]");


		// create mesh
		const arma::uword line_type = 3; // line elements
		vtk_data->set_elements(n,line_type);

		// return data object
		return vtk_data;
	}

	// write particle tracks to csv
	void TrackData::export_csv(const boost::filesystem::path& fpath) const{
		// open output stream
		std::ofstream fid(fpath.string(), std::ios::out | std::ios::binary);

		// ensure file is open
		if(!fid.is_open())rat_throw_line("could not open file for writing");

		// set precision
		fid<<std::fixed;
		fid<<std::setprecision(7);

		// walk over particles
		arma::uword cnt = 0;
		for(auto it=particles_.begin();it!=particles_.end();it++,cnt++){
			if(it!=particles_.begin())fid<<",";
			fid<<"x"<<cnt<<" [m], y"<<cnt<<" [m], z"<<cnt<<" [m]";
		}

		// add linebreak
		fid<<"\n";

		// write the tracks
		for(arma::uword i=0;;i++){
			// check if there is track left
			bool positions_left = false;

			// walk over particles
			for(auto it=particles_.begin();it!=particles_.end();it++){
				if(it!=particles_.begin())fid<<",";

				// get track coordinates by reference
				const arma::Mat<fltp>& Rt = (*it).get_raw_track_coords();

				// write coordinate
				if(i<Rt.n_cols){
					fid<<Rt(0,i)<<", "<<Rt(1,i)<<", "<<Rt(2,i);
				}else{
					fid<<"NaN, NaN, NaN";
				}

				// check if there are positions left
				if(Rt.n_cols>1){
					if(i<Rt.n_cols-1){
						positions_left = true;
					}
				}
			}

			// add linebreak
			fid<<"\n";

			// done
			if(!positions_left)break;
		}

		// close the output stream
		fid.close();
	}

	// // map tracks to frame
	// void TrackData::map2frame(const ShFramePr& frame)const{

	// 	// get coordinates from frame
	// 	const arma::Mat<fltp> Rf = cmn::Extra::field2mat(frame->get_coords());
	// 	const arma::Mat<fltp> Lf = cmn::Extra::field2mat(frame->get_longitudinal());
	// 	const arma::Mat<fltp> Nf = cmn::Extra::field2mat(frame->get_normal());
	// 	const arma::Mat<fltp> Df = cmn::Extra::field2mat(frame->get_transverse());
	// 	const arma::uword num_frame = Rf.n_cols;

	// 	// allocate
	// 	arma::Mat<fltp> t(num_particles_, num_frame); 
	// 	arma::Mat<fltp> u(num_particles_, num_frame); arma::Mat<fltp> v(num_particles_, num_frame); 
	// 	arma::Mat<fltp> pu(num_particles_, num_frame); arma::Mat<fltp> pv(num_particles_, num_frame);

	// 	// create marching cubes
	// 	const cmn::ShMarchingCubesPr mc = cmn::MarchingCubes::create();
	// 	mc->setup_line2isopoint();

	// 	// walk over particles
	// 	for(arma::uword i=0;i<num_particles_;i++){
	// 		// get relevant track data
	// 		const arma::Mat<fltp> tt = particles_(i)->get_track_time();
	// 		const arma::Mat<fltp>& Rt = particles_(i)->get_coords();
	// 		const arma::Mat<fltp> Pt = particles_(i)->get_track_momentum_ev();
		
	// 		// create a mesh for this track
	// 		const arma::Mat<arma::uword> n = arma::join_vert(arma::regspace(0,Rt.n_cols-2),arma::regspace(1,Rt.n_cols-1));

	// 		// walk over frame
	// 		for(arma::uword j=0;j<Rf.n_cols;j++){
	// 			// calculate offset distance
	// 			const arma::Row<fltp> normal_offset = cmn::Extra::dot(Lf.col(j),Rt.each_col()-Rf.col(j));

	// 			// intersect with plane
	// 			const arma::Mat<fltp> Rp = mc->polygonise(Rt, normal_offset, n);

	// 			// calculate relative distance to reference point
				
	// 		}
	// 	}



	// }


}}
