// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calcpolargrid.hh"

// rat-common headers
#include "rat/common/extra.hh"

// rat-mlfmm headers
#include "rat/mlfmm/mlfmm.hh"

// rat-model headers
#include "crosspoint.hh"
#include "modelmesh.hh"
#include "pathcircle.hh"
#include "modelgroup.hh"
#include "pathaxis.hh"
#include "polargriddata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcPolarGrid::CalcPolarGrid(){
		set_name("Polar Grid");

		// settinsg
		stngs_->set_num_exp(5);
		stngs_->set_large_ilist(true);
	}

	CalcPolarGrid::CalcPolarGrid(const ShModelPr &model) : CalcPolarGrid(){
		set_model(model);
	}

	CalcPolarGrid::CalcPolarGrid(
		const ShModelPr &model, const char axis,
		const fltp rin, const fltp rout, const arma::uword num_rad,
		const fltp theta1, const fltp theta2, const arma::uword num_theta,
		const fltp zlow, const fltp zhigh, const arma::uword num_axial,
		const arma::Col<fltp>::fixed<3> offset) : CalcPolarGrid(model){
		
		set_axis(axis);
		set_rin(rin); set_rout(rout); set_num_rad(num_rad);
		set_theta1(theta1); set_theta2(theta2); set_num_theta(num_theta);
		set_zlow(zlow); set_zhigh(zhigh); set_num_axial(num_axial);
		set_offset(offset);
	}
	
	// factory methods
	ShCalcPolarGridPr CalcPolarGrid::create(){
		return std::make_shared<CalcPolarGrid>();
	}

	// create and set model
	ShCalcPolarGridPr CalcPolarGrid::create(const ShModelPr &model){
		return std::make_shared<CalcPolarGrid>(model);
	}

	// create and set model and grid dimensions
	ShCalcPolarGridPr CalcPolarGrid::create(
		const ShModelPr &model, const char axis,
		const fltp rin, const fltp rout, const arma::uword num_rad,
		const fltp theta1, const fltp theta2, const arma::uword num_theta,
		const fltp zlow, const fltp zhigh, const arma::uword num_axial,
		const arma::Col<fltp>::fixed<3> offset){
		return std::make_shared<CalcPolarGrid>(model,axis,rin,rout,num_rad,theta1,theta2,num_theta,zlow,zhigh,num_axial,offset);
	}

	// setters
	void CalcPolarGrid::set_integrate_theta(const bool integrate_theta){
		integrate_theta_ = integrate_theta;
	}

	void CalcPolarGrid::set_offset(const arma::Col<fltp>::fixed<3> &offset){
		offset_ = offset;
	}

	void CalcPolarGrid::set_axis(const char axis){
		axis_ = axis;
	}

	void CalcPolarGrid::set_visibility(const bool visibility){
		visibility_ = visibility;
	}

	void CalcPolarGrid::set_rin(const fltp rin){
		rin_ = rin;
	}
	
	void CalcPolarGrid::set_rout(const fltp rout){
		rout_ = rout;
	}
	
	void CalcPolarGrid::set_theta1(const fltp theta1){
		theta1_ = theta1;
	}

	void CalcPolarGrid::set_theta2(const fltp theta2){
		theta2_ = theta2;
	}
	
	void CalcPolarGrid::set_zlow(const fltp zlow){
		zlow_ = zlow;
	}
	
	void CalcPolarGrid::set_zhigh(const fltp zhigh){
		zhigh_ = zhigh;
	}
	
	void CalcPolarGrid::set_num_rad(const arma::uword num_rad){
		num_rad_ = num_rad;
	}

	void CalcPolarGrid::set_num_theta(const arma::uword num_theta){
		num_theta_ = num_theta;
	}
	
	void CalcPolarGrid::set_num_axial(const arma::uword num_axial){
		num_axial_ = num_axial;
	}
	
	// getters
	bool CalcPolarGrid::get_integrate_theta()const{
		return integrate_theta_;
	}

	arma::Col<fltp>::fixed<3> CalcPolarGrid::get_offset() const{
		return offset_;
	}

	char CalcPolarGrid::get_axis()const{
		return axis_;
	}

	fltp CalcPolarGrid::get_rin()const{
		return rin_;
	}

	fltp CalcPolarGrid::get_rout()const{
		return rout_;
	}

	fltp CalcPolarGrid::get_theta1()const{
		return theta1_;
	}

	fltp CalcPolarGrid::get_theta2()const{
		return theta2_;
	}

	fltp CalcPolarGrid::get_zlow()const{
		return zlow_;
	}

	fltp CalcPolarGrid::get_zhigh()const{
		return zhigh_;
	}

	arma::uword CalcPolarGrid::get_num_rad()const{
		return num_rad_;
	}

	arma::uword CalcPolarGrid::get_num_theta()const{
		return num_theta_;
	}

	arma::uword CalcPolarGrid::get_num_axial()const{
		return num_axial_;
	}

	// set mesh enabled
	bool CalcPolarGrid::get_visibility()const{
		return visibility_;
	}

	// calculate with inductance data output
	ShGridDataPr CalcPolarGrid::calculate_grid(
		const fltp time, 
		const cmn::ShLogPr &lg,
		const ShSolverCachePr& cache){

		// when calculating the settings must be valid
		is_valid(true);

		// get date and time
		auto timdata = std::time(nullptr);
		auto localtime = *std::localtime(&timdata);
		std::ostringstream date_stream,time_stream;
		date_stream << std::put_time(&localtime, "%d-%m-%Y");
		time_stream << std::put_time(&localtime, "%H:%M:%S");
		const std::string date_str = date_stream.str();
		const std::string time_str = time_stream.str();
		
		// general info
		lg->msg("%s%s=== Starting Polar Grid Calculation ===%s\n",KBLD,KGRN,KNRM);
		lg->msg(2,"%s%sGENERAL INFO%s\n",KBLD,KGRN,KNRM);
		lg->msg("date: %s%s%s %s(dd-mm-yyyy)%s\n",KYEL,date_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("time: %s%s%s %s(hh:mm:ss)%s\n",KYEL,time_str.c_str(),KNRM,KBLU,KNRM);
		lg->msg("model name: %s%s%s\n",KYEL,model_->get_name().c_str(),KNRM);
		lg->msg("path name: %s%s%s\n",KYEL,get_name().c_str(),KNRM);
		lg->msg(-2,"\n");

		// create plane
		const ShPolarGridDataPr polar_grd = PolarGridData::create(
			axis_, rin_,rout_,num_rad_, theta1_,theta2_,num_theta_, zlow_,zhigh_,num_axial_, offset_);
		polar_grd->set_field_type("ABHM",{3,3,3,3});

		// check input model
		if(model_==NULL)rat_throw_line("model not set");
		model_->is_valid(true);
		
		// settings for mesh creation
		MeshSettings mesh_settings;
		mesh_settings.time = time;
		mesh_settings.low_poly = false;
		mesh_settings.combine_sections = true;

		// create meshes
		const std::list<ShMeshDataPr> meshes = model_->create_meshes({},mesh_settings);

		// show meshes
		lg->msg(2,"%s%sGEOMETRY SETUP%s\n",KBLD,KGRN,KNRM);
		MeshData::display(lg, meshes);
		lg->msg(-2,"\n"); 

		// run field calculation
		calculate_field(meshes,polar_grd,time,lg,cache);

		// integration requested
		ShGridDataPr grd;
		if(integrate_theta_)grd = polar_grd->azymuthal_integral(); else grd = polar_grd;

		// return the line data
		return grd;
	}

	// generalized calculation
	std::list<ShDataPr> CalcPolarGrid::calculate(
		const fltp time, 
		const cmn::ShLogPr &lg,
		const ShSolverCachePr& cache){
		return {calculate_grid(time,lg,cache)};
	}

	// creation of calculation data objects
	std::list<ShMeshDataPr> CalcPolarGrid::create_meshes(
		const std::list<arma::uword> &trace, 
		const MeshSettings &stngs)const{

		// trace
		if(!trace.empty())rat_throw_line("trace must be empty for leaf");

		// mesh enabled
		if(!visibility_ || !enable_)return{};
		if(!is_valid(stngs.enable_throws))return{};

		// trace
		if(!trace.empty())rat_throw_line("trace must be empty for leaf");

		// element size

		// create a group
		const ShModelGroupPr group = ModelGroup::create();

		// start and end points of arc
		const arma::Col<fltp>::fixed<3> inner_arc_start{rin_*std::cos(theta1_), rin_*std::sin(theta1_), 0.0};
		const arma::Col<fltp>::fixed<3> inner_arc_end{rin_*std::cos(theta2_), rin_*std::sin(theta2_), 0.0};
		const arma::Col<fltp>::fixed<3> outer_arc_start{rout_*std::cos(theta1_), rout_*std::sin(theta1_), 0.0};
		const arma::Col<fltp>::fixed<3> outer_arc_end{rout_*std::cos(theta2_), rout_*std::sin(theta2_), 0.0};

		// inner circles
		if(rin_>RAT_CONST(0.0)){
			// create arc
			const ShPathGroupPr arc_in = PathGroup::create(inner_arc_start, {-std::sin(theta1_), std::cos(theta1_), 0.0}, {std::cos(theta1_), std::sin(theta1_), 0.0});
			arc_in->add_path(PathArc::create(rin_,theta2_-theta1_,arma::Datum<fltp>::tau*rin_/num_theta_));

			// add lower inner circle
			const ShModelMeshPr c1 = ModelMesh::create(arc_in, CrossPoint::create());
			c1->add_translation({0,0,zlow_});
			group->add_model(c1);

			// add upper inner circle
			const ShModelMeshPr c2 = ModelMesh::create(arc_in, CrossPoint::create());
			c2->add_translation({0,0,zhigh_});
			group->add_model(c2);
		}

		// create arc
		const ShPathGroupPr arc_out = PathGroup::create(outer_arc_start, {-std::sin(theta1_), std::cos(theta1_), 0.0}, {std::cos(theta1_), std::sin(theta1_), 0.0});
		arc_out->add_path(PathArc::create(rout_,theta2_-theta1_,arma::Datum<fltp>::tau*rout_/num_theta_));

		// add lower outer circle
		const ShModelMeshPr c3 = ModelMesh::create(arc_out, CrossPoint::create());
		c3->add_translation({0,0,zlow_});
		group->add_model(c3);

		// add upper outer circle
		const ShModelMeshPr c4 = ModelMesh::create(arc_out, CrossPoint::create());
		c4->add_translation({0,0,zhigh_});
		group->add_model(c4);

		// rectangles at start and end
		if(theta2_ - theta1_<arma::Datum<fltp>::tau){
			// vertical lines
			group->add_model(ModelMesh::create(PathAxis::create('z','x',zhigh_ - zlow_,inner_arc_start,zhigh_-zlow_), CrossPoint::create()));
			group->add_model(ModelMesh::create(PathAxis::create('z','x',zhigh_ - zlow_,inner_arc_end,zhigh_-zlow_), CrossPoint::create()));
			group->add_model(ModelMesh::create(PathAxis::create('z','x',zhigh_ - zlow_,outer_arc_start,zhigh_-zlow_), CrossPoint::create()));
			group->add_model(ModelMesh::create(PathAxis::create('z','x',zhigh_ - zlow_,outer_arc_end,zhigh_-zlow_), CrossPoint::create()));
			
			const ShPathGroupPr radial1 = PathGroup::create({rin_*std::cos(theta1_), rin_*std::sin(theta1_), zlow_}, {std::cos(theta1_), std::sin(theta1_), 0.0}, {-std::sin(theta1_), std::cos(theta1_), 0.0});
			radial1->add_path(PathStraight::create(rout_ - rin_, rout_ - rin_));
			group->add_model(ModelMesh::create(radial1, CrossPoint::create()));

			const ShPathGroupPr radial2 = PathGroup::create({rin_*std::cos(theta1_), rin_*std::sin(theta1_), zhigh_}, {std::cos(theta1_), std::sin(theta1_), 0.0}, {-std::sin(theta1_), std::cos(theta1_), 0.0});
			radial2->add_path(PathStraight::create(rout_ - rin_, rout_ - rin_));
			group->add_model(ModelMesh::create(radial2, CrossPoint::create()));

			const ShPathGroupPr radial3 = PathGroup::create({rin_*std::cos(theta2_), rin_*std::sin(theta2_), zlow_}, {std::cos(theta2_), std::sin(theta2_), 0.0}, {-std::sin(theta2_), std::cos(theta2_), 0.0});
			radial3->add_path(PathStraight::create(rout_ - rin_, rout_ - rin_));
			group->add_model(ModelMesh::create(radial3, CrossPoint::create()));

			const ShPathGroupPr radial4 = PathGroup::create({rin_*std::cos(theta2_), rin_*std::sin(theta2_), zhigh_}, {std::cos(theta2_), std::sin(theta2_), 0.0}, {-std::sin(theta2_), std::cos(theta2_), 0.0});
			radial4->add_path(PathStraight::create(rout_ - rin_, rout_ - rin_));
			group->add_model(ModelMesh::create(radial4, CrossPoint::create()));
		}

		// rotate for different axes
		if(axis_=='x')group->add_rotation({0,1,0},arma::Datum<fltp>::pi/2);
		if(axis_=='y')group->add_rotation({1,0,0},arma::Datum<fltp>::pi/2);

		// add offset
		group->add_translation(offset_);

		// return mesh
		return group->create_meshes({},stngs);
	}

	// validity check
	bool CalcPolarGrid::is_valid(const bool enable_throws) const{
		if(!CalcLeaf::is_valid(enable_throws))return false;
		if(theta2_-theta1_<=0){if(enable_throws){rat_throw_line("theta2 must be larger than theta1");} return false;}
		if(rout_-rin_<=0){if(enable_throws){rat_throw_line("rout must be larger than rin");} return false;}
		if(zhigh_-zlow_<=0){if(enable_throws){rat_throw_line("zhigh must be larger than zlow");} return false;}
		if(num_rad_<=0){if(enable_throws){rat_throw_line("number of nodes in radial direction must be larger than zero");} return false;}
		if(num_theta_<=0){if(enable_throws){rat_throw_line("number of nodes in azymuthal direction must be larger than zero");} return false;}
		if(num_axial_<=0){if(enable_throws){rat_throw_line("number of nodes in axial direction must be larger than zero");} return false;}
		return true;
	}	

	// serialization
	std::string CalcPolarGrid::get_type(){
		return "rat::mdl::calcpolargrid";
	}

	void CalcPolarGrid::serialize(
		Json::Value &js, cmn::SList &list) const{
		// parent
		CalcLeaf::serialize(js,list);
		
		// type
		js["type"] = get_type();

		// position 
		js["offset_x"] = offset_(0);
		js["offset_y"] = offset_(1);
		js["offset_z"] = offset_(2);

		// dimensions
		js["integrate_theta"] = integrate_theta_;
		js["axis"] = static_cast<int>(axis_);
		js["rin"] = rin_;
		js["rout"] = rout_; 
		js["zlow"] = zlow_;
		js["zhigh"] = zhigh_; 
		js["theta1"] = theta1_;
		js["theta2"] = theta2_; 
		
		// discretization
		js["num_rad"] = static_cast<int>(num_rad_);
		js["num_theta"] = static_cast<int>(num_theta_);
		js["num_axial"] = static_cast<int>(num_axial_);

		// enable visibility
		js["visibility"] = visibility_;
	}
	
	void CalcPolarGrid::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		CalcLeaf::deserialize(js,list,factory_list,pth);

		// position 
		offset_(0) = js["offset_x"].ASFLTP();
		offset_(1) = js["offset_y"].ASFLTP();
		offset_(2) = js["offset_z"].ASFLTP();

		// dimensions
		integrate_theta_ = js["integrate_theta"].asBool();
		axis_ = static_cast<char>(js["axis"].asInt());
		rin_ = js["rin"].ASFLTP();
		rout_ = js["rout"].ASFLTP(); 
		zlow_ = js["zlow"].ASFLTP();
		zhigh_ = js["zhigh"].ASFLTP(); 
		theta1_ = js["theta1"].ASFLTP();
		theta2_ = js["theta2"].ASFLTP();
		
		// discretization
		num_rad_ = js["num_rad"].asUInt64();
		num_theta_ = js["num_theta"].asUInt64();
		num_axial_ = js["num_axial"].asUInt64();

		// enable visibility
		visibility_ = js["visibility"].asBool();
	}

}}