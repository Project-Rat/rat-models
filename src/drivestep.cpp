// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "drivestep.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	DriveStep::DriveStep(){

	}

	// constructor
	DriveStep::DriveStep(
		const fltp amplitude, 
		const fltp tstart, 
		const fltp tpulse){
		set_amplitude(amplitude); set_tstart(tstart); 
		set_tpulse(tpulse);
		set_name("Step");
	}

	// factory
	ShDriveStepPr DriveStep::create(){
		return std::make_shared<DriveStep>();
	}

	// factory
	ShDriveStepPr DriveStep::create(
		const fltp amplitude, 
		const fltp tstart, 
		const fltp tpulse){
		return std::make_shared<DriveStep>(amplitude,tstart,tpulse);
	}

	// get scaling
	fltp DriveStep::get_scaling(
		const fltp position,
		const fltp /*time*/,
		const arma::uword derivative) const{
		// check spatial
		if(derivative==0){
			if(position>tstart_ && position<=tstart_+tpulse_)return amplitude_;
		}
		return 0.0;
	}

	// set amplitude of the pulse
	void DriveStep::set_amplitude(const fltp amplitude){
		amplitude_ = amplitude;
	}

	// set start time
	void DriveStep::set_tstart(const fltp tstart){
		tstart_ = tstart;
	}

	// set pulse duration
	void DriveStep::set_tpulse(const fltp tpulse){
		tpulse_ = tpulse;
	}

	// get times at which an inflection occurs
	arma::Col<fltp> DriveStep::get_inflection_points() const{
		return {tstart_, tstart_ + tpulse_};
	}

	// apply scaling for the input settings
	void DriveStep::rescale(const fltp scale_factor){
		amplitude_ *= scale_factor;
	}

	// get type
	std::string DriveStep::get_type(){
		return "rat::mdl::drivestep";
	}

	// method for serialization into json
	void DriveStep::serialize(Json::Value &js, cmn::SList &list) const{
		Drive::serialize(js,list);
		js["type"] = get_type();
		js["amplitude"] = amplitude_;
		js["tstart"] = tstart_;
		js["tpulse"] = tpulse_;
	}

	// method for deserialisation from json
	void DriveStep::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		Drive::deserialize(js,list,factory_list,pth);
		set_amplitude(js["amplitude"].ASFLTP());
		set_tstart(js["tstart"].ASFLTP());
		set_tpulse(js["tpulse"].ASFLTP());
	}

}}