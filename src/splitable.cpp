// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "splitable.hh"

// code specific to Rat
namespace rat{namespace mdl{
	// recursive splitting
	std::list<ShModelPr> Splitable::split_recursive(const ShSerializerPr &slzr)const{
		// get initial model list
		std::list<ShModelPr> model_list = split(slzr);
		std::list<ShModelPr> split_list;

		// walk over models
		for(auto it=model_list.begin();it!=model_list.end();){
			const ShSplitablePr splitable_model = std::dynamic_pointer_cast<Splitable>(*it);
			if(splitable_model!=NULL){
				split_list.splice(split_list.end(), splitable_model->split_recursive(slzr));
				it = model_list.erase(it);
			}else{
				it++;
			}
		}

		// splice them together
		model_list.splice(model_list.end(), split_list);

		// return the list
		return model_list;
	}
}}