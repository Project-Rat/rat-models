// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelcoil.hh"

#include "extra.hh"
#include "coildata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ModelCoil::ModelCoil(){
		set_name("Custom Coil");
	}

	// constructor
	ModelCoil::ModelCoil(
		const ShPathPr &input_path,
		const ShCrossPr &input_cross,
		const mat::ShConductorPr &conductor) : ModelCoil(){

		// set to self
		set_input_path(input_path);
		set_input_cross(input_cross); set_input_conductor(conductor);
	}

	// factory
	ShModelCoilPr ModelCoil::create(){
		return std::make_shared<ModelCoil>();
	}

	// factory immediately setting base and cross section
	ShModelCoilPr ModelCoil::create(
		const ShPathPr &input_path, 
		const ShCrossPr &input_cross, 
		const mat::ShConductorPr &conductor){
		return std::make_shared<ModelCoil>(input_path,input_cross,conductor);
	}

	// // use volume elements
	// void ModelCoil::set_use_volume_elements(const bool use_volume_elements){
	// 	use_volume_elements_ = use_volume_elements;
	// }

	// set current sharing 
	void ModelCoil::set_use_current_density(const bool use_current_density){
		use_current_density_ = use_current_density;
	}

	// set operating current 
	void ModelCoil::set_operating_current(const fltp operating_current){
		operating_current_ = operating_current;
	}

	// set operating current 
	void ModelCoil::set_operating_current_density(const fltp operating_current_density){
		operating_current_density_ = operating_current_density;
	}

	// get drive
	ShDrivePr ModelCoil::get_current_drive() const{
		return current_drive_;
	}

	// set drive
	void ModelCoil::set_current_drive(const ShDrivePr &current_drive){
		if(current_drive==NULL)rat_throw_line("supplied drive points to NULL");
		current_drive_ = current_drive;
	}

	// // set material
	// void ModelCoil::set_material(const mat::ShConductorPr &material){
	// 	conductor_ = material;
	// }

	// // get material
	// rat::mat::ShConductorPr ModelCoil::get_material() const{
	// 	return conductor_;
	// }

	// set number of turns
	void ModelCoil::set_number_turns(const fltp number_turns){
		set_num_turns(number_turns);
	}

	// set number of turns
	void ModelCoil::set_num_turns(const fltp num_turns){
		number_turns_ = num_turns;
	}


	// set number of turns
	void ModelCoil::set_softening(const fltp softening){
		softening_ = softening;
	}

	// set number of turns
	fltp ModelCoil::get_softening() const{
		return softening_;
	}

	// get current sharing 
	bool ModelCoil::get_use_current_density()const{
		return use_current_density_;
	}

	// getting of coil number of turns
	fltp ModelCoil::get_number_turns() const{
		return get_num_turns();
	}

	// getting of coil number of turns
	fltp ModelCoil::get_num_turns() const{
		return number_turns_;
	}

	// getting of coil number of turns
	void ModelCoil::set_num_gauss(const arma::sword num_gauss){
		num_gauss_ = num_gauss;
	}

	// operating current
	fltp ModelCoil::get_operating_current() const{
		return operating_current_;
	}

	// operating current
	arma::sword ModelCoil::get_num_gauss() const{
		return num_gauss_;
	}


	// operating current
	fltp ModelCoil::get_operating_current_density() const{
		return operating_current_density_;
	}

	// create data object
	ShMeshDataPr ModelCoil::create_data()const{
		return CoilData::create();
	}

	// factory for mesh objects
	std::list<ShMeshDataPr> ModelCoil::create_meshes(
		const std::list<arma::uword> &trace, 
		const MeshSettings &stngs)const{

		// check enabled
		if(!get_enable())return{};

		// trace
		if(!trace.empty())rat_throw_line("trace must be empty for leaf");

		// check model
		if(!is_valid(stngs.enable_throws))return{};
		
		// fill mesh data
		std::list<ShMeshDataPr> meshes = ModelMesh::create_meshes(trace,stngs);

		// check input
		assert(current_drive_!=NULL);

		// // in case of volume elements
		// if(use_volume_elements_)rat_throw_line("not yet implemented");


		// allocate list of bar meshes
		std::list<ShMeshDataPr> coil_meshes;
		for(auto it = meshes.begin();it!=meshes.end();it++){
			// reinterpret cast
			const ShCoilDataPr coil_mesh = std::dynamic_pointer_cast<CoilData>(*it);
			if(coil_mesh==NULL)rat_throw_line("pointer cast failed");

			// calculate current with drive scaling
			const fltp coil_current = 
				current_drive_->get_scaling(stngs.time)*(use_current_density_ ? 
				coil_mesh->get_base_area()*operating_current_density_/number_turns_ : operating_current_);

			// copy properties
			coil_mesh->set_operating_current(coil_current);
			coil_mesh->set_number_turns(number_turns_);
			coil_mesh->set_softening(softening_);
			coil_mesh->set_num_gauss_volume(num_gauss_);
			coil_mesh->set_num_gauss_surface(num_gauss_);

			// add to list
			coil_meshes.push_back(coil_mesh);
		}

		// return mesh data
		return coil_meshes;
	}

	// // factory for edge objects
	// ShEdgesPrList ModelCoil::create_edge(const fltp time) const{
	// 	// check model
	// 	if(!is_valid())return{};

	// 	// allocate edge data
	// 	ShEdgesCoilPr edge = EdgesCoil::create();

	// 	// calculate current with drive scaling
	// 	const fltp coil_current = current_drive_->get_scaling(time)*operating_current_;

	// 	// fill edge data
	// 	setup_edge(edge, time);

	// 	// copy properties
	// 	edge->set_material(get_material());
	// 	edge->set_operating_current(coil_current);
	// 	edge->set_number_turns(number_turns_);

	// 	// combine into list
	// 	ShEdgesPrList edges(1);
	// 	edges(0) = edge;

	// 	// return edge data
	// 	return edges;
	// }

	// check validity
	bool ModelCoil::is_valid(const bool enable_throws) const{
		// check mesh
		if(!ModelMesh::is_valid(enable_throws))return false;
		if(current_drive_==NULL){if(enable_throws){rat_throw_line("current drive is zero");} return false;}
		if(!current_drive_->is_valid(enable_throws))return false;
		if(number_turns_<=0){if(enable_throws){rat_throw_line("number of turns must be larger than zero");} return false;}
		if(num_gauss_==0){if(enable_throws){rat_throw_line("number of gauss points can not be zero");} return false;}
		if(softening_<0){if(enable_throws){rat_throw_line("softening must be zero or larger");} return false;}
		return true;
	}

	// get type
	std::string ModelCoil::get_type(){
		return "rat::mdl::modelcoil";
	}

	// method for serialization into json
	void ModelCoil::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents first 
		// this allows the type to be overridden
		ModelMesh::serialize(js,list);
		
		// current drive
		js["current_drive"] = cmn::Node::serialize_node(current_drive_, list);

		// properties
		js["type"] = get_type();
		js["use_current_density"] = use_current_density_;
		js["softening"] = softening_;
		js["number_turns"] = number_turns_;
		js["operating_current"] = operating_current_;
		js["operating_current_density"] = operating_current_density_;
		js["num_gauss"] = static_cast<int>(num_gauss_);

		// subnodes
		//js["circ"] = cmn::Node::serialize_node(circ_, list);
		//js["material"] = cmn::Node::serialize_node(conductor_, list);
	}

	// method for deserialisation from json
	void ModelCoil::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent objects
		ModelMesh::deserialize(js,list,factory_list,pth);
		
		// current drive
		if(js.isMember("current_drive"))set_current_drive(cmn::Node::deserialize_node<Drive>(js["current_drive"], list, factory_list, pth));

		// properties
		// set_num_gauss(js["num_gauss"].asInt64());
		set_use_current_density(js["use_current_density"].asBool());
		set_softening(js["softening"].ASFLTP());
		set_number_turns(js["number_turns"].ASFLTP());
		set_operating_current(js["operating_current"].ASFLTP());
		set_operating_current_density(js["operating_current_density"].ASFLTP());
		set_num_gauss(js["num_gauss"].asInt64());
	}

	// write current elements to VTK for checking
	// void ModelCoil::write_elements(
	// 	const std::string &fname) const{

	// 	// get coil meshes
	// 	rat::mdl::ShMeshDataPrList meshes = create_mesh();
	// 	rat::mdl::ShMeshCoilPrList cmshes = rat::mdl::Extra::filter<rat::mdl::MeshCoil>(meshes);
		
	// 	// get source elements
	// 	rat::fmm::ShCurrentSourcesPr src = cmshes(0)->create_current_sources();
	// 	arma::Mat<fltp> Rs = src->get_source_coords();
	// 	arma::Mat<fltp> dRs = src->get_direction();
	// 	arma::Row<fltp> Is = src->get_source_currents();

	// 	// create vtk file
	// 	rat::mdl::ShVTKUnstrPr vtk = rat::mdl::VTKUnstr::create();
	// 	vtk->set_nodes(Rs);
	// 	vtk->set_elements(arma::regspace<arma::Row<arma::uword> >(0,Rs.n_cols-1),1);
	// 	vtk->set_nodedata(dRs,"direction [m]");
	// 	vtk->set_nodedata(dRs.each_row()%Is,"current [Am]");

	// 	// write
	// 	vtk->write(fname);
	// }


}}