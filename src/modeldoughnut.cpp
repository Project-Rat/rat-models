// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modeldoughnut.hh"

#include "rat/common/error.hh"

#include "crosscircle.hh"
#include "pathcircle.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	ModelDoughnut::ModelDoughnut(){
		set_name("Doughnut");
	}

	// default constructor
	ModelDoughnut::ModelDoughnut(
		const fltp radius1,
		const fltp radius2,
		const fltp element_size) : ModelDoughnut(){

		// set to self
		set_radius1(radius1); 
		set_radius2(radius2);
		set_element_size(element_size);
	}

	// factory
	ShModelDoughnutPr ModelDoughnut::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<ModelDoughnut>();
	}

	// factory
	ShModelDoughnutPr ModelDoughnut::create(
		const fltp radius1,
		const fltp radius2,
		const fltp element_size){
		return std::make_shared<ModelDoughnut>(
			radius1, radius2, element_size);
	}

	// get base
	ShPathPr ModelDoughnut::get_input_path() const{
		// check input
		is_valid(true);

		// create axis
		const ShPathCirclePr circle = PathCircle::create(radius1_, 4, element_size_, radius2_);

		// return the racetrack
		return circle;
	}

	// get cross
	ShCrossPr ModelDoughnut::get_input_cross() const{
		// check input
		is_valid(true);

		// create circular cross section
		const ShCrossCirclePr circle = CrossCircle::create(radius2_, element_size_);

		// return the circle
		return circle;
	}

	// set properties
	void ModelDoughnut::set_radius1(const fltp radius1){
		radius1_ = radius1;
	}

	void ModelDoughnut::set_radius2(const fltp radius2){
		radius2_ = radius2;
	}

	void ModelDoughnut::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}


	// get properties
	fltp ModelDoughnut::get_radius1()const{
		return radius1_;
	}

	fltp ModelDoughnut::get_radius2()const{
		return radius2_;
	}

	fltp ModelDoughnut::get_element_size()const{
		return element_size_;
	}


	// check input
	bool ModelDoughnut::is_valid(const bool enable_throws) const{
		if(!ModelMeshWrapper::is_valid(enable_throws))return false;
		if(radius1_<=0){if(enable_throws){rat_throw_line("first radius must be larger than zero");} return false;};
		if(radius2_<=0){if(enable_throws){rat_throw_line("second radius must be larger than zero");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string ModelDoughnut::get_type(){
		return "rat::mdl::modeldoughnut";
	}

	// method for serialization into json
	void ModelDoughnut::serialize(
		Json::Value &js, cmn::SList &list) const{

		// parent
		ModelMeshWrapper::serialize(js,list);

		// set type
		js["type"] = get_type();
		
		// set properties
		js["radius1"] = radius1_;
		js["radius2"] = radius2_;
		js["element_size"] = element_size_;
	}

	// method for deserialisation from json
	void ModelDoughnut::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		ModelMeshWrapper::deserialize(js,list,factory_list,pth);

		// get properties
		radius1_ = js["radius1"].ASFLTP();
		radius2_ = js["radius2"].ASFLTP();
		element_size_ = js["element_size"].ASFLTP();
	}

}}