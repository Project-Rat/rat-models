// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathpolygon.hh"
#include "pathstraight.hh"
#include "patharc.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathPolygon::PathPolygon(){
		set_name("Polygon");
	}

	// constructor
	PathPolygon::PathPolygon(
		const arma::uword num_sides, const fltp alpha, const fltp element_size) : PathPolygon(){
		
		// set parameters
		set_num_sides(num_sides);
		set_alpha(alpha); 
		set_element_size(element_size);
	}

	// factory
	ShPathPolygonPr PathPolygon::create(){
		return std::make_shared<PathPolygon>();
	}

	// factory with dimensional input
	ShPathPolygonPr PathPolygon::create(
		const arma::uword num_sides, const fltp alpha, const fltp element_size){
		return std::make_shared<PathPolygon>(num_sides, alpha,element_size);
	}

	// set ell1
	void PathPolygon::set_num_sides(const arma::uword num_sides){
		num_sides_ = num_sides;
	}

	// set ell2
	void PathPolygon::set_alpha(const fltp alpha){
		alpha_ = alpha;
	}

	// get ell1
	arma::uword PathPolygon::get_num_sides() const{
		return num_sides_;
	}

	// get ell2
	fltp PathPolygon::get_alpha() const{
		return alpha_;
	}

	// set element size 
	void PathPolygon::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}
	
	// get element size 
	fltp PathPolygon::get_element_size() const{
		return element_size_;
	}

	// set path offset
	void PathPolygon::set_offset(const fltp offset){
		offset_ = offset;
	}
	
	// get path offset
	fltp PathPolygon::get_offset() const{
		return offset_;
	}

	// set radius
	void PathPolygon::set_radius(const fltp radius){
		radius_ = radius;
	}
	// set radius
	fltp PathPolygon::get_radius() const{
		return radius_;
	}

	// get frame
	ShFramePr PathPolygon::create_frame(const MeshSettings &stngs) const{
		// check input
		is_valid(true);

		// create unified path
		ShPathGroupPr pathgroup = PathGroup::create();

		// R = s/2*std::sin(arma::Datum<fltp>::pi/num_sides_) = alpha_/std::cos(pi/num_sides_)
		const fltp side_length = calc_side_length();

		// angle in corner
		const fltp arc_length = calc_arc_length();

		// Calculate the lengths that need to be subtracted
		// from the total to compensate for the rounded corners
		const fltp corner_length = calc_corner_length();

		// std::cout<<corner_length<<std::endl;
		// std::cout<<side_length<<std::endl;

		// add sections
		pathgroup->add_path(PathStraight::create(side_length/2-corner_length, element_size_));
		pathgroup->add_path(PathArc::create(radius_, arc_length, element_size_, offset_));

		// set offset
		pathgroup->add_translation(alpha_,0,0);
		pathgroup->set_sym(2*num_sides_);

		// create frame
		ShFramePr frame = pathgroup->create_frame(stngs);

		// apply transformations
		frame->apply_transformations(get_transformations(), stngs.time);
		
		// return frame
		return frame;
	}

	// calculate length of the sides
	fltp PathPolygon::calc_side_length() const{
		return alpha_*2*std::tan(arma::Datum<fltp>::pi/num_sides_);
	}

	// calculate length of arc in rad
	fltp PathPolygon::calc_arc_length() const{
		return arma::Datum<fltp>::pi/num_sides_;
	}

	// calculate corner length
	fltp PathPolygon::calc_corner_length() const{
		return radius_*std::tan(calc_arc_length()); 
	}

	// validity check
	bool PathPolygon::is_valid(const bool enable_throws) const{
		if(num_sides_<=2){if(enable_throws){rat_throw_line("number of sides must be larger than two");} return false;};
		if(alpha_<=0){if(enable_throws){rat_throw_line("alpha must be larger than zero");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		if(radius_<=0){if(enable_throws){rat_throw_line("radius must be larger than zero");} return false;};
		if(calc_side_length()<2*calc_corner_length()){if(enable_throws){rat_throw_line("side length is shorter than twice corner length");} return false;};
		return true;
	}

	// get type
	std::string PathPolygon::get_type(){
		return "rat::mdl::pathpolygon";
	}

	// method for serialization into json
	void PathPolygon::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Path::serialize(js,list);
		Transformations::serialize(js,list);

		// settings
		js["type"] = get_type();
		js["num_sides"] = static_cast<int>(num_sides_);
		js["alpha"] = alpha_;
		js["element_size"] = element_size_;
		js["radius"] = radius_;
		js["offset"] = offset_;
	}

	// method for deserialisation from json
	void PathPolygon::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent
		Path::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);
		
		// properties
		set_num_sides(js["num_sides"].asUInt64());
		set_alpha(js["alpha"].ASFLTP());
		set_element_size(js["element_size"].ASFLTP());
		set_radius(js["radius"].ASFLTP());
		set_offset(js["offset"].ASFLTP());
	}


}}