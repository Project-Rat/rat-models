// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathbspline.hh"

// rat-common headers
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathBSpline::PathBSpline(){
		set_name("B-Spline");
	}

	// constructor
	PathBSpline::PathBSpline(
		const arma::Col<fltp>::fixed<3>& pstart, 
		const arma::Col<fltp>::fixed<3>& lstart,
		const arma::Col<fltp>::fixed<3>& nstart,
		const arma::Col<fltp>::fixed<3>& pend, 
		const arma::Col<fltp>::fixed<3>& lend,
		const arma::Col<fltp>::fixed<3>& nend,
		const fltp str1, const fltp str2, 
		const fltp element_size, const fltp offset){

		// check input
		assert(element_size>0); assert(str1>0); assert(str2>0);
		assert(std::abs(arma::as_scalar(cmn::Extra::vec_norm(lend))-RAT_CONST(1.0))<1e-6);
		assert(std::abs(arma::as_scalar(cmn::Extra::vec_norm(nend))-RAT_CONST(1.0))<1e-6);

		// set values
		pstart_ = pstart; lstart_ = lstart; nstart_ = nstart;
		pend_ = pend; lend_ = lend; nend_ = nend;
		str1_ = str1; str2_ = str2; 
		element_size_ = element_size;
		offset_ = offset;
	}

	// constructor
	PathBSpline::PathBSpline(
		const fltp pstartx, const fltp pstarty,	const fltp pstartz,
		const fltp lstartx, const fltp lstarty,	const fltp lstartz,
		const fltp nstartx, const fltp nstarty,	const fltp nstartz,
		const fltp pendx, const fltp pendy,	const fltp pendz,
		const fltp lendx, const fltp lendy,	const fltp lendz,
		const fltp nendx, const fltp nendy,	const fltp nendz,
		const fltp str1, const fltp str2, 
		const fltp element_size, const fltp offset){

		// make vectors
		pstart_ = arma::Col<fltp>::fixed<3>{pstartx,pstarty,pstartz};
		lstart_ = arma::Col<fltp>::fixed<3>{lstartx,lstarty,lstartz};
		nstart_ = arma::Col<fltp>::fixed<3>{nstartx,nstarty,nstartz};
		pend_ = arma::Col<fltp>::fixed<3>{pendx,pendy,pendz};
		lend_ = arma::Col<fltp>::fixed<3>{lendx,lendy,lendz};
		nend_ = arma::Col<fltp>::fixed<3>{nendx,nendy,nendz};

		// check input
		assert(element_size>0); assert(str1>0); assert(str2>0);
		assert(std::abs(arma::as_scalar(cmn::Extra::vec_norm(lend_))-RAT_CONST(1.0))<1e-6);
		assert(std::abs(arma::as_scalar(cmn::Extra::vec_norm(nend_))-RAT_CONST(1.0))<1e-6);

		// set values
		str1_ = str1; str2_ = str2; 
		element_size_ = element_size;
		offset_ = offset;
	}

	// factory
	ShPathBSplinePr PathBSpline::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<PathBSpline>();
	}

	// factory with dimension input
	ShPathBSplinePr PathBSpline::create(
		const arma::Col<fltp>::fixed<3>& pstart, 
		const arma::Col<fltp>::fixed<3>& lstart,
		const arma::Col<fltp>::fixed<3>& nstart,
		const arma::Col<fltp>::fixed<3>& pend, 
		const arma::Col<fltp>::fixed<3>& lend,
		const arma::Col<fltp>::fixed<3>& nend,
		const fltp str1, const fltp str2, 
		const fltp element_size, const fltp offset){
		return std::make_shared<PathBSpline>(pstart,lstart,nstart,pend,lend,nend,str1,str2,element_size,offset);
	}

	// factory with dimension input
	ShPathBSplinePr PathBSpline::create(
		const fltp pstartx, const fltp pstarty,	const fltp pstartz,
		const fltp lstartx, const fltp lstarty,	const fltp lstartz,
		const fltp nstartx, const fltp nstarty,	const fltp nstartz,
		const fltp pendx, const fltp pendy,	const fltp pendz,
		const fltp lendx, const fltp lendy,	const fltp lendz,
		const fltp nendx, const fltp nendy,	const fltp nendz,
		const fltp str1, const fltp str2, 
		const fltp element_size, const fltp offset){
		return std::make_shared<PathBSpline>(
			pstartx,pstarty,pstartz,lstartx,lstarty,lstartz,nstartx,nstarty,nstartz,
			pendx,pendy,pendz,lendx,lendy,lendz,nendx,nendy,nendz,str1,str2,element_size,offset);
	}


	// method for creating the frame
	ShFramePr PathBSpline::create_frame(const MeshSettings &stngs) const{
		// get spline length
		const fltp ell = get_length();

		// calculate number of points
		arma::uword num_points = std::max(2llu,static_cast<arma::uword>(std::ceil(ell/element_size_)+1));
		while((num_points-1)%stngs.element_divisor!=0)num_points++;

		// make regular time array
		const arma::Row<fltp> treq = arma::linspace<arma::Row<fltp> >(0.0,1.0,num_points);

		// get regular times
		arma::Mat<fltp> R,L,N,D;
		get_frame_from_time(R,L,N,D,treq,true);

		// check if frame are truely orthogonal
		assert(arma::all(cmn::Extra::dot(L,N)<1e-6));
		assert(arma::all(cmn::Extra::dot(L,D)<1e-6));
		assert(arma::all(cmn::Extra::dot(D,N)<1e-6));

		// check handedness
		assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N,L),D)>RAT_CONST(0.0)));
		
		// create frame and return
		return Frame::create(R,L,N,D);
	}

	// calculate curve length (with offset)
	fltp PathBSpline::get_length() const{
		// regular time array
		const arma::Row<fltp> t = arma::linspace<arma::Row<fltp> >(0.0,1.0,Nreg_);

		// get frame
		arma::Mat<fltp> R,L,N,D;
		get_frame_from_time(R,L,N,D,t,false);

		// offset frame
		const arma::Mat<fltp> Roffset = R + offset_*N;

		// calculate length
		// const fltp ell1 = arma::as_scalar(arma::sum(arma::sqrt(arma::sum(arma::pow(arma::diff(Roffset,1,1),2),0)),1));
		// const fltp ell2 = arma::as_scalar(arma::sum(arma::sqrt(arma::sum(arma::pow(arma::diff(R,1,1),2),0)),1));
		const fltp ell1 = arma::accu(cmn::Extra::vec_norm(arma::diff(Roffset,1,1)));
		const fltp ell2 = arma::accu(cmn::Extra::vec_norm(arma::diff(R,1,1)));

		// return length
		return std::max(ell1,ell2);
	}

	// calculation
	void PathBSpline::get_frame_from_time(
		arma::Mat<fltp> &R, arma::Mat<fltp> &L, 
		arma::Mat<fltp> &N, arma::Mat<fltp> &D,
		const arma::Row<fltp> &t, const bool make_regular) const{

		// check input
		assert(str1_>0);
		assert(str2_>0);

		// get anchor 1 and anchor 2 coordinates
		arma::Col<fltp>::fixed<3> anchor1 = pstart_ + bc_*str1_*lstart_;
		arma::Col<fltp>::fixed<3> anchor2 = pend_ - bc_*str2_*lend_;

		// calculate parameters connecting the shapes
		arma::Col<fltp>::fixed<3> c = 3*(anchor1-pstart_);
		arma::Col<fltp>::fixed<3> b = 3*(anchor2-anchor1)-c;
		arma::Col<fltp>::fixed<3> a = pend_-pstart_-c-b;  

		// use regular time arrays
		arma::Row<fltp> tt;
		if(make_regular)tt = regular_times(a,b,c,t); else tt = t;

		// calculate length
		R = calculate_coords(a,b,c,tt);
		L = calculate_direction(a,b,c,tt);
		
		// normalization
		L.each_row()/=cmn::Extra::vec_norm(L);

		// apply offset
		R.each_col()+=pstart_;
		
		// allocate acceleration vectors
		arma::Mat<fltp> A(3,R.n_cols);

		// walk over nodes
		if(!in_plane_){
			// walk over nodes and set acceleration
			A.col(0) = -nstart_;
			for(arma::uword i=1;i<R.n_cols;i++){
				A.col(i) = cmn::Extra::cross(cmn::Extra::cross(A.col(i-1),-L.col(i)),L.col(i));
			}
		}else{
			// get out of plane vector
			arma::Col<fltp>::fixed<3> plane_vec = cmn::Extra::cross(lstart_,nstart_);
			assert(std::abs(arma::as_scalar(cmn::Extra::dot(plane_vec,lend_))-RAT_CONST(1.0))>1e-6);

			// walk over nodes and set acceleration
			for(arma::uword i=0;i<R.n_cols;i++){
				A.col(i) = cmn::Extra::cross(L.col(i),plane_vec);
			}
		}

		// normal vector
		// arma::Mat<fltp> A = calculate_acceleration(a,b,c,tt);
		D = cmn::Extra::cross(L,A);

		// normalization
		L.each_row()/=cmn::Extra::vec_norm(L);
		D.each_row()/=cmn::Extra::vec_norm(D);

		// calculate normal vectors
		N = cmn::Extra::cross(L,D);

		// calculate direction of rotation
		const fltp sign1 = cmn::Extra::sign(arma::as_scalar(cmn::Extra::dot(L.head_cols(1),cmn::Extra::cross(N.head_cols(1), nstart_))));
		const fltp sign2 = cmn::Extra::sign(arma::as_scalar(cmn::Extra::dot(L.tail_cols(1),cmn::Extra::cross(N.tail_cols(1), nend_))));

		// apply rotation if needed
		const fltp rot1 = sign1*std::acos(std::max(-1.0,std::min(1.0,arma::as_scalar(cmn::Extra::dot(N.head_cols(1),nstart_))/arma::as_scalar(cmn::Extra::vec_norm(N.head_cols(1))%cmn::Extra::vec_norm(nstart_)))));
		fltp rot2 = sign2*std::acos(std::max(-1.0,std::min(1.0,arma::as_scalar(cmn::Extra::dot(N.tail_cols(1),nend_))/arma::as_scalar(cmn::Extra::vec_norm(N.tail_cols(1))%cmn::Extra::vec_norm(nend_)))));

		// add extra twists
		rot2 += 2*num_twist_*arma::Datum<fltp>::pi;

		// check if rotation needed
		if(std::abs(rot1)>1e-9 || std::abs(rot2)>1e-9){
			// calculate rotation to apply using spline fit
			const fltp brot = 3*(rot2-rot1);
			const fltp arot = rot2-rot1-brot;
			const arma::Row<fltp> rot = arot*t%t%t + brot*t%t + rot1;

			// apply rotation
			for(arma::uword i=0;i<t.n_elem;i++){
				const arma::Mat<fltp>::fixed<3,3> M = cmn::Extra::create_rotation_matrix(L.col(i),rot(i));
				N.col(i) = M*N.col(i); D.col(i) = M*D.col(i);
			}
		}
	}

	// calculate coordinates
	arma::Row<fltp> PathBSpline::calculate_lengths(
		const arma::Col<fltp>::fixed<3> &a,
		const arma::Col<fltp>::fixed<3> &b,
		const arma::Col<fltp>::fixed<3> &c,
		const arma::Row<fltp> &t) const{

		// calculate
		arma::Mat<fltp> R = calculate_coords(a,b,c,t);

		// calculate length
		arma::Row<fltp> ell = arma::sqrt(arma::sum(arma::pow(arma::diff(R,1,1),2),0));

		// return coordinates
		return ell;
	}

	// regular times
	arma::Row<fltp> PathBSpline::regular_times(
		const arma::Col<fltp>::fixed<3> &a,
		const arma::Col<fltp>::fixed<3> &b,
		const arma::Col<fltp>::fixed<3> &c,
		const arma::Row<fltp> &treq) const{

		// calculate line with regular spaced times
		arma::Row<fltp> treg = arma::linspace<arma::Row<fltp> >(0,1,Nreg_);

		// calculate lengths for regular spaced times
		arma::Row<fltp> ell = calculate_lengths(a,b,c,treg);

		// calculate accumalation of length
		arma::Row<fltp> rp(Nreg_,arma::fill::zeros);
		rp.tail_cols(Nreg_-1) = arma::cumsum(ell)/arma::sum(ell);

		// allocate output
		arma::Col<fltp> tset;

		// use interpolation
		arma::interp1(rp.t(),treg.t(),treq.t(),tset,"linear");
		
		// fix first and last point
		tset(0) = 0; tset.tail(1) = 1;
		
		// check if sorted
		assert(tset.is_sorted("ascend"));
		
		// return time
		return tset.t();
	}


	// calculate coordinates
	arma::Mat<fltp> PathBSpline::calculate_coords(
		const arma::Col<fltp>::fixed<3> &a,
		const arma::Col<fltp>::fixed<3> &b,
		const arma::Col<fltp>::fixed<3> &c,
		const arma::Row<fltp> &t) const{

		// calculate
		arma::Mat<fltp> R(3,t.n_elem);
		for(arma::uword i=0;i<3;i++)
			R.row(i) = a(i)*t%t%t + b(i)*t%t + c(i)*t;

		// return coordinates
		return R;
	}

	// calculate direction
	arma::Mat<fltp> PathBSpline::calculate_direction(
		const arma::Col<fltp>::fixed<3> &a,
		const arma::Col<fltp>::fixed<3> &b,
		const arma::Col<fltp>::fixed<3> &c,
		const arma::Row<fltp> &t) const{

		// calculate
		arma::Mat<fltp> L(3,t.n_elem);
		for(arma::uword i=0;i<3;i++)
			L.row(i) = 3*a(i)*t%t + 2*b(i)*t + c(i);

		// return velocity
		return L;    
	}

	// calculate acceleration
	arma::Mat<fltp> PathBSpline::calculate_acceleration(
		const arma::Col<fltp>::fixed<3> &a,
		const arma::Col<fltp>::fixed<3> &b,
		const arma::Col<fltp>::fixed<3> &/*c*/,
		const arma::Row<fltp> &t) const{

		// calculate
		arma::Mat<fltp> A(3,t.n_elem);
		for(arma::uword i=0;i<3;i++)
			A.row(i) = 6*a(i)*t + 2*b(i);

		// return velocity
		return A;    
	}

	// set number of additional twists
	void PathBSpline::set_num_twist(const arma::sword num_twist){
		num_twist_ = num_twist;
	}

	// in plane
	void PathBSpline::set_in_plane(const bool in_plane){
		in_plane_ = in_plane;
	}

	// set path offset
	void PathBSpline::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	// set path offset
	void PathBSpline::set_offset(const fltp offset){
		offset_ = offset;
	}

}}