// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "crosswedge.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CrossWedge::CrossWedge(){
		set_name("Wedge");
	}

	// constructor with input
	CrossWedge::CrossWedge(
		const fltp dinner, 
		const fltp douter,
		const fltp t1, 
		const fltp t2, 
		const arma::uword num_thickness,
		const arma::uword num_width) : CrossWedge(){
		set_dinner(dinner);
		set_douter(douter);
		set_t1(t1);
		set_t2(t2);
		set_num_thickness(num_thickness);
		set_num_width(num_width);
	}

	// factory
	ShCrossWedgePr CrossWedge::create(){
		return std::make_shared<CrossWedge>();
	}

	// factory with dimension input
	ShCrossWedgePr CrossWedge::create(
		const fltp dinner, 
		const fltp douter,
		const fltp t1, 
		const fltp t2, 
		const arma::uword num_thickness,
		const arma::uword num_width){
		return std::make_shared<CrossWedge>(dinner,douter,t1,t2,num_thickness,num_width);
	}


	// setters
	void CrossWedge::set_dinner(const fltp dinner){
		dinner_ = dinner;
	}

	void CrossWedge::set_douter(const fltp douter){
		douter_ = douter;
	}
	 
	void CrossWedge::set_t1(const fltp t1){
		t1_ = t1;
	}

	void CrossWedge::set_t2(const fltp t2){
		t2_ = t2;
	}
	
	void CrossWedge::set_num_thickness(const arma::uword num_thickness){
		num_thickness_ = num_thickness;
	}
	
	void CrossWedge::set_num_width(const arma::uword num_width){
		num_width_ = num_width;
	}
	
	void CrossWedge::set_surface_offset(const fltp surface_offset){
		surface_offset_ = surface_offset;
	}


	// getters
	fltp CrossWedge::get_dinner()const{
		return dinner_;
	}
	 
	fltp CrossWedge::get_douter()const{
		return douter_;
	}
	 
	fltp CrossWedge::get_t1()const{
		return t1_;
	}

	fltp CrossWedge::get_t2()const{
		return t2_;
	}
	
	arma::uword CrossWedge::get_num_thickness()const{
		return num_thickness_;
	}
	 
	arma::uword CrossWedge::get_num_width()const{
		return num_width_;
	}

	fltp CrossWedge::get_surface_offset()const{
		return surface_offset_;
	}


	// get keystone angle
	fltp CrossWedge::calc_keystone_angle()const{
		return std::atan((douter_ - dinner_)/(2*(t2_ - t1_)));
	}

	// volume mesh
	ShAreaPr CrossWedge::create_area(const MeshSettings &stngs) const{
		// check validity
		is_valid(true);
		
		// calculate number of elements for each direction
		arma::uword num_width = num_width_;
		arma::uword num_thickness = num_thickness_;

		// calculate offsetted cable size
		const fltp keystone_angle = calc_keystone_angle();
		const fltp t1 = t1_ - surface_offset_;
		const fltp t2 = t2_ + surface_offset_;
		const fltp dinner = dinner_ - 2*surface_offset_*std::tan(keystone_angle) + 2*surface_offset_/std::cos(keystone_angle);
		const fltp douter = douter_ + 2*surface_offset_*std::tan(keystone_angle) + 2*surface_offset_/std::cos(keystone_angle);

		// override for low poly
		if(stngs.low_poly){num_width = 1; num_thickness = 1;}

		// set values
		const arma::Mat<fltp> u = arma::repmat(
			arma::linspace<arma::Row<fltp> >(-RAT_CONST(0.5),RAT_CONST(0.5),num_thickness+1),num_width+1,1).eval().each_col()
			%arma::linspace<arma::Col<fltp> >(dinner,douter,num_width+1);
		const arma::Mat<fltp> v = arma::repmat(arma::linspace<arma::Col<fltp> >(t1,t2,num_width+1),1,num_width+1);

		// calculate number of nodes
		const arma::uword num_nodes = (num_width+1)*(num_thickness+1);

		// create node coordinates
		arma::Mat<fltp> Rn(2,num_nodes);
		Rn.row(0) = arma::reshape(u,1,num_nodes);
		Rn.row(1) = arma::reshape(v,1,num_nodes);

		// create matrix of node indices
		arma::Mat<arma::uword> node_idx = 
			arma::regspace<arma::Mat<arma::uword> >(0,num_nodes-1);
		node_idx.reshape(num_width+1, num_thickness+1);

		// get corner matrices
		const arma::Mat<arma::uword> M0 = node_idx.submat(0, 0, num_width-1, num_thickness-1);
		const arma::Mat<arma::uword> M1 = node_idx.submat(1, 0, num_width, num_thickness-1);
		const arma::Mat<arma::uword> M2 = node_idx.submat(1, 1, num_width, num_thickness);
		const arma::Mat<arma::uword> M3 = node_idx.submat(0, 1, num_width-1, num_thickness);

		// reshape into element matrix
		const arma::Mat<arma::uword> n = arma::join_vert(
			arma::vectorise(M0).t(), arma::vectorise(M1).t(),
			arma::vectorise(M2).t(), arma::vectorise(M3).t());

		// check 
		assert(n.n_rows==4);

		// create orientaton
		arma::Mat<fltp> N(2,Rn.n_cols,arma::fill::zeros); N.row(0).fill(RAT_CONST(1.0));
		arma::Mat<fltp> D(2,Rn.n_cols,arma::fill::zeros); D.row(1).fill(RAT_CONST(1.0));

		// return positive
		const ShAreaPr area = Area::create(Rn,N,D,n);

		// set center coord
		area->set_center_coord({0,(t1 + t2)/2});

		// setup
		area->setup_perimeter();
		area->setup_corner_nodes();

		// // return the area
		return area;
	}

	// get bounding box
	arma::Mat<fltp>::fixed<2,2> CrossWedge::get_bounding() const{
		arma::Mat<fltp>::fixed<2,2> bnd;
		const fltp keystone_angle = calc_keystone_angle();
		const fltp dinner = dinner_ - 2*surface_offset_*std::tan(keystone_angle) + 2*surface_offset_/std::cos(keystone_angle);
		const fltp douter = douter_ + 2*surface_offset_*std::tan(keystone_angle) + 2*surface_offset_/std::cos(keystone_angle);
		bnd.col(0) = arma::Col<fltp>::fixed<2>{-std::max(dinner,douter)/2,std::max(dinner,douter)/2};
		bnd.col(1) = arma::Col<fltp>::fixed<2>{t1_-surface_offset_,t2_+surface_offset_};
		return bnd;
	}


	// validity check
	bool CrossWedge::is_valid(const bool enable_throws) const{
		// 
		if(dinner_<=0){if(enable_throws){rat_throw_line("inner cable thickness must be positive");} return false;};
		if(douter_<=0){if(enable_throws){rat_throw_line("outer thickness must be positive");} return false;};
		if(t2_<=t1_){if(enable_throws){rat_throw_line("cable width must be positive");} return false;};
		if(num_thickness_<=0){if(enable_throws){rat_throw_line("number of elements in the thickness direction must be positive");} return false;};
		if(num_width_<=0){if(enable_throws){rat_throw_line("number of elements in the width direction must be positive");} return false;};
		if(dinner_ - 2*surface_offset_*std::tan(calc_keystone_angle()) + 2*surface_offset_/std::cos(calc_keystone_angle())<=0.0){if(enable_throws){rat_throw_line("off-setted cable has negative inner thickness");} return false;};
		if(douter_ + 2*surface_offset_*std::tan(calc_keystone_angle()) + 2*surface_offset_/std::cos(calc_keystone_angle())<=0.0){if(enable_throws){rat_throw_line("off-setted cable has negative outer thickness");} return false;};
		return true;
	}

	// get type
	std::string CrossWedge::get_type(){
		return "rat::mdl::crosswedge";
	}

	// method for serialization into json
	void CrossWedge::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Cross::serialize(js,list);

		// properties
		js["type"] = get_type();
			
		// properties
		js["dinner"] = dinner_;
		js["douter"] = douter_;
		js["t1"] = t1_;
		js["t2"] = t2_;
		js["num_width"] = static_cast<unsigned int>(num_width_);
		js["num_thickness"] = static_cast<unsigned int>(num_thickness_);
		js["surface_offset"] = surface_offset_;
	}

	// method for deserialisation from json
	void CrossWedge::deserialize (
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth) {
		
		// parent
		Cross::deserialize(js,list,factory_list,pth);

		// properties
		set_dinner(js["dinner"].ASFLTP());
		set_douter(js["douter"].ASFLTP());
		set_t1(js["t1"].ASFLTP());
		set_t2(js["t2"].ASFLTP());
		set_num_width(js["num_width"].asUInt64());
		set_num_thickness(js["num_thickness"].asUInt64());
		set_surface_offset(js["surface_offset"].ASFLTP());
	}

}}
