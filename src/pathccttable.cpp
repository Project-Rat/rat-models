// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathccttable.hh"

// rat-common headers
#include "rat/common/extra.hh"

// rat-models headers
#include "driveinterp.hh"
#include "driveinterpbcr.hh"
#include "drivedc.hh"
#include "pathcctcustom.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathCCTTable::PathCCTTable(){
		set_name("CCT Table");
	}

	// constructor
	PathCCTTable::PathCCTTable(
		const boost::filesystem::path& table_file) : PathCCTTable(){
		
		// set parameters
		set_table_file(table_file);
	}

	// factory
	ShPathCCTTablePr PathCCTTable::create(){
		return std::make_shared<PathCCTTable>();
	}

	// factory with dimensional input
	ShPathCCTTablePr PathCCTTable::create(
		const boost::filesystem::path& table_file){
		return std::make_shared<PathCCTTable>(table_file);
	}

	// setters
	void PathCCTTable::set_table_file(const boost::filesystem::path& table_file){
		table_file_ = table_file;
	}

	void PathCCTTable::set_normalize_length(const bool normalize_length){
		normalize_length_ = normalize_length;
	}

	void PathCCTTable::set_use_omega_normal(const bool use_omega_normal){
		use_omega_normal_ = use_omega_normal;
	}

	void PathCCTTable::set_num_layers(const arma::uword num_layers){
		num_layers_ = num_layers;
	}

	void PathCCTTable::set_rho_increment(const fltp rho_increment){
		rho_increment_ = rho_increment;
	}

	void PathCCTTable::set_num_nodes_per_turn(const arma::uword num_nodes_per_turn){
		num_nodes_per_turn_ = num_nodes_per_turn;
	}

	void PathCCTTable::set_use_bcr_interp(const bool use_bcr_interp){
		use_bcr_interp_ = use_bcr_interp;
	}

	void PathCCTTable::set_bcr_order(const arma::uword bcr_order){
		bcr_order_ = bcr_order;
	}

	void PathCCTTable::set_num_repeat(const arma::uword num_repeat){
		num_repeat_ = num_repeat;
	}

	void PathCCTTable::set_use_angle(const bool use_angle){
		use_angle_ = use_angle;
	}

	void PathCCTTable::set_field_style(const bool field_style){
		field_style_ = field_style;
	}

	void PathCCTTable::set_use_frenet_serret(const bool use_frenet_serret){
		use_frenet_serret_ = use_frenet_serret;
	}



	// getters
	const boost::filesystem::path& PathCCTTable::get_table_file()const{
		return table_file_;
	}

	bool PathCCTTable::get_normalize_length()const{
		return normalize_length_;
	}

	bool PathCCTTable::get_use_omega_normal()const{
		return use_omega_normal_;
	}

	arma::uword PathCCTTable::get_num_layers()const{
		return num_layers_;
	}

	fltp PathCCTTable::get_rho_increment()const{
		return rho_increment_;
	}

	arma::uword PathCCTTable::get_num_nodes_per_turn()const{
		return num_nodes_per_turn_;
	}

	bool PathCCTTable::get_use_bcr_interp()const{
		return use_bcr_interp_;
	}

	arma::uword PathCCTTable::get_bcr_order()const{
		return bcr_order_;
	}

	arma::uword PathCCTTable::get_num_repeat()const{
		return num_repeat_;
	}
	
	bool PathCCTTable::get_use_angle()const{
		return use_angle_;
	}

	bool PathCCTTable::get_use_frenet_serret()const{
		return use_frenet_serret_;
	}

	bool PathCCTTable::get_field_style()const{
		return field_style_;
	}


	// create interpolation drive
	ShDrivePr PathCCTTable::create_interp_drive(
		const arma::Col<fltp> &turn, 
		const arma::Col<fltp> &value)const{

		// all values are the same
		if(arma::all(value==value(0))){
			return DriveDC::create(value(0));
		}

		// barycentric rational interpolation
		else if(use_bcr_interp_){
			return DriveInterpBCR::create(turn, value, bcr_order_);
		}

		// linear interpolation
		else{
			return DriveInterp::create(turn, value);
		}
	}


	// get frame
	ShFramePr PathCCTTable::create_frame(const MeshSettings &stngs) const{
		// validity check
		is_valid(true);

		// load the table from the input file
		arma::Mat<fltp> M;
		arma::field<std::string> header;
		const bool ok = M.load(arma::csv_name(table_file_.string(), header)); // auto detect type
		if(!ok)rat_throw_line("could not load table from: " + table_file_.string());

		// check matrix
		if(M.empty())rat_throw_line("table is empty");
		if(M.n_cols!=25)rat_throw_line("table has incorrect number of columns");
		if(M.n_rows<2)rat_throw_line("table must have at least two rows");

		// drop the last rows if they are all zeros
		// this is required as excel saves a empty lines at
		// the end of the file and this is currently not captured
		// by the armadillo loading routine
		arma::uword idx=M.n_rows;
		for(;idx>0;idx--){
			if(arma::any(M.row(idx-1)!=0))break;
		}
		M.resize(idx,M.n_cols);

		// check table rows
		if(use_bcr_interp_ && M.n_rows<=bcr_order_)
			rat_throw_line("with barycentric rational interpolation number of table rows must be larger than bcr order: " + std::to_string(M.n_rows) + "/" + std::to_string(bcr_order_));

		// repeat table
		if(num_repeat_>1){
			const arma::Col<fltp> theta = M.col(1);
			const fltp theta1 = theta.front();
			const fltp theta2 = theta.back();
			const fltp shift = theta2 - theta1;
			const arma::uword num_rows = M.n_rows;
			arma::field<arma::Mat<fltp> > Mfld(num_repeat_);
			for(arma::uword i=0;i<num_repeat_;i++){
				Mfld(i) = M.rows(0,i==num_repeat_-1 ? num_rows-1 : num_rows-2);
				Mfld(i).col(1) += i*shift;
			}
			M = rat::cmn::Extra::field2mat(Mfld);
		}

		// get columns
		// const arma::Col<fltp> index = M.col(0);
		const arma::Col<fltp> theta = M.col(1); // convert from deg to rad
		const arma::Col<fltp> pitch = M.col(2);
		const arma::Col<fltp> radius = M.col(3);
		const arma::Col<fltp> twist = M.col(4)*arma::Datum<fltp>::tau/360; // convert from deg to rad
		const arma::Mat<fltp> skew = M.cols(5,14);
		const arma::Mat<fltp> main = M.cols(15,24);
		M.clear();

		// check
		if(!theta.is_sorted("ascend"))rat_throw_line("theta does not ascend");

		// convert theta to turn
		const arma::Col<fltp> turn = theta/arma::Datum<fltp>::tau;
		// turn -= (turn.back() + turn.front())/2;

		// create a custom cct path
		const ShPathCCTCustomPr cct = PathCCTCustom::create();
		cct->set_range(turn.front(), turn.back(), turn.front(), turn.back());
		cct->set_normalize_length(normalize_length_);
		cct->set_use_omega_normal(use_omega_normal_);
		cct->set_use_field_style_omega(field_style_);
		cct->set_num_layers(num_layers_);
		cct->set_rho_increment(rho_increment_);
		cct->set_num_nodes_per_turn(num_nodes_per_turn_);
		// cct->set_use_use_frenet_serret(true);

		// set pitch
		cct->set_omega(create_interp_drive(turn, pitch));

		// set radius
		cct->set_rho(create_interp_drive(turn, radius));

		// set twist
		cct->set_twist(create_interp_drive(turn, twist));

		// set skew harmonics
		for(arma::uword i=0;i<skew.n_cols;i++){
			if(arma::any(skew.col(i)!=0)){
				// convert to amplitude
				const arma::Col<fltp> strength = use_angle_ ? skew.col(i)*arma::Datum<fltp>::tau/360 : skew.col(i).eval();
				const ShCCTHarmonicPr harmonic = CCTHarmonicDrive::create(i+1,true,create_interp_drive(turn, strength),use_angle_);
				harmonic->set_normalize_length(normalize_length_);
				cct->add_harmonic(harmonic);
			}
		}

		// set main harmonics
		for(arma::uword i=0;i<main.n_cols;i++){
			if(arma::any(main.col(i)!=0)){
				const arma::Col<fltp> strength = use_angle_ ? main.col(i)*arma::Datum<fltp>::tau/360 : main.col(i).eval();
				const ShCCTHarmonicPr harmonic = CCTHarmonicDrive::create(i+1,false,create_interp_drive(turn, strength),use_angle_);
				harmonic->set_normalize_length(normalize_length_);
				cct->add_harmonic(harmonic);
			}
		}

		// frenet serret
		cct->set_use_frenet_serret(use_frenet_serret_);

		// create frame
		const ShFramePr frame = cct->create_frame(stngs);
		
		// transformations
		frame->apply_transformations(get_transformations(), stngs.time);

		// return the frame
		return frame;
	}

	// validity check
	bool PathCCTTable::is_valid(const bool enable_throws) const{
		boost::system::error_code ec;
		if(!boost::filesystem::exists(table_file_,ec)){if(enable_throws){rat_throw_line("table file does not exist");} return false;};
		if(num_repeat_==0){if(enable_throws){rat_throw_line("number of repeats must be larger than zero");} return false;};
		if(use_bcr_interp_){
			if(bcr_order_<=0){if(enable_throws){rat_throw_line("barycentric rational order must be larger than zero");} return false;};
		}
		return true;
	}

	// get type
	std::string PathCCTTable::get_type(){
		return "rat::mdl::pathccttable";
	}

	// method for serialization into json
	void PathCCTTable::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Transformations::serialize(js,list);

		// type
		js["type"] = get_type();

		// properties
		js["table_file"] = table_file_.string();

		// switches
		js["normalize_length"] = normalize_length_;
		js["use_omega_normal"] = use_omega_normal_;
		js["num_layers"] = static_cast<int>(num_layers_);
		js["rho_increment"] = rho_increment_;
		js["num_nodes_per_turn"] = static_cast<int>(num_nodes_per_turn_);
		js["bcr_order"] = static_cast<int>(bcr_order_);
		js["use_bcr_interp"] = use_bcr_interp_;
		js["num_repeat"] = static_cast<int>(num_repeat_);
		js["use_angle"] = use_angle_;
		js["use_frenet_serret"] = use_frenet_serret_;
		js["field_style"] = field_style_;
	}

	// method for deserialisation from json
	void PathCCTTable::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		
		// parent
		Transformations::deserialize(js,list,factory_list,pth);

		// get path
		boost::filesystem::path filepath(js["table_file"].asString());

		// check if path exists
		boost::system::error_code ec;
		if(!boost::filesystem::exists(filepath,ec)){
			if(boost::filesystem::exists(pth/filepath.filename(),ec))
				filepath = pth/filepath.filename();
		}

		// properties
		set_table_file(filepath);

		// switches
		normalize_length_ = js["normalize_length"].asBool();
		use_omega_normal_ = js["use_omega_normal"].asBool();
		num_layers_ = js["num_layers"].asUInt64();
		rho_increment_ = js["rho_increment"].ASFLTP();
		num_nodes_per_turn_ = js["num_nodes_per_turn"].asUInt64();
		bcr_order_ = js["bcr_order"].asUInt64();
		use_bcr_interp_ = js["use_bcr_interp"].asBool();
		if(js.isMember("num_repeat"))num_repeat_ = js["num_repeat"].asUInt64();
		if(js.isMember("use_angle"))use_angle_ = js["use_angle"].asBool();
		use_frenet_serret_ = js["use_frenet_serret"].asBool();
		field_style_ = js["field_style"].asBool();
	}


}}