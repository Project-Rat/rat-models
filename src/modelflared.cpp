// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelflared.hh"

#include "crossrectangle.hh"
#include "pathflared.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	ModelFlared::ModelFlared(){
		set_name("Flared");
	}

	// constructor with dimension input
	ModelFlared::ModelFlared(
		const fltp ell1, const fltp ell2, 
		const fltp arcl, const fltp radius1, 
		const fltp radius2, const fltp coil_width, 
		const fltp coil_thickness, const fltp element_size) : ModelFlared(){

		// set to self
		set_ell1(ell1);	set_ell2(ell2);
		set_arcl(arcl);	set_radius1(radius1); set_radius2(radius2);
		set_element_size(element_size);	
		set_coil_width(coil_width);
		set_coil_thickness(coil_thickness);
	}

	// factory
	ShModelFlaredPr ModelFlared::create(){
		//return ShModelFlaredPr(new ModelFlared);
		return std::make_shared<ModelFlared>();
	}

	// factory with dimension input
	ShModelFlaredPr ModelFlared::create(
		const fltp ell1, const fltp ell2, 
		const fltp arcl, const fltp radius1, 
		const fltp radius2, const fltp coil_width, 
		const fltp coil_thickness, const fltp element_size){
		return std::make_shared<ModelFlared>(ell1,ell2,arcl,
			radius1,radius2,coil_width,coil_thickness,element_size);
	}

	// get base and cross
	ShPathPr ModelFlared::get_input_path() const{
		// check input
		is_valid(true);

		// create circular path
		const rat::fltp offset_arc = 0.0;
		ShPathFlaredPr flared = PathFlared::create(
			ell1_,ell2_,arcl_,radius1_,radius2_,
			element_size_,coil_thickness_,offset_arc);

		// return the racetrack
		return flared;
	}

	// get cross section
	ShCrossPr ModelFlared::get_input_cross() const{
		// check input
		is_valid(true);

		// create rectangular cross
		ShCrossRectanglePr rectangle = CrossRectangle::create(
			0, coil_thickness_, 0, coil_width_, element_size_, element_size_);

		// return the rectangle
		return rectangle;
	}


	// set first length
	void ModelFlared::set_ell1(const fltp ell1){
		ell1_ = ell1;
	}

	// set second length
	void ModelFlared::set_ell2(const fltp ell2){
		ell2_ = ell2;
	}

	// set arc length
	void ModelFlared::set_arcl(const fltp arcl){
		arcl_ = arcl;
	}

	// set first radius
	void ModelFlared::set_radius1(const fltp radius1){
		radius1_ = radius1;
	}

	// set second radius
	void ModelFlared::set_radius2(const fltp radius2){
		radius2_ = radius2;
	}

	// set coil thickness
	void ModelFlared::set_coil_thickness(const fltp coil_thickness){
		coil_thickness_ = coil_thickness;
	}

	// set coil width
	void ModelFlared::set_coil_width(const fltp coil_width){
		coil_width_ = coil_width;
	}

	// set axial element size
	void ModelFlared::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}


	// set first length
	fltp ModelFlared::get_ell1() const{
		return ell1_;
	}

	// set second length
	fltp ModelFlared::get_ell2() const{
		return ell2_;
	}

	// set arc length
	fltp ModelFlared::get_arcl() const{
		return arcl_;
	}

	// set first radius
	fltp ModelFlared::get_radius1() const{
		return radius1_;
	}

	// set second radius
	fltp ModelFlared::get_radius2() const{
		return radius2_;
	}

	// get coil thickness
	fltp ModelFlared::get_coil_thickness()const{
		return coil_thickness_;
	}

	// get coil width
	fltp ModelFlared::get_coil_width()const{
		return coil_width_;
	}

	// get axial element size
	fltp ModelFlared::get_element_size()const {
		return element_size_;
	}

	
	// vallidity check
	bool ModelFlared::is_valid(const bool enable_throws) const{
		if(!ModelCoilWrapper::is_valid(enable_throws))return false;
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		if(ell1_<=0){if(enable_throws){rat_throw_line("first length must be larger than zero");} return false;};
		if(ell2_<=0){if(enable_throws){rat_throw_line("second length must be larger than zero");} return false;};
		if(arcl_==0){if(enable_throws){rat_throw_line("arc length must be larger than zero");} return false;};
		if(radius1_<=0){if(enable_throws){rat_throw_line("first radius must be larger than zero");} return false;};
		if(radius2_<=0){if(enable_throws){rat_throw_line("second radius must be larger than zero");} return false;};
		if(coil_thickness_<=0){if(enable_throws){rat_throw_line("coil thickness must be larger than zero");} return false;};
		if(coil_width_<=0){if(enable_throws){rat_throw_line("coil width must be larger than zero");} return false;};
		return true;
	}

	// get type
	std::string ModelFlared::get_type(){
		return "rat::mdl::modelflared";
	}

	// method for serialization into json
	void ModelFlared::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		ModelCoilWrapper::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["ell1"] = ell1_;
		js["ell2"] = ell2_;
		js["arcl"] = arcl_;
		js["radius1"] = radius1_;
		js["radius2"] = radius2_;
		js["coil_thickness"] = coil_thickness_;
		js["coil_width"] = coil_width_;
		js["element_size"] = element_size_;
	}

	// method for deserialisation from json
	void ModelFlared::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent
		ModelCoilWrapper::deserialize(js,list,factory_list,pth);

		// properties
		set_ell1(js["ell1"].ASFLTP());
		set_ell2(js["ell2"].ASFLTP());
		set_arcl(js["arcl"].ASFLTP());
		set_radius1(js["radius1"].ASFLTP());
		set_radius2(js["radius2"].ASFLTP());
		set_coil_thickness(js["coil_thickness"].ASFLTP());
		set_coil_width(js["coil_width"].ASFLTP());
		set_element_size(js["element_size"].ASFLTP());
	}

}}
