// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathunfold.hh"

// rat-common headers
#include "rat/common/extra.hh"
#include "rat/common/error.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathUnfold::PathUnfold(){
		set_name("Unfold");
	}

	PathUnfold::PathUnfold(const ShPathPr &base) : PathUnfold(){
		set_input_path(base);
	}

	// factory
	ShPathUnfoldPr PathUnfold::create(){
		return std::make_shared<PathUnfold>();
	}

	ShPathUnfoldPr PathUnfold::create(const ShPathPr &base){
		return std::make_shared<PathUnfold>(base);
	}

	// get frame
	ShFramePr PathUnfold::create_frame(const MeshSettings &stngs) const{
		// create frame
		const ShFramePr base_frame = input_path_->create_frame(stngs);

		// combine sections
		base_frame->combine();

		// coordinates and vectors
		arma::Mat<fltp> R = base_frame->get_coords(0);
		arma::Mat<fltp> L = base_frame->get_direction(0);
		arma::Mat<fltp> N = base_frame->get_normal(0);
		arma::Mat<fltp> D = base_frame->get_transverse(0);
		arma::Mat<fltp> B = base_frame->get_block(0);

		// get number of coordinates
		const arma::uword num_coords = R.n_cols;

		// walk over coordinates
		for(arma::uword i=1;i<num_coords-1;i++){
			// get adjacent direction vectors
			const arma::Col<fltp>::fixed<3> L1 = R.col(i) - R.col(i-1);
			const arma::Col<fltp>::fixed<3> L2 = R.col(i+1) - R.col(i);
			assert(L1.is_finite());
			assert(L2.is_finite());
			if(arma::any(cmn::Extra::vec_norm(L1)<1e-9))rat_throw_line("vector is zero");
			if(arma::any(cmn::Extra::vec_norm(L2)<1e-9))rat_throw_line("vector is zero");

			// get darboux vector
			const arma::Col<fltp>::fixed<3> db = D.col(i);
			if(arma::any(cmn::Extra::vec_norm(db)<1e-9))rat_throw_line("vector is zero");

			// get the two adjacent normal vectors
			const arma::Col<fltp>::fixed<3> V1 = cmn::Extra::cross(L1,db);
			const arma::Col<fltp>::fixed<3> V2 = cmn::Extra::cross(L2,db);
			assert(V1.is_finite());
			assert(V2.is_finite());

			// create rotation matrix with this angle around the darboux vector
			const arma::Mat<fltp>::fixed<3,3> M = cmn::Extra::create_rotation_matrix(V2,V1); // rotate V2 onto V1
			assert(M.is_finite());

			// center of rotation
			const arma::Col<fltp>::fixed<3> rotation_origin = R.col(i);
			assert(rotation_origin.is_finite());

			// rotate remaining coordinates
			R.cols(i+1,num_coords-1) = (M*(R.cols(i+1,num_coords-1).eval().each_col() - rotation_origin)).eval().each_col() + rotation_origin;
			
			// rotate remaining vectors
			L.cols(i+1,num_coords-1) = M*L.cols(i+1,num_coords-1);
			D.cols(i+1,num_coords-1) = M*D.cols(i+1,num_coords-1);
			N.cols(i+1,num_coords-1) = M*N.cols(i+1,num_coords-1);
			B.cols(i+1,num_coords-1) = M*B.cols(i+1,num_coords-1);
		}

		// check handedness
		assert(arma::all(cmn::Extra::dot(cmn::Extra::cross(N,L),D)>RAT_CONST(0.0)));
		
		// create new frame
		const ShFramePr frame = Frame::create(R,L,N,D,B);

		// return the frame
		return frame;
	}

	// get type
	std::string PathUnfold::get_type(){
		return "rat::mdl::pathunfold";
	}

	// method for serialization into json
	void PathUnfold::serialize(Json::Value &js, cmn::SList &list) const{
		// type
		InputPath::serialize(js,list);
		js["type"] = get_type();
	}

	// method for deserialisation from json
	void PathUnfold::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		InputPath::deserialize(js,list,factory_list,pth);
	}

}}
