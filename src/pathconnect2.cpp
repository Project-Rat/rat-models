// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathconnect2.hh"

// common headers
#include "rat/common/extra.hh"

// models headers
#include "transflip.hh"

// include optimizer
#include "nlopt.hpp"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathConnect2::PathConnect2(){
		set_name("Connection Optimizer");
	}

	// constructor with dimension input
	PathConnect2::PathConnect2(
		const ShPathPr &prev_path, 
		const ShPathPr &next_path, 
		const fltp ell,
		const fltp ell_trans1, 
		const fltp ell_trans2, 
		const fltp element_size) : PathConnect2(){

		//check input
		assert(prev_path!=NULL);
		assert(next_path!=NULL);

		// store to self
		add_path(prev_path);
		add_path(next_path);
		
		// set length
		ell_ = ell;

		// set transition length
		ell_trans1_ = ell_trans1;
		ell_trans2_ = ell_trans2;

		// set element size
		element_size_ = element_size;
	}


	// factory
	ShPathConnect2Pr PathConnect2::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<PathConnect2>();
	}

	// factory with dimension input
	ShPathConnect2Pr PathConnect2::create(
		const ShPathPr &prev_path, 
		const ShPathPr &next_path, 
		const fltp ell,
		const fltp ell_trans1, 
		const fltp ell_trans2, 
		const fltp element_size){
		//return ShPathCirclePr(new PathCircle(radius,dl));
		return std::make_shared<PathConnect2>(prev_path,next_path,ell,ell_trans1,ell_trans2,element_size);
	}

	// retreive model at index
	ShPathPr PathConnect2::get_path(const arma::uword index) const{
		auto it = input_paths_.find(index);
		if(it==input_paths_.end())rat_throw_line("index does not exist");
		return (*it).second;
	}

	// delete model at index
	bool PathConnect2::delete_path(const arma::uword index){	
		auto it = input_paths_.find(index);
		if(it==input_paths_.end())return false;
		(*it).second = NULL; return true;
	}

	// re-index nodes after deleting
	void PathConnect2::reindex(){
		Transformations::reindex();
		std::map<arma::uword, ShPathPr> new_paths; arma::uword idx = 1;
		for(auto it=input_paths_.begin();it!=input_paths_.end();it++)
			if((*it).second!=NULL)new_paths.insert({idx++, (*it).second});
		input_paths_ = new_paths;
	}

	// function for adding a path to the path list
	arma::uword PathConnect2::add_path(const ShPathPr &path){
		// check input
		assert(path!=NULL); 

		// get counters
		const arma::uword index = input_paths_.size()+1;

		// insert
		input_paths_.insert({index, path});

		// return index
		return index;
	}

	// get number of paths
	arma::uword PathConnect2::get_num_paths() const{
		return input_paths_.size();
	}

	// setters
	void PathConnect2::set_uvw1(const arma::Mat<fltp>&uvw1){
		uvw1_ = uvw1;
	}

	void PathConnect2::set_uvw2(const arma::Mat<fltp>&uvw2){
		uvw2_ = uvw2;
	}

	void PathConnect2::set_ell_trans1(const fltp ell_trans1){
		ell_trans1_ = ell_trans1;
	}

	void PathConnect2::set_ell_trans2(const fltp ell_trans2){
		ell_trans2_ = ell_trans2;
	}

	void PathConnect2::set_element_size(const fltp element_size){
		element_size_ = element_size;
	}

	void PathConnect2::set_order(const arma::uword order){
		order_ = order;
	}

	void PathConnect2::set_ell(const fltp ell){
		ell_ = ell;
	}

	void PathConnect2::set_exact_length(const bool exact_length){
		exact_length_ = exact_length;
	}

	void PathConnect2::set_use_previous(const bool use_previous){
		use_previous_ = use_previous;
	}

	void PathConnect2::set_is_symmetric(const bool is_symmetric){
		is_symmetric_ = is_symmetric;
	}

	void PathConnect2::set_enable_w(const bool enable_w){
		enable_w_ = enable_w;
	}

	void PathConnect2::set_flip_first(const bool flip_first){
		flip_first_ = flip_first;
	}

	void PathConnect2::set_flip_second(const bool flip_second){
		flip_second_ = flip_second;
	}

	void PathConnect2::set_min_bending_radius(const fltp min_bending_radius){
		min_bending_radius_ = min_bending_radius;
	}

	void PathConnect2::set_t1(const fltp t1){
		t1_ = t1;
	}

	void PathConnect2::set_t2(const fltp t2){
		t2_ = t2;
	}

	void PathConnect2::set_ft(const fltp ft){
		ft_ = ft;
	}

	void PathConnect2::set_fn(const fltp fn){
		fn_ = fn;
	}

	void PathConnect2::set_fg(const fltp fg){
		fg_ = fg;
	}

	void PathConnect2::set_xtol(const fltp xtol){
		xtol_ = xtol;
	}

	void PathConnect2::set_use_binormal(const bool use_binormal){
		use_binormal_ = use_binormal;
	}

	void PathConnect2::set_strain_energy_exponent(const fltp strain_energy_exponent){
		strain_energy_exponent_ = strain_energy_exponent;
	}



	// getters
	const arma::Mat<fltp>& PathConnect2::get_uvw1()const{
		return uvw1_;
	}

	const arma::Mat<fltp>& PathConnect2::get_uvw2()const{
		return uvw2_;
	}

	fltp PathConnect2::get_ell_trans1()const{
		return ell_trans1_;
	}

	fltp PathConnect2::get_ell_trans2()const{
		return ell_trans2_;
	}

	fltp PathConnect2::get_element_size()const{
		return element_size_;
	}

	arma::uword PathConnect2::get_order()const{
		return order_;
	}

	fltp PathConnect2::get_ell()const{
		return ell_;
	}
	
	bool PathConnect2::get_exact_length()const{
		return exact_length_;
	}

	bool PathConnect2::get_use_previous()const{
		return use_previous_;
	}

	bool PathConnect2::get_is_symmetric()const{
		return is_symmetric_;
	}

	bool PathConnect2::get_enable_w()const{
		return enable_w_;
	}

	bool PathConnect2::get_flip_first()const{
		return flip_first_;
	}

	bool PathConnect2::get_flip_second()const{
		return flip_second_;
	}

	fltp PathConnect2::get_min_bending_radius()const{
		return min_bending_radius_;
	}

	fltp PathConnect2::get_t1()const{
		return t1_;
	}

	fltp PathConnect2::get_t2()const{
		return t2_;
	}

	fltp PathConnect2::get_xtol()const{
		return xtol_;
	}

	fltp PathConnect2::get_ft()const{
		return ft_;
	}

	fltp PathConnect2::get_fn()const{
		return fn_;
	}

	fltp PathConnect2::get_fg()const{
		return fg_;
	}

	bool PathConnect2::get_use_binormal()const{
		return use_binormal_;
	}

	fltp PathConnect2::get_strain_energy_exponent()const{
		return strain_energy_exponent_;
	}


	// validity check
	bool PathConnect2::is_valid(const bool enable_throws) const{
		if(xtol_<=0){if(enable_throws){rat_throw_line("tolerance must be larger than zero");} return false;};
		if(uvw1_.n_cols<4){if(enable_throws){rat_throw_line("need at least four points for uvw1");} return false;};
		if(uvw2_.n_cols<4){if(enable_throws){rat_throw_line("need at least four points for uvw2");} return false;};
		if(input_paths_.size()!=1 && input_paths_.size()!=2){if(enable_throws){rat_throw_line("need one or two input paths (no more)");} return false;};
		if(ell_trans1_<0){if(enable_throws){rat_throw_line("transition must be larger than or equal to zero");} return false;};
		if(ell_trans2_<0){if(enable_throws){rat_throw_line("transition must be larger than or equal to zero");} return false;};
		if(ell_<=0){if(enable_throws){rat_throw_line("length must be larger than zero");} return false;};
		if(t2_<=t1_){if(enable_throws){rat_throw_line("t2 must be larger than t1");} return false;};
		if(element_size_<=0){if(enable_throws){rat_throw_line("element size must be larger than zero");} return false;};
		if(strain_energy_exponent_<=0.0){if(enable_throws){rat_throw_line("strain energy exponent must be larger than zero");} return false;};
		if(order_<3){if(enable_throws){rat_throw_line("order must be at least cubic (three)");} return false;};
		for(auto it=input_paths_.begin();it!=input_paths_.end();it++)if(!(*it).second->is_valid(enable_throws))return false;
		return true;
	}

	// build bezier
	ShPathBezierPr PathConnect2::create_bezier(
		const ShFramePr& prev_gen, 
		const ShFramePr& next_gen)const{
		
		// get frame of first path
		const arma::field<arma::Mat<fltp> >& R0 = prev_gen->get_coords();
		const arma::field<arma::Mat<fltp> >& L0 = prev_gen->get_direction();
		const arma::field<arma::Mat<fltp> >& N0 = prev_gen->get_normal();
		const arma::field<arma::Mat<fltp> >& D0 = prev_gen->get_transverse();

		// get frame of second path
		const arma::field<arma::Mat<fltp> >& R1 = next_gen->get_coords();
		const arma::field<arma::Mat<fltp> >& L1 = next_gen->get_direction();
		const arma::field<arma::Mat<fltp> >& N1 = next_gen->get_normal();
		const arma::field<arma::Mat<fltp> >& D1 = next_gen->get_transverse();

		// start and end vectors
		const arma::uword num_first = R0.n_elem;
		const arma::Col<fltp>::fixed<3> r0 = R0(num_first-1).tail_cols(1);
		const arma::Col<fltp>::fixed<3> l0 = L0(num_first-1).tail_cols(1);
		const arma::Col<fltp>::fixed<3> n0 = N0(num_first-1).tail_cols(1);
		const arma::Col<fltp>::fixed<3> d0 = D0(num_first-1).tail_cols(1);
		const arma::Col<fltp>::fixed<3> r1 = R1(0).head_cols(1);
		const arma::Col<fltp>::fixed<3> l1 = L1(0).head_cols(1);
		const arma::Col<fltp>::fixed<3> n1 = N1(0).head_cols(1);
		const arma::Col<fltp>::fixed<3> d1 = D1(0).head_cols(1);

		// check input
		assert(uvw1_.is_finite());
		assert(uvw2_.is_finite());

		// check connection
		const fltp path_offset = RAT_CONST(0.0);
		const ShPathBezierPr bezier = PathBezier::create(
			r0, l0, n0, d0, r1, l1, n1, d1, uvw1_, uvw2_, 
			ell_trans1_, ell_trans2_, 
			element_size_, path_offset, use_binormal_);

		// return bezier
		return bezier;
	}

	// update function
	ShFramePr PathConnect2::create_frame(const MeshSettings &stngs) const{
		// check validity
		is_valid(true);

		// get paths
		ShPathPr prev_path, next_path;
		if(input_paths_.size()==1){
			prev_path = input_paths_.at(1);
			next_path = input_paths_.at(1);
		}else{
			prev_path = input_paths_.at(1);
			next_path = input_paths_.at(2);
		}

		// sanity check
		assert(prev_path!=NULL);
		assert(next_path!=NULL);

		// create frame
		const ShFramePr prev_gen = prev_path->create_frame(stngs);
		const ShFramePr next_gen = next_path->create_frame(stngs);

		// apply flip
		if(flip_first_)prev_gen->apply_transformation(TransFlip::create(),stngs.time);
		if(flip_second_)next_gen->apply_transformation(TransFlip::create(),stngs.time);

		// create bezier
		const ShPathBezierPr bezier = create_bezier(prev_gen,next_gen);

		// return frame for this path
		const ShFramePr gen = bezier->create_frame(stngs);

		// check for sign flips
		const bool flip12 = arma::as_scalar(cmn::Extra::dot(
			prev_gen->get_transverse().back().tail_cols(1),
			gen->get_transverse().front().head_cols(1)))<RAT_CONST(0.0);
		if(flip12)gen->apply_transformation(TransFlip::create(),stngs.time);
		if(prev_path!=next_path){
			const bool flip23 = arma::as_scalar(cmn::Extra::dot(
				gen->get_transverse().back().tail_cols(1),
				next_gen->get_transverse().front().head_cols(1)))<RAT_CONST(0.0);
			if(flip23)next_gen->apply_transformation(TransFlip::create(),stngs.time);
		}

		// transfer type
		gen->set_conductor_type(prev_gen->get_conductor_type());

		// create generator list
		std::list<ShFramePr> genlist{prev_gen,gen};
		if(prev_path!=next_path)genlist.push_back(next_gen);

		// combine frame and return
		const ShFramePr combined_frame = Frame::create(genlist);

		// apply transformations
		combined_frame->apply_transformations(get_transformations(), stngs.time);

		// return frame
		return combined_frame;
	}

	// objective function
	double PathConnect2::objective_fun(
		unsigned n, const double* x, 
		double* /*grad*/, void* f_stngs){

		// get settings
		UserData* user_data = static_cast<UserData*>(f_stngs);
		PathConnect2* stngs = user_data->stngs;
		assert(stngs!=NULL); assert(user_data->prev_gen!=NULL); assert(user_data->next_gen!=NULL);

		// calculate length
		stngs->set_uvw(n,x);

		// create bezier
		const arma::uword num_elem = std::min(1000llu, std::max(4llu,static_cast<arma::uword>(std::round(stngs->ell_/stngs->element_size_))));
		const ShPathBezierPr bezier = stngs->create_bezier(user_data->prev_gen,user_data->next_gen);
		const arma::Mat<fltp> P = bezier->create_control_points();
		const arma::Row<fltp> t = bezier->regular_times(P, bezier->get_element_size(), bezier->get_path_offset(), num_elem);
		const arma::field<arma::Mat<fltp> > C = PathBezier::create_bezier_tn(t,P,4);

		// get derivatives
		const arma::Mat<fltp>& R = C(0); // coords
		const arma::Mat<fltp>& V = C(1); // velocity
		const arma::Mat<fltp>& A = C(2); // acceleration
		const arma::Mat<fltp>& J = C(3); // jerk

		// safety check
		if(R.n_cols<4)return RAT_CONST(0.0);

		// normalize velocity
		const arma::Row<fltp> normV = cmn::Extra::vec_norm(V);
		const arma::Mat<fltp> T = V.each_row()/normV; // previously called T

		// setup frame
		const arma::Mat<fltp> B = cmn::Extra::cross(V,A); 
		const arma::Row<fltp> normB = cmn::Extra::vec_norm(B);

		// normal vector
		const arma::Mat<fltp> N = cmn::Extra::cross(T,B.each_row()/normB);
		const arma::Row<fltp> normN = cmn::Extra::vec_norm(N);

		// calculate 
		const fltp regularization = 1024*arma::Datum<fltp>::eps;
		// const arma::Row<fltp> kappa = normB/(normV%normV%normV);
		const arma::Row<fltp> tau = cmn::Extra::dot(B,J)/(normB%normB + regularization);

		// calculate position along length
		const arma::Row<fltp> ell = arma::join_horiz(arma::Row<fltp>{0.0},arma::cumsum(cmn::Extra::vec_norm(arma::diff(R,1,1))));

		// get components of K
		const arma::Row<fltp> kn = cmn::Extra::dot(B.each_row()/normB,cmn::Extra::cross(V,A))/(normV%normV%normV);
		const arma::Row<fltp> kg = cmn::Extra::dot(N.each_row()/normN,cmn::Extra::cross(V,A))/(normV%normV%normV);

		// convert
		// const arma::Row<fltp> ellstar = ell.cols(1,ell.n_elem-2);
		arma::Row<fltp> taustar = tau;
		arma::Row<fltp> kgstar = kg;
		arma::Row<fltp> knstar = kn;

		// integrate strain energy density
		const fltp strain_energy = 
			stngs->ft_*arma::as_scalar(arma::trapz(ell, arma::pow(arma::abs(taustar), stngs->strain_energy_exponent_), 1)) + 
			stngs->fn_*arma::as_scalar(arma::trapz(ell, arma::pow(arma::abs(knstar), stngs->strain_energy_exponent_), 1)) +
			stngs->fg_*arma::as_scalar(arma::trapz(ell, arma::pow(arma::abs(kgstar), stngs->strain_energy_exponent_), 1)); // J

		// report
		user_data->iter++;
		if((user_data->iter-1)%20==0){
			user_data->lg->msg("\n");
			user_data->lg->msg("%s%04llu %+10.2e%s",KCYN,user_data->iter,strain_energy,KNRM);
		}

		// make L and D orthogonal by minimizing them
		return strain_energy;
	}

	// constraint function
	void PathConnect2::edge_regression_constraint_fun(
		unsigned /*m*/, double *result, unsigned n, 
		const double* x, double* /*grad*/, void* f_stngs){

		// get settings
		UserData* user_data = static_cast<UserData*>(f_stngs);
		PathConnect2* stngs = user_data->stngs;
		assert(stngs!=NULL); assert(user_data->prev_gen!=NULL); assert(user_data->next_gen!=NULL);

		// calculate length
		stngs->set_uvw(n,x);

		// create frame and calculate length
		const ShPathBezierPr bezier = stngs->create_bezier(user_data->prev_gen,user_data->next_gen);
		const ShFramePr frame = bezier->create_frame(MeshSettings()); frame->combine();

		// get coordinates and darboux vectors
		const arma::Mat<fltp>& R = frame->get_coords(0);
		const arma::Mat<fltp>& D = frame->get_transverse(0);

		// walk over triangles
		arma::Row<fltp> edge_regression_violation_distance(R.n_cols-1);
		arma::Row<fltp> check(R.n_cols-1);
		for(arma::uword i=0;i<R.n_cols-1;i++){
			// get this triangle
			const arma::Col<fltp>::fixed<3> R1 = R.col(i);
			const arma::Col<fltp>::fixed<3> R2 = R.col(i+1);
			const arma::Col<fltp>::fixed<3> D1 = D.col(i);
			const arma::Col<fltp>::fixed<3> D2 = D.col(i+1);

			// calculate base of triangle
			const arma::Col<fltp>::fixed<3> dR12 = R2 - R1;

			// calculate angles of triangle corners
			const fltp alpha = std::acos(arma::as_scalar(cmn::Extra::dot(D1,dR12)/(cmn::Extra::vec_norm(D1)%cmn::Extra::vec_norm(dR12))));
			const fltp beta = arma::Datum<fltp>::pi - std::acos(arma::as_scalar(cmn::Extra::dot(D2,dR12)/(cmn::Extra::vec_norm(D2)%cmn::Extra::vec_norm(dR12))));

			// calculate lengths of the triangle sides
			const fltp ell12 = arma::as_scalar(cmn::Extra::vec_norm(dR12));
			const fltp ell1 = ell12/(std::cos(alpha) + (std::sin(alpha)/std::sin(beta))*std::cos(beta));
			const fltp ell2 = ell1*std::sin(alpha)/std::sin(beta);

			// calculate 
			const fltp ellt1 = stngs->t1_/std::sin(alpha);
			const fltp ellt2 = stngs->t2_/std::sin(alpha);
			const fltp ellt3 = stngs->t1_/std::sin(beta);
			const fltp ellt4 = stngs->t2_/std::sin(beta);

			// check
			edge_regression_violation_distance(i) = std::min(
				std::max(-ell1 + ellt1, ell1 - ellt2),
				std::max(-ell2 + ellt3, ell2 - ellt4));
		}

		// return length
		result[0] = -arma::min(edge_regression_violation_distance);

		// report
		if((user_data->iter-1)%20==0)user_data->lg->msg(0," %s%+10.2e%s",KCYN,result[0],KNRM);
	}

	// constraint function
	void PathConnect2::length_constraint_fun(
		unsigned /*m*/, double *result, unsigned n, 
		const double* x, double* /*grad*/, void* f_stngs){

		// get settings
		UserData* user_data = static_cast<UserData*>(f_stngs);
		PathConnect2* stngs = user_data->stngs;
		assert(stngs!=NULL); assert(user_data->prev_gen!=NULL); assert(user_data->next_gen!=NULL);

		// calculate length
		stngs->set_uvw(n,x);

		// create frame and calculate length
		const ShPathBezierPr bezier = stngs->create_bezier(user_data->prev_gen,user_data->next_gen);
		const ShFramePr frame = bezier->create_frame(MeshSettings()); frame->combine();

		// create frame and calculate length
		const fltp ell = frame->calc_total_ell();

		// return length
		result[0] = ell - stngs->ell_;

		// report
		if((user_data->iter-1)%20==0)user_data->lg->msg(0," %s%+10.2e%s",KCYN,result[0],KNRM);
	}

	// constraint function
	void PathConnect2::curvature_constraint_fun(
		unsigned /*m*/, double *result, unsigned n, 
		const double* x, double* /*grad*/, void* f_stngs){

		// get settings
		UserData* user_data = static_cast<UserData*>(f_stngs);
		PathConnect2* stngs = user_data->stngs;
		assert(stngs!=NULL); assert(user_data->prev_gen!=NULL); assert(user_data->next_gen!=NULL);

		// calculate length
		stngs->set_uvw(n,x);

		// create frame and calculate length
		const ShPathBezierPr bezier = stngs->create_bezier(user_data->prev_gen,user_data->next_gen);
		const ShFramePr frame = bezier->create_frame(MeshSettings()); frame->combine();

		// get coords
		const arma::Col<fltp>::fixed<3> R1 = user_data->prev_gen->get_coords().back().tail_cols(2).col(0);
		const arma::Col<fltp>::fixed<3> R2 = user_data->next_gen->get_coords().front().head_cols(2).col(1);
		const arma::Mat<fltp> R = arma::join_horiz(R1,frame->get_coords(0),R2);

		// create frame and calculate length
		arma::Mat<fltp> V = arma::diff(R,1,1);
		arma::Mat<fltp> A = arma::diff(V,1,1);
		V = (V.head_cols(V.n_cols-1) + V.tail_cols(V.n_cols-1))/2;

		// calculate radius
		const arma::Row<fltp> bending_radius = arma::pow(cmn::Extra::vec_norm(V),3)/
			arma::sqrt(arma::square(cmn::Extra::vec_norm(V))%arma::square(cmn::Extra::vec_norm(A)) - arma::square(cmn::Extra::dot(V,A)));

		// get minimum radius
		result[0] = -arma::min(bending_radius) + stngs->min_bending_radius_;

		// report
		if((user_data->iter-1)%20==0)user_data->lg->msg(0," %s%+10.2e%s",KCYN,result[0],KNRM);
	}

	// number of variables
	arma::uword PathConnect2::get_num_vars()const{
		arma::uword num_vars = 0;
		num_vars += order_;
		if(order_>3)num_vars += order_ - 3;
		if(enable_w_ && order_>6)num_vars += order_ - 6;
		if(!is_symmetric_)num_vars*=2;
		return num_vars;
	}

	// vectorize
	std::vector<double> PathConnect2::get_x0()const{
		arma::Col<fltp> x0(get_num_vars()); arma::uword cnt = 0;
		for(arma::uword i=1;i<4;i++)x0(cnt++) = uvw1_(0,i) - uvw1_(0,i-1);
		for(arma::uword i=4;i<uvw1_.n_cols;i++)x0(cnt++) = uvw1_(0,i) - uvw1_(0,i-1);
		for(arma::uword i=4;i<uvw1_.n_cols;i++)x0(cnt++) = uvw1_(1,i) - uvw1_(1,i-1);
		if(enable_w_)for(arma::uword i=7;i<uvw1_.n_cols;i++)x0(cnt++) = uvw1_(2,i) - uvw1_(2,i-1);
		if(!is_symmetric_){
			for(arma::uword i=1;i<4;i++)x0(cnt++) = uvw2_(0,i) - uvw2_(0,i-1);
			for(arma::uword i=4;i<uvw2_.n_cols;i++)x0(cnt++) = uvw2_(0,i) - uvw2_(0,i-1);
			for(arma::uword i=4;i<uvw2_.n_cols;i++)x0(cnt++) = uvw2_(1,i) - uvw2_(1,i-1);
			if(enable_w_)for(arma::uword i=7;i<uvw2_.n_cols;i++)x0(cnt++) = uvw2_(2,i) - uvw2_(2,i-1);
		}
		assert(cnt==x0.n_elem);
		return arma::conv_to<std::vector<double> >::from(x0);
	}

	// vectorize
	std::vector<double> PathConnect2::get_lb()const{
		arma::Col<fltp> lb(get_num_vars()); arma::uword cnt = 0;
		for(arma::uword i=1;i<4;i++)lb(cnt++) = ell_/1e3;
		for(arma::uword i=4;i<uvw1_.n_cols;i++)lb(cnt++) = 0.0; // was -ell_
		for(arma::uword i=4;i<uvw1_.n_cols;i++)lb(cnt++) = 0.0;
		if(enable_w_)for(arma::uword i=7;i<uvw1_.n_cols;i++)lb(cnt++) = -ell_;
		if(!is_symmetric_){
			for(arma::uword i=1;i<4;i++)lb(cnt++) = ell_/1e3;
			for(arma::uword i=4;i<uvw2_.n_cols;i++)lb(cnt++) = 0.0; // was -ell_
			for(arma::uword i=4;i<uvw2_.n_cols;i++)lb(cnt++) = 0.0;
			if(enable_w_)for(arma::uword i=7;i<uvw2_.n_cols;i++)lb(cnt++) = -ell_;
		}
		assert(cnt==lb.n_elem);
		return arma::conv_to<std::vector<double> >::from(lb);
	}

	// vectorize
	std::vector<double> PathConnect2::get_ub()const{
		arma::Col<fltp> ub(get_num_vars()); arma::uword cnt = 0;
		for(arma::uword i=1;i<4;i++)ub(cnt++) = ell_;
		for(arma::uword i=4;i<uvw1_.n_cols;i++)ub(cnt++) = ell_;
		for(arma::uword i=4;i<uvw1_.n_cols;i++)ub(cnt++) = ell_;
		if(enable_w_)for(arma::uword i=7;i<uvw1_.n_cols;i++)ub(cnt++) = ell_;
		if(!is_symmetric_){
			for(arma::uword i=1;i<4;i++)ub(cnt++) = ell_;
			for(arma::uword i=4;i<uvw2_.n_cols;i++)ub(cnt++) = ell_;
			for(arma::uword i=4;i<uvw2_.n_cols;i++)ub(cnt++) = ell_;
			if(enable_w_)for(arma::uword i=7;i<uvw2_.n_cols;i++)ub(cnt++) = ell_;
		}
		assert(cnt==ub.n_elem);
		return arma::conv_to<std::vector<double> >::from(ub);
	}

	// unvectorise
	void PathConnect2::set_uvw(unsigned n, const double* x){
		arma::uword cnt = 0;
		for(arma::uword i=1;i<4;i++)uvw1_(0,i) = uvw1_(0,i-1) + x[cnt++];
		for(arma::uword i=4;i<uvw1_.n_cols;i++)uvw1_(0,i) = uvw1_(0,i-1) + x[cnt++];
		for(arma::uword i=4;i<uvw1_.n_cols;i++)uvw1_(1,i) = uvw1_(1,i-1) + x[cnt++];
		if(enable_w_)for(arma::uword i=7;i<uvw1_.n_cols;i++)uvw1_(2,i) = uvw1_(2,i-1) + x[cnt++];
		if(!is_symmetric_){
			for(arma::uword i=1;i<4;i++)uvw2_(0,i) = uvw2_(0,i-1) + x[cnt++];
			for(arma::uword i=4;i<uvw2_.n_cols;i++)uvw2_(0,i) = uvw2_(0,i-1) + x[cnt++];
			for(arma::uword i=4;i<uvw2_.n_cols;i++)uvw2_(1,i) = uvw2_(1,i-1) + x[cnt++];
			if(enable_w_)for(arma::uword i=7;i<uvw2_.n_cols;i++)uvw2_(2,i) = uvw2_(2,i-1) + x[cnt++];
		}else{
			uvw2_ = uvw1_;
		}
		if(cnt!=n)rat_throw_line("incorrect number of parameters supplied");
	}



	// optimize
	void PathConnect2::optimize_control_points(const cmn::ShLogPr& lg){

		// message
		lg->msg(2, "%sCONTROL POINT OPTIMIZATION%s\n",KGRN,KNRM);

		// set uvw
		const bool is_solution = arma::any(arma::vectorise(uvw1_)!=0) || arma::any(arma::vectorise(uvw2_)!=0);
		const bool order_match = uvw1_.n_elem/3==order_+1 && uvw2_.n_elem/3==order_+1;
		if(use_previous_ && is_solution && order_match){
			uvw1_.reshape(3,order_+1); uvw2_.reshape(3,order_+1);
		}else{
			uvw1_.zeros(3,order_+1); uvw2_.zeros(3,order_+1);
			uvw1_.row(0) = arma::linspace<arma::Row<fltp> >(0.0,ell_/4,order_+1);
			uvw2_.row(0) = arma::linspace<arma::Row<fltp> >(0.0,ell_/4,order_+1);
			for(arma::uword i=4;i<order_+1;i++)uvw1_(1,i) = i*ell_/24;
			for(arma::uword i=4;i<order_+1;i++)uvw2_(1,i) = i*ell_/24;
		}

		// get paths
		ShPathPr prev_path, next_path;
		if(input_paths_.size()==1){
			prev_path = input_paths_.at(1);
			next_path = input_paths_.at(1);
		}else{
			prev_path = input_paths_.at(1);
			next_path = input_paths_.at(2);
		}

		// create mesh settings
		MeshSettings stngs;

		// create frame
		const ShFramePr prev_gen = prev_path->create_frame(stngs); prev_gen->combine();
		const ShFramePr next_gen = next_path->create_frame(stngs); next_gen->combine();

		// apply flip
		if(flip_first_)prev_gen->apply_transformation(TransFlip::create(),stngs.time);
		if(flip_second_)next_gen->apply_transformation(TransFlip::create(),stngs.time);

		// check distance
		if(arma::as_scalar(cmn::Extra::vec_norm(next_gen->get_coords(0).head_cols(1) - prev_gen->get_coords(0).tail_cols(1))>ell_))
			rat_throw_line("distance between start and end exceeds requested spline length");

		// create userdata
		UserData user_data{0llu,this,prev_gen,next_gen,lg};

		// get start point and bounds
		std::vector<double> x = get_x0();
		std::vector<double> lb = get_lb();
		std::vector<double> ub = get_ub();

		// clamp range
		for(arma::uword i=0;i<x.size();i++)x[i] = std::max(lb[i], std::min(x[i], ub[i])); 
		

		// run optimization
		nlopt::opt opt(nlopt::LN_COBYLA, static_cast<unsigned int>(get_num_vars()));
		opt.set_min_objective(PathConnect2::objective_fun, reinterpret_cast<void*>(&user_data));
		opt.add_inequality_mconstraint(PathConnect2::edge_regression_constraint_fun, reinterpret_cast<void*>(&user_data), {(t2_-t1_)/100.0});
		if(exact_length_)opt.add_equality_mconstraint(PathConnect2::length_constraint_fun, reinterpret_cast<void*>(&user_data), {ell_/1000.0});
		else opt.add_inequality_mconstraint(PathConnect2::length_constraint_fun, reinterpret_cast<void*>(&user_data), {ell_/1000.0});
		if(min_bending_radius_>0)opt.add_inequality_mconstraint(PathConnect2::curvature_constraint_fun, reinterpret_cast<void*>(&user_data), {min_bending_radius_/100.0});
		opt.set_xtol_rel(xtol_); 
		opt.set_lower_bounds(lb); 
		opt.set_upper_bounds(ub);
		opt.set_maxtime(5.0);
		opt.set_maxeval(500);
		// opt.set_population(50);

		// header
		lg->msg(2,"%sRun Non-Linear Optimization%s\n",KBLU,KNRM);
		lg->msg("\n");
		lg->msg("%sercf = edge regression inequality constraint function%s\n",KCYN,KNRM);
		lg->msg("%slcf  = length %s constraint function%s\n",KCYN,exact_length_ ? "equality" : "inequality",KNRM);
		if(min_bending_radius_>0)lg->msg("%sccf = curvature inequality constraint function%s\n",KCYN,KNRM);
		lg->msg("\n");
		if(min_bending_radius_>0)
			lg->msg("%s%4s %10s %10s %10s %10s%s",KCYN,"iter","fval","ercf","lcf","ccf",KNRM);
		else
			lg->msg("%s%4s %10s %10s %10s%s",KCYN,"iter","fval","ercf","lcf",KNRM);
		


		// run optimization in try catch block
		try{
			// target
			double opt_f = 0;

			// optimize
			const nlopt::result result = opt.optimize(x, opt_f);
			
			// analyse solver output
			lg->msg("\n");
			if(result==nlopt::SUCCESS)lg->msg("%ssuccess%s\n",KGRN,KNRM);
			if(result==nlopt::STOPVAL_REACHED)lg->msg("%sstopval reached%s\n",KGRN,KNRM);
			if(result==nlopt::FTOL_REACHED)lg->msg("%sftol reached%s\n",KGRN,KNRM);
			if(result==nlopt::XTOL_REACHED)lg->msg("%sxtol reached%s\n",KGRN,KNRM);
			if(result==nlopt::MAXEVAL_REACHED)lg->msg("%smaxeval reached%s\n",KYEL,KNRM);
			if(result==nlopt::MAXTIME_REACHED)lg->msg("%smaxtime reached%s\n",KYEL,KNRM);

			// set result
			if(uvw1_.is_finite() && uvw2_.is_finite())
				set_uvw(unsigned(x.size()),&x[0]);
		}

		// in case of an error catch and convert to rat errors
		catch(const std::runtime_error &err){
			rat_throw_line(std::string(err.what()));
		}
		catch(const std::invalid_argument &err){
			rat_throw_line(std::string(err.what()));
		}
		catch(const std::bad_alloc &err){
			rat_throw_line(std::string(err.what()));
		}

		// done
		lg->msg(-4,"\n");

		// std::cout<<uvw1_<<std::endl;
		// std::cout<<uvw2_<<std::endl;
	}

	// get type
	std::string PathConnect2::get_type(){
		return "rat::mdl::pathconnect2";
	}

	// method for serialization into json
	void PathConnect2::serialize(Json::Value &js, cmn::SList &list) const{

		// parent objects
		Path::serialize(js,list);
		Transformations::serialize(js,list);

		// type
		js["type"] = get_type();

		// serialize the models
		for(auto it = input_paths_.begin();it!=input_paths_.end();it++)
			js["input_paths"].append(cmn::Node::serialize_node((*it).second, list));

		// start control points
		for(arma::uword i=0;i<uvw1_.n_cols;i++){
			Json::Value jsuvw;
			jsuvw["u"] = uvw1_(0,i);
			jsuvw["v"] = uvw1_(1,i);
			jsuvw["w"] = uvw1_(2,i);
			js["uvw1"].append(jsuvw);
		}

		// end control points
		for(arma::uword i=0;i<uvw2_.n_cols;i++){
			Json::Value jsuvw;
			jsuvw["u"] = uvw2_(0,i);
			jsuvw["v"] = uvw2_(1,i);
			jsuvw["w"] = uvw2_(2,i);
			js["uvw2"].append(jsuvw);
		}

		// shape
		js["is_symmetric"] = is_symmetric_;
		js["enable_w"] = enable_w_;
		js["exact_length"] = exact_length_;
		js["use_previous"] = use_previous_;
		js["flip_first"] = flip_first_;
		js["flip_second"] = flip_second_;
		js["order"] = static_cast<int>(order_);
		js["ell"] = ell_;
		js["ell_trans1"] = ell_trans1_;
		js["ell_trans2"] = ell_trans2_;
		js["min_bending_radius"] = min_bending_radius_;
		js["t1"] = t1_; 
		js["t2"] = t2_;
		js["element_size"] = element_size_;
		js["xtol"] = xtol_;
		js["ft"] = ft_;
		js["fn"] = fn_;
		js["fg"] = fg_;
		js["use_binormal"] = use_binormal_;
	}

	// method for deserialisation from json
	void PathConnect2::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent objects
		Path::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);

		// new implementation
		for(auto it = js["input_paths"].begin();it!=js["input_paths"].end();it++)
			add_path(cmn::Node::deserialize_node<Path>(
				(*it), list, factory_list, pth));

		// start control points
		uvw1_.set_size(3,js["uvw1"].size()); arma::uword cnt1 = 0;
		for(auto it = js["uvw1"].begin();it!=js["uvw1"].end();it++,cnt1++){
			const Json::Value& jsuvw = (*it);
			uvw1_(0,cnt1) = jsuvw["u"].ASFLTP();
			uvw1_(1,cnt1) = jsuvw["v"].ASFLTP();
			uvw1_(2,cnt1) = jsuvw["w"].ASFLTP();
		}

		// end control points
		uvw2_.set_size(3,js["uvw2"].size()); arma::uword cnt2 = 0;
		for(auto it = js["uvw2"].begin();it!=js["uvw2"].end();it++,cnt2++){
			const Json::Value& jsuvw = (*it);
			uvw2_(0,cnt2) = jsuvw["u"].ASFLTP();
			uvw2_(1,cnt2) = jsuvw["v"].ASFLTP();
			uvw2_(2,cnt2) = jsuvw["w"].ASFLTP();
		}

		// parameters
		set_enable_w(js["enable_w"].asBool());
		set_exact_length(js["exact_length"].asBool());
		set_use_previous(js["use_previous"].asBool());
		set_is_symmetric(js["is_symmetric"].asBool());
		set_flip_first(js["flip_first"].asBool());
		set_flip_second(js["flip_second"].asBool());

		set_ell_trans1(js["ell_trans1"].ASFLTP());
		set_ell_trans2(js["ell_trans2"].ASFLTP());
		
		set_order(js["order"].asUInt64());
		
		set_ell(js["ell"].ASFLTP());
		set_min_bending_radius(js["min_bending_radius"].ASFLTP());
		set_t1(js["t1"].ASFLTP());
		set_t2(js["t2"].ASFLTP());
		set_element_size(js["element_size"].ASFLTP());
		set_xtol(js["xtol"].ASFLTP());
		set_ft(js["ft"].ASFLTP());
		set_fn(js["fn"].ASFLTP());
		set_fg(js["fg"].ASFLTP());
		set_use_binormal(js["use_binormal"].asBool());
	}

}}