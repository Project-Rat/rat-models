// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// only available for non-linear solver
#ifdef ENABLE_NL_SOLVER

// include header
#include "nldata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	NLData::NLData(){
		add_field_type('M',3); set_output_type("nl");
	}

	// constructor with input
	NLData::NLData(const ShFramePr &frame, const ShAreaPr &area) : NLData(){
		setup(frame, area);
		if(n_dim_!=3)rat_throw_line("NL mesh must be volumetric (3 dim)");
	}

	// factory
	ShNLDataPr NLData::create(){
		return std::make_shared<NLData>();
	}

	// factory with input
	ShNLDataPr NLData::create(const ShFramePr &frame, const ShAreaPr &area){
		return std::make_shared<NLData>(frame,area);
	}


	// setters
	void NLData::set_hb_data(const nl::ShHBDataPr& hb_data){
		hb_data_ = hb_data;
	}

	// getters
	const nl::ShHBDataPr& NLData::get_hb_data()const{
		return hb_data_;
	}

	// VTK export
	ShVTKObjPr NLData::export_vtk() const{
		// create unstructured grid
		const ShVTKUnstrPr vtk = std::dynamic_pointer_cast<VTKUnstr>(MeshData::export_vtk());
		assert(vtk!=NULL);

		// check if field calculated
		if(has('B') && has_field()){
			if(hb_data_!=NULL){
				vtk->set_nodedata(calc_relative_permeability(),"Relative Permeability");
				vtk->set_nodedata(calc_relative_reluctivity(),"Relative Reluctivity");
			}
		}

		// return the unstructured mesh
		return vtk;
	}

	// set magnetization
	void NLData::set_magnetisation(const arma::Mat<fltp>&M, const bool is_nodal){
		M_ = M; is_nodal_ = is_nodal;
	}

	// // calculate energy
	// // method described in:
	// // integrate over volume (ignoring coil energy here)
	// // Quentin Debray, Gerard Meunier, Olivier Chadebec, 
	// // Jean-Louis Coulomb and Anthony Carpentier, "An expression 
	// // of the Magnetic Co-Energy adapted to MagnetoStatic Volume
	// // Integral Formulations - Application to the Magnetic Force
	// // computation", International Journal of Applied Electromagnetics
	// // and Mechanics 50 (2017) 1–7
	// fltp NLData::calculate_magnetic_energy()const{
	// 	// calculate nodal magnetisation
	// 	const arma::Mat<fltp> Mn = calc_nodal_magnetization();

	// 	// get magnetic field at nodes
	// 	const arma::Mat<fltp> Bn = get_field('B');

	// 	// create gauss points
	// 	const arma::Mat<fltp> gp = cmn::Hexahedron::create_gauss_points(num_gauss_volume_);
	// 	const arma::Mat<fltp> Rg = gp.rows(0,2); const arma::Row<fltp> wg = gp.row(3);

	// 	// derivative of shape function
	// 	const arma::Mat<fltp> dN = cmn::Hexahedron::shape_function_derivative(Rg);

	// 	// integrate hb-curve
	// 	const arma::Col<fltp>& H = hb_data_->get_magnetic_field_table();
	// 	const arma::Col<fltp>& B = hb_data_->get_flux_density_table();
	// 	const arma::Row<fltp> hb_cumtrapz = cmn::Extra::cumtrapz(H.t(),B.t(),1);

	// 	// allocate energy
	// 	fltp co_energy = RAT_CONST(0.0);

	// 	// walk over elements
	// 	for(arma::uword i=0;i<n_.n_cols;i++){
	// 		// calculate fields at gauss points
	// 		const arma::Mat<fltp> myRn = R_.cols(n_.col(i));
	// 		const arma::Mat<fltp> Bg = cmn::Hexahedron::quad2cart(Bn.cols(n_.col(i)),Rg);
	// 		const arma::Mat<fltp> Mg = cmn::Hexahedron::quad2cart(Mn.cols(n_.col(i)),Rg);
	// 		const arma::Mat<fltp> Hg = Bg/arma::Datum<fltp>::mu_0 - Mg;

	// 		// calculate jacobian determinant and weights
	// 		const arma::Mat<fltp> J = cmn::Hexahedron::shape_function_jacobian(myRn,dN);
	// 		const arma::Row<fltp> Jdet = -cmn::Hexahedron::jacobian2determinant(J);

	// 		// calculate integral
	// 		arma::Col<fltp> hb_int;
	// 		cmn::Extra::interp1(H,hb_cumtrapz.t(),cmn::Extra::vec_norm(Hg).t(),hb_int,"linear",true);

	// 		// add energy contribution
	// 		// we are ignoring H0 term here, is this compensated by the J dot A in the coil?
	// 		co_energy += arma::accu((hb_int.t()-cmn::Extra::dot(Bg,Hg)/2)%wg%Jdet);
	// 	}

	// 	// return energy
	// 	return co_energy;
	// }


	// // magnetization at faces
	// arma::Mat<fltp> NLData::calc_face_magnetization()const{

	// }


	// calculate magnetization at nodes
	// this part is still dodgey need to think how to improve
	arma::Mat<fltp> NLData::calc_nodal_magnetization()const{
		// check if set
		if(M_.empty())return arma::Mat<fltp>(3,R_.n_cols,arma::fill::zeros);

		// check nodal
		if(is_nodal_)return M_;

		// allocate field at nodes
		arma::Mat<fltp> Mn(M_.n_rows,R_.n_cols,arma::fill::zeros);
		arma::Row<fltp> valence(R_.n_cols,arma::fill::zeros);

		// assign to nodes and average
		for(arma::uword i=0;i<n_.n_cols;i++){
			for(arma::uword j=0;j<n_.n_rows;j++){
				Mn.col(n_(j,i)) += M_.col(i);
				valence.col(n_(j,i)) += RAT_CONST(1.0);
			}
		}

		// average
		Mn.each_row()/=valence;

		// return the field at the nodes
		return Mn;
	}

	// relative permeability
	arma::Row<fltp> NLData::calc_relative_permeability()const{
		// get H field
		const arma::Mat<fltp> B = get_field('B');

		// return relative permeability
		return hb_data_->calc_relative_permeability(cmn::Extra::vec_norm(B));
	}


}}

#endif