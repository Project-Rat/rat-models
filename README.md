![Logo](./figs/RATLogo.png)
# Coil Design Library
<em>Hellooo Rat is very pleased with coil modeler. Many possibilities yes?</em>

## Introduction
Setting up coil geometries for performing field calculations, optimization, quench analysis and ultimately coil construction, can prove to be a challenging task. This library aims to simplify the setup of coil geometries and provides basic classes for calculating their magnetic, vector potential at any point in space. In addition specialized calculation classes are available, allowing to calculate, for instance, the magnetic field on the surface (for peak field calculation) and coil harmonics. The generated meshes and field-maps can be exported and used in for instance quench codes. The ultimate aim is that this code holds the reference geometry of any magnet. This to avoid slight variations in the magnet geometry across different calculation tools, as is now often the case. 

The main concept of the code is by generating the cables and coil blocks by extruding their cross section along a line path. This is done through a coordinate transformation that takes into account the orientation (pitch) of the path. The path consists of one or multiple sections defined by a simple geometric objects, such as a straight line or an arc. 

## Installation
As this library is part of a collection of inter-dependent libraries a detailed description of the installation process is provided in a separate repository located here: [rat-docs](https://gitlab.com/Project-Rat/rat-documentation). It is required that you install the [rat-common](https://gitlab.com/Project-Rat/rat-common), [distmesh-ccp](https://gitlab.com/Project-Rat/distmesh-cpp), [rat-mlfmm](https://gitlab.com/Project-Rat/rat-mlfmm) libraries before installing this.In addition it is required to set the ENABLE_CUDA flag in the cmakelists.txt file. It decides whether the code is build using NVidia Cuda or not. A quick build and installation is achieved by using

```bash
mkdir release
cd release
cmake -DCMAKE_BUILD_TYPE=T ..
make -jX
```

where X is the number of cores you wish to use for the build. When the library is build run unit tests and install using

```bash
make test
sudo make install
```

## Modeling a Solenoid
This section demonstrates how to do the most basic thing with the rat-models library: creating a solenoid and calculating the magnetic field inside the conductor. The idea here is to demonstrate how the library works and what it can accomplish. This is shown from a code point of view. Later on to run the code one will need to be able to compile it and link the required libraries. In Rat any coil is modeled using its cross section in combination with a path, a space-curve with corresponding [moving frame](https://en.wikipedia.org/wiki/Moving_frame). The coil is then converted into a mesh, which is used both as sources and as targets for the MLFMM. Eventually the mesh and corresponding magnetic field is written to a VTK output file, to be analyzed with Paraview. The code reads as follows and is added to the rat-models example files under the name solenoid.cpp for convenience. More examples can be found in the examples directory.

```cpp
// header files
#include "pathcircle.hh"
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "calcmesh.hh"

// main function
int main(){
	// model geometric input parameters
	const rat::fltp time = 0; // output time [s]
	const rat::fltp radius = 40e-3; // coil inner radius [m]
	const rat::fltp dcoil = 10e-3; // thickness of the coil [m]
	const rat::fltp wcable = 12e-3; // width of the cable [m]
	const rat::fltp delem = 1e-3; // element size [m]
	const arma::uword num_sections = 4; // number of coil sections

	// model operating parameters
	const rat::fltp operating_current = 200; // operating current in [A]
	const arma::uword num_turns = 100; // number of turns

	// create a circular path object
	const rat::mdl::ShPathCirclePr circle = rat::mdl::PathCircle::create(radius, num_sections, delem);
	circle->set_offset(dcoil);

	// create a rectangular cross section object
	rat::mdl::ShCrossRectanglePr rectangle = rat::mdl::CrossRectangle::create(0, dcoil, 0, wcable, delem);

	// create a coil object
	const rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(circle, rectangle);
	coil->set_number_turns(num_turns);
	coil->set_operating_current(operating_current);

	// create calculation
	const rat::mdl::ShCalcMeshPr mesh_calculation = rat::mdl::CalcMesh::create(coil);

	// calculate everything
	mesh_calculation->calculate_write({time},"./data");
}
```

The resulting Paraview Data (pvd) file located at "./solenoid/mesh.pvd" visualized in Paraview, after slicing it in half, looks like this:

![Solenoid](./figs/solenoid.png)

## Modeling Advanced Geometry
Obviously one doesn't need Rat for calculating solenoids, this can be done more effectively in other codes (such as Soleno). It becomes much more interesting when complicated coil geometries are modeled. In the examples folder you find many examples demonstrating the capabilities of this code. Until a more detailed tutorial becomes available the user is referred here.

## Gallery
The output of the [rat-models](https://gitlab.com/Project-Rat/rat-models) library can be in VTK XML format and can be visualized using Kitware [VTK](https://vtk.org/) and [Paraview](https://www.paraview.org/), which are completely free and open-source. Additionally, they are available on all platforms and even offer a web-interface (need further investigation). These are excellent software packages allow for creating stunning pictures.

For instance the clover_rt.cpp example in rat-models can be visualized as:
![clover_rt](./figs/clover_rt2.png)

and the toroid.cpp example in rat-models as:
![mini_toroid](./figs/mini_toroid4.png)

and the cct.cpp example in rat-models as:
![cct_dipole](./figs/cct_dipole.png)

radially magnetized ring:
![ring](./figs/ring_radial.png)

## Authors
* Jeroen van Nugteren
* Nikkie Deelen

## License
This project is licensed under the [MIT](LICENSE).