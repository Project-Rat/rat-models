get_filename_component(RatModels_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
include(CMakeFindDependencyMacro)

# add path
list(APPEND CMAKE_MODULE_PATH ${RatModels_CMAKE_DIR})
if(@CUDA_FOUND@)
	include("ConvertCUDAFlags.cmake")
endif()
list(REMOVE_AT CMAKE_MODULE_PATH -1)

# Rat dependencies
find_dependency(RatMLFMM)
find_dependency(RatDmsh)
find_dependency(RatMat)
find_dependency(RatMath)
if(@ENABLE_NL_SOLVER@)
	find_dependency(RatNL)
endif()

# nlopt optimizer
find_dependency(NLopt)

# MPI
if(@ENABLE_MPI@)
	find_dependency(MPI REQUIRED)
	if(NOT TARGET MPI::MPI)
		add_library(MPI::MPI INTERFACE IMPORTED)
		set_target_properties(MPI::MPI PROPERTIES
			INTERFACE_INCLUDE_DIRECTORIES "${MPI_CXX_INCLUDE_PATH}"
			INTERFACE_LINK_LIBRARIES "${MPI_CXX_LIBRARIES}")
	endif()
endif()

# visualisation toolkit from paraview
if(@ENABLE_PARAVIEW_VTK@)
  # When importing paraview, some internal tests need C to be enabled
  ENABLE_LANGUAGE(C)
  find_dependency(ParaView REQUIRED COMPONENTS VTKExtensionsCore)
endif()

# visualisation toolkit
if(NOT @ENABLE_PARAVIEW_VTK@)
	find_dependency(VTK REQUIRED COMPONENTS
		CommonCore CommonDataModel
		IOXML FiltersCore FiltersGeneral REQUIRED)
endif()

if(NOT TARGET VTK::VTK)
	add_library(VTK::VTK INTERFACE IMPORTED)
	set_target_properties(VTK::VTK PROPERTIES
		INTERFACE_INCLUDE_DIRECTORIES "${VTK_INCLUDE_DIRS}"
		INTERFACE_LINK_LIBRARIES "${VTK_LIBRARIES}")
endif()

if(NOT TARGET Rat::Models)
    include("${RatModels_CMAKE_DIR}/RatModelsTargets.cmake")
endif()

set(RatModels_LIBRARIES Rat::Models)

