## PROJECT SETUP
cmake_minimum_required(VERSION 3.20 FATAL_ERROR)
project(Rat-Models VERSION 2.018.5 LANGUAGES CXX C)

# set all policies to this version of cmake
cmake_policy(VERSION ${CMAKE_VERSION})

# switchboard
option(ENABLE_TESTING "Build unit/system tests" ON)
option(ENABLE_EXAMPLES "Build examples" ON)
option(ENABLE_PARAVIEW_VTK "Use ParaView's internal VTK library" OFF)
option(ENABLE_MPI "Enable MPI as required for paraview VTK" OFF)
option(ENABLE_NL_SOLVER "Enable Nonlinear Solver" OFF)

# ensure out of core build
if(PROJECT_SOURCE_DIR STREQUAL PROJECT_BINARY_DIR)
	message(FATAL_ERROR "In-source builds not allowed. Please make a new directory (called a build directory) and run CMake from there.\n")
endif()

# set build type
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()

# C++ standard
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

# compiler options
if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
	message(STATUS "using GCC settings")
	set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -g")
	set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -march=native -O3")
elseif (CMAKE_CXX_COMPILER_ID STREQUAL "Clang" OR CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang")
	message(STATUS "using Clang settings")
	set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -g")
	set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -march=native -O3 -flto -funroll-loops")
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
	message(STATUS "using MSVC settings")
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /utf-8")
	set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /O2")
else()
	message(STATUS "Compiler not recognized")
endif()

# windows specific
if(WIN32)
set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS ON)
endif()

# output paths
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/bin CACHE PATH "" FORCE)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/lib CACHE PATH "" FORCE)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/lib CACHE PATH "" FORCE)

# report
message(STATUS "building in ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}")


# EXTERNAL LIBRARIES
# find the rat mlfmm library
find_package(RatMLFMM 2.018.5 REQUIRED)

# find the rat distmesh library
find_package(RatDmsh 2.018.5 REQUIRED)

# find the rat materials library
find_package(RatMat 2.018.5 REQUIRED)

# find the rat materials library
find_package(RatMath 2.018.5 REQUIRED)


# find the rat nonlinear solver
if(ENABLE_NL_SOLVER)
	find_package(RatNL 2.018.5 REQUIRED)
endif()


# MPI
if(ENABLE_MPI)
	find_package(MPI REQUIRED)
	if(NOT TARGET MPI::MPI)
		add_library(MPI::MPI INTERFACE IMPORTED)
		set_target_properties(MPI::MPI PROPERTIES
			INTERFACE_INCLUDE_DIRECTORIES "${MPI_CXX_INCLUDE_PATH}"
			INTERFACE_LINK_LIBRARIES "${MPI_CXX_LIBRARIES}")
	endif()
endif()

# paraview extensions
if(ENABLE_PARAVIEW_VTK)
	# When importing paraview, some internal tests need C to be enabled
	ENABLE_LANGUAGE(C)
	find_package(ParaView REQUIRED COMPONENTS VTKExtensionsCore)
endif()

# visualisation toolkit
if(NOT ENABLE_PARAVIEW_VTK)
	find_package(VTK REQUIRED COMPONENTS 
		CommonCore CommonDataModel 
		IOXML FiltersCore FiltersGeneral QUIET)
endif()

# create vtk target if it doesn't exist
if(NOT TARGET VTK::VTK)
	add_library(VTK::VTK INTERFACE IMPORTED)
	set_target_properties(VTK::VTK PROPERTIES
		INTERFACE_INCLUDE_DIRECTORIES "${VTK_INCLUDE_DIRS}"
		INTERFACE_LINK_LIBRARIES "${VTK_LIBRARIES}")
endif()

# optimization
find_package(NLopt REQUIRED)


## LIBRARY
# create a list of sources
# ls *.cpp >> CMakeLists.txt
add_library(ratmodels SHARED 
	src/area.cpp
	src/background.cpp
	src/data.cpp
	# src/targetdata.cpp
	src/griddata.cpp
	src/cartesiangriddata.cpp
	src/harmonicsdata.cpp
	src/inductancedata.cpp
	src/lengthdata.cpp
	src/linedata.cpp
	src/trackdata.cpp
	src/bardata.cpp
	src/coildata.cpp
	src/meshdata.cpp
	src/importdata.cpp
	src/coilsurfacedata.cpp
	src/surfacedata.cpp
	src/cross.cpp
	src/crosscircle.cpp
	src/crossdmsh.cpp
	src/crossline.cpp
	src/crossline2.cpp
	src/crosspoint.cpp
	src/crosspath.cpp
	src/crossrectangle.cpp
	src/crosswedge.cpp
	src/darboux.cpp
	src/driveac.cpp
	src/drive.cpp
	src/drivedc.cpp
	src/drivefourier.cpp
	src/driveinterp.cpp
	src/drivetrapz.cpp
	src/drivestep.cpp
	src/driveptrain.cpp
	src/drivelinear.cpp
	src/driveinterpbcr.cpp
	src/drivercct.cpp
	src/drivescurve.cpp
	src/driverandom.cpp
	src/emitterbeam.cpp
	src/emittercoordscan.cpp
	src/emitteromni.cpp
	src/frame.cpp
	src/modelarray.cpp
	src/modelbar.cpp
	src/modelcoil.cpp
	src/model.cpp
	src/modelgroup.cpp
	src/modelmesh.cpp
	src/modelmirror.cpp
	src/modeltoroid.cpp
	src/modelsolenoid.cpp
	src/modelracetrack.cpp
	src/modelrectangle.cpp
	src/modeltrapezoid.cpp
	src/modelcoilwrapper.cpp
	src/modelbarwrapper.cpp
	src/modelimport.cpp
	src/modelsphere.cpp
	src/modelmgnsphere.cpp
	src/modeldshape.cpp
	src/modelflared.cpp
	src/modelclover.cpp
	src/modelpoint.cpp
	src/modelcube.cpp
	src/modelcylinder.cpp
	src/modeldoughnut.cpp
	src/modelplasma.cpp
	src/modelpatharray.cpp
	src/modelclip.cpp
	src/modeledges.cpp
	src/calc.cpp
	src/calcgroup.cpp
	src/calcleaf.cpp
	src/calcmlfmm.cpp
	src/calcinductance.cpp
	src/calcline.cpp
	src/calcplane.cpp
	src/calcgrid.cpp
	src/calcpath.cpp
	src/calcmesh.cpp
	src/calcimport.cpp
	src/calcharmonics.cpp
	src/calclength.cpp
	src/calcpoints.cpp
	src/calcspharm.cpp
	src/calctracks.cpp
	src/nameable.cpp
	src/particle.cpp
	src/path.cpp
	src/patharc.cpp
	src/pathellipticalarc.cpp
	src/patharray.cpp
	src/pathattach.cpp
	src/pathaxis.cpp
	src/pathbezier.cpp
	src/pathbspline.cpp
	src/pathcable.cpp
	src/pathcct.cpp
	src/pathcctcustom.cpp
	src/pathrail.cpp
	# src/pathccct.cpp
	src/pathcircle.cpp
	src/pathclover.cpp
	src/pathconnect.cpp
	src/pathconnect2.cpp
	src/pathdshape.cpp
	src/pathellipse.cpp
	src/pathequation.cpp
	src/pathflared.cpp
	src/pathgroup.cpp
	src/pathoffset.cpp
	src/pathoffsetdrive.cpp
	src/pathprofile.cpp
	src/pathrectangle.cpp
	src/pathrutherford.cpp
	src/pathstraight.cpp
	src/pathsub.cpp
	src/pathpolygon.cpp
	src/pathplasma.cpp
	src/pathfrenet.cpp
	src/pathspiral.cpp
	src/pathpoint.cpp
	src/pathcurl.cpp
	src/pathmap.cpp
	src/pathmerge.cpp
	src/pathstaircable.cpp
	src/pathfile.cpp
	src/pathfile2.cpp
	src/pathxyz.cpp
	src/pathccttable.cpp
	src/pathunfold.cpp
	src/pathdash.cpp
	src/transbend.cpp
	src/trans.cpp
	src/transflip.cpp
	src/transformations.cpp
	src/transreflect.cpp
	src/transreverse.cpp
	src/transrotate.cpp
	src/transtranslate.cpp
	src/vtkpvdata.cpp
	src/vtkimg.cpp
	src/vtktable.cpp
	src/vtkunstr.cpp
	src/serializer.cpp
	src/modelroot.cpp
	src/inputpath.cpp
	src/inputcross.cpp
	src/modelstick.cpp
	src/circuit.cpp
	src/inputcircuits.cpp
	src/spharmdata.cpp
	src/modelcct.cpp
	src/modelbarcube.cpp
	src/modelring.cpp
	src/modelmeshwrapper.cpp
	src/modelline.cpp
	src/emitter.cpp
	src/emitternewton.cpp
	src/pathsuperellipse.cpp
	src/pathtrapezoid.cpp
	src/mesh.cpp
	src/meshgen.cpp
	src/modelnl.cpp
	src/nldata.cpp
	src/hbcurve.cpp
	src/hbcurvetable.cpp
	src/hbcurvefile.cpp
	src/hbcurvevlv.cpp
	src/mgndata.cpp
	src/modelnlsphere.cpp
	src/inputhbcurve.cpp
	src/calcpolargrid.cpp
	src/polargriddata.cpp
	src/splitable.cpp
	src/modelnlfile.cpp
	src/pathcostheta.cpp
	src/calcquench0d.cpp
	src/quench0ddata.cpp
	src/protectioncircuit.cpp
	# src/patheq.cpp
	src/maxwellfieldmap.cpp
	src/calcfreecad.cpp
	# src/fcbuilder.cpp
	src/pathlemniscate.cpp
	src/crossgroup.cpp
)

# Add an alias so that library can be used inside the build tree, e.g. when testing
add_library(Rat::Models ALIAS ratmodels)

# disable windows warnings
if(WIN32)
	target_compile_definitions(ratmodels PUBLIC -D_CRT_SECURE_NO_WARNINGS)
endif()

# properties
set_target_properties(ratmodels PROPERTIES VERSION ${PROJECT_VERSION})
set_target_properties(ratmodels PROPERTIES SOVERSION 1)

# add include directory
target_include_directories(ratmodels
	PUBLIC 
		$<INSTALL_INTERFACE:include>    
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
		$<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/include>
	PRIVATE
		${CMAKE_CURRENT_SOURCE_DIR}/src
)

# NVIDIA CUDA linker language
if(CMAKE_CUDA_COMPILER)
	set_target_properties(ratmodels PROPERTIES LINKER_LANGUAGE CUDA)
endif()

# enable warnings
target_compile_options(ratmodels PRIVATE 
	$<$<OR:$<CXX_COMPILER_ID:Clang>,$<CXX_COMPILER_ID:AppleClang>,$<CXX_COMPILER_ID:GNU>>:
		-Wpedantic -Wall -Wextra -Wsuggest-override -Wold-style-cast>
	$<$<CXX_COMPILER_ID:MSVC>:
		/W3>)

# disable hdf5 in widnows
if(WIN32)
	target_compile_definitions(ratmodels PUBLIC -DARMA_DONT_USE_HDF5)
endif()

# link to libraries
target_link_libraries(ratmodels PUBLIC 
	Rat::MLFMM Rat::Dmsh Rat::Mat 
	Rat::Math VTK::VTK NLopt::nlopt)

# link to optional nl solver library
if(ENABLE_NL_SOLVER)
	message(STATUS "linking NL solver")
	target_link_libraries(ratmodels PUBLIC Rat::NL)
	target_compile_definitions(ratmodels PUBLIC -DENABLE_NL_SOLVER)
endif()

# Version header file
string(REGEX REPLACE "^0+" "" PROJECT_VERSION_MAJOR_STRIPPED ${PROJECT_VERSION_MAJOR})
string(REGEX REPLACE "^0+" "" PROJECT_VERSION_MINOR_STRIPPED ${PROJECT_VERSION_MINOR})
string(REGEX REPLACE "^0+" "" PROJECT_VERSION_PATCH_STRIPPED ${PROJECT_VERSION_PATCH})
configure_file(
	${CMAKE_CURRENT_SOURCE_DIR}/cmake/version.hh.in 
	${CMAKE_CURRENT_BINARY_DIR}/include/version.hh)

# UNIT/SYSTEM TESTS
# build unit tests
if(ENABLE_TESTING)
	# allow for unit tests
	enable_testing()
	
	# add test directory
	add_subdirectory(test)
endif(ENABLE_TESTING)


# EXAMPLES
# compile examples
if(ENABLE_EXAMPLES)
	# add examples directory
	add_subdirectory(examples)
endif()


## INSTALLATION
# get install directories
include(GNUInstallDirs)
set(INSTALL_CONFIGDIR ${CMAKE_INSTALL_LIBDIR}/cmake/ratmodels)

install(TARGETS ratmodels
	EXPORT ratmodels-targets
	LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}/rat
	ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}/rat
	RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)

# This is required so that the exported target has the name Models and not ratmodels
set_target_properties(ratmodels PROPERTIES EXPORT_NAME Models)

# Install header files
install(DIRECTORY include/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/rat/models)
install(FILES ${CMAKE_BINARY_DIR}/include/version.hh DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/rat/models)

# Export the targets to a script
install(EXPORT ratmodels-targets
	FILE
		RatModelsTargets.cmake
	NAMESPACE
		Rat::
	DESTINATION
		${INSTALL_CONFIGDIR}
)

# Create a ConfigVersion.cmake file
include(CMakePackageConfigHelpers)
write_basic_package_version_file(
	${CMAKE_CURRENT_BINARY_DIR}/RatModelsConfigVersion.cmake
	VERSION ${PROJECT_VERSION}
	COMPATIBILITY AnyNewerVersion
)

configure_package_config_file(${CMAKE_CURRENT_LIST_DIR}/cmake/RatModelsConfig.cmake.in
	${CMAKE_CURRENT_BINARY_DIR}/RatModelsConfig.cmake
	INSTALL_DESTINATION ${INSTALL_CONFIGDIR}
)

# Install the config, configversion and custom find modules
install(FILES
	# ${CMAKE_CURRENT_LIST_DIR}/cmake/ConvertCUDAFlags.cmake
	# ${CMAKE_CURRENT_LIST_DIR}/cmake/EnableLanguageCUDA.cmake
	${CMAKE_CURRENT_BINARY_DIR}/RatModelsConfig.cmake
	${CMAKE_CURRENT_BINARY_DIR}/RatModelsConfigVersion.cmake
	DESTINATION ${INSTALL_CONFIGDIR}
)

## EXPORT
# Exporting from the build tree
# configure_file(${CMAKE_CURRENT_LIST_DIR}/cmake/ConvertCUDAFlags.cmake
# 	${CMAKE_CURRENT_BINARY_DIR}/ConvertCUDAFlags.cmake COPYONLY)
# configure_file(${CMAKE_CURRENT_LIST_DIR}/cmake/EnableLanguageCUDA.cmake
# 	${CMAKE_CURRENT_BINARY_DIR}/EnableLanguageCUDA.cmake COPYONLY)

export(EXPORT ratmodels-targets
	FILE ${CMAKE_CURRENT_BINARY_DIR}/RatModelsTargets.cmake
	NAMESPACE Rat::)

# Register package in user's package registry
export(PACKAGE RatModels)

include(GNUInstallDirs)

# Set RPATHS
SET(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
list(APPEND CMAKE_INSTALL_RPATH "$ORIGIN/../${CMAKE_INSTALL_LIBDIR}")

# Create Debian package, must be the last thing
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake")
include(GenerateDebianPackage)
