// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "matstainless316.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	MatStainless316::MatStainless316(){
		
	}

	// factory
	ShMatStainless316Pr MatStainless316::create(){
		return std::make_shared<MatStainless316>();
	}

	// calculate thermal conductivity in [W m^-1 K^-1]
	arma::Row<double> MatStainless316::calc_thermal_conductivity(
		const arma::Row<double> &/*Bm*/, const arma::Row<double> &T) const{
		// check range
		if(arma::any(T<1))rat_throw_line("temperature below range");
		// if(arma::any(arma::any(T>300)))rat_throw_line("temperature beyond range");

		// coefficients of fit
		arma::Row<double> ft = {-1.4087,1.3982,0.2543,-0.6260,0.2334,0.4256,-0.4658,0.1650,-0.0199};

		// calculate and return
		arma::Row<double> A(T); A.zeros();
		arma::Row<double> log10T = arma::log10(T);
		arma::Row<double> plt(T); plt.fill(1.0);
		for(arma::uword i=0;i<ft.n_elem;i++){
			A += ft(i)*plt; plt%=log10T;
		}

		// 10^A and multiply with density
		arma::Row<double> k = arma::exp10(A);

		// extrapolation beyond end
		const arma::Row<arma::uword> extrap = arma::find(T>300).t();
		if(!extrap.is_empty()){
			arma::Row<double> Te = arma::Row<double>{298,300};
			arma::Row<double> Be = arma::Row<double>{0,0}; // this material has no field dependence anyways
			arma::Row<double> ke = calc_thermal_conductivity(Be,Te);
			k.cols(extrap) = ke(1) + (T.cols(extrap)-Te(1))*((ke(1)-ke(0))/(Te(1)-Te(0)));
		}

		// return answer
		return k;
	}

	// calculate electrical resistivity output in [Ohm m]
	arma::Mat<double> MatStainless316::calc_resistivity(
		const arma::Row<double> &Bm, const arma::Row<double> &T, const bool /*combined*/) const{
		// lorenz number (for Wiedemann Franz Law)
		// https://en.wikipedia.org/wiki/Wiedemann%E2%80%93Franz_law
		const double L_ = 2.44e-8; // W Ohm K^-2

		// calculate thermal conductivity conductivity
		const arma::Row<double> k = calc_thermal_conductivity(Bm,T);

		// relate to thermal conductivity (using Wiedemann Franz Law)
		const arma::Mat<double> rho = (L_*T)/k; 

		// return resistivity
		return rho; 
	}

	// calculate current density from electric field
	arma::Row<double> MatStainless316::calc_current_density(
		const arma::Row<double> &E, const arma::Row<double> &/*Bm*/, 
		const arma::Row<double> &/*T*/, const arma::Row<double> &/*alpha*/,
		const arma::Mat<double> &/*Jc*/, const arma::Mat<double> &rho) const{

		// calculate resistivity in W/(mK)
		// const arma::Row<double> rho = calc_resistivity(Bm,T);
		assert(rho.n_rows==1);

		// calculate current density
		const arma::Row<double> J = E/rho; // A/m^2

		// return current density
		return J;
	}

	// calculate electric field from current density
	arma::Row<double> MatStainless316::calc_electric_field(
		const arma::Row<double> &J, const arma::Row<double> &/*Bm*/, 
		const arma::Row<double> &/*T*/, const arma::Row<double> &/*alpha*/,
		const arma::Mat<double> &/*Jc*/, const arma::Mat<double> &rho) const{
		
		// calculate resistivity in W/(mK)
		// const arma::Row<double> rho = calc_resistivity(Bm,T);
		assert(rho.n_rows==1);

		// return electric field
		const arma::Row<double> E = rho%J; // V/m

		// return electric field
		return E;
	}

	// copy constructor
	ShMaterialPr MatStainless316::copy() const{
		return std::make_shared<MatStainless316>(*this);
	}

	// specific heat output in [J m^-3 K^-1]
	// source NIST: https://trc.nist.gov/cryogenics/materials/OFHC%20Copper/OFHC_Copper_rev1.htm
	arma::Row<double> MatStainless316::calc_specific_heat(const arma::Row<double> &T)const{
		// check range
		if(arma::any(T<4))rat_throw_line("temperature below range");
		// if(arma::any(arma::any(T>300)))rat_throw_line("temperature beyond range");

		// coefficients of fit
		const arma::Row<double> ft1 = {12.2486,-80.6422,218.743,-308.854,239.5296,-89.9982,3.15315,8.44996,-1.91368};
		const arma::Row<double> ft2 = {-1879.464,3643.198,76.70125,-6176.028,7437.6247,-4305.7217,1382.4627,-237.22704,17.05262};
		
		// find indexes of two temperature ranges
		// described by the fit
		const arma::Col<arma::uword> idx1 = arma::find(T<50);
		const arma::Col<arma::uword> idx2 = arma::find(T>=50);

		// calculate and return
		arma::Row<double> A(T); A.zeros();
		arma::Row<double> log10T = arma::log10(T);
		arma::Row<double> plt(T); plt.fill(1.0);
		for(arma::uword i=0;i<ft1.n_elem;i++){
			if(!idx1.is_empty())A(idx1) += ft1(i)*plt(idx1);
			if(!idx2.is_empty())A(idx2) += ft2(i)*plt(idx2);
			plt%=log10T;
		}

		// 10^A and multiply with density
		arma::Row<double> sh = density_*arma::exp10(A);

		// extrapolation beyond end
		const arma::Row<arma::uword> extrap = arma::find(T>300).t();
		if(!extrap.is_empty()){
			arma::Row<double> Te = arma::Row<double>{298,300};
			arma::Row<double> Cpe = calc_specific_heat(Te);
			sh.cols(extrap) = Cpe(1) + (T.cols(extrap)-Te(1))*((Cpe(1)-Cpe(0))/(Te(1)-Te(0)));
		}

		// return answer
		return sh;
	}

	// get type
	std::string MatStainless316::get_type(){
		return "mdl::matstainless316";
	}

	// method for serialization into json
	void MatStainless316::serialize(Json::Value &js, std::list<cmn::ShNodePr> &) const{
		// settings
		js["type"] = get_type();
		// for(arma::uword i=0;i<tc_params_.n_elem;i++)
		// js["tc_params"].append(tc_params_(i));
		// js["RRR"] = RRR_;
	}

	// method for deserialisation from json
	void MatStainless316::deserialize(const Json::Value &/*js*/, std::list<cmn::ShNodePr> &, const cmn::NodeFactoryMap &){
		// const arma::uword num_tc_params = js["tc_params"].size();
		// tc_params_.set_size(num_tc_params);
		// for(arma::uword i=0;i<num_tc_params;i++)
		// tc_params_(i) = js["tc_params"].get(i,0).ASFLTP();
		// RRR_ = js["RRR"].ASFLTP();
	}

}}