// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_COIL_HH
#define MDL_MODEL_COIL_HH

#include <armadillo> 
#include <memory>

#include "rat/common/error.hh"
#include "rat/mat/conductor.hh"
#include "rat/mat/baseconductor.hh"

#include "model.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelSolenoid> ShModelSolenoidPr;

	// solenoid wrapper
	class ModelSolenoid: public Model{
		// properties
		protected:
			// geometry
			fltp inner_radius_;
			fltp outer_radius_;
			fltp height_;

			// winding geometry
			fltp number_turns_ = 1;

			// material of this coil
			rat::mat::ShConductorPr material_;

			// current sharing feature
			// when enabled the current can flow freely over
			// the cross section of the conductor (use for cables)
			bool enable_current_sharing_ = false;

			// use line elements or volume elements
			bool use_volume_elements_ = false;

			// number of gauss points
			arma::sword num_gauss_ = 1;

			// softening factor
			fltp softening_ = 1.0;

			// design operation current
			fltp operating_current_ = 0;

			// current drive
			ShDrivePr current_drive_ = DriveDC::create();

		// methods
		public:
			// constructor
			ModelSolenoid();
			ModelSolenoid(ShPathPr pth, ShCrossPr crss, rat::mat::ShConductorPr material = NULL);

			// factory methods
			static ShModelSolenoidPr create();
			static ShModelSolenoidPr create(ShPathPr pth, ShCrossPr crss, rat::mat::ShConductorPr material = NULL);

			// get number of calculation data objects
			virtual arma::uword get_num_objects() const = 0;

			// creation of calculation data objects
			virtual ShCalcMeshPrList create_meshes(const fltp time = 0) const = 0; // create volume current elements
			virtual ShEdgesPrList create_edge(const fltp time = 0) const = 0; // create edges of the coil

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
