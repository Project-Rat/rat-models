// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef CALCULATE_MESH_HH
#define CALCULATE_MESH_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "common/defines.hh"
#include "path.hh"
#include "pathstraight.hh"
#include "pathgroup.hh"
#include "mlfmm/mgntargets.hh"
#include "common/log.hh"
#include "cross.hh"
#include "vtkunstr.hh"

// VTK headers
#include <vtkSmartPointer.h>
#include <vtkDoubleArray.h>
#include <vtkUnstructuredGrid.h>
#include <vtkXMLUnstructuredGridWriter.h> 
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkCellArray.h>

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcMesh> ShCalcMeshPr;

	// simple mesh calculation
	class CalcMesh: public fmm::MgnTargets{		
		// properties
		protected:
			// mesh base and cross section
			ShPathPr base_;
			ShCrossPr crss_;

			// stored mesh (node coordinates in Rt)
			arma::Mat<arma::uword> n_;

		// methods
		public:
			// constructor
			CalcMesh();
			CalcMesh(ShPathPr base, ShCrossPr crss);

			// factory methods
			static ShCalcMeshPr create();
			static ShCalcMeshPr create(ShPathPr base, ShCrossPr crss);

			// setting of the path
			void set_cross(ShCrossPr crss);
			void set_base(ShPathPr base);
			void set_base(const char axis, const char dir, const double ell, const double x, const double y, const double z, const double dl);

			// calculation
			void setup(cmn::ShLogPr lg = cmn::NullLog::create());

			// export to vtk
			vtkSmartPointer<vtkUnstructuredGrid> create_vtk_ugrid(cmn::ShLogPr lg) const;
			ShVTKUnstrPr export_vtk_mesh(cmn::ShLogPr lg = cmn::NullLog::create()) const;
	};

}}

#endif
