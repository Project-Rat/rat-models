// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// Description:
// Calculates the critical current density of Nb3Sn using the A. God
// formula. The initial parameters can be set using two standard
// variants: Powder in Tube (PIT) and Rod-Restack Process (RRP). It must
// be noted that the formulas are not accurate in the low field region.

// references:
// A. Godeke, Msc. Thesis: Performance Boundaries in Nb3Sn Superconductors,
// Twente University 2005, p168

#ifndef MDL_MATERIAL_NB3SN_HH
#define MDL_MATERIAL_NB3SN_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "material.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class MatNb3Sn> ShMatNb3SnPr;

	// Lubell/Kramer scaling relation
	class MatNb3Sn: public Material{
		private:
			// density
			double density_ = 8950; // [Kg m^-3]

			// deformation related parameters
			double Ca1_ = 0;
			double Ca2_ = 0;
			double eps_0a_ = 0; // normalized
			double C1_ = 0; // [kAT mm^-2]

			// superconducting parameters
			double Bc2m_ = 0; // [T]
			double Tcm_ = 0; // [K]

			// magnetic field dependence parameters
			double p_ = 0;
			double q_ = 0;

			// environmental parameters
			double eps_ax_ = 0;

			// electric field criterion
			double E0_ = 1e-5; // [V/m]
			double N_ = 40; 

			// interpolation arrays
			arma::Col<double> Ti_;
			arma::Col<double> ki_;
			arma::Col<double> Cpi_; 

		// methods
		public:
			// conductor
			MatNb3Sn();

			// factory
			static ShMatNb3SnPr create();

			// default conductor settings
			void set_ITER();
			void set_PIT();
			void set_RRP();
			
			// calculate critical current
			arma::Mat<double> calc_critical_current_density(const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha, const bool combined = true) const;

			// calculate electric field
			arma::Row<double> calc_current_density(const arma::Row<double> &E, const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha, const arma::Mat<double> &Jc, const arma::Mat<double> &rho) const;
			arma::Row<double> calc_electric_field(const arma::Row<double> &J, const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha, const arma::Mat<double> &Jc, const arma::Mat<double> &rho) const;

			// thermal properties
			// arma::Row<double> calc_resistivity(const arma::Row<double> &Bm, const arma::Row<double> &T) const;
			arma::Row<double> calc_specific_heat(const arma::Row<double> &T)const;
			arma::Row<double> calc_thermal_conductivity(const arma::Row<double> &Bm, const arma::Row<double> &T)const;

			// copy
			ShMaterialPr copy() const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, std::list<cmn::ShNodePr> &list) const;
			virtual void deserialize(const Json::Value &js, std::list<cmn::ShNodePr> &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif