// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_LINE_HH
#define MDL_MODEL_LINE_HH

// general headers
#include <armadillo> 
#include <memory>
#include <boost/filesystem.hpp>

// common headers
#include "rat/common/error.hh"

// multipole method headers
#include "rat/mlfmm/mlfmm.hh"
#include "rat/mlfmm/multisources.hh"
#include "rat/mlfmm/multitargets2.hh"
#include "rat/mlfmm/settings.hh"

// headers
#include "modelgroup.hh"
#include "vtkpvdata.hh"
#include "data.hh"
#include "griddata.hh"
#include "background.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelLine> ShModelLinePr;

	// a collection of coils
	class ModelLine: public CalcMlfmm{
		// set coils
		protected:
			// input path
			ShPathPr path_;

		// methods
		public:
			// constructor
			ModelLine();

			// factory methods
			static ShModelLinePr create();

			// set path
			void set_path(ShPathPr path);
			ShPathPr get_path() const;

			// setup function
			void setup();

			// calculate
	};

}}

#endif
