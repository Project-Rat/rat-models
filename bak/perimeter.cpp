// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "perimeter.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	Perimeter::Perimeter(){

	}

	// constructor with input
	Perimeter::Perimeter(
		const arma::Mat<fltp> &Rn, 
		const arma::Mat<arma::uword> &n){
		
		// set mesh
		set_mesh(Rn,n);	
	}

	// factory
	ShPerimeterPr Perimeter::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<Perimeter>();
	}

	// factory with dimension input
	ShPerimeterPr Perimeter::create(
		const arma::Mat<fltp> &Rn, 
		const arma::Mat<arma::uword> &n){
		return std::make_shared<Perimeter>(Rn,n);
	}

	// get number of elements
	arma::uword Perimeter::get_num_elements() const{
		return n_.n_cols;
	}

	// get number of nodes
	arma::uword Perimeter::get_num_nodes() const{
		return Rn_.n_cols;
	}

	// get node coordinates
	arma::Mat<fltp> Perimeter::get_nodes() const{
		return Rn_;
	}

	// get elements
	arma::Mat<arma::uword> Perimeter::get_elements() const{
		return n_;
	}

	// method for setting the mesh
	void Perimeter::set_mesh(
		const arma::Mat<fltp> &Rn, 
		const arma::Mat<arma::uword> &n){

		// check mesh
		assert(n.n_rows==2);
		assert(arma::max(arma::max(n))<Rn.n_cols);

		// set to self
		Rn_ = Rn; n_ = n;
	}

	// calculate length of periphery
	fltp Perimeter::calculate_length() const{
		// calculate edge vectors
		const arma::Mat<fltp> dR = 
			Rn_.cols(n_.row(1))-Rn_.cols(n_.row(0));

		// calculate length, sum and return
		return arma::as_scalar(arma::sum(arma::sqrt(arma::sum(dR%dR,0)),1));
	}

}}