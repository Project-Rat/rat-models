// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "calcgroup.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcGroup::CalcGroup(){

	}

	// factory
	ShCalcGroupPr CalcGroup::create(){
		return std::make_shared<CalcGroup>();
	}

	// add claculations
	void CalcGroup::add_calculation(ShCalcPr calc){
		// check input
		if(calc==NULL)rat_throw_line("calc points to zero");

		// get number of calcs
		const arma::uword num_calcs = calcs_.n_elem;

		// allocate new source list
		ShCalcPrList new_calcs(num_calcs + 1);

		// set old and new sources
		for(arma::uword i=0;i<num_calcs;i++)new_calcs(i) = calcs_(i);
		new_calcs(num_calcs) = calc;
		
		// set new source list
		calcs_ = new_calcs;
	}

	// calculate and write
	void CalcGroup::calculate_and_write(cmn::ShLogPr lg){
		for(arma::uword i=0;i<calcs_.n_elem;i++)
			calcs_(i)->calculate_and_write(lg);
	}

	// run calculation
	void CalcGroup::calculate(cmn::ShLogPr lg){
		for(arma::uword i=0;i<calcs_.n_elem;i++)
			calcs_(i)->calculate(lg);
	}

	// run calculation
	void CalcGroup::write(cmn::ShLogPr lg){
		for(arma::uword i=0;i<calcs_.n_elem;i++)
			calcs_(i)->write(lg);
	}

	// get type
	std::string CalcGroup::get_type(){
		return "rat::mdl::calcgroup";
	}

	// method for serialization into json
	void CalcGroup::serialize(Json::Value &js, cmn::SList &list) const{
		// properties
		js["type"] = get_type();
		
		// subnodes
		for(arma::uword i=0;i<calcs_.n_elem;i++)
			js["calcs"].append(cmn::Node::serialize_node(calcs_(i), list));
	}

	// method for deserialisation from json
	void CalcGroup::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// subnodes
		const arma::uword num_calcs = js["calcs"].size();
		calcs_.set_size(num_calcs);
		for(arma::uword i=0;i<num_calcs;i++)
			calcs_(i) = cmn::Node::deserialize_node<Calc>(js["calcs"].get(i,0), list, factory_list, pth);
	}

}}
