// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_FIELDMAP_HH
#define MDL_FIELDMAP_HH

#include <armadillo> 
#include <memory>
#include <map>

#include "rat/common/defines.hh"
#include "rat/common/log.hh"
#include "rat/common/node.hh"

#include "rat/mlfmm/settings.hh"
#include "rat/mlfmm/mlfmm.hh"
#include "rat/mlfmm/sources.hh"
#include "rat/mlfmm/multisources.hh"
#include "rat/mlfmm/currentsurface.hh"
#include "rat/mlfmm/currentmesh.hh"
#include "rat/mlfmm/currentsources.hh"
#include "rat/mlfmm/mgntargets.hh"
#include "rat/mlfmm/interp.hh"

#include "modelgroup.hh"
#include "drivedc.hh"
#include "vtkobj.hh"
#include "background.hh"
#include "calc.hh"
#include "extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcFieldMap> ShCalcFieldMapPr;

	// a collection of coils
	class CalcFieldMap: public Calc{
		// settings
		protected:
			// model that generates the field
			ShModelPr source_model_;

			// calculated field type and its dimensions
			std::string field_type_ = "AHM";
			arma::Row<arma::uword> num_dim_ = {3,3,3};

			// settings for the MLFMM
			fmm::ShSettingsPr fmm_stngs_ = fmm::Settings::create();

			// background field (only added once)
			arma::field<ShBackgroundPr> bg_;

			// list of drives sorted by index
			std::map<arma::uword, ShDrivePr> drives_;

			// use line elements or volume elements
			//bool use_volume_elements_ = false;


		// storage
		protected:
			// time used for calculating field
			double time_ = 0;

			// target coordinates
			arma::Mat<double> Rt_;

			// unique drive ids in this simulation
			arma::Row<arma::uword> drive_id_;

			// stored targets for each drive
			arma::field<fmm::ShMgnTargetsPr> tar_;

		// methods
		public:
			// constructor
			CalcFieldMap();

			// destructor
			~CalcFieldMap(){};


			// set the field type
			void set_field_type(const std::string &field_type, const arma::Row<arma::uword> &num_dim);
			void set_field_type(const std::string &field_type, const arma::uword num_dim);

			// set source model
			void set_source_model(ShModelPr model);

			// add drive
			void add_drive(ShDrivePr drive);

			// add a background field
			void add_background(ShBackgroundPr bg);

			// set the settings
			void set_fmm_settings(fmm::ShSettingsPr settings);
			// void set_mdl_settings(mdl::ShSettingsPr settings);

			// set volume elements
			// void set_use_volume_elements(const bool use_volume_elements);

			// access the settings
			fmm::ShSettingsPr get_fmm_settings();
			// mdl::ShSettingsPr get_mdl_settings();

			// calculate the fieldmap
			virtual void calculate(cmn::ShLogPr lg = cmn::NullLog::create());

			// set time
			virtual void set_time(const double time);
			double get_time() const;

			// checking which fields are available
			bool has_field() const;
			bool has(const std::string &type) const;

			// getting field
			arma::Mat<double> get_field(const std::string &type, const double time) const;
			arma::Mat<double> get_field(const std::string &type) const;

			// extended filename with time index
			static std::string indexed_output_fname(const std::string &fname, const arma::uword idx);

			// setup function
			virtual void setup(cmn::ShLogPr lg = cmn::NullLog::create()) = 0;
			virtual void write(cmn::ShLogPr lg = cmn::NullLog::create()) = 0;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) const override;
	};

}}

#endif
