// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_EDGES_COIL_HH
#define MDL_EDGES_COIL_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/error.hh"
#include "trans.hh"
#include "rat/common/freecad.hh"
#include "rat/common/opera.hh"
#include "nameable.hh"
#include "rat/mat/conductor.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class EdgesCoil> ShEdgesCoilPr;
	typedef arma::field<ShEdgesCoilPr> ShEdgesCoilPrList;

	// mesh of a coil
	// this objects carries all properties 
	// required for the calculations
	class EdgesCoil: public Edges{
		protected:
			// operating current
			fltp operating_current_;
			
			// number of turns
			fltp number_turns_;
			
			// material of this coil
			rat::mat::ShConductorPr material_;

		// methods
		public:
			// constructor
			EdgesCoil();
			EdgesCoil(ShFramePr &frame, ShAreaPr &area);

			// destructor
			virtual ~EdgesCoil(){};

			// factory
			static ShEdgesCoilPr create();
			static ShEdgesCoilPr create(ShFramePr &frame, ShAreaPr &area);

			// setting and getting
			// operating current
			void set_operating_current(const fltp operating_current);
			fltp get_operating_current() const;

			// number of turns
			void set_number_turns(const fltp number_turns);
			fltp get_number_turns() const;

			// material
			void set_material(rat::mat::ShConductorPr material);
			rat::mat::ShConductorPr get_material() const;

			// export edges to freecad file
			virtual void export_freecad(cmn::ShFreeCADPr freecad) const override;
			virtual void export_opera(cmn::ShOperaPr opera) const override;

			// create a copy
			virtual cmn::ShNodePr copy() const;
	};

}}

#endif
