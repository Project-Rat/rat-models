
#include "rat/common/typedefs.hh"
#include <armadillo>


int main(){
	const fltp sphere_radius=10;
	const fltp core_radius=5;
	const fltp num_mantel=5;
	const fltp num_core=8;

	// calculate
	const fltp core_elem_size = 2*core_radius/num_core;
	const fltp mantek_elem_size = 2*core_radius/num_core;

	// circle section
	arma::Row<fltp> theta = arma::linspace<arma::Row<fltp> >(
		-arma::Datum<fltp>::pi/4,arma::Datum<fltp>::pi/4,num_core+1);

	// sphere  patch
	arma::Row<fltp> as = sphere_radius*arma::sin(theta);
	arma::Row<fltp> bs = sphere_radius*arma::cos(theta);

	// allocate nodes
	arma::uword num_nodes = (num_core+1)*(num_core+1)*(num_core+1) + 
		6*(num_core+1)*(num_core+1)*(num_mantel+1);
	arma::uword cnt = 0;
	const arma::Mat<fltp> R(3,num_nodes);

	// walk oevr nodes
	for(arma::uword i=0;i<num_core+1;i++){
		for(arma::uword j=0;j<num_core+1;j++){
			for(arma::uword k=0;k<num_core+1;k++){
				R.col(cnt++) = arma::Col<fltp>{i,j,k};
			}
		}
	}

	for(arma::uword i=0;i<num_core+1;i++){
		for(arma::uword j=0;j<num_core+1;j++){
			// start point

			// end point

			// line
			for(arma::uword k=0;k<num_mantel;k++){

			}
		}
	}

	// remove duplicate nodes
}