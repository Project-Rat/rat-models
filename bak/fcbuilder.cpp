/* Rat-ST - Spherical Tokamak Geometry
Copyright (C) 2020  Jeroen van Nugteren*/

// header file
#include "fcbuilder.hh"

// code specific to Raccoon
namespace rat{namespace mdl{

	// export to FreeCAD
	void FCBuilder::export_freecad(
		std::ofstream& cmnd,
		const mdl::ShModelPr& model, 
		const fltp time,
		const boost::filesystem::path &output_dir, 
		const std::string &fname,
		const cmn::ShLogPr& /*lg*/){

		// create union
		const bool create_union = false;

		// create directory
		boost::filesystem::create_directories(output_dir);

		// output file
		const boost::filesystem::path macro_file = output_dir/(fname + ".FCMacro");
		const boost::filesystem::path fcmodel_file = output_dir/(fname + ".FCStd");
		const boost::filesystem::path step_file = output_dir/(fname + ".step");

		// settings for constructing meshes
		mdl::MeshSettings stngs;
		stngs.time = time;
		stngs.low_poly = false;
		stngs.combine_sections = false;
		stngs.visual_tolerance = RAT_CONST(0.0);
		stngs.visual_radius = RAT_CONST(0.0);
		stngs.is_calculation = true;

		// create mesh objects
		const std::list<mdl::ShMeshDataPr> meshes = model->create_meshes({},stngs);

		// ensure unique names
		std::map<std::string, std::list<mdl::ShMeshDataPr> > list;
		for(auto it = meshes.begin();it!=meshes.end();it++){
			// get mesh
			const mdl::ShMeshDataPr& mesh = (*it);

			// count the number of times this name exists
			list[mesh->get_name()].push_back(mesh);
		}

		// set names
		for(auto it1 = list.begin();it1!=list.end();it1++){
			const std::string name = (*it1).first;
			const std::list<mdl::ShMeshDataPr>& mesh_list = (*it1).second;
			if(mesh_list.size()>1){
				arma::uword cnt = 0;
				for(auto it2 = mesh_list.begin();it2!=mesh_list.end();it2++,cnt++)
					(*it2)->set_name(name + "_" + std::to_string(cnt));
			}
		}

		// create freecad file
		{
			const cmn::ShFreeCADPr fc = cmn::FreeCAD::create(macro_file, model->get_name());
			fc->set_no_graphics(); 
			fc->set_create_union(create_union);
			fc->set_num_max(1e5);

			// write step file for each mesh
			for(auto it = meshes.begin();it!=meshes.end();it++){
				// get mesh
				const mdl::ShMeshDataPr& mesh = (*it);

				// export the mesh to freecad
				mesh->export_freecad(fc);
			}

			// save to step file
			fc->save_doc(fcmodel_file);
			fc->save_step_file(step_file);

			// remove existing freecad file so it doesn't create a second filename
			boost::filesystem::remove(fcmodel_file);
		}

		// add macro file to the command execution list
		cmnd<<"freecad -c \""<<macro_file.string()<<"\"\n";
	}

	// recursive freecad creation
	void FCBuilder::create_freecad(
		std::ofstream& cmnd,
		const mdl::ShModelPr& model, 
		const fltp time,
		const boost::filesystem::path& output_dir, 
		const mdl::ShSerializerPr& slzr, 
		const cmn::ShLogPr& lg,
		const arma::uword depth){

		// cast to splitable
		const mdl::ShSplitablePr base = std::dynamic_pointer_cast<mdl::Splitable>(model);

		// create filename
		std::string name = model->get_name();
		std::replace(name.begin(), name.end(), ' ', '_');

		// check if splitable model
		if(base!=NULL){
			// split
			std::list<mdl::ShModelPr> models = base->split(slzr);

			// recursive call
			for(auto it=models.begin();it!=models.end();it++)
				create_freecad(cmnd,*it,time,depth==0 ? output_dir : output_dir/name,slzr,lg,depth+1);

		}else{
			// convert leaf model into freecad file
			export_freecad(cmnd,model,time,output_dir,name,lg);
		}
	}

	// recursive freecad creation
	void FCBuilder::combine_freecad_core(
		std::ofstream& script, 
		std::list<boost::filesystem::path>& dependencies,
		const arma::uword idx,
		const mdl::ShModelPr& model, 
		const fltp time,
		const boost::filesystem::path& output_dir,
		const mdl::ShSerializerPr& slzr,  
		const cmn::ShLogPr& lg){

		// cast to splitable
		const mdl::ShSplitablePr base = std::dynamic_pointer_cast<mdl::Splitable>(model);

		// create filename
		std::string name = model->get_name();
		std::replace(name.begin(), name.end(), ' ', '_');

		// check if splitable model
		if(base!=NULL){
			// split
			const std::list<mdl::ShModelPr> models = base->split(slzr);

			script<<"group"<<idx+1<<" = doc.addObject('App::DocumentObjectGroup','"<<model->get_name()<<"')\n";
			script<<"group"<<idx<<".addObject(group"<<(idx+1)<<")\n";

			// recursive call
			for(auto it=models.begin();it!=models.end();it++)
				combine_freecad_core(script,dependencies,idx+1,*it,time,idx==0 ? output_dir : output_dir/name,slzr,lg);
			
			// // create parts list for this level
			// if(models.size()>1){
			// 	script<<"group"<<idx+1<<" = doc.addObject('App::DocumentObjectGroup','"<<model->get_name()<<"')\n";
			// 	script<<"group"<<idx<<".addObject(group"<<(idx+1)<<")\n";

			// 	// recursive call
			// 	for(auto it=models.begin();it!=models.end();it++)
			// 		combine_freecad_core(script,dependencies,idx+1,*it,idx==0 ? output_dir : output_dir/name,slzr,lg);
			// }else{
			// 	// recursive call
			// 	for(auto it=models.begin();it!=models.end();it++)
			// 		combine_freecad_core(script,dependencies,idx,*it,idx==0 ? output_dir : output_dir/name,slzr,lg);
			// }

		}else{
			// add step file
			script<<"part = doc.addObject('Part::Feature','"<<model->get_name()<<"')\n"; 
			script<<"part.Shape = Part.read('"<<(output_dir/name).string()<<".step')\n";
			
			// set color
			script<<"part.ViewObject.LineColor = (1.0,1.0,1.0,0.0)\n";
			script<<"part.ViewObject.LineWidth = 0.5\n";
			script<<"part.ViewObject.ShapeColor = ("<<
				model->get_color()(0)<<","<<model->get_color()(1)<<","<<model->get_color()(2)<<",0.0)\n";

			// add to group
			script<<"group"<<std::to_string(idx)<<".addObject(part)\n";

			// add to dependency list
			dependencies.push_back(output_dir/(name + ".step"));
		}

	}

	// create combined freecad output file
	void FCBuilder::combine_freecad(
		std::ofstream& script,
		const mdl::ShModelPr& model,
		const fltp time,
		const boost::filesystem::path& output_dir, 
		const mdl::ShSerializerPr& slzr,
		const cmn::ShLogPr& lg){

		// create a stream
		std::list<boost::filesystem::path> dependencies;

		// start new document and load required libraries
		script<<"import FreeCAD,Draft,Part,Import\n";
		script<<"doc = App.newDocument('Combine')\n";
		script<<std::fixed<<std::setprecision(12);

		// create base group
		script<<"group0 = doc.addObject('App::DocumentObjectGroup','model')\n";

		// call recursive part
		combine_freecad_core(script, dependencies, 0, model, time, output_dir, slzr, lg);

		// recompute
		script<<"doc.recompute()\n";
	}

	// boolean
	void FCBuilder::create_freecad_combine(
		std::ofstream& cmnd,
		const std::string& part_name,
		const std::list<boost::filesystem::path> &input_files, 
		const boost::filesystem::path &output_dir, 
		const cmn::ShLogPr& /*lg*/){

		// create paths
		const boost::filesystem::path macro_file = output_dir/(part_name + ".FCMacro");
		const boost::filesystem::path step_file =  output_dir/(part_name + ".step");

		// create freecad script
		std::ofstream script(macro_file.string());

		// start new document and load required libraries
		script<<"import FreeCAD,Draft,Part,Import\n";
		script<<"doc = App.newDocument('Combine')\n";
		script<<"all=[]\n";

		// load part A
		for(auto it=input_files.begin();it!=input_files.end();it++){
			script<<"part = doc.addObject('Part::Feature','part')\n"; 
			script<<"part.Shape = Part.read('"<<(*it).string()<<"')\n";
			script<<"all.append(part)\n";
		}

		// recompute
		script<<"doc.recompute()\n";

		// combine with multi-fuse
		script<<"op = doc.addObject('Part::MultiFuse','Fusion')\n";
		script<<"op.Shapes = all\n";

		// recompute
		script<<"doc.recompute()\n";

		// take shape and store in feature
		script<<"F = doc.addObject('Part::Feature','"<<part_name<<"')\n";
		script<<"F.Shape = Part.getShape(op,'',needSubElement=False,refine=True)\n";
		
		// recompute
		script<<"doc.recompute()\n";

		// remove the operation
		script<<"doc.removeObject(op.Name)\n";

		// export to output file
		script<<"Import.export([F],'"<<step_file.string()<<"')\n";

		// close program
		script<<"exit()\n";

		// script done
		script.close();

		// make a command
		cmnd<<"freecad -c \""<<macro_file.string()<<"\"\n";
	}

	// boolean
	void FCBuilder::create_freecad_subtract(
		std::ofstream& cmnd,
		const std::string& part_name,
		const std::list<boost::filesystem::path> &input_files, 
		const boost::filesystem::path &output_dir, 
		const cmn::ShLogPr& /*lg*/){

		// create paths
		const boost::filesystem::path macro_file = output_dir/(part_name + ".FCMacro");
		const boost::filesystem::path step_file =  output_dir/(part_name + ".step");

		// create freecad script
		std::ofstream script(macro_file.string());

		// start new document and load required libraries
		script<<"import FreeCAD,Draft,Part,Import\n";
		script<<"doc = App.newDocument('Combine')\n";
		script<<"all=[]\n";

		script<<"base = doc.addObject('Part::Feature','part')\n"; 
		script<<"base.Shape = Part.read('"<<(*input_files.begin()).string()<<"')\n";

		// load parts
		for(auto it=std::next(input_files.begin());it!=input_files.end();it++){
			// load cutting tool
			script<<"part = doc.addObject('Part::Feature','part')\n"; 
			script<<"part.Shape = Part.read('"<<(*it).string()<<"')\n";

			// combine with cut
			script<<"op = doc.addObject('Part::Cut','Cut')\n";
			script<<"op.Base = base\n";
			script<<"op.Tool = part\n";

			// update base
			script<<"base = op\n";
		}

		// recompute
		script<<"doc.recompute()\n";

		// take shape and store in feature
		script<<"F = doc.addObject('Part::Feature','"<<part_name<<"')\n";
		script<<"F.Shape = Part.getShape(base,'',needSubElement=False,refine=True)\n";
		
		// recompute
		script<<"doc.recompute()\n";

		// remove the operation
		script<<"doc.removeObject(op.Name)\n";

		// export to output file
		script<<"Import.export([F],'"<<step_file.string()<<"')\n";

		// close program
		script<<"exit()\n";

		// close script
		script.close();

		// add to freecad
		cmnd<<"freecad -c \""<<macro_file.string()<<"\"\n";
	}

	// boolean
	void FCBuilder::create_freecad_intersect(
		std::ofstream& cmnd,
		const std::string& part_name,
		const std::list<boost::filesystem::path> &input_files, 
		const boost::filesystem::path &output_dir, 
		const cmn::ShLogPr& /*lg*/){

		// create paths
		const boost::filesystem::path macro_file = output_dir/(part_name + ".FCMacro");
		const boost::filesystem::path step_file =  output_dir/(part_name + ".step");

		// create freecad script
		std::ofstream script(macro_file.string());

		// start new document and load required libraries
		script<<"import FreeCAD,Draft,Part,Import\n";
		script<<"doc = App.newDocument('Combine')\n";
		script<<"all=[]\n";

		// load part A
		for(auto it=input_files.begin();it!=input_files.end();it++){
			script<<"part = doc.addObject('Part::Feature','part')\n"; 
			script<<"part.Shape = Part.read('"<<(*it).string()<<"')\n";
			script<<"all.append(part)\n";
		}

		// recompute
		script<<"doc.recompute()\n";

		// combine with multi-fuse
		script<<"op = doc.addObject('Part::MultiCommon','Common')\n";
		script<<"op.Shapes = all\n";

		// recompute
		script<<"doc.recompute()\n";

		// take shape and store in feature
		script<<"F = doc.addObject('Part::Feature','"<<part_name<<"')\n";
		script<<"F.Shape = Part.getShape(op,'',needSubElement=False,refine=True)\n";
		
		// recompute
		script<<"doc.recompute()\n";

		// remove the operation
		script<<"doc.removeObject(op.Name)\n";

		// export to output file
		script<<"Import.export([F],'"<<step_file.string()<<"')\n";

		// close program
		script<<"exit()\n";

		// script done
		script.close();

		// make a command
		cmnd<<"freecad -c \""<<macro_file.string()<<"\"\n";
	}

}}