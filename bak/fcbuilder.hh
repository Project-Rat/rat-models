/* Rat-ST - Spherical Tokamak Geometry
Copyright (C) 2020  Jeroen van Nugteren*/

#ifndef MDL_FC_BUILDER_HH
#define MDL_FC_BUILDER_HH

#include <armadillo> 
#include <memory>
#include <cassert>

// header files for models
#include "rat/models/model.hh"
#include "rat/models/serializer.hh"
#include "rat/models/splitable.hh"

// code specific to Raccoon
namespace rat{namespace mdl{

	// freecad builder
	class FCBuilder{
		public:
			// export to FreeCAD
			static void export_freecad(
				std::ofstream& cmnd,
				const mdl::ShModelPr& model, 
				const fltp time,
				const boost::filesystem::path &output_dir, 
				const std::string &fname,
				const cmn::ShLogPr& lg);

			// recursive freecad creation
			static void create_freecad(
				std::ofstream& cmnd,
				const mdl::ShModelPr& model, 
				const fltp time,
				const boost::filesystem::path& output_dir, 
				const mdl::ShSerializerPr& slzr, 
				const cmn::ShLogPr& lg,
				const arma::uword depth = 0);

			// recursive freecad creation
			static void combine_freecad_core(
				std::ofstream& script, 
				std::list<boost::filesystem::path>& dependencies,
				const arma::uword idx,
				const mdl::ShModelPr& model, 
				const fltp time,
				const boost::filesystem::path& output_dir,
				const mdl::ShSerializerPr& slzr,  
				const cmn::ShLogPr& lg);

			// create combined freecad output file
			static void combine_freecad(
				std::ofstream& script,
				const mdl::ShModelPr& model,
				const fltp time,
				const boost::filesystem::path& output_dir, 
				const mdl::ShSerializerPr& slzr,
				const cmn::ShLogPr& lg);

			// boolean
			static void create_freecad_combine(
				std::ofstream& cmnd,
				const std::string& part_name,
				const std::list<boost::filesystem::path> &input_files, 
				const boost::filesystem::path &output_dir, 
				const cmn::ShLogPr& lg);

			// boolean
			static void create_freecad_subtract(
				std::ofstream& cmnd,
				const std::string& part_name,
				const std::list<boost::filesystem::path> &input_files, 
				const boost::filesystem::path &output_dir, 
				const cmn::ShLogPr& lg);

			// boolean
			static void create_freecad_intersect(
				std::ofstream& cmnd,
				const std::string& part_name,
				const std::list<boost::filesystem::path> &input_files, 
				const boost::filesystem::path &output_dir, 
				const cmn::ShLogPr& lg);
	};

}}

#endif