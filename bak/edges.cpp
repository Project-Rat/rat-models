// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "edges.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	Edges::Edges(){

	}

	// constructor with input
	Edges::Edges(ShFramePr &frame, ShAreaPr &area){
		setup(frame, area);
	}

	// factory
	ShEdgesPr Edges::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<Edges>();
	}

	// factory with input
	ShEdgesPr Edges::create(ShFramePr &frame, ShAreaPr &area){
		return std::make_shared<Edges>(frame, area);
	}

	// setup function
	void Edges::setup(ShFramePr &frame, ShAreaPr &area){
		// check input
		assert(frame!=NULL); assert(area!=NULL);

		// set generator and area
		gen_ = frame; area_ = area;

		// get number of sections
		const arma::uword num_sections = frame->get_num_sections();

		// get edges
		const arma::Col<arma::uword> c = area->get_corner_nodes();

		// get nodes
		const arma::Mat<fltp> Rn = area->get_nodes();

		// edge matrices
		Re_.set_size(c.n_elem, num_sections);

		// walk over sections
		for(arma::uword i=0;i<num_sections;i++){
			// walk over edges
			for(arma::uword j=0;j<c.n_elem;j++){
				// perform shift and store
				Re_(j,i) = frame->perform_shift(i,Rn(0,c(j)),Rn(1,c(j)));
			}
		}

		// get section and turn indices
		section_ = frame->get_section();
		turn_ = frame->get_turn();
	}

	// set operating temperature
	void Edges::set_operating_temperature(const fltp operating_temperature){
		operating_temperature_ = operating_temperature;
	}

	// get operating temperature
	fltp Edges::get_operating_temperature() const{
		return operating_temperature_;
	}

	// transformations
	void Edges::apply_transformations(const ShTransPrList &trans){
		for(arma::uword i=0;i<trans.n_elem;i++)
			apply_transformation(trans(i));
	}
		
	// apply a transformation
	void Edges::apply_transformation(const ShTransPr &trans){
		// apply transformations
		for(arma::uword j=0;j<Re_.n_rows;j++)
			for(arma::uword i=0;i<Re_.n_cols;i++)
				trans->apply_coords(Re_(j,i));
	}

	// calculate edge length
	arma::Mat<fltp> Edges::calc_ell() const{
		// allocate lengths
		arma::Mat<fltp> ell(Re_.n_rows,Re_.n_cols);

		// walk over edges
		for(arma::uword i=0;i<Re_.n_rows;i++){
			// walk over sections
			for(arma::uword j=0;j<Re_.n_cols;j++){
				ell(i,j) = arma::sum(cmn::Extra::vec_norm(Re_(i,j)));
			}
		}

		// return length matrix
		return ell;
	}


	// get xyz matrices
	void Edges::create_xyz(
		arma::field<arma::Mat<fltp> > &x, 
		arma::field<arma::Mat<fltp> > &y, 
		arma::field<arma::Mat<fltp> > &z) const{ 

		// count number sectoins and edges
		const arma::uword num_sections = Re_.n_cols;
		const arma::uword num_edges = Re_.n_rows;

		// allocate
		x.set_size(1,num_sections); 
		y.set_size(1,num_sections); 
		z.set_size(1,num_sections);

		// walk over sections
		for(arma::uword i=0;i<num_sections;i++){
			// allocate edge matrix for this section
			x(i).set_size(num_edges,Re_(0,i).n_cols);
			y(i).set_size(num_edges,Re_(0,i).n_cols);
			z(i).set_size(num_edges,Re_(0,i).n_cols);

			// walk over edges
			for(arma::uword j=0;j<num_edges;j++){
				// copy edge
				x(i).row(j) = Re_(j,i).row(0);
				y(i).row(j) = Re_(j,i).row(1);
				z(i).row(j) = Re_(j,i).row(2);
			}
		}
	}


	// export edges to freecad
	void Edges::export_freecad(cmn::ShFreeCADPr freecad) const{
		// get counters
		const arma::uword num_edges = Re_.n_rows;

		// make sure it is four edges
		if(num_edges!=4)rat_throw_line("freecad currently only works with four edges");

		// allocate and get edge matrices
		arma::field<arma::Mat<fltp> > x,y,z; create_xyz(x,y,z);

		// write to freecad
		freecad->write_edges(x,y,z,section_,turn_,myname_);
	}

	// export edges to freecad
	void Edges::export_opera(cmn::ShOperaPr opera) const{
		// get counters
		const arma::uword num_edges = Re_.n_rows;

		// make sure it is four edges
		if(num_edges!=4)rat_throw_line("opera currently only works with four edges");

		// allocate and get edge matrices
		arma::field<arma::Mat<fltp> > x,y,z; create_xyz(x,y,z);

		// calculate current density
		const fltp J = 0; //number_turns_*circuit_->get_operating_current()/area_crss_;

		// write edges with current density
		opera->write_edges(x,y,z,J);
	}

}}