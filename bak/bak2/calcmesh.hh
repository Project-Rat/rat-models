// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALC_MESH_HH
#define MDL_CALC_MESH_HH

#include <armadillo> 
#include <memory>
#include <iomanip>
#include <sstream>

#include "rat/common/defines.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/log.hh"
#include "rat/common/parfor.hh"

#include "modelgroup.hh"
#include "mesh.hh"
#include "meshcoil.hh"
#include "area.hh"
#include "frame.hh"
#include "vtkunstr.hh"
#include "calcfieldmap.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcMesh> ShCalcMeshPr;
	typedef arma::field<ShCalcMeshPr> ShCalcMeshPrList;

	// template for coil
	class CalcMesh: public CalcFieldMap{		
		// internal storage
		protected:
			// model
			ShModelPr target_model_;

			// elements
			//arma::Mat<arma::uword> n_;
			arma::field<arma::Mat<arma::uword> > n_;

			// list of mesh geometries
			ShMeshPrList meshes_;

			// number of coils
			arma::uword num_objects_;

			// number of nodes and elements in each object
			arma::Row<arma::uword> num_nodes_object_;
			arma::Row<arma::uword> num_elements_object_;
			
			// total number of nodes and elements
			arma::uword num_nodes_;
			arma::uword num_elements_;

			// indexing into the separate objects
			arma::Row<arma::uword> idx_nodes_;
			arma::Row<arma::uword> idx_elem_;

		// methods
		public:
			// constructor	
			CalcMesh();
			CalcMesh(ShModelPr source_model, ShModelPr target_model);

			// factory
			static ShCalcMeshPr create();
			static ShCalcMeshPr create(ShModelPr source_model, ShModelPr target_model);

			// setting the model
			void set_target_model(ShModelPr target_model);

			// get target coordinates
			arma::Mat<double> get_coords() const;

			// get properties from coils
			arma::Mat<double> get_nodal_current_density() const;
			arma::Row<double> get_nodal_temperature() const;

			// calculation of properties
			arma::Row<double> calc_Tc(const arma::Mat<double> &Bn, const arma::Mat<double> &Jn, const bool use_parallel = true) const;
			arma::Row<double> calc_alpha(const arma::Mat<double> &Bn) const;
			arma::Row<double> calc_Je(const arma::Mat<double> &Bn, const arma::Mat<double> &Tn, const bool use_parallel = true) const;
			arma::Mat<double> calc_E(const arma::Mat<double> &Bn, const arma::Mat<double> &Jn, const arma::Mat<double> &Tn, const bool use_parallel = true) const;
			arma::Row<double> calc_pct_load(const arma::Mat<double> &Bn, const arma::Mat<double> &Jn, const arma::Mat<double> &Tn, const bool use_parallel = true) const;

			// get orientation
			arma::Mat<double> get_longitudinal() const;
			arma::Mat<double> get_transverse() const;
			arma::Mat<double> get_normal() const;

			// setup function
			void setup(cmn::ShLogPr lg = cmn::NullLog::create()) override;

			// export to vtk file
			ShVTKUnstrPr export_vtk(cmn::ShLogPr lg = cmn::NullLog::create()) const;
			virtual void write(cmn::ShLogPr lg = cmn::NullLog::create()) override;

			// gmsh file export
			// void export_gmsh(cmn::ShGmshFilePr gmsh) const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
