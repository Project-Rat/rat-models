// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelsources.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ModelSources::ModelSources(){

	}

	// constructor with model input
	ModelSources::ModelSources(ShModelPr model){
		if(model==NULL)rat_throw_line("model points to NULL");
		add_model(model);
	}

	// factory
	ShModelSourcesPr ModelSources::create(){
		//return ShModelCoilPr(new ModelCoil);
		return std::make_shared<ModelSources>();
	}

	// factory
	ShModelSourcesPr ModelSources::create(ShModelPr model){
		return std::make_shared<ModelSources>(model);
	}

	// setup function
	void ModelSources::setup(cmn::ShLogPr lg){
		// header
		lg->msg(2,"%s%sSETTING UP LINE SOURCES%s\n",KBLD,KGRN,KNRM);

		// create meshes
		ShMeshPrList meshes = create_mesh();
		
		// display meshes
		Mesh::display(lg, meshes);

		// // select only coilmeshes
		// ShMeshCoilPrList cmshes = MeshCoil::extract(meshes);

		// count number of meshes
		//const arma::uword num_coils = cmshes.n_elem;
		
		// allocate sources
		fmm::ShCurrentSourcesPrList srcs(meshes.n_elem);

		// create source objects
		for(arma::uword i=0;i<meshes.n_elem;i++)
			srcs(i) = meshes(i)->create_current_sources();

		// combine current sources
		set_sources(srcs);

		// calculate field at nodes
		set_target_coords(Rs_);

		// done
		lg->msg(-2,"\n");
	}

}}

