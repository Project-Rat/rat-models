// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "modelinductance.hh"

// code specific to Rat
namespace rat{namespace mdl{

// constructor
	ModelMerge::ModelMerge(){

	}

	// constructor with model input
	ModelMerge::ModelMerge(ShModelPr model){
		models_.set_size(1);
		models_(0) = model;
	}

	// constructor with multiple input models
	ModelMerge::ModelMerge(ShModelPrList models){
		models_ = models;
	}

	// factory
	ShModelMergePr ModelMerge::create(){
		return std::make_shared<ModelMerge>();
	}

	// factory
	ShModelMergePr ModelMerge::create(ShModelPr model){
		return std::make_shared<ModelMerge>(model);
	}

	// factory
	ShModelMergePr ModelMerge::create(ShModelPrList models){
		return std::make_shared<ModelMerge>(models);
	}

	// get number of calculation objects in all submodels
	arma::uword ModelMerge::get_num_objects() const{
		return 1;
	}

	// serialization type
	static std::string ModelMerge::get_type(){
		return "rat::mdl::modelmerge";
	}

	// serialization
	void ModelMerge::serialize(Json::Value &js, cmn::SList &list) const{

	}
	
	// deserialization
	virtual void ModelMerge::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

	}

}}