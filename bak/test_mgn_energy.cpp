// Copyright 2025 Jeroen van Nugteren
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>
#include <iostream>
#include <cmath>

#include <boost/math/special_functions/ellint_1.hpp>
#include <boost/math/special_functions/ellint_2.hpp>

#include "rat/common/error.hh"
#include "rat/common/log.hh"

#include "rat/mlfmm/soleno.hh"

#include "crosscircle.hh"
#include "pathaxis.hh"
#include "modelbar.hh"
#include "calcinductance.hh"

// inductance of sheet analytical (Lorenz formula)
// a is radius, l is length, n is number turns
rat::fltp sheet_inductance(const rat::fltp a, const rat::fltp l, const rat::fltp n = RAT_CONST(1.0)){
	const rat::fltp d = std::sqrt(4*a*a+l*l);
	const rat::fltp k = (2*a)/d;
	const rat::fltp phi = arma::Datum<rat::fltp>::pi/2; // for complete elliptical integrals
	const rat::fltp F = boost::math::ellint_1(k,phi); // elliptical integral first kind
	const rat::fltp E = boost::math::ellint_2(k,phi); // elliptical integral second kind
	const rat::fltp L = (4*arma::Datum<rat::fltp>::pi*n*n/(3*l*l))*(d*(4*a*a - l*l)*E + d*l*l*F-8*a*a*a);
	return (arma::Datum<rat::fltp>::mu_0/(4*arma::Datum<rat::fltp>::pi))*L;
}


// main
int main(){
	// settings
	const rat::fltp radius = RAT_CONST(0.004);
	const rat::fltp height = RAT_CONST(0.02);
	const rat::fltp element_size = RAT_CONST(0.0005);
	const rat::fltp Mz = RAT_CONST(1.0)/arma::Datum<rat::fltp>::mu_0; // A/m
	const rat::fltp tolerance = RAT_CONST(1e-1); // this is actually a very difficult calculation so we need to allow for some tolerance, need to add analytical surface integral for field calculation in future
	const arma::uword num_gauss_surface = 6llu;
	const rat::fltp softness_factor = RAT_CONST(0.1);
	const rat::fltp time = RAT_CONST(0.0);
	
	// create rectangle object
	const rat::mdl::ShCrossCirclePr crss = rat::mdl::CrossCircle::create(radius,element_size);

	// circular path
	const rat::mdl::ShPathAxisPr base = rat::mdl::PathAxis::create('z','x',height,{0,0,0},element_size);

	// create area
	const rat::mdl::ShModelBarPr bar = rat::mdl::ModelBar::create(base, crss);
	bar->set_magnetisation({Mz,0,0}); // in LND coords
	bar->set_softening(softness_factor);
	bar->set_num_gauss_surface(num_gauss_surface);

	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// calculate volume
	const rat::mdl::ShCalcInductancePr inductance = rat::mdl::CalcInductance::create(bar);
	const rat::mdl::ShInductanceDataPr data = inductance->calculate_inductance(time,lg);
	const rat::fltp energy_rat = data->get_stored_energy();

	// analytical
	const rat::fltp L = sheet_inductance(radius, height);
	const rat::fltp surface_current_density = Mz;
	const rat::fltp surface_current = surface_current_density*height;
	const rat::fltp energy_analytical = RAT_CONST(0.5)*L*surface_current*surface_current;

	const rat::fltp err = std::abs(energy_rat-energy_analytical)/energy_analytical;

	std::cout<<err<<std::endl;

	// check output
	if(err>tolerance)rat_throw_line("error is larger than tolerance");
}