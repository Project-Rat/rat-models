// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "pathtransition.hh"

// common headers
#include "rat/common/extra.hh"

// models headers
#include "pathbezier.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathTransition::PathTransition(){
		set_name("transition");
	}

	// factory
	ShPathTransitionPr PathTransition::create(){
		return std::make_shared<PathTransition>();
	}

	// retreive model at index
	ShPathPr PathTransition::get_path(const arma::uword index) const{
		auto it = input_paths_.find(index);
		if(it==input_paths_.end())rat_throw_line("index does not exist");
		return (*it).second;
	}

	// delete model at index
	bool PathTransition::delete_path(const arma::uword index){	
		auto it = input_paths_.find(index);
		if(it==input_paths_.end())return false;
		(*it).second = NULL; return true;
	}

	// re-index nodes after deleting
	void PathTransition::reindex(){
		Transformations::reindex();
		std::map<arma::uword, ShPathPr> new_paths; arma::uword idx = 1;
		for(auto it=input_paths_.begin();it!=input_paths_.end();it++)
			if((*it).second!=NULL)new_paths.insert({idx++, (*it).second});
		input_paths_ = new_paths;
	}

	// function for adding a path to the path list
	arma::uword PathTransition::add_path(const ShPathPr &path){
		// check input
		assert(path!=NULL); 

		// get counters
		const arma::uword index = input_paths_.size()+1;

		// insert
		input_paths_.insert({index, path});

		// return index
		return index;
	}

	// get number of paths
	arma::uword PathTransition::get_num_paths() const{
		return input_paths_.size();
	}
	
	// frame creation
	ShFramePr PathTransition::create_frame(const MeshSettings &stngs) const{
		// check validity
		is_valid(true);

		// creat frames for both input paths
		const ShFramePr frame1 = input_paths_.at(1)->create_frame(stngs);
		const ShFramePr frame2 = input_paths_.at(2)->create_frame(stngs);

		// create S-curves


	}

	// validity check
	bool PathTransition::is_valid(const bool enable_throws) const{
		if(input_paths_.size()!=2){if(enable_throws){rat_throw_line("need two input paths");} return false;};
		for(auto it=input_paths_.begin();it!=input_paths_.end();it++)if(!(*it).second->is_valid(enable_throws))return false;
		return true;
	}


	// get type
	std::string PathTransition::get_type(){
		return "rat::mdl::pathtransition";
	}

	// method for serialization into json
	void PathTransition::serialize(Json::Value &js, cmn::SList &list) const{

		// parent objects
		Path::serialize(js,list);
		Transformations::serialize(js,list);

		// type
		js["type"] = get_type();

		// input paths
		for(auto it = input_paths_.begin();it!=input_paths_.end();it++)
			js["input_paths"].append(cmn::Node::serialize_node((*it).second, list));
	}

	// method for deserialisation from json
	void PathTransition::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent objects
		Path::deserialize(js,list,factory_list,pth);
		Transformations::deserialize(js,list,factory_list,pth);
		
		// input paths
		for(auto it = js["input_paths"].begin();it!=js["input_paths"].end();it++)
			add_path(cmn::Node::deserialize_node<Path>(
				(*it), list, factory_list, pth));
	}

}}