// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// specific headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"
#include "rat/common/elements.hh"
#include "rat/common/extra.hh"

// model headers
#include "crossrectangle.hh"
#include "modelrectangle.hh"
#include "modelnl.hh"
#include "pathaxis.hh"
#include "hbcurve.hh"
#include "calcpoints.hh"
#include "modelmirror.hh"
#include "crosspoint.hh"
#include "crossrectangle.hh"
#include "pathrectangle.hh"
#include "modelarray.hh"
#include "pathgroup.hh"
#include "pathpoint.hh"

// main
int main(){
	// check if NL solver enabled
	#ifndef ENABLE_NL_SOLVER
	rat_throw_line("this test requires the nlsolver");
	#endif

	// SETTINGS
	// coil
	const rat::fltp time = 0.0;
	const rat::fltp coil_width = 150e-3;
	const rat::fltp coil_length = 150e-3;
	const rat::fltp coil_radius = 25e-3;
	const rat::fltp coil_thickness = 25e-3;
	const rat::fltp coil_cable_width = 100e-3;
	const rat::fltp coil_current = 1000.0; // ampere turns
	const rat::fltp coil_element_size = 5e-3;

	// strips
	const rat::fltp strip_width = 50e-3;
	const rat::fltp strip_thickness = 3.2e-3;
	const rat::fltp strip_height = 126.4e-3;
	const rat::fltp strip_length = 120e-3;
	const rat::fltp strip_offset = 40e-3;
	const rat::fltp strip_shift = 4.2e-3;
	const rat::fltp strip_element_size = 3.2e-3;

	// GEOMETRY
	// create the racetrack
	const rat::mdl::ShModelRectanglePr coil = rat::mdl::ModelRectangle::create();
	coil->set_arc_radius(coil_radius);
	coil->set_length(coil_length);
	coil->set_width(coil_width);
	coil->set_coil_thickness(coil_thickness);
	coil->set_coil_width(coil_cable_width);
	coil->set_element_size(coil_element_size);
	coil->set_number_turns(1);
	coil->set_operating_current(coil_current);
	coil->set_circuit_index(1);
	coil->set_name("Coil");

	// create hb-curve
	const rat::mdl::ShHBCurveTablePr hb = rat::mdl::HBCurveTable::create();
	hb->set_team13();

	// create a cross section for the strip
	const rat::mdl::ShCrossRectanglePr rect = rat::mdl::CrossRectangle::create(
		-strip_thickness/2,strip_thickness/2,-strip_width/2,strip_width/2,strip_element_size);

	// axial path
	const rat::mdl::ShPathAxisPr axis1 = rat::mdl::PathAxis::create(
		'z','y',strip_height,rat::cmn::Extra::null_vec(),strip_element_size);

	// create the non-linear magnetic steel strips
	const rat::mdl::ShModelNLPr strip1 = rat::mdl::ModelNL::create(axis1,rect,hb);
	strip1->set_name("strip1");
	
	// axial path
	const rat::mdl::ShPathAxisPr axis2 = rat::mdl::PathAxis::create(
		'x','y',strip_length,{strip_shift/2+strip_length/2,strip_offset,(strip_height-strip_thickness)/2},strip_element_size);

	// create the non-linear magnetic steel strips
	const rat::mdl::ShModelNLPr strip2 = rat::mdl::ModelNL::create(axis2,rect,hb);
	strip2->set_name("strip2");

	// axial path
	const rat::mdl::ShPathAxisPr axis3 = rat::mdl::PathAxis::create(
		'x','y',strip_thickness,{strip_shift/2+strip_length+strip_thickness/2,strip_offset,(strip_height-strip_thickness)/2},strip_element_size);

	// create the non-linear magnetic steel strips
	const rat::mdl::ShModelNLPr strip3 = rat::mdl::ModelNL::create(axis3,rect,hb);
	strip3->set_name("strip3");

	// axial path
	const rat::mdl::ShPathAxisPr axis4 = rat::mdl::PathAxis::create(
		'z','y',(strip_height-2*strip_thickness)/2,{strip_shift/2+strip_length+strip_thickness/2,strip_offset,(strip_height-2*strip_thickness)/4},strip_element_size);

	// create the non-linear magnetic steel strips
	const rat::mdl::ShModelNLPr strip4 = rat::mdl::ModelNL::create(axis4,rect,hb);
	strip4->set_name("strip4");

	// create mirror
	const rat::mdl::ShModelMirrorPr mirror1 = rat::mdl::ModelMirror::create();
	mirror1->add_model(strip2); mirror1->add_model(strip3); mirror1->add_model(strip4);
	mirror1->set_plane_vector(rat::cmn::Extra::unit_vec('z'));
	mirror1->set_name("z-mirror");

	// create mirror
	const rat::mdl::ShModelMirrorPr mirror2 =rat::mdl:: ModelMirror::create();
	mirror2->add_model(mirror1);
	mirror2->set_plane_vector(rat::cmn::Extra::unit_vec('x'));
	mirror2->set_keep_original(false);
	mirror2->set_name("x-mirror");

	const rat::mdl::ShModelMirrorPr mirror3 = rat::mdl::ModelMirror::create();
	mirror3->add_model(mirror2);
	mirror3->set_plane_vector(rat::cmn::Extra::unit_vec('y'));
	mirror3->set_keep_original(false);
	mirror3->set_name("y-mirror");

	// create a group for all the magnetic steel
	const rat::mdl::ShModelGroupPr steel = rat::mdl::ModelGroup::create(std::list<rat::mdl::ShModelPr>{strip1,mirror1,mirror3});
	steel->set_color({15.0/255,76.0/255,82.0/255},true);
	steel->set_name("Steel");

	// create model
	const rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create(std::list<rat::mdl::ShModelPr>{coil,steel});
	model->set_name("Model Tree");
	

	// create flux planes
	const rat::mdl::ShModelGroupPr flux_planes = rat::mdl::ModelGroup::create();
	const arma::Col<rat::fltp> xpos2{2.1e-3,10.0e-3,20.0e-3,30.0e-3,40.0e-3,50.0e-3,60.0e-3,80.0e-3,100.0e-3,110.0e-3,122.1e-3};
	const rat::mdl::ShCrossRectanglePr crss = rat::mdl::CrossRectangle::create(-strip_thickness/2,strip_thickness/2,-strip_width/2,strip_width/2,strip_element_size);
	for(arma::uword i=0;i<7;i++){
		const rat::mdl::ShPathGroupPr point = rat::mdl::PathGroup::create({rat::mdl::PathPoint::create()});
		point->set_start_coord({0,0,i*10e-3});
		point->set_start_longitudinal({0,0,1});
		point->set_start_normal({1,0,0});
		const rat::mdl::ShModelMeshPr mesh = rat::mdl::ModelMesh::create(point, crss);
		flux_planes->add_model(mesh);
	}
	for(arma::uword i=0;i<xpos2.n_elem;i++){
		const rat::mdl::ShPathGroupPr point = rat::mdl::PathGroup::create({rat::mdl::PathPoint::create()});
		point->set_start_coord({xpos2(i), strip_offset,strip_height/2 - strip_thickness/2});
		point->set_start_longitudinal({1,0,0});
		point->set_start_normal({0,0,1});
		const rat::mdl::ShModelMeshPr mesh = rat::mdl::ModelMesh::create(point, crss);
		flux_planes->add_model(mesh);
	}
	for(arma::uword i=0;i<7;i++){
		const rat::mdl::ShPathGroupPr point = rat::mdl::PathGroup::create({rat::mdl::PathPoint::create()});
		point->set_start_coord({123.7e-3,strip_offset,60e-3 - arma::sword(i)*10e-3});
		point->set_start_longitudinal({0,0,-1});
		point->set_start_normal({-1,0,0});
		const rat::mdl::ShModelMeshPr mesh = rat::mdl::ModelMesh::create(point, crss);
		flux_planes->add_model(mesh);
	}
	const arma::uword num_planes = flux_planes->num_models();

	// export json
	rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(rat::mdl::ModelGroup::create({model,flux_planes}));
	slzr->export_json("team13.json");

	// powering circuits
	const rat::mdl::ShCircuitPr circuit1 = rat::mdl::Circuit::create(1,rat::mdl::DriveDC::create(1000.0));
	const rat::mdl::ShCircuitPr circuit2 = rat::mdl::Circuit::create(1,rat::mdl::DriveDC::create(3000.0));
	


	// CALCULATE
	// create a cache for solve storage
	const rat::mdl::ShSolverCachePr cache = rat::mdl::SolverCache::create();

	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// create meshes
	const std::list<rat::mdl::ShMeshDataPr> meshes = flux_planes->create_meshes({},rat::mdl::MeshSettings());
	if(meshes.size()!=num_planes)rat_throw_line("number of meshes does not match number of planes");

	// create coordinates for calculation
	arma::uword cnt = 0;
	arma::field<arma::Mat<rat::fltp> > Rt_fld(1,num_planes);
	arma::field<arma::Mat<rat::fltp> > area_fld(1,num_planes); 
	arma::field<arma::Mat<rat::fltp> > N_fld(1,num_planes);
	for(const auto& mesh : meshes){
		Rt_fld(cnt) = mesh->get_element_coords();
		area_fld(cnt) = mesh->calc_cis_areas();
		N_fld(cnt) = rat::cmn::Quadrilateral::calc_face_normals(mesh->get_nodes(), mesh->get_elements());
		cnt++;
	}

	// combine target coords
	const arma::Mat<rat::fltp> Rt = rat::cmn::Extra::field2mat(Rt_fld);

	// calculate field on these planes
	const rat::mdl::ShCalcPointsPr calc_points = rat::mdl::CalcPoints::create(model);
	calc_points->add_circuit(circuit1);
	calc_points->set_coords(Rt);
	const rat::mdl::ShMeshDataPr point_data = calc_points->calculate_points(time, lg, cache);
	const arma::Mat<rat::fltp> B = point_data->get_field('B');

	// get field epr mesh
	arma::uword cnt2=0, idx1=0;
	arma::field<arma::Mat<rat::fltp> > Bfld(1,num_planes);
	arma::Col<rat::fltp> average_flux_density(num_planes);
	for(const auto& mesh : meshes){
		const arma::uword num_elem = mesh->get_num_elements();
		const arma::uword idx2 = idx1 + num_elem - 1;
		average_flux_density(cnt2) = arma::accu(rat::cmn::Extra::dot(B.cols(idx1,idx2),N_fld(cnt2))%area_fld(cnt2))/arma::accu(area_fld(cnt2));
		cnt2++; idx1+=num_elem;
	}

	// expected values from measurement
	const arma::Col<rat::fltp> measured_flux_density{
		1.333,1.329,1.286,1.225,1.129,0.985,0.655, 
		0.259,0.453,0.554,0.637,0.698,0.755,0.809,0.901,0.945,0.954,0.956, 
		0.960,0.965,0.970,0.974,0.981,0.984,0.985};
	if(measured_flux_density.n_elem!=num_planes)rat_throw_line("measured flux density table does not match number of planes");


	std::cout<<arma::join_horiz(average_flux_density,measured_flux_density)<<std::endl;



	// // create table header
	// const arma::uword table_size = 83;
	// lg->msg(2,"%s%sResults%s\n",KBLD,KGRN,KNRM);
	// lg->msg(2,"%sAverage Flux Density in steel plate (1000 AT)%s\n",KBLU,KNRM);
	// lg->msg("%5s | %15s %15s %15s | %12s %12s\n","No.","x [mm]","y [mm]","z [mm]","B [T] (nmrc)","B [T] (msrd)");
	
	// lg->msg("="); 
	// for(arma::uword i=0;i<table_size-1;i++)lg->msg(0,"=");
	// lg->msg("\n");

	// // expected values from measurement
	// const arma::Col<rat::fltp> measured_flux_density{
	// 	1.333,1.329,1.286,1.225,1.129,0.985,0.655, 
	// 	0.259,0.453,0.554,0.637,0.698,0.755,0.809,0.901,0.945,0.954,0.956, 
	// 	0.960,0.965,0.970,0.974,0.981,0.984,0.985};
	// if(measured_flux_density.n_elem!=num_wires)rat_throw_line("measured flux density table does not match number of wires");

	// // walk over meshes and calculate averaged flux
	// auto it = meshes.begin(); arma::uword cnt = 0;
	// for(arma::uword i=0;i<7;i++,it++,cnt++){
	// 	// calculate flux from vector potential
		
	// 	lg->msg("%+05llu | %+06.1f<x<%+06.1f %+06.1f<y<%06.1f %015.2f | %012.4e %012.4e\n",cnt,
	// 		-1e3*strip_thickness/2,1e3*strip_thickness/2,
	// 		-1e3*strip_width/2,1e3*strip_width/2,i*10.0,flux_density,measured_flux_density(cnt));
	// }
	
	// lg->msg("="); 
	// for(arma::uword i=0;i<table_size-1;i++)lg->msg(0,"=");
	// lg->msg("\n");

	// for(arma::uword i=0;i<xpos2.n_elem;i++,it++,cnt++){
	// 	// calculate flux from vector potential
	// 	const rat::fltp flux = (*it)->calculate_flux();
	// 	const rat::fltp flux_density = flux/(strip_width*strip_thickness);
	// 	lg->msg("%+05llu | %015.2f %+06.1f<y<%+06.1f %+06.1f<z<%+06.1f | %012.4e %012.4e\n",cnt,
	// 		1e3*xpos2(i),
	// 		-1e3*strip_width/2 + strip_offset, 1e3*strip_width/2 + strip_offset,
	// 		1e3*(strip_height/2 - strip_thickness), 1e3*strip_height/2, flux_density, 
	// 		measured_flux_density(cnt));
	// }
	
	// lg->msg("="); 
	// for(arma::uword i=0;i<table_size-1;i++)lg->msg(0,"=");
	// lg->msg("\n");

	// for(arma::uword i=0;i<7;i++,it++,cnt++){
	// 	// calculate flux from vector potential
	// 	const rat::fltp flux = (*it)->calculate_flux();
	// 	const rat::fltp flux_density = flux/(strip_width*strip_thickness);
	// 	lg->msg("%+05llu | %+06.1f<x<%+06.1f %+06.1f<y<%+06.1f %015.2f | %012.4e %012.4e\n",i,
	// 		-1e3*strip_thickness/2,1e3*strip_thickness/2,
	// 		-1e3*strip_width/2,1e3*strip_width/2,60.0 - arma::sword(i)*10.0,flux_density,measured_flux_density(cnt));
	// }
	
	// lg->msg("="); 
	// for(arma::uword i=0;i<table_size-1;i++)lg->msg(0,"=");
	// lg->msg("\n");

	// // sanity check
	// if(it!=meshes.end())rat_throw_line("not all wires were included in table");
	// if(cnt!=num_wires)rat_throw_line("counter does not match number of meshes");


	// lg->msg(-4,"\n");
	
	

}
