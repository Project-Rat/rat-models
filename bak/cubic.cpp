#include <iostream>
#include <armadillo>

#include "rat/common/typedefs.hh"

#include <boost/math/interpolators/barycentric_rational.hpp>

int main(){
	arma::Col<rat::fltp> x = {1,2,3,4,5};
	arma::Col<rat::fltp> y = {1,2,1,0,2};

	// Now we want to interpolate this potential at any r:
	boost::math::interpolators::barycentric_rational<rat::fltp> fun(x.memptr(), y.memptr(), x.n_elem);

	std::cout<<fun(2.3)<<std::endl;




}