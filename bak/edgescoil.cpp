// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "edgescoil.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	EdgesCoil::EdgesCoil(){

	}

	// constructor with input
	EdgesCoil::EdgesCoil(ShFramePr &frame, ShAreaPr &area){
		setup(frame, area);
	}

	// factory
	ShEdgesCoilPr EdgesCoil::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<EdgesCoil>();
	}

	// factory with input
	ShEdgesCoilPr EdgesCoil::create(ShFramePr &frame, ShAreaPr &area){
		return std::make_shared<EdgesCoil>(frame, area);
	}

	// set operating current
	void EdgesCoil::set_operating_current(const fltp operating_current){
		operating_current_ = operating_current;
	}

	// get operating current
	fltp EdgesCoil::get_operating_current() const{
		return operating_current_;
	}

	// set number of turns
	void EdgesCoil::set_number_turns(const fltp number_turns){
		number_turns_ = number_turns;
	}

	// get number of turns
	fltp EdgesCoil::get_number_turns() const{
		return number_turns_;
	}

	// set material
	void EdgesCoil::set_material(rat::mat::ShConductorPr material){
		material_ = material;
	}

	// get material
	rat::mat::ShConductorPr EdgesCoil::get_material() const{
		return material_;
	}


	// export edges to freecad
	void EdgesCoil::export_freecad(cmn::ShFreeCADPr freecad) const{
		// get counters
		const arma::uword num_edges = Re_.n_rows;

		// make sure it is four edges
		if(num_edges!=4)rat_throw_line("freecad currently only works with four edges");

		// allocate and get edge matrices
		arma::field<arma::Mat<fltp> > x,y,z; create_xyz(x,y,z);

		// write to freecad
		freecad->write_edges(x,y,z,section_,turn_,myname_);
	}

	// export edges to freecad
	void EdgesCoil::export_opera(cmn::ShOperaPr opera) const{
		// get counters
		const arma::uword num_edges = Re_.n_rows;

		// make sure it is four edges
		if(num_edges!=4)rat_throw_line("opera currently only works with four edges");

		// allocate and get edge matrices
		arma::field<arma::Mat<fltp> > x,y,z; create_xyz(x,y,z);

		// calculate current density
		const fltp J = number_turns_*operating_current_/area_->get_area();

		// write edges with current density
		opera->write_edges(x,y,z,J);
	}

	// copy constructor
	cmn::ShNodePr EdgesCoil::copy() const{
		return std::make_shared<EdgesCoil>(*this);
	}

}}