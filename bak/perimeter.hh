// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MESH_PERIMETER_HH
#define MDL_MESH_PERIMETER_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/typedefs.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Perimeter> ShPerimeterPr;

	// path virtual class description
	class Perimeter{
		// properties
		protected:
			// nodes [2xN] matrix
			arma::Mat<fltp> Rn_;

			// elements
			arma::Mat<arma::uword> n_;
			
		// methods
		public:
			// default constructor
			Perimeter();

			// constructor with input
			Perimeter(const arma::Mat<fltp> &Rn, const arma::Mat<arma::uword> &n);

			// factory
			static ShPerimeterPr create();

			// factory with input
			static ShPerimeterPr create(const arma::Mat<fltp> &Rn, const arma::Mat<arma::uword> &n);

			// set mesh
			void set_mesh(const arma::Mat<fltp> &Rn, const arma::Mat<arma::uword> &n);

			// calculate length of periphery
			fltp calculate_length() const;

			// get number of elements
			arma::uword get_num_elements() const;

			// get number of nodes
			arma::uword get_num_nodes() const;

			// access nodes and elements
			arma::Mat<fltp> get_nodes() const;
			arma::Mat<arma::uword> get_elements() const;
	};

}}

#endif
