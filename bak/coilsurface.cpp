// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "coilsurface.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	CoilSurface::CoilSurface(){

	}

	// constructor with input
	CoilSurface::CoilSurface(
		const arma::Mat<double> &R, 
		const arma::Mat<double> &L,
		const arma::Mat<double> &N,
		const arma::Mat<double> &D,
		const arma::Mat<arma::uword> &s){
		
		// make sure it is a quad mesh
		assert(s.n_rows==4); assert(arma::max(arma::max(s))<R.n_cols);
		assert(L.n_cols==R.n_cols); assert(N.n_cols==R.n_cols); assert(D.n_cols==R.n_cols);

		// set mesh
		R_ = R; L_ = L; N_ = N; D_ = D; s_ = s;
	}


	// factory
	ShCoilSurfacePr CoilSurface::create(){
		return std::make_shared<CoilSurface>();
	}

	// factory with input
	ShCoilSurfacePr CoilSurface::create(
		const arma::Mat<double> &R, 
		const arma::Mat<double> &L,
		const arma::Mat<double> &N,
		const arma::Mat<double> &D,
		const arma::Mat<arma::uword> &s){
		return std::make_shared<CoilSurface>(R,L,N,D,s);
	}

	// set conductor
	void CoilSurface::set_circuit(ShCircuitPr circuit){
		assert(circuit!=NULL);
		circuit_ = circuit;
	}

	// get conductor
	ShCircuitPr CoilSurface::get_circuit() const{
		assert(circuit_!=NULL);
		return circuit_;
	}

	// set conductor
	void CoilSurface::set_coolant(ShCoolantPr coolant){
		assert(coolant!=NULL);
		coolant_ = coolant;
	}

	// get conductor
	ShCoolantPr CoilSurface::get_coolant() const{
		assert(coolant_!=NULL);
		return coolant_;
	}

	// set conductor
	void CoilSurface::set_conductor(ShConductorPr con){
		assert(con!=NULL);
		con_ = con;
	}

	// get conductor
	ShConductorPr CoilSurface::get_conductor() const{
		assert(con_!=NULL);
		return con_;
	}

	// set generators
	void CoilSurface::set_generators(ShFramePr gen){
		assert(gen!=NULL);
		gen_ = gen;
	}

	// get area
	ShAreaPr CoilSurface::get_area() const{
		assert(area_!=NULL);
		return area_;
	}

	// set area
	void CoilSurface::set_area(ShAreaPr area){
		assert(area!=NULL);
		area_ = area;
	}

	// getting of coil number of turns
	double CoilSurface::get_number_turns() const{
		return number_turns_;
	}

	// set number of turns
	void CoilSurface::set_number_turns(const double number_turns){
		number_turns_ = number_turns;
	}

	// access generators
	ShFramePr CoilSurface::get_generators() const{
		return gen_;
	}

	// get coordinates for section
	arma::Mat<double> CoilSurface::get_coords() const{
		return R_;
	}

	// get coordinates for section
	arma::Mat<double> CoilSurface::get_direction() const{
		return L_;
	}

	// get coordinates for section
	arma::Mat<double> CoilSurface::get_normal() const{
		return N_;
	}

	// get coordinates for section
	arma::Mat<double> CoilSurface::get_transverse() const{
		return D_;
	}

	// get coordinates for section
	arma::Mat<arma::uword> CoilSurface::get_elements() const{
		return s_;
	}

	// get number of nodes
	arma::uword CoilSurface::get_num_nodes() const{
		return R_.n_cols;
	}

	// get number of elements
	arma::uword CoilSurface::get_num_elements() const{
		return s_.n_cols;
	}

	// gmsh export
	void CoilSurface::export_gmsh(cmn::ShGmshFilePr gmsh, const bool incl_vectors){
		// write nodes and elements
		gmsh->write_nodes(R_);
		gmsh->write_elements(s_,arma::Row<arma::uword>(s_.n_cols,arma::fill::ones));
			
		// write vectors
		if(incl_vectors){
			gmsh->write_nodedata(L_,"dir. vector [m]");
			gmsh->write_nodedata(N_,"norm. vector [m]");
			gmsh->write_nodedata(D_,"trans. vector [m]");
		}
	}

}}
