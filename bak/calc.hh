// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALC_HH
#define MDL_CALC_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "common/node.hh"
#include "mlfmm/settings.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Path> ShCalcPr;
	typedef arma::field<ShCalcPr> ShCalcPrList;

	// template class for path
	class Calc: public fmm::MgnTargets, virtual public cmn::Node{
		// properties
		protected:
			// optional settings for the fmm
			fmm::ShSettingsPr stngs_= NULL;

		// methods
		public:
			// virtual destructor (obligatory)
			virtual ~Calc(){};
			
			// setting the settings
			set_settings(fmm::ShSettingsPr stngs){stngs_ = stngs;}

			// must have setup function
			virtual void setup(cmn::ShLogPr lg = NullLog::create()) = 0;
	};

}}

#endif
