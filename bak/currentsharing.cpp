// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>

// header files for FX-Models
#include "material.hh"
#include "matgroup.hh"
#include "matnbti.hh"
#include "matcopper.hh"

// main
int main(){
	// settings
	double fnbti = 0.4;
	double fcu = 0.6;

	// create conductor group
	rat::mdl::ShMatGroupPr grp = rat::mdl::MatGroup::create();
	{
		// add nbti superconductor
		rat::mdl::ShMatNbTiPr nbti = rat::mdl::MatNbTi::create();
		nbti->set_nbti_LHC();
		grp->add_material(fnbti,nbti);

		// add copper matrix
		rat::mdl::ShMatCopperPr copper = rat::mdl::MatCopper::create();
		copper->set_copper_OFHC_RRR100();
		grp->add_material(fcu,copper);
	}
	rat::mdl::ShMaterialPr mat = grp;

	// current sharing time
	arma::Row<double> Bm = arma::Row<double>(20,arma::fill::ones)*8;
	arma::Row<double> T = arma::Row<double>(20,arma::fill::ones)*1.9;
	arma::Row<double> alpha = arma::Row<double>(20,arma::fill::zeros);

	// chose critical current density of NbTi
	arma::Row<double> Jop = arma::linspace<arma::Row<double> >(0,2e9,20);

	std::cout<<Jop<<std::endl;

	// calculate electric field
	arma::Row<double> E = mat->calc_electric_field(Jop,Bm,T,alpha);
	
	std::cout<<Jop<<std::endl;

	// output
	std::cout<<E<<std::endl;
}	