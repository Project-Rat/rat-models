// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MATERIAL_STAINLESS316_HH
#define MDL_MATERIAL_STAINLESS316_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "material.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class MatStainless316> ShMatStainless316Pr;

	// material properties based on NIST
	class MatStainless316: public Material{
		// parameters
		private:
			// density
			double density_ = 8027; // [kg/m3]

		// methods
		public:
			// conductor
			MatStainless316();

			// factory
			static ShMatStainless316Pr create();

			// calculate electrical resistivity
			arma::Mat<double> calc_resistivity(const arma::Row<double> &Bm, const arma::Row<double> &T, const bool combined = true) const;

			// calculate electric field
			arma::Row<double> calc_current_density(const arma::Row<double> &E, const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha, const arma::Mat<double> &Jc, const arma::Mat<double> &rho) const;
			arma::Row<double> calc_electric_field(const arma::Row<double> &J, const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha, const arma::Mat<double> &Jc, const arma::Mat<double> &rho) const;

			// copy
			ShMaterialPr copy() const;

			// thermal properties
			virtual arma::Row<double> calc_thermal_conductivity(const arma::Row<double> &Bm, const arma::Row<double> &T) const;
			virtual arma::Row<double> calc_specific_heat(const arma::Row<double> &T)const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, std::list<cmn::ShNodePr> &list) const;
			virtual void deserialize(const Json::Value &js, std::list<cmn::ShNodePr> &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif