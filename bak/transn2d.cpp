// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "transn2d.hh"

// rat-common headers
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructors
	TransN2D::TransN2D(){
		set_name("swap n2d");
	}

	// factory methods
	ShTransN2DPr TransN2D::create(){
		return std::make_shared<TransN2D>();
	}

	// apply reversion
	void TransN2D::apply_vectors(
		const arma::Mat<fltp> &/*R*/,
		arma::Mat<fltp> &/*L*/, 
		arma::Mat<fltp> &N, 
		arma::Mat<fltp> &D, 
		const fltp /*time*/) const{
		assert(N.n_cols==D.n_cols);
		arma::swap(N,D);
	}

	// get type
	std::string TransN2D::get_type(){
		return "rat::mdl::transn2d";
	}

	// method for serialization into json
	void TransN2D::serialize(Json::Value &js, cmn::SList &list) const{
		Trans::serialize(js,list);
		js["type"] = get_type();
	}

	// method for deserialisation from json
	void TransN2D::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		Trans::deserialize(js,list,factory_list,pth);
	}
}}