// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALCULATE_HARMONICS_HH
#define MDL_CALCULATE_HARMONICS_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/defines.hh"
#include "rat/common/log.hh"

#include "rat/mlfmm/mgntargets.hh"
#include "rat/mlfmm/currentsources.hh"
#include "rat/mlfmm/mgntargets.hh"

#include "path.hh"
#include "pathgroup.hh"
#include "pathstraight.hh"
#include "model.hh"
#include "vtkunstr.hh"
#include "vtktable.hh"
#include "calcfieldmap.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcHarmonics> ShCalcHarmonicsPr;

	// calculates 2D pseudo harmonics along a path
	// this path uses the same object as used for setting
	// up the coils and can thus be curved.
	class CalcHarmonics: public CalcFieldMap{		
		// properties
		protected:
			// if set to true the harmonics are 
			// compensated for the element length
			// this is useful for curved magnets
			bool compensate_curvature_ = false;

			// settings
			double radius_ = 0;
			arma::uword num_theta_ = 64;
			arma::uword num_max_ = 10;
			
			// path along which the harmonics are calculated
			// ShModelPr model_ = NULL;
			ShPathPr base_ = NULL;

			// angle
			arma::Row<double> theta_;

			// base
			arma::Mat<double> Rb_;
			arma::Mat<double> Nb_;
			arma::Mat<double> Db_;
			arma::Mat<double> Lb_;

			// result matrices
			arma::uword num_ell_;
			arma::Row<double> ell_;
			arma::Mat<double> An_;
			arma::Mat<double> Bn_;
		
			// element length
			arma::Row<double> ellt_;

		// methods
		public:
			// constructor
			CalcHarmonics();
			CalcHarmonics(ShModelPr source_model, ShPathPr base);
			
			// factory methods
			static ShCalcHarmonicsPr create();
			static ShCalcHarmonicsPr create(ShModelPr source_model, ShPathPr base);

			// setting
			void set_radius(const double radius);
			void set_num_theta(const arma::uword num_theta);

			// setting of the path
			void set_base(ShPathPr base);

			// set integral contribution
			void set_compensate_curvature(const bool compensate_curvature);

			// calculation function
			void setup(cmn::ShLogPr lg = cmn::NullLog::create()) override;

			// set the time
			void set_time(const double time) override;

			// post processing
			void post_process();

			// getting calculated harmonic tables
			void get_harmonics(arma::Row<double> &ell, arma::Mat<double> &An, arma::Mat<double> &Bn) const;

			// getting integrated harmonics
			void get_harmonics(arma::Row<double> &An, arma::Row<double> &Bn) const;

			// VTK exporting
			ShVTKUnstrPr export_vtk_coord(cmn::ShLogPr lg = cmn::NullLog::create()) const;
			ShVTKTablePr export_vtk_table(cmn::ShLogPr lg = cmn::NullLog::create()) const;

			// write output files
			void write(cmn::ShLogPr lg = cmn::NullLog::create()) override;

			// display function
			void display(cmn::ShLogPr lg, const arma::uword nmax = 10) const;


			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
