// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MATERIAL_LK_HH
#define MDL_MATERIAL_LK_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "material.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class MatNbTi> ShMatNbTiPr;

	// Lubell/Kramer scaling relation
	class MatNbTi: public Material{
		private:
			// density
			double density_ = 8570;

			// electric field criterion
			double E0_ = 1e-5; // [V/m]
			double N_ = 50; // N-value

			// fitting parameters
			double C0_ = 31.4; // [T]
			double alpha_ = 0.63;
			double beta_ = 1.0;
			double gamma_ = 2.3;
			double n_ = 1.7;

			// materialistic parameters
			double Bc20_ = 14.5; // [T] upper critical magnetic flux at 0K
			double Tc0_ = 9.2; // [K] critical temperature at zero magnetic flux
			double Jref_ = 3000; // [A/mm^2]critical current density at 4.2K and 5T

			// interpolation arrays
			arma::Col<double> Ti_;
			arma::Col<double> ki_;
			arma::Col<double> Cpi_; 

		// methods
		public:
			// conductor
			MatNbTi();

			// factory
			static ShMatNbTiPr create();

			// default conductor settings
			void set_nbti_LHC();
			
			// calculate critical current
			arma::Mat<double> calc_critical_current_density(const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha, const bool combined = true) const;

			// calculate electric field
			arma::Row<double> calc_current_density(const arma::Row<double> &E, const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha, const arma::Mat<double> &Jc, const arma::Mat<double> &rho) const;
			arma::Row<double> calc_electric_field(const arma::Row<double> &J, const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha, const arma::Mat<double> &Jc, const arma::Mat<double> &rho) const;

			// thermal properties
			// arma::Row<double> calc_resistivity(const arma::Row<double> &Bm, const arma::Row<double> &T) const;
			arma::Row<double> calc_specific_heat(const arma::Row<double> &T)const;
			arma::Row<double> calc_thermal_conductivity(const arma::Row<double> &Bm, const arma::Row<double> &T)const;

			// copy
			ShMaterialPr copy() const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, std::list<cmn::ShNodePr> &list) const;
			virtual void deserialize(const Json::Value &js, std::list<cmn::ShNodePr> &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif