# CUDA GPU Kernels
# CUDA is not compatible with all versions of FindGCC
# Find more information here:
# https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html#axzz3x7mwvZrG
# https://stackoverflow.com/questions/6622454/cuda-incompatible-with-my-gcc-version
# it was found that you can change the gcc version cuda uses using soft links:
# sudo ln -s /usr/bin/gcc-7 /usr/local/cuda/bin/gcc 
# sudo ln -s /usr/bin/g++-7 /usr/local/cuda/bin/g++
# the compilation.
if(ENABLE_CUDA)
	# check if cuda available
	include(CheckLanguage)
	check_language(CUDA)

	# check if any cuda compiler found
	if(CMAKE_CUDA_COMPILER)
		# enable cuda language
		enable_language(CUDA)

		# check if nvidia
		if(CMAKE_CUDA_COMPILER_ID STREQUAL "NVIDIA")
			# inform user
			message(STATUS "CUDA found, building GPU kernels.")

			# set the standard
			if(NOT DEFINED CMAKE_CUDA_STANDARD)
				set(CMAKE_CUDA_STANDARD 11)
				set(CMAKE_CUDA_STANDARD_REQUIRED ON)
			endif()
		else()
			message(STATUS "CUDA found but not NVIDIA.")
		endif()
	else()
		message(STATUS "CUDA not found.")
	endif()
else()
	message(STATUS "CUDA disabled.")
endif()