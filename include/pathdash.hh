// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_DASH_HH
#define MDL_PATH_DASH_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "path.hh"
#include "frame.hh"
#include "transformations.hh"
#include "inputpath.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathDash> ShPathDashPr;

	// cross section of coil
	class PathDash: public Path, public Transformations, public InputPath{
		// data enums
		public: 	
			enum class Align{LEFT,CENTER,RIGHT};
		
		// properties
		protected:
			// combine frame
			bool combine_frame_ = true;

			// center dashes around their mid-points
			// otherwise dashes will be left aligned
			Align alignment_ = Align::CENTER;

			// length of each dash
			fltp ell_segment_ = RAT_CONST(50e-3);

			// offset
			fltp ell_offset_ = RAT_CONST(0.0);

			// pitch
			// when set to zero pitch is determines from 
			// the length of the base path and the number of segments
			fltp omega_ = RAT_CONST(0.0); 

			// length of each dash
			arma::uword num_segment_ = 5llu;

		// methods
		public:
			// constructor
			PathDash();

			// factory
			static ShPathDashPr create();

			// setters
			void set_num_segment(const arma::uword num_segment);
			void set_ell_segment(const fltp ell_segment);
			void set_combine_frame(const bool combine_frame = true);
			void set_ell_offset(const fltp ell_offset);
			void set_omega(const fltp omega);
			void set_alignment(const Align align);

			// getters
			arma::uword get_num_segment()const;
			fltp get_ell_segment()const;
			bool get_combine_frame()const;
			fltp get_ell_offset()const;
			fltp get_omega()const;
			Align get_alignment()const;

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// tree structure
			void reindex() override;

			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
