// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// Class Description:
// connects two paths with a connector
// this then becomes a new combined path
// assumed is that the top profile resides 
// in the xy plane, while the side profile 
// is located in the yz plane. The axis of 
// the magnet is then oriented along the 
// y-axis.

#ifndef MDL_PATH_PROFILE_HH
#define MDL_PATH_PROFILE_HH

#include <armadillo> 
#include <cassert>
#include <memory>

#include "path.hh"
#include "frame.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathProfile> ShPathProfilePr;

	// profile class
	class PathProfile: public Path, public Transformations{
		// properties
		private:
			// top profile in xy plane
			ShPathPr top_ = NULL;

			// side profile in yz plane
			ShPathPr side_ = NULL;

			// introduce a twist along the path
			// this is used in Feather-M2
			fltp alpha_ = 0; // shear angle [rad]
			fltp pshear_ = 1; // shear parameter
			fltp ellshear_ = 1; // shear length [m]

		// methods
		public:
			// constructor
			PathProfile();
			PathProfile(const ShPathPr& top, const ShPathPr& side);

			// factory methods
			static ShPathProfilePr create();
			static ShPathProfilePr create(const ShPathPr& top, const ShPathPr& side);

			// set properties
			void set_top(const ShPathPr& top);
			void set_side(const ShPathPr& side);
			void set_shearing(const fltp alpha, const fltp ptwist, const fltp ellshear);

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// validity check
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
