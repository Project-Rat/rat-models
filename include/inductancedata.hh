// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_INDUCTANCE_DATA_HH
#define MDL_INDUCTANCE_DATA_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/log.hh"

#include "data.hh"
#include "meshdata.hh"
#include "vtktable.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class InductanceData> ShInductanceDataPr;

	// template for coil
	class InductanceData: public Data{		
		// properties
		protected:
			// time
			fltp time_;

			// circuit index
			arma::Col<arma::uword> circuit_index_;

			// inductance matrix
			arma::Mat<fltp> M_;

			// stored energy
			std::list<std::pair<std::string, fltp> > energy_table_; 

			// calculation time
			fltp calculation_time_ = RAT_CONST(0.0);

			// coil names
			arma::field<std::string> coil_names_;

		// methods
		public:
			// default constructor
			InductanceData();
			InductanceData(
				const fltp time, 
				const arma::Mat<fltp> &M, 
				const arma::Col<arma::uword>& circuit_index,
				const std::list<std::pair<std::string, fltp> > &energy_table, 
				const arma::field<std::string> &coil_names, 
				const fltp calculation_time);
			
			// factory
			static ShInductanceDataPr create();
			static ShInductanceDataPr create(
				const fltp time, 
				const arma::Mat<fltp> &M, 
				const arma::Col<arma::uword>& circuit_index,
				const std::list<std::pair<std::string, fltp> > &energy_table,
				const arma::field<std::string> &coil_names, 
				const fltp calculation_time);

			// get names
			const arma::field<std::string>& get_coil_names() const;

			// get circuit index
			const arma::Col<arma::uword>& get_circuit_indices() const;

			// display matrix and stored energy
			void display(cmn::ShLogPr lg = cmn::NullLog::create());

			// export to vtk
			ShVTKObjPr export_vtk() const override;

			// get inductance matrix
			const arma::Mat<fltp>& get_matrix() const;

			// get energy table
			const std::list<std::pair<std::string, fltp> >& get_energy_table() const;

			// get inductance and stored energy
			fltp get_inductance() const;
			fltp get_stored_energy() const;

	};
}}

#endif
