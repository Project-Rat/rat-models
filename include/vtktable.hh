// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef MDL_VTK_TABLE_HH
#define MDL_VTK_TABLE_HH

// general headers
#include <armadillo> 
#include <memory>
#include <iomanip>

// magrat headers
#include "rat/common/log.hh"

#include "vtkobj.hh"

// VTK headers
#include <vtkSmartPointer.h>
#include <vtkTable.h>
#include <vtkXMLTableWriter.h>
#include <vtkDoubleArray.h>
#include <vtkUnstructuredGrid.h>
#include <vtkXMLUnstructuredGridWriter.h> 
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkCellArray.h>
#include <vtkStringArray.h>
#include <vtkDataArray.h>

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class VTKTable> ShVTKTablePr;

	// VTK Unstructured grid
	class VTKTable: public VTKObj{
		protected:
			// data storage
			arma::uword num_rows_ = 0;
			std::map<std::string, arma::Col<fltp> > float_data_;
			std::map<std::string, arma::field<std::string> > string_data_;

		public:
			// constructor
			VTKTable(const arma::uword num_rows);

			// factory
			static ShVTKTablePr create(const arma::uword num_rows);

			// write mesh
			void set_num_rows(const arma::uword num_rows);

			// set data directly
			void set_data(const arma::field<std::string> &v, const std::string &name);
			void set_data(const arma::Col<fltp> &v, const std::string &name);

			// get filename extension
			std::string get_filename_ext() const override;

			// write output file
			void write(const boost::filesystem::path fname, const cmn::ShLogPr& lg = cmn::NullLog::create()) override;
	};

}}

#endif
