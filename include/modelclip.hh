// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_CLIP_HH
#define MDL_MODEL_CLIP_HH

#include <armadillo> 
#include <memory>

#include "modelgroup.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelClip> ShModelClipPr;

	// applies toroid symmetry to the input coil
	class ModelClip: public ModelGroup{
		// set coils
		protected:
			// invert
			bool invert_ = false;

			// direction of plane
			arma::Col<fltp>::fixed<3> vector_{RAT_CONST(1.0),RAT_CONST(0.0),RAT_CONST(0.0)};

			// offset from origin
			fltp offset_ = RAT_CONST(0.0);

			// velocity
			fltp velocity_ = RAT_CONST(0.0);

		// methods
		public:
			// constructor
			ModelClip();
			explicit ModelClip(const ShModelPr &base_model, const arma::Col<fltp>::fixed<3> &vector = {RAT_CONST(1.0),RAT_CONST(0.0),RAT_CONST(0.0)}, const fltp offset = RAT_CONST(0.0));
			explicit ModelClip(const std::list<ShModelPr> &models, const arma::Col<fltp>::fixed<3> &vector = {RAT_CONST(1.0),RAT_CONST(0.0),RAT_CONST(0.0)}, const fltp offset = RAT_CONST(0.0));

			// factory method
			static ShModelClipPr create();
			static ShModelClipPr create(const ShModelPr &base_model, const arma::Col<fltp>::fixed<3> &vector = {RAT_CONST(1.0),RAT_CONST(0.0),RAT_CONST(0.0)}, const fltp offset = RAT_CONST(0.0));
			static ShModelClipPr create(const std::list<ShModelPr> &models, const arma::Col<fltp>::fixed<3> &vector = {RAT_CONST(1.0),RAT_CONST(0.0),RAT_CONST(0.0)}, const fltp offset = RAT_CONST(0.0));

			// setters
			void set_vector(const arma::Col<fltp>::fixed<3>& vector);
			void set_offset(const fltp offset);
			void set_velocity(const fltp velocity);
			void set_invert(const bool invert);

			// getters
			const arma::Col<fltp>::fixed<3>& get_vector()const;
			fltp get_offset()const;
			fltp get_velocity()const;
			bool get_invert()const;

			// create calculation data objects
			std::list<ShMeshDataPr> create_meshes_core(
				const std::list<arma::uword> &trace, 
				const MeshSettings &stngs = MeshSettings()) const override; // create volume current elements

			// validity check
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
