// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALC_LEAF_HH
#define MDL_CALC_LEAF_HH

#include <armadillo> 
#include <memory>

#include "rat/mlfmm/settings.hh"

#include "model.hh"
#include "calc.hh"
#include "inputcircuits.hh"
#include "background.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcLeaf> ShCalcLeafPr;

	// a collection of coils
	class CalcLeaf: public Calc, public InputCircuits{
		// set coils
		protected:
			// settings for MLFMM
			fmm::ShSettingsPr stngs_;

			// input model
			ShModelPr model_;

			// background field
			ShBackgroundPr bg_;

		// methods
		public:
			// consrtuctor
			CalcLeaf();

			// setting
			void set_settings(const fmm::ShSettingsPr &stngs);
			void set_background(const ShBackgroundPr &bg);

			// getting
			const fmm::ShSettingsPr& get_settings() const;
			const ShBackgroundPr& get_background() const;

			// add model function
			void set_model(const ShModelPr &model)override;

			// get stored model
			const ShModelPr& get_model();

			// calculate field from model onto target points
			void calculate_field(
				const std::list<ShMeshDataPr>& meshes, 
				const fmm::ShTargetsPr& tar, 
				const fltp time, 
				const cmn::ShLogPr &lg,
				const ShSolverCachePr& cache)const;

			// validity check
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif







