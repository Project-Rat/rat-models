// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_DRIVE_TRAPZ_HH
#define MDL_DRIVE_TRAPZ_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "drive.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class DriveTrapz> ShDriveTrapzPr;
	typedef arma::field<ShDriveTrapzPr> ShDriveTrapzPrList;

	// circuit class
	class DriveTrapz: public Drive{
		// properties
		protected:
			// tstart is center of pulse
			bool centered_ = false;

			// vertical offset
			fltp offset_ = RAT_CONST(0.0);

			// pulse amplitude
			fltp amplitude_ = RAT_CONST(1.0);

			// start time
			fltp tstart_ = RAT_CONST(1.0);

			// pulse ramprate
			fltp ramprate_ = RAT_CONST(0.1);

			// pulse duration
			fltp tpulse_ = RAT_CONST(2.0);

		// methods
		public:
			// constructor
			DriveTrapz();
			DriveTrapz(
				const fltp amplitude, 
				const fltp tstart, 
				const fltp ramprate, 
				const fltp tpulse, 
				const fltp offset = RAT_CONST(0.0), 
				const bool centered = false);

			// factory
			static ShDriveTrapzPr create();
			static ShDriveTrapzPr create(
				const fltp amplitude, 
				const fltp tstart, 
				const fltp ramprate, 
				const fltp tpulse, 
				const fltp offset = RAT_CONST(0.0), 
				const bool centered = false);

			// set properties
			void set_centered(const bool centered = true);
			void set_amplitude(const fltp amplitude);
			void set_tstart(const fltp tstart);
			void set_ramprate(const fltp ramprate);
			void set_tpulse(const fltp tpulse);
			void set_offset(const fltp offset);

			// get properties
			bool get_centered()const;
			fltp get_amplitude()const;
			fltp get_tstart()const;
			fltp get_ramprate()const;
			fltp get_tpulse()const;
			fltp get_offset()const;

			// getters
			virtual fltp get_v1() const override;
			virtual fltp get_v2() const override;

			// some helper functions for the function shape
			fltp get_ramp_time()const;
			fltp get_center_position()const;
			fltp get_edge_position()const;

			// get scaling at given time
			fltp get_scaling(
				const fltp position,
				const fltp time = 0.0,
				const arma::uword derivative = 0) const override;
			
			// get inflection points
			arma::Col<fltp> get_inflection_points() const override;

			// apply scaling for the input settings
			virtual void rescale(const fltp scale_factor) override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
