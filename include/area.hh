// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_AREA_HH
#define MDL_AREA_HH

#include <armadillo> 
#include <memory>
#include <cassert>
#include <list>

#include "trans.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Area> ShAreaPr;
	typedef arma::field<ShAreaPr> ShAreaPrList;

	// area carries the mesh of the 
	// cross-sectional area of the coil
	class Area{
		// properties
		protected:
			// hidden dimensions
			fltp hidden_thickness_ = RAT_CONST(1.0);
			fltp hidden_width_ = RAT_CONST(1.0);

			// nodes [2xN] matrix
			arma::Mat<fltp> Rn_;

			// center point
			arma::Col<fltp>::fixed<2> Rc_;

			// orientation vectors
			arma::Mat<fltp> N_;
			arma::Mat<fltp> D_;

			// elements
			arma::Mat<arma::uword> n_; // volume
			arma::Mat<arma::uword> p_; // perimeter (surface)
			arma::Mat<arma::uword> i_; // internal edges
			arma::Col<arma::uword> q_; // corner nodes

		// private methods
		public:
			// default constructor
			Area();

			// constructor with input
			Area(
				const arma::Mat<fltp> &Rn, 
				const arma::Mat<fltp> &N, 
				const arma::Mat<fltp> &D, 
				const arma::Mat<arma::uword> &n,
				const fltp hidden_thickness = 1.0, 
				const fltp hidden_width = 1.0);

			// construct by combining other areas
			Area(const std::list<ShAreaPr>& areas, 
				const bool merge_nodes = true);

			// factory with input
			static ShAreaPr create(
				const arma::Mat<fltp> &Rn, 
				const arma::Mat<fltp> &N, 
				const arma::Mat<fltp> &D, 
				const arma::Mat<arma::uword> &n, 
				const fltp hidden_thickness = 1.0, 
				const fltp hidden_width = 1.0);

			// copy factory
			static ShAreaPr create(
				const std::list<ShAreaPr>& areas, 
				const bool merge_nodes = true);

			// set mesh
			void set_mesh(
				const arma::Mat<fltp> &Rn, 
				const arma::Mat<fltp> &N, 
				const arma::Mat<fltp> &D, 
				const arma::Mat<arma::uword> &n);
			void set_center_coord(const arma::Col<fltp>::fixed<2>& Rc);

			// set perimeter mesh elements
			void set_perimeter(const arma::Mat<arma::uword>& p);
			void set_internal(const arma::Mat<arma::uword>& i);
			void set_corner_nodes(const arma::Col<arma::uword>& q);

			// calculate perimeter from mesh
			void setup_perimeter(); 
			void setup_corner_nodes();

			// set hidden dimensions
			void set_hidden_thickness(const fltp hidden_thickness);
			void set_hidden_width(const fltp hidden_width);

			// get hidden dimensions
			fltp get_hidden_thickness() const;
			fltp get_hidden_width() const;
			fltp get_hidden_dim() const;
				
			// edge matrix construction
			void create_edge_matrix(
				arma::Mat<arma::uword> &s, 
				arma::Mat<arma::uword> &in) const;

			// claculate corner nodes
			arma::Col<arma::uword> calculate_corner_nodes() const;

			// get counters
			arma::uword get_num_elements() const;
			arma::uword get_num_nodes() const;
			arma::uword get_num_perimeter() const;
			arma::uword get_num_corner() const;

			// access nodes and elements
			const arma::Mat<fltp>& get_nodes() const;
			const arma::Mat<arma::uword>& get_elements() const;
			const arma::Mat<arma::uword>& get_perimeter()const;
			const arma::Mat<arma::uword>& get_internal()const;
			const arma::Col<arma::uword>& get_corner_nodes()const;
			const arma::Mat<fltp>& get_transverse() const;
			const arma::Mat<fltp>& get_normal() const;
			const arma::Col<fltp>::fixed<2>& get_center_coord()const;

			// apply smoothing to mesh
			void smoothen(const arma::uword N, const fltp dmp);

			// apply a transformation to these frame
			void apply_transformations(
				const std::list<ShTransPr> &trans, 
				const fltp time);
			void apply_transformation(
				const ShTransPr &trans, 
				const fltp time);

			// get conductor area
			fltp get_area()const;
			arma::Row<fltp> get_areas()const;
			fltp get_perimeter_length()const;
			arma::Row<fltp> get_perimeter_lengths()const;

			// element size
			arma::Row<fltp> get_element_size() const;

			// extract peripheral mesh
			// ShPerimeterPr extract_perimeter() const;
	};

}}

#endif
