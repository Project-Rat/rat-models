// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_OFFSET_DRIVE_HH
#define MDL_PATH_OFFSET_DRIVE_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "path.hh"
#include "frame.hh"
#include "transformations.hh"
#include "inputpath.hh"

#include "drive.hh"
#include "drivedc.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathOffsetDrive> ShPathOffsetDrivePr;

	// cross section of coil
	class PathOffsetDrive: public Path, public Transformations, public InputPath{
		// properties
		protected:
			// normalize length
			bool normalize_length_ = false;
			bool reorient_ = false;

			// offset in the main directions
			ShDrivePr noff_ = DriveDC::create(RAT_CONST(0.0)); // offset in normal direction [m]
			ShDrivePr doff_ = DriveDC::create(RAT_CONST(0.0)); // offset in transverse direction [m]
			ShDrivePr boff_ = DriveDC::create(RAT_CONST(0.0));

			// scaling in the main directions
			ShDrivePr nscale_ = DriveDC::create(RAT_CONST(1.0)); // offset in normal direction [m]
			ShDrivePr dscale_ = DriveDC::create(RAT_CONST(1.0)); // offset in transverse direction [m]

			// rotation around longitudinal vector
			ShDrivePr rotate_ = DriveDC::create(RAT_CONST(0.0));

		// methods
		public:
			// constructor
			PathOffsetDrive();
			explicit PathOffsetDrive(
				const ShPathPr &base, 
				const ShDrivePr& noff = DriveDC::create(0), 
				const ShDrivePr& doff = DriveDC::create(0), 
				const ShDrivePr& boff = DriveDC::create(0));

			// factory
			static ShPathOffsetDrivePr create();
			static ShPathOffsetDrivePr create(
				const ShPathPr& base, 
				const ShDrivePr& noff = DriveDC::create(0), 
				const ShDrivePr& doff = DriveDC::create(0), 
				const ShDrivePr& boff = DriveDC::create(0));

			// set properties
			void set_noff(const ShDrivePr& noff);
			void set_doff(const ShDrivePr& doff);
			void set_boff(const ShDrivePr& doff);
			void set_rotate(const ShDrivePr& rotate);
			void set_nscale(const ShDrivePr& nscale);
			void set_dscale(const ShDrivePr& tscale);
			void set_normalize_length(const bool normalize_length = true);
			void set_reorient(const bool reorient = true);

			// get properties
			const ShDrivePr& get_noff()const;
			const ShDrivePr& get_doff()const;
			const ShDrivePr& get_boff()const;
			const ShDrivePr& get_rotate()const;
			const ShDrivePr& get_nscale()const;
			const ShDrivePr& get_dscale()const;
			bool get_normalize_length()const;
			bool get_reorient()const;
			
			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// tree structure
			void reindex() override;

			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
