// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_BEZIER2_HH
#define MDL_PATH_BEZIER2_HH

#include <armadillo>
#include <memory>

// rat-models headers
#include "path.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathBezier> ShPathBezierPr;

	// bezier spline implementation
	class PathBezier: public Path, public Transformations{
		// properties
		protected:
			// calculate the frame analytically or numerically
			bool analytic_frame_ = true;
			//bool spline_order_ = 5;
				
			// number of points used for calculating length
			arma::uword num_reg_ = 20;

			// start point
			arma::Col<fltp>::fixed<3> pstart_;
			arma::Col<fltp>::fixed<3> nstart_;
			arma::Col<fltp>::fixed<3> lstart_;
			arma::Col<fltp>::fixed<3> dstart_;
			
			// end point 
			arma::Col<fltp>::fixed<3> pend_;
			arma::Col<fltp>::fixed<3> nend_;
			arma::Col<fltp>::fixed<3> lend_;
			arma::Col<fltp>::fixed<3> dend_;

			// position of control points
			arma::Mat<fltp> Puv1_; // control point positions with respect to start point in uv plane
			arma::Mat<fltp> Puv2_; // control point positions with respect to end point in uv plane

			// transition length
			fltp ell_trans1_;
			fltp ell_trans2_;

			// element size
			fltp element_size_;

			// use planar windings (winding plane determined by start point)
			bool planar_winding_ = false;

			// path offset
			fltp path_offset_;

			// number of sections
			arma::uword num_sections_ = 1;

			// use binormal vector instead of darboux vector
			bool use_binormal_ = false;

		// methods
		public:
			// constructor
			PathBezier();
			PathBezier(
				const arma::Col<fltp>::fixed<3>& pstart, 
				const arma::Col<fltp>::fixed<3>& lstart, 
				const arma::Col<fltp>::fixed<3>& nstart,
				const arma::Col<fltp>::fixed<3>& dstart,
				const arma::Col<fltp>::fixed<3>& pend, 
				const arma::Col<fltp>::fixed<3>& lend, 
				const arma::Col<fltp>::fixed<3>& nend, 
				const arma::Col<fltp>::fixed<3>& dend, 
				const arma::Mat<fltp> &Puv1,
				const arma::Mat<fltp> &Puv2,
				const fltp ell_trans1, 
				const fltp ell_trans2, 
				const fltp element_size, 
				const fltp path_offset = 0,
				const bool use_binormal = false);
			
			// factory
			static ShPathBezierPr create();
			static ShPathBezierPr create(
				const arma::Col<fltp>::fixed<3>& pstart, 
				const arma::Col<fltp>::fixed<3>& lstart, 
				const arma::Col<fltp>::fixed<3>& nstart,
				const arma::Col<fltp>::fixed<3>& pend, 
				const arma::Col<fltp>::fixed<3>& lend, 
				const arma::Col<fltp>::fixed<3>& nend, 
				const arma::Mat<fltp> &Puv1,
				const arma::Mat<fltp> &Puv2,
				const fltp ell_trans1, 
				const fltp ell_trans2, 
				const fltp element_size, 
				const fltp path_offset = 0,
				const bool use_binormal = false);
			static ShPathBezierPr create(
				const arma::Col<fltp>::fixed<3>& pstart, 
				const arma::Col<fltp>::fixed<3>& lstart, 
				const arma::Col<fltp>::fixed<3>& nstart,
				const arma::Col<fltp>::fixed<3>& dstart,
				const arma::Col<fltp>::fixed<3>& pend, 
				const arma::Col<fltp>::fixed<3>& lend, 
				const arma::Col<fltp>::fixed<3>& nend, 
				const arma::Col<fltp>::fixed<3>& dend, 
				const arma::Mat<fltp> &Puv1,
				const arma::Mat<fltp> &Puv2,
				const fltp ell_trans1, 
				const fltp ell_trans2, 
				const fltp element_size, 
				const fltp path_offset = 0,
				const bool use_binormal = false);

			// setters
			void set_planar_winding(const bool planar_winding = true);
			void set_analytic_frame(const bool analytic_frame = true);
			void set_num_sections(const arma::uword num_sections);
			void set_ell_trans1(const fltp ell_trans1);
			void set_ell_trans2(const fltp ell_trans2);
			
			// getters
			fltp get_ell_trans1()const;
			fltp get_ell_trans2()const;
			arma::uword get_num_reg()const;
			fltp get_path_offset()const;
			fltp get_element_size()const;

			// create control points by combining Puv1 and Puv2
			arma::Mat<fltp> create_control_points()const;

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// create regular time array
			static arma::Row<fltp> regular_times(
				const arma::Mat<fltp> &P, 
				const fltp element_size,
				const fltp path_offset = RAT_CONST(0.0), 
				const arma::uword num_reg = 20);
			
			// bezier function
			static void create_bezier(
				arma::Mat<fltp> &R,
				arma::Mat<fltp> &V,
				arma::Mat<fltp> &A,
				const arma::Row<fltp> &t, 
				const arma::Mat<fltp> &P);

			// recursive bezier function
			static arma::field<arma::Mat<fltp> > create_bezier_tn(
				const arma::Row<fltp> &t, 
				const arma::Mat<fltp> &P, 
				const arma::uword depth);

			// create control points from derivatives at start point
			// the first column is the coordinate of the start point itself
			// input D = [R,V,A,J ...]
			static arma::Mat<fltp> create_control_points(const arma::Mat<fltp>& D);

			// function for calculating derivatives
			static arma::field<arma::Mat<fltp> > diff(
				const arma::Mat<fltp>&R, 
				const arma::uword order, 
				const arma::uword depth);

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};


}}

#endif
