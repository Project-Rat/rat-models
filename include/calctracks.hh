// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALC_TRACKS_HH
#define MDL_CALC_TRACKS_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "calcleaf.hh"
#include "emitter.hh"
#include "model.hh"
#include "trackdata.hh"
#include "modelgroup.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcTracks> ShCalcTracksPr;

	// calculate field on line defined 
	// by start and end point coordinates
	class CalcTracks: public CalcLeaf{
		// tracktype
		public:
			enum class TrackingType{NEWTON, RELATIVISTIC, FLUX_LINES, ELECTRIC_FIELD_LINES};

		// properties
		protected:
			// visibility (of tracking mesh)
			bool visibility_ = true;

			// include time-dependence in the track generation
			// this means that each time step the field is recalculated 
			// from the circuit current in addition electric field is included
			bool enable_dynamic_;

			// track field lines instead of particle tracks
			TrackingType track_type_;

			// integration method
			cmn::RungeKutta::IntegrationMethod integration_method_ = cmn::RungeKutta::IntegrationMethod::RUNGE_KUTTA_FEHLBERG45;

			// tracking step size
			fltp max_step_size_; // [m]
			fltp rabstol_; // [m] per step
			fltp rreltol_; // this one is not so relevant maybe
			fltp pabstol_; // [GeV/C] for relativistic
			fltp preltol_; // relative energy tolerance

			// particle emitters
			std::map<arma::uword, ShEmitterPr> emitters_;

			// tracking model to define the shape of the tracking region
			ShModelPr tracking_model_;

		// methods
		public:
			// track calculations
			CalcTracks();
			CalcTracks(
				const ShModelPr &model);
			CalcTracks(
				const ShModelPr &model, 
				const ShModelPr &tracking_model, 
				const std::list<ShEmitterPr> &emitters);
			static ShCalcTracksPr create();
			static ShCalcTracksPr create(
				const ShModelPr &model);
			static ShCalcTracksPr create(
				const ShModelPr &model, 
				const ShModelPr &tracking_model, 
				const std::list<ShEmitterPr> &emitters);

			// setters
			void set_visibility(const bool visibility = true);
			void set_tracking_model(const ShModelPr &tracking_model);
			void set_integration_method(const cmn::RungeKutta::IntegrationMethod integration_method);
			void set_rabstol(const fltp rabstol);
			void set_pabstol(const fltp vabstol);
			void set_rreltol(const fltp rreltol);
			void set_preltol(const fltp vreltol);
			void set_enable_dynamic(const bool enable_dynamic = true);
			void set_track_type(const TrackingType track_type);
			void set_max_step_size(const fltp max_step_size);

			// getters
			bool get_visibility() const;
			const ShModelPr& get_tracking_model()const;
			cmn::RungeKutta::IntegrationMethod get_integration_method()const;
			cmn::RungeKutta::IntegrationMethod* access_integration_method();
			fltp get_max_step_size()const;
			fltp get_rabstol()const;
			fltp get_pabstol()const;
			fltp get_rreltol()const;
			fltp get_preltol()const;
			bool get_enable_dynamic()const;
			TrackingType get_track_type()const;

			// control emitters
			arma::uword add_emitter(const ShEmitterPr &emitter);
			arma::Row<arma::uword> add_emitters(const std::list<ShEmitterPr> &emitters);
			ShEmitterPr get_emitter(const arma::uword index) const;
			std::list<ShEmitterPr> get_emitters() const;
			bool delete_emitter(const arma::uword index);
			void reindex() override;
			arma::uword num_emitters() const;

			// creation of calculation data objects
			std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override; // create a mesh

			// calculate tracks
			ShTrackDataPr calculate_tracks(
				const fltp time = RAT_CONST(0.0), 
				const cmn::ShLogPr &lg = cmn::NullLog::create(),
				const ShSolverCachePr& cache = NULL)const;

			// calculation function
			virtual std::list<ShDataPr> calculate(
				const fltp time, 
				const cmn::ShLogPr &lg = cmn::NullLog::create(),
				const ShSolverCachePr& cache = NULL) override;
			
			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
