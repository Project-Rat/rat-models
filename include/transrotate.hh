// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_TRANS_ROTATE_HH
#define MDL_TRANS_ROTATE_HH

#include <armadillo> 
#include <memory>

#include "trans.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class TransRotate> ShTransRotatePr;

	// cross section of coil
	class TransRotate: public Trans{
		// properties
		private:
			// rotation vector
			arma::Col<fltp>::fixed<3> origin_{RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0)};
			arma::Col<fltp>::fixed<3> vector_{RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(1.0)};
			arma::Col<fltp>::fixed<3> offset_{RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0)}; // post translation

			// rotation angle and vector
			fltp alpha_ = RAT_CONST(0.0);
			fltp valpha_ = RAT_CONST(0.0);

		// methods
		public:
			// constructors
			TransRotate();
			TransRotate(const fltp ux, const fltp uy, const fltp uz, const fltp alpha);
			TransRotate(const arma::Col<fltp>::fixed<3> &vector, const fltp alpha);
			TransRotate(const arma::Col<fltp>::fixed<3> &origin, const arma::Col<fltp>::fixed<3> &vector, const fltp alpha);
			TransRotate(const arma::Col<fltp>::fixed<3> &origin, const arma::Col<fltp>::fixed<3> &vector, const fltp alpha, const arma::Col<fltp>::fixed<3> &offset);
			explicit TransRotate(const arma::Mat<fltp>::fixed<3,3> &M);

			// factory methods
			static ShTransRotatePr create();
			static ShTransRotatePr create(const fltp ux, const fltp uy, const fltp uz, const fltp alpha);
			static ShTransRotatePr create(const arma::Col<fltp>::fixed<3> &vector, const fltp alpha);
			static ShTransRotatePr create(const arma::Col<fltp>::fixed<3> &origin, const arma::Col<fltp>::fixed<3> &vector, const fltp alpha);
			static ShTransRotatePr create(const arma::Col<fltp>::fixed<3> &origin, const arma::Col<fltp>::fixed<3> &vector, const fltp alpha, const arma::Col<fltp>::fixed<3> &offset);
			static ShTransRotatePr create(const arma::Mat<fltp>::fixed<3,3> &M);

			// set rotation
			void set_rotation(const fltp phi, const fltp theta, const fltp psi);
			void set_rotation(const fltp ux, const fltp uy, const fltp uz, const fltp alpha);
			void set_rotation(const arma::Col<fltp>::fixed<3> &V, const fltp alpha);
			void set_origin(const arma::Col<fltp>::fixed<3> &origin);
			void set_vector(const arma::Col<fltp>::fixed<3> &vector);
			void set_offset(const arma::Col<fltp>::fixed<3> &offset);
			void set_alpha(const fltp alpha);
			void set_valpha(const fltp alpha);

			// get rotation origin, vector and angle
			arma::Col<fltp>::fixed<3> get_vector() const;
			arma::Col<fltp>::fixed<3> get_origin() const;
			arma::Col<fltp>::fixed<3> get_offset() const;
			fltp get_alpha() const;
			fltp get_valpha() const;

			// create rotation matrix
			arma::Mat<fltp>::fixed<3,3> create_rotation_matrix(const fltp time) const;

			// apply rotation
			// void apply(arma::Mat<fltp> &R, arma::Mat<fltp> &L, arma::Mat<fltp> &N, arma::Mat<fltp> &D) const;
			virtual void apply_coords(arma::Mat<fltp> &R, const fltp time) const override;
			virtual void apply_vectors(const arma::Mat<fltp> &R, arma::Mat<fltp> &V, const char str, const fltp time) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
