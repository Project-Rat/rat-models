// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_PATH_ARRAY_HH
#define MDL_MODEL_PATH_ARRAY_HH

#include <armadillo> 
#include <memory>

#include "modelgroup.hh"
#include "inputpath.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelPathArray> ShModelPathArrayPr;

	// applies toroid symmetry to the input coil
	class ModelPathArray: public ModelGroup, public InputPath{
		// set coils
		protected:
			// gridsize
			arma::uword num_ell_ = 1;

			// number of parts
			arma::uword num_parts_ = 0; // zero for same number as num_ell

			// shift position along length
			arma::sword num_shift_ = 0;
			fltp ell_shift_ = RAT_CONST(0.0);
			fltp ell_velocity_ = RAT_CONST(0.0);

		// methods
		public:
			// constructor
			ModelPathArray();
			ModelPathArray(
				const ShModelPr &model, 
				const ShPathPr &pth, 
				const arma::uword num_ell = 1, 
				const arma::uword num_parts = 0,
				const arma::sword num_shift = 0,
				const fltp ell_velocity = 0.0);
			ModelPathArray(
				const std::list<ShModelPr> &model, 
				const ShPathPr &pth, 
				const arma::uword num_ell = 1, 
				const arma::uword num_parts = 0,
				const arma::sword num_shift = 0,
				const fltp ell_velocity = 0.0);

			// factories
			static ShModelPathArrayPr create();
			static ShModelPathArrayPr create(
				const ShModelPr &model, 
				const ShPathPr &pth, 
				const arma::uword num_ell = 1, 
				const arma::uword num_parts = 0,
				const arma::sword num_shift = 0,
				const fltp ell_velocity = 0.0);
			static ShModelPathArrayPr create(
				const std::list<ShModelPr> &model, 
				const ShPathPr &pth, 
				const arma::uword num_ell = 1, 
				const arma::uword num_parts = 0,
				const arma::sword num_shift = 0,
				const fltp ell_velocity = 0.0);

			// getters
			void set_num_ell(const arma::uword num_ell);
			void set_num_parts(const arma::uword num_parts);
			void set_ell_shift(const fltp ell_shift);
			void set_ell_velocity(const fltp ell_velocity);
			void set_num_shift(const arma::sword num_shift);

			// setters
			arma::uword get_num_ell()const;
			arma::uword get_num_parts()const;
			fltp get_ell_shift()const;
			fltp get_ell_velocity()const;
			arma::sword get_num_shift()const;

			// splitting the modelgroup
			std::list<ShModelPr> split(
				const ShSerializerPr &slzr = Serializer::create()) const override;

			// create specific mesh
			virtual std::list<ShMeshDataPr> create_meshes_core(
				const std::list<arma::uword> &trace, 
				const MeshSettings &stngs = MeshSettings()) const override;

			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
