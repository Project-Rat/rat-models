// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef MDL_HB_CURVE_TABLE_HH
#define MDL_HB_CURVE_TABLE_HH

// only available for non-linear solver
#ifdef ENABLE_NL_SOLVER

// general headers
#include <armadillo> 
#include <cassert>
#include <algorithm>
#include <memory>

// common headers
#include "rat/common/node.hh"

// nonlinear solver headers
#include "hbcurve.hh"

// code specific to Raccoon
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class HBCurveTable> ShHBCurveTablePr;
	typedef arma::field<ShHBCurveTablePr> ShHBCurveTablePrList;

	// hb curve containing data table
	class HBCurveTable: public HBCurve{
		// properties
		protected:
			// HB-data
			arma::Col<fltp> Hinterp_;
			arma::Col<fltp> Binterp_;

			// iron filling fraction
			fltp ff_ = RAT_CONST(1.0);

			// reference string
			std::string reference_;

		// methods
		public:
			// constructor
			HBCurveTable();
			static ShHBCurveTablePr create();
				
			// setting
			void set_filling_fraction(const fltp ff);
			void set_reference(const std::string& reference);

			// getting
			fltp get_filling_fraction()const;
			const std::string& get_reference()const;

			// set hard-coded BH tables
			void set_team13();
			void set_team13_1();
			void set_team13_2();
			void set_armco_hot_rolled();
			void set_armco_cold_rolled();

			// load table from file
			void load_from_file(const boost::filesystem::path& fpath);

			// save table to file
			void save_to_file(const boost::filesystem::path& fpath)const;

			// setters
			void set_table(const arma::Mat<fltp> &HB);

			// getters
			arma::Mat<fltp> get_table()const;

			// set BH curve using external data 
			void set_vinh_le_van_fit(
				const fltp mur, 
				const fltp Js, 
				const fltp ff = RAT_CONST(1.0));
			void set_extern(
				const arma::Row<fltp> &Hinterp, 
				const arma::Row<fltp> &Binterp, 
				const fltp ff = RAT_CONST(1.0));
			void set_constant_relative_permeability(
				const fltp mur);

			// create hb data
			virtual nl::ShHBDataPr create_hb_data()const override;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
#endif