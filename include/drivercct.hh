// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_DRIVE_RCCT_HH
#define MDL_DRIVE_RCCT_HH

#include <armadillo> 
#include <memory>
#include <iomanip>
#include <functional>

#include "drive.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class DriveRCCT> ShDriveRCCTPr;
	typedef arma::field<ShDriveRCCTPr> ShDriveRCCTPrList;
	typedef std::function<arma::field<arma::Row<fltp> >(const arma::Row<fltp>&)> ZFunType;

	// circuit class
	class DriveRCCT: public Drive{
		// properties
		protected:
			// the number of poles
			arma::uword num_poles_ = 3;

			// for skew harmonics
			bool is_skew_ = false;

			// the coil radius (to the middle of the straight parts)
			fltp radius_ = 0.03;

			// the skew angle for the main harmonic
			fltp alpha_ = 65.0*arma::Datum<fltp>::tau/360;

			// stumpness parameter
			fltp b_ = 0.0;

			// straightness parameter
			fltp fstr_ = 0.0;

		// methods
		public:
			// constructor
			DriveRCCT();
			DriveRCCT(
				const arma::uword num_poles,
				const fltp radius,
				const fltp alpha,
				const bool is_skew,
				const fltp b);

			// factory
			static ShDriveRCCTPr create();
			static ShDriveRCCTPr create(
				const arma::uword num_poles,
				const fltp radius,
				const fltp alpha = 65.0*arma::Datum<fltp>::tau/360,
				const bool is_skew = false,
				const fltp b = 0.0);

			// setters
			void set_radius(const fltp radius);
			void set_alpha(const fltp alpha);
			void set_num_poles(const arma::uword num_poles);
			void set_is_skew(const bool is_skew);
			void set_b(const fltp b);
			void set_fstr(const fltp fstr);

			// getters
			fltp get_radius()const;
			fltp get_alpha()const;
			arma::uword get_num_poles()const;
			bool get_is_skew()const;
			fltp get_b()const;
			fltp get_fstr()const;
			
			// calculate radius
			static arma::field<arma::Row<fltp> > calc_radius_cyl(
				const ZFunType &zfun, const arma::Row<fltp>& theta_mod, 
				const fltp alpha, const fltp radius);
			static arma::field<arma::Row<fltp> > calc_radius_cone(
				const ZFunType &zfun, const arma::Row<fltp>& theta_mod, 
				const fltp radius, const arma::uword num_poles);

			// get scaling as function of time
			fltp get_scaling(
				const fltp position,
				const fltp time = 0.0,
				const arma::uword derivative = 0) const override;

			// get scaling array fun
			arma::Row<fltp> get_scaling_vec(
				const arma::Row<fltp> &positions, 
				const fltp time = 0.0, 
				const arma::uword derivative = 0) const override;

			// apply scaling for the input settings
			virtual void rescale(const fltp scale_factor) override;

			// validity check
			bool is_valid(const bool enable_throws)const override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
