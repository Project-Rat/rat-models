// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALC_HARMONICS_HH
#define MDL_CALC_HARMONICS_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/log.hh"

#include "calcpath.hh"
#include "harmonicsdata.hh"
#include "pathaxis.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcHarmonics> ShCalcHarmonicsPr;

	// calculate field on line defined 
	// by start and end point coordinates
	class CalcHarmonics: public CalcPath{
		// properties
		protected:
			// reference radius
			fltp reference_radius_ = RAT_CONST(20e-3);

			// compenste for curved path by integrating over length
			bool compensate_curvature_ = false;
			
			// which field type to use
			bool use_magnetic_field_ = false;

			// number of target points around circle
			arma::uword num_theta_ = 64;

			// max number of harmonics
			arma::uword num_max_ = 10;

		// methods
		public:
			// constructor
			CalcHarmonics();
			explicit CalcHarmonics(const ShModelPr &model);
			CalcHarmonics(
				const ShModelPr &model, 
				const ShPathPr &input_path, 
				const fltp reference_radius, 
				const bool compensate_curvature);
			
			// factory methods
			static ShCalcHarmonicsPr create();
			static ShCalcHarmonicsPr create(
				const ShModelPr &model, 
				const ShPathPr &input_path = PathAxis::create('z', 'y', RAT_CONST(0.5), {RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0)}, RAT_CONST(4e-3)), 
				const fltp reference_radius = 20e-3, 
				const bool compensate_curvature = false);

			// setters
			void set_use_magnetic_field(const bool use_magnetic_field = true);
			void set_reference_radius(const fltp reference_radius);
			void set_num_theta(const arma::uword num_theta);
			void set_compensate_curvature(const bool compensate_curvature = true);
			void set_num_max(const arma::uword num_max);

			// getters 
			bool get_use_magnetic_field()const;
			fltp get_reference_radius() const;
			arma::uword get_num_theta() const;
			bool get_compensate_curvature() const;
			arma::uword get_num_max() const;

			// calculate with inductance data output
			ShHarmonicsDataPr calculate_harmonics(
				const fltp time = RAT_CONST(0.0), 
				const cmn::ShLogPr &lg = cmn::NullLog::create(),
				const ShSolverCachePr& cache = NULL);

			// calculation function
			virtual std::list<ShDataPr> calculate(
				const fltp time, 
				const cmn::ShLogPr &lg = cmn::NullLog::create(),
				const ShSolverCachePr& cache = NULL) override;
			
			// creation of calculation data objects
			std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override; // create a mesh

			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif

