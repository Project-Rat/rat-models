// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_CABLE_HH
#define MDL_PATH_CABLE_HH

// general headers
#include <armadillo> 
#include <memory>

// model headers
#include "path.hh"
#include "transformations.hh"
#include "inputpath.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathCable> ShPathCablePr;

	// path defining the shape of the cable
	// uses the coil path as input
	class PathCable: public Path, public InputPath, public Transformations{
		// properties
		protected:
			// flip N and D
			bool layer_wound_ = false; // wind in the transverse direction instead of the normal direction
			bool flip_direction_ = false; // flip winding direction from outward to inward
			bool reorient_ = false; // recalculate longitudinal and normal vectors after making the cable path

			// settings
			arma::uword num_turns_ = 1llu; // number of turns
			fltp turn_step_ = RAT_CONST(0.0); // stepsize of each turn: usually dcable + 2*dinsu

			// start position and increment position
			arma::uword idx_start_ = 0llu; // start section
			arma::uword idx_incr_ = 0llu; // section of increment
			arma::sword num_add_ = 0llu; // add extra sections
			//arma::uword num_rem_ = 0; // remove sections
			
			// offsetting the coil block
			arma::sword num_offset_ = 0ll; // ofset in n-direction (in cable thicnesses)
			fltp offset_ = RAT_CONST(0.0); // absolute offset in n-direction [m]

			// disable increment
			arma::uword incr_order_ = 3llu; 
			bool disable_increment_ = false;
			bool reverse_base_ = false;

		// methods
		public:
			// constructor
			PathCable();
			explicit PathCable(
				const ShPathPr &input_path,
				const bool reverse_base = false, 
				const arma::uword num_turns = 1, 
				const arma::uword num_add = 0, 
				const fltp turn_step = RAT_CONST(0.0), 
				const arma::uword idx_start = 0, 
				const arma::uword idx_incr = 0,
				const bool disable_increment = false,
				const arma::uword num_offset = 0,
				const fltp offset = RAT_CONST(0.0),
				const bool layer_wound = false,
				const bool flip_direction = false,
				const bool reorient = false);
			
			// factory methods
			static ShPathCablePr create();
			static ShPathCablePr create(
				const ShPathPr &input_path,
				const bool reverse_base = false, 
				const arma::uword num_turns = 1, 
				const arma::uword num_add = 0, 
				const fltp turn_step = RAT_CONST(0.0), 
				const arma::uword idx_start = 0, 
				const arma::uword idx_incr = 0,
				const bool disable_increment = false,
				const arma::uword num_offset = 0,
				const fltp offset = RAT_CONST(0.0),
				const bool layer_wound = false,
				const bool flip_direction = false,
				const bool reorient = false);

			// set base path
			void set_reverse_base(const bool reverse_base);

			// get indexing arrays
			// virtual void get_indexing(arma::Row<arma::uword> &section_idx, arma::Row<arma::uword> &turn_idx) const;

			// setters
			void set_flip_direction(const bool flip_direction);
			void set_rebuild_darboux_vectors(const bool rebuild_darboux_vectors = true);
			void set_num_turns(const arma::uword num_turns);
			void set_turn_step(const fltp turn_step);
			void set_disable_increment(const bool disable_increment = true);
			void set_num_add(const arma::sword num_add);
			void set_incr_order(const arma::uword idx_incr);
			void set_idx_incr(const arma::uword idx_incr);
			void set_num_offset(const arma::sword num_offset);
			void set_offset(const fltp offset);
			void set_idx_start(const arma::uword idx_start);
			void set_layer_wound(const bool layer_wound = true);
			void set_reorient(const bool reorient = true);

			// getters
			bool get_flip_direction()const;
			bool get_reverse_base() const;
			bool get_rebuild_darboux_vectors()const;
			arma::uword get_num_turns() const;
			fltp get_turn_step() const;
			arma::uword get_idx_start() const;
			arma::sword get_num_add() const;
			arma::uword get_idx_incr() const;
			arma::uword get_incr_order() const;
			arma::sword get_num_offset() const;
			fltp get_offset() const;
			bool get_disable_increment() const;
			bool get_layer_wound() const;
			bool get_reorient()const;

			// get frame
			virtual ShFramePr create_frame(
				const MeshSettings &stngs) const override;
			
			// indexing arrays
			void get_indexing(
				arma::Row<arma::uword> &section_idx, 
				arma::Row<arma::uword> &turn_idx) const;

			// tree structure
			void reindex() override;

			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
