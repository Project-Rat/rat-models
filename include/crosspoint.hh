// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CROSS_POINT_HH
#define MDL_CROSS_POINT_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <json/json.h>

#include "cross.hh"
#include "area.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CrossPoint> ShCrossPointPr;

	// cross section of coil
	class CrossPoint: public Cross{
		// properties
		private:
			// coordinate
			fltp nc_ = RAT_CONST(0.0);
			fltp tc_ = RAT_CONST(0.0);

			// hidden area
			fltp thickness_ = RAT_CONST(1e-3);
			fltp width_ = RAT_CONST(1e-3);

		// methods
		public:
			// constructors
			CrossPoint();
			CrossPoint(
				const fltp nc,const fltp tc, 
				const fltp thickness, const fltp width);

			// factory methods
			static ShCrossPointPr create();
			static ShCrossPointPr create(
				const fltp nc, const fltp tc, 
				const fltp thickness, const fltp width);

			// set properties
			void set_coord(const fltp uc, const fltp vc);
			void set_thickness(const fltp thickness);
			void set_width(const fltp width);
			void set_nc(const fltp uc);
			void set_tc(const fltp vc);

			// gettters
			fltp get_thickness() const;
			fltp get_width() const;
			fltp get_nc() const;
			fltp get_tc() const;

			// get bounding box
			virtual arma::Mat<fltp>::fixed<2,2> get_bounding() const override;

			// volume mesh
			virtual ShAreaPr create_area(const MeshSettings &stngs) const override;

			// surface mesh
			// virtual ShPerimeterPr create_perimeter() const override;

			// vallidity check 
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
