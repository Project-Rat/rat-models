// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_TOROID_HH
#define MDL_MODEL_TOROID_HH

#include <armadillo> 
#include <memory>

#include "modelgroup.hh"
#include "meshdata.hh"
#include "serializer.hh"


// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelToroid> ShModelToroidPr;

	// applies toroid symmetry to the input coil
	class ModelToroid: public ModelGroup{
		// set coils
		protected:
			// alternate
			bool alternate_ = false;

			// orientation
			char coil_azymuth_ = 'z';
			char coil_radial_ = 'x';
			char toroid_axis_ = 'z';
			char toroid_radial_ = 'x';

			// settings
			arma::uword num_coils_ = 6;
			arma::uword num_parts_ = 0;
			arma::sword num_offset_ = 0;
			fltp radius_ = 0;
			fltp phi_ = 0;

		// methods
		public:
			// constructor
			ModelToroid();
			explicit ModelToroid(const ShModelPr &base_model, const arma::uword num_coils = 0);
			explicit ModelToroid(const std::list<ShModelPr> &models, const arma::uword num_coils = 0);

			// factory method
			static ShModelToroidPr create();
			static ShModelToroidPr create(const ShModelPr &base_model, const arma::uword num_coils = 0);
			static ShModelToroidPr create(const std::list<ShModelPr> &models, const arma::uword num_coils = 0);

			// setting properties
			void set_num_coils(const arma::uword num_coils);
			void set_num_parts(const arma::uword num_parts);
			void set_num_offset(const arma::sword num_offset);
			void set_radius(const fltp radius);
			void set_phi(const fltp phi);
			
			// getting properties
			arma::uword get_num_coils()const;
			arma::uword get_num_parts()const;
			arma::sword get_num_offset()const;
			fltp get_radius()const;
			fltp get_phi()const;

			// splitting the modelgroup
			virtual std::list<ShModelPr> split(
				const ShSerializerPr &slzr = Serializer::create()) const override;

			// get number of calculation objects
			// virtual arma::uword get_num_objects() const override;
			
			// set orientation
			void set_alternate(const bool alternate = true);
			void set_coil_azymuth(const char coil_axis);
			void set_coil_radial(const char coil_radial);
			void set_toroid_axis(const char toroid_axis);
			void set_toroid_radial(const char toroid_radial);

			// get orientation
			bool get_alternate() const;
			char get_coil_azymuth() const;
			char get_toroid_axis() const;
			char get_coil_radial() const;
			char get_toroid_radial() const;

			// create calculation data objects
			std::list<ShMeshDataPr> create_meshes_core(
				const std::list<arma::uword> &trace, 
				const MeshSettings &stngs = MeshSettings()) const override; // create volume current elements

			// validity check
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
