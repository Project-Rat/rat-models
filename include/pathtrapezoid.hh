// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_TRAPEZOID_HH
#define MDL_PATH_TRAPEZOID_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "path.hh"
#include "transformations.hh"
#include "frame.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathTrapezoid> ShPathTrapezoidPr;

	// cross section of coil
	class PathTrapezoid: public Path, public Transformations{
		// properties
		protected:
			// coil dimensions
			fltp arc_radius_ = RAT_CONST(0.005);
			fltp length_1_ = RAT_CONST(0.3);
			fltp length_2_ = RAT_CONST(0.2);
			fltp width_ = RAT_CONST(0.1);

			// discretization
			fltp element_size_ = RAT_CONST(2e-3);
			fltp offset_ = RAT_CONST(0.0);

		// methods
		public:
			// constructor
			PathTrapezoid();
			PathTrapezoid(
				const fltp arc_radius, const fltp length_1,
				const fltp length_2, const fltp width,
				const fltp element_size, const fltp offset = RAT_CONST(0.0));

			// factory
			static ShPathTrapezoidPr create();
			static ShPathTrapezoidPr create(
				const fltp arc_radius, const fltp length_1,
				const fltp length_2, const fltp width,
				const fltp element_size, const fltp offset = RAT_CONST(0.0));

			// setting
			void set_arc_radius(const fltp arc_radius);
			void set_length_1(const fltp length_1);
			void set_length_2(const fltp length_2);
			void set_width(const fltp width);
			void set_element_size(const fltp element_size);
			void set_offset(const fltp offset);

			// getting
			fltp get_arc_radius()const;
			fltp get_length_1()const;
			fltp get_length_2()const;
			fltp get_width()const;
			fltp get_element_size()const;
			fltp get_offset()const;

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
