// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef MDL_VTK_UNSTR_HH
#define MDL_VTK_UNSTR_HH

// general headers
#include <armadillo> 
#include <memory>
#include <iomanip>

// VTK headers
#include <vtkUnstructuredGrid.h>
#include <vtkProbeFilter.h>
#include <vtkSmartPointer.h>

// magrat headers
#include "rat/common/log.hh"

// rat headers
#include "vtkobj.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class VTKUnstr> ShVTKUnstrPr;

	// VTK Unstructured grid
	class VTKUnstr: public VTKObj{
		// properties
		protected:
			// index of the part and mesh
			arma::uword part_index_ = 0;
			arma::uword mesh_index_ = 0;

			// elements
			std::map<arma::uword, std::list<arma::Mat<arma::uword> > > n_;
			std::map<arma::uword, std::list<arma::Mat<arma::uword> > > d_;

			// data at nodes
			std::map<std::string, arma::Mat<fltp> > node_data_;
			std::map<std::string, arma::Mat<fltp> > element_data_;

			// nodes
			arma::Mat<fltp> Rn_;

		// methods
		public:
			// constructor
			VTKUnstr();
			explicit VTKUnstr(
				const std::list<ShVTKUnstrPr> &unstr_list, 
				const bool merge_points = false, 
				const fltp merge_tolerance = 1e-6);

			// factory
			static ShVTKUnstrPr create();
			static ShVTKUnstrPr create(
				const std::list<ShVTKUnstrPr> &unstr_list, 
				const bool merge_points = false, 
				const fltp merge_tolerance = 1e-6);

			// merge meshes by index
			static std::list<ShVTKUnstrPr> merge_by_index(
				const std::list<ShVTKUnstrPr> &unstr_list, 
				const bool merge_points, 
				const fltp merge_tolerance,
				const bool merge_parts);

			// point merge
			void merge_points(const fltp merge_tolerance = 1e-6);

			// get raw data grid 
			vtkSmartPointer<vtkUnstructuredGrid> get_vtk_ugrid() const;

			// get number of nodes and number of elements
			arma::uword get_num_nodes() const;
			arma::uword get_num_elements() const;

			// write mesh
			void set_mesh_index(const arma::uword mesh_index);
			void set_part_index(const arma::uword part_index);
			void set_mesh(const arma::Mat<fltp> &Rn, const arma::Mat<arma::uword> &n, const arma::uword element_type);
			void set_nodes(const arma::Mat<fltp> &Rn);
			void set_elements(const arma::Mat<arma::uword> &n, const arma::uword element_type);
			void set_elements(const arma::field<arma::Mat<arma::uword> > &n, const arma::Row<arma::uword> &element_types);
			void merge_elements();

			// get elements
			arma::uword get_mesh_index()const;
			arma::uword get_part_index()const;
			const std::map<arma::uword, std::list<arma::Mat<arma::uword> > >& get_elements()const;
			const std::map<arma::uword, std::list<arma::Mat<arma::uword> > >& get_data_index()const;
			const std::map<std::string, arma::Mat<fltp> >& get_node_data()const;
			const std::map<std::string, arma::Mat<fltp> >& get_element_data()const;
			const arma::Mat<fltp>& get_nodes()const;

			// add time stamp to data
			// void set_global(const fltp val, const std::string &data_name);

			// write data at nodes
			void set_nodedata(const arma::Mat<fltp> &v, const std::string &data_name);
			void set_elementdata(const arma::Mat<fltp> &v, const std::string &data_name);

			// get filename extension
			std::string get_filename_ext() const override;

			// write output file
			void write(const boost::filesystem::path fname, const cmn::ShLogPr& lg = cmn::NullLog::create()) override;

			// interpolation methods
			// void setup_interpolation();
			arma::Mat<fltp> interpolate_field(const arma::Mat<fltp> &Rp, const std::string &field_name);

			// element type list
			static int get_element_type(const arma::uword num_rows, const arma::uword num_dim);
	};

}}

#endif
