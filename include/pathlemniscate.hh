// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_LEMNISCATE_HH
#define MDL_PATH_LEMNISCATE_HH

#include <armadillo> 
#include <cassert>
#include <memory>

#include "transformations.hh"
#include "path.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathLemniscate> ShPathLemniscatePr;

	// cross section of coil
	class PathLemniscate: public Path, public Transformations{
		// properties
		private:
			// lemniscate parameters
			fltp a_ = RAT_CONST(0.05);
			fltp b_ = RAT_CONST(0.05);
			fltp c_ = RAT_CONST(0.08);
			fltp d_ = RAT_CONST(0.03);
			fltp lambda_ = RAT_CONST(8.0);

			// center point offset
			fltp alpha_ = RAT_CONST(0.0);
			fltp beta_ = RAT_CONST(0.0);
			fltp gamma_ = RAT_CONST(0.0);

			// discretization
			fltp element_size_ = RAT_CONST(0.004);
			fltp offset_ = RAT_CONST(0.0);

			// number of sections
			arma::uword num_sections_ = 4llu;

		// methods
		public:
			// constructor
			PathLemniscate();
			PathLemniscate(
				const fltp a, 
				const fltp b, 
				const fltp c, 
				const fltp d,
				const fltp lambda,
				const fltp element_size,
				const fltp offset = RAT_CONST(0.0),
				const arma::uword num_sections = 4llu);

			// factory methods
			static ShPathLemniscatePr create();
			static ShPathLemniscatePr create(
				const fltp a, 
				const fltp b, 
				const fltp c, 
				const fltp d,
				const fltp lambda,
				const fltp element_size,
				const fltp offset = RAT_CONST(0.0),
				const arma::uword num_sections = 4llu);

			// set properties
			void set_a(const fltp a);
			void set_b(const fltp b);
			void set_c(const fltp c);
			void set_d(const fltp d);
			void set_lambda(const fltp lambda);
			void set_element_size(const fltp element_size);
			void set_num_sections(const arma::uword num_sections);
			void set_offset(const fltp offset);
			void set_alpha(const fltp alpha);
			void set_beta(const fltp beta);
			void set_gamma(const fltp gamma);

			// get properties
			fltp get_a() const;
			fltp get_b() const;
			fltp get_c() const;
			fltp get_d() const;
			fltp get_lambda() const;
			fltp get_element_size() const;
			arma::uword get_num_sections()const;
			fltp get_offset() const;
			fltp get_alpha() const;
			fltp get_beta() const;
			fltp get_gamma() const;

			// coords function
			arma::Row<fltp> calc_s(const arma::Row<fltp>& theta)const;
			arma::Row<fltp> calc_p(const arma::Row<fltp>& theta)const;
			arma::Mat<fltp> create_coords(const arma::Row<fltp>& theta)const;

			// get frame
			virtual ShFramePr create_frame(
				const MeshSettings &stngs) const override;

			// validity check
			virtual bool is_valid(
				const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif