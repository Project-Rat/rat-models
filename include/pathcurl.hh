// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_CURL_HH
#define MDL_PATH_CURL_HH

// general headers
#include <armadillo> 
#include <memory>

// model headers
#include "path.hh"
#include "transformations.hh"
#include "inputpath.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathCurl> ShPathCurlPr;

	// path defining the shape of the cable
	// uses the coil path as input
	class PathCurl: public Path, public InputPath, public Transformations{
		// properties
		protected:
			// disable rotation
			bool disable_rotation_ = false;

			// inner layer jump
			fltp first_height_ = 0.0;
			fltp first_radius_ = 4;

			// outer layer jump
			fltp second_height_ = 0.0;
			fltp second_radius_ = 4;

		// methods
		public:
			// constructor
			PathCurl();
			explicit PathCurl(const ShPathPr &input_path);
			
			// factory methods
			static ShPathCurlPr create();
			static ShPathCurlPr create(const ShPathPr &input_path);

			// setters
			void set_first_radius(const fltp first_radius);
			void set_first_height(const fltp first_height);
			void set_second_radius(const fltp second_radius);
			void set_second_height(const fltp second_height);
			void set_disable_rotation(const bool disable_rotation = true);
			
			// getters
			fltp get_first_radius() const;
			fltp get_first_height() const;
			fltp get_second_radius() const;
			fltp get_second_height() const;
			bool get_disable_rotation()const;

			// get frame
			virtual ShFramePr create_frame(
				const MeshSettings &stngs) const override;

			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
