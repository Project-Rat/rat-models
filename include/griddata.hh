// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_GRID_DATA_HH
#define MDL_GRID_DATA_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/log.hh"

#include "vtkimg.hh"
#include "meshdata.hh"
#include "targetdata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class GridData> ShGridDataPr;

	// template for coil
	class GridData: public TargetData{
		// properties
		protected:
			// additional lines
			arma::field<arma::Mat<fltp> > linedata_; // for example coil cross-section

		// methods
		public:
			// create element matrix
			virtual arma::Mat<arma::uword> create_elements()const = 0;

			// set properties
			void set_linedata(const arma::field<arma::Mat<fltp> > &lines);

			// get grid data
			const arma::field<arma::Mat<fltp> >& get_linedata()const;

			// extract iso surface
			virtual ShMeshDataPr extract_isosurface(
				const arma::Row<fltp> &values,
				const fltp iso_value, 
				const bool use_parallel = true) const = 0;
	};

}}

#endif
