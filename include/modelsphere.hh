// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_SPHERE_HH
#define MDL_MODEL_SPHERE_HH

#include <armadillo> 
#include <memory>

#include "rat/mat/inputconductor.hh"

#include "model.hh"
#include "drive.hh"
#include "drivedc.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelSphere> ShModelSpherePr;

	// model object that is capable of creating meshes
	// using a base path and a cross section
	class ModelSphere: public Model{
		// properties
		protected:
			// coordinates
			bool polar_orientation_ = false;

			// element type
			bool use_tetrahedrons_ = false; // otherwise use hexahedrons

			// sphere radius
			fltp radius_ = RAT_CONST(0.03);

			// element size
			fltp element_size_ = RAT_CONST(0.003);

			// core size
			fltp core_size_ = RAT_CONST(0.7); // only used for hexahedrons

			// position
			arma::Col<fltp>::fixed<3> R0_{RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0)};

			// temperature of the coil
			fltp operating_temperature_ = RAT_CONST(4.5);

			// current drive
			ShDrivePr temperature_drive_ = DriveDC::create(RAT_CONST(1.0));

		// methods
		public:
			// constructor
			ModelSphere();
			ModelSphere(const fltp radius, const fltp element_size);

			// factory methods
			static ShModelSpherePr create();
			static ShModelSpherePr create(const fltp radius, const fltp element_size);

			// getters
			void set_operating_temperature(const fltp operating_temperature);
			void set_temperature_drive(const ShDrivePr &temperature_drive);
			void set_radius(const fltp radius);
			void set_element_size(const fltp element_size);
			void set_use_tetrahedrons(const bool use_tetrahedrons = true);
			void set_polar_orientation(const bool polar_orientation = true);

			// setters
			fltp get_radius()const;
			fltp get_element_size()const;
			bool get_use_tetrahedrons()const;
			fltp get_operating_temperature() const;

			// function for setting up the spherical mesh
			void setup_mesh(
				const ShMeshDataPr &mesh_data, 
				const MeshSettings &stngs = MeshSettings()) const;

			// create specific mesh
			virtual std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override;

			// input validity
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
