// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_ELL_ARC_HH
#define MDL_PATH_ELL_ARC_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "path.hh"
#include "frame.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathEllipticalArc> ShPathEllipticalArcPr;

	// circular softway bend
	class PathEllipticalArc: public Path{
		// properties
		protected:
			// bend in normal or transverse direction
			// where transverse implies hard-way bending
			bool transverse_ = false;
			
			// bend in the other direction
			bool negative_bend_ = false;

			// offset in [m]
			fltp offset_ = RAT_CONST(0.0);

			// target element length in [m]
			fltp element_size_ = RAT_CONST(0.0);

			// semi major of ellipse in [m]
			fltp semi_major_ = RAT_CONST(0.0);

			// semi minor of ellipse in [m]
			fltp semi_minor_ = RAT_CONST(0.0);

			// angle of start point [rad]
			fltp theta1_ = RAT_CONST(0.0);

			// angle of end point [rad]
			fltp theta2_ = RAT_CONST(0.0);

		// methods
		public:
			// constructor
			PathEllipticalArc();
			PathEllipticalArc(
				const fltp semi_major,
				const fltp semi_minor,
				const fltp theta1,
				const fltp theta2,
				const fltp element_size,
				const fltp offset = RAT_CONST(0.0),
				const bool transverse = false);

			// factory
			static ShPathEllipticalArcPr create();
			static ShPathEllipticalArcPr create(
				const fltp semi_major,
				const fltp semi_minor,
				const fltp theta1,
				const fltp theta2,
				const fltp element_size,
				const fltp offset = RAT_CONST(0.0),
				const bool transverse = false);

			// set geometry
			void set_transverse(const bool transverse = true);
			void set_theta1(const fltp theta1);
			void set_theta2(const fltp theta2);
			void set_semi_major(const fltp semi_major);
			void set_semi_minor(const fltp semi_minor);
			void set_offset(const fltp offset);
			void set_element_size(const fltp element_size);
			void set_negative_bend(const bool negative_bend = true);

			// get geometry
			bool get_transverse()const;
			fltp get_offset()const;
			fltp get_element_size()const;
			fltp get_theta1()const;
			fltp get_theta2()const;
			fltp get_semi_major()const;
			fltp get_semi_minor()const;
			bool get_negative_bend()const;

			// get frame
			virtual ShFramePr create_frame(
				const MeshSettings &stngs) const override;

			// validity check
			virtual bool is_valid(
				const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
