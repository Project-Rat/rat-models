// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_DRIVE_INTERP_HH
#define MDL_DRIVE_INTERP_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "drive.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class DriveInterp> ShDriveInterpPr;
	typedef arma::field<ShDriveInterpPr> ShDriveInterpPrList;

	// circuit class
	class DriveInterp: public Drive{
		// properties
		protected:
			// smoothing
			fltp dt_ = RAT_CONST(0.0);

			// interpolation table
			arma::Col<fltp> ti_;
			arma::Col<fltp> vi_;

		// methods
		public:
			// constructor
			DriveInterp();
			DriveInterp(
				const arma::Col<fltp> &ti, 
				const arma::Col<fltp> &vi,
				const fltp dt = RAT_CONST(0.0));

			// factory
			static ShDriveInterpPr create();
			static ShDriveInterpPr create(
				const arma::Col<fltp> &ti, 
				const arma::Col<fltp> &vi,
				const fltp dt = RAT_CONST(0.0));

			// setters
			void set_interpolation_table(
				const arma::Col<fltp> &ti, 
				const arma::Col<fltp> &vi);
			void set_interpolation_times(
				const arma::Col<fltp> &ti);
			void set_interpolation_values(
				const arma::Col<fltp> &vi);

			// getters
			arma::uword get_num_times()const;

			// access interpolation table
			const arma::Col<fltp>& get_interpolation_times()const;
			const arma::Col<fltp>& get_interpolation_values()const;

			// plot range
			virtual fltp get_v1() const override;
			virtual fltp get_v2() const override;

			// smoothing
			void set_delta_time(const fltp dt);
			fltp get_delta_time()const;

			// get data
			fltp get_scaling_core(
				const fltp position,
				const arma::uword derivative) const;
			virtual fltp get_scaling(
				const fltp position,
				const fltp time = 0.0,
				const arma::uword derivative = 0) const override;

			// get inflection points
			arma::Col<fltp> get_inflection_points()const override;

			// validity check
			bool is_valid(const bool enable_throws)const override;

			// apply scaling for the input settings
			virtual void rescale(const fltp scale_factor) override;
			
			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
