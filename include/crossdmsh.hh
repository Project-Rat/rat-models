// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CROSS_DMSH_HH
#define MDL_CROSS_DMSH_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <json/json.h>

#include "rat/dmsh/distmesh2d.hh"
#include "rat/dmsh/distfun.hh"
#include "rat/dmsh/dfones.hh"

#include "area.hh"
#include "cross.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CrossDMsh> ShCrossDMshPr;

	// cross section of coil
	class CrossDMsh: public Cross{
		// properties
		private:
			// quad mesh
			bool quad_ = true;

			// element size
			fltp h0_ = RAT_CONST(0.01);

			// tolerance
			fltp dptol_ = RAT_CONST(0.001); // fraction of h0

			// mesh input
			dm::ShDistFunPr fd_;
			dm::ShDistFunPr fh_ = dm::DFOnes::create();

			// additional fixed points
			arma::Mat<fltp> pfix_ext_ = arma::Mat<fltp>(0llu,2llu); // no points

			// node limit
			arma::uword num_node_limit_ = 20000llu;

			// maximum number of iterations
			arma::uword max_iter_ = 1000llu;

			// random number generator seed
			unsigned int rng_seed_ = 1001;
			fltp rng_strength_ = RAT_CONST(0.2);

		// methods
		public:
			// constructors
			CrossDMsh();
			CrossDMsh(
				const fltp h0, 
				const dm::ShDistFunPr &df, 
				const dm::ShDistFunPr &hf = dm::DFOnes::create());
			
			// factory methods
			static ShCrossDMshPr create();
			static ShCrossDMshPr create(
				const fltp h0, 
				const dm::ShDistFunPr &df, 
				const dm::ShDistFunPr &hf = dm::DFOnes::create());

			// setters
			void set_h0(const fltp h0);
			void set_distfun(const dm::ShDistFunPr &fd);
			void set_scalefun(const dm::ShDistFunPr &fh);
			void set_rng_seed(const unsigned int rng_seed);
			void set_rng_strength(const fltp rng_strength);
			void set_dptol(const fltp dptol);
			void set_max_iter(const arma::uword max_iter);

			// getters
			fltp get_h0()const;
			void set_num_node_limit(const arma::uword num_node_limit);
			arma::uword get_num_node_limit()const;
			const dm::ShDistFunPr& get_distfun()const;
			const dm::ShDistFunPr& get_scalefun()const;
			unsigned int get_rng_seed()const;
			fltp get_rng_strength()const;
			fltp get_dptol()const;
			arma::uword get_max_iter()const;

			// set fixed points
			void set_fixed(const arma::Mat<fltp> &pfix_ext);

			// get bounding box
			arma::Mat<fltp>::fixed<2,2> get_bounding() const override;

			// create mesher
			dm::ShDistMesh2DPr create_distmesher()const;

			// volume mesh
			virtual ShAreaPr create_area(const MeshSettings &stngs) const override;

			// surface mesh
			// virtual ShPerimeterPr create_perimeter() const override;

			// validity check
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
