// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_HARMONICS_DATA_HH
#define MDL_HARMONICS_DATA_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/log.hh"

#include "targetdata.hh"
#include "frame.hh"
#include "vtkobj.hh"
#include "vtktable.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class HarmonicsData> ShHarmonicsDataPr;

	// calculates 2D pseudo harmonics along a path
	// this path uses the same object as used for setting
	// up the coils and can thus be curved.
	class HarmonicsData: public TargetData{
		// properties
		protected:
			// highest harmonic to output
			arma::uword num_max_ = 10;

			// angle
			arma::Row<fltp> theta_;

			// base
			arma::Mat<fltp> Rb_;
			arma::Mat<fltp> Nb_;
			arma::Mat<fltp> Db_;
			arma::Mat<fltp> Lb_;

			// reference radius
			fltp reference_radius_;

			// result matrices
			arma::Row<fltp> ell_;
			arma::Mat<fltp> An_;
			arma::Mat<fltp> Bn_;
		
			// axial field
			arma::Row<fltp> Ba_;

			// element length
			arma::Row<fltp> ellt_;

		// methods
		public:
			// constructor
			HarmonicsData();
			HarmonicsData(
				const ShFramePr &frame, 
				const fltp reference_radius, 
				const bool compensate_curvature, 
				const bool use_magnetic_field,
				const arma::uword num_theta);

			// factory methods
			static ShHarmonicsDataPr create();
			static ShHarmonicsDataPr create(
				const ShFramePr &frame, 
				const fltp reference_radius, 
				const bool compensate_curvature = false, 
				const bool use_magnetic_field = false,
				const arma::uword num_theta = 64);

			// set highest harmonic to output
			void set_num_max(const arma::uword num_max);
			arma::uword get_num_max() const;

			// get the reference radius
			fltp get_reference_radius() const;

			// calculation function
			void setup(
				const ShFramePr &frame, 
				const fltp reference_radius, 
				const bool compensate_curvature, 
				const bool use_magnetic_field,
				const arma::uword num_theta);

			// post processing
			void post_process() override;

			// getting calculated harmonic tables
			void get_harmonics(
				arma::Row<fltp> &ell, 
				arma::Mat<fltp> &An, 
				arma::Mat<fltp> &Bn) const;

			// getting integrated harmonics
			void get_harmonics(
				arma::Row<fltp> &An, 
				arma::Row<fltp> &Bn) const;

			// get fringe field
			const arma::Row<fltp>& get_axial_field() const;
			const arma::Row<fltp>& get_ell() const;
			
			// create elements to connect target points
			arma::Mat<arma::uword> create_line_elements() const;

			// VTK exporting
			ShVTKObjPr export_vtk() const override;
			ShVTKTablePr export_vtk_table() const;

			// display function
			void display(const cmn::ShLogPr &lg) const;
	};

}}

#endif
