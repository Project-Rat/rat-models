// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PARTICLE_HH
#define MDL_PARTICLE_HH

#include <armadillo> 
#include <memory>

#include "targetdata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// particle and its track
	class Particle{
		protected:
			// properties
			fltp rest_mass_ = 0; // [eV/C^2] 
			fltp charge_ = 0; // elementary charge

			// start location
			arma::Col<fltp>::fixed<3> R0_ = {0,0,0};
			arma::Col<fltp>::fixed<3> V0_ = {0,0,0};

			// track information
			bool is_relativistic_ = false;
			arma::Row<fltp> t_;
			arma::Mat<fltp> Rt_;
			arma::Mat<fltp> Vt_;
			arma::Mat<fltp> Bt_;
			arma::Mat<fltp> Et_;

			// track status
			bool alive_start_ = true;
			bool alive_end_ = true;
			arma::uword num_steps_ = 0;
			
			// indices
			arma::sword idx_max_ = 0;
			arma::sword idx_min_ = 0;
			
		public:
			// constructor
			Particle();

			// setting basic properties
			void set_rest_mass(const fltp rest_mass);
			void set_charge(const fltp charge);
			
			// get basic particle properties
			fltp get_rest_mass() const;
			fltp get_charge() const;

			// set start coordinate
			void set_num_steps(const arma::uword num_steps);
			void set_start_index(const arma::uword start_idx);
			void set_startcoord(
				const arma::Col<fltp>::fixed<3> &R0, 
				const arma::Col<fltp>::fixed<3> &V0);
			void set_is_relativistic(
				const bool is_relativistic = true);

			// allocate
			void setup();

			// reduce memory footprint
			void truncate(); 

			// get whether the track is alive
			bool is_alive_start() const;
			bool is_alive_end() const;

			// get start coordinate
			const arma::Col<fltp>::fixed<3>& get_initial_coord();
			const arma::Col<fltp>::fixed<3>& get_initial_velocity();

			// get the current time, position and velocity
			fltp get_first_time() const;
			arma::Col<fltp>::fixed<3> get_first_coord() const;
			arma::Col<fltp>::fixed<3> get_first_velocity() const;
			fltp get_last_time() const;
			arma::Col<fltp>::fixed<3> get_last_coord() const;
			arma::Col<fltp>::fixed<3> get_last_velocity() const;
			
			// set the next position
			void set_prev_coord(
				const fltp t, 
				const arma::Col<fltp>::fixed<3> &Rp, 
				const arma::Col<fltp>::fixed<3> &Vp,
				const arma::Col<fltp>::fixed<3> &Bp,
				const arma::Col<fltp>::fixed<3> &Ep);
			void set_next_coord(
				const fltp t, 
				const arma::Col<fltp>::fixed<3> &Rp, 
				const arma::Col<fltp>::fixed<3> &Vp,
				const arma::Col<fltp>::fixed<3> &Bp,
				const arma::Col<fltp>::fixed<3> &Ep);

			// termination
			void terminate_end();
			void terminate_start();

			// get full track
			bool is_empty()const;
			bool get_is_relativistic()const;
			fltp get_rest_mass_kg()const;
			arma::uword get_num_coords()const;
			arma::Row<fltp> get_time()const;
			arma::Mat<fltp> get_track_coords() const;
			const arma::Mat<fltp>& get_raw_track_coords()const;
			arma::Mat<fltp> get_track_velocities() const;
			arma::Mat<fltp> get_track_momentum() const;
			arma::Mat<fltp> get_track_momentum_ev()const;
			arma::Mat<fltp> get_track_curvature() const;
			arma::Row<fltp> get_track_radius() const;
			arma::Row<fltp> get_deviated_angle(const bool to_end = false) const;
			arma::Mat<fltp> get_track_velocities_c()const;
			arma::Row<fltp> get_track_gamma()const;
			arma::Row<fltp> get_track_temperature()const;

			// set magnetic field on track
			arma::Mat<fltp> get_flux_density()const;
			arma::Mat<fltp> get_electric_field()const;
	};

}}

#endif