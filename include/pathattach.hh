// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_ATTACH_HH
#define MDL_PATH_ATTACH_HH

#include <armadillo> 
#include <cassert>
#include <memory>

#include "path.hh"
#include "frame.hh"
#include "transformations.hh"


// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathAttach> ShPathAttachPr;

	// connects two paths with a connector
	// this then becomes a new combined path
	class PathAttach: public Path, public Transformations{
		// properties
		private:
			// index of the input path that remains fixed in place (usually the index of the coil)
			arma::uword fixed_index_ = 2;

			// list of pointers to the path sections
			std::map<arma::uword, ShPathPr> input_paths_;

		// methods
		public:
			// constructor
			PathAttach();
			PathAttach(const std::list<ShPathPr>&path_list, const arma::uword fixed_index);

			// factory methods
			static ShPathAttachPr create();
			static ShPathAttachPr create(const std::list<ShPathPr>&path_list, const arma::uword fixed_index);

			// function for adding a path to the path list
			arma::uword add_path(const ShPathPr &path);
			ShPathPr get_path(const arma::uword index) const;
			bool delete_path(const arma::uword index);
			void reindex() override;
			arma::uword get_num_paths() const;

			// setters
			void set_fixed_index(const arma::uword fixed_index);

			// getters
			arma::uword get_fixed_index()const;

			// validity check
			virtual bool is_valid(const bool enable_throws = false)const override;

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
