// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef MDL_VTK_PVDATA_HH
#define MDL_VTK_PVDATA_HH

// general headers
#include <armadillo> 
#include <iostream>
#include <iomanip>

#include "rat/common/log.hh"
#include "vtkobj.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class VTKPvData> ShVTKPvDataPr;

	// paraview data object
	class VTKPvData: public VTKObj{
		// create a struct to hold file data
		struct VTKFile{
			std::string data_name; fltp time; arma::uword part; boost::filesystem::path fpath;
		};

		// private data
		private:
			// list of files
			std::map<std::string, std::list<VTKFile> > file_list_;

		public:
			// constructor
			VTKPvData();

			// factory
			static ShVTKPvDataPr create();

			// data file
			void add_file(
				const std::string &collection, 
				const std::string &data_name, 
				const fltp time, 
				const arma::uword part, 
				boost::filesystem::path fpath);

			// get filename extension
			std::string get_filename_ext() const override;

			// write output file
			void write(
				const boost::filesystem::path fpath, 
				const cmn::ShLogPr& lg = cmn::NullLog::create()) override;

			// helper function for generating filenames
			static boost::filesystem::path create_fname(
				const std::string &prefix, 
				const arma::uword time_idx, 
				const arma::uword part_idx);
	};

}}

#endif
