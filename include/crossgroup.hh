// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CROSS_GROUP_HH
#define MDL_CROSS_GROUP_HH

#include <armadillo> 
#include <memory>
#include <json/json.h>

#include "cross.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CrossGroup> ShCrossGroupPr;

	// template class for cross section
	class CrossGroup: virtual public Cross{
		// properties
		private:
			// merge nodes
			bool merge_nodes_ = true;

			// input cross sections
			std::map<arma::uword, ShCrossPr> cross_;

		// methods
		public:
			// constructor
			CrossGroup();
			CrossGroup(const std::list<ShCrossPr>& cross);

			// factory
			static ShCrossGroupPr create();
			static ShCrossGroupPr create(const std::list<ShCrossPr>& cross);

			// setters
			void set_merge_nodes(const bool merge_nodes = true);

			// getters
			bool get_merge_nodes()const;

			// modify cross section list
			arma::uword add_cross(const ShCrossPr &cross);
			arma::Row<arma::uword> add_cross(const std::list<ShCrossPr> &cross);
			const ShCrossPr& get_cross(const arma::uword index) const;
			std::list<ShCrossPr> get_cross() const;
			bool delete_cross(const arma::uword index);
			void reindex() override;
			arma::uword num_cross() const;

			// volume mesh
			virtual ShAreaPr create_area(const MeshSettings &stngs) const override;

			// get bounding box
			virtual arma::Mat<fltp>::fixed<2,2> get_bounding() const override;

			// vallidity check
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
