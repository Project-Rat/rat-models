// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_DATA_HH
#define MDL_DATA_HH

#include <armadillo> 
#include <memory>
#include <cassert>

// common header files
#include "rat/common/node.hh"

// mlfmm header files
#include "vtkobj.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Data> ShDataPr;
	typedef arma::field<ShDataPr> ShDataPrList;

 	// calculation object interface
	class Data: virtual public cmn::Node{
		// name for export
		protected:
			// output name
			std::string output_type_ = "calc";

			// time
			fltp time_;

		// methods
		public:
			// make destructor
			virtual ~Data(){};

			// set time 
			void set_time(const fltp time);
			fltp get_time() const;

			// interpolate
			virtual arma::Mat<fltp> interpolate(
				const arma::Mat<fltp> &values,
				const arma::Mat<fltp> &R,
				const bool use_parallel = true)const;

			// export vtk
			virtual ShVTKObjPr export_vtk() const = 0;

			// output name
			void set_output_type(const std::string &output_type);
			std::string get_output_type() const;
	};

}}

#endif
