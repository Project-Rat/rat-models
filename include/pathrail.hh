// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_RAIL_HH
#define MDL_PATH_RAIL_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "path.hh"
#include "frame.hh"
#include "transformations.hh"
#include "inputpath.hh"

// calculates three point guided rail

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathRail> ShPathRailPr;

	// cross section of coil
	class PathRail: public Path, public Transformations, public InputPath{
		// properties
		protected:
			// settings
			fltp distance_ = RAT_CONST(0.15); // [m] distance between wheels
			fltp spacing_ = RAT_CONST(0.15); // [m] spacing between tracks
			fltp long_offset_ = RAT_CONST(0.0); // [m] offset

		// methods
		public:
			// constructor
			PathRail();
			explicit PathRail(
				const ShPathPr &base, 
				const fltp spacing, 
				const fltp distance);

			// factory
			static ShPathRailPr create();
			static ShPathRailPr create(
				const ShPathPr &base, 
				const fltp spacing, 
				const fltp distance);

			// set properties
			void set_spacing(const fltp spacing);
			void set_distance(const fltp distance);
			void set_long_offset(const fltp distance);

			// get properties
			fltp get_spacing()const;
			fltp get_distance()const;
			fltp get_long_offset()const;

			// get frame
			virtual ShFramePr create_frame(
				const MeshSettings &stngs) const override;

			// tree structure
			void reindex() override;

			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
