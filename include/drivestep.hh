// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_DRIVE_STEP_HH
#define MDL_DRIVE_STEP_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "drive.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class DriveStep> ShDriveStepPr;
	typedef arma::field<ShDriveStepPr> ShDriveStepPrList;

	// circuit class
	class DriveStep: public Drive{
		// properties
		protected:
			// in time or in place
			bool is_spatial_ = true;

			// amplitude
			fltp amplitude_ = RAT_CONST(1.0);

			// scaling amplitude
			fltp tstart_ = RAT_CONST(1.0);

			// phase
			fltp tpulse_ = RAT_CONST(2.0);

		// methods
		public:
			// constructor
			DriveStep();
			DriveStep(
				const fltp amplitude, 
				const fltp tstart, 
				const fltp tpulse);

			// factory
			static ShDriveStepPr create();
			static ShDriveStepPr create(
				const fltp amplitude, 
				const fltp tstart, 
				const fltp tpulse);

			// set and get definition
			void set_amplitude(const fltp amplitude);
			void set_tstart(const fltp tstart);
			void set_tpulse(const fltp tpulse);
			
			// get scaling at given time and place
			fltp get_scaling(
				const fltp position,
				const fltp time = 0.0,
				const arma::uword derivative = 0)const override;
			
			// get inflection points
			arma::Col<fltp> get_inflection_points()const override;

			// apply scaling for the input settings
			virtual void rescale(const fltp scale_factor) override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
