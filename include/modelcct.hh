// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// description:
// consists of base class which contains general properties
// and two derived classes which use skew angle 
// or amplitude settings respectively

#ifndef MDL_MODEL_CCT_HH
#define MDL_MODEL_CCT_HH

#include <armadillo> 
#include <memory>

#include "modelcoilwrapper.hh"
#include "pathcct.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelCCTBase> ShModelCCTBasePr;

	// base class
	class ModelCCTBase: public ModelCoilWrapper{
		// properties
		protected:
			// harmonic settings
			bool is_skew_ = false; // skew harmonic instead of main harmonics
			arma::uword num_poles_ = 1; // 1: dipole, 2: quadrupole, 3: sextupole, 4: octupole etc
			
			// inner coil radius
			fltp radius_ = RAT_CONST(30e-3); // [m] inner radius

			// skew angle
			bool use_skew_angle_ = false; // use alpha instead of amplitude
			bool is_reverse_ = false; // reverse winding angle
			fltp alpha_ = RAT_CONST(60.0)*arma::Datum<fltp>::tau/360; // [rad] winding angle
			fltp amplitude_ = RAT_CONST(60e-3); // [m] winding amplitude

			// spacing between turns (winding pitch)
			fltp drib_ = RAT_CONST(0.5e-3); // [m] rib thickness on the midplane

			// number of turns (can be non-integer)
			fltp nt1_ = -10; // [#] number of turns
			fltp nt2_ = 10; // [#] number of turns

			// additional twisting along length of cable
			fltp twist_ = RAT_CONST(0.0); // [rad] apply periodic twist to cable

			// num layers
			bool const_skew_angle_ = false; // skew angle is constant between layers (instead of amplitude)
			arma::uword num_layers_ = 2; // [#] number of layers to generate
			fltp rho_increment_ = RAT_CONST(6e-3); // m spacing from one layer to the next

			// coil pack dimensions
			fltp cable_thickness_ = RAT_CONST(0.002); // [m] this becomes width of slot
			fltp cable_width_ = RAT_CONST(0.005); // [m] this becomes depth of slot

			// discretization
			arma::uword num_nodes_per_turn_ = 180; // [#] number of nodes each turn (determines longitudinal element size)

			// element size
			fltp element_size_ = RAT_CONST(2e-3); // [m] transverse element size

			// bending along length for making curved CCT
			bool bending_origin_ = false; // position coil such that center of bending is on origin
			bool use_radius_ = false; // use bending radius instead of arc length
			fltp bending_radius_ = RAT_CONST(0.0); // [m] radius of bending to center of coil
			fltp bending_arc_length_ = RAT_CONST(0.0); // [rad] arc length of bend magnet

			// first lead
			bool enable_lead1_ = false; // enable first current lead
			fltp leadness1_ = RAT_CONST(0.05); // [m] distance of control point to determine the shape of the lead
			fltp zlead1_ = RAT_CONST(0.1); // [m] length of current lead with respect to magnetic length of coil
			fltp theta_lead1_ = arma::Datum<fltp>::pi/4; // [rad] azymuthal position of lead-end

			// second lead
			bool enable_lead2_ = false; // enable second current lead
			fltp leadness2_ = RAT_CONST(0.05); // [m] distance of control point to determine the shape of the lead
			fltp zlead2_ = RAT_CONST(0.1); // [m] length of current lead with respect to magnetic length of coil
			fltp theta_lead2_ = arma::Datum<fltp>::pi/4; // [rad] azymuthal position of lead-end

		// methods
		public:
			// constructor
			ModelCCTBase();

			// set properties
			void set_is_skew(const bool is_skew = true);
			void set_is_reverse(const bool is_reverse = true);
			void set_bending_arc_length(const fltp bending_arc_length);
			void set_twist(const fltp twist);
			void set_drib(const fltp drib);
			void set_num_turns(const fltp num_turns);
			void set_nt1(const fltp nt1);
			void set_nt2(const fltp nt2);
			void set_radius(const fltp radius);
			void set_num_poles(const arma::uword num_poles);
			void set_num_nodes_per_turn(const arma::uword num_nodes_per_turn);
			void set_num_layers(const arma::uword num_layers);
			void set_rho_increment(const fltp rho_increment);
			void set_cable_thickness(const fltp cable_thickness);
			void set_cable_width(const fltp cable_width);
			void set_element_size(const fltp element_size);
			void set_enable_lead1(const bool enable_lead1 = true);
			void set_enable_lead2(const bool enable_lead2 = true);
			void set_leadness1(const fltp leadness1);
			void set_leadness2(const fltp leadness2);
			void set_zlead1(const fltp zlead1);
			void set_zlead2(const fltp zlead2);
			void set_theta_lead1(const fltp theta_lead1);
			void set_theta_lead2(const fltp theta_lead2);
			void set_bending_origin(const bool bending_origin = true);
			void set_use_skew_angle(const bool use_skew_angle);
			void set_skew_angle(const fltp alpha);
			void set_amplitude(const fltp amplitude);
			void set_const_skew_angle(const bool const_skew_angle = true);
			void set_use_radius(const bool use_radius = true);
			void set_bending_radius(const fltp bending_radius);

			// get properties
			bool get_is_skew()const;
			bool get_is_reverse() const;
			fltp get_bending_arc_length() const;
			fltp get_twist()const;
			fltp get_drib()const;
			fltp get_num_turns()const;
			fltp get_nt1()const;
			fltp get_nt2()const;
			fltp get_radius()const;
			arma::uword get_num_poles()const;
			arma::uword get_num_nodes_per_turn() const;
			arma::uword get_num_layers() const;
			fltp get_rho_increment()const;
			fltp get_cable_thickness()const;
			fltp get_cable_width()const;
			fltp get_element_size()const;
			bool get_enable_lead1()const;
			bool get_enable_lead2()const;
			fltp get_leadness1()const;
			fltp get_leadness2()const;
			fltp get_zlead1()const;
			fltp get_zlead2()const;
			fltp get_theta_lead1()const;
			fltp get_theta_lead2()const;
			bool get_bending_origin()const;
			bool get_use_skew_angle()const;
			fltp get_skew_angle()const;
			fltp get_amplitude()const;
			bool get_const_skew_angle()const;
			bool get_use_radius() const;
			fltp get_bending_radius()const;

			// get base and cross
			ShPathPr get_input_path() const override;
			ShCrossPr get_input_cross() const override;

			// input validity
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};	



	// these two classes are here for backwards compatibility a
	// and will be removed in the future to be replaced by
	// the base class: ModelCCTBase.

	// shared pointer definition
	typedef std::shared_ptr<class ModelCCT> ShModelCCTPr;

	// CCT coil with skew angle as input
	class ModelCCT: public ModelCCTBase{
		// methods
		public:
			// constructor
			ModelCCT();

			// factory methods
			static ShModelCCTPr create();

			// serialization
			static std::string get_type();
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

	// shared pointer definition
	typedef std::shared_ptr<class ModelCCTAmpl> ShModelCCTAmplPr;

	// CCT coil with skew angle as input
	class ModelCCTAmpl: public ModelCCT{
		// methods
		public:
			// constructor
			ModelCCTAmpl();

			// factory methods
			static ShModelCCTAmplPr create();

			// serialization
			static std::string get_type();
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};


}}

#endif
