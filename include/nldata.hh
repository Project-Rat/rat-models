// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef MDL_NL_DATA_HH
#define MDL_NL_DATA_HH

// only available for non-linear solver
#ifdef ENABLE_NL_SOLVER

// general header files
#include <armadillo> 
#include <memory>
#include <cassert>

// rat-model header files
#include "mgndata.hh"
#include "rat/nl/hbdata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class NLData> ShNLDataPr;
	typedef arma::field<ShNLDataPr> ShNLDataPrList;

	// mesh of a coil
	// this objects carries all properties 
	// required for the calculations
	class NLData: public MgnData{
		// properties
		private:
			// hb curve
			nl::ShHBDataPr hb_data_;

			// magnetization
			arma::Mat<fltp> M_;
			bool is_nodal_;

		// methods
		public:
			// default constructor
			NLData();
			NLData(const ShFramePr &frame, const ShAreaPr &area);

			// factory
			static ShNLDataPr create();
			static ShNLDataPr create(const ShFramePr &frame, const ShAreaPr &area);

			// setters
			void set_hb_data(const nl::ShHBDataPr& hb_data);
			
			// getters
			const nl::ShHBDataPr& get_hb_data()const;

			// export reluctivity
			virtual ShVTKObjPr export_vtk() const override;

			// set magnetization
			void set_magnetisation(const arma::Mat<fltp>&M, const bool is_nodal);

			// calculate magnetic energy
			// virtual fltp calculate_magnetic_energy()const override;

			// calculate magnetization in nodes
			virtual arma::Mat<fltp> calc_nodal_magnetization()const override;

			// relative permeability
			virtual arma::Row<fltp> calc_relative_permeability()const override;

	};

}}

#endif
#endif
