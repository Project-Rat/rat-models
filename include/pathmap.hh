// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_MAP_HH
#define MDL_PATH_MAP_HH

#include <armadillo> 
#include <cassert>
#include <memory>

#include "path.hh"
#include "frame.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathMap> ShPathMapPr;

	// connects two paths with a connector
	// this then becomes a new combined path
	class PathMap: public Path, public Transformations{
		// properties
		protected:
			// geometric parameters
			ShPathPr base_path_ = NULL;
			ShPathPr source_path_ = NULL;

			// source path vectors
			char source2long_ = 'z';
			char source2normal_ = 'x';

			// map base path length to an interval between 0 and 1
			bool normalize_length_ = false; 

			// when source path length exceeds (normalized) 
			// base path length map back to the beginning
			bool loop_around_ = false;

			// smooth interpolation, uses BCR interpolation
			// such that the base curve is smooth
			bool use_bcr_interp_ = true;
			arma::uword bcr_order_ = 5llu;

		// methods
		public:
			// constructor
			PathMap();
			PathMap(
				const ShPathPr &base_path, 
				const ShPathPr &source_path);

			// factory methods
			static ShPathMapPr create();
			static ShPathMapPr create(
				const ShPathPr &base_path, 
				const ShPathPr &source_path);
			
			// setters
			void set_base_path(const ShPathPr &prev_path);
			void set_source_path(const ShPathPr &next_path);
			void set_source2long(const char source2long);
			void set_source2normal(const char source2normal);
			void set_normalize_length(const bool normalize_length = true);
			void set_loop_around(const bool loop_around = true);
			void set_use_bcr_interp(const bool use_bcr_interp = true);
			void set_bcr_order(const arma::uword bcr_order);

			// getters
			char get_source2long()const;
			char get_source2normal()const;
			bool get_normalize_length()const;
			bool get_loop_around()const;
			bool get_use_bcr_interp()const;
			arma::uword get_bcr_order()const;

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// validity check
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
