// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_EMITTER_BEAM_HH
#define MDL_EMITTER_BEAM_HH

#include <armadillo> 
#include <memory>

#include "particle.hh"
#include "emitter.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class EmitterBeam> ShEmitterBeamPr;

	// emitter for accelerator beam
	class EmitterBeam: public Emitter{
		protected:
			// seed for random distribution
			arma::uword seed_ = 1001llu;

			// relativistic particle settings
			bool is_momentum_ = false;
			fltp beam_energy_ = RAT_CONST(0.0); // [GeV]
			fltp beam_momentum_ = RAT_CONST(0.0); // [GeV/C]
			fltp sigma_beam_energy_ = RAT_CONST(0.0); // [GeV] 
			fltp sigma_momentum_ = RAT_CONST(0.0); // [GeV/C] 
			fltp rest_mass_ = RAT_CONST(0.0); // [GeV/C^2]
			fltp charge_ = RAT_CONST(0.0); // elementary charge

			// number of particles spawned
			arma::uword num_particles_ = 1000llu;

			// normal xx' relationship
			fltp corr_xx_ = RAT_CONST(0.9); // correlation between position and horizontal angle
			fltp sigma_x_ = RAT_CONST(0.004); // standard deviation in horizontal position
			fltp sigma_xa_ = RAT_CONST(0.5)*2*arma::datum::pi/360; // standard deviation in horizontal angle
			
			// transverse yy' relationship
			fltp corr_yy_ = RAT_CONST(0.9); // correlation between position and vertical angle
			fltp sigma_y_ = RAT_CONST(0.004); // standard deviation in vertical position
			fltp sigma_ya_ = RAT_CONST(0.5)*2*arma::datum::pi/360; // standard deviation in vertical angle

			// track settings
			arma::uword lifetime_ = 501llu;
			arma::uword start_idx_ = 250llu;


			// this coordinate and orientation 
			// can also be adjusted using transformations instead
			// this is more compatible with the GUI
			// emitter location
			arma::Col<fltp>::fixed<3> R_{0,0,0};
			
			// emitter orientation
			arma::Col<fltp>::fixed<3> L_{0,1,0};
			arma::Col<fltp>::fixed<3> N_{1,0,0};
			arma::Col<fltp>::fixed<3> D_{0,0,1};


		// methods
		public:
			// constructor
			EmitterBeam();
			EmitterBeam(
				const fltp sigma_x, const fltp sigma_y,
				const fltp sigma_xa, const fltp sigma_ya,
				const fltp corr_xx, const fltp corr_yy);
				
			// factory
			static ShEmitterBeamPr create();
			static ShEmitterBeamPr create(
				const fltp sigma_x, const fltp sigma_y,
				const fltp sigma_xa, const fltp sigma_ya,
				const fltp corr_xx, const fltp corr_yy);

			// default particles
			void set_proton();
			void set_electron();

			// set relation ship for normal direction
			void set_xx(const fltp corr_xx, const fltp sigma_x, const fltp sigma_xa);
			void set_yy(const fltp corr_yy, const fltp sigma_y, const fltp sigma_ya);

			// set all dims at once
			void set_spawn_coord(
				const arma::Col<fltp>::fixed<3> &R, 
				const arma::Col<fltp>::fixed<3> &L, 
				const arma::Col<fltp>::fixed<3> &N, 
				const arma::Col<fltp>::fixed<3> &D);

			// setting of properties
			void set_seed(const unsigned int seed);
			void set_beam_energy(const fltp beam_energy);
			void set_rest_mass(const fltp rest_mass);
			void set_charge(const fltp charge);
			void set_lifetime(const arma::uword lifetime);
			void set_start_idx(const arma::uword start_idx);
			void set_num_particles(const arma::uword num_particles);
			void set_coord(
				const arma::Col<fltp>::fixed<3> &R);
			void set_longitudinal(
				const arma::Col<fltp>::fixed<3> &L);
			void set_normal(
				const arma::Col<fltp>::fixed<3> &N);
			void set_transverse(
				const arma::Col<fltp>::fixed<3> &D);
			void set_corelation_xx(const fltp corr_xx);
			void set_corelation_yy(const fltp corr_yy);
			void set_sigma_x(const fltp sigma_x);
			void set_sigma_y(const fltp sigma_y);
			void set_sigma_xa(const fltp sigma_xa);
			void set_sigma_ya(const fltp sigma_ya);
			void set_is_momentum(const bool is_momentum = true);
			void set_sigma_energy(const fltp sigma_beam_energy);
			void set_beam_momentum(const fltp beam_momentum);
			void set_sigma_momentum(const fltp beam_momentum);

			// getters
			unsigned int get_seed()const;
			arma::uword get_num_particles() const;
			fltp get_rest_mass() const;
			fltp get_charge()const;
			fltp get_beam_energy()const;
			arma::uword get_lifetime()const;
			const arma::Col<fltp>::fixed<3>& get_coord()const;
			const arma::Col<fltp>::fixed<3>& get_longitudinal()const;
			const arma::Col<fltp>::fixed<3>& get_normal()const;
			const arma::Col<fltp>::fixed<3>& get_transverse()const;
			fltp get_corelation_xx()const;
			fltp get_corelation_yy()const;
			fltp get_sigma_x()const;
			fltp get_sigma_y()const;
			fltp get_sigma_xa()const;
			fltp get_sigma_ya()const;
			arma::uword get_start_idx()const;
			bool get_is_momentum()const;
			fltp get_sigma_energy()const;
			fltp get_sigma_momentum()const;
			fltp get_beam_momentum()const;

			// normal distribution but correlated
			static arma::Mat<fltp> correlated_normdist(
				const fltp correlation, 
				const arma::uword num_cols);

			// particle
			arma::field<Particle> spawn_particles(const fltp time) const override;

			// create specific mesh
			virtual std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif