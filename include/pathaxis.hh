// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_AXIS_HH
#define MDL_PATH_AXIS_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "path.hh"
#include "frame.hh"
#include "pathgroup.hh"
#include "pathstraight.hh"
#include "transformations.hh"
#include "frame.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathAxis> ShPathAxisPr;

	// circular softway bend
	class PathAxis: public Path, public Transformations{
		// properties
		protected:
			// orientation
			char axis_ = 'z';
			char transverse_direction_ = 'x';

			// length
			fltp ell_ = RAT_CONST(0.5);

			// position
			arma::Col<fltp>::fixed<3> position_ = {RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0)};

			// element size
			fltp element_size_ = RAT_CONST(2e-3);

		// methods
		public:
			// constructor
			PathAxis();
			PathAxis(
				const char axis, 
				const char dir, 
				const fltp ell, 
				const arma::Col<fltp>::fixed<3> &Rc, 
				const fltp dl);
			PathAxis(
				const char axis, 
				const char dir, 
				const fltp ell, 
				const fltp x, 
				const fltp y, 
				const fltp z, 
				const fltp dl);
			
			// factory
			static ShPathAxisPr create();
			static ShPathAxisPr create(
				const char axis, 
				const char dir, 
				const fltp ell, 
				const arma::Col<fltp>::fixed<3> &Rc, 
				const fltp dl);
			static ShPathAxisPr create(
				const char axis, 
				const char dir, 
				const fltp ell, 
				const fltp x, 
				const fltp y, 
				const fltp z, 
				const fltp dl);

			// set properties
			void set_axis(const char axis);
			void set_transverse_direction(const char dir);
			void set_ell(const fltp ell);
			void set_position(const arma::Col<fltp>::fixed<3> &position);
			void set_element_size(const fltp element_size);

			// get properties
			char get_axis()const;
			char get_transverse_direction()const;
			fltp get_ell()const;
			arma::Col<fltp>::fixed<3> get_position()const;
			fltp get_element_size()const;

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
