// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CROSS_LINE_HH
#define MDL_CROSS_LINE_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <json/json.h>

#include "cross.hh"
#include "area.hh"
#include "path.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CrossLine> ShCrossLinePr;

	// cross section of coil
	class CrossLine: public Cross{
		// properties
		private:
			// coordinate
			arma::Row<fltp> u_{0,0}; // in n direction
			arma::Row<fltp> v_{-6e-3,6e-3}; // in d direction

			// thickness
			fltp thickness_ = 0.1e-3;

		// methods
		public:
			// constructors
			CrossLine();
			CrossLine(
				const arma::Row<fltp> &u,
				const arma::Row<fltp> &v,
				const fltp thickness);
			CrossLine(
				const fltp u1, const fltp v1,
				const fltp u2, const fltp v2,
				const fltp thickness, const fltp dl);

			// factory methods
			static ShCrossLinePr create();
			static ShCrossLinePr create(
				const arma::Row<fltp> &u,
				const arma::Row<fltp> &v,
				const fltp thickness);
			static ShCrossLinePr create(
				const fltp u1, const fltp v1,
				const fltp u2, const fltp v2,
				const fltp thickness, const fltp dl);

			// set properties
			void set_coord(
				const arma::Row<fltp> &u,
				const arma::Row<fltp> &v);
			void set_coord(
				const fltp u1, const fltp v1,
				const fltp u2, const fltp v2,
				const fltp dl);
			

			// setters
			void set_u(const arma::Row<fltp> &u);
			void set_v(const arma::Row<fltp> &v);
			void set_thickness(const fltp thickness);
			
			// getters
			const arma::Row<fltp>& get_u()const;
			const arma::Row<fltp>& get_v()const;
			fltp get_thickness()const;

			// get bounding box
			arma::Mat<fltp>::fixed<2,2> get_bounding() const override;

			// volume mesh
			virtual ShAreaPr create_area(const MeshSettings &stngs) const override;

			// surface mesh
			// virtual ShPerimeterPr create_perimeter() const override;

			// vallidity check 
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
