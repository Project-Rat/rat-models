// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_FIELDMAP_HH
#define MDL_FIELDMAP_HH

// general headers
#include <armadillo> 
#include <memory>
#include <iomanip>
#include <list>
#include <map>

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class FieldMap> ShFieldMapPr;

	// fieldmap interface
	class FieldMap{
		public:
			// virtual destructor
			virtual ~FieldMap(){};

			// check if a target point is inside the grid
			virtual arma::Row<arma::uword> is_inside(const arma::Mat<fltp>&Rt)const = 0;

			// calculate the fields at the target points
			virtual std::pair<arma::Mat<double>,arma::Mat<double> > calc_fields(
				const arma::Mat<double>& Rt, 
				const fltp time = RAT_CONST(0.0), 
				const cmn::ShLogPr& lg = cmn::NullLog::create())const = 0;
	};

}}

#endif
