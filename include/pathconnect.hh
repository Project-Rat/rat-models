// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_CONNECT_HH
#define MDL_PATH_CONNECT_HH

#include <armadillo> 
#include <cassert>
#include <memory>

#include "path.hh"
#include "frame.hh"
#include "pathbspline.hh" // cubic spline
#include "pathbezier.hh" // quintic spline with constant perimeter

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathConnect> ShPathConnectPr;

	// connects two paths with a connector
	// this then becomes a new combined path
	class PathConnect: public Path, public Transformations{
		// properties
		protected:
			// list of pointers to the path sections	
			std::map<arma::uword, ShPathPr> input_paths_;

			// strength of start and end points
			fltp str1_ = 0.01;
			fltp str2_ = 0.01; 
			fltp str3_ = 0.002; 
			fltp str4_ = 0.002; 

			// other settings
			fltp ell_trans1_ = 0.0; 
			fltp ell_trans2_ = 0.0; 
			fltp element_size_ = 2e-3;

			// use binormal vector instead of darboux vector
			bool use_binormal_ = false;

		// methods
		public:
			// constructor
			PathConnect();
			PathConnect(
				const ShPathPr &prev_path, 
				const ShPathPr &next_path, 
				const fltp str1, const fltp str2, 
				const fltp str3, const fltp str4, 
				const fltp ell_trans1, 
				const fltp ell_trans2, 
				const fltp element_size);

			// factory methods
			static ShPathConnectPr create();
			static ShPathConnectPr create(
				const ShPathPr &prev_path, 
				const ShPathPr &next_path,
				const fltp str1, const fltp str2, 
				const fltp str3, const fltp str4, 
				const fltp ell_trans1, 
				const fltp ell_trans2, 
				const fltp element_size);
			
			// function for adding a path to the path list
			arma::uword add_path(const ShPathPr &path);
			ShPathPr get_path(const arma::uword index) const;
			bool delete_path(const arma::uword index);
			void reindex() override;
			arma::uword get_num_paths() const;
			
			// setters
			void set_strengths(const fltp str1, const fltp str2, const fltp str3, const fltp str4);
			void set_str1(const fltp str1);
			void set_str2(const fltp str2);
			void set_str3(const fltp str3);
			void set_str4(const fltp str4);
			void set_ell_trans1(const fltp ell_trans1);
			void set_ell_trans2(const fltp ell_trans2);
			void set_element_size(const fltp element_size);
			void set_use_binormal(const bool use_binormal = true);

			// getters
			fltp get_str1()const;
			fltp get_str2()const;
			fltp get_str3()const;
			fltp get_str4()const;
			fltp get_ell_trans1()const;
			fltp get_ell_trans2()const;
			fltp get_element_size()const;
			bool get_use_binormal()const;
			
			// validity check
			bool is_valid(const bool enable_throws) const override;

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
