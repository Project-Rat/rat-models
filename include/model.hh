// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef MDL_MODEL_HH
#define MDL_MODEL_HH

// general headers
#include <armadillo> 
#include <memory>

// rat common headers
#include "rat/common/freecad.hh"
#include "rat/common/opera.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/node.hh"
#include "rat/common/log.hh"
#include "rat/common/stlfile.hh"

// materials headers
#include "rat/mat/conductor.hh"
#include "rat/mat/inputconductor.hh"

// rat-models headers
#include "transformations.hh"
#include "meshdata.hh"
#include "meshgen.hh"
#include "serializer.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Model> ShModelPr;
	typedef arma::field<ShModelPr> ShModelPrList;

	// template for any modeling object
	class Model: public mat::InputConductor, public Transformations, public MeshGen{
		// properties
		protected:
			// color in RGB values
			bool use_custom_color_ = false;
			arma::Col<float>::fixed<3> color_{0.119538f,0.219526f,0.23074f}; // this is the default color

		// methods
		public:
			// virtual destructor
			virtual ~Model(){};

			// setters
			void set_use_custom_color(const bool use_custom_color = true);
			void set_color(const arma::Col<float>::fixed<3> &color);
			void set_color(const arma::Col<float>::fixed<3> &color, const bool use_custom_color);

			// getters
			const arma::Col<float>::fixed<3>& get_color()const;
			bool get_use_custom_color()const;

			// exporting of stored geometry
			void export_freecad(const cmn::ShFreeCADPr &freecad) const; // export edges to freecad file
			void export_opera(const cmn::ShOperaPr &opera) const; // export edges to opera file
			void export_gmsh(const cmn::ShGmshFilePr &gmsh) const;
			void export_abaqus(const cmn::ShAbaqusFilePr &abaqus) const;
			void export_stl(const cmn::ShSTLFilePr &stlfile) const;

			// volume and surface area calculation (mainly for testing)
			fltp calc_total_volume(const fltp time = 0) const;
			fltp calc_total_surface_area(const fltp time = 0) const;

			// create specific mesh
			virtual std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override;

			// validity check
			virtual bool is_valid(const bool enable_throws) const override;

			// freecad export taking into account directory structure
			// requires static call to enable casting Model to splitable
			static void export_freecad(
				const ShModelPr& model, 
				const cmn::ShFreeCADPr &freecad, 
				const ShSerializerPr& slzr = Serializer::create(),
				const arma::uword depth = 0,
				const arma::uword max_depth = 100);

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif