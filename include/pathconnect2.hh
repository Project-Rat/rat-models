// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_CONNECT2_HH
#define MDL_PATH_CONNECT2_HH

#include <armadillo> 
#include <cassert>
#include <memory>

#include "rat/common/log.hh"

#include "path.hh"
#include "frame.hh"
#include "pathbezier.hh" // quintic spline with constant perimeter

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathConnect2> ShPathConnect2Pr;

	struct UserData{
		arma::uword iter;
		PathConnect2* stngs;
		ShFramePr prev_gen;
		ShFramePr next_gen;
		cmn::ShLogPr lg;
	};

	// connects two paths with a connector
	// this then becomes a new combined path
	class PathConnect2: public Path, public Transformations{
		// properties
		protected:
			// list of pointers to the path sections	
			std::map<arma::uword, ShPathPr> input_paths_;

			// order
			bool is_symmetric_ = false;
			bool enable_w_ = true;
			arma::uword order_ = 7;

			// flipping of input paths
			bool flip_first_ = false;
			bool flip_second_ = false;

			// use previous uvw sets as a start point
			bool use_previous_ = true;

			// length
			fltp ell_ = RAT_CONST(0.04);
			bool exact_length_ = false;

			// other settings
			fltp ell_trans1_ = RAT_CONST(0.0); 
			fltp ell_trans2_ = RAT_CONST(0.0); 

			// size of the elements
			fltp element_size_ = RAT_CONST(2e-3);

			// minimal bending radius
			fltp min_bending_radius_ = RAT_CONST(0.02);

			// properties of the cable
			fltp ft_ = 1.0; // resistance to torsion
			fltp fn_ = 1.0; // resistance to normal curvature
			fltp fg_ = 1e5; // resistance to geodesic curvature

			// edge regression
			fltp t1_ = -RAT_CONST(8e-3);
			fltp t2_ = RAT_CONST(8e-3);

			// tolerance
			fltp xtol_ = RAT_CONST(1e-3);

			// coordinates of optimized control points
			arma::Mat<fltp> uvw1_ = arma::Mat<fltp>(3,0);
			arma::Mat<fltp> uvw2_ = arma::Mat<fltp>(3,0);

			// use binormal vector instead of darboux vector
			bool use_binormal_ = false;

			// exponent for strain energy calculation
			fltp strain_energy_exponent_ = RAT_CONST(2.0);


		// methods
		public:
			// constructor
			PathConnect2();
			PathConnect2(
				const ShPathPr &prev_path, 
				const ShPathPr &next_path, 
				const fltp ell,
				const fltp ell_trans1, 
				const fltp ell_trans2, 
				const fltp element_size);

			// factory methods
			static ShPathConnect2Pr create();
			static ShPathConnect2Pr create(
				const ShPathPr &prev_path, 
				const ShPathPr &next_path,
				const fltp ell,
				const fltp ell_trans1, 
				const fltp ell_trans2, 
				const fltp element_size);
			
			// function for adding a path to the path list
			arma::uword add_path(const ShPathPr &path);
			ShPathPr get_path(const arma::uword index) const;
			bool delete_path(const arma::uword index);
			void reindex() override;
			arma::uword get_num_paths() const;
			
			// setters
			void set_order(const arma::uword order);
			void set_is_symmetric(const bool is_symmetric = true);
			void set_enable_w(const bool enable_z = true);
			void set_flip_first(const bool flip_first = true);
			void set_flip_second(const bool flip_second = true);
			void set_uvw1(const arma::Mat<fltp>&uvw1);
			void set_uvw2(const arma::Mat<fltp>&uvw2);
			void set_ell_trans1(const fltp ell_trans1);
			void set_ell_trans2(const fltp ell_trans2);
			void set_element_size(const fltp element_size);
			void set_ell(const fltp ell);
			void set_exact_length(const bool exact_length = true);
			void set_use_previous(const bool use_previous = true);
			void set_min_bending_radius(const fltp min_bending_radius);
			void set_t1(const fltp t1);
			void set_t2(const fltp t2);
			void set_xtol(const fltp xtol);
			void set_ft(const fltp ft);
			void set_fn(const fltp fn);
			void set_fg(const fltp fg);
			void set_use_binormal(const bool use_binormal = true);
			void set_strain_energy_exponent(const fltp strain_energy_exponent);

			// getters
			const arma::Mat<fltp>& get_uvw1()const;
			const arma::Mat<fltp>& get_uvw2()const;
			fltp get_ell_trans1()const;
			fltp get_ell_trans2()const;
			fltp get_element_size()const;
			arma::uword get_order()const;
			bool get_is_symmetric()const;
			bool get_enable_w()const;
			bool get_flip_first()const;
			bool get_flip_second()const;
			fltp get_ell()const;
			bool get_exact_length()const;
			bool get_use_previous()const;
			fltp get_min_bending_radius()const;
			fltp get_t1()const;
			fltp get_t2()const;
			fltp get_xtol()const;
			fltp get_ft()const;
			fltp get_fn()const;
			fltp get_fg()const;
			bool get_use_binormal()const;
			fltp get_strain_energy_exponent()const;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// get frame
			ShPathBezierPr create_bezier(
				const ShFramePr& prev_gen, 
				const ShFramePr& next_gen)const;
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// optimize control points
			static double objective_fun(unsigned n, const double* x, double* grad, void* f_stngs);
			static void length_constraint_fun(unsigned m, double *result, unsigned n, const double* x, double* grad, void* f_stngs);
			static void curvature_constraint_fun(unsigned m, double *result, unsigned n, const double* x, double* grad, void* f_stngs);
			static void edge_regression_constraint_fun(unsigned m, double *result, unsigned n, const double* x, double* grad, void* f_stngs);
			arma::uword get_num_vars()const;
			std::vector<double> get_x0()const;
			std::vector<double> get_lb()const;
			std::vector<double> get_ub()const;
			void set_uvw(unsigned n, const double* x);
			void optimize_control_points(const cmn::ShLogPr& lg = cmn::NullLog::create());

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
