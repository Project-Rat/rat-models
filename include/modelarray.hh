// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_ARRAY_HH
#define MDL_MODEL_ARRAY_HH

#include <armadillo> 
#include <memory>

#include "modelgroup.hh"
#include "transformations.hh"
#include "meshdata.hh"
#include "nameable.hh"
#include "serializer.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelArray> ShModelArrayPr;

	// applies toroid symmetry to the input coil
	class ModelArray: public ModelGroup{
		// set coils
		protected:
			// centering
			bool centered_ = false;
			bool alternate_ = false;

			// gridsize
			arma::uword num_x_ = 1;
			arma::uword num_y_ = 1;
			arma::uword num_z_ = 1;

			// spacing
			fltp dx_ = 0;
			fltp dy_ = 0;
			fltp dz_ = 0;

		// methods
		public:
			// constructor
			ModelArray();
			explicit ModelArray(
				const ShModelPr &base_model);
			ModelArray(
				const ShModelPr &base_model,
				const arma::uword num_x, 
				const arma::uword num_y, 
				const arma::uword num_z,
				const fltp dx, 
				const fltp dy, 
				const fltp dz, 
				const bool centered = false);

			// factory method
			static ShModelArrayPr create();
			static ShModelArrayPr create(
				const ShModelPr &base_model);
			static ShModelArrayPr create(
				const ShModelPr &base_model,
				const arma::uword num_x, 
				const arma::uword num_y, 
				const arma::uword num_z,
				const fltp dx, 
				const fltp dy, 
				const fltp dz, 
				const bool centered = false);

			// set grid size and grid spacing
			void set_centered(const bool centered = true);
			void set_alternate(const bool alternate = true);
			void set_grid_size(
				const arma::uword num_x, 
				const arma::uword num_y, 
				const arma::uword num_z);
			void set_grid_spacing(
				const fltp dx, 
				const fltp dy, 
				const fltp dz);
			void set_num_x(const arma::uword num_x);
			void set_num_y(const arma::uword num_y);
			void set_num_z(const arma::uword num_z);
			void set_dx(const fltp dx);
			void set_dy(const fltp dy);
			void set_dz(const fltp dz);

			// get grid size and grid spacing
			bool get_centered()const;
			bool get_alternate() const;
			arma::uword get_num_x()const;
			arma::uword get_num_y()const;
			arma::uword get_num_z()const;
			fltp get_dx()const;
			fltp get_dy()const;
			fltp get_dz()const;

			// splitting the modelgroup
			std::list<ShModelPr> split(
				const ShSerializerPr &slzr = Serializer::create()) const override;

			// create specific mesh
			virtual std::list<ShMeshDataPr> create_meshes_core(
				const std::list<arma::uword> &trace, 
				const MeshSettings &stngs = MeshSettings()) const override;

			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
