// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_TRACK_DATA_HH
#define MDL_TRACK_DATA_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/log.hh"
#include "rat/common/rungekutta.hh"

#include "emitter.hh"
#include "particle.hh"
#include "vtkunstr.hh"
#include "meshdata.hh"
#include "data.hh"
#include "maxwellfieldmap.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class TrackData> ShTrackDataPr;

	// template for coil
	class TrackData: public Data{
		// properties
		protected:
			// integration method (see trackdata)
			cmn::RungeKutta::IntegrationMethod integration_method_ = 
				cmn::RungeKutta::IntegrationMethod::RUNGE_KUTTA_FEHLBERG45;

			// track data
			arma::field<Particle> particles_;

			// circuits
			std::list<ShCircuitPr> circuits_;

			// enable dynamic tracking
			bool enable_dynamic_;

			// use start time instead of tracking time
			bool use_start_time_;

		// methods
		public:
			// constructor
			TrackData();
			TrackData(
				const ShMeshDataPr &tracking_mesh, 
				const ShEmitterPr &emitter);

			// factory methods
			static ShTrackDataPr create();
			static ShTrackDataPr create(
				const ShMeshDataPr &tracking_mesh, 
				const ShEmitterPr &emitter);
			
			// set the integration method
			void set_integration_method(
				const cmn::RungeKutta::IntegrationMethod integration_method);

			// setters
			void set_stepsize(const fltp step_size);
			void set_circuits(const std::list<ShCircuitPr>& circuits);
			void set_enable_dynamic(const bool enable_dynamic = true);

			// getters
			const std::list<ShCircuitPr>& get_circuits()const;
			bool get_use_start_time()const;
			bool get_enable_dynamic()const;

			// field interpolation
			static std::pair<arma::Mat<fltp>,arma::Mat<fltp> > calc_field(
				const fltp time,
				const arma::Mat<fltp> &RV,
				const ShFieldMapPr &fieldmap,
				const cmn::ShLogPr& lg);

			// calculate tracks
			void create_flux_line_tracks(
				const ShFieldMapPr &fieldmap,
				const std::list<ShEmitterPr> &emitters,
				const fltp max_step_size = 0.05,
				const fltp rabstol = 1e-4, const fltp rreltol = 1e-4,
				const rat::cmn::ShLogPr &lg = rat::cmn::NullLog::create());

			void create_electric_field_tracks(
				const ShFieldMapPr &fieldmap,
				const std::list<ShEmitterPr> &emitters,
				const fltp max_step_size = 0.05,
				const fltp rabstol = 1e-4, const fltp rreltol = 1e-4,
				const rat::cmn::ShLogPr &lg = rat::cmn::NullLog::create());

			void create_newton_tracks(
				const ShFieldMapPr &fieldmap,
				const std::list<ShEmitterPr> &emitters,
				const fltp max_step_size = 0.05,
				const fltp rabstol = 1e-4, const fltp rreltol = 1e-4,
				const fltp pabstol = 1e-6, const fltp preltol = 1e-6,
				const rat::cmn::ShLogPr &lg = rat::cmn::NullLog::create());

			void create_relativistic_tracks(
				const ShFieldMapPr &fieldmap,
				const std::list<ShEmitterPr> &emitters,
				const fltp max_step_size = 0.05,
				const fltp rabstol = 1e-4, const fltp rreltol = 1e-4,
				const fltp pabstol = 1e-6, const fltp preltol = 1e-6,
				const rat::cmn::ShLogPr &lg = rat::cmn::NullLog::create());

			// export to vtk
			ShVTKObjPr export_vtk() const override;
			void export_csv(const boost::filesystem::path& fpath)const;

			// get particle
			const Particle& get_particle(const arma::uword idx) const;
			const arma::field<Particle>& get_particles() const;
			arma::uword get_num_particles()const;

			// map to frame
			// void map2frame(const ShFramePr& frame)const;
	};
}}

#endif
