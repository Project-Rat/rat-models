// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_GROUP_HH
#define MDL_MODEL_GROUP_HH

#include <armadillo> 
#include <memory>

#include "model.hh"
#include "meshdata.hh"
#include "splitable.hh"
#include "serializer.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelGroup> ShModelGroupPr;

	// a collection of coils
	class ModelGroup: public Model, public Splitable{
		// set coils
		protected:
			// lock
			bool lock_ = false;

			// input coils
			std::map<arma::uword, ShModelPr> models_;

			// apply transformations
			bool apply_trans_ = true;

			// set this groups name as partname
			bool is_part_ = false;  

		// methods
		public:
			// constructor
			ModelGroup();
			explicit ModelGroup(const ShModelPr &model);
			explicit ModelGroup(const std::list<ShModelPr> &models);

			// factory methods
			static ShModelGroupPr create();
			static ShModelGroupPr create(const ShModelPr &model);
			static ShModelGroupPr create(const std::list<ShModelPr> &models);

			// splitting the modelgroup
			virtual std::list<ShModelPr> split(
				const ShSerializerPr &slzr = Serializer::create()) const override;

			// create meshes and edges of stored coils untransformed
			virtual std::list<ShMeshDataPr> create_meshes_core(
				const std::list<arma::uword> &trace, 
				const MeshSettings &stngs) const;
			
			// create specific mesh
			virtual std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override;

			// set part
			void set_is_part(const bool is_part = true);

			// getters
			bool get_is_part()const;

			// model structure
			arma::uword add_model(const ShModelPr &model);
			arma::Row<arma::uword> add_models(const std::list<ShModelPr> &models);
			void reverse_models();
			const ShModelPr& get_model(const arma::uword index) const;
			std::list<ShModelPr> get_models() const;
			bool delete_model(const arma::uword index);
			void reindex() override;
			arma::uword num_models() const;

			// validity check
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
