// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_COIL_HH
#define MDL_MODEL_COIL_HH

#include <armadillo> 
#include <memory>

#include "modelmesh.hh"
#include "drive.hh"
#include "drivedc.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelCoil> ShModelCoilPr;

	// current carying coil extruded with
	// a base path and a cross section
	class ModelCoil: public ModelMesh{
		// properties
		protected:
			// winding geometry
			fltp number_turns_ = RAT_CONST(1.0);

			// number of gauss points
			arma::sword num_gauss_ = 2;

			// softening factor
			fltp softening_ = RAT_CONST(1.4);

			// current density switch
			bool use_current_density_ = false;

			// design operation current
			fltp operating_current_ = RAT_CONST(0.0);

			// operating current density
			fltp operating_current_density_ = RAT_CONST(0.0);

			// current drive
			ShDrivePr current_drive_ = DriveDC::create();

		// methods
		public:
			// constructor
			ModelCoil();
			ModelCoil(
				const ShPathPr &input_path, 
				const ShCrossPr &input_cross, 
				const mat::ShConductorPr &conductor = NULL);

			// factory methods
			static ShModelCoilPr create();
			static ShModelCoilPr create(
				const ShPathPr &input_path, 
				const ShCrossPr &input_cross, 
				const mat::ShConductorPr &conductor = NULL);


			// setters
			void set_use_current_density(const bool use_current_density = true);
			void set_softening(const fltp softening); // softening factor
			// void set_use_volume_elements(const bool use_volume_elements);
			void set_num_gauss(const arma::sword num_gauss);
			void set_operating_current(const fltp operating_current);
			void set_operating_current_density(const fltp operating_current_density);
			void set_current_drive(const ShDrivePr &current_drive);
			void set_number_turns(const fltp number_turns); // alias for compatibility
			void set_num_turns(const fltp num_turns);

			// getters
			bool get_use_current_density()const;
			fltp get_number_turns() const;
			fltp get_num_turns() const;
			fltp get_softening() const;
			ShDrivePr get_current_drive() const;
			arma::sword get_num_gauss() const;
			fltp get_operating_current() const;
			fltp get_operating_current_density() const;

			// create data
			ShMeshDataPr create_data()const override;

			// create specific mesh
			virtual std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override;

			// validity check
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
