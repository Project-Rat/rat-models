// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef MDL_HB_CURVE_FILE_HH
#define MDL_HB_CURVE_FILE_HH

// only available for non-linear solver
#ifdef ENABLE_NL_SOLVER

// general headers
#include <armadillo> 
#include <cassert>
#include <algorithm>
#include <memory>

// common headers
#include "rat/common/node.hh"

// nonlinear solver headers
#include "hbcurve.hh"

// code specific to Raccoon
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class HBCurveFile> ShHBCurveFilePr;
	typedef arma::field<ShHBCurveFilePr> ShHBCurveFilePrList;

	// hb curve containing data table
	class HBCurveFile: public HBCurve{
		// properties
		protected:
			// path to the file
			boost::filesystem::path hb_file_;

			// filling fraction
			fltp ff_ = RAT_CONST(1.0);

		// methods
		public:
			// constructor
			HBCurveFile();
			explicit HBCurveFile(const boost::filesystem::path& hb_file);

			// factories
			static ShHBCurveFilePr create();
			static ShHBCurveFilePr create(const boost::filesystem::path& hb_file);
				
			// set properties
			void set_hb_file(const boost::filesystem::path &file_name);
			void set_filling_fraction(const fltp ff);

			// get properties
			const boost::filesystem::path& get_hb_file()const;
			fltp get_filling_fraction()const;
			arma::Mat<fltp> get_table()const;

			// create hb data
			virtual nl::ShHBDataPr create_hb_data()const override;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
#endif