// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MGN_DATA_HH
#define MDL_MGN_DATA_HH

#include <armadillo> 
#include <memory>
#include <cassert>

// mlfmm header files
#include "rat/mlfmm/boundmesh.hh"
#include "rat/mlfmm/mgncharges.hh"
#include "rat/mlfmm/mgnchargesurface.hh"

//#include "coilsurface.hh"
#include "meshdata.hh"


// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class MgnData> ShMgnDataPr;
	typedef arma::field<ShMgnDataPr> ShMgnDataPrList;

	// magnetized mesh
	class MgnData: public MeshData{
		// methods
		public:
			// calculate magnetization in nodes
			virtual arma::Mat<fltp> calc_nodal_magnetization()const = 0;

			// create sources
			virtual fmm::ShMgnChargeSurfacePr create_surface_charges()const;
			virtual fmm::ShMgnChargesPr create_surface_point_charges()const;
			virtual fmm::ShMgnChargesPr create_vol_charges(const bool drop_zero_charge = true)const;
			virtual fmm::ShCurrentSourcesPr create_bnd_point_surf_currents()const;
			virtual fmm::ShSourcesPr create_bnd_surf_currents()const;
			virtual fmm::ShCurrentSourcesPr create_bnd_vol_currents(const bool drop_zero_current = true)const;
			virtual fmm::ShInterpPr create_magnetization_interp()const;

			// calculate integrated Lorentz forces
			virtual arma::Col<fltp>::fixed<3> calc_lorentz_force() const override;

			// set magnetization to self
			void set_self_magnetisation();

			// setup targets
			virtual void setup_targets() override;
			virtual arma::Mat<fltp> get_field(const char field_type)const override;
			
			// get specific fields
			arma::Mat<fltp> get_field_gp(const char field_type)const;
			arma::Mat<fltp> get_field_gps(const char field_type)const;
	};

}}

#endif
