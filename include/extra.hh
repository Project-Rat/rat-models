// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef MDL_EXTRA_HH
#define MDL_EXTRA_HH

// general headers
#include <armadillo> 

// code specific to Rat
namespace rat{namespace mdl{

	// serialization code for distmesh models
	class Extra{
		public:
			// function for extracting objects of a certian type from a field
			template<typename T1, typename T2> static arma::field<std::shared_ptr<T1> > filter(const arma::field<std::shared_ptr<T2> > &src);
			template<typename T1, typename T2> static std::list<std::shared_ptr<T1> > filter(const std::list<std::shared_ptr<T2> > &src);

			// conversions to armadillo field
			template<typename T> static arma::field<T> list2field(const std::list<T> &list);
			template<typename T> static arma::field<T> map2field(const std::map<arma::uword, T> &map);

			// function for combining field
			template<typename T> static arma::field<T> field2mat(const arma::field<arma::field<T> > &fld2);

			// join two fields horizontally
			template<typename T> static arma::field<T> join_horiz(const arma::field<T> &fld1, const arma::field<T> &fld2);
			template<typename T> static arma::field<T> join_vert(const arma::field<T> &fld1, const arma::field<T> &fld2);
	};

	// method for extracting objects of specific type
	// from a shared pointer list
	template<typename T1, typename T2> 
		arma::field<std::shared_ptr<T1> > Extra::filter(
		const arma::field<std::shared_ptr<T2> > &src){

		// allocate
		arma::field<std::shared_ptr<T1> > filtered(src.n_elem,1);

		// check for current sources
		arma::uword cnt = 0;
		// arma::Row<arma::uword> remain(src.n_elem,arma::fill::ones);
		for(arma::uword i=0;i<src.n_elem;i++){
			std::shared_ptr<T1> flt = std::dynamic_pointer_cast<T1>(src(i));
			if(flt!=NULL)filtered(cnt++) = flt;
		}

		// return targets
		if(cnt>0)return filtered.rows(0,cnt-1);
		else return arma::field<std::shared_ptr<T1> >(0);
	}

	// method for extracting objects of specific type
	// from a shared pointer list
	template<typename T1, typename T2> 
		std::list<std::shared_ptr<T1> > Extra::filter(
		const std::list<std::shared_ptr<T2> > &src){

		// allocate
		std::list<std::shared_ptr<T1> > filtered;

		// check for current sources
		for(auto it = src.begin();it!=src.end();it++){
			std::shared_ptr<T1> flt = std::dynamic_pointer_cast<T1>(*it);
			if(flt!=NULL)filtered.push_back(flt);
		}

		// return targets
		return filtered;
	}

	// merging field arrays containing field arrays
	template<typename T> arma::field<T> Extra::field2mat(
		const arma::field<arma::field<T> > &fld){
		// get sizes of all submatrices
		arma::Mat<arma::uword> num_rows(fld.n_rows,fld.n_cols);
		arma::Mat<arma::uword> num_cols(fld.n_rows,fld.n_cols);
		for(arma::uword i=0;i<fld.n_rows;i++){
			for(arma::uword j=0;j<fld.n_cols;j++){
				num_rows(i,j) = fld(i,j).n_rows;
				num_cols(i,j) = fld(i,j).n_cols;
			}
		}

		// check if submatrices fit
		arma::Col<arma::uword> num_rows_first_col = num_rows.col(0);
		arma::Row<arma::uword> num_cols_first_row = num_cols.row(0);

		// check if matrix fits
		assert(arma::all(arma::all(num_rows_first_col-num_rows.each_col()==0)));
		assert(arma::all(arma::all(num_cols_first_row-num_cols.each_row()==0)));

		// allocate output matrix
		arma::field<T> mt(arma::sum(num_rows_first_col),arma::sum(num_cols_first_row));

		// fill output matrix
		// walk over rows
		arma::uword row_start = 0;
		for(arma::uword i=0;i<fld.n_rows;i++){
			// walk over columns
			arma::uword col_start = 0;
			for(arma::uword j=0;j<fld.n_cols;j++){
				// walk over source rows
				for(arma::uword k=0;k<num_rows(i,j);k++){
					// walk over source columns
					for(arma::uword l=0;l<num_cols(i,j);l++){
						mt(row_start+k, col_start+l) = fld(i,j)(k,l);
					}
				}

				// increment column index
				col_start += num_cols_first_row(j);
			}

			// increment row index
			row_start += num_rows_first_col(i);
		}

		// return output matrix
		return mt;
	}

	// join field arrays of arbitrary type
	template<typename T> arma::field<T> Extra::join_horiz(
		const arma::field<T> &fld1, const arma::field<T> &fld2){
		// count rows and columns
		const arma::uword num_rows = fld1.n_rows; assert(fld2.n_rows==num_rows);
		const arma::uword num_cols = fld1.n_cols + fld2.n_cols;

		// allocate
		arma::field<T> mt(num_rows,num_cols);

		// copy data
		for(arma::uword i=0;i<num_rows;i++){
			for(arma::uword j=0;j<fld1.n_cols;j++){
				mt(i,j) = fld1(i,j);
			}
			for(arma::uword j=0;j<fld2.n_cols;j++){
				mt(i,j+fld1.n_cols) = fld2(i,j);
			}
		}

		// return combined field arrays
		return mt;
	}

	// join field arrays of arbitrary type vertically
	template<typename T> arma::field<T> Extra::join_vert(
		const arma::field<T> &fld1, const arma::field<T> &fld2){
		// count rows and columns
		const arma::uword num_cols = fld1.n_cols; assert(fld2.n_cols==num_cols);
		const arma::uword num_rows = fld1.n_rows + fld2.n_rows;

		// allocate
		arma::field<T> mt(num_rows,num_cols);

		// copy data
		for(arma::uword j=0;j<num_cols;j++){
			for(arma::uword i=0;i<fld1.n_rows;i++){
				mt(i,j) = fld1(i,j);
			}
			for(arma::uword i=0;i<fld2.n_rows;i++){
				mt(i+fld1.n_rows,j) = fld2(i,j);
			}
		}

		// return combined field arrays
		return mt;
	} 

	// list to field
	template<typename T> arma::field<T> Extra::list2field(const std::list<T> &list){
		arma::field<T> fld(list.size()); arma::uword cnt = 0;
		for(auto it=list.begin();it!=list.end();it++)fld(cnt++) = (*it);
		return fld;
	}

	// list to field
	template<typename T> arma::field<T> Extra::map2field(const std::map<arma::uword, T> &map){
		arma::field<T> fld(map.size()); arma::uword cnt = 0;
		for(auto it=map.begin();it!=map.end();it++)fld(cnt++) = (*it).second;
		return fld;
	}

}}

#endif