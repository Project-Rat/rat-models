// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALC_POINTS_HH
#define MDL_CALC_POINTS_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "calcleaf.hh"
#include "meshdata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcPoints> ShCalcPointsPr;

	// calculate field on line defined 
	// by start and end point coordinates
	class CalcPoints: public CalcLeaf{
		// properties
		protected:
			// enable visibility
			bool visibility_ = true;

			// number of steps in line
			arma::Mat<fltp> Rt_ = arma::Col<fltp>::fixed<3>{0,0,0};

		// methods
		public:
			// constructor
			CalcPoints();
			explicit CalcPoints(const ShModelPr &model);
			CalcPoints(const ShModelPr &model, const arma::Mat<fltp> &Rt);
			
			// factory methods
			static ShCalcPointsPr create();
			static ShCalcPointsPr create(const ShModelPr &model);
			static ShCalcPointsPr create(const ShModelPr &model, const arma::Mat<fltp> &Rt);

			// enable mesh
			bool get_visibility() const;
			void set_visibility(const bool visibility);

			// set/get coordinates
			void set_coords(const arma::Mat<fltp> &Rt);
			arma::Mat<fltp> get_coords() const;

			// calculate with inductance data output
			ShMeshDataPr calculate_points(
				const fltp time = RAT_CONST(0.0), 
				const cmn::ShLogPr &lg = cmn::NullLog::create(),
				const ShSolverCachePr& cache = NULL);

			// calculation function
			virtual std::list<ShDataPr> calculate(
				const fltp time, 
				const cmn::ShLogPr &lg = cmn::NullLog::create(),
				const ShSolverCachePr& cache = NULL) override;
			
			// creation of calculation data objects
			std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override; // create a mesh

			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
