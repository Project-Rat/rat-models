// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALC_POLAR_GRID_HH
#define MDL_CALC_POLAR_GRID_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "calcleaf.hh"
#include "griddata.hh"
#include "inputpath.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcPolarGrid> ShCalcPolarGridPr;

	// calculate field on line defined 
	// by start and end point coordinates
	class CalcPolarGrid: public CalcLeaf{
		// properties
		protected:
			// position 
			arma::Col<fltp>::fixed<3> offset_{RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0)};

			// dimensions
			char axis_ = 'z';
			fltp rin_ = RAT_CONST(0.0);
			fltp rout_ = RAT_CONST(0.2);
			fltp theta1_ = RAT_CONST(0.0);
			fltp theta2_ = arma::Datum<fltp>::tau;
			fltp zlow_ = -RAT_CONST(0.2);
			fltp zhigh_ = RAT_CONST(0.2);

			// number of points
			arma::uword num_rad_ = 50llu;
			arma::uword num_theta_ = 180llu;
			arma::uword num_axial_ = 100llu;

			// integration
			bool integrate_theta_ = false;

			// enable visibility
			bool visibility_ = true;

		// methods
		public:
			// constructor
			CalcPolarGrid();
			explicit CalcPolarGrid(
				const ShModelPr &model);
			CalcPolarGrid(
				const ShModelPr &model,const char axis,
				const fltp rin, const fltp rout, const arma::uword num_rad,
				const fltp theta1, const fltp theta2, const arma::uword num_theta,
				const fltp zlow, const fltp zhigh, const arma::uword num_axial,
				const arma::Col<fltp>::fixed<3> offset = {0,0,0});

			// factory methods
			static ShCalcPolarGridPr create();
			static ShCalcPolarGridPr create(
				const ShModelPr &model);
			static ShCalcPolarGridPr create(
				const ShModelPr &model,const char axis,
				const fltp rin, const fltp rout, const arma::uword num_rad,
				const fltp theta1, const fltp theta2, const arma::uword num_theta,
				const fltp zlow, const fltp zhigh, const arma::uword num_axial,
				const arma::Col<fltp>::fixed<3> offset = {0,0,0});

			// setters
			void set_integrate_theta(const bool integrate_theta);
			void set_offset(const arma::Col<fltp>::fixed<3> &offset);
			void set_axis(const char axis);
			void set_rin(const fltp rin);
			void set_rout(const fltp rout);
			void set_theta1(const fltp theta1);
			void set_theta2(const fltp theta2);
			void set_zlow(const fltp zlow);
			void set_zhigh(const fltp zhigh);
			void set_num_rad(const arma::uword num_rad);
			void set_num_theta(const arma::uword num_theta);
			void set_num_axial(const arma::uword num_axial);
			void set_visibility(const bool visibility);

			// getters
			bool get_integrate_theta()const;
			arma::Col<fltp>::fixed<3> get_offset()const;
			char get_axis()const;
			fltp get_rin()const;
			fltp get_rout()const;
			fltp get_theta1()const;
			fltp get_theta2()const;
			fltp get_zlow()const;
			fltp get_zhigh()const;
			arma::uword get_num_rad()const;
			arma::uword get_num_theta()const;
			arma::uword get_num_axial()const;
			bool get_visibility()const;

			// calculate with inductance data output
			ShGridDataPr calculate_grid(
				const fltp time = RAT_CONST(0.0), 
				const cmn::ShLogPr &lg = cmn::NullLog::create(),
				const ShSolverCachePr& cache = NULL);

			// calculation function
			virtual std::list<ShDataPr> calculate(
				const fltp time, 
				const cmn::ShLogPr &lg = cmn::NullLog::create(),
				const ShSolverCachePr& cache = NULL) override;

			// creation of calculation data objects
			std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override; // create a mesh

			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
