// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_CCT_TABLE_HH
#define MDL_PATH_CCT_TABLE_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <memory>
#include <cmath>

// models headers
#include "transformations.hh"
#include "path.hh"
#include "frame.hh"
#include "drive.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathCCTTable> ShPathCCTTablePr;

	// cross section of coil
	class PathCCTTable: public Path, public Transformations{
		// properties
		private:
			// table file
			boost::filesystem::path table_file_;

			// number of repeats
			arma::uword num_repeat_ = 1;

			// old field like behaviour
			bool field_style_ = false;

			// switches
			bool normalize_length_ = false;
			bool use_omega_normal_ = false;

			// use angle instead of amplitude
			bool use_angle_ = true;

			// barycentric rational interpolation
			arma::uword bcr_order_ = 3;
			bool use_bcr_interp_ = false;

			// layers
			arma::uword num_layers_ = 1;
			fltp rho_increment_ = RAT_CONST(6e-3);

			// elements
			arma::uword num_nodes_per_turn_ = 120;

			// use frenet-serret equations analytically
			bool use_frenet_serret_;

		// methods
		public:
			// constructor
			PathCCTTable();
			PathCCTTable(
				const boost::filesystem::path& table_file);

			// factory methods
			static ShPathCCTTablePr create();
			static ShPathCCTTablePr create(
				const boost::filesystem::path& table_file);

			// setters
			void set_table_file(const boost::filesystem::path& table_file);
			void set_normalize_length(const bool normalize_length = true);
			void set_use_omega_normal(const bool use_omega_normal = true);
			void set_num_layers(const arma::uword num_layers);
			void set_rho_increment(const fltp rho_increment);
			void set_num_nodes_per_turn(const arma::uword num_nodes_per_turn);
			void set_use_bcr_interp(const bool use_bcr_interp = true);
			void set_bcr_order(const arma::uword bcr_order);
			void set_num_repeat(const arma::uword num_repeat);
			void set_use_angle(const bool use_angle = true);
			void set_use_frenet_serret(const bool use_frenet_serret = true);
			void set_field_style(const bool field_style = true);

			// getters
			const boost::filesystem::path& get_table_file()const;
			bool get_normalize_length()const;
			bool get_use_omega_normal()const;
			arma::uword get_num_layers()const;
			fltp get_rho_increment()const;
			arma::uword get_num_nodes_per_turn()const;
			bool get_use_bcr_interp()const;
			arma::uword get_bcr_order()const;
			arma::uword get_num_repeat()const;
			bool get_use_angle()const;
			bool get_use_frenet_serret()const;
			bool get_field_style()const;

			// drive
			ShDrivePr create_interp_drive(
				const arma::Col<fltp> &turn, 
				const arma::Col<fltp> &value)const;

			// get frame
			virtual ShFramePr create_frame(
				const MeshSettings &stngs) const override;

			// vallidity check
			bool is_valid(
				const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
