// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_SUB_HH
#define MDL_PATH_SUB_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "path.hh"
#include "frame.hh"
#include "transformations.hh"
#include "inputpath.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathSub> ShPathSubPr;

	// cross section of coil
	class PathSub: public Path, public Transformations, public InputPath{
		// properties
		protected:		
			// settings
			arma::uword idx1_ = 0;
			arma::uword idx2_ = 0;

		// methods
		public:
			// constructor
			PathSub();
			PathSub(
				const ShPathPr& base, 
				const arma::uword idx1, 
				const arma::uword idx2);

			// factory
			static ShPathSubPr create();
			static ShPathSubPr create(
				const ShPathPr& base, 
				const arma::uword idx1, 
				const arma::uword idx2);

			// set properties
			void set_idx(const arma::uword idx1, const arma::uword idx2);
			void set_idx1(const arma::uword idx1);
			void set_idx2(const arma::uword idx2);

			// get properties
			arma::uword get_idx1()const;
			arma::uword get_idx2()const;

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// validity check
			bool is_valid(const bool enable_throws)const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
