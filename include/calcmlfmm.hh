// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALC_MLFMM_HH
#define MDL_CALC_MLFMM_HH

// general headers
#include <armadillo> 
#include <memory>

// multipole method headers
#include "rat/mlfmm/mlfmm.hh"
#include "rat/mlfmm/multisources.hh"
#include "rat/mlfmm/multitargets2.hh"
#include "rat/mlfmm/settings.hh"

// headers
#include "calcleaf.hh"
#include "data.hh"
#include "background.hh"
#include "model.hh"
#include "targetdata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcMlfmm> ShCalcMlfmmPr;

	// mlfmm calculation with custom targets
	// can be used to setup a calculation with
	// multiple target objects
	class CalcMlfmm: public CalcLeaf{
		// set coils
		protected:
			// add additional calculation targets
			ShTargetDataPrList targets_;

			// use the source meshes as targets
			bool output_meshes_ = false; // output meshes even if not added as targets
			bool target_meshes_ = false;
			bool target_grid_ = false;
			bool target_surface_ = false;
			
			// calculate conductor lengths
			bool calc_lengths_ = false;

			// additional settings
			fltp grid_scaling_factor_ = 1.3;

		// methods
		public:
			// constructor
			CalcMlfmm();
			explicit CalcMlfmm(const ShModelPr &model);

			// factory methods
			static ShCalcMlfmmPr create();
			static ShCalcMlfmmPr create(const ShModelPr &model);

			// add additional target
			void add_target(const ShTargetDataPr &target);
			void add_targets(const std::list<ShTargetDataPr> &target);

			// calculation function
			virtual std::list<ShDataPr> calculate(
				const fltp time, 
				const cmn::ShLogPr &lg = cmn::NullLog::create(),
				const ShSolverCachePr& cache = NULL) override;
			
			// calculation of field at specified targets
			std::list<ShTargetDataPr> calculate_targets(
				const fltp time = RAT_CONST(0.0), 
				const cmn::ShLogPr &lg = cmn::NullLog::create(),
				const ShSolverCachePr& cache = NULL);

			// enable build in targets
			void set_output_meshes(const bool output_meshes = true);
			void set_target_meshes(const bool target_meshes = true);
			void set_target_grid(const bool target_grid = true);
			void set_target_surface(const bool target_surface = true);

			// set background field
			void set_background(const ShBackgroundPr bg);

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
