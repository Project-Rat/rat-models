// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_FILE2_HH
#define MDL_PATH_FILE2_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <functional>

#include "path.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathFile2> ShPathFile2Pr;

	// cross section of coil
	class PathFile2: public Path, public Transformations{
		// properties
		private:
			// geometry input file
			boost::filesystem::path file_name_;

			// in case of multiple columns in a single file [x1,y1,z1,x2,y2,z2 ... ]
			arma::uword index_ = 0llu;

			// scale factor
			fltp scale_factor_ = RAT_CONST(1.0);

			// orientation vector normalization flag
			bool normalize_orientation_vectors_ = false;

			// element size limitter
			fltp element_size_limit_ = RAT_CONST(0.0); // limit element size (use 0.0 for no limit)

			// out of plane vector for calculating orientation
			bool use_plane_vector_ = true;
			bool is_cylinder_ = false; // when enabled Vn should be set as the axis of the cylinder
			arma::Col<fltp>::fixed<3> Vn_{0,0,1};

		// methods
		public:
			// constructor
			PathFile2();
			explicit PathFile2(const boost::filesystem::path &file_name, const arma::uword index = 0);

			// factory methods
			static ShPathFile2Pr create();
			static ShPathFile2Pr create(const boost::filesystem::path &file_name, const arma::uword index = 0);

			// set properties
			void set_file_name(const boost::filesystem::path &file_name);
			void set_index(const arma::uword index);
			void set_scale_factor(const fltp scale_factor = RAT_CONST(1.0));
			void set_plane_vector(const arma::Col<fltp>::fixed<3>& plane_vector);
			void set_normal_vector(const arma::Col<fltp>::fixed<3>& Vn);
			void set_use_plane_vector(const bool use_plane_vector = true);
			void set_is_cylinder(const bool is_cylinder = true);
			void set_element_size_limit(const fltp element_size_limit);
			void set_normalize_orientation_vectors(const bool normalize_orientation_vectors = true);

			// get properties
			const boost::filesystem::path& get_file_name()const;
			arma::uword get_index()const;
			fltp get_scale_factor()const;
			const arma::Col<fltp>::fixed<3>& get_plane_vector()const;
			bool get_is_cylinder()const;
			const arma::Col<fltp>::fixed<3>& get_normal_vector()const;
			fltp get_element_size_limit()const;
			bool get_normalize_orientation_vectors()const;
			bool get_use_plane_vector()const;

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// validity check
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif