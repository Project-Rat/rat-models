// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_DSHAPE_HH
#define MDL_PATH_DSHAPE_HH

#include <armadillo> 
#include <cassert>
#include <memory>

#include "transformations.hh"
#include "path.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathDShape> ShPathDShapePr;

	// cross section of coil
	class PathDShape: public Path, public Transformations{
		// properties
		private:
			// settings
			fltp ell1_ = RAT_CONST(0.0); // height of the D [m]
			fltp ell2_ = RAT_CONST(0.0); // width of the D [m]
			
			// element size
			fltp element_size_ = RAT_CONST(0.0); // size of the elements [m]

			// offset
			fltp offset_ = RAT_CONST(0.0); // offset [m]

		// methods
		public:
			// constructor
			PathDShape();
			PathDShape(
				const fltp ell1, 
				const fltp ell2, 
				const fltp element_size, 
				const fltp offset = RAT_CONST(0.0));

			// factory methods
			static ShPathDShapePr create();
			static ShPathDShapePr create(
				const fltp ell1, 
				const fltp ell2, 
				const fltp element_size, 
				const fltp offset = RAT_CONST(0.0));

			// set properties
			void set_ell1(const fltp ell1);
			void set_ell2(const fltp ell2);
			void set_element_size(const fltp element_size);
			void set_offset(const fltp offset);

			// get properties
			fltp get_ell1() const;
			fltp get_ell2() const;
			fltp get_element_size() const;
			fltp get_offset() const;

			// get frame
			virtual ShFramePr create_frame(
				const MeshSettings &stngs) const override;

			// validity check
			virtual bool is_valid(
				const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif