// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_LENGTH_DATA_HH
#define MDL_LENGTH_DATA_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/error.hh"
#include "rat/common/log.hh"

#include "data.hh"
#include "meshdata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class LengthData> ShLengthDataPr;

	// template for coil
	class LengthData: public Data{
		// properties
		protected:
			// stored properties
			arma::field<std::string> coil_names_; // mesh names
			arma::Row<fltp> ell_; // length of conductor calculated from volume
			arma::Row<fltp> volume_; // volume of mesh
			arma::Row<fltp> ell_gen_; // length of the generator frame
			arma::Row<fltp> num_turns_; // number of turns

		// methods
		public:
			// constructor
			explicit LengthData(
				const arma::field<std::string>& coil_names,
				const arma::Row<fltp>& volume,
				const arma::Row<fltp>& num_turns,
				const arma::Row<fltp>& ell_gen,
				const arma::Row<fltp>& ell);

			// factory
			static ShLengthDataPr create(
				const arma::field<std::string>& coil_names,
				const arma::Row<fltp>& volume,
				const arma::Row<fltp>& num_turns,
				const arma::Row<fltp>& ell_gen,
				const arma::Row<fltp>& ell);

			// access data
			const arma::field<std::string>& get_coil_names() const;
			const arma::Row<fltp>& get_volume() const;
			const arma::Row<fltp>& get_num_turns() const;
			const arma::Row<fltp>& get_ell_gen() const;
			const arma::Row<fltp>& get_ell() const;
			const arma::field<mat::ShConductorPr>& get_materials()const;

			// access data per coil
			arma::uword get_num_coils() const;
			std::string get_coil_name(const arma::uword index) const;
			fltp get_volume(const arma::uword index) const;
			fltp get_num_turns(const arma::uword index) const;
			fltp get_ell_gen(const arma::uword index) const;
			fltp get_ell(const arma::uword index) const;

			// export vtk
			ShVTKObjPr export_vtk() const override;

			// write output files
			void display(cmn::ShLogPr lg = cmn::NullLog::create());
	};
}}

#endif
