// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_DRIVE_DC_HH
#define MDL_DRIVE_DC_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "drive.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class DriveDC> ShDriveDCPr;
	typedef arma::field<ShDriveDCPr> ShDriveDCPrList;

	// circuit class
	class DriveDC: public Drive{
		// properties
		protected:
			// fixed scaling
			fltp amplitude_;

		// methods
		public:
			// constructor
			explicit DriveDC(const fltp amplitude = 1.0);

			// factory
			static ShDriveDCPr create(const fltp amplitude  = 1.0);

			// set and get current
			void set_amplitude(const fltp amplitude);
			fltp get_amplitude() const;

			// get scaling
			fltp get_scaling(
				const fltp position,
				const fltp time = 0.0,
				const arma::uword derivative = 0) const override;
			
			// apply scaling for the input settings
			virtual void rescale(const fltp scale_factor) override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
