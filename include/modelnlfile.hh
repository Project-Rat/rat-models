// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef MDL_MODEL_NL_FILE_HH
#define MDL_MODEL_NL_FILE_HH

// only available for non-linear solver
#ifdef ENABLE_NL_SOLVER

// general headers
#include <armadillo> 
#include <memory>

// model headers
#include "modelsphere.hh"
#include "inputhbcurve.hh"
#include "hbcurvevlv.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelNLFile> ShModelNLFilePr;

	// current carying coil extruded with
	// a base path and a cross section
	class ModelNLFile: public Model, public InputHBCurve{
		// properties
		protected:
			// the file
			boost::filesystem::path fname_;

			// position
			arma::Col<fltp>::fixed<3> R0_{RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0)};

			// softening factor
			fltp softening_ = RAT_CONST(1.4);

			// scaling
			fltp scale_factor_ = RAT_CONST(1.0);

			// number of gauss points
			arma::sword num_gauss_surface_ = 6ll;
			arma::sword num_gauss_volume_ = 2ll;

			// temperature of the coil
			fltp operating_temperature_ = RAT_CONST(4.5);

			// combine
			fltp combine_node_tolerance_ = 1024*arma::Datum<fltp>::eps; // [m]

		// methods
		public:
			// constructor
			ModelNLFile(
				const ShHBCurvePr &hb_curve = HBCurveVLV::create());
			explicit ModelNLFile(
				const boost::filesystem::path& fpath,
				const fltp scale_factor = RAT_CONST(1.0),
				const ShHBCurvePr &hb_curve = HBCurveVLV::create());
			
			// factory methods
			static ShModelNLFilePr create(
				const ShHBCurvePr &hb_curve = HBCurveVLV::create());
			static ShModelNLFilePr create(
				const boost::filesystem::path& fpath,
				const fltp scale_factor = RAT_CONST(1.0),
				const ShHBCurvePr &hb_curve = HBCurveVLV::create());
			
			// setters
			void set_file_name(const boost::filesystem::path& fname);
			void set_scale_factor(const fltp scale_factor);
			void set_softening(const fltp softening);
			void set_num_gauss_volume(const arma::sword num_gauss_volume);
			void set_num_gauss_surface(const arma::sword num_gauss_surface);
			void set_operating_temperature(const fltp operating_temperature);

			// getters
			const boost::filesystem::path& get_file_name()const;
			fltp get_scale_factor()const;
			fltp get_softening()const;
			arma::sword get_num_gauss_surface()const;
			arma::sword get_num_gauss_volume()const;
			fltp get_operating_temperature()const;

			// create specific mesh
			virtual std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
#endif
