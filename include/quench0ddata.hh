// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_QUENCH0D_DATA_HH
#define MDL_QUENCH0D_DATA_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "data.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Quench0DData> ShQuench0DDataPr;

	// calculate field on line defined 
	// by start and end point coordinates
	class Quench0DData: public Data{
		// properties
		protected:
			// system info
			arma::uword quenching_id_;
			arma::Row<arma::uword> circuit_id_;
			arma::field<std::string> coil_names_;
			arma::field<std::string> circuit_names_;
			arma::Row<arma::uword> heatered_;
			arma::Row<arma::uword> is_sc_;

			// data table
			arma::Col<fltp> time_;
			arma::Row<fltp> hotspot_position_;
			arma::Mat<fltp> hotspot_temperature_;
			arma::Mat<fltp> coil_temperatures_;
			arma::Mat<fltp> circuit_currents_;
			arma::Mat<fltp> circuit_voltage_;

			// event markers
			fltp treshold_time_;
			fltp detection_time_;
			arma::Row<fltp> switching_times_;
			arma::Row<fltp> heater_times_;

			// hotspot info
			fltp hotspot_start_temperature_;
			fltp hotspot_start_current_density_;
			fltp hotspot_flux_density_;
			fltp hotspot_alpha_;
			fltp normal_zone_propagation_velocity_;
			fltp minimal_propagation_zone_length_;
			fltp minimal_quench_energy_;

		// methods
		public:
			// constructor
			Quench0DData(
				const arma::uword quenching_id,
				const arma::Row<arma::uword>& circuit_id,
				const arma::field<std::string>& coil_names,
				const arma::field<std::string>& circuit_names,
				const arma::Col<fltp>&time, 
				const arma::Row<fltp>&hotspot_position,
				const arma::Mat<fltp>&hotspot_temperature, 
				const arma::Mat<fltp> &coil_temperatures, 
				const arma::Mat<fltp> &circuit_currents,
				const arma::Mat<fltp> &circuit_voltage,
				const fltp treshold_time,
				const fltp detection_time,
				const arma::Row<fltp> &switching_times,
				const arma::Row<fltp> &heater_times,
				const fltp hotspot_flux_density,
				const fltp hotspot_alpha,
				const fltp hotspot_start_temperature,
				const fltp hotspot_start_current_density,
				const fltp normal_zone_propagation_velocity,
				const fltp minimal_propagation_zone_length,
				const fltp minimal_quench_energy,
				const arma::Row<arma::uword>& heatered,
				const arma::Row<arma::uword>& is_sc);

			// factory methods
			static ShQuench0DDataPr create(
				const arma::uword quenching_id,
				const arma::Row<arma::uword>& circuit_id,
				const arma::field<std::string>& coil_names,
				const arma::field<std::string>& circuit_names,
				const arma::Col<fltp>&time, 
				const arma::Row<fltp>&hotspot_position,
				const arma::Mat<fltp>&hotspot_temperature, 
				const arma::Mat<fltp> &coil_temperatures, 
				const arma::Mat<fltp> &circuit_currents,
				const arma::Mat<fltp> &circuit_voltage,
				const fltp treshold_time,
				const fltp detection_time,
				const arma::Row<fltp> &switching_times,
				const arma::Row<fltp> &heater_times,
				const fltp hotspot_flux_density,
				const fltp hotspot_alpha,
				const fltp hotspot_start_temperature,
				const fltp hotspot_start_current_density,
				const fltp normal_zone_propagation_velocity,
				const fltp minimal_propagation_zone_length,
				const fltp minimal_quench_energy,
				const arma::Row<arma::uword>& heatered,
				const arma::Row<arma::uword>& is_sc);

			// getters
			arma::uword get_quenching_id()const;
			const arma::Row<arma::uword>& get_circuit_id()const;
			const arma::field<std::string>& get_coil_names()const;
			const arma::field<std::string>& get_circuit_names()const;
			const arma::Col<fltp>& get_time()const;
			const arma::Row<fltp>& get_hotspot_position()const;
			const arma::Mat<fltp>& get_hotspot_temperature()const;
			const arma::Mat<fltp>& get_coil_temperatures()const;
			const arma::Mat<fltp>& get_circuit_currents()const;
			const arma::Mat<fltp>& get_circuit_voltage()const;
			fltp get_treshold_time()const;
			fltp get_detection_time()const;
			const arma::Row<fltp>& get_switching_times()const;
			const arma::Row<fltp>& get_heater_times()const;
			fltp get_hotspot_flux_density()const;
			fltp get_hotspot_alpha()const;
			fltp get_normal_zone_propagation_velocity()const;
			const arma::Row<arma::uword>& get_heatered()const;
			const arma::Row<arma::uword>& get_is_sc()const;
			fltp get_hotspot_start_temperature()const;
			fltp get_hotspot_start_current_density()const;
			fltp get_minimal_propagation_zone_length()const;
			fltp get_minimal_quench_energy()const;

			// analysis
			fltp estimate_quench_integral()const;

			// export to vtk
			virtual ShVTKObjPr export_vtk() const override;
			void export_csv(const boost::filesystem::path& fpath) const;
			void export_tikz(const boost::filesystem::path& fpath) const;

	};

}}

#endif
