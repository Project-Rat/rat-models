// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_TRANSFORMATIONS_HH
#define MDL_TRANSFORMATIONS_HH

#include <armadillo> 
#include <cassert>

#include "rat/common/node.hh"
#include "trans.hh"
#include "transtranslate.hh"
#include "transrotate.hh"
#include "transreflect.hh"
#include "transreverse.hh"
#include "transflip.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Transformations> ShTransformationsPr;

	// superclass to add transformation options
	class Transformations: virtual public cmn::Node{
		// transformations
		protected:
			std::map<arma::uword, ShTransPr> trans_;

		// methods
		public:
			// constructor
			Transformations();

			// virtual destructor
			virtual ~Transformations(){};

			// factory (in case of standalone use)
			static ShTransformationsPr create();

			// get transformation
			ShTransPr get_transformation(const arma::uword idx) const;
			std::list<ShTransPr> get_transformations() const;
			bool delete_transformation(const arma::uword index);

			// transformations
			arma::uword add_transformation(const ShTransPr &trans);
			arma::Row<arma::uword> add_transformations(const std::list<ShTransPr> &trans_list);
			void set_transformation(const arma::uword idx, const ShTransPr trans);
			virtual void reindex() override;

			// get transformations
			arma::uword num_transformations() const;

			// combine function cleans up transformations
			void combine_transformations();
			void clear_transformations();
			
			// adding of standard transformations
			void add_translation(const fltp dx, const fltp dy, const fltp dz);
			void add_translation(const arma::Col<fltp>::fixed<3> &dR);
			void add_rotation(const fltp phi, const fltp theta, const fltp psi);
			void add_rotation(const fltp ux, const fltp uy, const fltp uz, const fltp alpha);
			void add_rotation(const arma::Col<fltp>::fixed<3> &V, const fltp alpha);
			void add_rotation(const arma::Col<fltp>::fixed<3> &O, const arma::Col<fltp>::fixed<3> &V, const fltp alpha, const arma::Col<fltp>::fixed<3>&offset = {0,0,0});
			void add_reflect_xy();
			void add_reflect_yz();
			void add_reflect_xz();
			void add_reverse();
			void add_flip();

			// check validity
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif




