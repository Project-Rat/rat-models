// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CROSS_PATH_HH
#define MDL_CROSS_PATH_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <json/json.h>

#include "cross.hh"
#include "area.hh"
#include "inputpath.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CrossPath> ShCrossPathPr;

	// cross section of coil
	class CrossPath: public Cross, public InputPath{
		// properties
		private:
			// one dimensional line or two dimensional area
			bool is_line_ = false;

			// normal coordinates
			fltp thickness_ = RAT_CONST(20e-3);
			fltp offset_ = RAT_CONST(0.0);

			// element size
			fltp element_size_ = RAT_CONST(10e-3);

		// methods
		public:
			// constructors
			CrossPath();
			CrossPath(
				const ShPathPr &input_path, 
				const fltp offset, 
				const fltp thickness, 
				const fltp element_size);

			// factory methods
			static ShCrossPathPr create();
			static ShCrossPathPr create(
				const ShPathPr &input_path, 
				const fltp offset, 
				const fltp thickness, 
				const fltp element_size);

			// set properties
			void set_thickness(const fltp thickness);
			void set_offset(const fltp offset);
			void set_element_size(const fltp element_size);
			void set_is_line(const bool is_line = true);

			// get properties
			fltp get_thickness()const;
			fltp get_offset()const;
			fltp get_element_size()const;
			bool get_is_line()const;

			// volume mesh
			virtual ShAreaPr create_area(const MeshSettings &stngs) const override;

			// surface mesh
			// virtual ShPerimeterPr create_perimeter() const override;

			// get bounding box
			virtual arma::Mat<fltp>::fixed<2,2> get_bounding() const override;

			// vallidity check
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
