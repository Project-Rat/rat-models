// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MESH_DATA_HH
#define MDL_MESH_DATA_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/node.hh"

#include "rat/mat/conductor.hh"

#include "rat/mlfmm/currentsources.hh"
#include "rat/mlfmm/mgntargets.hh"
#include "rat/mlfmm/currentmesh.hh"
#include "rat/mlfmm/sources.hh"
#include "rat/mlfmm/interp.hh"

#include "area.hh"
#include "frame.hh"
#include "trans.hh"
#include "targetdata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Mesh> ShMeshPr;
	typedef arma::field<ShMeshPr> ShMeshPrList;

	// mesh of a coil
	// this objects carries all properties 
	// required for the calculations
	class Mesh: virtual public cmn::Node{
		// properties
		protected:
			// material of this coil
			rat::mat::Direction dir_ = rat::mat::Direction::LONGITUDINAL;
			rat::mat::ShConductorPr material_;

			// area data
			fltp base_area_ = RAT_CONST(0.0);
			arma::uword num_base_elements_ = 0llu;
			arma::uword num_base_nodes_ = 0llu;
			arma::uword num_base_perimeter_ = 0llu;
			fltp hidden_thickness_ = RAT_CONST(1.0);
			fltp hidden_width_ = RAT_CONST(1.0);

			// frame data
			bool is_loop_ = false;
			arma::Row<arma::uword> is_closed_;
			arma::Row<arma::uword> section_;
			arma::Row<arma::uword> turn_;
			arma::Row<arma::uword> num_cis_frame_;
			arma::Row<fltp> ell_frame_;

			// mesh data
			bool is_extruded_ = false;
			arma::uword num_cs_ = 0llu;
			arma::uword num_cis_ = 0llu;
			arma::uword num_cs_elements_ = 0llu;
			arma::uword num_cis_elements_ = 0llu;
			arma::uword num_nodes_ = 0llu;

			// dimensionality of meshes
			arma::uword n_dim_ = 0llu;
			arma::uword s_dim_ = 0llu;

			// base node coordinates
			arma::Mat<fltp> Rb_;

			// node coordinates
			arma::Mat<fltp> R_;

			// node orientation vectors
			arma::Mat<fltp> L_;
			arma::Mat<fltp> N_;
			arma::Mat<fltp> D_;

			// mesh element indexes
			arma::Mat<arma::uword> n_; // volume (normally hex)
			arma::Mat<arma::uword> s_; // surface (normally quad)
			arma::Mat<arma::uword> b_; // base mesh
			arma::Mat<arma::uword> p_; // base mesh perimeter
			arma::Col<arma::uword> q_; // corner nodes in 2D

			// center line
			arma::field<arma::Mat<fltp> > Rc_;


		// methods
		public:
			// constructors
			Mesh(); // default
			Mesh(
				const ShFramePr &frame, 
				const ShAreaPr &area,
				const mat::ShConductorPr &material = NULL,
				const bool use_parallel = false);
			Mesh(
				const arma::Mat<fltp> &R, 
				const arma::Mat<arma::uword> &n, 
				const arma::Mat<arma::uword> &s);
			
			// factory
			static ShMeshPr create();
			static ShMeshPr create(
				const ShFramePr &frame, 
				const ShAreaPr &area,
				const mat::ShConductorPr &material = NULL,
				const bool use_parallel = false);
			static ShMeshPr create(
				const arma::Mat<fltp> &R, 
				const arma::Mat<arma::uword> &n, 
				const arma::Mat<arma::uword> &s);

			// virtual destructor
			virtual ~Mesh(){};

			// mesh setup using a frame and cross sectional area
			// the area is extruded along the frame to generate
			// the three-dimensional mesh
			virtual void setup(
				const ShFramePr &frame, 
				const ShAreaPr &area,
				const bool use_parallel = false);

			// manually setup the mesh by supplying the node coordinates,
			// the mesh elements and surface elements
			virtual void setup(
				const arma::Mat<fltp> &R, 
				const arma::Mat<arma::uword> &n, 
				const arma::Mat<arma::uword> &s);

			// function for setting the mesh
			void set_mesh(
				const arma::Mat<fltp> &R, 
				const arma::Mat<arma::uword> &n, 
				const arma::Mat<arma::uword> &s);

			// split this mesh into smaller section determined
			// by the number of cable intersections in the frame
			arma::field<ShMeshPr> split_series()const;

			// split mesh into parallel strips
			arma::field<ShMeshPr> split_parallel()const;

			// general split with input
			arma::field<ShMeshPr> split(const bool series = true, const bool parallel = false)const;

			// internal function for automatically 
			// determining the n_dim and s_dim called
			// by the setup functions
			void determine_dimensions();

			// set the mesh material
			void set_dir(const rat::mat::Direction dir);
			void set_material(const rat::mat::ShConductorPr &material);

			// get the mesh material
			rat::mat::Direction get_dir()const;
			const rat::mat::ShConductorPr& get_material()const;

			// get material properties
			bool is_insulator()const;

			// get mesh info
			arma::uword get_n_dim()const;
			arma::uword get_s_dim()const;
			arma::uword get_num_nodes()const;
			arma::uword get_num_elements()const;
			arma::uword get_num_base_nodes()const;
			arma::uword get_num_base_elements()const;
			arma::uword get_num_base_perimeter()const;
			arma::uword get_num_surface()const;
			arma::uword get_num_cis()const;
			arma::uword get_num_cs()const;
			arma::uword get_num_cis_elements()const;
			arma::uword get_num_cs_elements()const;
			arma::uword get_num_sections()const;

			// set mesh data directly
			void set_longitudinal(const arma::Mat<fltp> &L);
			void set_transverse(const arma::Mat<fltp> &D);
			void set_normal(const arma::Mat<fltp> &N);

			// set elmental data
			void set_elements(const arma::Mat<arma::uword> &n);
			void set_edge_nodes(const arma::Col<arma::uword> &q);
			void set_surface_elements(const arma::Mat<arma::uword> &s);
			void set_base_mesh(const arma::Mat<arma::uword> &b);
			void set_perimeter(const arma::Mat<arma::uword> &p);
			void set_base_nodes(const arma::Mat<fltp>&Rb);

			// set counters directly
			void set_is_extruded(const bool is_extruded = true);
			void set_is_loop(const bool is_loop = true);
			void set_is_closed(const arma::Row<arma::uword> &is_closed);
			void set_num_base_elements(const arma::uword num_base_elements);
			void set_num_base_nodes(const arma::uword num_base_nodes);
			void set_num_base_perimeter(const arma::uword num_base_perimeter);
			void set_num_cis(const arma::uword num_cis);
			void set_num_cs(const arma::uword num_cs);
			void set_num_cis_elements(const arma::uword num_cis_elements);
			void set_num_cs_elements(const arma::uword num_cs_elements);
			void set_num_cis_frame(const arma::Row<arma::uword>& num_cis_frame);
			void set_ell_frame(const arma::Row<fltp> &ell_frame);
			void set_num_cs_frame(const arma::Row<arma::uword>& num_cs_frame);
			void set_hidden_width(const fltp hidden_width);
			void set_hidden_thickness(const fltp hidden_thickness);
			void set_center_line(const arma::field<arma::Mat<fltp> > &Rc);
			void set_section(const arma::Row<arma::uword> &section);
			void set_turn(const arma::Row<arma::uword> &turn);
			void set_base_area(const fltp base_area);

			// convert to tetrahedron
			void refine_hexahedron_to_tetrahedron();

			// access mesh data
			bool get_is_loop()const;
			const arma::Row<arma::uword>& get_num_cis_frame()const;
			const arma::Mat<fltp>& get_nodes()const;
			const arma::Mat<fltp>& get_longitudinal()const;
			const arma::Mat<fltp>& get_normal() const;
			const arma::Mat<fltp>& get_transverse()const;
			const arma::Row<fltp>& get_ell_frame()const;
			const arma::field<arma::Mat<fltp> >& get_center_line()const;
			arma::Mat<fltp> calc_binormal()const;

			// get elements
			const arma::Mat<arma::uword>& get_elements()const;
			const arma::Mat<arma::uword>& get_surface_elements() const;
			const arma::Mat<arma::uword>& get_base_mesh()const;
			const arma::Mat<arma::uword>& get_base_perimeter()const;

			// calculate distance between the surface elements and the center line
			arma::Row<fltp> calc_depth()const;

			// get the area of the base
			// this is no longer guaranteed to be the 
			// cross sectional area everywhere
			fltp get_base_area()const;

			// convert elemental properties from the cable
			// intersection to the cable section by interpolation
			arma::Mat<fltp> cis2cs(const arma::Mat<fltp>& vcis)const;

			// get the barycenter coordinate of each element
			// output vectors in [m]
			arma::Mat<fltp> get_element_coords()const;

			// calculate the areas of the cross sectional
			// elements at the cable intersections in [m^2]
			arma::Row<fltp> calc_cis_areas()const;

			// calculate the areas of the cross sectional
			// elements at the cable sections in [m^2]
			arma::Row<fltp> calc_cs_areas(const arma::Row<fltp>& cis_areas)const;

			// same but with internal calculation of the 
			// areas at the intersections
			arma::Row<fltp> calc_cs_areas()const;

			// calculate the area of the mesh 
			// at the cable intersections in [m^2]
			arma::Row<fltp> calc_cis_total_area(
				const arma::Row<fltp>& cis_areas)const;

			// same but with internal calculation of cis areas
			arma::Row<fltp> calc_cis_total_area()const;

			// calculate the area of the mesh at 
			// the cable sections in [m^2]
			arma::Row<fltp> calc_cs_total_area(
				const arma::Row<fltp>& cis_total_area)const;

			// same but with internal calculation of cis areas
			arma::Row<fltp> calc_cs_total_area()const;
			
			// calculate the element size (distance of 
			// connected nodes to barycenter) in [m]
			arma::Row<fltp> calc_cis_delem()const;

			// calculate the average length of each cable section
			// using the respective volume and cross sectional area
			// output in [m]
			arma::Row<fltp> calc_cs_length(
				const arma::Row<fltp>& volume, 
				const arma::Row<fltp>& cis_total_area)const;

			// same but with internal volume and area calculation
			arma::Row<fltp> calc_cs_length()const;

			// calculate the longitudinal length of the mesh 
			// using the volume and cross sectional area in [m]
			fltp calc_ell(
				const arma::Row<fltp>& volume, 
				const arma::Row<fltp>& cis_total_area)const;

			// same but with internal volume and area calculation
			fltp calc_ell()const;

			// calculate the mass of this mesh in [kg]
			fltp calc_mass(
				const fltp temperature = 300.0)const;

			// calculate the integrated thermal energy of this mesh
			// at specified temperature [J]
			fltp calc_thermal_energy(
				const fltp temperature, 
				const fltp delta_temperature = RAT_CONST(1.0)) const; // output in [J]
			
			// calculate the temperature increase of this mesh
			// when dumping a specified amount of heat into the coil
			// this uses the conductor material
			// output delta T in [K]
			fltp calc_temperature_increase(
				const fltp initial_temperature, 
				const fltp added_heat,
				const fltp temperature_stepsize = RAT_CONST(1.0)) const;

			// calculate the volume of all the elements 
			// seperately in [m^3]
			arma::Row<fltp> calc_volume() const;

			// calculate the total volume of the mesh in [m^3]
			fltp calc_total_volume() const;

			// calculate the surface area of the mesh in [m^2]
			fltp calc_total_surface_area() const;
			
			// calculate the hidden dimension contribution
			fltp calc_hidden_dim()const;
			fltp get_hidden_thickness()const;
			fltp get_hidden_width()const;

			// create a field array with edges stored as {[x;y;z],[x;y;z],[x;y;z],[x;y;z]} in [m]
			arma::field<arma::Mat<fltp> > get_edges(
				const bool full_perimeter = false) const;

			// create edge matrices in place
			// the matrices are stored seperately as 
			// x = [x1,x2,x3,x4], y = [y1,y2,y3,y4], z = [z1,z2,z3,z4]
			// also stores the number of edges in num_edges
			void create_xyz(
				arma::field<arma::Mat<fltp> > &x, 
				arma::field<arma::Mat<fltp> > &y, 
				arma::field<arma::Mat<fltp> > &z, 
				arma::uword &num_edges,
				const bool full_perimeter = false) const;

			// calculate curvature on mesh [1/m] (is 1/radius)
			arma::Mat<fltp> calc_curvature() const; // TODO needs improving stability

			// apply a transformation to the mesh and orientation vectors
			void apply_transformations(const std::list<ShTransPr> &trans_list, const fltp time);
			void apply_transformation(const ShTransPr &trans, const fltp time);

			// get size and position of mesh
			arma::Col<fltp>::fixed<3> get_lower_bound() const;
			arma::Col<fltp>::fixed<3> get_upper_bound() const;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
