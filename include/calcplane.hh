// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALC_PLANE_HH
#define MDL_CALC_PLANE_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

// rat models headers
#include "calcleaf.hh"
#include "cartesiangriddata.hh"
#include "inputpath.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcPlane> ShCalcPlanePr;

	// calculate field on line defined 
	// by start and end point coordinates
	class CalcPlane: public CalcLeaf{
		// properties
		protected:
			// orientation
			char plane_normal_ = 'x';

			// position 
			arma::Col<fltp>::fixed<3> offset_ = {RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0)};

			// dimensions
			fltp ell1_ = RAT_CONST(0.3);
			fltp ell2_ = RAT_CONST(0.2);

			// discretization
			arma::uword num_steps1_ = 150;
			arma::uword num_steps2_ = 100;

			// enable visibility
			bool visibility_ = true;

		// methods
		public:
			// constructor
			CalcPlane();
			explicit CalcPlane(const ShModelPr &model);
			CalcPlane(
				const ShModelPr &model, 
				const char plane_normal, 
				const fltp ell1, 
				const fltp ell2, 
				const arma::uword num_steps1, 
				const arma::uword num_steps2);
			
			// factory methods
			static ShCalcPlanePr create();
			static ShCalcPlanePr create(const ShModelPr &model);
			static ShCalcPlanePr create(
				const ShModelPr &model, 
				const char plane_normal, 
				const fltp ell1, 
				const fltp ell2, 
				const arma::uword num_steps1, 
				const arma::uword num_steps2);

			// set properties
			void set_plane_normal(const char plane_normal);
			void set_offset(const arma::Col<fltp>::fixed<3> &offset);
			void set_ell1(const fltp ell1);
			void set_ell2(const fltp ell2);
			void set_num_steps1(const arma::uword num_steps1);
			void set_num_steps2(const arma::uword num_steps2);
			void set_visibility(const bool visibility);

			// get properties
			char get_plane_normal() const;
			bool get_visibility() const;
			arma::Col<fltp>::fixed<3> get_offset() const;
			fltp get_ell1() const;
			fltp get_ell2() const;
			arma::uword get_num_steps1() const;
			arma::uword get_num_steps2() const;

			// calculate with inductance data output
			ShCartesianGridDataPr calculate_plane(
				const fltp time = RAT_CONST(0.0), 
				const cmn::ShLogPr &lg = cmn::NullLog::create(),
				const ShSolverCachePr& cache = NULL);

			// calculation function
			virtual std::list<ShDataPr> calculate(
				const fltp time, 
				const cmn::ShLogPr &lg = cmn::NullLog::create(),
				const ShSolverCachePr& cache = NULL) override;
			
			// creation of calculation data objects
			std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override; // create a mesh

			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
