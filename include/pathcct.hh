// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_CCT_HH
#define MDL_PATH_CCT_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <memory>
#include <cmath>

// models headers
#include "transformations.hh"
#include "path.hh"
#include "frame.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathCCT> ShPathCCTPr;

	// cross section of coil
	class PathCCT: public Path, public Transformations{
		// properties
		private:
			// reverse skew
			bool is_reverse_ = false; // reverse harmonic oscillation
			bool is_solenoid_ = false; // all layers in same winding direction
			bool is_reverse_solenoid_ = false; // reverse solenoidal winding direction
			bool is_skew_ = false;
			bool is_rib_ = false;
			bool is_hardway_ = false;

			// bending along length
			bool bending_origin_ = false;
			bool use_radius_ = false;
			fltp bending_radius_ = RAT_CONST(0.0);
			fltp bending_arc_length_ = RAT_CONST(0.0);

			// harmonic 1: dipole, 2: quadrupole, 3: sextupole, 4: octupole etc
			arma::uword num_poles_ = 1; 

			// radius
			fltp radius_ = RAT_CONST(30e-3); 

			// skew amplitude
			bool use_skew_angle_ = false;
			fltp a_ = RAT_CONST(60e-3);
			fltp alpha_ = RAT_CONST(60.0)*arma::Datum<fltp>::tau/360;

			// spacing between turns (winding pitch)
			bool normal_omega_ = false;
			fltp omega_ = RAT_CONST(6e-3); 

			// number of turns (previously this was set by num_turns, where nt1 = -num_turns/2, nt2 = num_turns/2)
			fltp nt1_ = -10; 
			fltp nt2_ = 10; 

			// discretization
			arma::uword num_nodes_per_turn_ = 180;

			// additional twisting along length of cable
			fltp twist_ = RAT_CONST(0.0); 

			// squeeze transformation
			fltp scale_x_ = RAT_CONST(1.0); // 1.0 is not elliptic at all
			fltp scale_y_ = RAT_CONST(1.0); // 1.0 is not elliptic at all

			// settings for layers
			arma::uword num_layers_ = 1;
			bool const_skew_angle_ = false;
			fltp rho_increment_ = RAT_CONST(0.0);

			// layer jump
			const bool use_layer_jumps_ = false;

			// first lead
			bool enable_lead1_ = false;
			fltp leadness1_ = 0.05; // parameter defining curvature of the lead
			fltp zlead1_ = 0.1; // length of current lead with respect to magnetic length of coil
			fltp theta_lead1_ = arma::Datum<fltp>::pi/4; // position of lead-end

			// second lead
			bool enable_lead2_ = false;
			fltp leadness2_ = 0.05; // parameter defining curvature of the lead
			fltp zlead2_ = 0.1; // length of current lead with respect to magnetic length of coil
			fltp theta_lead2_ = arma::Datum<fltp>::pi/4; // position of lead-end

		// methods
		public:
			// constructor
			PathCCT();
			PathCCT(
				const arma::uword num_poles, 
				const fltp radius, const fltp a, 
				const fltp omega, const fltp num_turns, 
				const arma::uword num_nodes_per_turn);
			PathCCT(
				const arma::uword num_poles, 
				const fltp radius, const fltp a, 
				const fltp omega, const fltp nt1, const fltp nt2,
				const arma::uword num_nodes_per_turn);

			// factory methods
			static ShPathCCTPr create();
			static ShPathCCTPr create(
				const arma::uword num_poles, 
				const fltp radius, const fltp a, 
				const fltp omega, const fltp num_turns, 
				const arma::uword num_nodes_per_turn);
			static ShPathCCTPr create(
				const arma::uword num_poles, 
				const fltp radius, const fltp a, 
				const fltp omega, const fltp nt1, const fltp nt2, 
				const arma::uword num_nodes_per_turn);

			// set properties
			void set_is_reverse(const bool is_reverse = true);
			void set_is_solenoid(const bool is_solenoid = true);
			void set_is_reverse_solenoid(const bool is_reverse_solenoid = true);
			void set_is_skew(const bool is_skew = true);
			void set_bending_arc_length(const fltp bending_arc_length);
			void set_twist(const fltp twist);
			void set_use_skew_angle(const bool use_skew_angle = true);
			void set_amplitude(const fltp a);
			void set_alpha(const fltp alpha);
			void set_normal_omega(const bool normal_omega = true);
			void set_pitch(const fltp omega);
			void set_scale_x(const fltp scale_x);
			void set_scale_y(const fltp scale_y);
			void set_radius(const fltp radius);
			void set_num_poles(const arma::uword num_poles);
			void set_num_nodes_per_turn(const arma::uword num_nodes_per_turn);
			void set_num_layers(const arma::uword num_layers);
			void set_const_skew_angle(const bool const_skew_angle = true);
			void set_rho_increment(const fltp rho_increment);
			void set_enable_lead1(const bool enable_lead1 = true);
			void set_enable_lead2(const bool enable_lead2 = true);
			void set_leadness1(const fltp leadness1);
			void set_leadness2(const fltp leadness2);
			void set_zlead1(const fltp zlead1);
			void set_zlead2(const fltp zlead2);
			void set_theta_lead1(const fltp theta_lead1);
			void set_theta_lead2(const fltp theta_lead2);
			void set_bending_origin(const bool bending_origin = true);
			void set_use_radius(const bool use_radius);
			void set_bending_radius(const fltp bending_radius);
			void set_is_rib(const bool is_rib = true);
			void set_is_hardway(const bool is_hardway = true);
			void set_nt1(const fltp nt1);
			void set_nt2(const fltp nt2);

			// get properties
			bool get_is_reverse() const;
			bool get_is_reverse_solenoid()const;
			bool get_is_solenoid() const;
			bool get_is_skew()const;
			fltp get_bending_arc_length() const;
			fltp get_twist()const;
			bool get_use_skew_angle()const;
			fltp get_amplitude()const;
			void get_skew_angle()const;
			fltp get_alpha()const;
			bool get_normal_omega()const;
			fltp get_pitch()const;
			
			fltp get_radius()const;
			fltp get_scale_x()const;
			fltp get_scale_y()const;
			arma::uword get_num_poles()const;
			arma::uword get_num_nodes_per_turn() const;
			arma::uword get_num_layers() const;
			bool get_const_skew_angle()const;
			fltp get_rho_increment()const;
			bool get_enable_lead1()const;
			bool get_enable_lead2()const;
			fltp get_leadness1()const;
			fltp get_leadness2()const;
			fltp get_zlead1()const;
			fltp get_zlead2()const;
			fltp get_theta_lead1()const;
			fltp get_theta_lead2()const;
			bool get_bending_origin()const;
			bool get_use_radius() const;
			fltp get_bending_radius()const;
			bool get_is_rib()const;
			bool get_is_hardway()const;
			fltp get_nt1() const;
			fltp get_nt2() const;

			// compatibility with old number of turns setting
			fltp get_num_turns()const;
			void set_num_turns(const fltp num_turns);

			// omega calculation
			// fltp calc_alpha() const;

			// get frame
			arma::uword get_num_sect_per_turn()const;
			virtual ShFramePr create_frame(
				const MeshSettings &stngs) const override;

			// function for creating a current lead
			static ShFramePr create_current_lead(
				const arma::Col<fltp>::fixed<3> &R0,
				const arma::Col<fltp>::fixed<3> &L0,
				const fltp theta_lead,
				const fltp zlead,
				const fltp leadness,
				const fltp z_end,
				const fltp element_size,
				const bool reverse,
				const fltp twist = 0.0);

			// function for creating layer jump
			static ShFramePr create_layer_jump(
				const arma::Col<fltp>::fixed<3> &R0,
				const arma::Col<fltp>::fixed<3> &L0,
				const arma::Col<fltp>::fixed<3> &R1,
				const arma::Col<fltp>::fixed<3> &L1,
				const fltp leadness0,
				const fltp leadness1,
				const fltp element_size);

			// vallidity check
			bool is_valid(
				const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
