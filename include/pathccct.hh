// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_CCCT_HH
#define MDL_PATH_CCCT_HH

#include <armadillo>
#include <cassert>
#include <memory>
#include <functional>

#include "spfunc.h"
#include "path.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathCCCT> ShPathCCCTPr;

	// cross section of coil
	class PathCCCT : public Path, public Transformations{
		// properties
		private:
			// PARAMETERS
			rat::fltp rho_;			 // major radius of torus [m]
			rat::fltp R_in_;			 // inner radius of winding [m]
			rat::fltp d2_;			 // height of groove [m]
			arma::uword nc_;			 // number of conductors in groove [1]
			rat::fltp main_phi0_=0;              // angular pitch [rad] - initialize to zero so that if the user forgets to set it, a visibly bad result emerges
			
			spfunc<double,poly<double> >  phi0_;  // angular pitch as a smooth piecewise function

			rat::fltp n_turns_=1;	         // number of turns [1]
			rat::fltp I0_;		         // transfer function of single coil
			arma::Row<rat::fltp> Jn_;            // current density components [A/m^2]
			rat::fltp step_size_ = 0.005;	 // step size [m]
			int hand_ = 1;                       // Handedness. 1 = left, -1 = right-handed coil
			int current_sign_ = 1;                 // Direction of current w.r.t. the parameterization direction
			
			// function that computes the length of the tangent to the path at given theta
			rat::fltp tangent_length(rat::fltp theta) const;

		// methods
		public:
			// constructor
			PathCCCT(){}
			
			// factory methods
			static ShPathCCCTPr create(){return std::make_shared<PathCCCT>();}

			// Property setter functions, returning reference to *this so that they can be chained
			PathCCCT &rho(double r) { rho_ = r; return *this; }
			PathCCCT &R(double r) { R_in_ = r; return *this; }
			PathCCCT &d2(double d) { d2_ = d; return *this; }
			PathCCCT &nc(arma::uword n) { nc_ = n; return *this; }
			PathCCCT &main_phi0(rat::fltp p) { main_phi0_ = p; return *this; }
			PathCCCT &phi0(const spfunc<double,poly<double>> &f) { phi0_ = f; return *this; }
			PathCCCT &n_turns(rat::fltp n) { n_turns_ = n; return *this; }
			PathCCCT &I0(rat::fltp I) { I0_ = I; return *this; }
			PathCCCT &Jn(const arma::Row<rat::fltp> &J) { Jn_ = J; return *this; }
			PathCCCT &step_size(double s) { step_size_ = s; return *this; }
			PathCCCT &hand(int h) { if(h>0) hand_ = 1; else hand_ = -1; return *this; }
			PathCCCT &current_sign(int s) { if(s>0) current_sign_ = 1; else current_sign_ = -1; return *this; }
			
			// function that computes the phi values for the inner coil
			arma::Row<rat::fltp> phi(const arma::Row<rat::fltp> &theta) const;
			
			// function that computes the phi derivative values for the inner coil
			arma::Row<rat::fltp> dphi(const arma::Row<rat::fltp> &theta) const;
			
			// get frame
			virtual ShFramePr create_frame(const MeshSettings &) const override;
	};

}}

#endif
