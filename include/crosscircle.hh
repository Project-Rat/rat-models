// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CROSS_CIRCLE_HH
#define MDL_CROSS_CIRCLE_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <json/json.h>

#include "cross.hh"
#include "area.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CrossCircle> ShCrossCirclePr;

	// cross section of coil
	class CrossCircle: public Cross{
		// properties
		private:
			// switchboard
			bool enable_core_ = true;
			bool enable_quadrant1_ = true;
			bool enable_quadrant2_ = true;
			bool enable_quadrant3_ = true;
			bool enable_quadrant4_ = true;

			// orientation
			bool radial_vectors_ = true;

			// dimensions
			fltp radius_ = RAT_CONST(0.0);

			// center position
			fltp nc_ = RAT_CONST(0.0);
			fltp tc_ = RAT_CONST(0.0);

			// element size
			fltp dl_ = RAT_CONST(2e-3);

			// core dimensions
			fltp core_width_ = RAT_CONST(0.0); // when set to zero auto create
			fltp core_height_ = RAT_CONST(0.0);

			// mesh smoothening
			const arma::uword num_smooth_iter_ = 10llu;
			const fltp smooth_dampen_ = RAT_CONST(0.7);

		// methods
		public:
			// constructors
			CrossCircle();
			CrossCircle(
				const fltp radius, const fltp dl);
			CrossCircle(
				const fltp nc, const fltp tc, 
				const fltp radius, const fltp dl);
			
			// factory methods
			static ShCrossCirclePr create();
			static ShCrossCirclePr create(
				const fltp radius, const fltp dl);
			static ShCrossCirclePr create(
				const fltp nc, const fltp tc, 
				const fltp radius, const fltp dl);

			// set properties
			void set_nc(const fltp uc);
			void set_tc(const fltp vc);
			void set_radius(const fltp radius);
			void set_element_size(const fltp dl);
			void set_center(const fltp uc, const fltp vc);
			void set_radial_vectors(const bool radial_vectors = true);
			void set_core_width(const fltp core_width);
			void set_core_height(const fltp core_height);
			void set_enable_core(const bool no_core = true);
			void set_enable_quadrant1(const bool enable_quadrant1 = true);
			void set_enable_quadrant2(const bool enable_quadrant2 = true);
			void set_enable_quadrant3(const bool enable_quadrant3 = true);
			void set_enable_quadrant4(const bool enable_quadrant4 = true);

			// get geometry
			fltp get_nc() const;
			fltp get_tc() const;
			fltp get_radius() const;
			fltp get_element_size() const;
			bool get_radial_vectors() const;
			fltp get_core_width()const;
			fltp get_core_height()const;
			bool get_enable_core()const;
			bool get_enable_quadrant1()const;
			bool get_enable_quadrant2()const;
			bool get_enable_quadrant3()const;
			bool get_enable_quadrant4()const;

			// set all switches
			void set_enable_quadrant(
				const bool enable_quadrant1, 
				const bool enable_quadrant2, 
				const bool enable_quadrant3, 
				const bool enable_quadrant4,
				const bool enable_core = true);

			// get bounding box
			virtual arma::Mat<fltp>::fixed<2,2> get_bounding() const override;

			// volume mesh
			virtual ShAreaPr create_area(const MeshSettings &stngs) const override;

			// surface mesh
			// virtual ShPerimeterPr create_perimeter() const override;

			// check if any part of the mesh is enabled
			bool any_enabled()const;

			// vallidity check
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
