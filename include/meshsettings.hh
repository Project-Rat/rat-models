// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MESH_SETTINGS_HH
#define MDL_MESH_SETTINGS_HH

#include <armadillo> 
#include <memory>

// code specific to Rat
namespace rat{namespace mdl{

	// mesh settings struct
	struct MeshSettings{
		bool is_calculation = true;
		bool low_poly = false;
		bool combine_sections = true; // combine sections into single large mesh with no transitions
		bool separate_sections = false; // separate sections into different meshdata objects
		bool enable_throws = true;
		fltp time = RAT_CONST(0.0); 
		fltp visual_tolerance = RAT_CONST(1e-4);
		fltp visual_radius = RAT_CONST(0.0);
		arma::uword element_divisor = 1;
		arma::uword num_sub = 0;
		bool split_sub = false;
		fltp typical_scale = 0.1;
		bool use_parallel_groups = true;
		bool use_parallel_sections = false;
		bool use_parallel_extrusion = false;
	};

}}

#endif