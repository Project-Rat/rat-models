// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef MDL_CALC_FREECAD_HH
#define MDL_CALC_FREECAD_HH

// general headers
#include <armadillo> 
#include <memory>
#include <iomanip>

// rat models headers
#include "calc.hh"
#include "model.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcFreeCAD> ShCalcFreeCADPr;

	// calculate field on line defined 
	// by start and end point coordinates
	class CalcFreeCAD: public Calc{
		// properties
		protected:
			// the model to export
			ShModelPr model_;

			// output directory for all freecad files
			boost::filesystem::path output_directory_;

			// scaling factor
			fltp scaling_factor_ = RAT_CONST(1000.0); // default output in mm

			// call build files in parallel using the linux "parallel" command
			bool parallel_build_ = false;

			// run the build script from the command line
			bool run_build_ = false;

			// provide script to combine all models into a single freecad model file
			bool combine_models_ = true;

			// try create a union forming single parts
			bool create_union_ = false;

			// do not set colors and linewidths
			bool no_graphics_ = false;

			// disable graphic output during building
			bool enable_gui_update_ = false;

			// enforced number of subdivisions independent of angle
			arma::uword num_subdivide_ = 1llu;

			// subdivide long sections with angle more than 90 deg
			bool enable_refine_ = true;

			// maximum number of subdivisions when refining
			arma::uword num_refine_max_ = 100llu; // set zero to disable subdivisions

			// maximum number of nodes in one single section
			arma::uword num_node_max_ = 100000llu;

			// use ruled surface method even if 
			// it is not a perfect ruled surface
			// this will cause geometry to deviate 
			// in freecad in case of a frenet-serret frame
			bool override_ruled_surface_ = true;

			// file output
			bool enable_step_file_ = false;
			bool enable_fcstd_file_ = false;
			
		// methods
		public:
			// constructor
			CalcFreeCAD();
			explicit CalcFreeCAD(const ShModelPr &model);
			CalcFreeCAD(
				const ShModelPr &model, 
				const boost::filesystem::path& output_dir);
			
			// factory methods
			static ShCalcFreeCADPr create();
			static ShCalcFreeCADPr create(
				const ShModelPr &model);
			static ShCalcFreeCADPr create(
				const ShModelPr &model, 
				const boost::filesystem::path& output_dir);

			// setters
			virtual void set_model(const ShModelPr &model)override;
			void set_output_dir(const boost::filesystem::path& output_dir);
			void set_scaling_factor(const fltp scaling_factor);
			void set_parallel_build(const bool parallel_build = true);
			void set_run_build(const bool run_build = true);
			void set_combine_models(const bool combine_models = true);
			void set_create_union(const bool create_union = true);
			void set_no_graphics(const bool no_graphics = true);
			void set_enable_refine(const bool enable_refine = true);
			void set_num_subdivide(const arma::uword num_subdivide);
			void set_num_refine_max(const arma::uword num_refine_max);
			void set_num_node_max(const arma::uword num_node_max);
			void set_override_ruled_surface(const bool override_ruled_surface = true);
			void set_enable_gui_update(const bool enable_gui_update = true);
			void set_enable_step_file(const bool enable_step_file = true);
			void set_enable_fcstd_file(const bool enable_fcstd_file = true);

			// getters
			const ShModelPr& get_model()const;
			const boost::filesystem::path& get_output_dir()const;
			fltp get_scaling_factor()const;
			bool get_parallel_build()const;
			bool get_run_build()const;
			bool get_combine_models()const;
			bool get_create_union()const;
			bool get_no_graphics()const;
			bool get_enable_refine()const;
			arma::uword get_num_subdivide()const;
			arma::uword get_num_refine_max()const;
			arma::uword get_num_node_max()const;
			bool get_override_ruled_surface()const;
			bool get_enable_gui_update()const;
			bool get_enable_step_file()const;
			bool get_enable_fcstd_file()const;

			// freecad command
			static std::string get_freecad_command();

			// freecad calculation
			cmn::ShFreeCADPr create_freecad_file(
				const boost::filesystem::path& fpath, 
				const std::string& model_name)const;

			// generalized calculation
			void calculate_freecad(
				const fltp time = RAT_CONST(0.0), 
				const cmn::ShLogPr &lg = cmn::NullLog::create(),
				const ShSolverCachePr& cache = NULL);
			
			// calculation function
			virtual std::list<ShDataPr> calculate(
				const fltp time, 
				const cmn::ShLogPr &lg = cmn::NullLog::create(),
				const ShSolverCachePr& cache = NULL) override;

			// export to FreeCAD
			void export_freecad(
				std::ofstream& cmnd,
				const mdl::ShModelPr& model, 
				const fltp time,
				const boost::filesystem::path &output_dir, 
				const std::string &fname,
				const cmn::ShLogPr& lg)const;

			// recursive freecad creation
			void create_freecad(
				std::ofstream& cmnd,
				const mdl::ShModelPr& model, 
				const fltp time,
				const boost::filesystem::path& output_dir, 
				const mdl::ShSerializerPr& slzr, 
				const cmn::ShLogPr& lg,
				const arma::uword depth = 0)const;

			// recursive freecad creation
			void combine_freecad_core(
				std::ofstream& script, 
				std::list<boost::filesystem::path>& dependencies,
				const arma::uword depth,
				const mdl::ShModelPr& model, 
				const fltp time,
				const boost::filesystem::path& output_dir,
				const mdl::ShSerializerPr& slzr,  
				const cmn::ShLogPr& lg)const;

			// create combined freecad output file
			void combine_freecad(
				std::ofstream& script,
				const mdl::ShModelPr& model,
				const fltp time,
				const boost::filesystem::path& output_dir, 
				const mdl::ShSerializerPr& slzr,
				const cmn::ShLogPr& lg)const;

			// boolean
			void create_freecad_combine(
				std::ofstream& cmnd,
				const std::string& part_name,
				const std::list<boost::filesystem::path> &input_files, 
				const boost::filesystem::path &output_dir, 
				const cmn::ShLogPr& lg)const;

			// boolean
			void create_freecad_subtract(
				std::ofstream& cmnd,
				const std::string& part_name,
				const std::list<boost::filesystem::path> &input_files, 
				const boost::filesystem::path &output_dir, 
				const cmn::ShLogPr& lg)const;

			// boolean
			void create_freecad_intersect(
				std::ofstream& cmnd,
				const std::string& part_name,
				const std::list<boost::filesystem::path> &input_files, 
				const boost::filesystem::path &output_dir, 
				const cmn::ShLogPr& lg)const;

			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
