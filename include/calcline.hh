// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALC_LINE_HH
#define MDL_CALC_LINE_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/mlfmm/settings.hh"

#include "calcleaf.hh"
#include "linedata.hh"
#include "path.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcLine> ShCalcLinePr;

	// calculate field on line defined 
	// by start and end point coordinates
	class CalcLine: public CalcLeaf{
		// properties
		protected:
			// enable visibility
			bool visibility_ = true;

			// number of steps in line
			arma::uword num_steps_ = 1001;

			// start and end-point
			arma::Col<fltp>::fixed<3> start_point_{RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(-0.5)};
			arma::Col<fltp>::fixed<3> end_point_{RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.5)};

			// visual tolerance
			// fltp visual_tolerance_ = RAT_CONST(1.0e-4);

		// methods
		public:
			// constructor
			CalcLine();
			explicit CalcLine(
				const ShModelPr &model);
			CalcLine(
				const ShModelPr &model, 
				const arma::Col<fltp>::fixed<3> &start_point, 
				const arma::Col<fltp>::fixed<3> &end_point, 
				const arma::uword num_steps);

			// factory methods
			static ShCalcLinePr create();
			static ShCalcLinePr create(
				const ShModelPr &model);
			static ShCalcLinePr create(
				const ShModelPr &model, 
				const arma::Col<fltp>::fixed<3> &start_point, 
				const arma::Col<fltp>::fixed<3> &end_point, 
				const arma::uword num_steps = 1001);

			// enable mesh
			bool get_visibility() const;
			void set_visibility(const bool visibility);

			// set/get number of steps
			void set_num_steps(const arma::uword num_steps);
			arma::uword get_num_steps() const;

			// set start and end point
			void set_start_point(const arma::Col<fltp>::fixed<3> &start_point);
			void set_end_point(const arma::Col<fltp>::fixed<3> &end_point);

			// get start and end point
			arma::Col<fltp>::fixed<3> get_start_point() const;
			arma::Col<fltp>::fixed<3> get_end_point() const;

			// create base
			ShPathPr create_base() const;

			// calculate with inductance data output
			ShLineDataPr calculate_line(
				const fltp time = RAT_CONST(0.0), 
				const cmn::ShLogPr &lg = cmn::NullLog::create(),
				const ShSolverCachePr& cache = NULL);

			// calculation function
			virtual std::list<ShDataPr> calculate(
				const fltp time, 
				const cmn::ShLogPr &lg = cmn::NullLog::create(),
				const ShSolverCachePr& cache = NULL) override;

			// creation of calculation data objects
			std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override; // create a mesh

			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
