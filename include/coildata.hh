// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALC_COIL_HH
#define MDL_CALC_COIL_HH

#include <armadillo> 
#include <memory>
#include <cassert>

// common headers
#include "rat/common/gmshfile.hh"
#include "rat/common/log.hh"

// multipole method headers
#include "rat/mlfmm/sources.hh"

//#include "coilsurface.hh"
#include "meshdata.hh"
#include "coilsurfacedata.hh"
#include "vtkunstr.hh"
#include "vtktable.hh"
#include "data.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CoilData> ShCoilDataPr;
	typedef arma::field<ShCoilDataPr> ShCoilDataPrList;

	// mesh of a coil
	// this objects carries all properties 
	// required for the calculations
	class CoilData: public MeshData{
		// properties
		protected:
			// operating current
			fltp operating_current_;

			// number of turns
			fltp number_turns_;

		// methods
		public:
			// default constructor
			CoilData();
			CoilData(
				const ShFramePr &frame, 
				const ShAreaPr &area);
			CoilData(
				const ShFramePr &frame, 
				const ShAreaPr &area, 
				const mat::ShConductorPr &conductor);
			
			// destructor
			virtual ~CoilData(){};

			// factory
			static ShCoilDataPr create();
			static ShCoilDataPr create(
				const ShFramePr &frame, 
				const ShAreaPr &area);
			static ShCoilDataPr create(
				const ShFramePr &frame,
				const ShAreaPr &area,
				const mat::ShConductorPr &conductor);

			// create surface
			ShSurfaceDataPr create_surface() const override;

			// set the mesh material
			void set_conductor(const rat::mat::ShConductorPr &conductor);

			// setup sources and targets
			// struct GaussGrid;
			// GaussGrid setup_gauss_points()const;

			// create line currents in place
			void create_point_sources(
				arma::Mat<fltp>& Rs,
				arma::Mat<fltp>& dRs,
				arma::Row<fltp>& epss,
				arma::Row<fltp>& Is)const;
			// void create_line_elements(
			// 	arma::Mat<fltp>& Rs,
			// 	arma::Mat<fltp>& dRs,
			// 	arma::Row<fltp>& epss,
			// 	arma::Row<fltp>& Acrss,
			// 	arma::Row<fltp>& Is,
			// 	const arma::sword num_gauss)const;
			rat::fmm::ShSourcesPr create_currents()const;
			
			// setters
			void set_operating_current(const fltp operating_current);
			void set_number_turns(const fltp number_turns);

			// getters
			fltp get_operating_current() const override;
			fltp get_number_turns() const override;
			arma::sword get_num_gauss()const;
			arma::uword calc_num_sources_per_element()const;

			// calculate
			// arma::Row<fltp> calc_element_current(
			// 	const arma::Row<fltp>& cis_area, 
			// 	const arma::Row<fltp>& cis_total_area)const;
			arma::Row<fltp> calc_element_size(
				const arma::Row<fltp>& cis_delem)const;
			arma::Row<fltp> calc_cis_current_density()const;
			arma::Row<fltp> calc_cs_current_density()const;
			fltp calculate_flux() const override;
			fltp calculate_magnetic_energy()const override;
			// fltp calculate_element_self_inductance() const override;

			// setup and get current density
			arma::Row<fltp> calc_current_density()const;
			arma::Row<fltp> calc_current_density(const arma::Row<fltp>& cis_current_density)const;

			// export
			ShVTKObjPr export_vtk() const override;
			// ShVTKTablePr export_line_elements() const;
			
			// calculate properties at nodes
			virtual arma::Row<fltp> calc_critical_current_density(
				const bool use_current_sharing = false, 
				const FieldAngleType angle_type = FieldAngleType::CONE,
				const bool use_parallel = true)const override;
			virtual arma::Mat<fltp> calc_nodal_current_density()const override;
			virtual arma::Row<fltp> calc_critical_temperature(
				const bool use_current_sharing = false, 
				const FieldAngleType angle_type = FieldAngleType::CONE,
				const bool use_parallel = true) const override;
			virtual arma::Row<fltp> calc_temperature_margin(
				const bool use_current_sharing = false, 
				const FieldAngleType angle_type = FieldAngleType::CONE,
				const bool use_parallel = true) const override;
			virtual arma::Mat<fltp> calc_electric_field(
				const bool use_current_sharing = false, 
				const FieldAngleType angle_type = FieldAngleType::CONE,
				const bool use_parallel = true) const override;
			virtual arma::Row<fltp> calc_power_density(
				const bool use_current_sharing = false, 
				const FieldAngleType angle_type = FieldAngleType::CONE,
				const bool use_parallel = true) const override;
			virtual arma::Row<fltp> calc_loadline_fraction(
				const bool use_current_sharing = false, 
				const FieldAngleType angle_type = FieldAngleType::CONE,
				const bool use_parallel = true) const override;
			virtual arma::Row<fltp> calc_critical_current_fraction(
				const bool use_current_sharing = false, 
				const FieldAngleType angle_type = FieldAngleType::CONE,
				const bool use_parallel = true) const override;
			virtual arma::Row<fltp> calc_pressure(
				const arma::uword int_dir, const bool use_parallel = true)const override;
			virtual arma::Mat<fltp> calc_force_density() const override;
			virtual arma::Row<fltp> calc_voltage(
				const bool use_current_sharing = false, 
				const FieldAngleType angle_type = FieldAngleType::CONE,
				const bool use_parallel = true)const override;

			// setup targets
			virtual void setup_targets() override;

			// override field
			virtual arma::Mat<fltp> get_field(const char field_type)const override;
			arma::Mat<fltp> get_field_gp(const char field_type)const;

			// integrate lorentz force density
			virtual arma::Col<fltp>::fixed<3> calc_lorentz_force() const override;
	};

	// // gauss grid data
	// struct CoilData::GaussGrid{
	// 	// number of gauss points
	// 	arma::uword num_points;

	// 	// quadrilateral coordinates
	// 	arma::Mat<fltp> Rq;

	// 	// weights for length and area
	// 	arma::Row<fltp> ell_scale;
	// 	arma::Row<fltp> area_scale;
	// };

}}

#endif
