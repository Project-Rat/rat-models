// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PROTECTION_CIRCUIT_HH
#define MDL_PROTECTION_CIRCUIT_HH

#include <armadillo> 
#include <memory>

#include "circuit.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ProtectionCircuit> ShProtectionCircuitPr;

	// a collection of coils
	class ProtectionCircuit: virtual public Circuit{
		// set coils
		protected:
			// metrosil
			bool use_varistor_ = false;

			// rapid
			bool is_rapid_ = false;

			// dump resistor [Ohm]
			fltp protection_voltage_ = RAT_CONST(1000.0); // [V]
			fltp protection_current_ = RAT_CONST(1000.0); // [A]

			// detection treshold voltage [V]
			fltp detection_voltage_ = RAT_CONST(100e-3); // [V]

			// propagation in transverse direction
			fltp transverse_propagation_fraction_ = RAT_CONST(0.0);

			// delay time [s]
			fltp detection_delay_time_ = RAT_CONST(40e-3); // [s]

			// switch time
			fltp switching_time_ = RAT_CONST(20e-3); // [s]

			// metrosil specific settings
			fltp metrosil_exponent_ = RAT_CONST(0.42); // usually between 0.3 and 0.5

			// heater delay time
			fltp heater_delay_time_ = RAT_CONST(1e99);

			// percentage of coils hit by heater
			fltp fraction_hit_ = RAT_CONST(0.8);



		// methods
		public:
			// consrtuctor
			ProtectionCircuit();
			static ShProtectionCircuitPr create();

			// setters
			void set_use_varistor(const bool use_varistor = true);
			void set_is_rapid(const bool is_rapid = true);
			void set_protection_voltage(const fltp protection_voltage);
			void set_protection_current(const fltp protection_current);
			void set_detection_voltage(const fltp detection_voltage);
			void set_detection_delay_time(const fltp detection_delay_time);
			void set_switching_time(const fltp switching_time);
			void set_metrosil_exponent(const fltp metrosil_exponent);
			void set_heater_delay_time(const fltp heater_delay_time);
			void set_fraction_hit(const fltp fraction_hit);
			void set_transverse_propagation_fraction(const fltp transverse_propagation_fraction);

			// getters
			bool get_use_varistor()const;
			virtual bool get_is_rapid()const override;
			fltp get_protection_voltage()const;
			fltp get_protection_current()const;
			virtual fltp get_detection_voltage()const override;
			virtual fltp get_detection_delay_time()const override;
			virtual fltp get_switching_time()const override;
			fltp get_metrosil_exponent()const;
			virtual fltp get_heater_delay_time()const override;
			virtual fltp get_fraction_hit()const override;
			virtual fltp get_transverse_propagation_fraction()const override;

			// calculate voltage
			virtual fltp calc_protection_voltage(const fltp current)const override;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif







