// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_BAR_HH
#define MDL_MODEL_BAR_HH

#include <armadillo> 
#include <memory>

#include "rat/mat/conductor.hh"

#include "modelmesh.hh"
#include "bardata.hh"
#include "drivedc.hh"
#include "drive.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelBar> ShModelBarPr;

	// current carying coil extruded with
	// a base path and a cross section
	class ModelBar: public ModelMesh{
		// properties
		protected:
			// softening factor
			fltp softening_ = RAT_CONST(1.4);

			// magnetisation in frame coordinates
			arma::Col<fltp>::fixed<3> Mf_{RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0)}; // M_L,M_N,M_D

			// number of gauss points
			arma::sword num_gauss_surface_ = 2;
			arma::sword num_gauss_volume_ = 2;

			// current drive
			ShDrivePr magnetisation_drive_ = DriveDC::create();

		// methods
		public:
			// constructor
			ModelBar();
			ModelBar(
				const ShPathPr &input_path, 
				const ShCrossPr &input_cross);

			// factory methods
			static ShModelBarPr create();
			static ShModelBarPr create(
				const ShPathPr &input_path, 
				const ShCrossPr &input_cross);

			// setters
			void set_softening(const fltp softening); // softening factor
			void set_num_gauss_volume(const arma::sword num_gauss_volume);
			void set_num_gauss_surface(const arma::sword num_gauss_surface);
			void set_magnetisation(const arma::Col<fltp>::fixed<3> &Mf);
			void set_magnetisation_drive(const ShDrivePr &magnetisation_drive);

			// getters
			fltp get_softening() const;
			arma::sword get_num_gauss_surface()const;
			arma::sword get_num_gauss_volume()const;
			arma::Col<fltp>::fixed<3> get_magnetisation() const;

			// create data
			ShMeshDataPr create_data()const override;
			
			// create specific mesh
			virtual std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
