// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALC_IMPORT_HH
#define MDL_CALC_IMPORT_HH

#include <armadillo> 
#include <memory>

#include "model.hh"
#include "importdata.hh"
#include "calcleaf.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcImport> ShCalcImportPr;

	// model object that is capable of creating meshes
	// using a base path and a cross section
	class CalcImport: public CalcLeaf{
		// properties
		protected:
			// input file
			boost::filesystem::path fpath_;

			// position
			arma::Col<fltp>::fixed<3> R0_;

			// scaling factor
			fltp scaling_factor_ = RAT_CONST(1.0);

		// methods
		public:
			// constructor
			CalcImport();
			explicit CalcImport(
				const boost::filesystem::path &fpath, 
				const arma::Col<fltp>::fixed<3> &R0, 
				const rat::fltp scaling_factor);

			// factory methods
			static ShCalcImportPr create();
			static ShCalcImportPr create(
				const boost::filesystem::path &fpath, 
				const arma::Col<fltp>::fixed<3> &R0, 
				const rat::fltp scaling_factor);

			// set and get
			void set_fpath(const boost::filesystem::path &fpath);
			void set_scaling_factor(const rat::fltp scaling_factor);
			void set_position(const arma::Col<fltp>::fixed<3> &R0);
			boost::filesystem::path get_fpath() const;
			fltp get_scaling_factor()const;
			arma::Col<fltp>::fixed<3> get_position() const;
			
			// calculate with inductance data output
			ShImportDataPr calculate_import(
				const fltp time = RAT_CONST(0.0), 
				const cmn::ShLogPr &lg = cmn::NullLog::create(),
				const ShSolverCachePr& cache = NULL);
			
			// calculation function
			virtual std::list<ShDataPr> calculate(
				const fltp time, 
				const cmn::ShLogPr &lg = cmn::NullLog::create(),
				const ShSolverCachePr& cache = NULL) override;

			// input validity
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &fpath) override;
	};

}}

#endif
