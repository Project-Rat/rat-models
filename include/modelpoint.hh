// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_POINT_HH
#define MDL_MODEL_POINT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "modelmeshwrapper.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelPoint> ShModelPointPr;

	// line defined by start and end point
	class ModelPoint: public ModelMeshWrapper{
		// properties
		protected:
			arma::Col<fltp>::fixed<3> R_{RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0)}; 
			arma::Col<fltp>::fixed<3> L_{RAT_CONST(0.0),RAT_CONST(1.0),RAT_CONST(0.0)}; 
			arma::Col<fltp>::fixed<3> D_{RAT_CONST(1.0),RAT_CONST(0.0),RAT_CONST(0.0)}; 

		// methods
		public:
			// constructor
			ModelPoint();
			ModelPoint(const arma::Col<fltp>::fixed<3> &R);

			// factory
			static ShModelPointPr create();
			static ShModelPointPr create(const arma::Col<fltp>::fixed<3> &R);

			// get base and cross
			ShPathPr get_input_path() const override;
			ShCrossPr get_input_cross() const override;

			// set properties
			void set_coordinate(const arma::Col<fltp>::fixed<3> &R1);

			// get properties
			arma::Col<fltp>::fixed<3> get_coordinate() const;
			
			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
