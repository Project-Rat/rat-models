// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_POLYGON_HH
#define MDL_PATH_POLYGON_HH

#include <armadillo> 
#include <cassert>
#include <memory>

#include "path.hh"
#include "pathbspline.hh"
#include "pathgroup.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathPolygon> ShPathPolygonPr;

	// cross section of coil
	class PathPolygon: public Path, public Transformations{
		// properties
		private:
			// settings
			arma::uword num_sides_ = 6;
			fltp alpha_ = 0.1;
			fltp element_size_ = 1e-3;
			fltp radius_ = 0.02;
			fltp offset_ = 0.0;

		// methods
		public:
			// constructor
			PathPolygon();
			PathPolygon(const arma::uword num_sides, const fltp alpha, const fltp element_size);

			// factory methods
			static ShPathPolygonPr create();
			static ShPathPolygonPr create(const arma::uword num_sides, const fltp alpha, const fltp element_size);

			// set properties
			void set_num_sides(const arma::uword num_sides);
			void set_alpha(const fltp alpha);
			void set_element_size(const fltp element_size);
			void set_radius(const fltp radius);
			void set_offset(const fltp offset);

			// get properties
			arma::uword get_num_sides() const;
			fltp get_alpha() const;
			fltp get_element_size() const;
			fltp get_radius() const;
			fltp get_offset() const;

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// calculate important lengths
			fltp calc_side_length() const;
			fltp calc_arc_length() const;
			fltp calc_corner_length() const;

			// validity check
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif