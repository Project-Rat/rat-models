// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CROSS_WEDGE_HH
#define MDL_CROSS_WEDGE_HH

#include <armadillo> 
#include <memory>
#include <cmath>
#include <json/json.h>

#include "area.hh"
#include "cross.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CrossWedge> ShCrossWedgePr;

	// template class for cross section
	class CrossWedge: public Cross{
		// properties
		protected:
			// dimensions in [m]
			fltp surface_offset_ = RAT_CONST(0.0);
			fltp dinner_ = RAT_CONST(2e-3);
			fltp douter_ = RAT_CONST(2.2e-3);
			fltp t1_ = RAT_CONST(0.0);
			fltp t2_ = RAT_CONST(10e-3);

			// element size in [m]
			arma::uword num_thickness_ = 2llu;
			arma::uword num_width_ = 10llu;

		// methods
		public:
			// constructor
			CrossWedge();
			CrossWedge(
				const fltp dinner, 
				const fltp douter,
				const fltp t1, 
				const fltp t2, 
				const arma::uword num_thickness,
				const arma::uword num_width);

			// factory
			static ShCrossWedgePr create();
			static ShCrossWedgePr create(
				const fltp dinner, 
				const fltp douter,
				const fltp t1, 
				const fltp t2, 
				const arma::uword num_thickness,
				const arma::uword num_width);

			// setters
			void set_dinner(const fltp dinner); 
			void set_douter(const fltp douter); 
			void set_t1(const fltp t1);
			void set_t2(const fltp t2);
			void set_num_thickness(const arma::uword num_thickness); 
			void set_num_width(const arma::uword num_width); 
			void set_surface_offset(const fltp surface_offset);

			// getters
			fltp get_dinner()const; 
			fltp get_douter()const; 
			fltp get_t1()const;
			fltp get_t2()const;
			arma::uword get_num_thickness()const; 
			arma::uword get_num_width()const; 
			fltp get_surface_offset()const;

			// keystone angle
			fltp calc_keystone_angle()const;

			// get bounding box
			virtual arma::Mat<fltp>::fixed<2,2> get_bounding() const override;

			// volume mesh
			virtual ShAreaPr create_area(const MeshSettings &stngs) const override;

			// validity check
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
