// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef MDL_HB_CURVE_VLV_HH
#define MDL_HB_CURVE_VLV_HH

// only available for non-linear solver
#ifdef ENABLE_NL_SOLVER

// general headers
#include <armadillo> 
#include <cassert>
#include <algorithm>
#include <memory>

// common headers
#include "rat/common/node.hh"

// nonlinear solver headers
#include "hbcurve.hh"

// code specific to Raccoon
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class HBCurveVLV> ShHBCurveVLVPr;
	typedef arma::field<ShHBCurveVLVPr> ShHBCurveVLVPrList;

	// hb curve containing data table
	class HBCurveVLV: public HBCurve{
		// properties
		protected:
			// upper magnetic field
			fltp H2_ = RAT_CONST(60.0e3f);

			// relative permeability
			fltp mur_ = RAT_CONST(500.0f);

			// saturation field
			fltp Js_ = RAT_CONST(2.0f);

			// filling fraction
			fltp ff_ = RAT_CONST(1.0f);

			// number of points
			arma::uword num_points_ = 100;


		// methods
		public:
			// constructor
			HBCurveVLV();
			HBCurveVLV(
				const fltp mur, 
				const fltp Js, 
				const fltp ff = RAT_CONST(1.0), 
				const fltp H2 = RAT_CONST(60e3),
				const arma::uword num_points = 100);

			// factory
			static ShHBCurveVLVPr create();
			static ShHBCurveVLVPr create(
				const fltp mur, 
				const fltp Js, 
				const fltp ff = RAT_CONST(1.0), 
				const fltp H2 = RAT_CONST(60e3),
				const arma::uword num_points = 100);
			
			// setters
			void set_relative_permeability(const fltp mur);
			void set_saturation_field(const fltp Js);
			void set_filling_fraction(const fltp ff);
			void set_num_points(const arma::uword num_points);
			void set_upper_magnetic_field(const fltp H2);

			// getters
			fltp get_relative_permeability()const;
			fltp get_saturation_field()const;
			fltp get_filling_fraction()const;
			arma::uword get_num_points()const;
			fltp get_upper_magnetic_field()const;
			arma::Mat<fltp> get_table()const;

			// create hb data
			virtual nl::ShHBDataPr create_hb_data()const override;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
#endif