// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MESH_HH
#define MDL_MESH_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/mat/conductor.hh"

#include "rat/mlfmm/sources.hh"

#include "rat/common/gmshfile.hh"
#include "rat/common/log.hh"
#include "rat/common/node.hh"
#include "rat/common/freecad.hh"
#include "rat/common/opera.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/abaqusfile.hh"

#include "area.hh"
#include "frame.hh"
#include "mesh.hh"
#include "trans.hh"
#include "surfacedata.hh"
#include "vtkunstr.hh"
#include "targetdata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class MeshData> ShMeshDataPr;
	typedef arma::field<ShMeshDataPr> ShMeshDataPrList;

	// mesh of a coil
	// this objects carries all properties 
	// required for the calculations
	class MeshData: public Mesh, public TargetData{
		// enums
		public:
			// field angle calculation type
			// CONE : the field angle with respect to the LD plane
			// TRANSVERSE : the field angle inside the ND plane
			// LONGITUDINAL : the field angle inside the LN plane
			// PLANE : the field angle inside the LD plane
			enum class FieldAngleType{CONE, BINORMAL, LONGITUDINAL, PLANE};

		// properties
		protected:
			// softening factor for the source elements
			fltp softening_ = RAT_CONST(1.0);

			// number of gauss points requested
			arma::sword num_gauss_surface_ = 2;
			arma::sword num_gauss_volume_ = 2;

			// part name
			std::string part_name_ = "none";

			// color
			bool use_custom_color_ = false;
			arma::Col<float>::fixed<3> color_;

			// circuit index
			arma::uword circuit_index_ = 0;

			// mesh is made by calculation
			bool calc_mesh_ = false;

			// homogenized properties throughout cross section
			bool enable_current_sharing_ = false;

			// operating temperature
			fltp operating_temperature_ = RAT_CONST(0.0);

			// node temperatures
			arma::Row<fltp> temperature_;

			// trace of indexes storing the 
			// branches of the tree
			std::list<arma::uword> trace_;

		// methods
		public:
			// constructors
			MeshData(); // default
			MeshData(
				const ShFramePr &frame, 
				const ShAreaPr &area,
				const bool use_parallel);
			MeshData(
				const arma::Mat<fltp> &R, 
				const arma::Mat<arma::uword> &n, 
				const arma::Mat<arma::uword> &s);
			
			// factory
			static ShMeshDataPr create();
			static ShMeshDataPr create(
				const ShFramePr &frame, 
				const ShAreaPr &area,
				const bool use_parallel = false);
			static ShMeshDataPr create(
				const arma::Mat<fltp> &R, 
				const arma::Mat<arma::uword> &n, 
				const arma::Mat<arma::uword> &s);

			// virtual destructor
			virtual ~MeshData(){};

			// set part name
			void set_part_name(const std::string& part_name);
			const std::string& get_part_name()const;

			// number of gauss points
			void set_softening(
				const fltp softening);
			void set_num_gauss(
				const arma::sword num_gauss_volume, 
				const arma::sword num_gauss_surface);
			void set_num_gauss_surface(
				const arma::sword num_gauss_surface);
			void set_num_gauss_volume(
				const arma::sword num_gauss_volume);
			arma::sword get_num_gauss_surface()const;
			arma::sword get_num_gauss_volume()const;
			arma::uword get_num_gauss_per_volume_element()const;
			arma::uword get_num_gauss_per_surface_element()const;

			// set temperature
			void set_temperature(const fltp temperature); // at nodes same everywhere
			void set_temperature(const arma::Row<fltp> &temperature); // at nodes
			const arma::Row<fltp>& get_temperature()const;

			// where does the mesh come from
			void set_trace(const std::list<arma::uword>& trace);
			const std::list<arma::uword>& get_trace()const;
			void append_trace_id(const arma::uword trace_id);
			void clear_trace_id();
			void set_calc_mesh(const bool calc_mesh = true);
			// void set_field_at_nodes(const bool field_at_nodes = true);
			bool get_calc_mesh() const;

			// set operating temperature
			void set_operating_temperature(const fltp operating_temperature); // overall
			
			// get operating conditions
			fltp get_operating_temperature()const;
			virtual fltp get_number_turns()const; // to be overridden by CoilData
			virtual fltp get_operating_current()const; // to be overridden by CoilData
			fltp get_num_turns() const;

			// set mesh color
			void set_use_custom_color(const bool use_custom_color);
			void set_color(const arma::Col<float>::fixed<3> &color);

			// get mesh color
			bool get_use_custom_color()const;
			const arma::Col<float>::fixed<3>& get_color()const;

			// get connectivity for raccoon
			void set_enable_current_sharing(const bool enable_current_sharing);
			void set_circuit_index(const arma::uword circuit_index);

			// get connectivity for raccoon
			bool get_enable_current_sharing()const;
			arma::uword get_circuit_index()const;

			// calculate the captured flux in this mesh
			// requires the vector potential to be calculated
			// output in [Vs]
			virtual fltp calculate_flux()const;
			virtual fltp calculate_magnetic_energy()const;

			// // calculate the self inductance of all the
			// // line elements combined in [H]
			// // this is NOT the total inductance
			// virtual fltp calculate_element_self_inductance() const;

			// // calculate the average length of each cable section
			// // using the respective volume and cross sectional area
			// // output in [m]
			// arma::Row<fltp> calc_cs_length(
			// 	const arma::Row<fltp>& volume, 
			// 	const arma::Row<fltp>& cis_total_area)const;

			// // same but with internal volume and area calculation
			// arma::Row<fltp> calc_cs_length()const;

			// setup surface mesh
			void setup_surface(const ShSurfaceDataPr &surf) const;
			virtual ShSurfaceDataPr create_surface() const;
			
			// create an interpolation mesh
			fmm::ShInterpPr create_interp() const;

			// relative permeability
			virtual arma::Row<fltp> calc_relative_permeability()const;

			// reluctivity
			virtual arma::Row<fltp> calc_relative_reluctivity()const;

			// calculate properties at nodes for analysis
			virtual arma::Mat<fltp> calc_force_density() const;

			// calculate incident angle of the magnetic field
			virtual arma::Row<fltp> calc_magnetic_field_angle(
				const FieldAngleType type = FieldAngleType::CONE)const;

			// calculate the critical current density [A/m^2]
			virtual arma::Row<fltp> calc_critical_current_density(
				const bool use_current_sharing = false,
				const FieldAngleType type = FieldAngleType::CONE, 
				const bool use_parallel = true)const;

			// calculate the current density vector at the nodes in [A/m^2]
			virtual arma::Mat<fltp> calc_nodal_current_density()const;

			// calculate the fraction on the loadline
			virtual arma::Row<fltp> calc_loadline_fraction(
				const bool use_current_sharing = false, 
				const FieldAngleType type = FieldAngleType::CONE,
				const bool use_parallel = true) const;

			// calculate the fraction of the critical current I/Ic
			virtual arma::Row<fltp> calc_critical_current_fraction(
				const bool use_current_sharing = false, 
				const FieldAngleType type = FieldAngleType::CONE,
				const bool use_parallel = true) const;


			// calculate the critical temperature in [K]
			virtual arma::Row<fltp> calc_critical_temperature(
				const bool use_current_sharing = false,
				const FieldAngleType type = FieldAngleType::CONE, 
				const bool use_parallel = true) const;

			// calculate the temperature margin in [K]
			virtual arma::Row<fltp> calc_temperature_margin(
				const bool use_current_sharing = false, 
				const FieldAngleType type = FieldAngleType::CONE,
				const bool use_parallel = true) const;


			// calculate the electric field in [V/m]
			virtual arma::Mat<fltp> calc_electric_field(
				const bool use_current_sharing = false, 
				const FieldAngleType type = FieldAngleType::CONE,
				const bool use_parallel = true) const;

			// calculate the electrical power density in [W/m^3]
			virtual arma::Row<fltp> calc_power_density(
				const bool use_current_sharing = false,
				const FieldAngleType type = FieldAngleType::CONE, 
				const bool use_parallel = true) const;

			// calculate voltage
			virtual arma::Row<fltp> calc_voltage(
				const bool use_current_sharing = false, 
				const FieldAngleType type = FieldAngleType::CONE,
				const bool use_parallel = true)const;

			// the pressure calculations are a primitive way 
			// to integrate the coil force density to the outside
			// of the coil pack. These are in no way a replacement
			// for proper FEM
			// calculate von mises in [Pa]
			arma::Row<fltp> calc_von_mises()const;

			// calculate the accumulated pressure in the coil pack in [Pa]
			virtual arma::Mat<fltp> calc_pressure_vector()const;

			// calculate the pressure in the normal direction in [Pa]
			virtual arma::Row<fltp> calc_normal_pressure()const;

			// calculate the pressure in the transverse direction in [Pa]
			virtual arma::Row<fltp> calc_transverse_pressure()const;

			// calculate the pressure in the given direction in [Pa]
			virtual arma::Row<fltp> calc_pressure(
				const arma::uword int_dir, 
				const bool use_parallel = true)const;


			// calculate integrated Lorentz forces
			virtual arma::Col<fltp>::fixed<3> calc_lorentz_force() const;

			// special MFOM calculation for BabyIaxo project
			fltp calculate_mfom_2d(fltp bore_length = 0) const;
			fltp calculate_mfom_3d() const;


			// export mesh to VTK data object
			virtual ShVTKObjPr export_vtk() const override;


			// write the mesh to opera object
			virtual void export_opera(const cmn::ShOperaPr &opera) const;

			// write the mesh to freecad object
			void export_freecad(const cmn::ShFreeCADPr &freecad) const;

			// write the mesh to gmsh data object
			void export_gmsh(const cmn::ShGmshFilePr &gmsh, const arma::uword idx)const;

			// write the mesh to abaqus data object
			void export_abaqus(const cmn::ShAbaqusFilePr &abaqus) const;

			// setup targets
			virtual void setup_targets() override;

			// display function for multiple meshes
			static void display(
				const cmn::ShLogPr &lg, 
				const std::list<ShMeshDataPr> &meshes);	
	};

}}

#endif
