// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_STAIRCABLE_HH
#define MDL_PATH_STAIRCABLE_HH

#include <armadillo> 
#include <memory>

#include "rat/common/extra.hh"

#include "path.hh"
#include "inputpath.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathStairCable> ShPathStairCablePr;

	// path defining the shape of the cable
	// uses the coil path as input
	class PathStairCable: public rat::mdl::Path, public rat::mdl::InputPath, public rat::mdl::Transformations{
		// properties
		protected:
			bool extend_ends_ = false;
			bool flatten_cable_ = false;
			rat::fltp pitch_ = 0.1;
			rat::fltp spacing_ = 2e-3;
			arma::uword num_strands_ = 5;

		// methods
		public:
			// constructor
			PathStairCable();
			explicit PathStairCable(const rat::mdl::ShPathPr &base);
			
			// factory methods
			static ShPathStairCablePr create();
			static ShPathStairCablePr create(const rat::mdl::ShPathPr &base);

			// setters
			void set_pitch(const rat::fltp pitch);
			void set_spacing(const rat::fltp spacing);
			void set_num_strands(const arma::uword num_strands);
			void set_flatten_cable(const bool flatten_cable = true);

			// getters
			rat::fltp get_pitch()const;
			rat::fltp get_spacing()const;
			arma::uword get_num_strands()const;
			bool get_flatten_cable()const;

			// get frame
			virtual rat::mdl::ShFramePr create_frame(const rat::mdl::MeshSettings& stngs) const override;

			// validity check
			bool is_valid(const bool enable_throws = false) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				rat::cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				rat::cmn::DSList &list, 
				const rat::cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
