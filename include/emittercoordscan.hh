// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_EMITTER_COORD_SCAN_HH
#define MDL_EMITTER_COORD_SCAN_HH

#include <armadillo> 
#include <memory>

#include "particle.hh"
#include "emitter.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class EmitterCoordScan> ShEmitterCoordScanPr;

	// emitter for accelerator beam
	class EmitterCoordScan: public Emitter{
		public:
			typedef std::shared_ptr<class EmitterCoordScan> shptr; // Dani

		protected:
			// following unit system from CERN MAD
			// particle settings
			fltp beam_energy_ = 0; // [GeV] including rest mass
			fltp rest_mass_ = 0; // [GeV/C^2]
			fltp charge_ = 0; // elementary charge

			// Scan ranges - actually they are the half of the scan range
			fltp x_range_ = 1e-2;  // scan range, by default within [-1cm .. 1cm]
			arma::uword nx_ = 11;  // by default, 11 points are generated. 
			fltp xa_range_ = 1e-3; // [-1mrad .. 1mrad]
			arma::uword nxa_ = 11;  
			fltp y_range_ = 1e-2;
			arma::uword ny_ = 11;
			fltp ya_range_ = 1e-3;
			arma::uword nya_ = 11;
			fltp pt_range_ = 1e-2; // [-1% .. 1%]
			arma::uword npt_ = 11;

			// track settings
			arma::uword lifetime_ = 501;
			arma::uword start_idx_ = 250;


			// this coordinate and orientation 
			// can also be adjusted using transformations instead
			// this is more compatible with the GUI
			// emitter location
			arma::Col<fltp>::fixed<3> R_{0,0,0};
			
			// emitter orientation
			arma::Col<fltp>::fixed<3> L_{0,1,0};
			arma::Col<fltp>::fixed<3> N_{1,0,0};
			arma::Col<fltp>::fixed<3> D_{0,0,1};

		public:
			// constructor
			EmitterCoordScan();
					
			// factory
			static ShEmitterCoordScanPr create();

			// default particles
			void set_proton();
			void set_electron();
			// void set_field_line(); // set TrackingType::FIELDLINES in CalcTracks instead
			
			// setting of properties
			void set_beam_energy(const fltp beam_energy);
			void set_rest_mass(const fltp rest_mass);
			void set_charge(const fltp charge);
			void set_lifetime(const arma::uword lifetime);
			void set_start_idx(const arma::uword start_idx);
			void set_spawn_coord(
				const arma::Col<fltp>::fixed<3> &R, 
				const arma::Col<fltp>::fixed<3> &L, 
				const arma::Col<fltp>::fixed<3> &N, 
				const arma::Col<fltp>::fixed<3> &D);

			// getting properties
			fltp get_rest_mass() const;

			EmitterCoordScan &set_xrange(const fltp xrange) { x_range_ = xrange; if(xrange==0.0) nx_=1; return *this; }
			EmitterCoordScan &set_nx(const arma::uword nx) { nx_ = nx; return *this; }
			EmitterCoordScan &set_xrange(const fltp xrange,const arma::uword nx) { x_range_ = xrange; nx_ = nx; return *this; }

			EmitterCoordScan &set_xarange(const fltp xarange) { xa_range_ = xarange; if(xarange==0.0) nxa_=1; return *this; }
			EmitterCoordScan &set_nxa(const arma::uword nxa) { nxa_ = nxa; return *this; }
			EmitterCoordScan &set_xarange(const fltp xarange,arma::uword nxa) { xa_range_ = xarange; nxa_ = nxa; return *this; }

			EmitterCoordScan &set_yrange(const fltp yrange) { y_range_ = yrange; if(yrange==0.0) ny_=1; return *this; }
			EmitterCoordScan &set_ny(const arma::uword ny) { ny_ = ny; return *this; }
			EmitterCoordScan &set_yrange(const fltp yrange,const arma::uword ny) { y_range_ = yrange; ny_ = ny; return *this; }

			EmitterCoordScan &set_yarange(const fltp yarange) { ya_range_ = yarange; if(yarange==0.0) nya_=1; return *this; }
			EmitterCoordScan &set_nya(const arma::uword nya) { nya_ = nya; return *this; }
			EmitterCoordScan &set_yarange(const fltp yarange,const arma::uword nya) { ya_range_ = yarange; nya_ = nya; return *this; }

			EmitterCoordScan &set_ptrange(const fltp ptrange) { pt_range_ = ptrange; if(ptrange==0.0) npt_=1; return *this; }
			EmitterCoordScan &set_npt(const arma::uword npt) { npt_ = npt; return *this; }
			EmitterCoordScan &set_ptrange(const fltp ptrange,const arma::uword npt) { pt_range_ = ptrange; npt_ = npt; return *this; }

			// Generate a particle with transverse phase-space coordinates (x,xa) and (y,ya), and relative energy error pt (all according to MAD-X convention)
			void setup_particle(Particle &particle, const fltp x, const fltp xa, const fltp y, const fltp ya, const fltp pt, const fltp time) const;
			arma::field<Particle> spawn_particles(const fltp time) const override;

			// create specific mesh
			virtual std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
