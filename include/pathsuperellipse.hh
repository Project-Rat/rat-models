// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_SUPER_ELLIPSE_HH
#define MDL_PATH_SUPER_ELLIPSE_HH

#include <armadillo> 
#include <memory>
#include <cassert>
#include <json/json.h>

#include "path.hh"
#include "frame.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathSuperEllipse> ShPathSuperEllipsePr;

	// cross section of coil
	class PathSuperEllipse: public Path, public Transformations{
		// properties
		protected:
			// reverse option
			bool reverse_ = false;

			// number of poles
			arma::uword num_poles_ = 1; // 1 dipoles, 2 quadrupole, 3 sextupole etc.

			// order of the superellipse
			fltp n1_ = RAT_CONST(2.0); // for straight section
			fltp n2_ = RAT_CONST(2.0); // for coil end

			// circle radius
			fltp radius_ = RAT_CONST(25e-3);

			// start angle
			fltp phi_ = RAT_CONST(0.0);

			// arclength
			fltp arclength_ = arma::Datum<fltp>::pi/2;

			// element size
			fltp element_size_ = RAT_CONST(2e-3);

			// design variable
			fltp ba_ = RAT_CONST(0.05);

			// cone angle i.e. the angle between the legs
			fltp beta_ = RAT_CONST(0.0);

			// extra twist angle
			fltp zeta1_ = RAT_CONST(0.0); // adjustment of angle at onset of the coil
			fltp zeta2_ = RAT_CONST(0.0); // adjustment of angle at the nose of the coil

			// radius increment
			fltp delta_radius_ = RAT_CONST(0.0);

		// methods
		public:
			// constructor
			PathSuperEllipse(
				const fltp radius,
				const fltp ba,
				const fltp beta,
				const fltp phi,
				const fltp arclength,
				const fltp element_size,
				const arma::uword num_poles = 1llu,
				const fltp n1 = RAT_CONST(2.0),
				const fltp n2 = RAT_CONST(2.0),
				const fltp zeta1 = RAT_CONST(0.0),
				const fltp zeta2 = RAT_CONST(0.0),
				const fltp delta_radius = RAT_CONST(0.0));
			PathSuperEllipse();

			// factory
			static ShPathSuperEllipsePr create();
			static ShPathSuperEllipsePr create(
				const fltp radius,
				const fltp ba,
				const fltp beta,
				const fltp phi,
				const fltp arclength,
				const fltp element_size,
				const arma::uword num_poles = 1llu,
				const fltp n1 = RAT_CONST(2.0),
				const fltp n2 = RAT_CONST(2.0),
				const fltp zeta1 = RAT_CONST(0.0),
				const fltp zeta2 = RAT_CONST(0.0),
				const fltp delta_radius = RAT_CONST(0.0));

			// setters
			void set_num_poles(const arma::uword num_poles);
			void set_reverse(const bool reverse = true);
			void set_n1(const fltp n1);
			void set_n2(const fltp n2);
			void set_radius(const fltp radius);
			void set_phi(const fltp phi);
			void set_arclength(const fltp arclength);
			void set_element_size(const fltp element_size);
			void set_ba(const fltp ba);
			void set_beta(const fltp beta);
			void set_zeta1(const fltp zeta1);
			void set_zeta2(const fltp zeta2);
			void set_delta_radius(const fltp delta_radius);

			// getters
			arma::uword get_num_poles()const;
			bool get_reverse()const;
			fltp get_n1() const;
			fltp get_n2() const;
			fltp get_radius() const;
			fltp get_phi() const;
			fltp get_arclength() const;
			fltp get_element_size() const;
			fltp get_ba() const;
			fltp get_beta() const;
			fltp get_zeta1() const;
			fltp get_zeta2() const;
			fltp get_delta_radius() const;

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// validity check
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
