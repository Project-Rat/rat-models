// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_EDGES_HH
#define MDL_MODEL_EDGES_HH

#include <armadillo> 
#include <memory>

#include "model.hh"
#include "path.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelEdges> ShModelEdgesPr;

	// model object that is capable of creating meshes
	// using a base path and a cross section
	class ModelEdges: public Model{
		// properties
		protected:
			// file containing edge table (must have size 4)
			// edges:
			// o-->n
			// | 0---1
			// t |   |
			//   3---2

			// list of pointers to the path sections	
			std::map<arma::uword, ShPathPr> input_paths_;

			// number of elements in the two directions
			arma::uword num_n_ = 1llu;
			arma::uword num_t_ = 1llu;

			// circuit index
			arma::uword circuit_index_ = 0llu;

			// temperature of the coil
			fltp operating_temperature_ = RAT_CONST(4.5);

			// winding geometry
			fltp num_turns_ = RAT_CONST(1.0);

			// design operation current
			fltp operating_current_ = RAT_CONST(0.0);

			// current sharing feature
			// when enabled the current can flow freely over
			// the cross section of the conductor (use for cables)
			bool enable_current_sharing_ = false;

			// edge refinement shifts the edge coordinates along the 
			// path length in an attempt to keep the cross sections planar
			bool refine_edges_ = true;

			// when enabled the orientation vectors are taken from the paths
			// when disabled the orientation vectors are calculated from the
			// relative position of the input paths
			bool use_path_vectors_ = false; 

			// number of gauss points
			arma::sword num_gauss_ = 2;

			// softening factor
			fltp softening_ = RAT_CONST(1.4);

		// methods
		public:
			// constructor
			ModelEdges();
			ModelEdges(
				const ShPathPr& pth1, 
				const ShPathPr& pth2, 
				const ShPathPr& pth3, 
				const ShPathPr& pth4, 
				const arma::uword num_n = 1, 
				const arma::uword num_t = 1);

			// factory methods
			static ShModelEdgesPr create();
			static ShModelEdgesPr create(
				const ShPathPr& pth1, 
				const ShPathPr& pth2, 
				const ShPathPr& pth3, 
				const ShPathPr& pth4, 
				const arma::uword num_n = 1, 
				const arma::uword num_t = 1);

			// set circuit index
			void set_circuit_index(const arma::uword circuit_index);
			void set_enable_current_sharing(const bool enable_current_sharing = true);
			void set_num_n(const arma::uword num_n);
			void set_num_t(const arma::uword num_t);
			void set_operating_current(const fltp operating_current);
			void set_num_turns(const fltp num_turns);
			void set_refine_edges(const bool refine_edges = true);
			void set_use_path_vectors(const bool use_path_vectors = true);
			void set_num_gauss(const arma::sword num_gauss);
			void set_softening(const fltp softening);

			// get circuit index
			arma::uword get_circuit_index()const;
			bool get_enable_current_sharing()const;
			arma::uword get_num_n()const;
			arma::uword get_num_t()const;
			fltp get_num_turns() const;
			fltp get_operating_current() const;
			bool get_refine_edges() const;
			bool get_use_path_vectors() const;
			arma::sword get_num_gauss()const;
			fltp get_softening()const;

			// function for adding a path to the path list
			arma::uword add_path(const ShPathPr &path);
			const ShPathPr& get_path(const arma::uword index)const;
			bool delete_path(const arma::uword index);
			void reindex() override;
			arma::uword get_num_paths() const;

			// set operating conditions
			void set_operating_temperature(
				const fltp operating_temperature);
			
			// get operating temperature
			fltp get_operating_temperature() const;

			// create specific mesh
			virtual std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override;

			// refinement
			void refine_edges(
				arma::field<arma::Mat<fltp> > &R1234,
				arma::field<arma::Mat<fltp> > &L1234,
				arma::field<arma::Mat<fltp> > &N1234,
				arma::field<arma::Mat<fltp> > &D1234)const;

			// input validity
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
