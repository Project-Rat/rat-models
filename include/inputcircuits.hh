// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_INPUT_CIRCUITS_HH
#define MDL_INPUT_CIRCUITS_HH

#include <armadillo> 
#include <memory>
#include <map>

#include "rat/common/node.hh"

#include "circuit.hh"
#include "meshdata.hh"

// code specific to Rat
namespace rat{namespace mdl{
	
	// shared pointer definition
	typedef std::shared_ptr<class InputCircuits> ShInputCircuitsPr;

	// a collection of coils
	class InputCircuits: virtual public cmn::Node{
		// set coils
		protected:
			std::map<arma::uword, ShCircuitPr> circuits_;

		// methods
		public:
			// apply currents from circuits 
			void apply_circuit_currents(
				const fltp time, 
				const std::list<ShMeshDataPr> &meshes) const;

			// access stored circuits
			arma::uword add_circuit(const ShCircuitPr &circuit);
			void add_circuits(const arma::field<ShCircuitPr>& circuits);
			void add_circuits(const std::list<ShCircuitPr>& circuits);
			void set_circuits(const std::list<ShCircuitPr>& circuits);
			void set_circuits(const std::map<arma::uword, ShCircuitPr>& circuits);
			bool delete_circuit(const arma::uword index);
			virtual void reindex() override;
			ShCircuitPr get_circuit(const arma::uword index) const;
			std::list<ShCircuitPr> get_circuits() const;
			arma::uword num_circuits() const;

			// validity check
			virtual bool is_valid(const bool enable_throws) const override;
			
			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
