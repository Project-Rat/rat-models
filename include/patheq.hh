// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_EQ_HH
#define MDL_PATH_EQ_HH

#include <armadillo> 
#include <cassert>
#include <memory>

#include "rat/math/equation.hh"

#include "transformations.hh"
#include "path.hh"


// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathEq> ShPathEqPr;

	// cross section of coil
	class PathEq: public Path, public Transformations{
		// properties
		private:
			// running variable
			std::string variable_ = "x"; 
			math::Equation values_ = "linspace(0[s],1[s],101)";

			// run calculation
			math::Equation eqx_ = "sin(x)[m]";
			math::Equation eqy_ = "cos(x)[m]";
			math::Equation eqz_ = "0";
		
		// methods
		public:
			// constructor
			PathEq();
			PathEq(
				const std::string& variable,
				const math::Equation& values,
				const math::Equation& eqx, 
				const math::Equation& eqy, 
				const math::Equation& eqz = "0");
			
			// factory methods
			static ShPathEqPr create();
			static ShPathEqPr create(
				const std::string& variable,
				const math::Equation& values,
				const math::Equation& eqx, 
				const math::Equation& eqy, 
				const math::Equation& eqz = "0");

			// setters
			void set_eqx(const math::Equation& eqx);
			void set_eqy(const math::Equation& eqy);
			void set_eqz(const math::Equation& eqz);

			// get frame
			virtual ShFramePr create_frame(
				const MeshSettings &stngs) const override;

			// validity check
			virtual bool is_valid(
				const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif