// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_EMITTER_OMNI_HH
#define MDL_EMITTER_OMNI_HH

#include <armadillo> 
#include <memory>

#include "particle.hh"
#include "emitter.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class EmitterOmni> ShEmitterOmniPr;

	// omni directional particle emitter
	class EmitterOmni: public Emitter{
		// properties
		protected:
			// seed for random distribution
			unsigned int seed_ = 1001;

			// following unit system from CERN MAD
			// particle settings
			bool is_momentum_ = false;
			fltp beam_energy_ = RAT_CONST(0.0); // [GeV] including rest mass
			fltp beam_momentum_ = RAT_CONST(0.0); // [GeV/C]
			fltp sigma_beam_energy_ = RAT_CONST(0.0); // [GeV]
			fltp sigma_momentum_ = RAT_CONST(0.0); // [GeV/C] 
			fltp rest_mass_ = RAT_CONST(0.0); // [GeV/C^2]
			fltp charge_ = RAT_CONST(0.0); // elementary charge

			// number of particles spawned
			arma::uword num_particles_ = 1000llu;

			// emitter location
			arma::Col<fltp>::fixed<3> R_{RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0)};
			
			// track settings
			arma::uword lifetime_ = 501llu;

			// start index
			arma::uword start_idx_ = 0llu;

			// spawn radius
			fltp spawn_radius_ = RAT_CONST(0.0);

		// methods
		public:
			// constructor
			EmitterOmni();
			EmitterOmni(
				const fltp rest_mass, 
				const fltp beam_energy, 
				const fltp charge, 
				const arma::uword num_particles = 1000llu, 
				const arma::Col<fltp>::fixed<3> &R = {RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0)}, 
				const arma::uword lifetime = 501llu);

			// factory
			static ShEmitterOmniPr create();
			static ShEmitterOmniPr create(
				const fltp rest_mass, 
				const fltp beam_energy, 
				const fltp charge, 
				const arma::uword num_particles = 1000llu, 
				const arma::Col<fltp>::fixed<3> &R = {RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(0.0)}, 
				const arma::uword lifetime = 501llu);

			// default particles
			void set_proton();
			void set_electron();
			// void set_field_line(); // set TrackingType::FIELDLINES in CalcTracks instead

			// setting of properties
			void set_seed(const unsigned int seed);
			void set_beam_energy(const fltp beam_energy);
			void set_rest_mass(const fltp rest_mass);
			void set_charge(const fltp charge);
			void set_lifetime(const arma::uword lifetime);
			void set_num_particles(const arma::uword num_particles);
			void set_spawn_coord(const arma::Col<fltp>::fixed<3> &R);
			void set_spawn_radius(const fltp spawn_radius);
			void set_start_idx(const arma::uword start_idx);
			void set_is_momentum(const bool is_momentum = true);
			void set_sigma_energy(const fltp sigma_beam_energy);
			void set_beam_momentum(const fltp beam_momentum);
			void set_sigma_momentum(const fltp beam_momentum);

			// getters
			fltp get_rest_mass() const;
			arma::uword get_num_particles() const;
			fltp get_charge()const;
			fltp get_beam_energy()const;
			arma::uword get_lifetime()const;
			arma::Col<fltp>::fixed<3> get_spawn_coord()const;
			unsigned int get_seed()const;
			fltp get_spawn_radius()const;
			arma::uword get_start_idx()const;
			bool get_is_momentum()const;
			fltp get_sigma_energy()const;
			fltp get_sigma_momentum()const;
			fltp get_beam_momentum()const;

			// particle
			arma::field<Particle> spawn_particles(const fltp time) const override;

			// create specific mesh
			virtual std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override;
			
			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif