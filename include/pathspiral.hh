// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_SPIRAL_HH
#define MDL_PATH_SPIRAL_HH

#include <armadillo> 
#include <memory>
#include <cassert>
#include <json/json.h>

#include "path.hh"
#include "frame.hh"
#include "pathgroup.hh"
#include "patharc.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathSpiral> ShPathSpiralPr;

	// cross section of coil
	class PathSpiral: public Path, public Transformations{
		// properties
		protected:
			// bend in normal or transverse direction
			// where transverse implies hard-way bending
			bool transverse_ = true;

			// circle radius		
			fltp radius_ = 0.1; // [m]

			// number of sections
			fltp nt1_ = -10;
			fltp nt2_ = 10;

			// offset
			fltp pitch_ = RAT_CONST(6e-3); // [m] pitch
			fltp phase_ = RAT_CONST(0.0); // [rad] phase angle

			// element size
			fltp element_size_ = 4e-3; // [m]

		// methods
		public:
			// constructor
			PathSpiral();
			PathSpiral(
				const fltp radius, 
				const fltp nt1, 
				const fltp nt2, 
				const fltp pitch,
				const fltp element_size,
				const bool transverse);

			// factory
			static ShPathSpiralPr create();
			static ShPathSpiralPr create(
				const fltp radius, 
				const fltp nt1, 
				const fltp nt2, 
				const fltp pitch,
				const fltp element_size,
				const bool transverse);

			// set properties
			void set_radius(const fltp radius);
			void set_nt1(const fltp nt1);
			void set_nt2(const fltp nt2);
			void set_pitch(const fltp pitch);
			void set_element_size(const fltp element_size);
			void set_transverse(const bool transverse = true);
			void set_phase(const fltp phase);

			// get properties
			fltp get_radius() const;
			fltp get_nt1() const;
			fltp get_nt2() const;
			fltp get_pitch() const;
			fltp get_phase() const;
			fltp get_element_size() const;
			bool get_transverse()const;

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// validity check
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
