// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MODEL_MGNSPHERE_HH
#define MDL_MODEL_MGNSPHERE_HH

#include <armadillo> 
#include <memory>

#include "modelsphere.hh"
#include "drive.hh"
#include "drivedc.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelMgnSphere> ShModelMgnSpherePr;

	// model object that is capable of creating meshes
	// using a base path and a cross section
	class ModelMgnSphere: public ModelSphere{
		// properties
		protected:

			// magnetisation in frame coordinates
			arma::Col<fltp>::fixed<3> Mf_ = {RAT_CONST(0.0),RAT_CONST(0.0),RAT_CONST(1.0)/arma::Datum<fltp>::mu_0}; // M_L,M_N,M_D

			// softening factor
			fltp softening_ = RAT_CONST(1.4);

			// number of gauss points
			arma::sword num_gauss_surface_ = 2;
			arma::sword num_gauss_volume_ = 2;

			// current drive
			ShDrivePr magnetisation_drive_ = DriveDC::create(RAT_CONST(1.0));

		// methods
		public:
			// constructor
			ModelMgnSphere();
			ModelMgnSphere(
				const fltp radius, 
				const arma::Col<fltp>::fixed<3> &magnetisation, 
				const fltp element_size);

			// factory methods
			static ShModelMgnSpherePr create();
			static ShModelMgnSpherePr create(
				const fltp radius, 
				const arma::Col<fltp>::fixed<3> &magnetisation, 
				const fltp element_size);

			// setters
			void set_magnetisation(const arma::Col<fltp>::fixed<3> &Mf);
			void set_softening(const fltp softening); // softening factor
			void set_num_gauss_volume(const arma::sword num_gauss_volume);
			void set_num_gauss_surface(const arma::sword num_gauss_surface);

			// getters
			const arma::Col<fltp>::fixed<3>& get_magnetisation()const;
			fltp get_softening() const;
			arma::sword get_num_gauss_surface()const;
			arma::sword get_num_gauss_volume()const;

			// create specific mesh
			virtual std::list<ShMeshDataPr> create_meshes(
				const std::list<arma::uword> &trace = {}, 
				const MeshSettings &stngs = MeshSettings()) const override;

			// input validity
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list,
				const boost::filesystem::path &pth) override;
	};

}}

#endif
