// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_XYZ_HH
#define MDL_PATH_XYZ_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <functional>

#include "path.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathXYZ> ShPathXYZPr;

	// cross section of coil
	class PathXYZ: public Path, public Transformations{
		// properties
		private:
			// scaling factor
			fltp scale_factor_ = RAT_CONST(1.0);

			// coordinates
			arma::Mat<fltp> R_;

			// orientation vector normalization flag
			bool normalize_orientation_vectors_ = false;

			// orientation vectors
			arma::Mat<fltp> L_;
			arma::Mat<fltp> N_;
			arma::Mat<fltp> D_;

			// element size limitter
			fltp element_size_limit_ = RAT_CONST(0.0); // limit element size (use 0.0 for no limit)

			// out of plane vector for calculating orientation
			bool use_plane_vector_ = true;
			bool is_cylinder_ = false; // when enabled Vn should be set as the axis of the cylinder
			arma::Col<fltp>::fixed<3> Vn_{0,0,1};

		// methods
		public:
			// constructor
			PathXYZ();
			explicit PathXYZ(const arma::Mat<fltp> &R, const arma::Col<fltp>::fixed<3>& Vn = {0,0,1}, const bool is_cylinder = false);
			PathXYZ(const arma::Mat<fltp> &R, const arma::Mat<fltp> &L, const arma::Mat<fltp> &N, const arma::Mat<fltp> &D);

			// factory methods
			static ShPathXYZPr create();
			static ShPathXYZPr create(const arma::Mat<fltp> &R, const arma::Col<fltp>::fixed<3>& Vn = {0,0,1}, const bool is_cylinder = false);
			static ShPathXYZPr create(const arma::Mat<fltp> &R, const arma::Mat<fltp> &L, const arma::Mat<fltp> &N, const arma::Mat<fltp> &D);

			// set properties
			void set_use_plane_vector(const bool use_plane_vector = true);
			void set_scale_factor(const fltp scale_factor);
			void set_coords(const arma::Mat<fltp> &R);
			void set_longitudinal(const arma::Mat<fltp> &L);
			void set_normal(const arma::Mat<fltp> &N);
			void set_transverse(const arma::Mat<fltp> &D);
			void set_normal_vector(const arma::Col<fltp>::fixed<3>& Vn);
			void set_is_cylinder(const bool is_cylinder = true);
			void set_element_size_limit(const fltp element_size_limit);
			void set_normalize_orientation_vectors(const bool normalize_orientation_vectors = true);

			// get properties
			bool get_use_plane_vector()const;
			fltp get_scale_factor()const;
			const arma::Mat<fltp>& get_coords()const;
			const arma::Mat<fltp>& get_longitudinal()const;
			const arma::Mat<fltp>& get_normal()const;
			const arma::Mat<fltp>& get_transverse()const;
			bool get_is_cylinder()const;
			const arma::Col<fltp>::fixed<3>& get_normal_vector()const;
			fltp get_element_size_limit()const;
			bool get_normalize_orientation_vectors()const;

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// validity check
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif