// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_SURFACE_DATA_HH
#define MDL_SURFACE_DATA_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/log.hh"
#include "rat/common/elements.hh"
#include "rat/common/extra.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/stlfile.hh"

#include "trans.hh"
#include "nameable.hh"
#include "vtkunstr.hh"
#include "targetdata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class SurfaceData> ShSurfaceDataPr;
	typedef arma::field<ShSurfaceDataPr> ShSurfaceDataPrList;

	// mesh of a coil
	// this objects carries all properties 
	// required for the calculations
	class SurfaceData: public TargetData{
		// properties
		protected:
			// color in RGB values
			bool use_custom_color_;
			arma::Col<float>::fixed<3> color_;

			// // frame
			// ShFramePr gen_;

			// // cross sectional area
			// ShAreaPr area_;
			
			// node orientation vectors
			arma::Mat<fltp> L_;
			arma::Mat<fltp> N_;
			arma::Mat<fltp> D_;

			// elements
			arma::Mat<arma::uword> s_; // surface (tri/quad)

			// operating conditions
			fltp operating_temperature_;
			arma::Row<fltp> temperature_;

		// methods
		public:
			// default constructor
			SurfaceData();
			SurfaceData(
				const arma::Mat<fltp> &R,
				const arma::Mat<fltp> &L,
				const arma::Mat<fltp> &N,
				const arma::Mat<fltp> &D,
				const arma::Mat<arma::uword> &s);

			// virtual destructor
			virtual ~SurfaceData(){};

			// factory
			static ShSurfaceDataPr create();
			static ShSurfaceDataPr create(
				const arma::Mat<fltp> &R,
				const arma::Mat<fltp> &L,
				const arma::Mat<fltp> &N,
				const arma::Mat<fltp> &D,
				const arma::Mat<arma::uword> &s);

			// set area 
			// void set_area(const ShAreaPr &area);
			// void set_frame(const ShFramePr &gen);

			// set mesh
			void set_mesh(const arma::Mat<fltp> &R, const arma::Mat<arma::uword> &s);

			// function for calculating face normals
			arma::Mat<fltp> get_face_normals() const;

			// set/get color
			void set_use_custom_color(const bool use_custom_color);
			void set_color(const arma::Col<float>::fixed<3> &color);
			bool get_use_custom_color()const;
			const arma::Col<float>::fixed<3>& get_color()const;

			// set temperature
			void set_operating_temperature(const fltp operating_temperature);
			void set_temperature(const arma::Row<fltp> &temperature);

			// set orientation vectors
			void set_longitudinal(const arma::Mat<fltp> &L);
			void set_transverse(const arma::Mat<fltp> &D);
			void set_normal(const arma::Mat<fltp> &N);

			// access to vectors per section
			const arma::Mat<fltp>& get_direction() const;
			const arma::Mat<fltp>& get_normal() const;
			const arma::Mat<fltp>& get_transverse() const;

			// get coordinates and elements
			arma::Mat<fltp> get_element_coords()const;
			const arma::Mat<fltp>& get_coords() const;
			const arma::Mat<arma::uword>& get_elements() const;

			// get number of nodes and elements
			arma::uword get_num_nodes() const;
			arma::uword get_num_elements() const;

			// export vtk
			virtual ShVTKObjPr export_vtk() const override;
			void export_stl(const cmn::ShSTLFilePr &stlfile) const;
	};

}}

#endif
