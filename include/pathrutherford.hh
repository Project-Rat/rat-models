// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_RUTHERFORD_HH
#define MDL_PATH_RUTHERFORD_HH

#include <armadillo> 
#include <memory>

#include "path.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathRutherford> ShPathRutherfordPr;

	// path defining the shape of the cable
	// uses the coil path as input
	class PathRutherford: public Path, public Transformations{
		// properties
		protected:
			// basepath
			ShPathPr base_ = NULL;

			// geometry
			arma::uword num_strand_ = 20;
			fltp strand_diameter_ = 1.0e-3;
			
			// cable envelope
			fltp u1_ = 0;
			fltp u2_ = 0;
			fltp v1_ = 0;
			fltp v2_ = 0;

		// methods
		public:
			// constructor
			PathRutherford();
			explicit PathRutherford(ShPathPr base);
			
			// factory methods
			static ShPathRutherfordPr create();
			static ShPathRutherfordPr create(ShPathPr base);

			// set base path
			void set_base(ShPathPr base);
			void set_reverse_base(const bool reverse_base);

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;
			
			// indexing arrays
			void get_indexing(arma::Row<arma::uword> &section_idx, arma::Row<arma::uword> &turn_idx) const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
