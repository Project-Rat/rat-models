// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_DRIVE_SCURVE_HH
#define MDL_DRIVE_SCURVE_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "drive.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class DriveSCurve> ShDriveSCurvePr;
	typedef arma::field<ShDriveSCurvePr> ShDriveSCurvePrList;

	// circuit class
	class DriveSCurve: public Drive{
		// properties
		protected:
			// amplitude
			fltp amplitude_ = RAT_CONST(0.25);

			// scaling amplitude
			fltp tmid_ = RAT_CONST(0.0);
			
			// velocity 
			fltp velocity_ = RAT_CONST(0.0); 

			// slope
			fltp slope_ = RAT_CONST(0.8);

			// radius
			fltp radius_ = RAT_CONST(0.3);

			// offset 
			fltp offset_ = RAT_CONST(0.0);

		// methods
		public:
			// constructor
			DriveSCurve();
			DriveSCurve(
				const fltp offset, 
				const fltp amplitude, 
				const fltp slope, 
				const fltp radius, 
				const fltp tmid, 
				const fltp velocity = RAT_CONST(0.0));

			// factory
			static ShDriveSCurvePr create();
			static ShDriveSCurvePr create(
				const fltp offset, 
				const fltp amplitude, 
				const fltp slope, 
				const fltp radius, 
				const fltp tmid, 
				const fltp velocity = RAT_CONST(0.0));

			// setters
			void set_tmid(const fltp tmid);
			void set_radius(const fltp radius);
			void set_slope(const fltp slope);
			void set_amplitude(const fltp amplitude);
			void set_offset(const fltp offset);
			void set_velocity(const fltp velocity);

			// getters
			fltp get_tmid()const;
			fltp get_radius()const;
			fltp get_slope()const;
			fltp get_amplitude()const;
			fltp get_offset()const;
			fltp get_velocity()const;

			// plot range
			virtual fltp get_v1() const override;
			virtual fltp get_v2() const override;

			// get scaling at given time
			fltp get_scaling(
				const fltp position,
				const fltp time = 0.0,
				const arma::uword derivative = 0) const override;
			
			// apply scaling for the input settings
			virtual void rescale(const fltp scale_factor) override;

			// validity check
			bool is_valid(const bool enable_throws)const override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
