// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_CCT_CUSTOM_HH
#define MDL_PATH_CCT_CUSTOM_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <cmath>
#include <functional>

#include "path.hh"
#include "transformations.hh"
#include "frame.hh"
#include "drive.hh"
#include "drivedc.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CCTHarmonic> ShCCTHarmonicPr;
	typedef arma::field<ShCCTHarmonicPr> ShCCTHarmonicPrList;

	// cross section of coil
	class CCTHarmonic: public cmn::Node{
		// properties
		protected:
			// normalize length from 0 to 1 for harmonic calculatoin
			bool normalize_length_ = true;

			// define this harmonic by angle instead of amplitude
			bool use_skew_angle_ = false;

			// // reference radius for zmod
			// rat::fltp reference_radius_ = 0.0;

			// stumpness parameter
			fltp b_ = 0.0;

			// which harmonic
			arma::uword num_poles_ = 0;
			bool is_skew_ = false;

		public:
			// virtual destructor (obligatory)
			virtual ~CCTHarmonic(){};

			// set functions
			void set_num_poles(const arma::uword num_poles);
			void set_is_skew(const bool is_skew = true);
			// void set_reference_radius(const rat::fltp reference_radius);

			// get functions
			arma::uword get_num_poles() const;
			bool get_is_skew() const;
			// fltp get_reference_radius()const;

			// set normalization of the length coordinates
			void set_normalize_length(const bool normalize_length = true);
			bool get_normalize_length() const;
			void set_use_skew_angle(const bool use_skew_angle = true);
			bool get_use_skew_angle()const;
			void set_b(const fltp b);
			fltp get_b()const;

			// calculate position from theta
			static arma::Row<fltp> calc_position(
				const arma::Row<fltp> &theta, 
				const fltp thetamin, 
				const fltp thetamax,
				const bool normalize_length);

			// calculate position from theta
			static fltp calc_dposition(
				const fltp thetamin, 
				const fltp thetamax,
				const bool normalize_length);

			// function for converting skew angle to amplitude
			static arma::field<arma::Row<fltp> > skew2amplitude(
				const arma::field<arma::Row<fltp> > &rho, 
				const arma::field<arma::Row<fltp> > &alpha, 
				const arma::uword num_poles);

			// calculate z from local skew angle and azymuthal position
			arma::field<arma::Row<fltp> > calc_z_core(
				const arma::field<arma::Row<fltp> > &amplitude, 
				const arma::Row<fltp> &theta)const;

			// geometry functions
			virtual arma::field<arma::Row<fltp> > calc_z(
				const arma::Row<fltp> &theta, 
				const arma::field<arma::Row<fltp> > &rho, 
				const fltp thetamin, 
				const fltp thetamax,
				const fltp time) const = 0;

			// radius-z modification for fixing harmonics in non round apertures
			void radius_zmod(
				arma::field<arma::Row<rat::fltp> > &c, 
				const arma::field<arma::Row<rat::fltp> > &rho)const;

			// validity check
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js,
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};
		
	// shared pointer definition
	typedef std::shared_ptr<class CCTHarmonicInterp> ShCCTHarmonicInterpPr;

	// harmonics by interpolation
	class CCTHarmonicInterp: public CCTHarmonic{
		protected:
			// interpolation arrays
			arma::Row<fltp> turn_{RAT_CONST(-1e5),RAT_CONST(1e5)};
			arma::Row<fltp> a_{RAT_CONST(1.0),RAT_CONST(1.0)};

		public:
			// constructor
			CCTHarmonicInterp();
			CCTHarmonicInterp(
				const arma::uword num_poles, 
				const bool is_skew, 
				const arma::Row<fltp> &turn, 
				const arma::Row<fltp> &a, 
				const bool normalize_length = true);
			
			// factory
			static ShCCTHarmonicInterpPr create();
			static ShCCTHarmonicInterpPr create(
				const arma::uword num_poles, 
				const bool is_skew, 
				const arma::Row<fltp> &turn, 
				const arma::Row<fltp> &a, 
				const bool normalize_length = true);

			// geometry functions
			arma::field<arma::Row<fltp> > calc_z(
				const arma::Row<fltp> &theta, 
				const arma::field<arma::Row<fltp> > &rho, 
				const fltp thetamin, 
				const fltp thetamax,
				const fltp time) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

	// shared pointer definition
	typedef std::shared_ptr<class CCTHarmonicEquation> ShCCTHarmonicEquationPr;

	// define the coordinate function, calculates amplitude or angle, as function of turn
	typedef std::function<arma::Row<fltp>(const arma::Row<fltp> &turn, const fltp time)> CCTCustomFun;


	// harmonics by interpolation
	class CCTHarmonicEquation: public CCTHarmonic{
		protected:
			// interpolation arrays
			CCTCustomFun fun_;

		public:
			// constructor
			CCTHarmonicEquation(
				const arma::uword num_poles, 
				const bool is_skew, 
				const CCTCustomFun &fun);
			
			// factory
			static ShCCTHarmonicEquationPr create(
				const arma::uword num_poles, 
				const bool is_skew, 
				const CCTCustomFun &fun);

			// geometry functions
			arma::field<arma::Row<fltp> > calc_z(
				const arma::Row<fltp> &theta, 
				const arma::field<arma::Row<fltp> > &rho, 
				const fltp thetamin, 
				const fltp thetamax,
				const fltp time) const override;
	};

	// shared pointer definition
	typedef std::shared_ptr<class CCTHarmonicDrive> ShCCTHarmonicDrivePr;

	// harmonics by interpolation
	class CCTHarmonicDrive: public CCTHarmonic{
		// properties
		protected:
			// drive
			ShDrivePr drive_ = DriveDC::create(RAT_CONST(1.0));

		// methods
		public:
			// constructor
			CCTHarmonicDrive();
			CCTHarmonicDrive(
				const arma::uword num_poles, 
				const bool is_skew, 
				const ShDrivePr &drive,
				const bool use_skew_angle);
			CCTHarmonicDrive(
				const arma::uword num_poles, 
				const bool is_skew, 
				const fltp skew,
				const bool use_skew_angle);
			
			// factory
			static ShCCTHarmonicDrivePr create();
			static ShCCTHarmonicDrivePr create(
				const arma::uword num_poles, 
				const bool is_skew, 
				const ShDrivePr &drive = DriveDC::create(RAT_CONST(0.0)),
				const bool use_skew_angle = false);
			static ShCCTHarmonicDrivePr create(
				const arma::uword num_poles, 
				const bool is_skew, 
				const fltp amplitude,
				const bool use_skew_angle = false);

			// setters
			void set_drive(const ShDrivePr &drive);
			
			// getters
			const ShDrivePr& get_drive() const;

			// geometry functions
			arma::field<arma::Row<fltp> > calc_z(
				const arma::Row<fltp> &theta, 
				const arma::field<arma::Row<fltp> > &rho, 
				const fltp thetamin, 
				const fltp thetamax,
				const fltp time) const override;

			// validity check
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};


	// shared pointer definition
	typedef std::shared_ptr<class PathCCTCustom> ShPathCCTCustomPr;

	// cross section of coil
	class PathCCTCustom: public Path, public Transformations{
		// properties
		protected:
			// spacing 
			bool use_field_style_omega_ = false;
			bool use_omega_normal_ = false; // apply the pitch in the normal direction of the cable

			// reverse skew direction
			bool is_reverse_ = false; // when enabled reverse the winding angle
			bool is_reverse_solenoid_ = false; // reverse the solenoid direction of the winding
			bool is_solenoid_ = false; // when enabled do not reverse winding odd layers
			
			// the new behaviour orients the cross section such that it grows outwards
			// this is now the default behaviour and this flag is just to maintain
			// backwards compatibility with models before v2.016.4
			// bool new_orientation_ = true;

			// number of turns
			fltp nt1_ = -RAT_CONST(10.0);
			fltp nt2_ = RAT_CONST(10.0);
			fltp nt3_ = -RAT_CONST(0.0);
			fltp nt4_ = RAT_CONST(0.0);

			// normalize length from 0 to 1 
			// for pitch and radius
			bool normalize_length_ = true;
			fltp nt1nrm_ = -RAT_CONST(10.0);
			fltp nt2nrm_ = RAT_CONST(10.0);

			// discretization
			arma::uword num_nodes_per_turn_ = 120;
			arma::uword num_sect_per_turn_ = 4; // recommended setting is 4*num_poles of the main harmonic

			// apply bending
			bool bending_origin_ = false;
			bool use_radius_ = false;
			fltp bending_arc_length_ = RAT_CONST(0.0);
			fltp bending_radius_ = RAT_CONST(0.0);

			// drives for pitch and radius
			ShDrivePr omega_ = DriveDC::create(RAT_CONST(6e-3)); // pitch function for calculating the pitch locally (also see use_omega_normal)
			ShDrivePr rho_ = DriveDC::create(RAT_CONST(40e-3)); // radius function for calculating the radius locally (also see use_local_radius)
			ShDrivePr twist_ = DriveDC::create(0.0); // rotate the cable around its frame locally
			ShDrivePr normal_scaling_ = DriveDC::create(1.0); // scale the normal vectors

			// num layers
			arma::uword num_layers_ = 1; // number of layers (should be even to balance solenoid)
			fltp rho_increment_ = RAT_CONST(0.0); // increment distance between layers

			// layer jumps
			bool use_layer_jumps_ = false; // try create layer jump (work in progress)
			
			// first lead (odd layers)
			bool enable_lead1_ = false; // enable the generation of current leads
			fltp theta_lead1_ = arma::Datum<fltp>::pi/4; // azymuthal position of the lead end
			fltp zlead1_ = 0.1; // axial position of the lead end with respect to coil end
			fltp leadness1_ = 0.05; // straightness of the lead-coil connection
			
			// second lead (odd layers)
			bool enable_lead2_ = false;
			fltp theta_lead2_ = arma::Datum<fltp>::pi/4;
			fltp zlead2_ = 0.1;
			fltp leadness2_ = 0.05;

			// third lead (even layers)
			bool enable_lead3_ = false; // enable the generation of current leads
			fltp theta_lead3_ = arma::Datum<fltp>::pi/4; // azymuthal position of the lead end
			fltp zlead3_ = 0.1; // axial position of the lead end with respect to coil end
			fltp leadness3_ = 0.05; // straightness of the lead-coil connection

			// fourth lead (even layers)
			bool enable_lead4_ = false;
			fltp theta_lead4_ = arma::Datum<fltp>::pi/4;
			fltp zlead4_ = 0.1;
			fltp leadness4_ = 0.05;

			// when enabled use the same angle 
			// for all layers, when disabled the
			// same amplitude is used
			bool same_alpha_ = false;

			// calculate the radius locally after calculating the axial coordinates
			// this avoids intersecting the turns
			bool use_local_radius_ = false;

			// use frenet serret equations directly
			// using analytical derivatives
			bool use_frenet_serret_ = false;

			// use binormal vector instead of darboux 
			// vector for transverse direction setting
			// only used when frenet-serret is enabled
			bool use_binormal_ = false;

			// stored harmonics
			std::map<arma::uword,ShCCTHarmonicPr> harmonics_; // map of added harmonic functions
		

		// methods
		public:
			// constructor
			PathCCTCustom();

			// factory methods
			static ShPathCCTCustomPr create();

			// set normalization
			void set_normalize_length(const bool normalize_length = true);
			bool get_normalize_length() const;

			// edit harmonics
			const ShCCTHarmonicPr& get_harmonic(const arma::uword index) const;
			arma::uword add_harmonic(const ShCCTHarmonicPr &harm);
			void clear_harmonics();
			bool delete_harmonic(const arma::uword index);
			arma::uword num_harmonics()const;

			// edit drives
			const ShDrivePr& get_omega()const;
			const ShDrivePr& get_rho()const;
			const ShDrivePr& get_twist()const;
			const ShDrivePr& get_normal_scaling()const;
			void set_omega(const fltp omega);
			void set_rho(const fltp rho);
			void set_omega(const ShDrivePr &omega);
			void set_normal_scaling(const ShDrivePr &normal_scaling);
			void set_rho(const ShDrivePr &rho);
			void set_twist(const ShDrivePr &twist);

			// set functions
			void set_is_reverse(const bool is_reverse = true);
			void set_is_reverse_solenoid(const bool is_reverse_solenoid = true);
			void set_is_solenoid(const bool is_solenoid = true);
			void set_use_local_radius(const bool use_local_radius = true);
			void set_bending_arc_length(const fltp bending_arc_length);
			void set_num_nodes_per_turn(const arma::uword num_nodes_per_turn);
			void set_range(const fltp nt1, const fltp nt2);
			void set_range(const fltp nt1, const fltp nt2, const fltp nt1nrm, const fltp nt2nrm);
			void set_nt1(const fltp nt1);
			void set_nt2(const fltp nt2);
			void set_nt3(const fltp nt3);
			void set_nt4(const fltp nt4);
			void set_num_layers(const arma::uword num_layers);
			void set_rho_increment(const fltp rho_increment);
			void set_use_omega_normal(const bool use_omega_normal = true);
			void set_use_field_style_omega(const bool use_field_style_omega = true);
			void set_nt1nrm(const fltp nt1nrm);
			void set_nt2nrm(const fltp nt2nrm);
			void set_use_layer_jumps(const bool use_layer_jumps = true);
			void set_enable_lead1(const bool enable_lead1 = true);
			void set_enable_lead2(const bool enable_lead2 = true);
			void set_enable_lead3(const bool enable_lead3 = true);
			void set_enable_lead4(const bool enable_lead4 = true);
			void set_leadness1(const fltp leadness1);
			void set_leadness2(const fltp leadness2);
			void set_leadness3(const fltp leadness3);
			void set_leadness4(const fltp leadness4);
			void set_zlead1(const fltp zlead1);
			void set_zlead2(const fltp zlead2);
			void set_zlead3(const fltp zlead3);
			void set_zlead4(const fltp zlead4);
			void set_theta_lead1(const fltp theta_lead1);
			void set_theta_lead2(const fltp theta_lead2);
			void set_theta_lead3(const fltp theta_lead3);
			void set_theta_lead4(const fltp theta_lead4);
			void set_use_radius(const bool use_radius = true);
			void set_bending_origin(const bool bending_origin = true);
			void set_bending_radius(const fltp bending_radius);
			void set_same_alpha(const bool same_alpha = true);
			void set_use_frenet_serret(const bool use_frenet_serret = true);
			void set_num_sect_per_turn(const arma::uword num_sect_per_turn);
			void set_use_binormal(const bool use_binormal = true);
			// void set_new_orientation(const bool new_orientation = true);

			// get functions
			bool get_is_reverse() const;
			bool get_is_reverse_solenoid() const;
			bool get_is_solenoid() const;
			bool get_use_local_radius() const;
			fltp get_bending_arc_length() const;
			arma::uword get_num_nodes_per_turn() const;
			fltp get_nt1() const;
			fltp get_nt2() const;
			fltp get_nt3() const;
			fltp get_nt4() const;
			arma::uword get_num_layers() const;
			fltp get_rho_increment()const;
			bool get_use_omega_normal() const;
			fltp get_nt1nrm() const;
			fltp get_nt2nrm() const;
			bool get_use_layer_jumps()const;
			bool get_enable_lead1()const;
			bool get_enable_lead2()const;
			bool get_enable_lead3()const;
			bool get_enable_lead4()const;
			fltp get_leadness1()const;
			fltp get_leadness2()const;
			fltp get_leadness3()const;
			fltp get_leadness4()const;
			fltp get_zlead1()const;
			fltp get_zlead2()const;
			fltp get_zlead3()const;
			fltp get_zlead4()const;
			fltp get_theta_lead1()const;
			fltp get_theta_lead2()const;
			fltp get_theta_lead3()const;
			fltp get_theta_lead4()const;
			bool get_use_radius()const;
			bool get_bending_origin()const;
			fltp get_bending_radius() const;
			bool get_same_alpha()const;
			bool get_use_frenet_serret()const;
			arma::uword get_num_sect_per_turn()const;
			bool get_use_binormal()const;
			// bool get_new_orientation()const;

			// get frame function
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// interpolation function
			static arma::Mat<rat::fltp> interp_matrix(
				const arma::Col<fltp> &X, 
				const arma::Mat<fltp> &M, 
				const arma::Col<fltp> &Xi);

			// tree structure
			void reindex() override;

			// validity check
			bool is_valid_drives(const bool enable_throws)const;
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
