// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CIRCUIT_HH
#define MDL_CIRCUIT_HH

#include <armadillo> 
#include <memory>

#include "drive.hh"
#include "drivedc.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Circuit> ShCircuitPr;

	// a collection of coils
	class Circuit: virtual public cmn::Node{
		// set coils
		protected:
			// powering settings
			// circuit id
			arma::uword circuit_id_;

			// current drive
			ShDrivePr current_drive_;


		// methods
		public:
			// consrtuctor
			Circuit();
			explicit Circuit(
				const arma::uword id, 
				const ShDrivePr &current_drive);
			static ShCircuitPr create();
			static ShCircuitPr create(
				const arma::uword id, 
				const ShDrivePr &current_drive = DriveDC::create(0.0));
			
			// setters
			void set_circuit_id(const arma::uword circuit_id);
			void set_current_drive(const ShDrivePr &current_drive);	

			// getters
			arma::uword get_circuit_id()const;
			ShDrivePr get_current_drive()const;

			// get current as function of time
			fltp get_current(const fltp time)const;
			fltp get_dcurrent(const fltp time)const;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// protection
			virtual fltp get_detection_voltage()const;
			virtual fltp get_detection_delay_time()const;
			virtual fltp get_heater_delay_time()const;
			virtual fltp get_switching_time()const;
			virtual fltp get_fraction_hit()const;
			virtual fltp calc_protection_voltage(const fltp current)const;
			virtual fltp get_transverse_propagation_fraction()const;
			virtual bool get_is_rapid()const;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif







