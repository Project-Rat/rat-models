// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_COSTHETA_HH
#define MDL_PATH_COSTHETA_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/node.hh"
#include "rat/common/extra.hh"

#include "transformations.hh"
#include "path.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CosThetaBlock> ShCosThetaBlockPr;

	// blocks
	class CosThetaBlock: public cmn::Node{
		// properties
		private:
			// wedges 
			bool no_inter_turn_wedges_ = true;

			// symmetry
			arma::uword num_poles_ = 1llu; // 1 dipole, 2 quadrupole etc.

			// 2D settings
			arma::uword num_cables_ = 5llu;
			fltp radius_ = RAT_CONST(25e-3); // [m]
			fltp phi_  = RAT_CONST(0.0); // [rad]
			fltp alpha_  = RAT_CONST(0.0); // [rad]
			fltp dinner_  = RAT_CONST(1.4e-3); // [m]
			fltp douter_  = RAT_CONST(1.5e-3); // [m]
			fltp dinsu_ = RAT_CONST(0.0);
			fltp wcable_ = RAT_CONST(10e-3); // [m]

			// nose positiona and inclination
			fltp zend_ = 0.25; // acial position of the nose of the coil
			fltp beta_ = 55.0*arma::Datum<fltp>::tau/360; // inclination angle of the coil end
			
			// hyperellipse
			fltp lambda_ = RAT_CONST(1.2); // ratio between a and b for first turn
			fltp n1_ = RAT_CONST(2.0); // order of hyperellipse
			fltp n2_ = RAT_CONST(2.0);

		// methods
		public:
			// constructor
			CosThetaBlock();

			// constructor
			CosThetaBlock(
				const arma::uword num_poles,
				const arma::uword num_cables, 
				const fltp radius,
				const fltp phi, 
				const fltp alpha, 
				const fltp dinner, 
				const fltp douter,
				const fltp wcable,
				const fltp zend = 0.25,
				const fltp beta = 80.0*arma::Datum<fltp>::tau/360,
				const fltp dinsu = 0.0);

			// factory
			static ShCosThetaBlockPr create();

			// factory
			static ShCosThetaBlockPr create(
				const arma::uword num_poles,
				const arma::uword num_cables,
				const fltp radius, 
				const fltp phi, 
				const fltp alpha,
				const fltp dinner,
				const fltp douter, 
				const fltp wcable,
				const fltp zend = 0.25,
				const fltp beta = 80.0*arma::Datum<fltp>::tau/360,
				const fltp dinsu = 0.0);

			// setters
			void set_no_inter_turn_wedges(const bool no_inter_turn_wedges = true);
			void set_num_poles(const arma::uword num_poles);
			void set_num_cables(const arma::uword num_cables);
			void set_radius(const fltp radius);
			void set_phi(const fltp phi);
			void set_alpha(const fltp alpha);
			void set_dinner(const fltp dinner);
			void set_douter(const fltp douter);
			void set_wcable(const fltp wcable);
			void set_zend(const fltp zend);
			void set_beta(const fltp beta);
			void set_n1(const fltp n1);
			void set_n2(const fltp n2);
			void set_lambda(const fltp lambda);
			void set_dinsu(const fltp dinsu);

			// getters
			bool get_no_inter_turn_wedges()const;
			arma::uword get_num_poles()const;
			arma::uword get_num_cables()const;
			fltp get_radius()const;
			fltp get_phi()const;
			fltp get_alpha()const;
			fltp get_dinner()const;
			fltp get_douter()const;
			fltp get_wcable()const;
			fltp get_zend()const;
			fltp get_beta()const;
			fltp get_n1()const;
			fltp get_n2()const;
			fltp get_lambda()const;
			fltp get_dinsu()const;

			// distance point to circle along a vector
			fltp circle_distance(
				const fltp radius, 
				const arma::Col<fltp>::fixed<2>&p, 
				const arma::Col<fltp>::fixed<2>&v);

			// shift coordinates of first corner point such that the cable is touching the cylinder
			arma::Col<fltp>::fixed<2> shift_coords_to_circle(
				const arma::Col<fltp>::fixed<2>&P1, 
				const arma::Col<fltp>::fixed<2>&D1, 
				const arma::Col<fltp>::fixed<2>&D12);

			// get keystone angle
			fltp get_keystone_angle()const;

			// get insulated cable width
			fltp get_insulated_width();

			// get insulated cable inner thickness
			fltp get_insulated_dinner();

			// get insulated cable outer thickness
			fltp get_insulated_douter();

			// create cable frames
			void create_cable_frames(arma::Mat<fltp>&R, arma::Mat<fltp>&D);

			// shift coordinates of first corner point such that the cable is touching the cylinder
			arma::Col<fltp>::fixed<2> shift_coords_to_radius(
				const arma::Col<fltp>::fixed<2>&P12, 
				const arma::Col<fltp>::fixed<2>&Dshift, 
				const fltp radius);

			// create nose coords for a given radius and beta for each turn
			// beta is the angle to D1
			void create_nose(const arma::Row<fltp> &radius, const arma::Row<fltp> &beta, arma::Mat<fltp>&Rnose);

			// check validity
			bool is_valid(const bool enable_throws)const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;

	};

	// shared pointer definition
	typedef std::shared_ptr<class PathCosTheta> ShPathCosThetaPr;

	// cross section of coil
	class PathCosTheta: public Path, public Transformations{
		// properties
		protected:
			// symmetric end
			bool use_lead_end_ = true;

			// element size
			fltp element_size_ = RAT_CONST(5e-3);

			// store blocks
			std::map<arma::uword, ShCosThetaBlockPr> blocks_;

		// methods
		public:
			// default constructor
			PathCosTheta();

			// constructor
			PathCosTheta(const std::list<ShCosThetaBlockPr>& blocks, const fltp element_size);

			// factory
			static ShPathCosThetaPr create();

			// factory
			static ShPathCosThetaPr create(const std::list<ShCosThetaBlockPr>& blocks, const fltp element_size);

			// setters
			void set_element_size(const fltp element_size);
			void set_use_lead_end(const bool use_lead_end = true);

			// getters
			fltp get_element_size()const;
			bool get_use_lead_end()const;

			// get input path
			const ShCosThetaBlockPr& get_block(const arma::uword index) const;

			// add a block
			arma::uword add_block(const ShCosThetaBlockPr &block);
			bool delete_block(const arma::uword index);
			void reindex() override;
			arma::uword get_num_blocks() const;
				
			// calculate natural inclination angle
			static fltp calc_natural_inclination(
				const arma::uword num_poles, const fltp radius, 
				const fltp phi, const fltp ba, const fltp n1, 
				const fltp n2, const fltp delta_radius = 0.0);

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;
				
			// validity check
			virtual bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
