// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_POLAR_GRID_DATA_HH
#define MDL_POLAR_GRID_DATA_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/log.hh"

#include "vtkimg.hh"
#include "meshdata.hh"
#include "cartesiangriddata.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PolarGridData> ShPolarGridDataPr;

	// template for coil
	class PolarGridData: public GridData{
		// properties
		protected:
			// position 
			arma::Col<fltp>::fixed<3> offset_;

			// dimensions
			char axis_;
			fltp rin_;
			fltp rout_;
			fltp theta1_;
			fltp theta2_;
			fltp zlow_;
			fltp zhigh_;

			// number of points
			arma::uword num_rad_;
			arma::uword num_theta_;
			arma::uword num_axial_;

			// enable visibility
			bool visibility_ = true;

		// methods
		public:
			// constructor
			PolarGridData();
			PolarGridData(
				const char axis,
				const fltp rin, const fltp rout, const arma::uword num_rad,
				const fltp theta1, const fltp theta2, const arma::uword num_theta,
				const fltp zlow, const fltp zhigh, const arma::uword num_axial,
				const arma::Col<fltp>::fixed<3> offset);

			// factory methods
			static ShPolarGridDataPr create();
			static ShPolarGridDataPr create(
				const char axis,
				const fltp rin, const fltp rout, const arma::uword num_rad,
				const fltp theta1, const fltp theta2, const arma::uword num_theta,
				const fltp zlow, const fltp zhigh, const arma::uword num_axial,
				const arma::Col<fltp>::fixed<3> offset);

			// setters
			void set_offset(const arma::Col<fltp>::fixed<3> &offset);
			void set_axis(const char axis);
			void set_rin(const fltp rin);
			void set_rout(const fltp rout);
			void set_theta1(const fltp theta1);
			void set_theta2(const fltp theta2);
			void set_zlow(const fltp zlow);
			void set_zhigh(const fltp zhigh);
			void set_num_rad(const arma::uword num_rad);
			void set_num_theta(const arma::uword num_theta);
			void set_num_axial(const arma::uword num_axial);

			// getters
			arma::Col<fltp>::fixed<3> get_offset()const;
			char get_axis()const;
			fltp get_rin()const;
			fltp get_rout()const;
			fltp get_theta1()const;
			fltp get_theta2()const;
			fltp get_zlow()const;
			fltp get_zhigh()const;
			arma::uword get_num_rad()const;
			arma::uword get_num_theta()const;
			arma::uword get_num_axial()const;

			// calculation
			virtual void setup_targets() override;

			// interpolate to grid
			virtual arma::Mat<fltp> interpolate(
				const arma::Mat<fltp> &values,
				const arma::Mat<fltp> &R, 
				const bool use_parallel = true)const override;

			// extract iso surface
			virtual ShMeshDataPr extract_isosurface(
				const arma::Row<fltp> &values,
				const fltp iso_value, 
				const bool use_parallel = true) const override;

			// export vtk function
			virtual ShVTKObjPr export_vtk() const override;
				
			// create element matrix
			virtual arma::Mat<arma::uword> create_elements()const override;

			// get polar coordinates
			arma::Mat<fltp> get_target_polar_coords()const;

			// get field components
			arma::Mat<fltp> get_azymuthal()const;
			arma::Mat<fltp> get_radial()const;
			arma::Mat<fltp> get_axial()const;

			// integral
			ShCartesianGridDataPr azymuthal_integral()const;
	};

}}

#endif
