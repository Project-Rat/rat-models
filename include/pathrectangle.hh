// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_PATH_RECTANGLE_HH
#define MDL_PATH_RECTANGLE_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "path.hh"
#include "frame.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathRectangle> ShPathRectanglePr;

	// cross section of coil
	class PathRectangle: public Path, public Transformations{
		// properties
		protected:
			// shape parameters
			fltp width_ = RAT_CONST(30e-3); // width of rectangle [m]
			fltp height_ = RAT_CONST(50e-3); // height of rectangle [m]
			fltp radius_ = RAT_CONST(5e-3); // radius at corners [m]

			// element size 
			fltp element_size_ = RAT_CONST(2e-3); // size of the elements [m]

			// offset
			fltp offset_ = RAT_CONST(0.0); // ofset [m]

			// bend for small synchrotrons
			// this is different from bending 
			// transformation as it only bends 
			// the straight section. When
			// set to zero the feature is 
			// disabled.
			fltp bending_radius_ = RAT_CONST(0.0); // [m]

		// methods
		public:
			// constructor
			PathRectangle();
			PathRectangle(
				const fltp width, const fltp height, 
				const fltp radius, const fltp element_size);

			// factory
			static ShPathRectanglePr create();
			static ShPathRectanglePr create(
				const fltp width, const fltp height, 
				const fltp radius, const fltp element_size);

			// set properties
			void set_radius(const fltp radius);
			// void set_num_sections(const arma::uword num_sections);
			void set_width(const fltp width);
			void set_height(const fltp height);
			void set_element_size(const fltp element_size);
			void set_offset(const fltp offset);
			void set_bending_radius(const fltp bending_radius);

			// get properties
			fltp get_radius() const;
			// arma::uword get_num_sections() const;
			fltp get_width() const;
			fltp get_height() const;
			fltp get_element_size() const;
			fltp get_offset() const;
			fltp get_bending_radius() const;

			// get frame
			virtual ShFramePr create_frame(const MeshSettings &stngs) const override;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
