// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CALC_QUENCH0D_HH
#define MDL_CALC_QUENCH0D_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/rungekutta.hh"

#include "calcleaf.hh"
#include "quench0ddata.hh"


// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcQuench0D> ShCalcQuench0DPr;

	// calculate field on line defined 
	// by start and end point coordinates
	class CalcQuench0D: public CalcLeaf{
		// enumerates
		public:
			// scenarios to run
			enum class Scenario{FIRST, LAST, INDEX, WEAKEST, ALL};

		// properties
		protected:
			// run simulations for quenches in which coil
			Scenario scenario_ = Scenario::ALL;
			arma::uword scenario_circuit_ = 0;
			arma::uword scenario_coil_ = 0;

			// integration method
			cmn::RungeKutta::IntegrationMethod integration_method_ = 
				cmn::RungeKutta::IntegrationMethod::RUNGE_KUTTA_FEHLBERG45;

			// stop temperature
			fltp stop_temperature_ = RAT_CONST(1000.0); // [K]

			// stop time
			fltp stop_time_ = RAT_CONST(60.0); // [s]

			// maximum number of iterations
			arma::uword max_iter_ = 10000llu;

			// stop current
			fltp minimum_current_fraction_ = RAT_CONST(1e-3); // relative

			// solver settings
			fltp reltol_ = RAT_CONST(1e-6);
			fltp abstol_ = RAT_CONST(0.1);

			// energy check
			fltp energy_tolerance_ = RAT_CONST(1.0e-2);
			bool energy_check_hard_ = false;

			// normal zone settings
			bool use_nzp1d_ = false;
			fltp nzp_element_size_ = 25e-3;
			fltp nzp_length_ = 0.0;
			fltp num_nz_parallel_ = 1.0; // normal zone voltage multiplier
			fltp temperature_bias_ = RAT_CONST(1.0)/3; // this is a total fudge factor, if you don't like it use the 1D model

		// methods
		public:
			// constructor
			CalcQuench0D();
			explicit CalcQuench0D(const ShModelPr &model);
			
			// factory methods
			static ShCalcQuench0DPr create();
			static ShCalcQuench0DPr create(const ShModelPr &model);

			// setters
			void set_stop_temperature(const fltp stop_temperature);
			void set_stop_time(const fltp stop_time);
			void set_minimum_current_fraction(const fltp minimum_current_fraction);
			void set_reltol(const fltp reltol);
			void set_abstol(const fltp abstol);
			void set_integration_method(const cmn::RungeKutta::IntegrationMethod integration_method);
			void set_nzp_element_size(const fltp nzp_element_size);
			void set_use_nzp1d(const bool use_nzp1d = true);
			void set_nzp_length(const fltp nzp_length);
			void set_scenario(const CalcQuench0D::Scenario scenario);
			void set_scenario_circuit(const arma::uword scenario_circuit);
			void set_scenario_coil(const arma::uword scenario_coil);
			void set_num_nz_parallel(const fltp num_nz_parallel);
			void set_temperature_bias(const fltp temperature_bias);
			void set_max_iter(const arma::uword max_iter);

			// getters
			fltp get_stop_temperature()const;
			fltp get_stop_time()const;
			fltp get_minimum_current_fraction()const;
			fltp get_reltol()const;
			fltp get_abstol()const;
			cmn::RungeKutta::IntegrationMethod get_integration_method()const;
			fltp get_nzp_element_size()const;
			bool get_use_nzp1d()const;
			fltp get_nzp_length()const;
			Scenario get_scenario()const;
			arma::uword get_scenario_circuit()const;
			arma::uword get_scenario_coil()const;
			fltp get_num_nz_parallel()const;
			fltp get_temperature_bias()const;
			arma::uword get_max_iter()const;

			// calculate with inductance data output
			std::list<ShQuench0DDataPr> calculate_quench(
				const fltp time = RAT_CONST(0.0), 
				const cmn::ShLogPr &lg = cmn::NullLog::create(),
				const ShSolverCachePr& cache = NULL);

			// calculation function
			virtual std::list<ShDataPr> calculate(
				const fltp time, 
				const cmn::ShLogPr &lg = cmn::NullLog::create(),
				const ShSolverCachePr& cache = NULL) override;

			// vallidity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			virtual void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
