// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_MAXWELL_FIELDMAP_HH
#define MDL_MAXWELL_FIELDMAP_HH

// general headers
#include <armadillo> 
#include <memory>
#include <iomanip>
#include <list>
#include <map>

// rat-common headers
#include "rat/common/log.hh"

// rat-mlfmm headers
#include "rat/mlfmm/trackinggrid.hh"
#include "rat/mlfmm/settings.hh"

// rat-nl headers
#ifdef ENABLE_NL_SOLVER
#include "rat/nl/nonlinsolver.hh"
#endif

// rat-models headers
#include "circuit.hh"
#include "meshdata.hh"
#include "nldata.hh"
#include "solvercache.hh"
#include "background.hh"
#include "fieldmap.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class MaxwellFieldMap> ShMaxwellFieldMapPr;

	// fieldmap storing field as function of time
	// the calculated fields should be compliant
	// with the maxwell equations
	class MaxwellFieldMap: public FieldMap{
		// properties
		protected:
			// base current
			fltp base_current_ = RAT_CONST(1000.0);

			// settings for MLFMM
			fmm::ShSettingsPr stngs_;

			// maxwellian grids for calculating field from multipoles
			std::list<std::pair<ShCircuitPr, fmm::ShTrackingGridPr> > maxwellian_grids_;

			// field map of static sources
			fmm::ShTrackingGridPr static_grid_;

			// non-linear solver cache
			mdl::ShSolverCachePr cache_;

			// background field
			mdl::ShBackgroundPr bg_;

			// non-linear solver
			#ifdef ENABLE_NL_SOLVER
			// the solver object itself
			nl::ShNonLinSolverPr nl_solver_;

			// non-linear meshes
			std::list<ShNLDataPr> nl_meshes_;

			// field on non-linear targets
			std::list<std::pair<ShCircuitPr, arma::Mat<fltp> > > nl_dynamic_field_;

			// static field on non-linear targets
			arma::Mat<fltp> nl_static_field_;
			#endif

		// methods
		public:
			// constructor
			MaxwellFieldMap();

			// factory
			static ShMaxwellFieldMapPr create();

			// set an external solver cache
			void set_solver_cache(const mdl::ShSolverCachePr& cache);
			void set_fmm_settings(const fmm::ShSettingsPr& stngs);

			// check if a target point is inside the grid
			virtual arma::Row<arma::uword> is_inside(const arma::Mat<fltp>&Rt)const override;

			// check if the fieldmap is dynamic
			bool is_dynamic()const;

			// setup function
			void setup(
				const bool enable_dynamic,
				const fltp static_time,
				const std::list<ShCircuitPr>& circuits,
				const std::list<ShMeshDataPr>& source_meshes, 
				const std::list<ShMeshDataPr>& tracking_meshes,
				const mdl::ShBackgroundPr& bg,
				const cmn::ShLogPr& lg);

			// setup the FMM maxwellian grid
			static fmm::ShTrackingGridPr setup_maxwellian_grid(
				const bool use_direct_calculation,
				const fmm::ShSourcesPr& src, 
				const fmm::ShTargetsPr& tar, 
				const fmm::ShSettingsPr& stngs,
				const fmm::ShIListPr& ilist,
				const cmn::ShLogPr& lg);

			// calculation of E and B fields at target points
			virtual std::pair<arma::Mat<double>,arma::Mat<double> > calc_fields(
				const arma::Mat<double>& Rt, 
				const fltp time = RAT_CONST(0.0), 
				const cmn::ShLogPr& lg = cmn::NullLog::create())const override;

			// nonlinear field calculation
			#ifdef ENABLE_NL_SOLVER
			void calc_nl_field(
				const arma::Row<fltp>& Ae,
				const fmm::ShMgnTargetsPr& tar,
				const cmn::ShLogPr& lg)const;
			#endif
	};

}}

#endif
