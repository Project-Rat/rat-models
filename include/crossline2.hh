// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MDL_CROSS_LINE_2_HH
#define MDL_CROSS_LINE_2_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <json/json.h>

#include "cross.hh"
#include "area.hh"
#include "path.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CrossLine2> ShCrossLine2Pr;

	// cross section of coil
	class CrossLine2: public Cross{
		// properties
		private:
			// start u
			fltp u1_ = 0.0f; fltp v1_ = -6e-3;
			fltp u2_ = 0.0f; fltp v2_ = 6e-3;
			fltp thickness_ = 0.1e-3;
			fltp dl_ = 3e-3;

		// methods
		public:
			// constructors
			CrossLine2();
			CrossLine2(
				const fltp u1, const fltp v1,
				const fltp u2, const fltp v2,
				const fltp thickness, const fltp dl);

			// factory methods
			static ShCrossLine2Pr create();
			static ShCrossLine2Pr create(
				const fltp u1, const fltp v1,
				const fltp u2, const fltp v2,
				const fltp thickness, const fltp dl);

			// set properties
			void set_coord(
				const fltp u1, const fltp v1,
				const fltp u2, const fltp v2,
				const fltp dl);

			void get_coord() const;

			// setters
			void set_u1(const fltp u1);
			void set_u2(const fltp u2);
			void set_v1(const fltp v1);
			void set_v2(const fltp v2);
			void set_thickness(const fltp thickness);
			void set_element_size(const fltp dl);
			
			// getters
			fltp get_u1() const;
			fltp get_u2() const;
			fltp get_v1() const;
			fltp get_v2() const;
			fltp get_thickness() const;
			fltp get_element_size() const;

			// // get bounding box
			arma::Mat<fltp>::fixed<2,2> get_bounding() const override;

			// volume mesh
			virtual ShAreaPr create_area(const MeshSettings &stngs) const override;

			// vallidity check 
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();

			virtual void serialize(
				Json::Value &js,
				cmn::SList &list) const override;
			
			virtual void deserialize(
				const Json::Value &js,
				cmn::DSList &list,
				const cmn::NodeFactoryMap &factory_list,
				const boost::filesystem::path &pth) override;
	};
}}

#endif
